﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Text;
using OpenQA.Selenium.Remote;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Xml;
using System.Net;
using System.Globalization;
using System.Security.Cryptography;

using System.Data.OleDb;
using System.Data.SqlClient;
using DAL;

using WebApplication3;
using System.Web.Configuration;

namespace NeedlyApp
{
    public partial class WebWhatsapp1 : System.Web.UI.Page
    {
        WhatsAppConfig wa = new WhatsAppConfig();
        IWebDriver driver;
        Comman cmn = new Comman();
        int argFirstBrowserType = 2, delay = 4;
        string browserType = string.Empty;
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindSrNo1();
                BindSrNo2();
            }
           
        }

        protected void btnAddinstance_Click1(object sender, EventArgs e)
        {
            bool flag = false;
            string mobieNo = string.Empty;

            if (Session["WebDriver"] == null)
            {

                browserType = Comman.getBrowser(argFirstBrowserType);
                driver = Comman.SetBrowser(browserType);
                driver.Navigate().GoToUrl(wa.link);
                // driver  = new ChromeDriver(@"D:\\Tools");
                // driver.Navigate().GoToUrl("https://web.whatsapp.com");
                driver.Manage().Window.Maximize();
                #region login section
                do
                {
                    try
                    {
                        driver.FindElement(By.ClassName("_2MwRD"));
                        flag = true;
                    }
                    catch (Exception ex)
                    {
                    }
                } while (flag == false);

                #endregion login section
                if (flag)
                {
                    lblshowinstance.Text = "Instance Added";
                    lblshowinstance.ForeColor = System.Drawing.Color.LightCoral;
                    lblloggeninMNo.Text = "";
                    Session["WebDriver"] = driver;
                    mobieNo = cmn.GetMobileNumber(driver);
                    lblloggeninMNo.Text = "You Logged In: " + mobieNo;
                    lblloggeninMNo.ForeColor = System.Drawing.Color.Blue;
                    driver.Manage().Window.Minimize();
                }
                else
                {
                    driver.Manage().Window.Maximize();
                    lblloggeninMNo.Text = "";
                    lblshowinstance.Text = "Instance Not added.Please add your whatsapp instance code";
                    lblshowinstance.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                driver = (IWebDriver)Session["WebDriver"];
                lblshowinstance.Text = "Instance Added";
                lblshowinstance.ForeColor = System.Drawing.Color.LightCoral;
                mobieNo = mobieNo = cmn.GetMobileNumber(driver);
                lblloggeninMNo.Text = "You Logged In: " + mobieNo;
                lblloggeninMNo.ForeColor = System.Drawing.Color.Blue;
                driver.Manage().Window.Minimize();
            }
        }
        public void GridViewTemplate()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select Id, TemplateContent from Tbl_WhatsAppTemplateDetails where CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvTemplte.DataSource = ds.Tables[0];
                        GvTemplte.DataBind();
                    }

                    //string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
                    //String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    //LblContactCount.Text = ExcelContactCount;
                    //lblExcelCountMsg.Visible = true;
                    //PnlCB.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void GridViewForSerch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select SrNo ,MobileNo,CustomerName,CreatedBy from tbl_ContactDetails where MobileNo= '" + txtMobileNumber.Text + "' and CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvShowExcelData.DataSource = ds.Tables[0];
                        GvShowExcelData.DataBind();
                    }
                    else
                    {
                        Response.Redirect("No rocord found for Mobile number '" + txtMobileNumber.Text + "'");
                    }


                }
                catch (Exception ex)
                {

                }
            }
        }
        public void BindSrNo1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select * from tbl_ContactDetails where  CreatedBy = '" + Session["username"].ToString() + "' ");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    DdlSrno1.DataSource = dt;
                    DdlSrno1.DataBind();
                    DdlSrno1.DataTextField = "SrNo";
                    DdlSrno1.DataValueField = "ID";
                    DdlSrno1.DataBind();


                }
                catch (Exception ex)
                {

                }
            }




        }
        public void BindSrNo2()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select * from tbl_ContactDetails where  CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    DdlSrno2.DataSource = dt;
                    DdlSrno2.DataBind();
                    DdlSrno2.DataTextField = "SrNo";
                    DdlSrno2.DataValueField = "ID";
                    DdlSrno2.DataBind();


                }
                catch (Exception ex)
                {

                }
            }




        }

        public void BindBySrNo()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    //SqlCommand cmd = new SqlCommand("Select SrNo ,MobileNo,CustomerName,CreatedBy from tbl_ContactDetails where  CreatedBy = '" + Session["username"].ToString() + "' and SrNo Between '"+ DdlSrno1.SelectedItem.Text+"' and '"+DdlSrno2.SelectedItem.Text+"'");


                    try

                    {
                        SqlCommand cmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;


                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();
                        cmd.CommandText = "SP_DownloadCustomerContactDetails1";
                        cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("SrNo1", DdlSrno1.SelectedItem.Text);
                        cmd.Parameters.AddWithValue("SrNo2", DdlSrno2.SelectedItem.Text);

                        GvShowExcelData.DataSource = cmd.ExecuteReader();
                        GvShowExcelData.DataBind();

                    }

                    catch (Exception ex)

                    {

                        throw ex;

                    }
                    //else
                    //{
                    //    Response.Redirect("No rocord found for Mobile number '" + txtMobileNumber.Text + "'");
                    //}

                }
                catch (Exception ex)
                {

                }
            }
        }
        public void GridViewPhoneContact()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("select Id , MobileNo, FirstName from tbl_PhoneContacts where CreatedBy = '" + Session["username"].ToString() + "' ");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvPhoneContact.DataSource = ds.Tables[0];
                        GvPhoneContact.DataBind();
                    }

                    string SQl1 = "SELECT COUNT(Id) FROM tbl_PhoneContacts WHERE CreatedBy= '" + Session["username"].ToString() + "'";
                    String ExcelContactCount = cc.ExecuteScalar(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;

                }
                catch (Exception ex)
                {

                }
            }
        }
       
        protected void btnSearchByMobile_Click(object sender, EventArgs e)
        {
            GridViewForSerch();
        }

        protected void BtnSearchBySrNo_Click(object sender, EventArgs e)
        {
            BindBySrNo();
        }

      
        protected void rdbSelectContact_SelectedIndexChanged2(object sender, EventArgs e)
        {

            if (rdbSelectContact.SelectedValue == "1")
            {
                PnlShowExclData.Visible = true;
                PnlPhoneContacts.Visible = false;
                GridView();
            }
            if (rdbSelectContact.SelectedValue == "2")
            {
                PnlShowExclData.Visible = false;
                PnlPhoneContacts.Visible = true;
                GridViewPhoneContact();
            }
        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select SrNo ,MobileNo,CustomerName,CreatedBy from tbl_ContactDetails where CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvShowExcelData.DataSource = ds.Tables[0];
                        GvShowExcelData.DataBind();
                    }

                    string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
                    String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;
                    PnlSearch.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }

    }
}