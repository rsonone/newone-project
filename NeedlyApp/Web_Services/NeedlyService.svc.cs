﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.ServiceModel;
using System.Text;
using System.Web.Hosting;
using BAL;
using DAL;

namespace NeedlyApp.Web_Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NeedlyService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NeedlyService.svc or NeedlyService.svc.cs at the Solution Explorer and start debugging.
    public class NeedlyService : INeedlyService
    {
        string XMLParser = string.Empty;
        RegistrationsFunctions uploaddatanew = new RegistrationsFunctions();
        DownloadData Download = new DownloadData();

        //public List<NeedlyRegistrationDetail> NeedlyAppRegistration(Stream AppRegistration)
        //{
        //    try
        //    {
        //        using (StreamReader StreamXMLReader = new StreamReader(AppRegistration))
        //        {
        //            XMLParser = StreamXMLReader.ReadToEnd();
        //        }
        //        XMLParser = XMLParser.Replace("\"", "'");

        //        return Regfunc.NeedlyAppRegistration(XMLParser);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<FcmRegDetail> UploadFcmRegDetailsNeedly(Stream FcmRegDetailsData)
        {
            List<FcmRegDetail> objListFcmRegDetail = new List<FcmRegDetail>();

            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FcmRegDetailsData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFcmRegDetailsNeedly(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<NeedlyRegistrationDetail> NeedlyAppRegistration(Stream AppRegistration)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppRegistration))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.NeedlyAppRegistration(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<NeedlyRegistrationDetail> DownloadShopListWiseResult(string longitude, string latitude, string District, string Taluka)
        {
            List<NeedlyRegistrationDetail> XMLListJuniorjoinData = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListJuniorjoinData = Download.DownloadShopListWiseResult(longitude, latitude, District, Taluka);
            }
            catch (Exception ex)
            {

            }
            return XMLListJuniorjoinData.ToList();
        }

        public List<UploadOrder> UploadOrder(Stream Order)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Order))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadOrder(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadOrder> DownloadItem(string OrderId)
        {
            List<UploadOrder> XMLListItem = new List<UploadOrder>();
            try
            {
                XMLListItem = Download.DownloadItem(OrderId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        public List<UploadOrder> DownloadOrder(string Mobile_No, string UserRole)
        {
            List<UploadOrder> XMLListItem = new List<UploadOrder>();
            try
            {
                XMLListItem = Download.DownloadOrder(Mobile_No, UserRole);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<NeedlyRegistrationDetail> UploadProfileupdateData(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SendNotification> SendNotification(string Mobile_No, string OrderId, string StatusType)
        {
            List<SendNotification> XMLListItem = new List<SendNotification>();
            try
            {
                XMLListItem = Download.SendNotification(Mobile_No, OrderId, StatusType);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<DownloadBanner> DownloadBannerDetails(string State, string District, string Taluka, string WardNo, string BannerType)
        {
            List<DownloadBanner> XMLListItem = new List<DownloadBanner>();
            try
            {
                XMLListItem = Download.DownloadBannerDetails(State, District, Taluka, WardNo, BannerType);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<UploadKYCDetails> UploadKYCDetails(Stream KYCDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(KYCDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadKYCDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadSendOTPParameter> DownloadJsonSendOtp(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string UserType)
        {
            List<UploadSendOTPParameter> XMLDownloadSendOtp = new List<UploadSendOTPParameter>();

            try
            {
                XMLDownloadSendOtp = Download.SendOtpMethod(UserMobile, RefMobile, IMEINumber, AppKeyword, UserType);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }

        public List<UploadSendOTPParameter> DownloadJsonVerifyOTP(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string OtpValue)
        {
            List<UploadSendOTPParameter> XMLDownloadSendOtp = new List<UploadSendOTPParameter>();

            try
            {
                XMLDownloadSendOtp = Download.DownloadJsonVerifyOTP(UserMobile, RefMobile, IMEINumber, AppKeyword, OtpValue);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }

        public List<UploadOpenAppParameter> UploadOpenApp(Stream UploadData)
        {

            List<UploadOpenAppParameter> XMLListShortUrl = new List<UploadOpenAppParameter>();
            try
            {
                using (StreamReader streamReader = new StreamReader(UploadData))
                {
                    XMLParser = streamReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                XMLListShortUrl = uploaddatanew.UploadOpenApp(XMLParser);

                return XMLListShortUrl.ToList();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public List<ReportResultParameter> UploadReportResultMultipleShivbhojan(Stream UploadReportResultData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadReportResultData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReportResultMultipleShivbhojan(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<InventoryCategoryDetails> UploadInventoryCategory(Stream CategoryDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CategoryDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadInventoryCategory(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<InventoryComboDetails> UploadInventoryComboDetails(Stream InventoryComboDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(InventoryComboDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadInventoryComboDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<InventoryItem> UploadInventoryItem(Stream InventoryItem)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(InventoryItem))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadInventoryItem(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<DownloadInventoryItem> DownloadInventoryItem(string Reg_Id, string MobileNo)
        {
            List<DownloadInventoryItem> XMLListItem = new List<DownloadInventoryItem>();
            try
            {
                XMLListItem = Download.DownloadInventoryItem(Reg_Id, MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        public List<InventoryComboDetails> DownloadInventoryComboDetails(string Mobile_No, string Reg_Id)
        {
            List<InventoryComboDetails> XMLListItem = new List<InventoryComboDetails>();
            try
            {
                XMLListItem = Download.DownloadInventoryComboDetails(Mobile_No, Reg_Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<InventoryCategoryDetails> DownloadInventoryCategoryDetails(string Mobile_No, string Reg_Id)
        {
            List<InventoryCategoryDetails> XMLListItem = new List<InventoryCategoryDetails>();
            try
            {
                XMLListItem = Download.DownloadInventoryCategoryDetails(Mobile_No, Reg_Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        public List<AppUpdateParameter> UploadAppUpdate(Stream AppVersion)
        {
            List<AppUpdateParameter> objListAppUpdate = new List<AppUpdateParameter>();

            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppVersion))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(AppUpdateParameter));
                objListAppUpdate = uploaddatanew.UploadAppUpdate(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objListAppUpdate.ToList();
        }
        public List<InventoryCategoryDetails> DownloadInventoryCategoryDetailsNew(string Mobile_No, string Reg_Id)
        {
            List<InventoryCategoryDetails> XMLListItem = new List<InventoryCategoryDetails>();
            try
            {
                XMLListItem = Download.DownloadInventoryCategoryDetailsNew(Mobile_No, Reg_Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<UploadOrder> UploadOrderNew(Stream Order)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Order))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadOrderNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadReviewDetails> UploadReviewDetails(Stream ReviewDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReviewDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReviewDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadOrder> DownloadOrderNew(string Mobile_No, string UserRole)
        {
            List<UploadOrder> XMLListItem = new List<UploadOrder>();
            try
            {
                XMLListItem = Download.DownloadOrderNew(Mobile_No, UserRole);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<NeedlyRegistrationDetail> DownloadShopListWiseResultNew(string longitude, string latitude, string District, string Taluka,string Id)
        {
            List<NeedlyRegistrationDetail> XMLListJuniorjoinData = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListJuniorjoinData = Download.DownloadShopListWiseResultNew(longitude, latitude, District, Taluka,Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListJuniorjoinData.ToList();
        }
        
  public List<DownloadDashobordCount> DownloadDashbordCount( string Mobile_No)

        {
            List<DownloadDashobordCount> XMLListJuniorjoinData = new List<DownloadDashobordCount>();
            try
            {
                XMLListJuniorjoinData = Download.DownloadDashbordCount(Mobile_No);
            }
            catch (Exception ex)
            {

            }
            return XMLListJuniorjoinData.ToList();
        }
        public List<NeedlyRegistrationDetail> DownloadNeedlyRegistrationDetail(string Mobile)
        {
            List<NeedlyRegistrationDetail> XMLListJuniorjoinData = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListJuniorjoinData = Download.DownloadNeedlyRegistrationDetail(Mobile);
            }
            catch (Exception ex)
            {

            }
            return XMLListJuniorjoinData.ToList();
        }

        public List<UploadReferalDatails> UploadReferalDetails(Stream ReferalDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReferalDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferalDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<UploadNotifiction> UploadNotifactionData(Stream NotifactionData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NotifactionData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadNotifactionData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<DownloadReferral> DownloadReferaltableDetails(string MobileNo)
        {
            List<DownloadReferral> XMLListChapter = new List<DownloadReferral>();
            try
            {
                XMLListChapter = Download.DownloadReferaltableDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        public List<DownloadKYCDetails> DownloadKYCDetails(string MobileNo)
        {
            List<DownloadKYCDetails> XMLListChapter = new List<DownloadKYCDetails>();
            try
            {
                XMLListChapter = Download.DownloadKYCDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadOrder> UploadOrderNewDetails(Stream Order)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Order))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadOrderNewDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SendNotification> SendNotificationNew(string Mobile_No, string OrderId, string StatusType, string IsUpdate, string DeliveryCharges)
        {
            List<SendNotification> XMLListItem = new List<SendNotification>();
            try
            {
                XMLListItem = Download.SendNotificationNew(Mobile_No, OrderId, StatusType, IsUpdate, DeliveryCharges);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<Inventoryitem_Master> DownloadInventoryitem_Master(string ShopType)
        {
            List<Inventoryitem_Master> XMLListChapter = new List<Inventoryitem_Master>();
            try
            {
                XMLListChapter = Download.DownloadInventoryitem_Master(ShopType);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }



        public List<InventoryItem> UploadInventoryItemNew(Stream InventoryItem)
        {
            List<InventoryItem> objEnquiry = new List<InventoryItem>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(InventoryItem))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadInventoryItemNew(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<DownloadCategoryWiseShops> DownloadCategoryWiseShops(string AppKeyword)
        {
            List<DownloadCategoryWiseShops> XMLListChapter = new List<DownloadCategoryWiseShops>();
            try
            {
                XMLListChapter = Download.DownloadCategoryWiseShops(AppKeyword);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        
        public List<DownloadQuestionCategory> DownloadQuestionCategory(string ShopeType)
        {
            List<DownloadQuestionCategory> XMLListItem = new List<DownloadQuestionCategory>();
            try
            {
                XMLListItem = Download.DownloadQuestionCategory(ShopeType);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<Appointment_BasicDetail> UploadAppointment_BasicDetail(Stream Appointment_BasicDetail)
        {
            List<Appointment_BasicDetail> objEnquiry = new List<Appointment_BasicDetail>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Appointment_BasicDetail))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(Appointment_BasicDetail));
                objEnquiry = uploaddatanew.UploadAppointment_BasicDetail(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<Appointment_BasicDetail> DownloadAppointment_BasicDetail(string CreatedBy)
        {
            List<Appointment_BasicDetail> XMLListChapter = new List<Appointment_BasicDetail>();
            try
            {
                XMLListChapter = Download.DownloadAppointment_BasicDetail(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        public List<AppointmentShopDetails> UploadAppointmentShopDetails(Stream AppointmentShopeDetails)
        {
            List<AppointmentShopDetails> objEnquiry = new List<AppointmentShopDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentShopeDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
              
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof (AppointmentShopDetails));
                objEnquiry = uploaddatanew.UploadAppointmentShopDetails(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<AppointmentShopDetails> DownloadAppointmentShopDetails(string CreatedBy, string RegId)
        {
            // List<DownloadAppointmentShopDetails> XMLListChapter = new List<DownloadAppointmentShopDetails>();
            List<AppointmentShopDetails> XMLListChapter = new List<AppointmentShopDetails>();
            try
            {
                XMLListChapter = Download.DownloadAppointmentShopDetails(CreatedBy, RegId);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        public List<UploadBankDetails> UploadBankDetails(Stream BankDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(BankDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadBankDetails(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public List<UploadBankDetails> DownloadBankDetails(string CreatedBy, string RegId)
        {
            List<UploadBankDetails> XMLDownloadSendOtp = new List<UploadBankDetails>();
            try
            {
                XMLDownloadSendOtp = Download.DownloadBankDetails(CreatedBy, RegId);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }
        public List<UploadAppointmentBooking> UploadAppointmentBooking(Stream AppointmentBooking)
        {
            List<UploadAppointmentBooking> objEnquiry = new List<UploadAppointmentBooking>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.UploadAppointmentBooking(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }


        public List<UploadTimeSlot> UploadTimeSlot(Stream TimeSlot)
        {
            List<UploadTimeSlot> objRemark = new List<UploadTimeSlot>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(TimeSlot))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadTimeSlot));
                objRemark = uploaddatanew.UploadTimeSlot(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objRemark.ToList();
        }
        public List<UploadTimeSlot> DownloadTimeSlot(string CreatedBy)
        {
            List<UploadTimeSlot> XMLListNotesDetails = new List<UploadTimeSlot>();
            try
            {
                XMLListNotesDetails = Download.DownloadTimeSlot(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListNotesDetails.ToList();
        }


        public List<UploadOrder> UploadOrderNewDetails1(Stream Order)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Order))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadOrderNewDetails1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<AppointmentBooking> DownloadAppointmentBooking(string Mobile_No, string UserRole)
        {
            List<AppointmentBooking> XMLListChapter = new List<AppointmentBooking>();
            try
            {
                XMLListChapter = Download.DownloadAppointmentBooking(Mobile_No, UserRole);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }


        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew1(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew1(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<UploadBankDetails> UploadBankDetailsNew(Stream BankDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(BankDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadBankDetailsNew(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<DownloadReviewDetails> DownloadReviewsDetails(string RegistrationId)
        {
            List<DownloadReviewDetails> XMLDownloadSendOtp = new List<DownloadReviewDetails>();
            try
            {
                XMLDownloadSendOtp = Download.DownloadReviewsDetails(RegistrationId);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }

        public List<UploadReviewDetails> UploadReviewDetailsNew(Stream ReviewDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReviewDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReviewDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DownloadReviewRate> DownloadReviewsRate(string RegistrationId)
        {
            List<DownloadReviewRate> XMLDownloadSendOtp = new List<DownloadReviewRate>();
            try
            {
                XMLDownloadSendOtp = Download.DownloadReviewsRate(RegistrationId);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }
        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew2(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew2(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<UploadAppointmentBooking> UploadAppointmentBookingNew(Stream AppointmentBooking)
        {
            List<UploadAppointmentBooking> objEnquiry = new List<UploadAppointmentBooking>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.UploadAppointmentBookingNew(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }
        public List<UploadTimeSlot> DownloadTimeSlotDatewise(string CreatedBy, string Date, string DayId)
        {
            List<UploadTimeSlot> XMLListNotesDetails = new List<UploadTimeSlot>();
            try
            {
                XMLListNotesDetails = Download.DownloadTimeSlotDatewise(CreatedBy,Date,DayId);
            }
            catch (Exception ex)
            {

            }
            return XMLListNotesDetails.ToList();
        }


        public List<UploadAppointmentBooking> UploadAppointmentBookingNew1(Stream AppointmentBooking)
        {
            List<UploadAppointmentBooking> objEnquiry = new List<UploadAppointmentBooking>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.UploadAppointmentBookingNew1(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }


        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew3(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew3(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<DownloadAppointmentBooking> DownloadAppointmentBookingDetails(string MobileNo, string Date, string DayId)
        {
            List<DownloadAppointmentBooking> XMLListRegistration = new List<DownloadAppointmentBooking>();
            try
            {
                XMLListRegistration = Download.DownloadAppointmentBookingDetails(MobileNo, Date, DayId);
            }
            catch
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<UpdateAppointmentBooking> UpdateAppointmentBooking(Stream AppointmentBooking)
        {
            List<UpdateAppointmentBooking> objInsertPara = new List<UpdateAppointmentBooking>();

            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UpdateAppointmentBooking));
                objInsertPara = uploaddatanew.UpdateAppointmentBooking(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

       public List<DownloadAppointmentBookingStatus> DownloadAppointmentStatus(string ShopMobile, string UserMobile)
        {
            List<DownloadAppointmentBookingStatus> XMLListRegistration = new List<DownloadAppointmentBookingStatus>();
            try
            {
                XMLListRegistration = Download.DownloadAppointmentStatus(ShopMobile, UserMobile);
            }
            catch
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadOrder> UploadQuotationBasicDetails(Stream UploadData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadQuotationBasicDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadOrder> DownloadQuotationBasicDetails(string Latitude, string Longitude, string Role, string MobileNo, string State, string District, string Taluka)
        {
            List<UploadOrder> XMLListItem = new List<UploadOrder>();
            try
            {
                XMLListItem = Download.DownloadQuotationBasicDetails(Latitude, Longitude,Role,MobileNo,State,District,Taluka);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }



        public List<QuotationItemParameter> UploadQuotationItemsDetails(Stream UploadData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadQuotationItemsDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<QuotationItemParameter> DownloadQuotationItemsDetails(string QuotationBasic_Id)
        {
            List<QuotationItemParameter> XMLListItem = new List<QuotationItemParameter>();
            try
            {
                XMLListItem = Download.DownloadQuotationItemsDetails(QuotationBasic_Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<UploadOrder> UploadOrderNewDetails2(Stream Order)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Order))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadOrderNewDetails2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadOrder> UploadQuotationBasicDetails1(Stream UploadData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadQuotationBasicDetails1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<NeedlyRegistrationDetail> DownloadShopListWiseResult1(string longitude, string latitude, string District, string Taluka, string ShopType)
        {
            List<NeedlyRegistrationDetail> XMLListJuniorjoinData = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListJuniorjoinData = Download.DownloadShopListWiseResult1(longitude, latitude, District, Taluka, ShopType);
            }
            catch (Exception ex)
            {

            }
            return XMLListJuniorjoinData.ToList();
        }
        public List<Inventoryitem_Master> DownloadInventoryitem_MasterNew(string ShopType,string MaxId)
        {
            List<Inventoryitem_Master> XMLListChapter = new List<Inventoryitem_Master>();
            try
            {
                XMLListChapter = Download.DownloadInventoryitem_MasterNew(ShopType, MaxId);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }
        public List<UploadOrder> UploadOrderNewDetails3(Stream Order)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Order))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadOrderNewDetails3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadTimeSlot> UploadTimeSlot1(Stream TimeSlot)
        {
            List<UploadTimeSlot> objRemark = new List<UploadTimeSlot>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(TimeSlot))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadTimeSlot));
                objRemark = uploaddatanew.UploadTimeSlot1(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objRemark.ToList();
        }

        public List<UploadAppointmentBooking> UploadAppointmentBookingNew2(Stream AppointmentBooking)
        {
            List<UploadAppointmentBooking> objEnquiry = new List<UploadAppointmentBooking>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.UploadAppointmentBookingNew2(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<UploadAppointmentAddQuestion> UploadAppointmentAddQuestion(Stream UploadData)
        {
            List<UploadAppointmentAddQuestion> objEnquiry = new List<UploadAppointmentAddQuestion>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.UploadAppointmentAddQuestion(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<UploadAppointmentAddQuestion> DownloadAppointmentAddQuestion(string Createdby)
        {
            List<UploadAppointmentAddQuestion> XMLListItem = new List<UploadAppointmentAddQuestion>();
            try
            {
                XMLListItem = Download.DownloadAppointmentAddQuestion(Createdby);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }


        
            public List<UploadAppointmentBooking> updateAppointmentBookings(Stream AppointmentBooking)
        {
            List<UploadAppointmentBooking> objEnquiry = new List<UploadAppointmentBooking>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.updateAppointmentBookings(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }
        public List<UploadSharedContactsParameter> UpdateSharedContacts(Stream Shareddata)
        {
            List<UploadSharedContactsParameter> objEnquiry = new List<UploadSharedContactsParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Shareddata))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadSharedContactsParameter));
                objEnquiry = uploaddatanew.UpdateSharedContacts(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }


        public List<UploadSharedContactsParameter> DownloadSharedContacts(string Createdby)
        {
            List<UploadSharedContactsParameter> XMLListItem = new List<UploadSharedContactsParameter>();
            try
            {
                XMLListItem = Download.DownloadSharedContacts(Createdby);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<UploadAppointmentAddQuestion> UploadAppointmentAddQuestion1(Stream UploadData)
        {
            List<UploadAppointmentAddQuestion> objEnquiry = new List<UploadAppointmentAddQuestion>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadAppointmentBooking));
                objEnquiry = uploaddatanew.UploadAppointmentAddQuestion1(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<UploadAppointmentAddQuestion> DownloadAppointmentAddQuestionNew(string Createdby,string ShopType)
        {
            List<UploadAppointmentAddQuestion> XMLListItem = new List<UploadAppointmentAddQuestion>();
            try
            {
                XMLListItem = Download.DownloadAppointmentAddQuestionNew(Createdby, ShopType);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<UploadGroupFields> UploadGroupFields(Stream GroupFields)
        {
            List<UploadGroupFields> XMLVideoSOrL = new List<UploadGroupFields>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupFields))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadGroupFields));
                XMLVideoSOrL = uploaddatanew.UploadGroupFieldsData(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLVideoSOrL.ToList();
        }
        public List<DownloadGroupField> DownloadGroupField(string InstituteCode)
        {
            List<DownloadGroupField> XMLListDownloadGroup = new List<DownloadGroupField>();
            try
            {
                XMLListDownloadGroup = Download.DownloadGroupField(InstituteCode);
            }
            catch (Exception ex)
            {

            }
            return XMLListDownloadGroup.ToList();
        }




        public List<UploadLegder> UploadLedger(Stream Legder)
        {
            List<UploadLegder> objLegder = new List<UploadLegder>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Legder))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(UploadLegder));
                objLegder = uploaddatanew.UploadLegder(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objLegder.ToList();
        }
        public List<DownloadLedger> DownloadLedger(string Institute_Code)
        {
            List<DownloadLedger> XMLListDownloadLedger = new List<DownloadLedger>();
            try
            {
                XMLListDownloadLedger = Download.DownloadLedger(Institute_Code);
            }
            catch (Exception ex)
            {

            }
            return XMLListDownloadLedger.ToList();

        }

        public List<Voucher> DownloadTransactionAndVoucherDetailsNew(string InstituteCode, string FromDate, string ToDate)
        {
            List<Voucher> XMLListTransactionDetails = new List<Voucher>();
            try
            {
                XMLListTransactionDetails = Download.DownloadTransactionAndVoucherDetailsNew(InstituteCode, FromDate, ToDate);
            }
            catch (Exception ex)
            {

            }
            return XMLListTransactionDetails.ToList();
        }

        public List<UploadVoucherBlank> InsertBlankInVoucher()
        {
            List<UploadVoucherBlank> XMLListDiscountSchem = new List<UploadVoucherBlank>();
            try
            {
                XMLListDiscountSchem = Download.InsertBlankInVoucher();
            }
            catch (Exception ex)
            {

            }
            return XMLListDiscountSchem.ToList();
        }


        public List<VoucherDetailsParameter> UploadVoucherDetails(Stream UploadVoucher)
        {
            List<VoucherDetailsParameter> objRemark = new List<VoucherDetailsParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadVoucher))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(VoucherDetailsParameter));
                objRemark = uploaddatanew.UploadVoucherDetails(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objRemark.ToList();
        }

        public List<TransactionDetailsParameter> UploadTransactionDetailsNew(Stream UploadVoucher)
        {
            List<TransactionDetailsParameter> objRemark = new List<TransactionDetailsParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UploadVoucher))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("/", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(TransactionDetailsParameter));
                objRemark = uploaddatanew.UploadTransactionDetailsNew(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objRemark.ToList();
        }


        public List<UploadAddStaff> UploadAddStaff(Stream AddStaff)
        {
            List<UploadAddStaff> objInsertPara = new List<UploadAddStaff>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddStaff))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddStaff));
                objInsertPara = uploaddatanew.UploadAddStaff(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAddAttendance> UploadAddAttendance(Stream Attendance)
        {
            List<UploadAddAttendance> objInsertPara = new List<UploadAddAttendance>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Attendance))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddAttendance));
                objInsertPara = uploaddatanew.UploadAddAttendance(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAddStaff> DownloadAddStaff(string CreatedBy)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.DownloadAddStaff(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadAddAttendance> DownloadAddAttendance(string CreatedBy)
        {
            List<UploadAddAttendance> XMLListRegistration = new List<UploadAddAttendance>();
            try
            {
                XMLListRegistration = Download.DownloadAddAttendance(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadStaffDetails> UploadStaffDetails(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }


        public List<UploadStaffDetails> DownloadStaffDetails(string CreatedBy)
        {
            List<UploadStaffDetails> XMLListRegistration = new List<UploadStaffDetails>();
            try
            {
                XMLListRegistration = Download.DownloadStaffDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<AddLoan> UploadAddLoan(Stream AddLoan)
        {
            List<AddLoan> objInsertPara = new List<AddLoan>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddLoan))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(AddLoan));
                objInsertPara = uploaddatanew.UploadAddLoan(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<AddPayment> UploadAddPayment(Stream AddPayment)
        {
            List<AddPayment> objInsertPara = new List<AddPayment>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddPayment))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(AddPayment));
                objInsertPara = uploaddatanew.UploadAddPayment(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<AddBonus> UploadAddBonus(Stream AddBonus)
        {
            List<AddBonus> objInsertPara = new List<AddBonus>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddBonus))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(AddBonus));
                objInsertPara = uploaddatanew.UploadAddBonus(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<AddLoan> DownloadAddLoan(string CreatedBy)
        {
            List<AddLoan> XMLListRegistration = new List<AddLoan>();
            try
            {
                XMLListRegistration = Download.DownloadAddLoan(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<AddPayment> DownloadAddPayment(string CreatedBy)
        {
            List<AddPayment> XMLListRegistration = new List<AddPayment>();
            try
            {
                XMLListRegistration = Download.DownloadAddPayment(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<AddBonus> DownloadAddBonus(string CreatedBy)
        {
            List<AddBonus> XMLListRegistration = new List<AddBonus>();
            try
            {
                XMLListRegistration = Download.DownloadAddBonus(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<UploadNewsPaper> UploadNewsPaper(Stream NewsPaper)
        {
            List<UploadNewsPaper> objInsertPara = new List<UploadNewsPaper>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NewsPaper))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadNewsPaper));
                objInsertPara = uploaddatanew.UploadNewsPaper(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }



        public List<UploadNewsPaper> DownloadNewsPaper(string CreatedBy)
        {
            List<UploadNewsPaper> XMLListRegistration = new List<UploadNewsPaper>();
            try
            {
                XMLListRegistration = Download.DownloadNewsPaper(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }




        public List<RegisterNewsPaper> DownloadRegisterNewsPaper(string AppKeyword)
        {
            List<RegisterNewsPaper> XMLListRegistration = new List<RegisterNewsPaper>();
            try
            {
                XMLListRegistration = Download.DownloadRegisterNewsPaper(AppKeyword);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }




        public List<PaperRequest> UploadPaperRequest(Stream PaperRequest)
        {
            List<PaperRequest> objInsertPara = new List<PaperRequest>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(PaperRequest))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(PaperRequest));
                objInsertPara = uploaddatanew.UploadPaperRequest(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<BuildingDetails> UploadBuildingDetails(Stream BuildingDetails)
        {
            List<BuildingDetails> objInsertPara = new List<BuildingDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(BuildingDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(BuildingDetails));
                objInsertPara = uploaddatanew.UploadBuildingDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<BuildingDetails> DownloadBuildingDetails(string CreatedBy)
        {
            List<BuildingDetails> XMLListRegistration = new List<BuildingDetails>();
            try
            {
                XMLListRegistration = Download.DownloadBuildingDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<CustomerDetails> UploadCustomerDetails(Stream CustomerDetails)
        {
            List<CustomerDetails> objInsertPara = new List<CustomerDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(CustomerDetails));
                objInsertPara = uploaddatanew.UploadCustomerDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<CustomerDetails> DownloadCustomerDetails(string CreatedBy)
        {
            List<CustomerDetails> XMLListRegistration = new List<CustomerDetails>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadAddCustomerPaper> UploadAddCustomerPaper(Stream AddCustomerPaper)
        {
            List<UploadAddCustomerPaper> objInsertPara = new List<UploadAddCustomerPaper>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddCustomerPaper))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddCustomerPaper));
                objInsertPara = uploaddatanew.UploadAddCustomerPaper(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAddCustomerPaper> DownloadAddCustomerPaper(string CreatedBy)
        {
            List<UploadAddCustomerPaper> XMLListRegistration = new List<UploadAddCustomerPaper>();
            try
            {
                XMLListRegistration = Download.DownloadAddCustomerPaper(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<UploadAddCoupon> UploadAddCoupon(Stream AddCoupon)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddCoupon))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddCoupon(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadAddCoupon> DownloadAddCoupon(string CreatedBy)
        {
            List<UploadAddCoupon> XMLListRegistration = new List<UploadAddCoupon>();
            try
            {
                XMLListRegistration = Download.DownloadAddCoupon(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadAddHoliday> UploadAddHoliday(Stream AddHoliday)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddHoliday))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddHoliday(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadAddHoliday> DownloadAddHoliday(string CreatedBy)
        {
            List<UploadAddHoliday> XMLListRegistration = new List<UploadAddHoliday>();
            try
            {
                XMLListRegistration = Download.DownloadAddHoliday(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadMyStock> UploadMyStock(Stream MyStock)
        {
            List<UploadMyStock> objInsertPara = new List<UploadMyStock>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(MyStock))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadMyStock));
                objInsertPara = uploaddatanew.UploadMyStock(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadMyStock> DownloadMyStock(string CreatedBy)
        {
            List<UploadMyStock> XMLListRegistration = new List<UploadMyStock>();
            try
            {
                XMLListRegistration = Download.DownloadMyStock(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadCustomerBillDetails> UploadCustomerBillDetails(Stream CustomerBillDetails)
        {
            List<UploadCustomerBillDetails> objInsertPara = new List<UploadCustomerBillDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerBillDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadCustomerBillDetails));
                objInsertPara = uploaddatanew.UploadCustomerBillDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadCustomerBillDetails> DownloadCustomerBillDetails(string CreatedBy)
        {

            List<UploadCustomerBillDetails> XMLListRegistration = new List<UploadCustomerBillDetails>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerBillDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadAddCouponNew> UploadAddCouponNew(Stream AddCoupon)
        {

            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddCoupon))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddCouponNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadAddCouponNew> DownloadCouponPaperDetails(string CreatedBy)
        {
            List<UploadAddCouponNew> XMLListRegistration = new List<UploadAddCouponNew>();
            try
            {
                XMLListRegistration = Download.DownloadCouponPaperDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }



        public List<CustomerPaymentDetails> UploadCustomerPaymentDetails(Stream CustomerPaymentDetails)
        {
            List<CustomerPaymentDetails> objInsertPara = new List<CustomerPaymentDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerPaymentDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(CustomerPaymentDetails));
                objInsertPara = uploaddatanew.UploadCustomerPaymentDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<CustomerPaymentDetails> DownloadCustomerPaymentDetails(string CreatedBy)
        {
            List<CustomerPaymentDetails> XMLListRegistration = new List<CustomerPaymentDetails>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerPaymentDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<InventoryCategoryDetails> UploadInventoryCategoryNew(Stream CategoryDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CategoryDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadInventoryCategoryNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<InventoryItem> UploadInventoryItemNew1(Stream InventoryItem)
        {
            List<InventoryItem> objEnquiry = new List<InventoryItem>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(InventoryItem))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadInventoryItemNew1(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<NeedlyRegistrationDetail> DownloadNeedlyRegData(string DynamicURL)
        {
            List<NeedlyRegistrationDetail> XMLListLicenseRate = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListLicenseRate = Download.DownloadNeedlyRegData(DynamicURL);
            }
            catch (Exception ex)
            {

            }
            return XMLListLicenseRate.ToList();
        }

            public List<UploadCategoryMasterParameter> DownloadCategoryMaster(string ShopType)
        {
            List<UploadCategoryMasterParameter> XMLListLicenseRate = new List<UploadCategoryMasterParameter>();
            try
            {
                XMLListLicenseRate = Download.DownloadCategoryMaster(ShopType);
            }
            catch (Exception ex)
            {

            }
            return XMLListLicenseRate.ToList();
        }

      public  List<InventoryItem> DownloadImage(string Id)
                  {
            List<InventoryItem> XMLListLicenseRate = new List<InventoryItem>();
            try
            {
                XMLListLicenseRate = Download.DownloadImage(Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListLicenseRate.ToList();
        }


        public List<UploadNotifiction> UploadNotifactionMobileData(Stream NotifactionData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NotifactionData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadNotifactionMobileData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<UploadJobDetailsParameter> UploadJobDetails(Stream JobDataDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(JobDataDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadJobDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadJobDetailsParameter> DownloadJobDetails(string MobileNo)
        {
            List<UploadJobDetailsParameter> XMLListLicenseRate = new List<UploadJobDetailsParameter>();
            try
            {
                XMLListLicenseRate = Download.DownloadJobDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListLicenseRate.ToList();
        }


        public List<UploadJobDetailsParameter> DownloadStateWiseJobDetailS(string State, string District, string Taluka)
        {
            List<UploadJobDetailsParameter> XMLListItem = new List<UploadJobDetailsParameter>();
            try
            {
                XMLListItem = Download.DownloadStateWiseJobDetailS(State, District, Taluka);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        public List<UploadJobApplicationDetailsParameter> UploadJobApplicationDetails(string JobAppDataDetails, Stream stream)
        {
            try
            {
                //using (StreamReader StreamXMLReader = new StreamReader(WhatsAppMessageDetails))
                //{
                //    XMLParser = StreamXMLReader.ReadToEnd();
                //}
                XMLParser = JobAppDataDetails.Replace("\"", "'");

                return uploaddatanew.UploadJobApplicationDetails(XMLParser, stream);


            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadNewsPaper> UploadNewsPaperNew(Stream NewsPaper)
        {
            List<UploadNewsPaper> objInsertPara = new List<UploadNewsPaper>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NewsPaper))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadNewsPaper));
                objInsertPara = uploaddatanew.UploadNewsPaperNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<BuildingDetails> UploadBuildingDetailsNew(Stream BuildingDetails)
        {
            List<BuildingDetails> objInsertPara = new List<BuildingDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(BuildingDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(BuildingDetails));
                objInsertPara = uploaddatanew.UploadBuildingDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<CustomerDetails> UploadCustomerDetailsNew(Stream CustomerDetails)
        {
            List<CustomerDetails> objInsertPara = new List<CustomerDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(CustomerDetails));
                objInsertPara = uploaddatanew.UploadCustomerDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAddCustomerPaper> UploadAddCustomerPaperNew(Stream AddCustomerPaper)
        {
            List<UploadAddCustomerPaper> objInsertPara = new List<UploadAddCustomerPaper>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddCustomerPaper))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddCustomerPaper));
                objInsertPara = uploaddatanew.UploadAddCustomerPaperNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAddCouponNew> UploadAddCouponNew1(Stream AddCoupon)
        {

            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddCoupon))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddCouponNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAddHoliday> UploadAddHolidayNew(Stream AddHoliday)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddHoliday))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddHolidayNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadMyStock> UploadMyStockNew(Stream MyStock)
        {
            List<UploadMyStock> objInsertPara = new List<UploadMyStock>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(MyStock))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadMyStock));
                objInsertPara = uploaddatanew.UploadMyStockNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadCustomerBillDetails> UploadCustomerBillDetailsNew(Stream CustomerBillDetails)
        {
            List<UploadCustomerBillDetails> objInsertPara = new List<UploadCustomerBillDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerBillDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadCustomerBillDetails));
                objInsertPara = uploaddatanew.UploadCustomerBillDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<CustomerPaymentDetails> UploadCustomerPaymentDetailsNew(Stream CustomerPaymentDetails)
        {
            List<CustomerPaymentDetails> objInsertPara = new List<CustomerPaymentDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerPaymentDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(CustomerPaymentDetails));
                objInsertPara = uploaddatanew.UploadCustomerPaymentDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadTemplateParameter> DownloadTemplateDetails(string ShopType)
        {
            List<UploadTemplateParameter> XMLListRegistration = new List<UploadTemplateParameter>();
            try
            {
                XMLListRegistration = Download.DownloadTemplateDetails(ShopType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadJobApplicationDetailsParameter> DownloadApplicationJobId(string JobId)
        {
            List<UploadJobApplicationDetailsParameter> XMLListLicenseRate = new List<UploadJobApplicationDetailsParameter>();
            try
            {
                XMLListLicenseRate = Download.DownloadApplicationJobId(JobId);
            }
            catch (Exception ex)
            {

            }
            return XMLListLicenseRate.ToList();
        }


        public List<UploadJobApplicationDetailsParameter> DownloadJobApplication(string MobileNo)
        {
            List<UploadJobApplicationDetailsParameter> XMLListLicenseRate = new List<UploadJobApplicationDetailsParameter>();
            try
            {
                XMLListLicenseRate = Download.DownloadJobApplication(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListLicenseRate.ToList();
        }
        public List<UpdateRefMobileNoParameter> UpdateRefMobileNo(Stream UpdateMobile)
        {
            List<UpdateRefMobileNoParameter> objInsertPara = new List<UpdateRefMobileNoParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UpdateMobile))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UpdateRefMobileNoParameter));
                objInsertPara = uploaddatanew.UpdateRefMobileNo(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadCustomerBillDetails> UploadCustomerBillDetailsNew1(Stream CustomerBillDetails)
        {
            List<UploadCustomerBillDetails> objInsertPara = new List<UploadCustomerBillDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerBillDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadCustomerBillDetails));
                objInsertPara = uploaddatanew.UploadCustomerBillDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadReferalDatails> UploadReferalDetails1(Stream ReferalDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReferalDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferalDetails1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadStaffDetails> UploadStaffDetailsNew(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        
        public List<UpdateAddItemParameter> UploadAddItemMaster(Stream AddItemDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddItemDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddItemMaster(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UpdateAddItemParameter> DownloadAddItemMaster(string Category, string CreatedBy)
        {
            List<UpdateAddItemParameter> XMLListRegistration = new List<UpdateAddItemParameter>();
            try
            {
                XMLListRegistration = Download.DownloadAddItemMaster(Category, CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<UploadAssignedParameter> UploadAssignedStaff(Stream AssignedStaffDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AssignedStaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAssignedStaff(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAssignedParameter> DownloadAssignedStaff(string CreatedBy)
        {
            List<UploadAssignedParameter> XMLListRegistration = new List<UploadAssignedParameter>();
            try
            {
                XMLListRegistration = Download.DownloadAssignedStaff(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<UploadAllotManpowerParameter> UploadAllotManpower(Stream AllotManpowerDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AllotManpowerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAllotManpower(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAllotManpowerParameter> DownloadAllotManpower(string CreatedBy)
        {
            List<UploadAllotManpowerParameter> XMLListRegistration = new List<UploadAllotManpowerParameter>();
            try
            {
                XMLListRegistration = Download.DownloadAllotManpower(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadStaffProfileDetailsParameter> UploadStaffProfileDetails(Stream ProfileDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadStaffProfileDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadStaffProfileDetailsParameter> DownloadStaffProfileDetails(string ServerId)
        {
            List<UploadStaffProfileDetailsParameter> XMLListRegistration = new List<UploadStaffProfileDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadStaffProfileDetails(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<AddPayment> UploadAddClientPayment(Stream AddPayment)
        {
            List<AddPayment> objInsertPara = new List<AddPayment>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddPayment))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(AddPayment));
                objInsertPara = uploaddatanew.UploadAddClientPayment(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<AddPayment> DownloadAddClientPayment(string CreatedBy)
        {
            List<AddPayment> XMLListRegistration = new List<AddPayment>();
            try
            {
                XMLListRegistration = Download.DownloadAddClientPayment(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadNewsPaper> UploadNewsPaperNew1(Stream NewsPaper)
        {
            List<UploadNewsPaper> objInsertPara = new List<UploadNewsPaper>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NewsPaper))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadNewsPaper));
                objInsertPara = uploaddatanew.UploadNewsPaperNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAddAttendance> UploadAddAttendanceNew(Stream Attendance)
        {
            List<UploadAddAttendance> objInsertPara = new List<UploadAddAttendance>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Attendance))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddAttendance));
                objInsertPara = uploaddatanew.UploadAddAttendanceNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadAddStaff> UploadAddStaffNew(Stream AddStaff)
        {
            List<UploadAddStaff> objInsertPara = new List<UploadAddStaff>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddStaff))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddStaff));
                objInsertPara = uploaddatanew.UploadAddStaffNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAllotManpowerParameter> UploadAllotManpowerNew(Stream AllotManpowerDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AllotManpowerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAllotManpowerNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadCustomerBillDetails> UploadCustomerBillDetailsNew2(Stream CustomerBillDetails)
        {
            List<UploadCustomerBillDetails> objInsertPara = new List<UploadCustomerBillDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerBillDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadCustomerBillDetails));
                objInsertPara = uploaddatanew.UploadCustomerBillDetailsNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadStaffDetails> UploadStaffDetailsNew1(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadStaffDetails> DownloadShowStaffDetails(string CreatedBy, string MobileNo)
        {
            List<UploadStaffDetails> XMLListRegistration = new List<UploadStaffDetails>();
            try
            {
                XMLListRegistration = Download.DownloadShowStaffDetails(CreatedBy, MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadAddAttendance> UploadAddAttendanceNew1(Stream Attendance)
        {
            List<UploadAddAttendance> objInsertPara = new List<UploadAddAttendance>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Attendance))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddAttendance));
                objInsertPara = uploaddatanew.UploadAddAttendanceNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        public List<UploadAllotManpowerParameter> UploadAllotManpowerNew1(Stream AllotManpowerDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AllotManpowerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAllotManpowerNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadAllotManpowerParameter> UploadAllotManpowerNew2(Stream AllotManpowerDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AllotManpowerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAllotManpowerNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAddSubstituteDetailsParameter> UploadAddSubstituteDetails(Stream AddSubstituteDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddSubstituteDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddSubstituteDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadStaffDetails> UploadStaffDetailsNew2(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadAddSubstituteDetailsParameter> UploadAddSubstituteDetailsNew(Stream AddSubstituteDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddSubstituteDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddSubstituteDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UpdateAddItemParameter> UploadAddItemMasterNew(Stream AddItemDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddItemDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddItemMasterNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UpdateWorkDetailsParameter> UploadWorkDetails(Stream WorkDetailsData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WorkDetailsData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWorkDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadAddAttendance> UploadAddAttendanceNew2(Stream Attendance)
        {
            List<UploadAddAttendance> objInsertPara = new List<UploadAddAttendance>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Attendance))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddAttendance));
                objInsertPara = uploaddatanew.UploadAddAttendanceNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadStaffDetails> UploadStaffDetailsNew3(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadAddStaff> UploadAddStaffNew1(Stream AddStaff)
        {
            List<UploadAddStaff> objInsertPara = new List<UploadAddStaff>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddStaff))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadAddStaff));
                objInsertPara = uploaddatanew.UploadAddStaffNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadAssignedParameter> UploadAssignedStaffNew(Stream AssignedStaffDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AssignedStaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAssignedStaffNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<UploadStaffDetails> UploadStaffDetailsNew4(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew4(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        public List<UploadAddStaff> DeleteAddStaff(string ServerId)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = uploaddatanew.DeleteAddStaff(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadStaffDetails> DeleteStaffDetails(string ServerId)
        {
            List<UploadStaffDetails> XMLListRegistration = new List<UploadStaffDetails>();
            try
            {
                XMLListRegistration = uploaddatanew.DeleteStaffDetails(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CustomerDetails> DeleteCustomerDetails(string ServerId)
        {
            List<CustomerDetails> XMLListRegistration = new List<CustomerDetails>();
            try
            {
                XMLListRegistration = uploaddatanew.DeleteCustomerDetails(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

       public List<UploadAssignedParameter> DeleteAssignedStaff(string ServerId)
        {
            List<UploadAssignedParameter> XMLListRegistration = new List<UploadAssignedParameter>();
            try
            {
                XMLListRegistration = uploaddatanew.DeleteAssignedStaff(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UpdateAddItemParameter> UploadAddItemMasterNew1(Stream AddItemDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AddItemDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAddItemMasterNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


     
        public List<UploadAddStaff> NotificationDetailsNew(string MobileNo, string Message)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.NotificationDetailsNew(MobileNo, Message);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadAddStaff> NotificationGPSDetails(string MobileNo, string UserMobile)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.NotificationGPSDetails(MobileNo, UserMobile);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UploadStaffDetails> UploadStaffDetailsNew5(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew5(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }



        public List<CustomerDetails> UploadCustomerDetailsNew1(Stream CustomerDetails)
        {
            List<CustomerDetails> objInsertPara = new List<CustomerDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(CustomerDetails));
                objInsertPara = uploaddatanew.UploadCustomerDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }


        public List<UpdateWorkDetailsParameter> UploadWorkDetailsNew(Stream WorkDetailsData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WorkDetailsData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWorkDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerDetails> DownloadCustomerDetailsNew(string CreatedBy, string Date)
        {
            List<CustomerDetails> XMLListRegistration = new List<CustomerDetails>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerDetailsNew(CreatedBy, Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<UpdateWorkDetailsParameter> DownloadWorkDetails(string CreatedBy)
        {
            List<UpdateWorkDetailsParameter> XMLListChapter = new List<UpdateWorkDetailsParameter>();
            try
            {
                XMLListChapter = Download.DownloadWorkDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        public List<UploadExtraStaff> DownloadExtraStaff(string CreatedBy, string Date)
        {
            List<UploadExtraStaff> XMLListRegistration = new List<UploadExtraStaff>();
            try
            {
                XMLListRegistration = Download.DownloadExtraStaff(CreatedBy, Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadExtraStaff> UploadExtraStaff(Stream ExtraShiftData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExtraShiftData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExtraStaff(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAddSubstituteDetailsParameter> DownloadSubstituteDetails(string CreatedBy, string Date)
        {
            List<UploadAddSubstituteDetailsParameter> XMLListRegistration = new List<UploadAddSubstituteDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadSubstituteDetails(CreatedBy, Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<ParameterEmployeeDetails> UploadEmployeeDetails(Stream EmployeeData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(EmployeeData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadEmployeeDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterEmployeeDetails> DownloadEmployeeDetails(string CreatedBy, string Date)
        {
            List<ParameterEmployeeDetails> XMLListRegistration = new List<ParameterEmployeeDetails>();
            try
            {
                XMLListRegistration = Download.DownloadEmployeeDetails(CreatedBy, Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //30/01/2021
        public List<UploadAddAttendance> DownloadAddAttendanceNew(string CreatedBy, string Date)
        {
            List<UploadAddAttendance> XMLListRegistration = new List<UploadAddAttendance>();
            try
            {
                XMLListRegistration = Download.DownloadAddAttendanceNew(CreatedBy , Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //03/02/2021

        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew4(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew4(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //12/02/2021
        public List<ParameterAttendanceDetails> UploadAttendanceDetails(Stream AttendanceData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AttendanceData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAttendanceDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterAttendanceDetails> DownloadAttendanceDetails(string CreatedBy)
        {
            List<ParameterAttendanceDetails> XMLListRegistration = new List<ParameterAttendanceDetails>();
            try
            {
                XMLListRegistration = Download.DownloadAttendanceDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<ClientPaymentHistoryParameter> UploadClientPaymentHistoryDetails(Stream ClientPaymentData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ClientPaymentData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadClientPaymentHistoryDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ClientPaymentHistoryParameter> DownloadUploadClientPaymentDetails(string CreatedBy)
        {
            List<ClientPaymentHistoryParameter> XMLListRegistration = new List<ClientPaymentHistoryParameter>();
            try
            {
                XMLListRegistration = Download.DownloadUploadClientPaymentDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //15/02/2021
        public List<NeedlyRegistrationDetail> DownloadNeedlyRegistrationDetail1(string Mobile)
        {
            List<NeedlyRegistrationDetail> XMLListJuniorjoinData = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListJuniorjoinData = Download.DownloadNeedlyRegistrationDetail1(Mobile);
            }
            catch (Exception ex)
            {

            }
            return XMLListJuniorjoinData.ToList();
        }

        //16/02/2021
        public List<UploadAllotManpowerParameter> DownloadAllotManpower1(string CreatedBy)
        {
            List<UploadAllotManpowerParameter> XMLListRegistration = new List<UploadAllotManpowerParameter>();
            try
            {
                XMLListRegistration = Download.DownloadAllotManpower1(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadAllotManpowerParameter> UploadAllotManpowerNew3(Stream AllotManpowerDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AllotManpowerDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAllotManpowerNew3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        //08/03/2021 --shubhangi --
        public List<UploadPetrolReadingParameter> UploadPetrolReading(Stream PetrolReadingDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(PetrolReadingDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadPetrolReading(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<UploadPetrolReadingParameter> DownloadPetrolReading(string CreatedBy)
        {
            List<UploadPetrolReadingParameter> XMLListRegistration = new List<UploadPetrolReadingParameter>();
            try
            {
                XMLListRegistration = Download.DownloadPetrolReading(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //11/03/2021
        public List<MaterialIssuedParameter> UploadMaterialIssued(Stream MaterialDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(MaterialDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadMaterialIssued(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<MaterialIssuedParameter> DownloadMaterialIssued(string CreatedBy)
        {
            List<MaterialIssuedParameter> XMLListRegistration = new List<MaterialIssuedParameter>();
            try
            {
                XMLListRegistration = Download.DownloadMaterialIssued(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<DownloadDateWiseParameter> DownloadDateWise_StartDateToEnd(string LBDistrictID, string ElectionId)
        {
            List<DownloadDateWiseParameter> XMLListChapter = new List<DownloadDateWiseParameter>();
            try
            {
                XMLListChapter = Download.DownloadDateWise_StartDateToEnd(LBDistrictID, ElectionId);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }


        //16/03/2021
        public List<CustomerContactDetailsParameter> DownloadCustomerContactDetails(string CreatedBy)
        {
            List<CustomerContactDetailsParameter> XMLListRegistration = new List<CustomerContactDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerContactDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //22/03/2021

        public List<DownloadQuestionCategory> DownloadQuestionCategory1(string ShopType, string CreatedBy)
        {
            List<DownloadQuestionCategory> XMLListItem = new List<DownloadQuestionCategory>();
            try
            {
                XMLListItem = Download.DownloadQuestionCategory1(ShopType, CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //25/03/2021
        public List<WhatsappCountParameter> UploadWhatsAppCount(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCount(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //26/03/2021
        public List<WhatsAppTemplateParameter> UploadWhatsAppTemplateDetails(Stream WhatsAppTemplateDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsAppTemplateDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppTemplateDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<WhatsAppTemplateParameter> DownloadWhatsAppTemplateDetails(string CreatedBy)
        {
            List<WhatsAppTemplateParameter> XMLListItem = new List<WhatsAppTemplateParameter>();
            try
            {
                XMLListItem = Download.DownloadWhatsAppTemplateDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //01/04/2021
        public List<CustomerContactDetailsParameter> DownloadContactExcelHeading(string CreatedBy)
        {
            List<CustomerContactDetailsParameter> XMLListRegistration = new List<CustomerContactDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadContactExcelHeading(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //07/04/2021
        public List<CustomerContactDetailsParameter> DownloadCustomerContactDetails1(string CreatedBy, string FromSrno, string ToSrno)
        {
            List<CustomerContactDetailsParameter> XMLListRegistration = new List<CustomerContactDetailsParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadCustomerContactDetails1(CreatedBy, FromSrno, ToSrno);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //09/04/2021
        public List<CompanyDetailsParameter> UploadCompanyDetails(Stream CompanyDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CompanyDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCompanyDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CompanyDetailsParameter> DownloadCompanyDetails(string CreatedBy)
        {
            List<CompanyDetailsParameter> XMLListItem = new List<CompanyDetailsParameter>();
            try
            {
                XMLListItem = Download.DownloadCompanyDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<SocialLinksDetailsParameter> UploadSocialLinks(Stream SocialLinksDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(SocialLinksDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadSocialLinks(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SocialLinksDetailsParameter> DownloadSocialLinks(string CreatedBy)
        {
            List<SocialLinksDetailsParameter> XMLListItem = new List<SocialLinksDetailsParameter>();
            try
            {
                XMLListItem = Download.DownloadSocialLinks(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        //10/04/2021
        public List<PhoneContactsDetailsParameter> UploadPhoneContacts(Stream PhoneContactsDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(PhoneContactsDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadPhoneContacts(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //14/04/2021
        public List<WhatsappCountParameter> DownloadWhatsAppMsgCount(string CreatedBy)
        {
            List<WhatsappCountParameter> XMLListItem = new List<WhatsappCountParameter>();
            try
            {
                XMLListItem = Download.DownloadWhatsAppMsgCount(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //16/04/2021
        public List<PaymentOptionParameter> UploadPaymentOption(Stream PaymentOptionDetails)
        {
            List<PaymentOptionParameter> objEnquiry = new List<PaymentOptionParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(PaymentOptionDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadPaymentOption(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }
        //17/04/2021
        public List<PaymentOptionParameter> DownloadPaymentOption(string CreatedBy)
        {
            List<PaymentOptionParameter> XMLListItem = new List<PaymentOptionParameter>();
            try
            {
                XMLListItem = Download.DownloadPaymentOption(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        //20/04/2021
        public List<ProductServiceParameter> UploadProductServiceDetails(Stream ProductServiceDetails)
        {
            List<ProductServiceParameter> objEnquiry = new List<ProductServiceParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProductServiceDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadProductServiceDetails(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }
        public List<ProductServiceParameter> DownloadProductServiceDetails(string CreatedBy)
        {
            List<ProductServiceParameter> XMLListItem = new List<ProductServiceParameter>();
            try
            {
                XMLListItem = Download.DownloadProductServiceDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        // 24/04/2021
        public List<ImageGalleryParameter> UploadImageGalleryDetails(Stream ImageGalleryDetails)
        {
            List<ImageGalleryParameter> objEnquiry = new List<ImageGalleryParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ImageGalleryDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadImageGalleryDetails(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        //26/04/2021
        public List<ParameterGroupDetails> UploadGroupDetails(Stream GroupDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //27/04/2021
        public List<ParameterRuleDetails> UploadRuleDetails(Stream RuleDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(RuleDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadRuleDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //28/04/2021
        public List<ParameterKeywordDetails> UploadKeywordDetails(Stream KeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(KeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadKeywordDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //29/04/2021
        public List<ParameterRuleDetails> DownloadRuleDetails(string CreatedBy)
        {
            List<ParameterRuleDetails> XMLListItem = new List<ParameterRuleDetails>();
            try
            {
                XMLListItem = Download.DownloadRuleDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterKeywordDesription> UploadKeywordDesription(Stream KeywordDesription)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(KeywordDesription))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadKeywordDesription(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterKeywordDetails> DownloadKeywordDetails(string CreatedBy)
        {
            List<ParameterKeywordDetails> XMLListItem = new List<ParameterKeywordDetails>();
            try
            {
                XMLListItem = Download.DownloadKeywordDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //30/04/2021
        public List<ParameterKeywordDesription> DownloadKeywordDesription(string KeywordId)
        {
            List<ParameterKeywordDesription> XMLListItem = new List<ParameterKeywordDesription>();
            try
            {
                XMLListItem = Download.DownloadKeywordDesription(KeywordId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterGroupDetails> DownloadGroupDetails(string CreatedBy)
        {
            List<ParameterGroupDetails> XMLListItem = new List<ParameterGroupDetails>();
            try
            {
                XMLListItem = Download.DownloadGroupDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem;
        }

        public List<ParameterProjectDetails> UploadProjectDetails(Stream ProjectDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProjectDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProjectDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterProjectDetails> DownloadProjectDetails(string CreatedBy)
        {
            List<ParameterProjectDetails> XMLListItem = new List<ParameterProjectDetails>();
            try
            {
                XMLListItem = Download.DownloadProjectDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //01/05/2021
        public List<CustomerContactDetailsParameter> UploadContactDetails(Stream ContactDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ContactDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadContactDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterProjectMember> UploadProjectMember(Stream ProjectMemberDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProjectMemberDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProjectMember(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterProjectMember> DownloadProjectMember(string CreatedBy)
        {
            List<ParameterProjectMember> XMLListItem = new List<ParameterProjectMember>();
            try
            {
                XMLListItem = Download.DownloadProjectMember(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //03/05/2021
        public List<ParameterProjectDetails> DownloadProjectByProjectId(string MobileNo)
        {
            List<ParameterProjectDetails> XMLListItem = new List<ParameterProjectDetails>();
            try
            {
                XMLListItem = Download.DownloadProjectByProjectId(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

       
        //06/05/2021
        public List<ParameterAlarmDetails> UploadAlarmDetails(Stream AlarmDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AlarmDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAlarmDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterAlarmDetails> DownloadAlarmDetails(string CreatedBy)
        {
            List<ParameterAlarmDetails> XMLListItem = new List<ParameterAlarmDetails>();
            try
            {
                XMLListItem = Download.DownloadAlarmDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //12/05/2021

        public List<ParameterFormDetails> UploadFormDetails(Stream FormDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //14/05/2021
        public List<ParameterVaccinationDetails> DownloadVaccinationDetailsByMob(string MobileNo)
        {
            List<ParameterVaccinationDetails> XMLListItem = new List<ParameterVaccinationDetails>();
            try
            {
                XMLListItem = Download.DownloadVaccinationDetailsByMob(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //12/05/2021

        public List<ParameterFormMember> UploadFormMember(Stream FormMember)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormMember))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormMember(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //15/05/2021
        public List<ParameterVaccinationDetails> DownloadVaccinationDetails(string VaccinationCentre, string FromTokenNo, string ToTokenNo)
        {
            List<ParameterVaccinationDetails> XMLListRegistration = new List<ParameterVaccinationDetails>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadVaccinationDetails(VaccinationCentre, FromTokenNo, ToTokenNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //16/05/2021
        public List<ParameterFormKeyword> UploadFormKeyword(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeyword(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //17/05/2021
        public List<ParameterProjectMember> UploadProjectMemberNew(Stream ProjectMemberDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProjectMemberDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProjectMemberNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //18/05/2021
        public List<ParameterVaccinationDetails> UpdateVaccinationDateTimeSlot(Stream VaccinationDateTimeSlot)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(VaccinationDateTimeSlot))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UpdateVaccinationDateTimeSlot(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //18/05/2021
        public List<ParameterFormKeywordDesription> UploadFormKeywordDescription(Stream FormKeywordDescription)

        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDescription))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordDescription(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<ParameterFormDetails> DownloadFormDetails(string CreatedBy)
        {
            List<ParameterFormDetails> XMLListItem = new List<ParameterFormDetails>();
            try
            {
                XMLListItem = Download.DownloadFormDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        //19/05/2021
        public List<ParameterFormMember> DownloadFormMember(string CreatedBy)
        {
            List<ParameterFormMember> XMLListItem = new List<ParameterFormMember>();
            try
            {
                XMLListItem = Download.DownloadFormMember(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterFormKeyword> DownloadFormKeywords(string CreatedBy)
        {
            List<ParameterFormKeyword> XMLListItem = new List<ParameterFormKeyword>();
            try
            {
                XMLListItem = Download.DownloadFormKeywords(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterFormKeywordDesription> DownloadFormKeywordDescription(string KeywordId)
        {
            List<ParameterFormKeywordDesription> XMLListItem = new List<ParameterFormKeywordDesription>();
            try
            {
                XMLListItem = Download.DownloadFormKeywordDescription(KeywordId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //20/05/2021
        public List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCount()
        {
            List<ParameterVaccinationDetailsCount> XMLListItem = new List<ParameterVaccinationDetailsCount>();
            try
            {
                XMLListItem = Download.DownloadVaccinationDetailsCount();
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCountByDate(string Date)
        {
            List<ParameterVaccinationDetailsCount> XMLListItem = new List<ParameterVaccinationDetailsCount>();
            try
            {
                XMLListItem = Download.DownloadVaccinationDetailsCountByDate(Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }


        // 21/05/2021
        public List<CardLinkParameter> UploadCardLinkDetails(Stream CardLinkDetails)
        {
            List<CardLinkParameter> objEnquiry = new List<CardLinkParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CardLinkDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadCardLinkDetails(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }


        public List<ParameterFormDetails> DownloadFormDetailsByMemberNo(string MemberNo)
        {
            List<ParameterFormDetails> XMLListItem = new List<ParameterFormDetails>();
            try
            {
                XMLListItem = Download.DownloadFormDetailsByMemberNo(MemberNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<RegstrationRefParameter> UploadReferncesMobile(Stream ReferealCode)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReferealCode))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferncesMobile(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //25/05/2021

        public List<ParameterRegistrationShareUrl> DownloadregistrationShareURL(string ShareURL)
        {
            List<ParameterRegistrationShareUrl> XMLListItem = new List<ParameterRegistrationShareUrl>();
            try
            {
                XMLListItem = Download.DownloadregistrationShareURL(ShareURL);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterRegistrationShareUrl> DownloadregistrationByReferenceNo(string ReferenceMobile)
        {
            List<ParameterRegistrationShareUrl> XMLListItem = new List<ParameterRegistrationShareUrl>();
            try
            {
                XMLListItem = Download.DownloadregistrationByReferenceNo(ReferenceMobile);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        //27/05/2021
        public List<ParameterVClink> DownloadVaccinationlink(string District)
        {
            List<ParameterVClink> XMLListItem = new List<ParameterVClink>();
            try
            {
                XMLListItem = Download.DownloadVaccinationlink(District);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //public List<ParameterVaccinationCenter> DownloadVaccinationCenter(string District )
        //{
        //    List<ParameterVaccinationCenter> XMLListItem = new List<ParameterVaccinationCenter>();
        //    try
        //    {
        //        XMLListItem = Download.DownloadVaccinationCenter(District);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return XMLListItem.ToList();
        //}

        //28/05/2021
        public List<ParameterVaccinationItemName> DownloadVaccinationItemName(string ReportID, string FieldID)
        {
            List<ParameterVaccinationItemName> XMLListItem = new List<ParameterVaccinationItemName>();
            try
            {
                XMLListItem = Download.DownloadVaccinationItemName(ReportID, FieldID);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //30/05/2021
        public List<ParameterFormKeyword> UploadFormKeywordNew(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterFormKeyword> UploadFormKeywordNew1(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerContactDetailsParameter> DownloadCustomerContactDetailsByGroupId(string GroupId)
        {
            List<CustomerContactDetailsParameter> XMLListRegistration = new List<CustomerContactDetailsParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadCustomerContactDetailsByGroupId(GroupId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //02/06/2021
        public List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCountByReportId(String ReportId)
        {
            List<ParameterVaccinationDetailsCount> XMLListItem = new List<ParameterVaccinationDetailsCount>();
            try
            {
                XMLListItem = Download.DownloadVaccinationDetailsCountByReportId(ReportId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCountByDateAndReportId(string Date, string ReportId)
        {
            List<ParameterVaccinationDetailsCount> XMLListItem = new List<ParameterVaccinationDetailsCount>();
            try
            {
                XMLListItem = Download.DownloadVaccinationDetailsCountByDateAndReportId(Date,ReportId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterVaccinationDetails> DownloadVaccinationDetailsByCreatedBy(string CreatedBy)
        {
            List<ParameterVaccinationDetails> XMLListItem = new List<ParameterVaccinationDetails>();
            try
            {
                XMLListItem = Download.DownloadVaccinationDetailsByCreatedBy(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterFormKeywordDesription> UploadFormKeywordDescriptionNew(Stream FormKeywordDescription)

        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDescription))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordDescriptionNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //08/06/2021
        public List<NeedlyRegistrationDetail> DownloadNeedlyRegistrationIMEIDetails(string MobileNo, string IMEI)
        {
            List<NeedlyRegistrationDetail> XMLListItem = new List<NeedlyRegistrationDetail>();
            try
            {
                XMLListItem = Download.DownloadNeedlyRegistrationIMEIDetails(MobileNo,IMEI);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        //08/06/2021
        public List<ParameterFormKeyword> UploadFormKeywordNew2(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterGroupDetails> UploadGroupDetailsNew(Stream GroupDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //09/06/2021
        public List<ParameterFormKeywordDesription> UploadFormKeywordDescriptionNew1(Stream FormKeywordDescription)

        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDescription))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordDescriptionNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterGroupOfGroup> UploadGroupOfGroup(Stream GroupOfGroupDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupOfGroupDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupOfGroup(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterGroupOfGroup> DownloadGroupOfGroupByGroupId(string GroupId)
        {
            List<ParameterGroupOfGroup> XMLListItem = new List<ParameterGroupOfGroup>();
            try
            {
                XMLListItem = Download.DownloadGroupOfGroupByGroupId(GroupId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }
        //11/06//2021
        public List<ParameterKeywordDetails> UploadKeywordDetailsNew(Stream KeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(KeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadKeywordDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public List<ParameterVaccinationDetailsCount> DownloadVaccinationCreatedByCount()
        //{
        //    List<ParameterVaccinationDetailsCount> XMLListItem = new List<ParameterVaccinationDetailsCount>();
        //    try
        //    {
        //        XMLListItem = Download.DownloadVaccinationCreatedByCount();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return XMLListItem.ToList();
        //}

        public List<ParameterFormKeyword> UploadFormKeywordNew3(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //14/06/2021
        public List<ParameterFormKeywordDesription> UploadFormKeywordDescriptionNew2(Stream FormKeywordDescription)

        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDescription))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordDescriptionNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //15/06/2021
        public List<UploadReferalDatails> UploadReferalDetails2(Stream ReferalDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReferalDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferalDetails2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //16/06/2021
        //public List<ParameterGroupOfGroup> DownloadKeywordFieldName(string MobileNo, string CreatedBy, string keyword)
        //{
        //    List<ParameterGroupOfGroup> XMLListItem = new List<ParameterGroupOfGroup>();
        //    try
        //    {
        //        XMLListItem = Download.DownloadKeywordFieldName( MobileNo,  CreatedBy,  keyword);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return XMLListItem.ToList();
        //}

        public List<ParameterFormMember> UploadFormMemberNew(Stream FormMember)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormMember))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormMemberNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterProjectMember> UploadProjectMemberNew1(Stream ProjectMemberDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProjectMemberDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProjectMemberNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //18/06/2021
        public List<ParameterGroupDetails> UploadGroupDetailsNew1(Stream GroupDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //19/06/2021
        public List<ParameterFormKeyword> UploadFormKeywordNew4(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew4(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        //28/06/2021
        public List<ParameterChatDetails> UploadChatDetails(Stream ChatDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ChatDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadChatDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterChatDetails> DownloadChatDetails(string CreatedBy)
        {
            List<ParameterChatDetails> XMLListItem = new List<ParameterChatDetails>();
            try
            {
                XMLListItem = Download.DownloadChatDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterPhoene> DownloadPhone(string CreatedBy)
        {
            List<ParameterPhoene> XMLListItem = new List<ParameterPhoene>();
            try
            {
                XMLListItem = Download.DownloadPhone(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<UploadStaffDetails> UploadStaffDetailsNew6(Stream StaffDetails)
        {
            List<UploadStaffDetails> objInsertPara = new List<UploadStaffDetails>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(StaffDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UploadStaffDetails));
                objInsertPara = uploaddatanew.UploadStaffDetailsNew6(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }
        //
        public List<ParameterLoginDetails> DownloadLoginDetails(string UserName)
        {
            List<ParameterLoginDetails> XMLListItem = new List<ParameterLoginDetails>();
            try
            {
                XMLListItem = Download.DownloadLoginDetails(UserName);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //09/07/2021
        public List<ParameterAlarmDetails> UploadAlarmDetailsNew1(Stream AlarmDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AlarmDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAlarmDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterWhatsAppMessageDetails> UploadWhatsAppMessageDetails(string WhatsAppMessageDetails, Stream stream)
        {
            try
            {
                //using (StreamReader StreamXMLReader = new StreamReader(WhatsAppMessageDetails))
                //{
                //    XMLParser = StreamXMLReader.ReadToEnd();
                //}
                XMLParser = WhatsAppMessageDetails.Replace("\"", "'");
              
                return uploaddatanew.UploadWhatsAppMessageDetails(XMLParser, stream);


            }
            catch (Exception)
            {
                throw;
            }
        }

        private void UploadFile(object fileName, object stream)
        {
            throw new NotImplementedException();
        }

        //14/07/2021
        public List<CustomerContactDetailsParameterSrNoCount> DownloadCustomerContactDetailsSrNoCount(string CreatedBy)
        {
            List<CustomerContactDetailsParameterSrNoCount> XMLListRegistration = new List<CustomerContactDetailsParameterSrNoCount>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerContactDetailsSrNoCount(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CustomerContactDetailsParameter> DownloadCustomerContactDetailsByCategory(string CreatedBy, string Category)
        {
            List<CustomerContactDetailsParameter> XMLListRegistration = new List<CustomerContactDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerContactDetailsByCategory(CreatedBy, Category);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<ParameterDiscountDeduction> UploadDiscountDeduction(Stream DiscountDeduction)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(DiscountDeduction))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadDiscountDeduction(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterDiscountDeduction> DownloadDiscountDeduction(string UserMobileNo)
        {
            List<ParameterDiscountDeduction> XMLListRegistration = new List<ParameterDiscountDeduction>();
            try
            {
                XMLListRegistration = Download.DownloadDiscountDeduction(UserMobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //19/07/2021
        public List<UpdateAppointmentBooking> UpdateAppointmentBookingByType(Stream AppointmentBooking)
        {
            List<UpdateAppointmentBooking> objInsertPara = new List<UpdateAppointmentBooking>();

            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppointmentBooking))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializers = new DataContractJsonSerializer(typeof(UpdateAppointmentBooking));
                objInsertPara = uploaddatanew.UpdateAppointmentBookingByType(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
            return objInsertPara.ToList();
        }

        //21/07/2021
        public List<WhatsappCountParameter> UploadWhatsAppCountNew(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCountNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //23/07/2021
        public List<UsageStatisticsDateparameter> UploadUsageStatisticsDate(Stream UsageStatisticsDate)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UsageStatisticsDate))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadUsageStatisticsDate(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //23/07/2021
        public List<UsageStatisticsDateparameter> DownloadUsageStatisticseDate(string MobileNo)
        {
            List<UsageStatisticsDateparameter> XMLListRegistration = new List<UsageStatisticsDateparameter>();
            try
            {
                XMLListRegistration = Download.DownloadUsageStatisticseDate(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CustomerContactDetailsParameter> UpdateGroupDetails(Stream GroupDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UpdateGroupDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

       // 23/07/2021
        public List<MarketinReporteparameter> DownloadMarketinReport(string MobileNo, string ReeralCode)
        {
            List<MarketinReporteparameter> XMLListRegistration = new List<MarketinReporteparameter>();
            try
            {
                XMLListRegistration = Download.DownloadMarketinReport(MobileNo, ReeralCode);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //28/07/2021
        public List<TemplateCategoryDetailsParameter> UploadTemplateCategoryDetails(Stream TemplateCategoryDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(TemplateCategoryDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadTemplateCategoryDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<FormTemplateDetailsParameter> UploadFormTemplateDetails(Stream FormTemplateDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormTemplateDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormTemplateDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //29/07/2021
        public List<TemplateCategoryDetailsParameter> DownloadDownloadTemplateCategoryDetails()
        {
            List<TemplateCategoryDetailsParameter> XMLListRegistration = new List<TemplateCategoryDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadDownloadTemplateCategoryDetails();
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<FormTemplateDetailsParameter> DownloadDownloadFormTemplateDetails()
        {
            List<FormTemplateDetailsParameter> XMLListRegistration = new List<FormTemplateDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadDownloadFormTemplateDetails();
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<GoogleFormQuestionDetailsParameter> UploadGoogleFormQuestionDetails(Stream GoogleFormQuestionDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GoogleFormQuestionDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGoogleFormQuestionDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<GoogleFormQuestionDetailsParameter> DownloadGoogleFormQuestion(string TemplateId)
        {
            List<GoogleFormQuestionDetailsParameter> XMLListRegistration = new List<GoogleFormQuestionDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadGoogleFormQuestion(TemplateId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //04/07/2021
        public List<TemplateLoginDetailsParameter> DownloadTemplateLoginDetails(string MobileNo)

        {
            List<TemplateLoginDetailsParameter> XMLListRegistration = new List<TemplateLoginDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadTemplateLoginDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //04/08/2021
        public List<CustomerContactDetailsParameter> DownloadCustomerContactDetailsByGroupIdNew(string GroupId, string CreatedBy)
        {
            List<CustomerContactDetailsParameter> XMLListRegistration = new List<CustomerContactDetailsParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadCustomerContactDetailsByGroupIdNew(GroupId, CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //08/08/2021
        public List<Cashbookparameter> DownloadCashBookDetails(string MobileNo)

        {
            List<Cashbookparameter> XMLListRegistration = new List<Cashbookparameter>();
            try
            {
                XMLListRegistration = Download.DownloadCashBookDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //10/08/2021
        public List<UdhaarDetailsParameter> UploadUdhaarDetails(Stream UdhaarDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UdhaarDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadUdhaarDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UdhaarDetailsParameter> DownloadUdhaarDetails(string Reg_Id)

        {
            List<UdhaarDetailsParameter> XMLListRegistration = new List<UdhaarDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadUdhaarDetails(Reg_Id);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CallLogFormDetailsParameter> UploadCallLogFormDetails(Stream CallLogForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogFormDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallLogFormDetailsParameter> DownloadCallLogFormDetails(string CreatedBy)

        {
            List<CallLogFormDetailsParameter> XMLListRegistration = new List<CallLogFormDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCallLogFormDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //13/08/2021
        public List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetails(Stream FeedBackForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FeedBackForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFeedBackFormAswerDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<FeedBackFormNaswerParameter> DownloadFeedBackFormAnswerDetails(string CustomerNumber)

        {
            List<FeedBackFormNaswerParameter> XMLListRegistration = new List<FeedBackFormNaswerParameter>();
            try
            {
                XMLListRegistration = Download.DownloadFeedBackFormAnswerDetails(CustomerNumber);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<WatsappSendMessageParameter> SendMessage(string UserName, string Password, string FilepathURL, string receiverMobileNo, string message)

        {
            List<WatsappSendMessageParameter> XMLListRegistration = new List<WatsappSendMessageParameter>();
            try
            {
                XMLListRegistration = Download.SendMessage(UserName, Password, FilepathURL, receiverMobileNo, message);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UdhaarDetailsParameter> DownloadUdharByDate(string CreatedDateFrom, string CreatedDateTo)
        {
            List<UdhaarDetailsParameter> XMLListRegistration = new List<UdhaarDetailsParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadUdharByDate(CreatedDateFrom, CreatedDateTo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UdhaarDetailsParameter> DownloadUdharByCustomerName(string CustomerName, string CreatedDateFrom, string CreatedDateTo)
        {
            List<UdhaarDetailsParameter> XMLListRegistration = new List<UdhaarDetailsParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadUdharByCustomerName(CustomerName, CreatedDateFrom, CreatedDateTo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<ParameterFormKeyword> UploadFormKeywordNew5(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew5(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TemplateCategoryParameter> DownloadTemplateCategoty(string Day, string Month,string TemplateType)
        {
            List<TemplateCategoryParameter> XMLListRegistration = new List<TemplateCategoryParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadTemplateCategoty(Day, Month, TemplateType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadTemplateParameter> DownloadTemplateDetailsNew(string ShopType, string TemplateType)
        {
            List<UploadTemplateParameter> XMLListRegistration = new List<UploadTemplateParameter>();
            try
            {
                XMLListRegistration = Download.DownloadTemplateDetailsNew(ShopType, TemplateType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UdhaarDetailsParameter> DeleteUdhaar(string ServerId)
        {
            List<UdhaarDetailsParameter> XMLListRegistration = new List<UdhaarDetailsParameter>();
            try
            {
                XMLListRegistration = uploaddatanew.DeleteUdhaar(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<NeedlyRegiUpdateMobileSecurity> NeedlyAppUpdateMobileSecurity(Stream AppRegistration)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppRegistration))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.NeedlyAppUpdateMobileSecurity(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void UploadFile(string fileName, Stream stream)
        {
            string FilePath = Path.Combine
                   (HostingEnvironment.MapPath("~/UploadExcel"), fileName);

            int length = 0;
            using (FileStream writer = new FileStream(FilePath, FileMode.Create))
            {
                int readCount;
                var buffer = new byte[8192];
                while ((readCount = stream.Read(buffer, 0, buffer.Length)) != 0)
                {
                    writer.Write(buffer, 0, readCount);
                    length += readCount;
                }
            }
        }

        public List<ParameterPurchaseTemplate> UploadPurchasetemplate(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadPurchasetemplate(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<WhatsappCountParameter> UploadWhatsAppCountNew1(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCountNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ParameterPurchaseTemplate> DownloadPurchasetemplate(string TemplateId, string CreatedBy)
        {
            List<ParameterPurchaseTemplate> XMLListRegistration = new List<ParameterPurchaseTemplate>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadPurchasetemplate(TemplateId, CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CheckIsAppInstalledParameter> CheckIsAppInstalled(Stream CheckApp)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CheckApp))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.CheckIsAppInstalled(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<NeedlySpreadsheetParameter> UploadNeedlySpreadsheet(Stream NeedlySpreadsheet)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NeedlySpreadsheet))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadNeedlySpreadsheet(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<NeedlySpreadsheetParameter> DownloadNeedlySpreadsheet(string MobileNo, string Type)
        {
            List<NeedlySpreadsheetParameter> XMLListRegistration = new List<NeedlySpreadsheetParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadNeedlySpreadsheet(MobileNo, Type);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadReferalNotification> UploadReferalNotification(Stream Referal)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Referal))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferalNotification(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TimeEvaluationReportParameter> UploadTimeEvaluationReport(Stream Report)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Report))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.TimeEvaluationReport(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TimeEvaluationReportParameter> DownloadTimeEvaluationReport(string CreatedBy)
        {
            List<TimeEvaluationReportParameter> XMLListRegistration = new List<TimeEvaluationReportParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DownloadTimeEvaluationReport(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew(Stream FeedBackForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FeedBackForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFeedBackFormAswerDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DeleterContactDetailsParameter> DeleteCustomerContact(string CreatedBy, string FromSrno, string ToSrno)
        {
            List<DeleterContactDetailsParameter> XMLListRegistration = new List<DeleterContactDetailsParameter>();
            try
            {
                //int a = Convert.ToInt32(FromSrno);
                //int b = Convert.ToInt32(ToSrno);
                XMLListRegistration = Download.DeleteCustomerContact(CreatedBy, FromSrno, ToSrno);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<FeedBackFormNaswerParameter> DownloadFeedBackFormAnswerDetailsNew(string CustomerNumber, string CreatedBy, string MaxId)

        {
            List<FeedBackFormNaswerParameter> XMLListRegistration = new List<FeedBackFormNaswerParameter>();
            try
            {
                XMLListRegistration = Download.DownloadFeedBackFormAnswerDetailsNew(CustomerNumber, CreatedBy, MaxId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<KeywordExcelParameter> DownloadKeyWordExcelData(string MobileNo, string CreatedBy, string Keyword)

        {
            List<KeywordExcelParameter> XMLListRegistration = new List<KeywordExcelParameter>();
            try
            {
                XMLListRegistration = Download.DownloadKeyWordExcelData(MobileNo, CreatedBy, Keyword);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew(Stream CallLogForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogFormDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<InventoryItem> UploadInventoryItemNew2(Stream InventoryItem)
        {
            List<InventoryItem> objEnquiry = new List<InventoryItem>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(InventoryItem))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadInventoryItemNew2(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<NewQuotationBasicDetailsParaMeters> UploadNewQuotationDetails(Stream QuotationDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(QuotationDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadNewQuotationDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<FormSpreadsheetDataParameters> UploadNeedlyFormSpreadsheetData(Stream FormSpreadsheetData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormSpreadsheetData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadNeedlyFormSpreadsheetData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<NewQuotationBasicDetailsParaMeters> DownloadNewQuotationBasicDetails(string CreatedBy)
        {
            List<NewQuotationBasicDetailsParaMeters> XMLListItem = new List<NewQuotationBasicDetailsParaMeters>();
            try
            {
                XMLListItem = Download.DownloadNewQuotationBasicDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<CustomerWiseItemRateParameter> UploadCustomerWiseItemRate(Stream CustomerWiseItemRate)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CustomerWiseItemRate))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCustomerWiseItemRate(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LadiesSafetyHelplineParaMeters> DownloadLadiesSafetyHelpline(string State, string District, string Taluka)
        {
            List<LadiesSafetyHelplineParaMeters> XMLListItem = new List<LadiesSafetyHelplineParaMeters>();
            try
            {
                XMLListItem = Download.DownloadLadiesSafetyHelpline(State,  District, Taluka);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<RecordHeadingParameter> DownloadRecordHeadingData(string MarketingPersonNo)

        {
            List<RecordHeadingParameter> XMLListRegistration = new List<RecordHeadingParameter>();
            try
            {
                XMLListRegistration = Download.DownloadRecordHeadingData(MarketingPersonNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<PincodeDataParameter> DownloadPincodeData(string MarketingPersonNo, string Pincode, string FromId, string ToId)
        {
            List<PincodeDataParameter> XMLListItem = new List<PincodeDataParameter>();
            try
            {
                XMLListItem = Download.DownloadPincodeData(MarketingPersonNo, Pincode, FromId, ToId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<CustomerWiseItemRateParameter> DownloadCustomerWiseItemRate(string CreatedBy)

        {
            List<CustomerWiseItemRateParameter> XMLListRegistration = new List<CustomerWiseItemRateParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerWiseItemRate(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<InventoryCategoryDetails> UploadInventoryCategoryNew1(Stream CategoryDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CategoryDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadInventoryCategoryNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<EmergencyNumberParameter> UploadEmergencyNumber(Stream EmergencyNumber)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(EmergencyNumber))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadEmergencyNumber(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<EmergencyNumberParameter> DownloadEmergencyNumber(string CreatedBy)

        {
            List<EmergencyNumberParameter> XMLListRegistration = new List<EmergencyNumberParameter>();
            try
            {
                XMLListRegistration = Download.DownloadEmergencyNumber(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<LocationTrackingParameter> UploadLocationTracking(Stream EmergencyNumber)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(EmergencyNumber))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadLocationTracking(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LocationTrackingParameter> DownloadLocationTracking(string CreatedBy)

        {
            List<LocationTrackingParameter> XMLListRegistration = new List<LocationTrackingParameter>();
            try
            {
                XMLListRegistration = Download.DownloadLocationTracking(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<EmergencyNumberParameter> DownloadEmergencyNumberByNo(string Number)

        {
            List<EmergencyNumberParameter> XMLListRegistration = new List<EmergencyNumberParameter>();
            try
            {
                XMLListRegistration = Download.DownloadEmergencyNumberByNo(Number);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadReferalDatails> UploadReferalDetails3(Stream ReferalDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ReferalDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferalDetails3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EmergencyNumberParameter> UploadEmergencyNumberNew(Stream EmergencyNumber)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(EmergencyNumber))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadEmergencyNumberNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAddStaff> DownloadAddStaffByAddUnder(string CreatedBy, string AddUnder)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.DownloadAddStaffByAddUnder(CreatedBy, AddUnder);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<DownloadReferral> DownloadReferaltableDetailsNew(string Customer_Mobile)
        {
            List<DownloadReferral> XMLListChapter = new List<DownloadReferral>();
            try
            {
                XMLListChapter = Download.DownloadReferaltableDetailsNew(Customer_Mobile);
            }
            catch (Exception ex)
            {

            }
            return XMLListChapter.ToList();
        }

        public List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew1(Stream FeedBackForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FeedBackForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFeedBackFormAswerDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<ParameterGroupDetails> DeteteGroupDetails(Stream GroupDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.DeteteGroupDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<EmergencyNumberParameter> DeleteEmergencyNumber(Stream EmergencyNumber)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(EmergencyNumber))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.DeleteEmergencyNumber(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadAddStaff> DownloadAddStaffByNo(string MobileNo)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.DownloadAddStaffByNo(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadAddStaff1> DownloadAddStaffStatus(string CreatedBy, string MobileNo)
        {
            List<UploadAddStaff1> XMLListRegistration = new List<UploadAddStaff1>();
            try
            {
                XMLListRegistration = Download.DownloadAddStaffStatus(CreatedBy, MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<CallLogCountDetailsParameter> UploadCallLogCountDetails(Stream CallLogCount)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogCount))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogCountDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallLogCountDetailsParameter> DownloadCallLogCountDetails(string CreatedBy)
        {
            List<CallLogCountDetailsParameter> XMLListRegistration = new List<CallLogCountDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCallLogCountDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<AudioFileParameter> UploadAduiFileDetails(string AduiFile, Stream stream)
        {
            try
            {
                //using (StreamReader StreamXMLReader = new StreamReader(WhatsAppMessageDetails))
                //{
                //    XMLParser = StreamXMLReader.ReadToEnd();
                //}
                XMLParser = AduiFile.Replace("\"", "'");

                return uploaddatanew.UploadAduiFileDetails(XMLParser, stream);


            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AudioFileParameter> UploadAudioFileDetails(string AudioFile, Stream stream)
        {
            try
            {
                //using (StreamReader StreamXMLReader = new StreamReader(WhatsAppMessageDetails))
                //{
                //    XMLParser = StreamXMLReader.ReadToEnd();
                //}
                XMLParser = AudioFile.Replace("\"", "'");

                return uploaddatanew.UploadAudioFileDetails(XMLParser, stream);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<GenerateCouponCodeParameter> DownloadGenerateCouponDetails(string ProjectName, string LicenseType, string MobileNo)
        {
            List<GenerateCouponCodeParameter> XMLListRegistration = new List<GenerateCouponCodeParameter>();
            try
            {
                XMLListRegistration = Download.DownloadGenerateCouponDetails(ProjectName, LicenseType, MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        public List<CouponDistributedParameter> UploadCouponDistributeCode(Stream CouponDistributeCode)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CouponDistributeCode))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCouponDistributeCode(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TrueVoterUserParameter> UploadTrueVoterUserDetails(Stream UserDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(UserDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadTrueVoterUserDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew1(Stream CallLogForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogFormDetailsNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew2(Stream FeedBackForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FeedBackForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFeedBackFormAswerDetailsNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<WhatsappCountParameter> UploadWhatsAppCountNew2(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCountNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<WhatsappCountParameter> UploadWhatsAppCountNew3(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCountNew3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TemplateShareParameter> UploadTemplateShare(Stream TemplateShare)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(TemplateShare))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadTemplateShare(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TemplateShareParameter> DownloadTemplateShare(string CreatedBy)
        {
            List<TemplateShareParameter> XMLListRegistration = new List<TemplateShareParameter>();
            try
            {
                XMLListRegistration = Download.DownloadTemplateShare(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<WhatsappCountParameter> UploadWhatsAppCountNew4(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCountNew4(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CouponDistributionPara> UploadCouponDistributeCode1(Stream CouponDistributeCode)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CouponDistributeCode))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCouponDistributeCode1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DistributedCoupon> DownloadDistributedCoupon(string CreatedBy)
        {
            List<DistributedCoupon> XMLListRegistration = new List<DistributedCoupon>();
            try
            {
                XMLListRegistration = Download.DownloadDistributedCoupon(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CompanyDetailsParameter> UploadCompanyDetails1(Stream CompanyDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CompanyDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCompanyDetails1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<WebsiteAvilableParameter> CheckWebsiteAvailability(string WebsiteName)
        {
            List<WebsiteAvilableParameter> XMLListRegistration = new List<WebsiteAvilableParameter>();
            try
            {
                XMLListRegistration = Download.CheckWebsiteAvailability(WebsiteName);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //11/12/2021
        public List<CardLinkParameter> UploadCardLinkDetails1(Stream CardLinkDetails)
        {
            List<CardLinkParameter> objEnquiry = new List<CardLinkParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CardLinkDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadCardLinkDetails1(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        //14/12/2021
        public List<EmployeedataParameter> UploadEmployeedataDetails(Stream employee)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(employee))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadEmployeedataDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EmployeedataParameter> DownloadEmployeedatadetails(string role)
        {
            List<EmployeedataParameter> XMLListRegistration = new List<EmployeedataParameter>();
            try
            {
                XMLListRegistration = Download.DownloadEmployeedatadetails(role);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //20/12/2021
        public List<UploadTemplateParameter> DownloadTemplateDetailsNew1(string CategoryId, string TemplateType,string ShopType)
        {
            List<UploadTemplateParameter> XMLListRegistration = new List<UploadTemplateParameter>();
            try
            {
                XMLListRegistration = Download.DownloadTemplateDetailsNew1(CategoryId, TemplateType, ShopType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //23/12/2021
        public List<CallLogCountDetailsParameter> UploadCallLogCountDetailsNew(Stream CallLogCount)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogCount))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogCountDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //24/12/2021
        public List<CallLogCountDetailsParameter> DownloadCallLogCountDetailsNew(string CreatedBy, string Date)
        {
            List<CallLogCountDetailsParameter> XMLListRegistration = new List<CallLogCountDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCallLogCountDetailsNew(CreatedBy, Date);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //28/12/2021
        public List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew11(Stream CallLogForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogFormDetailsNew11(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<truevoterparameter> SendnotificationTrueVoter(Stream notification)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(notification))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.SendnotificationTrueVoter(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //30/12/2021
        public List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew3(Stream FeedBackForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FeedBackForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFeedBackFormAswerDetailsNew3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        
        //04/01/2022
        public List<GenerateAllocatecouponCodeParameter> DownloadAllocateCouponDetails(string ProjectName, string CoupanCodeType, string MobileNo, string UseType, string RechargePeriod)
        {
            List<GenerateAllocatecouponCodeParameter> XMLListRegistration = new List<GenerateAllocatecouponCodeParameter>();
            try
            {
                XMLListRegistration = Download.DownloadAllocateCouponDetails(ProjectName, CoupanCodeType, MobileNo, UseType, RechargePeriod);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<GenerateAllocatecouponCodeParameter> DownloadImeiNo(string MobileNo, string keyword)
        {
            List<GenerateAllocatecouponCodeParameter> XMLListRegistration = new List<GenerateAllocatecouponCodeParameter>();
            try
            {
                XMLListRegistration = Download.DownloadImeiNo(MobileNo, keyword);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<CouponAllocatePara> UploadCouponAllocateCode(Stream CouponAllocateCode)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CouponAllocateCode))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCouponAllocateCode(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //08/01/2022
        public List<RegstrationRefParameter> UploadReferncesMobile1(Stream RefernceMobile)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(RefernceMobile))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReferncesMobile1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }
        //12/01/2022
        public List<UploadRechargeWallet> DownloadRechargeWallet2(string coupencode, string Keyword)
        {
            List<UploadRechargeWallet> XMLListRegistration = new List<UploadRechargeWallet>();
            try
            {
                XMLListRegistration = Download.DownloadRechargeWallet2(coupencode, Keyword);
            }
            catch
            {

            }
            return XMLListRegistration.ToList();
        }

        //15/01/2022
        public List<WhatsappCountParameter> UploadWhatsAppCountNew5(Stream WhatsappCountDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(WhatsappCountDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWhatsAppCountNew5(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //24/01/22
        public List<ImageGalleryParameter> UploadNewsAndMedia(Stream NewsMedia)
        {
            List<ImageGalleryParameter> objEnquiry = new List<ImageGalleryParameter>();
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NewsMedia))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");
                DataContractJsonSerializer DataContractJsonSerializer = new DataContractJsonSerializer(typeof(InventoryItem));
                objEnquiry = uploaddatanew.UploadNewsAndMedia(XMLParser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objEnquiry.ToList();
        }

        public List<CompanyDetailsParameter2> UploadCompanyDetails2(Stream CompanyDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CompanyDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCompanyDetails2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //27/01/22
        public List<TemplateParameter> DownloadTemplateDetails1()
        {
            List<TemplateParameter> XMLListRegistration = new List<TemplateParameter>();
            try
            {
                XMLListRegistration = Download.DownloadTemplateDetails1();
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }
        //28/01/22
        public List<WebsiteDetails> UploadWebsiteDetails(Stream website)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(website))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadWebsiteDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<WebsiteDetails> DownloadWebsiteDetails(string CreatedBy)
        {
            List<WebsiteDetails> XMLListItem = new List<WebsiteDetails>();
            try
            {
                XMLListItem = Download.DownloadWebsiteDetails(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }


        //31/01/22
        public List<ImageGalleryParameter> DownloadNewsAndMedia(string CreatedBy)
        {
            List<ImageGalleryParameter> XMLListItem = new List<ImageGalleryParameter>();
            try
            {
                XMLListItem = Download.DownloadNewsAndMedia(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ImageGalleryParameter> DownloadImageGallery(string CreatedBy)
        {
            List<ImageGalleryParameter> XMLListItem = new List<ImageGalleryParameter>();
            try
            {
                XMLListItem = Download.DownloadImageGallery(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //03/02/2022
        public List<DeleteGroupofGroupDetailsparameter> DeleteGroupofGroupDetails(Stream ServerId)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ServerId))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.DeleteGroupofGroupDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //08/02/22
        public List<SocialLinksDetailsParameter> UploadSocialLinksNew(Stream SocialLinksDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(SocialLinksDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadSocialLinksNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //09/01/22
        public List<VisitCountParameter> DownloadVisitCount(string CreatedBy)
        {
            List<VisitCountParameter> XMLListItem = new List<VisitCountParameter>();
            try
            {
                XMLListItem = Download.DownloadVisitCount(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<ParameterProjectMember> UploadProjectMemberNew2(Stream ProjectMemberDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProjectMemberDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProjectMemberNew2(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //11/02/22
        public List<DownloadEmail> DownloadEmail(string mobileno, string keyword)
        {
            List<DownloadEmail> XMLListItem = new List<DownloadEmail>();
            try
            {
                XMLListItem = Download.DownloadEmail(mobileno, keyword);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<DownloadEnquiry> DownloadCustomerEnquiry(string CreatedBy)
        {
            List<DownloadEnquiry> XMLListItem = new List<DownloadEnquiry>();
            try
            {
                XMLListItem = Download.DownloadCustomerEnquiry(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<DownlaodCustomerFeedbackparameter> DownloadCustomerFeedback(string CreatedBy)
        {
            List<DownlaodCustomerFeedbackparameter> XMLListRegistration = new List<DownlaodCustomerFeedbackparameter>();
            try
            {
                XMLListRegistration = Download.DownloadCustomerFeedback(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //12/02/22
        public List<CustomerContactDetailsParameter> UploadContactDetailsNew(Stream ContactDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ContactDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadContactDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //15/02/2022
        public List<Reportdataparameter> UploadReportdata(Stream Reportdata)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Reportdata))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReportdata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Reportdataparameter> DownloadReportData(string CreatedBy)
        {
            List<Reportdataparameter> XMLListRegistration = new List<Reportdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadReportData(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //16/02/2022

        public List<fieldaliasdataparameter> Uploadfieldaliasdata(Stream fieldaliasdata)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(fieldaliasdata))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.Uploadfieldaliasdata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<fieldaliasdataparameter> Downloadfieldaliasdata(string CreatedBy, string FormId)
        {
            List<fieldaliasdataparameter> XMLListRegistration = new List<fieldaliasdataparameter>();
            try
            {
                XMLListRegistration = Download.Downloadfieldaliasdata(CreatedBy, FormId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //17/02/2022

        public List<formstructuredataparameter> UploadFormstructuredata(Stream fieldaliasdata)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(fieldaliasdata))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormstructuredata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<formstructuredataparameter> DownloadFormstructuredata(string reportid)
        {
            List<formstructuredataparameter> XMLListRegistration = new List<formstructuredataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadFormstructuredata(reportid);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //18/02/2022

        public List<VoterUpdateCounts> UploadVoterUpdateCounts(Stream VoterUpdateCounts)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(VoterUpdateCounts))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadVoterUpdateCounts(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Reportdataparameter> DownloadReportData1(string ProjectType)
        {
            List<Reportdataparameter> XMLListRegistration = new List<Reportdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadReportData1(ProjectType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }



        //21/02/22


        public List<VoterUpdateCounts> UploadVoterUpdateCounts1(Stream ProjectMemberDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProjectMemberDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadVoterUpdateCounts1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //22/02/2022

        public List<VoterUpdateCounts> DownloadVoterUpdateCounts(string candidateMobile)
        {
            List<VoterUpdateCounts> XMLListRegistration = new List<VoterUpdateCounts>();
            try
            {
                XMLListRegistration = Download.DownloadVoterUpdateCounts(candidateMobile);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //25/02/2022
        public List<Reportdataparameter> UploadReportdata1(Stream Reportdata)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Reportdata))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadReportdata1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Reportdataparameter> DownloadReportDataNew(string CreatedBy, string Category)
        {
            List<Reportdataparameter> XMLListRegistration = new List<Reportdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadReportDataNew(CreatedBy, Category);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<Reportdataparameter> DownloadReportDataNew1(string ProjectType, string Category)
        {
            List<Reportdataparameter> XMLListRegistration = new List<Reportdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadReportDataNew1(ProjectType, Category);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //03/03/2022

        public List<Grouppostparameter> UploadGroupPostData(Stream GroupPost)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupPost))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupPostData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Grouppostparameter> DownloadGroupPostData(string GroupId)
        {
            List<Grouppostparameter> XMLListRegistration = new List<Grouppostparameter>();
            try
            {
                XMLListRegistration = Download.DownloadGroupPostData(GroupId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<Groupchatparameter> UploadGroupChatData(Stream GroupChat)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupChat))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupChatData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Groupchatparameter> DownloadGroupChatData(string GroupId)
        {
            List<Groupchatparameter> XMLListRegistration = new List<Groupchatparameter>();
            try
            {
                XMLListRegistration = Download.DownloadGroupChatData(GroupId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //08/03/2022

        public List<ParameterFormKeyword> UploadFormKeywordNew55(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew55(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //14/03/2022

        public List<Grouppostparameter> DownloadGroupPostDataNew(string GroupId, string ApproveStatus)
        {
            List<Grouppostparameter> XMLListRegistration = new List<Grouppostparameter>();
            try
            {
                XMLListRegistration = Download.DownloadGroupPostDataNew(GroupId, ApproveStatus);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<CompanyDetailsParameter2> UploadCompanyDetails3(Stream CompanyDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CompanyDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCompanyDetails3(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Grouppostparameter1> UploadGroupPostDataNew(Stream GroupPost)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupPost))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupPostDataNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //17/03/2022

        public List<ParameterFormKeyword> UploadFormKeywordNew555(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew555(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //23/03/2022

        public List<Grouppostparameter> UploadGroupPostDataNew1(Stream GroupPost)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(GroupPost))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadGroupPostDataNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //28/03/2022

        public List<ParameterFormKeyword> DownloadFormKeywordsNew(string ServerId)
        {
            List<ParameterFormKeyword> XMLListItem = new List<ParameterFormKeyword>();
            try
            {
                XMLListItem = Download.DownloadFormKeywordsNew(ServerId);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<DynamicReportparameter> UploadDynamicReportdata(Stream Categoryname)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(Categoryname))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadDynamicReportdata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DynamicReportparameter> DownloadDynamicReportdata()
        {
            List<DynamicReportparameter> XMLListItem = new List<DynamicReportparameter>();
            try
            {
                XMLListItem = Download.DownloadDynamicReportdata();
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //04/04/2022

        public List<Grouppostparameter> DownloadGroupPostDataNew1(string GroupId, string MaxId)
        {
            List<Grouppostparameter> XMLListRegistration = new List<Grouppostparameter>();
            try
            {
                XMLListRegistration = Download.DownloadGroupPostDataNew1(GroupId, MaxId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<Groupchatparameter> DownloadGroupChatDataNew(string GroupId, string MaxId)
        {
            List<Groupchatparameter> XMLListRegistration = new List<Groupchatparameter>();
            try
            {
                XMLListRegistration = Download.DownloadGroupChatDataNew(GroupId, MaxId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<Grouppostparameter> DownloadGroupPostDataNew11(string GroupId, string ApproveStatus, string MaxId)
        {
            List<Grouppostparameter> XMLListRegistration = new List<Grouppostparameter>();
            try
            {
                XMLListRegistration = Download.DownloadGroupPostDataNew11(GroupId, ApproveStatus, MaxId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //09/04/2022

        public List<NeedlySOSPromotionparameter> UploadNeedlySOSPromotionData(Stream NeedlySOSPromotion)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(NeedlySOSPromotion))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadNeedlySOSPromotionData(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<NeedlySOSPromotionparameter> DownloadNeedlySOSPromotionData(string DynamicURL)
        {
            List<NeedlySOSPromotionparameter> XMLListRegistration = new List<NeedlySOSPromotionparameter>();
            try
            {
                XMLListRegistration = Download.DownloadNeedlySOSPromotionData(DynamicURL);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<NeedlySOSPromotionparameter> DownloadNeedlySOSPromotionDataNew(string Mobilenumber)
        {
            List<NeedlySOSPromotionparameter> XMLListRegistration = new List<NeedlySOSPromotionparameter>();
            try
            {
                XMLListRegistration = Download.DownloadNeedlySOSPromotionDataNew(Mobilenumber);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<EductionDetails> UploadEductionalDetails(Stream eductionaldetail)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(eductionaldetail))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadEductionalDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //14/04/22
        public List<DownloadEductionalDetails> DownloadEductionalDetails(string MobileNo)
        {
            List<DownloadEductionalDetails> XMLListItem = new List<DownloadEductionalDetails>();
            try
            {
                XMLListItem = Download.DownloadEductionalDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //14/04/2022

        public List<JobRegistrationParameter> UploadJobRegistration(Stream JobRegistration)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(JobRegistration))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadJobRegistration(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<JobRegistrationParameter> DownloadJobRegistration(string CreatedBy)
        {
            List<JobRegistrationParameter> XMLListRegistration = new List<JobRegistrationParameter>();
            try
            {
                XMLListRegistration = Download.DownloadJobRegistration(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<SelfEmploymentBusinessdataparameter> UploadSelfEmploymentBusiness(Stream JobRegistration)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(JobRegistration))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadSelfEmploymentBusiness(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SelfEmploymentBusinessdataparameter> DownloadSelfEmploymentBusiness(string CreatedBy)
        {
            List<SelfEmploymentBusinessdataparameter> XMLListRegistration = new List<SelfEmploymentBusinessdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadSelfEmploymentBusiness(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<AgricultureRegistrationdataparameter> UploadAgricultureRegistrationdata(Stream JobRegistration)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(JobRegistration))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadAgricultureRegistrationdata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AgricultureRegistrationdataparameter> DownloadAgricultureRegistrationdata(string CreatedBy)
        {
            List<AgricultureRegistrationdataparameter> XMLListRegistration = new List<AgricultureRegistrationdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadAgricultureRegistrationdata(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew5(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew5(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<workdetailparameter> Uploadworkdetaildata(Stream workdetail)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(workdetail))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.Uploadworkdetaildata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<workdetailparameter> Downloadworkdetaildata(string USERMOBILE)
        {
            List<workdetailparameter> XMLListRegistration = new List<workdetailparameter>();
            try
            {
                XMLListRegistration = Download.Downloadworkdetaildata(USERMOBILE);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<SectorWiseSchme> UploadSectorWiseScheme(Stream sectorwise)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(sectorwise))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadSectorWiseScheme(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<DownloadSector> DownloadSectorWiseScheme(string MobileNo)
        {
            List<DownloadSector> XMLListItem = new List<DownloadSector>();
            try
            {
                XMLListItem = Download.DownloadSectorWiseScheme(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }


        //19/04/22
        public List<DemandJobParameter> UploadDemandJob(Stream demanjob)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(demanjob))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadDemandJob(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }



        public List<DemandJobParameter> DownloadDemandjob(string MobileNo)
        {
            List<DemandJobParameter> XMLListItem = new List<DemandJobParameter>();
            try
            {
                XMLListItem = Download.DownloadDemandjob(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //19/04/2022

        public List<JobCategoryparameter> DownloadJobCategorydata()
        {
            List<JobCategoryparameter> XMLListRegistration = new List<JobCategoryparameter>();
            try
            {
                XMLListRegistration = Download.DownloadJobCategorydata();
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<jobtypeparameter> Downloadjobtypedata(string CategoryId)
        {
            List<jobtypeparameter> XMLListRegistration = new List<jobtypeparameter>();
            try
            {
                XMLListRegistration = Download.Downloadjobtypedata(CategoryId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }


        public List<DemandJobParameter> DownloadRegularjob(string type, string job_type, string category, string state, string district, string taluka)
        {
            List<DemandJobParameter> XMLListItem = new List<DemandJobParameter>();
            try
            {
                XMLListItem = Download.DownloadRegularjob(type, job_type, category, state, district, taluka);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }


        public List<DemandJobParameter> Downloadsectorjob(string type, string sector, string category, string state, string district, string taluka, string jobrole)
        {
            List<DemandJobParameter> XMLListItem = new List<DemandJobParameter>();
            try
            {
                XMLListItem = Download.Downloadsectorjob(type, sector, category, state, district, taluka, jobrole);
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        //25/04/2022

        public List<assigntaskdataparameter> Uploadassigntaskdata(Stream assigntask)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(assigntask))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.Uploadassigntaskdata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<assigntaskdataparameter> Downloadassigntaskdata()
        {
            List<assigntaskdataparameter> XMLListRegistration = new List<assigntaskdataparameter>();
            try
            {
                XMLListRegistration = Download.Downloadassigntaskdata();
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<assigntaskdataparameter> DownloadassigntaskdataNew(String OfficeName, string DepartmentName, string StaffName, string FromDate, string ToDate, string Status)
        {
            List<assigntaskdataparameter> XMLListRegistration = new List<assigntaskdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadassigntaskdataNew(OfficeName, DepartmentName, StaffName, FromDate, ToDate, Status);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //26/04/2022

        public List<UploadEzeeClassProfile> UploadEzeeClassProfile(Stream EzeeClassProfile)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(EzeeClassProfile))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadEzeeClassProfile(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadEzeeClassProfile> DownloadEzeeClassProfile(string MobileNo)
        {
            List<UploadEzeeClassProfile> XMLListRegistration = new List<UploadEzeeClassProfile>();
            try
            {
                XMLListRegistration = Download.DownloadEzeeClassProfile(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //27/04/2022

        public List<assigntaskdataparameter> DownloadassigntaskdataNew1(string CreatedBy)
        {
            List<assigntaskdataparameter> XMLListRegistration = new List<assigntaskdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadassigntaskdataNew1(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //29/04/2022

        public List<assigntaskdataparameter> UploadassigntaskdataNew(Stream assigntask)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(assigntask))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadassigntaskdataNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }


        //30/04/2022

        public List<LadiesSafetyHelplineParaMeters> DownloadLadiesSafetyHelplineNew()
        {
            List<LadiesSafetyHelplineParaMeters> XMLListItem = new List<LadiesSafetyHelplineParaMeters>();
            try
            {
                XMLListItem = Download.DownloadLadiesSafetyHelplineNew();
            }
            catch (Exception ex)
            {

            }
            return XMLListItem.ToList();
        }

        public List<assigntaskdataparameter> DownloadassigntaskdataNew11(string FromDate, string ToDate, string TaskStatus,string CreatedBy)
        {
            List<assigntaskdataparameter> XMLListRegistration = new List<assigntaskdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadassigntaskdataNew11(FromDate, ToDate, TaskStatus,CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //02/05/2022

        public List<UploadAddStaff> DownloadAddStaffDetails(string MobileNo)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.DownloadAddStaffDetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //04/05/2022

        public List<assigntaskdataparameter> DownloadassigntaskdataonTaskCategory(String TaskCategory)
        {
            List<assigntaskdataparameter> XMLListRegistration = new List<assigntaskdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadassigntaskdataonTaskCategory(TaskCategory);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //06/05/2022

        public List<assigntaskdataparameter> DownloadassigntaskdataonBasis(String TaskCategory, string OfficeName, string DepartmentName, string StaffName, string PostName,string MaxId)
        {
            List<assigntaskdataparameter> XMLListRegistration = new List<assigntaskdataparameter>();
            try
            {
                XMLListRegistration = Download.DownloadassigntaskdataonBasis(TaskCategory, OfficeName, DepartmentName, StaffName, PostName,MaxId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //09/05/2022

        public List<UploadCreateGroup> UploadCreateGroupNew1(Stream CreateGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CreateGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCreateGroupNew1(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<NeedlyRegistrationDetail> UploadProfileupdateDataNew6(Stream ProfileData)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ProfileData))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadProfileupdateDataNew6(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //10/05/2022

        public List<UploadSendOTPParameter> DownloadJsonSendOtpNew(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string UserType)
        {
            List<UploadSendOTPParameter> XMLDownloadSendOtp = new List<UploadSendOTPParameter>();

            try
            {
                XMLDownloadSendOtp = Download.SendOtpMethodNew(UserMobile, RefMobile, IMEINumber, AppKeyword, UserType);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }

        public List<UploadCreateGroup> DownloadGrpdetails(String MobileNo)
        {
            List<UploadCreateGroup> XMLListRegistration = new List<UploadCreateGroup>();
            try
            {
                XMLListRegistration = Download.DownloadGrpdetails(MobileNo);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //13/05/2022

        public List<NeedlyRegistrationDetail> NeedlyAppRegistrationNew(Stream AppRegistration)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(AppRegistration))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.NeedlyAppRegistrationNew(XMLParser);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<UploadCreateGroup> UploadCreateGroupNew11(Stream CreateGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CreateGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCreateGroupNew11(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //19/05/2022

        public List<notificationreportparameter> Uploadnotificationreportdata(Stream notificationreport)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(notificationreport))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.Uploadnotificationreportdata(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<notificationreportparameter> Downloadnotificationreportdata(string CreatedBy)
        {
            List<notificationreportparameter> XMLListRegistration = new List<notificationreportparameter>();
            try
            {
                XMLListRegistration = Download.Downloadnotificationreportdata(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //24/05/2022

        public List<UploadAddStaff> DownloadAddStaffByNoNew(string MobileNo, string AddUnder)
        {
            List<UploadAddStaff> XMLListRegistration = new List<UploadAddStaff>();
            try
            {
                XMLListRegistration = Download.DownloadAddStaffByNoNew(MobileNo, AddUnder);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //27/05/2022

        public List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew111(Stream CallLogForm)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CallLogForm))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCallLogFormDetailsNew111(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallLogFormDetailsParameter> DownloadCallLogFormDetailsNew(string Groupid)

        {
            List<CallLogFormDetailsParameter> XMLListRegistration = new List<CallLogFormDetailsParameter>();
            try
            {
                XMLListRegistration = Download.DownloadCallLogFormDetailsNew(Groupid);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<MemberExtraQuestionAnswerParameter> UploadMemberExtraQuestionAnswer(Stream MemberQuestions)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(MemberQuestions))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadMemberExtraQuestionAnswer(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //08/06/2022

        public List<UploadCreateGroup> UploadCreateGroupNew111(Stream CreateGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(CreateGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCreateGroupNew111(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //09/06/2022

        public List<UploadMemberGroup> UploadMemberGroupNew(Stream MemberGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(MemberGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadMemberGroupNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //16/06/2022

        public List<UploadSendOTPParameter> DownloadJsonReSendOtp(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string UserType)
        {
            List<UploadSendOTPParameter> XMLDownloadSendOtp = new List<UploadSendOTPParameter>();

            try
            {
                XMLDownloadSendOtp = Download.ReSendOtpMethod(UserMobile, RefMobile, IMEINumber, AppKeyword, UserType);

            }
            catch (Exception ex)
            {
                throw;
            }
            return XMLDownloadSendOtp.ToList();
        }

        //17/06/2022

        public List<UploadCalllogHistoryparameter> UploadCalllogHistory(Stream MemberGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(MemberGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadCalllogHistory(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //29/06/2022

        public List<UploadJobApplicationDetailsParameter> UploadJobApplicationDetailsNew(Stream JobAppDataDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(JobAppDataDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadJobApplicationDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //19/07/2022

        public List<Uploadkeyworddetails> DownloadKeywordExcelUploadHeading(string MobileNo, string Category)
        {
            List<Uploadkeyworddetails> XMLListRegistration = new List<Uploadkeyworddetails>();
            try
            {
                XMLListRegistration = Download.DownloadKeywordExcelUploadHeading(MobileNo, Category);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //22/07/2022

        public List<MemberExtraQuestionAnswerParameter> DownloadQuestionAnswer(string GroupId, string CreatedBy, string QuestionType)
        {
            List<MemberExtraQuestionAnswerParameter> XMLListRegistration = new List<MemberExtraQuestionAnswerParameter>();
            try
            {
                XMLListRegistration = Download.DownloadQuestionAnswer(GroupId, CreatedBy, QuestionType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //23/07/2022

        public List<MemberExtraQuestionAnswerParameter> DownloadQuestionAnsweronthebasiscustomer(string GroupId, string CustomerNo, string QuestionType)
        {
            List<MemberExtraQuestionAnswerParameter> XMLListRegistration = new List<MemberExtraQuestionAnswerParameter>();
            try
            {
                XMLListRegistration = Download.DownloadQuestionAnsweronthebasiscustomer(GroupId, CustomerNo, QuestionType);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //26/07/2022

        public List<UploadExpenseGroupParameter> UploadExpenseGroup(Stream ExpenseGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseGroup(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //27/07/2022

        public List<UploadExpenseGroupParameter> UploadExpenseHead(Stream ExpenseHead)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseHead))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseHead(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadExpenseGroupParameter> UploadExpenseSubHead(Stream ExpenseSubHead)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseSubHead))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseSubHead(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadExpenseGroupParameter> DownloadExpenseDetails()
        {
            List<UploadExpenseGroupParameter> XMLListRegistration = new List<UploadExpenseGroupParameter>();
            try
            {
                XMLListRegistration = Download.DownloadExpenseDetails();
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //28/07/2022

        public List<FormSpreadsheetDataParameters> DownloadNeedlyFormSpreadsheetData(string FormId)
        {
            List<FormSpreadsheetDataParameters> XMLListRegistration = new List<FormSpreadsheetDataParameters>();
            try
            {
                XMLListRegistration = Download.DownloadNeedlyFormSpreadsheetData(FormId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //29/07/2022

        public List<UploadExpenseGroupParameter> DownloadExpenseHeadCount(string GroupName, string CreatedBy)
        {
            List<UploadExpenseGroupParameter> XMLListRegistration = new List<UploadExpenseGroupParameter>();
            try
            {
                XMLListRegistration = Download.DownloadExpenseHeadCount(GroupName, CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadExpenseGroupParameter> UpdateExpenseDetails(Stream ExpenseSubHead)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseSubHead))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UpdateExpenseDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadExpenseGroupParameter> DownloadExpenseAmountCount(string GroupName, string Category, string HeadName)
        {
            List<UploadExpenseGroupParameter> XMLListRegistration = new List<UploadExpenseGroupParameter>();
            try
            {
                XMLListRegistration = Download.DownloadExpenseAmountCount(GroupName, Category, HeadName);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //01/08/2022

        public List<ParameterFormKeyword> UploadFormKeywordNew5555(Stream FormKeywordDetails)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(FormKeywordDetails))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadFormKeywordNew5555(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //02/08/2022

        public List<FormSpreadsheetDataParameters> DownloadNeedlyFormSpreadsheetDataWithColumn(string FormId, string ColumnName, string ColumnValue)
        {
            List<FormSpreadsheetDataParameters> XMLListRegistration = new List<FormSpreadsheetDataParameters>();
            try
            {
                XMLListRegistration = Download.DownloadNeedlyFormSpreadsheetDataWithColumn(FormId, ColumnName, ColumnValue);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //06/08/2022

        public List<UploadExpenseGroupParameter> DownloadExpenseGroup(string CreatedBy)
        {
            List<UploadExpenseGroupParameter> XMLListRegistration = new List<UploadExpenseGroupParameter>();
            try
            {
                XMLListRegistration = Download.DownloadExpenseGroup(CreatedBy);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadExpenseGroupParameter> DownloadExpenseHead(string CreatedBy, string ParentId)
        {
            List<UploadExpenseGroupParameter> XMLListRegistration = new List<UploadExpenseGroupParameter>();
            try
            {
                XMLListRegistration = Download.DownloadExpenseHead(CreatedBy, ParentId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        public List<UploadExpenseGroupParameter> DownloadExpenseSubHead(string CreatedBy, string ParentId)
        {
            List<UploadExpenseGroupParameter> XMLListRegistration = new List<UploadExpenseGroupParameter>();
            try
            {
                XMLListRegistration = Download.DownloadExpenseSubHead(CreatedBy, ParentId);
            }
            catch (Exception ex)
            {

            }
            return XMLListRegistration.ToList();
        }

        //05/09/2022

        public List<UploadExpenseGroupParameter> UploadExpenseGroupDetails(Stream ExpenseGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseGroupDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadExpenseGroupParameter> UploadExpenseHeadDetails(Stream ExpenseHead)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseHead))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseHeadDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UploadExpenseGroupParameter> UploadExpenseSubHeadDetails(Stream ExpenseSubHead)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseSubHead))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseSubHeadDetails(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //12/09/2022

        public List<UploadExpenseGroupParameter> UploadExpenseGroupDetailsNew(Stream ExpenseGroup)
        {
            try
            {
                using (StreamReader StreamXMLReader = new StreamReader(ExpenseGroup))
                {
                    XMLParser = StreamXMLReader.ReadToEnd();
                }
                XMLParser = XMLParser.Replace("\"", "'");

                return uploaddatanew.UploadExpenseGroupDetailsNew(XMLParser);
            }
            catch (Exception)
            {
                throw;
            }
        }











    }
}
