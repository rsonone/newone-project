﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BAL;

namespace NeedlyApp.Web_Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INeedlyService" in both code and config file together.
    [ServiceContract]
    public interface INeedlyService
    {
        //[OperationContract]
        //[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "NeedlyAppRegistration/AppRegistration")]
        //List<NeedlyRegistrationDetail> NeedlyAppRegistration(Stream AppRegistration);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "NeedlyAppRegistration/AppRegistration")]
        List<NeedlyRegistrationDetail> NeedlyAppRegistration(Stream AppRegistration);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadShopListWiseResult?longitude={longitude}&latitude={latitude}&District={District}&Taluka={Taluka}")]
        List<NeedlyRegistrationDetail> DownloadShopListWiseResult(string longitude, string latitude, string District, string Taluka);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOrder/Order")]
        List<UploadOrder> UploadOrder(Stream Order);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadItem?OrderId={OrderId}")]
        List<UploadOrder> DownloadItem(string OrderId);

        //[OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadOrder?Mobile_No={Mobile_No}")]
        //List<DownloadOrder> DownloadOrder(string Mobile_No);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadOrder?Mobile_No={Mobile_No}&UserRole={UserRole}")]
        List<UploadOrder> DownloadOrder(string Mobile_No, string UserRole);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateData/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateData(Stream ProfileData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendNotification?Mobile_No={Mobile_No}&OrderId={OrderId}&StatusType={StatusType}")]
        List<SendNotification> SendNotification(string Mobile_No, string OrderId, string StatusType);


        [OperationContract]
       
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadBannerDetails?State={State}&District={District}&Taluka={Taluka}&WardNo={WardNo}&Bannertype={Bannertype}")]
        List<DownloadBanner> DownloadBannerDetails(string State, string District, string Taluka, string WardNo, string BannerType);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadKYCDetails/KYCDetails")]
        List<UploadKYCDetails> UploadKYCDetails(Stream KYCDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFcmRegDetailsNeedly/FcmRegDetailsData")]
        List<FcmRegDetail> UploadFcmRegDetailsNeedly(Stream FcmRegDetailsData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJsonSendOtp?UserMobile={UserMobile}&RefMobile={RefMobile}&IMEINumber={IMEINumber}&AppKeyword={AppKeyword}&UserType={UserType}")]
        List<UploadSendOTPParameter> DownloadJsonSendOtp(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string UserType);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJsonVerifyOTP?UserMobile={UserMobile}&RefMobile={RefMobile}&IMEINumber={IMEINumber}&AppKeyword={AppKeyword}&OTPValue={OTPValue}")]
        List<UploadSendOTPParameter> DownloadJsonVerifyOTP(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string OTPValue);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOpenApp/UploadData")]
        List<UploadOpenAppParameter> UploadOpenApp(Stream UploadData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReportResultMultipleShivbhojan/UploadReportResultData")]
        List<ReportResultParameter> UploadReportResultMultipleShivbhojan(Stream UploadReportResultData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryCategory/CategoryDetails")]
        List<InventoryCategoryDetails> UploadInventoryCategory(Stream CategoryDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryComboDetails/InventoryComboDetails")]
        List<InventoryComboDetails> UploadInventoryComboDetails(Stream InventoryComboDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryItem/InventoryItem")]
        List<InventoryItem> UploadInventoryItem(Stream InventoryItem);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadInventoryItem?Reg_Id={Reg_Id}&MobileNo={MobileNo}")]
        List<DownloadInventoryItem> DownloadInventoryItem(string Reg_Id, string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadInventoryComboDetails?Mobile_No={Mobile_No}&Reg_Id={Reg_Id}")]
        List<InventoryComboDetails> DownloadInventoryComboDetails(string Mobile_No, string Reg_Id);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadInventoryCategoryDetails?Mobile_No={Mobile_No}&Reg_Id={Reg_Id}")]
        List<InventoryCategoryDetails> DownloadInventoryCategoryDetails(string Mobile_No, string Reg_Id);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppUpdate/AppVersion")]
        List<AppUpdateParameter> UploadAppUpdate(Stream AppVersion);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadInventoryCategoryDetailsNew?Mobile_No={Mobile_No}&Reg_Id={Reg_Id}")]
        List<InventoryCategoryDetails> DownloadInventoryCategoryDetailsNew(string Mobile_No, string Reg_Id);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOrderNew/Order")]
        List<UploadOrder> UploadOrderNew(Stream Order);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReviewDetails/ReviewDetails")]
        List<UploadReviewDetails> UploadReviewDetails(Stream ReviewDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadOrderNew?Mobile_No={Mobile_No}&UserRole={UserRole}")]
        List<UploadOrder> DownloadOrderNew(string Mobile_No, string UserRole);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadShopListWiseResultNew?longitude={longitude}&latitude={latitude}&District={District}&Taluka={Taluka}&Id={Id}")]
        List<NeedlyRegistrationDetail> DownloadShopListWiseResultNew(string longitude, string latitude, string District, string Taluka,string Id);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDashbordCount?Mobile_No={Mobile_No}")]
        List<DownloadDashobordCount> DownloadDashbordCount(string Mobile_No);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlyRegistrationDetail?Mobile_No={Mobile_No}")]
        List<NeedlyRegistrationDetail> DownloadNeedlyRegistrationDetail(string Mobile_No);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferalDetails/ReferalDetails")]
        List<UploadReferalDatails> UploadReferalDetails(Stream ReferalDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNotifactionData/NotifactionData")]
        List<UploadNotifiction> UploadNotifactionData(Stream NotifactionData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReferaltableDetails?MobileNo={MobileNo}")]
        List<DownloadReferral> DownloadReferaltableDetails(string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadKYCDetails?MobileNo={MobileNo}")]
        List<DownloadKYCDetails> DownloadKYCDetails(string MobileNo);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew(Stream ProfileData);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOrderNewDetails/Order")]
        List<UploadOrder> UploadOrderNewDetails(Stream Order);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendNotificationNew?Mobile_No={Mobile_No}&OrderId={OrderId}&StatusType={StatusType}&IsUpdate={IsUpdate}&DeliveryCharges={DeliveryCharges}")]
        List<SendNotification> SendNotificationNew(string Mobile_No, string OrderId, string StatusType,string IsUpdate,string DeliveryCharges);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadInventoryitem_Master?ShopType={ShopType}")]
        List<Inventoryitem_Master> DownloadInventoryitem_Master(string ShopType);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryItemNew/InventoryItem")]
        List<InventoryItem> UploadInventoryItemNew(Stream InventoryItem);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCategoryWiseShops?AppKeyword={AppKeyword}")]
        List<DownloadCategoryWiseShops> DownloadCategoryWiseShops(string AppKeyword);



        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadQuestionCategory?ShopType={ShopType}")]
        List<DownloadQuestionCategory> DownloadQuestionCategory(string ShopType);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointment_BasicDetail/Appointment_BasicDetail")]
        List<Appointment_BasicDetail> UploadAppointment_BasicDetail(Stream Appointment_BasicDetail);


        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointment_BasicDetail?CreatedBy={CreatedBy}")]
        List<Appointment_BasicDetail> DownloadAppointment_BasicDetail(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "AppointmentShopDetails/AppointmentShopDetails")]
        List<AppointmentShopDetails> UploadAppointmentShopDetails(Stream AppointmentShopDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointmentShopDetails?CreatedBy={CreatedBy}&RegId={RegId}")]
        List<AppointmentShopDetails> DownloadAppointmentShopDetails(string CreatedBy, string RegId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadBankDetails/BankDetails")]
        List<UploadBankDetails> UploadBankDetails(Stream BankDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadBankDetails?CreatedBy={CreatedBy}&RegId={RegId}")]
        List<UploadBankDetails> DownloadBankDetails(string CreatedBy, string RegId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointmentBooking/AppointmentBooking")]
        List<UploadAppointmentBooking> UploadAppointmentBooking(Stream AppointmentBooking);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTimeSlot/TimeSlot")]
        List<UploadTimeSlot> UploadTimeSlot(Stream TimeSlot);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTimeSlot?CreatedBy={CreatedBy}")]
        List<UploadTimeSlot> DownloadTimeSlot(string CreatedBy);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOrderNewDetails1/Order")]
        List<UploadOrder> UploadOrderNewDetails1(Stream Order);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointmentBooking?Mobile_No={Mobile_No}&UserRole={UserRole}")]
        List<AppointmentBooking> DownloadAppointmentBooking(string Mobile_No, string UserRole);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew1/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew1(Stream ProfileData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadBankDetailsNew/BankDetails")]
        List<UploadBankDetails> UploadBankDetailsNew(Stream BankDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReviewsDetails?RegistrationId={RegistrationId}")]
        List<DownloadReviewDetails> DownloadReviewsDetails(string RegistrationId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReviewDetailsNew/ReviewDetails")]
        List<UploadReviewDetails> UploadReviewDetailsNew(Stream ReviewDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReviewsRate?RegistrationId={RegistrationId}")]
        List<DownloadReviewRate> DownloadReviewsRate(string RegistrationId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew2/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew2(Stream ProfileData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointmentBookingNew/AppointmentBooking")]
        List<UploadAppointmentBooking> UploadAppointmentBookingNew(Stream AppointmentBooking);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTimeSlotDatewise?CreatedBy={CreatedBy}&Date={Date}&DayId={DayId}")]
        List<UploadTimeSlot> DownloadTimeSlotDatewise(string CreatedBy,string Date,string DayId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointmentBookingNew1/AppointmentBooking")]
        List<UploadAppointmentBooking> UploadAppointmentBookingNew1(Stream AppointmentBooking);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew3/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew3(Stream ProfileData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointmentBookingDetails?MobileNo={MobileNo}&Date={Date}&DayId={DayId}")]
        List<DownloadAppointmentBooking> DownloadAppointmentBookingDetails(string MobileNo, string Date, string DayId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateAppointmentBooking/AppointmentBooking")]
        List<UpdateAppointmentBooking> UpdateAppointmentBooking(Stream AppointmentBooking);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointmentStatus?ShopMobile={ShopMobile}&UserMobile={UserMobile}")]
        List<DownloadAppointmentBookingStatus> DownloadAppointmentStatus(string ShopMobile, string UserMobile);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadQuotationBasicDetails/UploadData")]
        List<UploadOrder> UploadQuotationBasicDetails(Stream UploadData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadQuotationBasicDetails?Latitude={Latitude}&Longitude={Longitude}&Role={Role}&MobileNo={MobileNo}&State={State}&District={District}&Taluka={Taluka}")]
        List<UploadOrder> DownloadQuotationBasicDetails(string Latitude,string Longitude,string Role,string MobileNo, string State, string District, string Taluka);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadQuotationItemsDetails/UploadData")]
        List<QuotationItemParameter> UploadQuotationItemsDetails(Stream UploadData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadQuotationItemsDetails?QuotationBasic_Id={QuotationBasic_Id}")]
        List<QuotationItemParameter> DownloadQuotationItemsDetails(string QuotationBasic_Id);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOrderNewDetails2/Order")]
        List<UploadOrder> UploadOrderNewDetails2(Stream Order);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadQuotationBasicDetails1/UploadData")]
        List<UploadOrder> UploadQuotationBasicDetails1(Stream UploadData);
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadShopListWiseResult1?longitude={longitude}&latitude={latitude}&District={District}&Taluka={Taluka}&ShopType={ShopType}")]
        List<NeedlyRegistrationDetail> DownloadShopListWiseResult1(string longitude, string latitude, string District, string Taluka, string ShopType);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadInventoryitem_MasterNew?ShopType={ShopType}&MaxId={MaxId}")]
        List<Inventoryitem_Master> DownloadInventoryitem_MasterNew(string ShopType, string MaxId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadOrderNewDetails3/Order")]
        List<UploadOrder> UploadOrderNewDetails3(Stream Order);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTimeSlot1/TimeSlot")]
        List<UploadTimeSlot> UploadTimeSlot1(Stream TimeSlot);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointmentBookingNew2/AppointmentBooking")]
        List<UploadAppointmentBooking> UploadAppointmentBookingNew2(Stream AppointmentBooking);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointmentAddQuestion/UploadData")]
        List<UploadAppointmentAddQuestion> UploadAppointmentAddQuestion(Stream UploadData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointmentAddQuestion?Createdby={Createdby}")]
        List<UploadAppointmentAddQuestion> DownloadAppointmentAddQuestion(string Createdby);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "updateAppointmentBookings/AppointmentBooking")]
        List<UploadAppointmentBooking> updateAppointmentBookings(Stream AppointmentBooking);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateSharedContacts/UploadData")]
        List<UploadSharedContactsParameter> UpdateSharedContacts(Stream UploadData);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadSharedContacts?Createdby={Createdby}")]
        List<UploadSharedContactsParameter> DownloadSharedContacts(string Createdby);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAppointmentAddQuestion1/UploadData")]
        List<UploadAppointmentAddQuestion> UploadAppointmentAddQuestion1(Stream UploadData);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAppointmentAddQuestionNew?Createdby={Createdby}&ShopType={ShopType}")]
        List<UploadAppointmentAddQuestion> DownloadAppointmentAddQuestionNew(string Createdby,string ShopType);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupFields/GroupFields")]
        List<UploadGroupFields> UploadGroupFields(Stream GroupFields);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadLedger/Legder")]
        List<UploadLegder> UploadLedger(Stream Legder);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadLedger?Institute_Code={Institute_Code}")]
        List<DownloadLedger> DownloadLedger(string Institute_Code);



        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupFields?InstituteCode={InstituteCode}")]
        List<DownloadGroupField> DownloadGroupField(string InstituteCode);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTransactionAndVoucherDetailsNew?InstituteCode={InstituteCode}&FromDate={FromDate}&ToDate={ToDate}")]
        List<Voucher> DownloadTransactionAndVoucherDetailsNew(string InstituteCode, string FromDate, string ToDate);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertBlankInVoucher?")]
        List<UploadVoucherBlank> InsertBlankInVoucher();


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadVoucherDetails/UploadData")]
        List<VoucherDetailsParameter> UploadVoucherDetails(Stream UploadData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTransactionDetailsNew/UploadData")]
        List<TransactionDetailsParameter> UploadTransactionDetailsNew(Stream UploadData);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddStaff/AddStaff")]
        List<UploadAddStaff> UploadAddStaff(Stream AddStaff);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddAttendance/Attendance")]
        List<UploadAddAttendance> UploadAddAttendance(Stream Attendance);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddStaff?CreatedBy={CreatedBy}")]
        List<UploadAddStaff> DownloadAddStaff(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddAttendance?CreatedBy={CreatedBy}")]
        List<UploadAddAttendance> DownloadAddAttendance(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetails/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetails(Stream StaffDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadStaffDetails?CreatedBy={CreatedBy}")]
        List<UploadStaffDetails> DownloadStaffDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddLoan/AddLoan")]
        List<AddLoan> UploadAddLoan(Stream AddLoan);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddPayment/AddPayment")]
        List<AddPayment> UploadAddPayment(Stream AddPayment);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddBonus/AddBonus")]
        List<AddBonus> UploadAddBonus(Stream AddBonus);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddLoan?CreatedBy={CreatedBy}")]
        List<AddLoan> DownloadAddLoan(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddPayment?CreatedBy={CreatedBy}")]
        List<AddPayment> DownloadAddPayment(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddBonus?CreatedBy={CreatedBy}")]
        List<AddBonus> DownloadAddBonus(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNewsPaper/NewsPaper")]
        List<UploadNewsPaper> UploadNewsPaper(Stream NewsPaper);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNewsPaperNew/NewsPaper")]
        List<UploadNewsPaper> UploadNewsPaperNew(Stream NewsPaper);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNewsPaper?CreatedBy={CreatedBy}")]
        List<UploadNewsPaper> DownloadNewsPaper(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadRegisterNewsPaper?AppKeyword={AppKeyword}")]
        List<RegisterNewsPaper> DownloadRegisterNewsPaper(string AppKeyword);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadPaperRequest/PaperRequest")]
        List<PaperRequest> UploadPaperRequest(Stream PaperRequest);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadBuildingDetails/BuildingDetails")]
        List<BuildingDetails> UploadBuildingDetails(Stream BuildingDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadBuildingDetails?CreatedBy={CreatedBy}")]
        List<BuildingDetails> DownloadBuildingDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerDetails/CustomerDetails")]
        List<CustomerDetails> UploadCustomerDetails(Stream CustomerDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerDetails?CreatedBy={CreatedBy}")]
        List<CustomerDetails> DownloadCustomerDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddCustomerPaper/AddCustomerPaper")]
        List<UploadAddCustomerPaper> UploadAddCustomerPaper(Stream AddCustomerPaper);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddCustomerPaper?CreatedBy={CreatedBy}")]
        List<UploadAddCustomerPaper> DownloadAddCustomerPaper(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddCoupon/AddCoupon")]
        List<UploadAddCoupon> UploadAddCoupon(Stream AddCoupon);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddCoupon?CreatedBy={CreatedBy}")]
        List<UploadAddCoupon> DownloadAddCoupon(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddHoliday/AddHoliday")]
        List<UploadAddHoliday> UploadAddHoliday(Stream AddHoliday);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddHoliday?CreatedBy={CreatedBy}")]
        List<UploadAddHoliday> DownloadAddHoliday(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadMyStock/MyStock")]
        List<UploadMyStock> UploadMyStock(Stream MyStock);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadMyStock?CreatedBy={CreatedBy}")]
        List<UploadMyStock> DownloadMyStock(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerBillDetails/CustomerBillDetails")]
        List<UploadCustomerBillDetails> UploadCustomerBillDetails(Stream CustomerBillDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerBillDetails?CreatedBy={CreatedBy}")]
        List<UploadCustomerBillDetails> DownloadCustomerBillDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddCouponNew/AddCoupon")]
        List<UploadAddCouponNew> UploadAddCouponNew(Stream AddCoupon);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCouponPaperDetails?CreatedBy={CreatedBy}")]
        List<UploadAddCouponNew> DownloadCouponPaperDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerPaymentDetails/CustomerPaymentDetails")]
        List<CustomerPaymentDetails> UploadCustomerPaymentDetails(Stream CustomerPaymentDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerPaymentDetails?CreatedBy={CreatedBy}")]
        List<CustomerPaymentDetails> DownloadCustomerPaymentDetails(string CreatedBy);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryCategoryNew/CategoryDetails")]
        List<InventoryCategoryDetails> UploadInventoryCategoryNew(Stream CategoryDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryItemNew1/InventoryItem")]
        List<InventoryItem> UploadInventoryItemNew1(Stream InventoryItem);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlyRegData?DynamicURL={DynamicURL}")]
        List<NeedlyRegistrationDetail> DownloadNeedlyRegData(string DynamicURL);
        

        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCategoryMaster?ShopType={ShopType}")]
        List<UploadCategoryMasterParameter> DownloadCategoryMaster(string ShopType);

        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadImage?Id={Id}")]
        List<InventoryItem> DownloadImage(string Id);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNotifactionMobileData/NotifactionData")]
        List<UploadNotifiction> UploadNotifactionMobileData(Stream NotifactionData);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadJobDetails/JobDataDetails")]
        List<UploadJobDetailsParameter> UploadJobDetails(Stream JobDataDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJobDetails?MobileNo={MobileNo}")]
        List<UploadJobDetailsParameter> DownloadJobDetails(string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadStateWiseJobDetailS?State={State}&District={District}&Taluka={Taluka}")]
        List<UploadJobDetailsParameter> DownloadStateWiseJobDetailS(string State, string District, string Taluka);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadJobApplicationDetails/JobAppDataDetails?JobAppDataDetails={JobAppDataDetails}")]
        List<UploadJobApplicationDetailsParameter> UploadJobApplicationDetails(string JobAppDataDetails, Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadBuildingDetailsNew/BuildingDetails")]
        List<BuildingDetails> UploadBuildingDetailsNew(Stream BuildingDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerDetailsNew/CustomerDetails")]
        List<CustomerDetails> UploadCustomerDetailsNew(Stream CustomerDetails);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddCustomerPaperNew/AddCustomerPaper")]
        List<UploadAddCustomerPaper> UploadAddCustomerPaperNew(Stream AddCustomerPaper);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddCouponNew1/AddCoupon")]
        List<UploadAddCouponNew> UploadAddCouponNew1(Stream AddCoupon);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddHolidayNew/AddHoliday")]
        List<UploadAddHoliday> UploadAddHolidayNew(Stream AddHoliday);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadMyStockNew/MyStock")]
        List<UploadMyStock> UploadMyStockNew(Stream MyStock);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerBillDetailsNew/CustomerBillDetails")]
        List<UploadCustomerBillDetails> UploadCustomerBillDetailsNew(Stream CustomerBillDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerPaymentDetailsNew/CustomerPaymentDetails")]
        List<CustomerPaymentDetails> UploadCustomerPaymentDetailsNew(Stream CustomerPaymentDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateDetails?ShopType={ShopType}")]
        List<UploadTemplateParameter> DownloadTemplateDetails(string ShopType);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJobApplication?MobileNo={MobileNo}")]
        List<UploadJobApplicationDetailsParameter> DownloadJobApplication(string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadApplicationJobId?JobId={JobId}")]
        List<UploadJobApplicationDetailsParameter> DownloadApplicationJobId(string JobId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateRefMobileNo/UpdateMobile")]
        List<UpdateRefMobileNoParameter> UpdateRefMobileNo(Stream UpdateMobile);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerBillDetailsNew1/CustomerBillDetails")]
        List<UploadCustomerBillDetails> UploadCustomerBillDetailsNew1(Stream CustomerBillDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferalDetails1/ReferalDetails")]
        List<UploadReferalDatails> UploadReferalDetails1(Stream ReferalDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew(Stream StaffDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddItemMaster/AddItemDetails")]
        List<UpdateAddItemParameter> UploadAddItemMaster(Stream AddItemDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddItemMaster?Category={Category}&CreatedBy={CreatedBy}")]
        List<UpdateAddItemParameter> DownloadAddItemMaster(string Category, string CreatedBy);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAssignedStaff/AssignedStaffDetails")]
        List<UploadAssignedParameter> UploadAssignedStaff(Stream AssignedStaffDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAssignedStaff?CreatedBy={CreatedBy}")]
        List<UploadAssignedParameter> DownloadAssignedStaff( string CreatedBy);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAllotManpower/AllotManpowerDetails")]
        List<UploadAllotManpowerParameter> UploadAllotManpower(Stream AllotManpowerDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAllotManpower?CreatedBy={CreatedBy}")]
        List<UploadAllotManpowerParameter> DownloadAllotManpower(string CreatedBy);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffProfileDetails/ProfileDetails")]
        List<UploadStaffProfileDetailsParameter> UploadStaffProfileDetails(Stream ProfileDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadStaffProfileDetails?ServerId={ServerId}")]
        List<UploadStaffProfileDetailsParameter> DownloadStaffProfileDetails(string ServerId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddClientPayment/AddPayment")]
        List<AddPayment> UploadAddClientPayment(Stream AddPayment);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddClientPayment?CreatedBy={CreatedBy}")]
        List<AddPayment> DownloadAddClientPayment(string CreatedBy);
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNewsPaperNew1/NewsPaper")]
        List<UploadNewsPaper> UploadNewsPaperNew1(Stream NewsPaper);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddAttendanceNew/Attendance")]
        List<UploadAddAttendance> UploadAddAttendanceNew(Stream Attendance);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddStaffNew/AddStaff")]
        List<UploadAddStaff> UploadAddStaffNew(Stream AddStaff);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAllotManpowerNew/AllotManpowerDetails")]
        List<UploadAllotManpowerParameter> UploadAllotManpowerNew(Stream AllotManpowerDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerBillDetailsNew2/CustomerBillDetails")]
        List<UploadCustomerBillDetails> UploadCustomerBillDetailsNew2(Stream CustomerBillDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew1/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew1(Stream StaffDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadShowStaffDetails?CreatedBy={CreatedBy}&MobileNo={MobileNo}")]
        List<UploadStaffDetails> DownloadShowStaffDetails(string CreatedBy,string MobileNo);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddAttendanceNew1/Attendance")]
        List<UploadAddAttendance> UploadAddAttendanceNew1(Stream Attendance);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAllotManpowerNew1/AllotManpowerDetails")]
        List<UploadAllotManpowerParameter> UploadAllotManpowerNew1(Stream AllotManpowerDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAllotManpowerNew2/AllotManpowerDetails")]
        List<UploadAllotManpowerParameter> UploadAllotManpowerNew2(Stream AllotManpowerDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddSubstituteDetails/AddSubstituteDetails")]
        List<UploadAddSubstituteDetailsParameter> UploadAddSubstituteDetails(Stream AddSubstituteDetails);



        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew2/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew2(Stream StaffDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddSubstituteDetailsNew/AddSubstituteDetails")]
        List<UploadAddSubstituteDetailsParameter> UploadAddSubstituteDetailsNew(Stream AddSubstituteDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddItemMasterNew/AddItemDetails")]
        List<UpdateAddItemParameter> UploadAddItemMasterNew(Stream AddItemDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWorkDetails/WorkDetailsData")]
        List<UpdateWorkDetailsParameter> UploadWorkDetails(Stream WorkDetailsData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddAttendanceNew2/Attendance")]
        List<UploadAddAttendance> UploadAddAttendanceNew2(Stream Attendance);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew3/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew3(Stream StaffDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddStaffNew1/AddStaff")]
        List<UploadAddStaff> UploadAddStaffNew1(Stream AddStaff);




        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAssignedStaffNew/AssignedStaffDetails")]
        List<UploadAssignedParameter> UploadAssignedStaffNew(Stream AssignedStaffDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew4/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew4(Stream StaffDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteAddStaff?ServerId={ServerId}")]
        List<UploadAddStaff> DeleteAddStaff(string ServerId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteStaffDetails?ServerId={ServerId}")]
        List<UploadStaffDetails> DeleteStaffDetails(string ServerId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteCustomerDetails?ServerId={ServerId}")]
        List<CustomerDetails> DeleteCustomerDetails(string ServerId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteAssignedStaff?ServerId={ServerId}")]
        List<UploadAssignedParameter> DeleteAssignedStaff(string ServerId);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAddItemMasterNew1/AddItemDetails")]
        List<UpdateAddItemParameter> UploadAddItemMasterNew1(Stream AddItemDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "NotificationDetailsNew?MobileNo={MobileNo}&Message={Message}")]
        List<UploadAddStaff> NotificationDetailsNew(string MobileNo, string Message);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "NotificationGPSDetails?MobileNo={MobileNo}&UserMobile={UserMobile}")]
        List<UploadAddStaff> NotificationGPSDetails(string MobileNo, string UserMobile);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew5/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew5(Stream StaffDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerDetailsNew1/CustomerDetails")]
        List<CustomerDetails> UploadCustomerDetailsNew1(Stream CustomerDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWorkDetailsNew/WorkDetailsData")]
        List<UpdateWorkDetailsParameter> UploadWorkDetailsNew(Stream WorkDetailsData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerDetailsNew?CreatedBy={CreatedBy}&Date={Date}")]
        List<CustomerDetails> DownloadCustomerDetailsNew(string CreatedBy, string Date);

        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadWorkDetails?CreatedBy={CreatedBy}")]
        List<UpdateWorkDetailsParameter> DownloadWorkDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExtraStaff?CreatedBy={CreatedBy}&Date={Date}")]
        List<UploadExtraStaff> DownloadExtraStaff(string CreatedBy, string Date);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExtraStaff/ExtraShiftData")]
        List<UploadExtraStaff> UploadExtraStaff(Stream ExtraShiftData);

        //23/01/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadSubstituteDetails?CreatedBy={CreatedBy}&Date={Date}")]
        List<UploadAddSubstituteDetailsParameter> DownloadSubstituteDetails(string CreatedBy, string Date);


        //25/01/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadEmployeeDetails/EmployeeData")]
        List<ParameterEmployeeDetails> UploadEmployeeDetails(Stream EmployeeData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEmployeeDetails?CreatedBy={CreatedBy}&Date={Date}")]
        List<ParameterEmployeeDetails> DownloadEmployeeDetails(string CreatedBy, string Date);

        //30/01/2021

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddAttendanceNew?CreatedBy={CreatedBy}&Date={Date}")]
        List<UploadAddAttendance> DownloadAddAttendanceNew(string CreatedBy, string Date);

        //03/02/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew4/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew4(Stream ProfileData);


        //12/02/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAttendanceDetails/AttendanceData")]
        List<ParameterAttendanceDetails> UploadAttendanceDetails(Stream AttendanceData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAttendanceDetails?CreatedBy={CreatedBy}")]
        List<ParameterAttendanceDetails> DownloadAttendanceDetails(string CreatedBy);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadClientPaymentHistoryDetails/ClientPaymentData")]
        List<ClientPaymentHistoryParameter> UploadClientPaymentHistoryDetails(Stream ClientPaymentData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadUploadClientPaymentDetails?CreatedBy={CreatedBy}")]
        List<ClientPaymentHistoryParameter> DownloadUploadClientPaymentDetails(string CreatedBy);

        //15/02/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlyRegistrationDetail1?Mobile_No={Mobile_No}")]
        List<NeedlyRegistrationDetail> DownloadNeedlyRegistrationDetail1(string Mobile_No);


        //16/02/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAllotManpower1?CreatedBy={CreatedBy}")]
        List<UploadAllotManpowerParameter> DownloadAllotManpower1(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAllotManpowerNew3/AllotManpowerDetails1")]
        List<UploadAllotManpowerParameter> UploadAllotManpowerNew3(Stream AllotManpowerDetails1);


        //08/03/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadPetrolReading/PetrolReadingDetails")]
        List<UploadPetrolReadingParameter> UploadPetrolReading(Stream PetrolReadingDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadPetrolReading?CreatedBy={CreatedBy}")]
        List<UploadPetrolReadingParameter> DownloadPetrolReading(string CreatedBy);

        //11/03/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadMaterialIssued/MaterialDetails")]
        List<MaterialIssuedParameter> UploadMaterialIssued(Stream MaterialDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadMaterialIssued?CreatedBy={CreatedBy}")]
        List<MaterialIssuedParameter> DownloadMaterialIssued(string CreatedBy);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDateWise_StartDateToEnd?LBDistrictID={LBDistrictID}&ElectionId={ElectionId}")]
        List<DownloadDateWiseParameter> DownloadDateWise_StartDateToEnd(string LBDistrictID, string ElectionId);

        //viraj requirement
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerContactDetails?CreatedBy={CreatedBy}")]
        List<CustomerContactDetailsParameter> DownloadCustomerContactDetails(string CreatedBy);

        //22/03/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadQuestionCategory1?ShopType={ShopType}&CreatedBy={CreatedBy}")]
        List<DownloadQuestionCategory> DownloadQuestionCategory1(string ShopType, string CreatedBy);

        //25/03/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCount/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCount(Stream WhatsappCountDetails);

        //26/03/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppTemplateDetails/WhatsAppTemplateDetails")]
        List<WhatsAppTemplateParameter> UploadWhatsAppTemplateDetails(Stream WhatsAppTemplateDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "WhatsAppTemplateDetails?CreatedBy={CreatedBy}")]
        List<WhatsAppTemplateParameter> DownloadWhatsAppTemplateDetails(string CreatedBy);

        //01/04/2021
        //viraj requirement
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadContactExcelHeading?CreatedBy={CreatedBy}")]
        List<CustomerContactDetailsParameter> DownloadContactExcelHeading(string CreatedBy);

        //07/04/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerContactDetails1?CreatedBy={CreatedBy}&FromSrno={FromSrno}&ToSrno={ToSrno}")]
        List<CustomerContactDetailsParameter> DownloadCustomerContactDetails1(string CreatedBy,string FromSrno, string ToSrno);
        
        //09/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCompanyDetails/CompanyDetails")]
        List<CompanyDetailsParameter> UploadCompanyDetails(Stream CompanyDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCompanyDetails?CreatedBy={CreatedBy}")]
        List<CompanyDetailsParameter> DownloadCompanyDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadSocialLinks/SocialLinksDetails")]
        List<SocialLinksDetailsParameter> UploadSocialLinks(Stream SocialLinksDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadSocialLinks?CreatedBy={CreatedBy}")]
        List<SocialLinksDetailsParameter> DownloadSocialLinks(string CreatedBy);
        //10/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadPhoneContacts/PhoneContactsDetails")]
        List<PhoneContactsDetailsParameter> UploadPhoneContacts(Stream PhoneContactsDetails);
        //14/04/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadWhatsAppMsgCount?CreatedBy={CreatedBy}")]
        List<WhatsappCountParameter> DownloadWhatsAppMsgCount(string CreatedBy);

        //16/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadPaymentOption/PaymentOptionDetails")]
        List<PaymentOptionParameter> UploadPaymentOption(Stream PaymentOptionDetails);
        //17/04/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadPaymentOption?CreatedBy={CreatedBy}")]
        List<PaymentOptionParameter> DownloadPaymentOption(string CreatedBy);

        //20/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProductServiceDetails/ProductServiceDetails")]
        List<ProductServiceParameter> UploadProductServiceDetails(Stream ProductServiceDetails);
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadProductServiceDetails?CreatedBy={CreatedBy}")]
        List<ProductServiceParameter> DownloadProductServiceDetails(string CreatedBy);

        //24/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadImageGalleryDetails/ImageGalleryDetails")]
        List<ImageGalleryParameter> UploadImageGalleryDetails(Stream ImageGalleryDetails);
        //26/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupDetails/GroupDetails")]
        List<ParameterGroupDetails> UploadGroupDetails(Stream GroupDetails);

        //27/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadRuleDetails/RuleDetails")]
        List<ParameterRuleDetails> UploadRuleDetails(Stream RuleDetails);

        //28/04/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadKeywordDetails/KeywordDetails")]
        List<ParameterKeywordDetails> UploadKeywordDetails(Stream KeywordDetails);

        //29/04/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadRuleDetails?CreatedBy={CreatedBy}")]
        List<ParameterRuleDetails> DownloadRuleDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadKeywordDesription/KeywordDesription")]
        List<ParameterKeywordDesription> UploadKeywordDesription(Stream KeywordDesription);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadKeywordDetails?CreatedBy={CreatedBy}")]
        List<ParameterKeywordDetails> DownloadKeywordDetails(string CreatedBy);
        //30/04/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadKeywordDesription?KeywordId={KeywordId}")]
        List<ParameterKeywordDesription> DownloadKeywordDesription(string KeywordId);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupDetails?CreatedBy={CreatedBy}")]
        List<ParameterGroupDetails> DownloadGroupDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProjectDetails/ProjectDetails")]
        List<ParameterProjectDetails> UploadProjectDetails(Stream ProjectDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadProjectDetails?CreatedBy={CreatedBy}")]
        List<ParameterProjectDetails> DownloadProjectDetails(string CreatedBy);
       
        //01/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadContactDetails/ContactDetails")]
        List<CustomerContactDetailsParameter> UploadContactDetails(Stream ContactDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProjectMember/ProjectMemberDetails")]
        List<ParameterProjectMember> UploadProjectMember(Stream ProjectMemberDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadProjectMember?CreatedBy={CreatedBy}")]
        List<ParameterProjectMember> DownloadProjectMember(string CreatedBy);

        //03/05/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadProjectByProjectId?MobileNo={MobileNo}")]
        List<ParameterProjectDetails> DownloadProjectByProjectId(string MobileNo);

       

        //06/05/2021

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAlarmDetails/AlarmDetails")]
        List<ParameterAlarmDetails> UploadAlarmDetails(Stream AlarmDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAlarmDetails?CreatedBy={CreatedBy}")]
        List<ParameterAlarmDetails> DownloadAlarmDetails(string CreatedBy);

        //12/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormDetails/FormDetails")]
        List<ParameterFormDetails> UploadFormDetails(Stream FormDetails);

        //14/05/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetailsByMob?MobileNo={MobileNo}")]
        List<ParameterVaccinationDetails> DownloadVaccinationDetailsByMob(string MobileNo);

        //15/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormMember/FormMember")]
        List<ParameterFormMember> UploadFormMember(Stream FormMember);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetails?VaccinationCentre={VaccinationCentre}&FromTokenNo={FromTokenNo}&ToTokenNo={ToTokenNo}")]
        List<ParameterVaccinationDetails> DownloadVaccinationDetails(string VaccinationCentre, string FromTokenNo, string ToTokenNo);

        //16/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeyword/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeyword(Stream FormKeywordDetails);

        //17/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProjectMemberNew/ProjectMemberDetails")]
        List<ParameterProjectMember> UploadProjectMemberNew(Stream ProjectMemberDetails);

        //17/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateVaccinationDateTimeSlot/VaccinationDateTimeSlot")]
        List<ParameterVaccinationDetails> UpdateVaccinationDateTimeSlot(Stream VaccinationDateTimeSlot);

        //18/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordDescription/FormKeywordDescription")]
        List<ParameterFormKeywordDesription> UploadFormKeywordDescription(Stream FormKeywordDescription);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormDetails?CreatedBy={CreatedBy}")]
        List<ParameterFormDetails> DownloadFormDetails(string CreatedBy);
        //19/05/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormMember?CreatedBy={CreatedBy}")]
        List<ParameterFormMember> DownloadFormMember(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormKeywords?CreatedBy={CreatedBy}")]
        List<ParameterFormKeyword> DownloadFormKeywords(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormKeywordDescription?KeywordId={KeywordId}")]
        List<ParameterFormKeywordDesription> DownloadFormKeywordDescription(string KeywordId);


        //20/05/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetailsCount")]
        List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCount();


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetailsCountByDate?Date={Date}")]
        List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCountByDate(string Date);

        //21/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCardLinkDetails/CardLinkDetails")]
        List<CardLinkParameter> UploadCardLinkDetails(Stream CardLinkDetails);



        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormDetailsByMemberNo?MemberNo={MemberNo}")]
        List<ParameterFormDetails> DownloadFormDetailsByMemberNo(string MemberNo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferncesMobile/ReferealCode")]
        List<RegstrationRefParameter> UploadReferncesMobile(Stream ReferealCode);

        //25/05/2021

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadregistrationShareURL?ShareURL={ShareURL}")]
        List<ParameterRegistrationShareUrl> DownloadregistrationShareURL(string ShareURL);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadregistrationByReferenceNo?ReferenceMobile={ReferenceMobile}")]
        List<ParameterRegistrationShareUrl> DownloadregistrationByReferenceNo(string ReferenceMobile);

        // 27/05/2021

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationlink?District={District}")]
        List<ParameterVClink> DownloadVaccinationlink(string District);

        //[OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationCenter?District={District}")]
        //List<ParameterVaccinationCenter> DownloadVaccinationCenter(string District);

        //28/05/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationItemName?ReportID={ReportID}&FieldID={FieldID}")]
        List<ParameterVaccinationItemName> DownloadVaccinationItemName(string ReportID, string FieldID);

        // 30/05/2021

        
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew(Stream FormKeywordDetails);

        //31/05/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew1/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew1(Stream FormKeywordDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerContactDetailsByGroupId?GroupId={GroupId}")]
        List<CustomerContactDetailsParameter> DownloadCustomerContactDetailsByGroupId(string GroupId);
        //02/06/2021
        
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetailsCountByReportId?ReportId={ReportId}")]
        List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCountByReportId(String ReportId);

        [OperationContract]

        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetailsCountByDateAndReportId?Date={Date}&ReportId={ReportId}")]
        List<ParameterVaccinationDetailsCount> DownloadVaccinationDetailsCountByDateAndReportId(string Date, string ReportId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationDetailsByCreatedBy?CreatedBy={CreatedBy}")]
        List<ParameterVaccinationDetails> DownloadVaccinationDetailsByCreatedBy(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordDescriptionNew/FormKeywordDescription")]
        List<ParameterFormKeywordDesription> UploadFormKeywordDescriptionNew(Stream FormKeywordDescription);

        //08/06/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlyRegistrationIMEIDetails?MobileNo={MobileNo}&IMEI={IMEI}")]
        List<NeedlyRegistrationDetail> DownloadNeedlyRegistrationIMEIDetails(string MobileNo, string IMEI);

        //08/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew2/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew2(Stream FormKeywordDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupDetailsNew/GroupDetails")]
        List<ParameterGroupDetails> UploadGroupDetailsNew(Stream GroupDetails);

        //09/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordDescriptionNew1/FormKeywordDescription")]
        List<ParameterFormKeywordDesription> UploadFormKeywordDescriptionNew1(Stream FormKeywordDescription);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupOfGroup/GroupOfGroupDetails")]
        List<ParameterGroupOfGroup> UploadGroupOfGroup(Stream GroupOfGroupDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupOfGroupByGroupId?GroupIdGroupId={GroupId}")]
        List<ParameterGroupOfGroup> DownloadGroupOfGroupByGroupId(string GroupId);

        //11/06/2021
        
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadKeywordDetailsNew/KeywordDetails")]
        List<ParameterKeywordDetails> UploadKeywordDetailsNew(Stream KeywordDetails);

        //[OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVaccinationCreatedByCount")]
        //List<ParameterVaccinationDetailsCount> DownloadVaccinationCreatedByCount();

        //08/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew3/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew3(Stream FormKeywordDetails);

        //14/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordDescriptionNew2/FormKeywordDescription")]
        List<ParameterFormKeywordDesription> UploadFormKeywordDescriptionNew2(Stream FormKeywordDescription);
        //15/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferalDetails2/ReferalDetails")]
        List<UploadReferalDatails> UploadReferalDetails2(Stream ReferalDetails);

        //16/06/2021
        //[OperationContract]
        //[WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadKeywordFieldName?MobileNo={MobileNo}&CreatedBy={CreatedBy}&keyword={keyword}")]
        //List<ParameterKeywordField> DownloadKeywordFieldName(string MobileNo, string CreatedBy, string keyword);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormMemberNew/FormMember")]
        List<ParameterFormMember> UploadFormMemberNew(Stream FormMember);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProjectMemberNew1/ProjectMemberDetails")]
        List<ParameterProjectMember> UploadProjectMemberNew1(Stream ProjectMemberDetails);
        //18/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupDetailsNew1/GroupDetails")]
        List<ParameterGroupDetails> UploadGroupDetailsNew1(Stream GroupDetails);

        //19/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew4/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew4(Stream FormKeywordDetails);

        //28/06/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadChatDetails/ChatDetails")]
        List<ParameterChatDetails> UploadChatDetails(Stream ChatDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadChatDetails?CreatedBy={CreatedBy}")]
        List<ParameterChatDetails> DownloadChatDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadPhone?CreatedBy={CreatedBy}")]
        List<ParameterPhoene> DownloadPhone(string CreatedBy);

        //29/06/2021 [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadStaffDetailsNew6/StaffDetails")]
        List<UploadStaffDetails> UploadStaffDetailsNew6(Stream StaffDetails);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadLoginDetails?UserName={UserName}")]
        List<ParameterLoginDetails> DownloadLoginDetails(string UserName);
        //9/07/021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAlarmDetailsNew1/AlarmDetails")]
        List<ParameterAlarmDetails> UploadAlarmDetailsNew1(Stream AlarmDetails);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppMessageDetails/WhatsAppMessageDetails?WhatsAppMessageDetails={WhatsAppMessageDetails}")]
        List<ParameterWhatsAppMessageDetails> UploadWhatsAppMessageDetails(string WhatsAppMessageDetails, Stream stream);

        //14/07/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerContactDetailsSrNoCount?CreatedBy={CreatedBy}")]
        List<CustomerContactDetailsParameterSrNoCount> DownloadCustomerContactDetailsSrNoCount(string CreatedBy);

       
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerContactDetailsByCategory?CreatedBy={CreatedBy}&Category={Category}")]
        List<CustomerContactDetailsParameter> DownloadCustomerContactDetailsByCategory(string CreatedBy, string Category);

        //17/07/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadDiscountDeduction/DiscountDeduction")]
        List<ParameterDiscountDeduction> UploadDiscountDeduction(Stream DiscountDeduction);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDiscountDeduction?UserMobileNo={UserMobileNo}")]
        List<ParameterDiscountDeduction> DownloadDiscountDeduction(string UserMobileNo);

        //19/07/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateAppointmentBookingByType/AppointmentBooking")]
        List<UpdateAppointmentBooking> UpdateAppointmentBookingByType(Stream AppointmentBooking);

        //21/07/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCountNew/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCountNew(Stream WhatsappCountDetails);

        //23/07/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadUsageStatisticsDate/UsageStatisticsDate")]
        List<UsageStatisticsDateparameter> UploadUsageStatisticsDate(Stream UsageStatisticsDate);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadUsageStatisticseDate?MobileNo={MobileNo}")]
        List<UsageStatisticsDateparameter> DownloadUsageStatisticseDate(string MobileNo);

        //23/07/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateGroupDetails/GroupDetails")]
        List<CustomerContactDetailsParameter> UpdateGroupDetails(Stream GroupDetails);


        //24/07/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadMarketinReport?MobileNo={MobileNo}&ReeralCode={ReeralCode}")]
        List<MarketinReporteparameter> DownloadMarketinReport(string MobileNo, string ReeralCode);

        //28/07/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTemplateCategoryDetails/TemplateCategoryDetails")]
        List<TemplateCategoryDetailsParameter> UploadTemplateCategoryDetails(Stream TemplateCategoryDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormTemplateDetails/FormTemplateDetails")]
        List<FormTemplateDetailsParameter> UploadFormTemplateDetails(Stream FormTemplateDetails);

        //29/07/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDownloadTemplateCategoryDetails")]
        List<TemplateCategoryDetailsParameter> DownloadDownloadTemplateCategoryDetails();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDownloadFormTemplateDetails")]
        List<FormTemplateDetailsParameter> DownloadDownloadFormTemplateDetails();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGoogleFormQuestionDetails/GoogleFormQuestionDetails")]
        List<GoogleFormQuestionDetailsParameter> UploadGoogleFormQuestionDetails(Stream GoogleFormQuestionDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGoogleFormQuestion?TemplateId={TemplateId}")]
        List<GoogleFormQuestionDetailsParameter> DownloadGoogleFormQuestion(string TemplateId);

        //04/08/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateLoginDetails?MobileNo={MobileNo}")]
        List<TemplateLoginDetailsParameter> DownloadTemplateLoginDetails(string MobileNo);

        //04/08/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerContactDetailsByGroupIdNew?GroupId={GroupId}&CreatedBy={CreatedBy}")]
        List<CustomerContactDetailsParameter> DownloadCustomerContactDetailsByGroupIdNew(string GroupId, string CreatedBy);

        //08/08/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCashBookDetails?MobileNo={MobileNo}")]
        List<Cashbookparameter> DownloadCashBookDetails(string MobileNo);

        //10/08/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadUdhaarDetails/UdhaarDetails")]
        List<UdhaarDetailsParameter> UploadUdhaarDetails(Stream UdhaarDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadUdhaarDetails?Reg_Id={Reg_Id}")]
        List<UdhaarDetailsParameter> DownloadUdhaarDetails(string Reg_Id);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogFormDetails/CallLogForm")]
        List<CallLogFormDetailsParameter> UploadCallLogFormDetails(Stream CallLogForm);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCallLogFormDetails?CreatedBy={CreatedBy}")]
        List<CallLogFormDetailsParameter> DownloadCallLogFormDetails(string CreatedBy);

        //13/08/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFeedBackFormAswerDetails/FeedBackForm")]
        List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetails(Stream FeedBackForm);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFeedBackFormAnswerDetails?CustomerNumber={CustomerNumber}")]
        List<FeedBackFormNaswerParameter> DownloadFeedBackFormAnswerDetails(string CustomerNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendMessage?UserName={UserName}&Password={Password}&FilepathURL={FilepathURL}&receiverMobileNo={receiverMobileNo}&message={message}")]
        List<WatsappSendMessageParameter> SendMessage(string UserName, string Password, string FilepathURL, string receiverMobileNo,string message);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadUdharByDate?CreatedDateFrom={CreatedDateFrom}&CreatedDateTo={CreatedDateTo}")]
        List<UdhaarDetailsParameter> DownloadUdharByDate(string CreatedDateFrom, string CreatedDateTo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadUdharByCustomerName?CustomerName={CustomerName}&CreatedDateFrom={CreatedDateFrom}&CreatedDateTo={CreatedDateTo}")]
        List<UdhaarDetailsParameter> DownloadUdharByCustomerName(string CustomerName, string CreatedDateFrom, string CreatedDateTo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew5/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew5(Stream FormKeywordDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateCategoty?Day={Day}&Month={Month}&TemplateType={TemplateType}")]
        List<TemplateCategoryParameter> DownloadTemplateCategoty(string Day, string Month, string TemplateType);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateDetailsNew?ShopType={ShopType}&TemplateType={TemplateType}")]
        List<UploadTemplateParameter> DownloadTemplateDetailsNew(string ShopType, string TemplateType);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteUdhaar?ServerId={ServerId}")]
        List<UdhaarDetailsParameter> DeleteUdhaar(string ServerId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "NeedlyAppUpdateMobileSecurity/AppRegistration")]
        List<NeedlyRegiUpdateMobileSecurity> NeedlyAppUpdateMobileSecurity(Stream AppRegistration);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UploadFile?fileName={fileName}")]
        void UploadFile(string fileName, Stream stream);


        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadPurchasetemplate/purchaseTemplate")]
        List<ParameterPurchaseTemplate> UploadPurchasetemplate(Stream purchaseTemplate);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCountNew1/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCountNew1(Stream WhatsappCountDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadPurchasetemplate?TemplateId={TemplateId}&CreatedBy={CreatedBy}")]
        List<ParameterPurchaseTemplate> DownloadPurchasetemplate(string TemplateId, string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckIsAppInstalled/CheckApp")]
        List<CheckIsAppInstalledParameter> CheckIsAppInstalled(Stream CheckApp);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNeedlySpreadsheet/NeedlySpreadsheet")]
        List<NeedlySpreadsheetParameter> UploadNeedlySpreadsheet(Stream NeedlySpreadsheet);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlySpreadsheet?MobileNo={MobileNo}&Type={Type}")]
        List<NeedlySpreadsheetParameter> DownloadNeedlySpreadsheet(string MobileNo, string Type);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferalNotification/Referal")]
        List<UploadReferalNotification> UploadReferalNotification(Stream Referal);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTimeEvaluationReport/Report")]
        List<TimeEvaluationReportParameter> UploadTimeEvaluationReport(Stream Report);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTimeEvaluationReport?CreatedBy={CreatedBy}")]
        List<TimeEvaluationReportParameter> DownloadTimeEvaluationReport(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFeedBackFormAswerDetailsNew/FeedBackForm")]
        List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew(Stream FeedBackForm);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteCustomerContact?CreatedBy={CreatedBy}&FromSrno={FromSrno}&ToSrno={ToSrno}")]
        List<DeleterContactDetailsParameter> DeleteCustomerContact(string CreatedBy, string FromSrno, string ToSrno);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFeedBackFormAnswerDetailsNew?CustomerNumber={CustomerNumber}&CreatedBy={CreatedBy}&MaxId={MaxId}")]
        List<FeedBackFormNaswerParameter> DownloadFeedBackFormAnswerDetailsNew(string CustomerNumber, string CreatedBy, string MaxId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadKeyWordExcelData?MobileNo={MobileNo}&CreatedBy={CreatedBy}&Keyword={Keyword}")]
        List<KeywordExcelParameter> DownloadKeyWordExcelData(string MobileNo, string CreatedBy, string Keyword);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogFormDetailsNew/CallLogForm")]
        List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew(Stream CallLogForm);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryItemNew2/InventoryItem")]
        List<InventoryItem> UploadInventoryItemNew2(Stream InventoryItem);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNewQuotationDetails/QuotationDetails")]
        List<NewQuotationBasicDetailsParaMeters> UploadNewQuotationDetails(Stream QuotationDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNeedlyFormSpreadsheetData/FormSpreadsheetData")]
        List<FormSpreadsheetDataParameters> UploadNeedlyFormSpreadsheetData(Stream FormSpreadsheetData);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNewQuotationBasicDetails?CreatedBy={CreatedBy}")]
        List<NewQuotationBasicDetailsParaMeters> DownloadNewQuotationBasicDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCustomerWiseItemRate/CustomerWiseItemRate")]
        List<CustomerWiseItemRateParameter> UploadCustomerWiseItemRate(Stream CustomerWiseItemRate);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadLadiesSafetyHelpline?State={State}&District={District}&Taluka={Taluka}")]
        List<LadiesSafetyHelplineParaMeters> DownloadLadiesSafetyHelpline(string State, string District, string Taluka);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadRecordHeadingData?MarketingPersonNo={MarketingPersonNo}")]
        List<RecordHeadingParameter> DownloadRecordHeadingData(string MarketingPersonNo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadPincodeData?MarketingPersonNo={MarketingPersonNo}&Pincode={Pincode}&FromId={FromId}&ToId={ToId}")]
        List<PincodeDataParameter> DownloadPincodeData(string MarketingPersonNo, string Pincode, string FromId, string ToId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerWiseItemRate?CreatedBy={CreatedBy}")]
        List<CustomerWiseItemRateParameter> DownloadCustomerWiseItemRate(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadInventoryCategoryNew1/CategoryDetails")]
        List<InventoryCategoryDetails> UploadInventoryCategoryNew1(Stream CategoryDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadEmergencyNumber/EmergencyNumber")]
        List<EmergencyNumberParameter> UploadEmergencyNumber(Stream EmergencyNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEmergencyNumber?CreatedBy={CreatedBy}")]
        List<EmergencyNumberParameter> DownloadEmergencyNumber(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadLocationTracking/LocationTracking")]
        List<LocationTrackingParameter> UploadLocationTracking(Stream LocationTracking);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadLocationTracking?CreatedBy={CreatedBy}")]
        List<LocationTrackingParameter> DownloadLocationTracking(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEmergencyNumberByNo?Number={Number}")]
        List<EmergencyNumberParameter> DownloadEmergencyNumberByNo(string Number);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferalDetails3/ReferalDetails")]
        List<UploadReferalDatails> UploadReferalDetails3(Stream ReferalDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadEmergencyNumberNew/EmergencyNumber")]
        List<EmergencyNumberParameter> UploadEmergencyNumberNew(Stream EmergencyNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddStaffByAddUnder?CreatedBy={CreatedBy}&AddUnder={AddUnder}")]
        List<UploadAddStaff> DownloadAddStaffByAddUnder(string CreatedBy, string AddUnder);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReferaltableDetailsNew?Customer_Mobile={Customer_Mobile}")]
        List<DownloadReferral> DownloadReferaltableDetailsNew(string Customer_Mobile);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFeedBackFormAswerDetailsNew1/FeedBackForm")]
        List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew1(Stream FeedBackForm);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeteteGroupDetails/GroupDetails")]
        List<ParameterGroupDetails> DeteteGroupDetails(Stream GroupDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteEmergencyNumber/EmergencyNumber")]
        List<EmergencyNumberParameter> DeleteEmergencyNumber(Stream EmergencyNumber);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddStaffByNo?MobileNo={MobileNo}")]
        List<UploadAddStaff> DownloadAddStaffByNo(string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddStaffStatus?CreatedBy={CreatedBy}&MobileNo={MobileNo}")]
        List<UploadAddStaff1> DownloadAddStaffStatus(string CreatedBy, string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogCountDetails/CallLogCount")]
        List<CallLogCountDetailsParameter> UploadCallLogCountDetails(Stream CallLogCount);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCallLogCountDetails?CreatedBy={CreatedBy}")]
        List<CallLogCountDetailsParameter> DownloadCallLogCountDetails(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAduiFileDetails/AduiFile?AduiFile={AduiFile}")]
        List<AudioFileParameter> UploadAduiFileDetails(string AduiFile, Stream stream);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAudioFileDetails/AudioFile?AudioFile={AudioFile}")]
        List<AudioFileParameter> UploadAudioFileDetails(string AudioFile, Stream stream);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGenerateCouponDetails?ProjectName={ProjectName}&LicenseType={LicenseType}&MobileNo={MobileNo}")]
        List<GenerateCouponCodeParameter> DownloadGenerateCouponDetails(string ProjectName, string LicenseType, string MobileNo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCouponDistributeCode/CouponDistributeCode")]
        List<CouponDistributedParameter> UploadCouponDistributeCode(Stream CouponDistributeCode);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTrueVoterUserDetails/UserDetails")]
        List<TrueVoterUserParameter> UploadTrueVoterUserDetails(Stream UserDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogFormDetailsNew1/CallLogForm")]
        List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew1(Stream CallLogForm);
        
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFeedBackFormAswerDetailsNew2/FeedBackForm")]
        List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew2(Stream FeedBackForm);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCountNew2/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCountNew2(Stream WhatsappCountDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCountNew3/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCountNew3(Stream WhatsappCountDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadTemplateShare/TemplateShare")]
        List<TemplateShareParameter> UploadTemplateShare(Stream TemplateShare);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateShare?CreatedBy={CreatedBy}")]
        List<TemplateShareParameter> DownloadTemplateShare(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCountNew4/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCountNew4(Stream WhatsappCountDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCouponDistributeCode1/CouponDistributeCode")]
        List<CouponDistributionPara> UploadCouponDistributeCode1(Stream CouponDistributeCode);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDistributedCoupon?CreatedBy={CreatedBy}")]
        List<DistributedCoupon> DownloadDistributedCoupon(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCompanyDetails1/CompanyDetails")]
        List<CompanyDetailsParameter> UploadCompanyDetails1(Stream CompanyDetails);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "CheckWebsiteAvailability?WebsiteName={WebsiteName}")]
        List<WebsiteAvilableParameter> CheckWebsiteAvailability(string WebsiteName);

        //11/12/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCardLinkDetails1/CardLinkDetails")]
        List<CardLinkParameter> UploadCardLinkDetails1(Stream CardLinkDetails);

        //14/12/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadEmployeedataDetails/employee")]
        List<EmployeedataParameter> UploadEmployeedataDetails(Stream employee);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEmployeedatadetails?role={role}")]
        List<EmployeedataParameter> DownloadEmployeedatadetails(string role);

        //20/12/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateDetailsNew1?CategoryId={CategoryId}&TemplateType={TemplateType}&ShopType={ShopType}")]
        List<UploadTemplateParameter> DownloadTemplateDetailsNew1(string CategoryId, string TemplateType, string ShopType);

        //23/12/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogCountDetailsNew/CallLogCount")]
        List<CallLogCountDetailsParameter> UploadCallLogCountDetailsNew(Stream CallLogCount);

        //24/12/2021
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCallLogCountDetailsNew?CreatedBy={CreatedBy}&Date={Date}")]
        List<CallLogCountDetailsParameter> DownloadCallLogCountDetailsNew(string CreatedBy, string Date);

        //28/12/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogFormDetailsNew11/CallLogForm")]
        List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew11(Stream CallLogForm);

        //28/12/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "SendnotificationTrueVoter/notification")]
        List<truevoterparameter> SendnotificationTrueVoter(Stream notification);
        //30/12/2021
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFeedBackFormAswerDetailsNew3/FeedBackForm")]
        List<FeedBackFormNaswerParameter> UploadFeedBackFormAswerDetailsNew3(Stream FeedBackForm);

        //04/01/2022
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAllocateCouponDetails?ProjectName={ProjectName}&CoupanCodeType={CoupanCodeType}&MobileNo={MobileNo}&UseType={UseType}&RechargePeriod={RechargePeriod}")]
        List<GenerateAllocatecouponCodeParameter> DownloadAllocateCouponDetails(string ProjectName, string CoupanCodeType, string MobileNo, string UseType, string RechargePeriod);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadImeiNo?MobileNo={MobileNo}&keyword={keyword}")]
        List<GenerateAllocatecouponCodeParameter> DownloadImeiNo(string MobileNo, string Keyword);

        //05/01/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCouponAllocateCode/CouponAllocateCode")]
        List<CouponAllocatePara> UploadCouponAllocateCode(Stream CouponAllocateCode);
        //08/01/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReferncesMobile1/RefernceMobile")]
        List<RegstrationRefParameter> UploadReferncesMobile1(Stream RefernceMobile);

        //12/01/2022
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadRechargeWallet2?coupencode={coupencode}&Keyword={Keyword}")]
        List<UploadRechargeWallet> DownloadRechargeWallet2(string coupencode, string Keyword);

        //15/01/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWhatsAppCountNew5/WhatsappCountDetails")]
        List<WhatsappCountParameter> UploadWhatsAppCountNew5(Stream WhatsappCountDetails);

        //24/01/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNewsAndMedia/NewsMedia")]
        List<ImageGalleryParameter> UploadNewsAndMedia(Stream NewsMedia);

        //25/01/22

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCompanyDetails2/CompanyDetails")]
        List<CompanyDetailsParameter2> UploadCompanyDetails2(Stream CompanyDetails);



        //27/01/22
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadTemplateDetails1")]
        List<TemplateParameter> DownloadTemplateDetails1();


        //28/01/22

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadWebsiteDetails/website")]
        List<WebsiteDetails> UploadWebsiteDetails(Stream website);



        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadWebsiteDetails?CreatedBy={CreatedBy}")]
        List<WebsiteDetails> DownloadWebsiteDetails(string CreatedBy);

        //31/01/22
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNewsAndMedia?CreatedBy={CreatedBy}")]
        List<ImageGalleryParameter> DownloadNewsAndMedia(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadImageGallery?CreatedBy={CreatedBy}")]
        List<ImageGalleryParameter> DownloadImageGallery(string CreatedBy);

        //03/02/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DeleteGroupofGroupDetails/ServerId")]
        List<DeleteGroupofGroupDetailsparameter> DeleteGroupofGroupDetails(Stream ServerId);

        //08/02/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadSocialLinksNew/SocialLinksDetails")]
        List<SocialLinksDetailsParameter> UploadSocialLinksNew(Stream SocialLinksDetails);

        //09/01/22
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVisitCount?CreatedBy={CreatedBy}")]
        List<VisitCountParameter> DownloadVisitCount(string CreatedBy);

        //10/02/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProjectMemberNew2/ProjectMemberDetails")]
        List<ParameterProjectMember> UploadProjectMemberNew2(Stream ProjectMemberDetails);


        //11/02/22

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEmail?mobileno={mobileno}&keyword={keyword}")]
        List<DownloadEmail> DownloadEmail(string mobileno, string keyword);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerEnquiry?CreatedBy={CreatedBy}")]
        List<DownloadEnquiry> DownloadCustomerEnquiry(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCustomerFeedback?CreatedBy={CreatedBy}")]
        List<DownlaodCustomerFeedbackparameter> DownloadCustomerFeedback(string CreatedBy);



        //12/02/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadContactDetailsNew/ContactDetails")]
        List<CustomerContactDetailsParameter> UploadContactDetailsNew(Stream ContactDetails);

        //15/02/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReportdata/Reportdata")]
        List<Reportdataparameter> UploadReportdata(Stream Reportdata);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReportData?CreatedBy={CreatedBy}")]
        List<Reportdataparameter> DownloadReportData(string CreatedBy);

        //16/02/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Uploadfieldaliasdata/fieldaliasdata")]
        List<fieldaliasdataparameter> Uploadfieldaliasdata(Stream fieldaliasdata);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Downloadfieldaliasdata?CreatedBy={CreatedBy}&FormId={FormId}")]
        List<fieldaliasdataparameter> Downloadfieldaliasdata(string CreatedBy, string FormId);

        //17/02/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormstructuredata/formstructuredata")]
        List<formstructuredataparameter> UploadFormstructuredata(Stream formstructuredata);
        
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormstructuredata?reportid={reportid}")]
        List<formstructuredataparameter> DownloadFormstructuredata(string reportid);

        //18/02/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadVoterUpdateCounts/VoterUpdateCounts")]
        List<VoterUpdateCounts> UploadVoterUpdateCounts(Stream VoterUpdateCounts);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReportData1?ProjectType={ProjectType}")]
        List<Reportdataparameter> DownloadReportData1(string ProjectType);


        //21/02/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadVoterUpdateCounts1/VoterUpdateCounts")]
        List<VoterUpdateCounts> UploadVoterUpdateCounts1(Stream ProjectMemberDetails);

        //22/02/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadVoterUpdateCounts?candidateMobile={candidateMobile}")]
        List<VoterUpdateCounts> DownloadVoterUpdateCounts(string candidateMobile);

        //25/02/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadReportdata1/Reportdata")]
        List<Reportdataparameter> UploadReportdata1(Stream Reportdata);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReportDataNew?CreatedBy={CreatedBy}&Category={Category}")]
        List<Reportdataparameter> DownloadReportDataNew(string CreatedBy, string Category);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadReportDataNew1?ProjectType={ProjectType}&Category={Category}")]
        List<Reportdataparameter> DownloadReportDataNew1(string ProjectType, string Category);

        //03/03/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupPostData/GroupPost")]
        List<Grouppostparameter> UploadGroupPostData(Stream GroupPost);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupPostData?GroupId={GroupId}")]
        List<Grouppostparameter> DownloadGroupPostData(string GroupId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupChatData/GroupChat")]
        List<Groupchatparameter> UploadGroupChatData(Stream GroupChat);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupChatData?GroupId={GroupId}")]
        List<Groupchatparameter> DownloadGroupChatData(string GroupId);

        //08/03/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew55/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew55(Stream FormKeywordDetails);

        //14/03/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupPostDataNew?GroupId={GroupId}&ApproveStatus={ApproveStatus}")]
        List<Grouppostparameter> DownloadGroupPostDataNew(string GroupId, string ApproveStatus);

        //14/03/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCompanyDetails3/CompanyDetails")]
        List<CompanyDetailsParameter2> UploadCompanyDetails3(Stream CompanyDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupPostDataNew/GroupPost")]
        List<Grouppostparameter1> UploadGroupPostDataNew(Stream GroupPost);

        //17/03/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew555/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew555(Stream FormKeywordDetails);

        //23/03/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadGroupPostDataNew1/GroupPost")]
        List<Grouppostparameter> UploadGroupPostDataNew1(Stream GroupPost);

        //28/03/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadFormKeywordsNew?ServerId={ServerId}")]
        List<ParameterFormKeyword> DownloadFormKeywordsNew(string ServerId);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadDynamicReportdata/Categoryname")]
        List<DynamicReportparameter> UploadDynamicReportdata(Stream Categoryname);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDynamicReportdata")]
        List<DynamicReportparameter> DownloadDynamicReportdata();

        //04/04/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupPostDataNew1?GroupId={GroupId}&MaxId={MaxId}")]
        List<Grouppostparameter> DownloadGroupPostDataNew1(string GroupId, string MaxId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupChatDataNew?GroupId={GroupId}&MaxId={MaxId}")]
        List<Groupchatparameter> DownloadGroupChatDataNew(string GroupId, string MaxId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGroupPostDataNew11?GroupId={GroupId}&ApproveStatus={ApproveStatus}&MaxId={MaxId}")]
        List<Grouppostparameter> DownloadGroupPostDataNew11(string GroupId, string ApproveStatus, string MaxId);

        //09/04/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadNeedlySOSPromotionData/NeedlySOSPromotion")]
        List<NeedlySOSPromotionparameter> UploadNeedlySOSPromotionData(Stream NeedlySOSPromotion);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlySOSPromotionData?DynamicURL={DynamicURL}")]
        List<NeedlySOSPromotionparameter> DownloadNeedlySOSPromotionData(string DynamicURL);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlySOSPromotionDataNew?Mobilenumber={Mobilenumber}")]
        List<NeedlySOSPromotionparameter> DownloadNeedlySOSPromotionDataNew(string Mobilenumber);


        //13/04/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadEductionalDetails/eductionaldetail")]
        List<EductionDetails> UploadEductionalDetails(Stream eductiondetail);

        //14/04/22
        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEductionalDetails?MobileNo={MobileNo}")]
        List<DownloadEductionalDetails> DownloadEductionalDetails(string MobileNo);

        //14/04/2022
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadJobRegistration/JobRegistration")]
        List<JobRegistrationParameter> UploadJobRegistration(Stream JobRegistration);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJobRegistration?CreatedBy={CreatedBy}")]
        List<JobRegistrationParameter> DownloadJobRegistration(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadSelfEmploymentBusiness/SelfEmploymentBusiness")]
        List<SelfEmploymentBusinessdataparameter> UploadSelfEmploymentBusiness(Stream SelfEmploymentBusiness);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadSelfEmploymentBusiness?CreatedBy={CreatedBy}")]
        List<SelfEmploymentBusinessdataparameter> DownloadSelfEmploymentBusiness(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadAgricultureRegistrationdata/AgricultureRegistrationdata")]
        List<AgricultureRegistrationdataparameter> UploadAgricultureRegistrationdata(Stream AgricultureRegistrationdata);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAgricultureRegistrationdata?CreatedBy={CreatedBy}")]
        List<AgricultureRegistrationdataparameter> DownloadAgricultureRegistrationdata(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew5/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew5(Stream ProfileData);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Uploadworkdetaildata/workdetail")]
        List<workdetailparameter> Uploadworkdetaildata(Stream workdetail);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Downloadworkdetaildata?USERMOBILE={USERMOBILE}")]
        List<workdetailparameter> Downloadworkdetaildata(string USERMOBILE);

        //16/04/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadSectorWiseScheme/sectorwise")]
        List<SectorWiseSchme> UploadSectorWiseScheme(Stream sectorwise);



        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadSectorWiseScheme?MobileNo={MobileNo}")]
        List<DownloadSector> DownloadSectorWiseScheme(string MobileNo);



        //19/04/22
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadDemandJob/demanjob")]
        List<DemandJobParameter> UploadDemandJob(Stream demanjob);




        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadDemandjob?CreatedBy={CreatedBy}")]
        List<DemandJobParameter> DownloadDemandjob(string CreatedBy);

        //19/04/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJobCategorydata?")]
        List<JobCategoryparameter> DownloadJobCategorydata();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Downloadjobtypedata?CategoryId={CategoryId}")]
        List<jobtypeparameter> Downloadjobtypedata(string CategoryId);



        //22/04/22

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadRegularjob?type={type}&job_type={job_type}&category={category}&state={state}&district={district}&taluka={taluka}")]
        List<DemandJobParameter> DownloadRegularjob(string type, string job_type, string category, string state, string district, string taluka);


        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Downloadsectorjob?type={type}&sector={sector}&category={category}&state={state}&district={district}&taluka={taluka}&jobrole={jobrole}")]
        List<DemandJobParameter> Downloadsectorjob(string type, string sector, string category, string state, string district, string taluka, string jobrole);

        //25/04/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Uploadassigntaskdata/assigntask")]
        List<assigntaskdataparameter> Uploadassigntaskdata(Stream assigntask);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Downloadassigntaskdata?")]
        List<assigntaskdataparameter> Downloadassigntaskdata();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadassigntaskdataNew?OfficeName={OfficeName}&DepartmentName={DepartmentName}&StaffName={StaffName}&FromDate={FromDate}&ToDate={ToDate}&Status={Status}")]
        List<assigntaskdataparameter> DownloadassigntaskdataNew(String OfficeName, string DepartmentName, string StaffName, string FromDate, string ToDate, string Status);

        //26/04/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadEzeeClassProfile/EzeeClassProfile")]
        List<UploadEzeeClassProfile> UploadEzeeClassProfile(Stream EzeeClassProfile);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadEzeeClassProfile?MobileNo={MobileNo}")]
        List<UploadEzeeClassProfile> DownloadEzeeClassProfile(string MobileNo);

        //27/04/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadassigntaskdataNew1?CreatedBy={CreatedBy}")]
        List<assigntaskdataparameter> DownloadassigntaskdataNew1(string CreatedBy);

        //29/04/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadassigntaskdataNew/assigntask")]
        List<assigntaskdataparameter> UploadassigntaskdataNew(Stream assigntask);

        //30/04/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadLadiesSafetyHelplineNew?")]
        List<LadiesSafetyHelplineParaMeters> DownloadLadiesSafetyHelplineNew();

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadassigntaskdataNew11?FromDate={FromDate}&ToDate={ToDate}&TaskStatus={TaskStatus}&CreatedBy={CreatedBy}")]
        List<assigntaskdataparameter> DownloadassigntaskdataNew11(string FromDate, string ToDate, string TaskStatus, string CreatedBy);

        //02/05/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddStaffDetails?MobileNo={MobileNo}")]
        List<UploadAddStaff> DownloadAddStaffDetails(string MobileNo);

        //04/05/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadassigntaskdataonTaskCategory?TaskCategory={TaskCategory}")]
        List<assigntaskdataparameter> DownloadassigntaskdataonTaskCategory(String TaskCategory);

        //06/05/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadassigntaskdataonBasis?TaskCategory={TaskCategory}&OfficeName={OfficeName}&DepartmentName={DepartmentName}&StaffName={StaffName}&PostName={PostName}&MaxId={MaxId}")]
        List<assigntaskdataparameter> DownloadassigntaskdataonBasis(String TaskCategory, string OfficeName, string DepartmentName, string StaffName, string PostName,string MaxId);

        //09/05/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCreateGroupNew1/CreateGroup")]
        List<UploadCreateGroup> UploadCreateGroupNew1(Stream CreateGroup);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadProfileupdateDataNew6/ProfileData")]
        List<NeedlyRegistrationDetail> UploadProfileupdateDataNew6(Stream ProfileData);

        //10/05/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJsonSendOtpNew?UserMobile={UserMobile}&RefMobile={RefMobile}&IMEINumber={IMEINumber}&AppKeyword={AppKeyword}&UserType={UserType}")]
        List<UploadSendOTPParameter> DownloadJsonSendOtpNew(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string UserType);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadGrpdetails?MobileNo={MobileNo}")]
        List<UploadCreateGroup> DownloadGrpdetails(String MobileNo);
       
        //13/05/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "NeedlyAppRegistrationNew/AppRegistration")]
        List<NeedlyRegistrationDetail> NeedlyAppRegistrationNew(Stream AppRegistration);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCreateGroupNew11/CreateGroup")]
        List<UploadCreateGroup> UploadCreateGroupNew11(Stream CreateGroup);

        //19/05/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Uploadnotificationreportdata/notificationreport")]
        List<notificationreportparameter> Uploadnotificationreportdata(Stream notificationreport);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "Downloadnotificationreportdata?CreatedBy={CreatedBy}")]
        List<notificationreportparameter> Downloadnotificationreportdata(string CreatedBy);

        //24/05/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadAddStaffByNoNew?MobileNo={MobileNo}&AddUnder={AddUnder}")]
        List<UploadAddStaff> DownloadAddStaffByNoNew(string MobileNo, string AddUnder);

        //27/05/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCallLogFormDetailsNew111/CallLogForm")]
        List<CallLogFormDetailsParameter> UploadCallLogFormDetailsNew111(Stream CallLogForm);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadCallLogFormDetailsNew?Groupid={Groupid}")]
        List<CallLogFormDetailsParameter> DownloadCallLogFormDetailsNew(string Groupid);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadMemberExtraQuestionAnswer/MemberQuestions")]
        List<MemberExtraQuestionAnswerParameter> UploadMemberExtraQuestionAnswer(Stream MemberQuestions);

        //08/06/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCreateGroupNew111/CreateGroup")]
        List<UploadCreateGroup> UploadCreateGroupNew111(Stream CreateGroup);

        //09/06/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadMemberGroupNew/MemberGroup")]
        List<UploadMemberGroup> UploadMemberGroupNew(Stream MemberGroup);

        //16/06/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadJsonReSendOtp?UserMobile={UserMobile}&RefMobile={RefMobile}&IMEINumber={IMEINumber}&AppKeyword={AppKeyword}&UserType={UserType}")]
        List<UploadSendOTPParameter> DownloadJsonReSendOtp(string UserMobile, string RefMobile, string IMEINumber, string AppKeyword, string UserType);

        //17/06/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadCalllogHistory/CallLogHistory")]
        List<UploadCalllogHistoryparameter> UploadCalllogHistory(Stream CallLogHistory);

        //29/06/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadJobApplicationDetailsNew/JobAppDataDetails")]
        List<UploadJobApplicationDetailsParameter> UploadJobApplicationDetailsNew(Stream JobAppDataDetails);

        //19/07/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadKeywordExcelUploadHeading?MobileNo={MobileNo}&Category={Category}")]
        List<Uploadkeyworddetails> DownloadKeywordExcelUploadHeading(string MobileNo, string Category);

        //22/07/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadQuestionAnswer?GroupId={GroupId}&CreatedBy={CreatedBy}&QuestionType={QuestionType}")]
        List<MemberExtraQuestionAnswerParameter> DownloadQuestionAnswer(string GroupId, string CreatedBy, string QuestionType);

        //23/07/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadQuestionAnsweronthebasiscustomer?GroupId={GroupId}&CustomerNo={CustomerNo}&QuestionType={QuestionType}")]
        List<MemberExtraQuestionAnswerParameter> DownloadQuestionAnsweronthebasiscustomer(string GroupId, string CustomerNo, string QuestionType);

        //26/07/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseGroup/ExpenseGroup")]
        List<UploadExpenseGroupParameter> UploadExpenseGroup(Stream ExpenseGroup);

        //27/07/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseHead/ExpenseHead")]
        List<UploadExpenseGroupParameter> UploadExpenseHead(Stream ExpenseHead);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseSubHead/ExpenseSubHead")]
        List<UploadExpenseGroupParameter> UploadExpenseSubHead(Stream ExpenseSubHead);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExpenseDetails?")]
        List<UploadExpenseGroupParameter> DownloadExpenseDetails();


        //28/07/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlyFormSpreadsheetData?FormId={FormId}")]
        List<FormSpreadsheetDataParameters> DownloadNeedlyFormSpreadsheetData(string FormId);

        //29/07/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExpenseHeadCount?GroupName={GroupName}&CreatedBy={CreatedBy}")]
        List<UploadExpenseGroupParameter> DownloadExpenseHeadCount(string GroupName, string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateExpenseDetails/ExpenseSubHead")]
        List<UploadExpenseGroupParameter> UpdateExpenseDetails(Stream ExpenseSubHead);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExpenseAmountCount?GroupName={GroupName}&Category={Category}&HeadName={HeadName}")]
        List<UploadExpenseGroupParameter> DownloadExpenseAmountCount(string GroupName, string Category, string HeadName);

        //01/08/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadFormKeywordNew5555/FormKeywordDetails")]
        List<ParameterFormKeyword> UploadFormKeywordNew5555(Stream FormKeywordDetails);

        //02/08/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadNeedlyFormSpreadsheetDataWithColumn?FormId={FormId}&ColumnName={ColumnName}&ColumnValue={ColumnValue}")]
        List<FormSpreadsheetDataParameters> DownloadNeedlyFormSpreadsheetDataWithColumn(string FormId, string ColumnName, string ColumnValue);


        //06/08/2022

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExpenseGroup?CreatedBy={CreatedBy}")]
        List<UploadExpenseGroupParameter> DownloadExpenseGroup(string CreatedBy);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExpenseHead?CreatedBy={CreatedBy}&ParentId={ParentId}")]
        List<UploadExpenseGroupParameter> DownloadExpenseHead(string CreatedBy, string ParentId);

        [OperationContract]
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "DownloadExpenseSubHead?CreatedBy={CreatedBy}&ParentId={ParentId}")]
        List<UploadExpenseGroupParameter> DownloadExpenseSubHead(string CreatedBy, string ParentId);

        //05/09/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseGroupDetails/ExpenseGroup")]
        List<UploadExpenseGroupParameter> UploadExpenseGroupDetails(Stream ExpenseGroup);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseHeadDetails/ExpenseHead")]
        List<UploadExpenseGroupParameter> UploadExpenseHeadDetails(Stream ExpenseHead);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseSubHeadDetails/ExpenseSubHead")]
        List<UploadExpenseGroupParameter> UploadExpenseSubHeadDetails(Stream ExpenseSubHead);

        //12/09/2022

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "UploadExpenseGroupDetailsNew/ExpenseGroup")]
        List<UploadExpenseGroupParameter> UploadExpenseGroupDetailsNew(Stream ExpenseGroup);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "")]
        List<StudentInfo> UploadStudentInfo(Stream StudentInfo);











    }

}
