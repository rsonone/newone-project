﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="AddCategory1.aspx.cs" Inherits="NeedlyApp.Admin.AddCategory1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add Category<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select ShopType: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlShop" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Category Name: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtCatName" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>
               </div></div> 
       
             
                <div class="col-md-12">
                     <div class="box-body col-sm-5">
                        
                    </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" OnClick="btnUpdate_Click" />
                        </div>
                 </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddCat"  runat="server" OnPageIndexChanging="GVAddCat_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                     <%--   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     --%>
                         <asp:BoundField DataField="Id" HeaderText="Server ID" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="CategoryName" HeaderText="CategoryName">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ShopType" HeaderText="ShopType">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="CreatedDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                       
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                         </div>
                    </div>
                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>

