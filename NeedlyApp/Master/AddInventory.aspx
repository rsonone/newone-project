﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddInventory.aspx.cs" Inherits="NeedlyApp.Master.AddInventory" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Add Inventory<small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                                  <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                                          <asp:RadioButtonList ID="rdSelect" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Add Product" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="Add Category" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="Add Combos" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    </div>
               </div>
                                      </div>
                  <asp:Panel ID="pnlStaff" runat="server">
                <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Item Name : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtItem" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                    Unit : </label>
                               <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlUnit" runat="server" AutoPostBack="true" CssClass="form-control" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />        </div></div></div>

        </div> </div></div> 

                         <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Rate : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtRate" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                           <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Company Brand : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtbrand" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

        </div> </div></div> 

                           <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Offer Price : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtoffer" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                           <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Discount : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtDiscountproduct" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

        </div> </div></div> 

                <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                                        <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    In Stock : </label>
                                   <div class="col-sm-9">
                                   <asp:RadioButtonList ID="ddlStock" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>     </div></div></div>
                          
                               <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                   Product Image: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload2" runat="server" />
                          </div></div></div>

                        </div>
                    </div>
                </div>

                         <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                    <div class="box-body col-sm-6">
                    <div class="box-body col-sm-offset-4  col-sm-8">
                           <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." Font-Bold="true" Font-Size="Medium"  ForeColor="Red"></asp:LinkButton>
                          </div>
                        </div>
                    </div>
              </div>
              </div>

                      </asp:Panel>
                       <asp:Panel ID="pnCategory" runat="server">
                       <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Category Name : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtCategory" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                    Product List : </label>
                               <div class="col-sm-9">
                               <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="true" CssClass="form-control" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />                                     </div></div></div>

        </div> </div></div> 

                              <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                                        <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    In Stock : </label>
                                   <div class="col-sm-9">
                                   <asp:RadioButtonList ID="rdCatStock" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>     </div></div></div>
                          
                               <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                   Product Image: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload3" runat="server" />
                          </div></div></div>

                        </div>
                    </div>
                </div>

                    
                           </asp:Panel>

                 <asp:Panel ID="pnlCombos" runat="server">
                       <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Combo Name : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtCombo" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                    Rate : </label>
                               <div class="col-sm-9">
                                   <asp:TextBox ID="txtComRate" CssClass="form-control" runat="server"></asp:TextBox>
                                   </div></div></div>

        </div> </div></div> 




                       <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Company Brand: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtCompany" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                    Product List : </label>
                               <div class="col-sm-9">
                                     <asp:DropDownList ID="ddlProductlist" runat="server" AutoPostBack="true" CssClass="form-control" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />                                       </div></div></div>

        </div> </div></div> 


                      <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Offer Price : </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtOfferPrice" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                    Discount (%) : </label>
                               <div class="col-sm-9">
                                   <asp:TextBox ID="txtDiscount" CssClass="form-control" runat="server"></asp:TextBox>
                                   </div></div></div>

        </div> </div></div> 

                                <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                                        <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    In Stock : </label>
                                   <div class="col-sm-9">
                                   <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>     </div></div></div>
                          
                               <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                   Combo Image: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload4" runat="server" />
                          </div></div></div>

                        </div>
                    </div>
                </div>


                   

                           </asp:Panel>
                


                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="BtnSave" runat="server" class="btn btn-success"  Text="Save" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancle" runat="server" class="btn btn-success"  Text="Reset" />&nbsp;&nbsp;

                 <%--       <asp:Button ID="btnExcel" runat="server" class="btn btn-success" OnClick="btnExcel_Click" Text="Export To Excel" />--%>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </div>
                </div>

                 <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label4" runat="server">Count:</asp:Label>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
                  <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report"  runat="server"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:BoundField DataField="FullName" HeaderText="FullName">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="firmName" HeaderText="Firm Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="mobileNo" HeaderText="Mobile No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="EntryDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="State" HeaderText="State">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="District" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="UserRole" HeaderText="UserRole">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
