﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalJobPost.aspx.cs" MasterPageFile="~/MasterPage/NeedlyMaster.Master" Inherits="NeedlyApp.Master.LocalJobPost" %>


<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Local Job Post<small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                  <asp:Panel ID="pnlStaff" runat="server">
                 
                <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                       Job Category:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlJob" runat="server" AutoPostBack="true" CssClass="form-control" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>

                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Job Type:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="form-control" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Job Title:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtJob" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>

                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                       Salary in Range:
                                    </label>
                                    <div class="col-sm-9">
                                         <asp:TextBox ID="txtSalary" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                              <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                           <%-- <div class="box-body col-sm-12">--%>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Job Discription:
                                    </label>
                                    <div class="col-sm-9">
                                          <asp:TextBox ID="txtMonthly" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                <%--</div>--%>
                            </div>

                        </div>
                    </div>
                </div>

            <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Contact No:
                                    </label>
                                    <div class="col-sm-9">
                                          <asp:TextBox ID="txtContact" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>

                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                     Contact Name:
                                   
                                    <div class="col-sm-9">
                                       <asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                          <div class="row">
                    <div class="panel-body">


                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        WhatsApp No:
                                    </label>
                                    <div class="col-sm-9">
                                          <asp:TextBox ID="txtWhatsApp" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>

                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                       Business Name:
                                    </label>
                                    <div class="col-sm-9">
                                         <asp:TextBox ID="txtBusiness" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                                   <div class="box-body col-sm-6">
                                <div class="form-group">
                                        Address:
                                   
                                    <div class="col-sm-9">
                                          <asp:TextBox ID="txtAdd" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>

                                 <div class="box-body col-sm-6">
                                <div class="form-group">
                                        Landmark:
                                   
                                    <div class="col-sm-9">
                                          <asp:TextBox ID="txtLandmark" CssClass="form-control" runat="server"></asp:TextBox>
                     
                                    </div>
                                </div>
                            </div>
                       
                        </div>
                    </div>
                </div>


                      </asp:Panel>
                 
                   <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                    <div class="box-body col-sm-6">
                    <div class="box-body col-sm-offset-4  col-sm-8">
                           <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." Font-Bold="true" Font-Size="Medium"  ForeColor="Red"></asp:LinkButton>
                          </div>
                        </div>
                    </div>
              </div>
              </div>



                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="BtnSave" runat="server" class="btn btn-success"  Text="Save" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancle" runat="server" class="btn btn-success"  Text="Reset" />&nbsp;&nbsp;

                 <%--       <asp:Button ID="btnExcel" runat="server" class="btn btn-success" OnClick="btnExcel_Click" Text="Export To Excel" />--%>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </div>
                </div>

                 <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label4" runat="server">Count:</asp:Label>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
                  <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report"  runat="server"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:BoundField DataField="ClientName" HeaderText="Client Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ServiceDepartment" HeaderText="Service Department">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Post" HeaderText="Post">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="StaffName" HeaderText="Staff Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Shift" HeaderText="Shift">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Posting" HeaderText="Posting">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <%--<asp:BoundField DataField="UserRole" HeaderText="UserRole">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>--%>
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
