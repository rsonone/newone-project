﻿using System;
using DAL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NeedlyApp.Static
{
    public partial class ShopLink : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            DataFieldLoad();
        }

        public void DataFieldLoad()
        {
            try
            {
                //    SqlCommand cmd = new SqlCommand("sp_ShowData");
                SqlCommand cmd = new SqlCommand("sp_ShowDataimages");

                cmd.Connection = con;
                //  con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Parameters.AddWithValue("@Id", ViewState["Id"].ToString());
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);



               if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                    {
                        string Image = Convert.ToString(ds.Tables[0].Rows[r]["Image"]);
                        string title = Convert.ToString(ds.Tables[0].Rows[r]["title"]);
                        string helpkeyID = Convert.ToString(ds.Tables[0].Rows[r]["helpkeyID"]);
                        string Id = Convert.ToString(ds.Tables[0].Rows[r]["ID"]);

                        string Html = string.Empty;
                        string Idd = string.Empty;

                            if (Image != "" && Image != null)
                        {
                            Html = "<div class='catalog-card-view' >" +
                             "<div class='catalog-item-container'>" +
                               " <div class='item-card'>" +
                                 //"<label id='lbl"+Id+" Text='"+ Id + "'> </label >"+
                                 "<asp:Label ID = 'lbl" + Id + "' runat = 'server' Text = 'A single int' ></asp:Label >"+
                                           "  <div class='item-card__head' style='background-image: url(" + Image + ");'></div>" +
                                   "<div class='item-card__body'>" +
                                     " <div class='item-card__name' id='ItemName"+ Id + "'> " + title + "</div>" +
                                      "<div class='item-card__box' style ='justify-content: space-between;'>" +
                                        " <div class='item-card__amount' id='ItemID" + Id + "' >₹ " + helpkeyID + ".00 </div>" +
                                        " <div class='item-card__addCta'>" +
                                           " <div class='addtocart-btn' style='background-color: rgb(255, 255, 255);'>" +
                                          //    "  <div class='addtocart-btn__changeIcon'><span>ADD</span><img src ='" + Image + "' alt='Add Item button'></div>" +
                                          //<%= lbl"+Id+".ClientID %> onclick='functions();'
                                          "         <div class='addtocart-btn__changeIcon'  id='btn" + Id + "'><span>ADD</span><img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC' alt='Add Item button'></div>" +
                                             " </div>" +
                                        " </div>" +
                                     " </div>" +
                                  " </div>" +
                               " </div>" +
                            " </div>" +
                         " </div>";
                        }

                        else
                        {
                            Html = "<div class='catalog-view-gap'></div>" +
                        "<div class='catalog-list-view'>" +
                           "<div class='catalog-item-container'>" +
                            "  <div class='catalog-item__list'>" +
                                 "<div class='catalog-item__list--left'>" +
                                  "  <div class='catalog-item__list--left-name'>" + title + "</div>" +
                                 "   <div class='catalog-item__list--left-amount'>₹ " + helpkeyID + ".00 </div>" +
                                " </div>" +
                                 "<div class='catalog-item__list--right'>" +
                                   "<div class='addtocart-btn' style='background-color: rgb(255, 255, 255);'>" +
                                       "<div class='addtocart-btn__changeIcon'><img src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAFCAYAAABFA8wzAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAABQAAAADMlWz1AAAAGUlEQVQYGWN8FRj4n4GKgImKZoGNGoEGAgAJJgKVJxPQJQAAAABJRU5ErkJggg==' alt='Decreament button'>" +

                                       "</div>" +
                                       "<div class='addtocart-btn__text'>1" +

                                       "</div>" +
                                       "<div class='addtocart-btn__changeIcon'>" +
                                       "    <img src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC' alt='Increment button'>" +
"</div></div>" +
                               "  </div>" +
                              "</div>" +
                           "</div>";
                            
                        }

                        //if (Idd == "")
                        //{
                        //    Idd = "#" + Id;
                        //}
                        //else {
                        //    Idd = Idd + ",#" + Id;
                        //}

                        //Html = Html + "<Script> debugger; var items = document.querySelectorAll('" + Idd + "');'" +
                        //" for (var i = 0; i < items.length; i++)" +
                        //"{" +
                        //    "items[i].onclick = function() {" +
                        //        "this.innerText = this.innerText + ' 11';" +
                        //    "};" +
                        //"}";
                        

                        PlaceHolder2.Controls.Add(new Literal() { ID = "literal", Text = Html });

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}