﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopLink.aspx.cs" Inherits="NeedlyApp.Static.ShopLink" %>

<!DOCTYPE html>

<html lang="en">
   <head>

      <meta charset="utf-8">
      <link rel="icon" href="/logo-new.png">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="apple-touch-icon" href="/logo-new.png">
      <link rel="manifest" href="/manifest.json">
   
      <meta name="description" content="Now order online from my whatsapp store &amp; pay using 150+ UPI Apps">
      <meta property="og:title" content="Order Now">
      <meta property="og:image" content="/home/ubuntu/digitaldukancfe/build/logo-new.png">
      <meta property="og:url" content="$OG_URL">
      <script async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="https://d2r1yp2w7bby2u.cloudfront.net/js/a.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-K93GVKM"></script><script>!function(e,t,a,n,g){e[n]=e[n]||[],e[n].push({"gtm.start":(new Date).getTime(),event:"gtm.js"});var m=t.getElementsByTagName(a)[0],r=t.createElement(a);r.async=!0,r.src="https://www.googletagmanager.com/gtm.js?id=GTM-K93GVKM",m.parentNode.insertBefore(r,m)}(window,document,"script","dataLayer")</script><script type="text/javascript">var clevertap={event:[],profile:[],account:[],onUserLogin:[],notifications:[],privacy:[]};clevertap.account.push({id:"449-6R6-695Z"}),clevertap.privacy.push({optOut:!1}),clevertap.privacy.push({useIP:!1}),function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"==document.location.protocol?"https://d2r1yp2w7bby2u.cloudfront.net":"http://static.clevertap.com")+"/js/a.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}(),window.clevertap=clevertap</script>
      <style>@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gTD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3g3D_vx3rCubqg.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gbD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gfD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gnD_vx3rCs.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WRhyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459W1hyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WZhyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WdhyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gTD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3g3D_vx3rCubqg.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gbD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gfD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gnD_vx3rCs.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gTD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3g3D_vx3rCubqg.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gbD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gfD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}</style>
      <link href="/static/css/2.5e365797.chunk.css" rel="stylesheet">
      <link href="/static/css/main.6ee71c34.chunk.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/static/css/12.3cf8092d.chunk.css">
      <script charset="utf-8" src="/static/js/12.7de49878.chunk.js"></script><script type="text/javascript" src="https://wzrkt.com/a?t=96&amp;type=push&amp;d=N4IgLgngDgpiBcIoCcD2AzAlgGzgGiTS1wVAGMwB9VKMVAVzAXQENsBnGAXwMwBMEIACxCAnAFoAbACVJU0QFYAWiAIBzQXzJDW6UZIDMARgBMMIZNF6ADHyHHRMdNZjWh1kwdUh2CI5Osja1EAdlEhEwIoDXgTLiAA%3D&amp;optOut=false&amp;rn=1&amp;i=1601098676&amp;sn=0&amp;gc=dc4faf96312e469f90d4319ef0e04023&amp;arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsApbAMwEEBnAcUxABMIAXSuABi3oAt2sA3OAEYskVvGAACAL51s9fABYFATgC0ANgBK6jcoCsCOgFNmYjiABOpoeraC2AZmUAmAOxspQAA&amp;r=1601098676353" rel="nofollow" async=""></script><script type="text/javascript" src="https://wzrkt.com/a?t=96&amp;type=page&amp;d=N4IglgJiBcICxwJwFoBsAlVbEFYBaIANCAOYwgQDGcAZgIY2KoDMAjAEwCmcqijADBDhtEnGv0784%2FdsyIgAzjFap%2BrfogDsiOO2IAHMtGbFKh8gAsALlf0LoAegcQArgGs6dAHYA6CAHtbTh8wLwdWHFYADlRWZgdKOgAnK3kAdTx0AGkAfQBhAEEAWQAFGFAAdwBbfRh%2BYisAG0oYAG0AXQBfTqAAA&amp;rn=2&amp;i=1601098676&amp;sn=0&amp;gc=dc4faf96312e469f90d4319ef0e04023&amp;arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsApbAMwEEBnAcUxABMIAXSuABi3oAt2sA3OAEYskVvGAACAL51s9fABYFATgC0ANgBK6jcoCsCOgFNmYjiABOpoeraC2AZmUAmAOxspQAA&amp;r=1601098676363" rel="nofollow" async=""></script>
   

       <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-K93GVKM&amp;gtm_auth=&amp;gtm_preview=&amp;gtm_cookies_win=x"></script><script>
         (function(w,d,s,l,i){w[l]=w[l]||[];
           w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js', });
           var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
           j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl+'&gtm_auth=&gtm_preview=&gtm_cookies_win=x';
           f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-K93GVKM');
      </script>
      <meta charset="utf-8">
      <link rel="icon" href="/logo-new.png">
      <meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="apple-touch-icon" href="/logo-new.png">
      <link rel="manifest" href="/manifest.json">
      <title>Nitish Shop</title>
         <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../ImagesCSS/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="../ImagesCSS/css/style.css" type="text/css">

      <meta name="description" content="Now order online from my whatsapp store &amp; pay using 150+ UPI Apps">
      <meta property="og:title" content="Nitish Shop">
      <meta property="og:image" content="https://dotpe.s3.ap-south-1.amazonaws.com/kiranaStatic/image/default-ssr-icon-22092020.png">
      <meta property="og:url" content="https://d-1518613.dotpe.in/">
      <script async="" src="https://www.google-analytics.com/analytics.js"></script>
      <script type="text/javascript" async="" src="https://d2r1yp2w7bby2u.cloudfront.net/js/a.js"></script>
      <script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-K93GVKM"></script>
      <script>!function(e,t,a,n,g){e[n]=e[n]||[],e[n].push({"gtm.start":(new Date).getTime(),event:"gtm.js"});var m=t.getElementsByTagName(a)[0],r=t.createElement(a);r.async=!0,r.src="https://www.googletagmanager.com/gtm.js?id=GTM-K93GVKM",m.parentNode.insertBefore(r,m)}(window,document,"script","dataLayer")</script><script type="text/javascript">var clevertap={event:[],profile:[],account:[],onUserLogin:[],notifications:[],privacy:[]};clevertap.account.push({id:"449-6R6-695Z"}),clevertap.privacy.push({optOut:!1}),clevertap.privacy.push({useIP:!1}),function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src=("https:"==document.location.protocol?"https://d2r1yp2w7bby2u.cloudfront.net":"http://static.clevertap.com")+"/js/a.js";var e=document.getElementsByTagName("script")[0];e.parentNode.insertBefore(t,e)}(),window.clevertap=clevertap</script>
      <style>@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gTD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3g3D_vx3rCubqg.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gbD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gfD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:300;font-display:swap;src:local("Montserrat Light"),local("Montserrat-Light"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_cJD3gnD_vx3rCs.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WRhyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459W1hyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WZhyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WdhyyTh89ZNpQ.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:400;font-display:swap;src:local("Montserrat Regular"),local("Montserrat-Regular"),url(https://fonts.gstatic.com/s/montserrat/v14/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gTD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3g3D_vx3rCubqg.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gbD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gfD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:600;font-display:swap;src:local("Montserrat SemiBold"),local("Montserrat-SemiBold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_bZF3gnD_vx3rCs.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gTD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3g3D_vx3rCubqg.woff2) format("woff2");unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gbD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gfD_vx3rCubqg.woff2) format("woff2");unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:Montserrat;font-style:normal;font-weight:700;font-display:swap;src:local("Montserrat Bold"),local("Montserrat-Bold"),url(https://fonts.gstatic.com/s/montserrat/v14/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2) format("woff2");unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}</style>
     
      <link href="CSS/main.6ee71c34.chunk.css" rel="stylesheet">
       <link href="CSS/2.5e365797.chunk.css" rel="stylesheet" />
      <script type="text/javascript" src="https://wzrkt.com/a?t=96&amp;type=push&amp;d=N4IgLgngDgpiBcIoCcD2AzAlgGzgGiTS1wVAGMwB9VKMVAVzAXQENsBnGAXwMwBMEIACxCAnAFoAbACVJU0QFYAWiAIBzQXzJDW6UZIDMARgBMMIZNF6ADHyHHRMdNZjWh1kwdUh2CI5OsjawMADhChBQIoDXgDLiAA%3D&amp;optOut=false&amp;rn=1&amp;i=1601039272&amp;sn=0&amp;gc=dc4faf96312e469f90d4319ef0e04023&amp;arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsApbAMwEEBnAcUxABMIAXSuABi3oAt2sA3OAEYskVvGAACAL51s9fABYFATgC0ANgBK6jcoCsCOgFNmYjiABOpoeraC2AZmUAmAOxspQAA&amp;r=1601039272599" rel="nofollow" async=""></script><script type="text/javascript" src="https://wzrkt.com/a?t=96&amp;type=page&amp;d=N4IglgJiBcICxwJwFoBsAlVbEFYBaIANCAOYwgQDGcAZgIY2KoDMAjAEwCmcqijADBDhtEnGv0784%2FdsyIgAzjFap%2BrfswAcmuDmIAHMtDjFKh8gAsALlf0LoAegcQArgGs6dAHYA6CAHtbTh8wLwdWHFZNVFY5YgB1PHQAaQB9AGEAQQBZAAUYUAB3AFt9GH5iKwAbShgAbQBdAF8moAA%3D%3D&amp;rn=2&amp;i=1601039272&amp;sn=0&amp;gc=dc4faf96312e469f90d4319ef0e04023&amp;arp=N4IgVg%2BgdiBcIC0DuBeFIA0ICW06IFsApbAMwEEBnAcUxABMIAXSuABi3oAt2sA3OAEYskVvGAACAL51s9fABYFATgC0ANgBK6jcoCsCOgFNmYjiABOpoeraC2AZmUAmAOxspQAA&amp;r=1601039272627" rel="nofollow" async=""></script>
      <link rel="stylesheet" type="text/css" href="CSS/4.0aaee4f2.chunk.css">
      <script charset="utf-8" src="js/4.39010235.chunk.js"></script>
      <meta name="description" content="details " data-react-helmet="true">
      <meta name="keywords" content="product_name" data-react-helmet="true">
      <meta property="og:type" content="article" data-react-helmet="true">
      <meta property="og:title" content="Nitish Shop" data-react-helmet="true">
      <meta property="og:image" content="thumbnail" data-react-helmet="true">
      <meta property="og:url" content="https://dukaan.dotpe.in/1518613" data-react-helmet="true">
      <link rel="stylesheet" type="text/css" href="CSS/11.74f0c807.chunk.css">
      <script charset="utf-8" 
         src="js/11.93b552f9.chunk.js"></script>
          <style>
           .addtocart-btn__changeIcon12{padding:1px 10px;font:normal normal 600 12px Montserrat;letter-spacing:.72px;color:#f4071b;font-weight:600;display:flex;align-items:center}
           .addtocart-btn__changeIcon12>span{margin-right:4px}
           .addtocart-btn__changeIcon12>img{display:inline;width:9px}
           .shoping__cart__table table tbody tr td.shoping__cart__item__close {
	text-align: right;
}
.arrow_up, .arrow_down, .arrow_left, .arrow_right, .arrow_left-up, .arrow_right-up, .arrow_right-down, .arrow_left-down, .arrow-up-down, .arrow_up-down_alt, .arrow_left-right_alt, .arrow_left-right, .arrow_expand_alt2, .arrow_expand_alt, .arrow_condense, .arrow_expand, .arrow_move, .arrow_carrot-up, .arrow_carrot-down, .arrow_carrot-left, .arrow_carrot-right, .arrow_carrot-2up, .arrow_carrot-2down, .arrow_carrot-2left, .arrow_carrot-2right, .arrow_carrot-up_alt2, .arrow_carrot-down_alt2, .arrow_carrot-left_alt2, .arrow_carrot-right_alt2, .arrow_carrot-2up_alt2, .arrow_carrot-2down_alt2, .arrow_carrot-2left_alt2, .arrow_carrot-2right_alt2, .arrow_triangle-up, .arrow_triangle-down, .arrow_triangle-left, .arrow_triangle-right, .arrow_triangle-up_alt2, .arrow_triangle-down_alt2, .arrow_triangle-left_alt2, .arrow_triangle-right_alt2, .arrow_back, .icon_minus-06, .icon_plus, .icon_close, .icon_check, .icon_minus_alt2, .icon_plus_alt2, .icon_close_alt2, .icon_check_alt2, .icon_zoom-out_alt, .icon_zoom-in_alt, .icon_search, .icon_box-empty, .icon_box-selected, .icon_minus-box, .icon_plus-box, .icon_box-checked, .icon_circle-empty, .icon_circle-slelected, .icon_stop_alt2, .icon_stop, .icon_pause_alt2, .icon_pause, .icon_menu, .icon_menu-square_alt2, .icon_menu-circle_alt2, .icon_ul, .icon_ol, .icon_adjust-horiz, .icon_adjust-vert, .icon_document_alt, .icon_documents_alt, .icon_pencil, .icon_pencil-edit_alt, .icon_pencil-edit, .icon_folder-alt, .icon_folder-open_alt, .icon_folder-add_alt, .icon_info_alt, .icon_error-oct_alt, .icon_error-circle_alt, .icon_error-triangle_alt, .icon_question_alt2, .icon_question, .icon_comment_alt, .icon_chat_alt, .icon_vol-mute_alt, .icon_volume-low_alt, .icon_volume-high_alt, .icon_quotations, .icon_quotations_alt2, .icon_clock_alt, .icon_lock_alt, .icon_lock-open_alt, .icon_key_alt, .icon_cloud_alt, .icon_cloud-upload_alt, .icon_cloud-download_alt, .icon_image, .icon_images, .icon_lightbulb_alt, .icon_gift_alt, .icon_house_alt, .icon_genius, .icon_mobile, .icon_tablet, .icon_laptop, .icon_desktop, .icon_camera_alt, .icon_mail_alt, .icon_cone_alt, .icon_ribbon_alt, .icon_bag_alt, .icon_creditcard, .icon_cart_alt, .icon_paperclip, .icon_tag_alt, .icon_tags_alt, .icon_trash_alt, .icon_cursor_alt, .icon_mic_alt, .icon_compass_alt, .icon_pin_alt, .icon_pushpin_alt, .icon_map_alt, .icon_drawer_alt, .icon_toolbox_alt, .icon_book_alt, .icon_calendar, .icon_film, .icon_table, .icon_contacts_alt, .icon_headphones, .icon_lifesaver, .icon_piechart, .icon_refresh, .icon_link_alt, .icon_link, .icon_loading, .icon_blocked, .icon_archive_alt, .icon_heart_alt, .icon_star_alt, .icon_star-half_alt, .icon_star, .icon_star-half, .icon_tools, .icon_tool, .icon_cog, .icon_cogs, .arrow_up_alt, .arrow_down_alt, .arrow_left_alt, .arrow_right_alt, .arrow_left-up_alt, .arrow_right-up_alt, .arrow_right-down_alt, .arrow_left-down_alt, .arrow_condense_alt, .arrow_expand_alt3, .arrow_carrot_up_alt, .arrow_carrot-down_alt, .arrow_carrot-left_alt, .arrow_carrot-right_alt, .arrow_carrot-2up_alt, .arrow_carrot-2dwnn_alt, .arrow_carrot-2left_alt, .arrow_carrot-2right_alt, .arrow_triangle-up_alt, .arrow_triangle-down_alt, .arrow_triangle-left_alt, .arrow_triangle-right_alt, .icon_minus_alt, .icon_plus_alt, .icon_close_alt, .icon_check_alt, .icon_zoom-out, .icon_zoom-in, .icon_stop_alt, .icon_menu-square_alt, .icon_menu-circle_alt, .icon_document, .icon_documents, .icon_pencil_alt, .icon_folder, .icon_folder-open, .icon_folder-add, .icon_folder_upload, .icon_folder_download, .icon_info, .icon_error-circle, .icon_error-oct, .icon_error-triangle, .icon_question_alt, .icon_comment, .icon_chat, .icon_vol-mute, .icon_volume-low, .icon_volume-high, .icon_quotations_alt, .icon_clock, .icon_lock, .icon_lock-open, .icon_key, .icon_cloud, .icon_cloud-upload, .icon_cloud-download, .icon_lightbulb, .icon_gift, .icon_house, .icon_camera, .icon_mail, .icon_cone, .icon_ribbon, .icon_bag, .icon_cart, .icon_tag, .icon_tags, .icon_trash, .icon_cursor, .icon_mic, .icon_compass, .icon_pin, .icon_pushpin, .icon_map, .icon_drawer, .icon_toolbox, .icon_book, .icon_contacts, .icon_archive, .icon_heart, .icon_profile, .icon_group, .icon_grid-2x2, .icon_grid-3x3, .icon_music, .icon_pause_alt, .icon_phone, .icon_upload, .icon_download, .social_facebook, .social_twitter, .social_pinterest, .social_googleplus, .social_tumblr, .social_tumbleupon, .social_wordpress, .social_instagram, .social_dribbble, .social_vimeo, .social_linkedin, .social_rss, .social_deviantart, .social_share, .social_myspace, .social_skype, .social_youtube, .social_picassa, .social_googledrive, .social_flickr, .social_blogger, .social_spotify, .social_delicious, .social_facebook_circle, .social_twitter_circle, .social_pinterest_circle, .social_googleplus_circle, .social_tumblr_circle, .social_stumbleupon_circle, .social_wordpress_circle, .social_instagram_circle, .social_dribbble_circle, .social_vimeo_circle, .social_linkedin_circle, .social_rss_circle, .social_deviantart_circle, .social_share_circle, .social_myspace_circle, .social_skype_circle, .social_youtube_circle, .social_picassa_circle, .social_googledrive_alt2, .social_flickr_circle, .social_blogger_circle, .social_spotify_circle, .social_delicious_circle, .social_facebook_square, .social_twitter_square, .social_pinterest_square, .social_googleplus_square, .social_tumblr_square, .social_stumbleupon_square, .social_wordpress_square, .social_instagram_square, .social_dribbble_square, .social_vimeo_square, .social_linkedin_square, .social_rss_square, .social_deviantart_square, .social_share_square, .social_myspace_square, .social_skype_square, .social_youtube_square, .social_picassa_square, .social_googledrive_square, .social_flickr_square, .social_blogger_square, .social_spotify_square, .social_delicious_square, .icon_printer, .icon_calulator, .icon_building, .icon_floppy, .icon_drive, .icon_search-2, .icon_id, .icon_id-2, .icon_puzzle, .icon_like, .icon_dislike, .icon_mug, .icon_currency, .icon_wallet, .icon_pens, .icon_easel, .icon_flowchart, .icon_datareport, .icon_briefcase, .icon_shield, .icon_percent, .icon_globe, .icon_globe-2, .icon_target, .icon_hourglass, .icon_balance, .icon_rook, .icon_printer-alt, .icon_calculator_alt, .icon_building_alt, .icon_floppy_alt, .icon_drive_alt, .icon_search_alt, .icon_id_alt, .icon_id-2_alt, .icon_puzzle_alt, .icon_like_alt, .icon_dislike_alt, .icon_mug_alt, .icon_currency_alt, .icon_wallet_alt, .icon_pens_alt, .icon_easel_alt, .icon_flowchart_alt, .icon_datareport_alt, .icon_briefcase_alt, .icon_shield_alt, .icon_percent_alt, .icon_globe_alt, .icon_clipboard {
	font-family: 'ElegantIcons';
	speak: none;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	text-transform: none;
	line-height: 1;
	-webkit-font-smoothing: antialiased;
}
       </style>
   </head>
   <body>
         
      <noscript>
         <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K93GVKM&gtm_auth=&gtm_preview=&gtm_cookies_win=x"
            height="0" width="0" style="display:none;visibility:hidden" id="tag-manager"></iframe>
      </noscript>
      <noscript>You need to enable JavaScript to run this app.</noscript>
      <div id="root" style="overflow-y:scroll">
        
            <div class="product-container">
                
               <div  class="product-header">
                  <div class="product-header__top"><span class="custom-icon--btn"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAHqADAAQAAAABAAAAHgAAAADpiRU/AAAEMUlEQVRIDa1Wb2xTVRQ/57Wu2To6ELDb3DpYyXSbYGaIshAF56BtGCBxxqjxgxJtw774J2TGxD8kGkVRIgbHSNgHY1TEiAQjbC4BSVz4IGrAzezDNNmeU+dIlA1Jm+wez3mlb6+vb7XtdpP2nvM795zfu+eed95FyGFUdm8tcat4C4HWgkB3AUIFAfqB/xDpD55+Q6IBQOofjfV9nUNIDpFllPdElt8QV528KoaA3ixLZ01EEwrxBT16uof9aNaQLs1JHDgU6gTAN9KX56PR90rh4/qu0xedvDKIlx1Zv6g4UXqUcxhxcsgLI4grVLv1WN97dr804qp3mouxpOwcg2vtC+ejE9DHY7HeR6wxNFPhCtGKfZ8uNKnE5/p4uLor9KrJxYIrpVRVhnfzgo6ULrOvyAttwQ2wrnI1/DgxbDXlLfPR3VPaVjs49eXIkDi75a+mO1ShCF4ROTWE9Kv2g1C9yG9A/FDQc+mLlLmg2UXaPmb5nH/KSDUp2MspLrFGe3l9zCQV/InV91vNhckINQF/6FFx1m48EPFxG3jMHqm9rjUNkp2vq1yThhWm4DPip3mLaIs9wFwEOxdm103lXeEVnGrabie+Ep+2Q4bu85Q64vmCblRb5YxvtTsOXf4FriSupsGi7/n2UBpWqIKE9RoX1U1OAewVLLo80MIMLOcdY4VTsCMXj6ft+gFbsTn55IrJZiXVU04OktrnzrxtmqSq92181tTnJ9A1V1lb8Ene9RKnQCN/61DGBdXkT5ZB47Ig6NMT8045IZ5zlW1Z1crvcZ0TsWDfjF2A5ptvh6rrHSy0ohl8Hq+BW30altbC8R37QWan4rSuRYITLl9bsJb7aIvVYJf7fh2AjYG1sLwkmZg7/PWweWUzSEb0qT+Nnn44/BKsWlwNkhV53+VB5bjEnjE0eBEDh0ONoPCnDKMNkN59dNubxo6spsHLI4bauDRohU25ptv+Wad/RqO9S7TRp3oHicDxlmB6syBPH/msI+NDIYRzkTq9fkT4iVyJpKqBEN6ykmST9wx0w0MnO+H875eyLTNsx4Yz7n2JGaVeEyO/UsnBd6yfWc3oYim70yw9/cFbNoH9gyLZ2f/dhxnZUUDv6rHepyXWLHFX+HnWXnci+D9Mzr+Bi0pmIR2aHElrPuLP15/BGSy6czx68l/R3fIng5A2yMe+kCFk58fnLhMimgZN254iFQ7jjOFAxMON+95CSHPwmeLL9X1j0VPJ8r/uYOw44FZ3cxPx5BAkvyVyuddmWvVYf0YlJneMsMmMSDDM53FQIfEVhc6YeJ4Cp7cn4dFu06OZpBLK2LFCuIqkds5oqn881j9q4fio6v3wGo07DWPtFjybeAyU2ju2q+9CtkU5V5P/g81ezzWtCUA1kMI6brP1nBEeOMnzJGfph7hKnPir46zz9cX2FP8B8jVg2B7NlScAAAAASUVORK5CYII=" alt="Whatsapp Icon Button">Contact</span><span class="custom-icon--btn"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAhCAYAAABX5MJvAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAIaADAAQAAAABAAAAIQAAAAA1iKI6AAABmElEQVRYCe1XzVXDMAy2eHDnQq90A7IB6QbuBmEDmIBsQEYIE7QbkG7QTAAcKQfaKz2IT8XpM/mxIeSQvBe/l8afPklWZUVOlBpHQwY+tD63qRZ4+hd70T2xDWS+J8qKhTdap3ulniwcAT9DHogu7lVMJHxk+MDoH/EnUSqcPcgGMt/M56yY15iuFVEkMoNT4MTgLQwTJoprMYTEHLNSt7A5ZNbC+WSxCA925qc+CFuj+/mqHERlO7pf0+9xDKLI0WkxabwzP6LAXhp5D0FEIVSuXWreIFC56WS5zFxOXNyb1jECcQbRi5rwZgJb8YDms3X9WxcH+6mLF84bBJpN4HPi4iuNqEZ5GNuBdju7+H9h3tck4CgaRiZQWFfvcqi1HMx86TP9TWEmrSOQ1clfmsPYDrxLjG3bV0+d8d7CRFGObbuzdPsc9eIR7XEQ6A3oDzdWGnMcZHclPAPeGZnwNt4ZnBu+jC1XNV9g0pzwOhfhSk0g+RlziJM0KeEMOIS3leEPGE/Tq8jl5BW58D/w94cVxOPoaQa+AAfIyvYpxBVGAAAAAElFTkSuQmCC" alt="Book Icon Button">My Orders</span></div>
                  <div class="product-header__bottom" style="background-image: url(&quot;/static/media/product-header-bg.dc85c9e0.png&quot;);">
                     <div class="product-header__bottom--box">
                        <span class="product-header__bottom--storenameText">Nitish Shop</span>
                        <div><img src="../Logincss/images/Needly_Logo-Playstore.png"  alt="Dukaan banner"></div>
                    </div>
                  </div>
                  <div class="product-header__bottom--searchbox "><span style="visibility: visible;">Categories<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAALCAYAAACZIGYHAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAEaADAAQAAAABAAAACwAAAACHCz4YAAABJElEQVQoFY2Pu04CYRCFZyAUFBaYgAUF76HB+ABbsCT4BlppYqlWNl46C630Baxgq6XWxBew0kivARNCB0vc8Rx2QRHc9RT/P2cuX2ZUoG6tdqWqT8VW65Y+TR+OUw5zuTvMbBebzTftuu4pho45qCK7aaB+vV4JzO7RW8HIa3443MiY2ZgAykRueq67E7nFl4Cx2WMMYMMnn8ya553gv6ChCMJ5e5H7fqcAZMpx9hlbbK602z1AI+Gsc0SHUy9m+yXPu6ZPArA+g9AsA1k262sYPqAcbWDWyY9G69yAM9QchAmcciaqR4wnMuvDF+L4RYJgq+T773F18i1AmAXoEoMHPxtxXgeA6m8Ae5ZCWJgDJQASITOQiPPXBuz5lwaNxmpa4xdvlYRyESN0/wAAAABJRU5ErkJggg=="></span><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAhCAYAAAC4JqlRAAACDUlEQVRYhb1Xy3HCMBB98eQedxB3EFIBUEGggriD0IGdCkIqYKiAUAGmgtgdhGtOpoOMMquZnbVWkjH4XRhLMu9p/777XS4RQAbgBcACQApgQsdrAC2ALwBHeu6Ne88LhugDwMyzD7ZfAXin32gkykFD/O0hd8GcPQDY9BEgLWBMvFOIGzI3h3HLk1jLyTpzcpEXMgYODvItgBLAj/JHRvQKQCHW6xgR3AVrQX4G8Ew30shBBCWdPbF1G0NeJOzwmyDPekZ2Tf/TsLU8FEdWgFQ6i/GfAy2RntmWdE1HQCZUbi/NaUJN7rSYsZR1CliItXIAucVaPOexAppAwMXCuGLPznot8MCeZZ4PAXfj1CdAVTcQUZbUSvFoSETeXhNZrAV4vvdpPiFw16qXTETgTam2XwM88NQWnTg2V1cgL8VF1OxKKF2ObK0YmBmZ6CunkAXgqH6bC11h5wn+rteiVkBFPcBiQrNBH0tkjnf2oeLG60AuotWKKCKsUdAIJwXLntCBnIhSsoYcs0A3qanCtUQ2odTVBLY0Fand1TWWp6T8NaRegQm6x1gRrlJsh4q5yI4Y4iXFAu+EqS+ofb2gIvOaWe9TEdNQ8M6J2AacFk8dETFfRpfCFU+dSfmW3bAlC3otcet2HBQxxjygidiNJUAT8T8tjzkRGRFmALZfT0ZM7fs8vwVMFbUVtAaAP7S7fejWq/5FAAAAAElFTkSuQmCC" alt="Search Icon"></div>
                  <div class="product-header__bottom--searchbox-dummy" style="display: none;"></div>
               </div>
               <div class="rodal rodal-fade-leave bottom-modal" tabindex="-1" style="display: none; animation-duration: 300ms;">
                  <div class="rodal-mask"></div>
                  <div class="rodal-dialog rodal-slideUp-leave" style="width: 100%; min-height: 300px; animation-duration: 300ms;">
                     <div class="category-modal-wrap">
                        <div><span class="closeicon"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAArFJREFUWAntVv1d2zAQPZkByscChgWawgC4E9QbECZou0E3aDtBwwYwQcwAKekCJAtQYID6+p6D/JMUyXaS/lf0hy2dnu496U4fIv97MdsuwOMoL8SI2vEHd8tbW9/kP1gACHPJsgs1WoqYUZxE50bNtdT11cF8uYxjfGuvABDv11n21RgZ+0O7W6oyyer6M4Q8dSE7BYB8pJmZijH7XU6SfapPptb3EDFPYbJUx+NpPta97G5rcjqGcPqgrxRPdAWamZP8Hxbzp34XW4k1AYw5ln2x08xjwlfhOA5zYi0E9Z75liB/FtGbmG/f1mCADQrCgWT+EljFE4DZ50bMRQhC+xlLWBzOFqWKXkX6GxP7iCGWY0IcdtJHcrh2TwAUfnI7nfrUxu9othjHRNDGPo5ZYbViPSzgGLs2TwAOtsLtdOrlw+nJD9sORbjkxDycHU+wBT5YvPs3Rj27l4S/z07ao9UdZOs8XI5+3l/a9opIxM6cdtoSYbTD5HB23/K2FcSmwJ6dtqhEJRThwoaQE48c4eFUse6FgIa+wiPZDYfFDyW3ePvfWEAzEDeOdbDrf2MBYcJZAWFiWnvf35tJbxI6W42OmyTE/Rwm5iZJ6K+A6q+U4nDmNuZhTvSuRMDhCVDBYyJa9KZrq8VEpI7tkMMTgAfEJM4v59imI/bZmYc4V0SDVTkPMWyHHF4OEJAiENxmuKQqQEriOso1sEXsQsMZ8h354h33awKgno+IJQjedJBs08ULLe+9jgl4uc22IUmOoc+QnGAvB+xoAPG6rS9te9c/fdFnzM9aCFwQkwnhqGDbNhzNOyJFTq7oCrCDpVkJxI3Js7IM/3LMS8yjM7eeOlfAgvjHauR8TODNUCLD37p9bR2HDPc5txrEL1t7R2WwgNAHBBWuDYSV236tD12Bv6trRs7+ButJAAAAAElFTkSuQmCC" alt="cancel"></span></div>
                        <div class="category-box">
                           <ul>
                              <li></li>
                              <li>Cello</li>
                           </ul>
                        </div>
                     </div>
                     <div class="category-havesomthing">
                        <div class="havesomething-in-mind__box">
                           <div class="havesomething-in-mind"><span>Have some specific thing in mind ?</span><span>Make a list of items you need<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAYKADAAQAAAABAAAAYAAAAACK+310AAAB9UlEQVR4Ae3cQUrDUBAG4MatCB7OC7hz1xOI8Xpu3HifOIEUSijFpK+dMf0CoVRJZvz+ebTY8nY7BwECBAgQIECAAAECBAgQIECAAAECBAgQIECgsMAwDO9x9oVb3G5rAb+P83AI4ZZRh/oxvhBujP92ED/xaCVcM4wAfz2BPv+REK4RQii/zKXPPBdC6xAC+znOnzPo818JQQitBQrcz0oQQgGBAi1YCXVC+J6/+p553hdoe1stBPZTnELIjFUImfpTbSEIoYBAgRasBCEUECjQgpUghAICBVqwEoRQQKBAC9NK+IrHvx59gba31ULIP8YphMxYhZCpP9UWghAKCBRoocJK6C51iD9iuPQe/+z6z67r+lY9P7S60R3d5yNmTgDJgTcLwQpYn2STEASwPoAmVwpgPWOTF2MBrAugCf5YWgDLA2iGv7z0xq6It5P+OZeVKfws+agLH36iQGJpkw8/USCxdEz++A1qH0VmZDDh+/o6/AyBxJomH36iQGJpkw8/USCxtMmHnyiQWDomf9zEw/v8jAwmfDuowM8QSKxp8uEnCiSWNvnwEwWSS8f027QvOYPxw3TbVhYIwcatBULYx2qYH312X3dVP/SPQ4CfkX6EYPv6DHg1CRAgQIAAAQIECBAgQIAAAQIECBAgQIAAgUUCv/mA9bof+IauAAAAAElFTkSuQmCC" class="havesomething-in-mind__arrow" alt="forward arrow icon"></span></div>
                           <div class="havesomething-in-mind__box-image"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHIAAABqCAYAAABgWAWLAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAcqADAAQAAAABAAAAagAAAAADJmxvAAAZLklEQVR4Ae1dCXgcxZV+1T2XpJGs09ZpyZIsY4yJOWxuMFniGBMHcxhsA4kTghMSSDiXwJIAG8jmS2BxyCbBsAlsgrFZMITlcMAsVzCn2YA5fWBbki1fsiTrGmmmu2v/6tGMZkbTre7W6LAz9XnU1VWvXr2qv9+rqlfVbaJ0SPdAugfSPZDugXQPpHsg3QPpHkj3QLoH0j2Q7oF0D6R7IN0D6R5I90C6B0axB9hw111QPGkWI54p6uEkdUkuagsoUlvnnq37h7vufyT+KQeysLjqDJKkLxOjU8H8ZCLmM+5QvpETvUycv0zBjlebm5s7jGnTOWY9kDIgC0qrzmUk38YYHWNWoWke5+sB7DqNqS+27Kp/y5Q2LrMKD8uOntikgtJJvz7QtP1HsWmHc3zIQI4bX1Hjcbl/T4x9JZUdxYm3MU5/UTk9KysdL+zfv78zGf+C0uqzGLGrm5u+ODeSX1hacyMeqF+qpJxs74GIcDj0rq6hiFxQVn2lROx3Q+FhVBbg5MI8L5XxI08OFZZmb4KZ3oTx9jNci5E3BYPuUbj6cX0zwqeotOaHSPuluGeaVIOLDc2OcDn0ro6BRIc9jOZ+c6SazBibgrrwY1+P1tlnTzROLxcUV89kMt2GvHOi+RLLjsYP84gjIGG6VuOpv3is9I3E6HsksVsT5YFpPpiYdrje2wayqLT6obEEog4Mo8JkAGnE/2GWOFKyDjBKKyypvgaTmqVG+WMtPajJH4w1mYZLHsuz1sLimjMxBr08XIKkmi+WMbuad31Rnmq+Y5WfJY0sLCzMJpmvHKuNSCoXp+eTph+midbGSM+4f4fqlhxKfaCR9vihJO9QZR3UtOYX106TZf7xUCsyKj9lUglNqhiv/w60ddLuva20cVMDHezoNioyaDrnvL65aVvVoISHEcGgGilJ/OfD0d6TjplMv7ntcirI9Sdl/0XDHvr7pzvoA/z+tuFzamhqTkqXNJHRr5OmH8aJphpZVFY1g0j+e6rbf/IxdbTy3h/aYiu09Nn/fZ+eWvcONbcm9dbp/P7RJjmRTpQjkWTXzOyC5fBZHpUsz2lacVEurV5+DXm9blssJhSOo9NmTqVli86i446qplBIoc07dg/gMak0//tNTU0fDcg4zBMMNTI3tyrXnSW3prr9q5f/iE6YMTklbDs6A/TC+o30fx9vox0799ElXz+NzjnzmMf8k+ctSkkFhxATQyALS2q+yyS6P5Vt+fH3FtB3oVHDHTiTpmfXfnXYJmjDLb8T/obrSMb4+U4YGpWZfcKRIwKiqJ9x7W4jOQ6F9HcXzixef/7JlXZkTaqRRUVFfmwdpWy3/qi6Cvrv31xHGTbHRTsNiaPlFMrivlI25UwbU904DpZv+PZXcrvUnnOxlbZUL8RpR5bbdy2bdGabGZMP5xydFcjMOUIjqoPlq8NBmDpfycRjMqpqp4SaGnQFk3PyFE9x2braK2+cZ8ZL5CUFMr+kaq4syWsHK2wlv2biBHrit9dRbnaWFfIU0vBrMVYuTwXDji1rZ6Nnz+BcftpfNyfqv+3cvHYpZ/xefe80WhGvJ+5aIOg2HHecOzTRXctIqtOI1aGz6zgDcIiDvDhaBBH/tGOJhYLEewOxyXrcXTghMGHGCVPzzrmgfkBmX0JSIAtLq+/A/t9PjQrZSf+fFTfR9CkVdoqkhBbLkKezJ5+9YCjMurY+v4Br7F487lVRPoz/mjTXw0QKdoHYDJEOB0R7+8dbHtj93Et7Qy2torE6YLhWooNNVwaifMbEapKxPIgEaCG58Qt8/hFpPWFg3cXlzUfcdFdRhCbxauQQODaR0Mn9eXNmjgqIuqycTkMH43nErqSDoGsbJ7FlFw04fvIaBZWvMDfDWaBwRuuGjdruZ9blyBneG/zVE8lz3FGkBHrowPr3ouXMIgx21ZOTR2pHm06WN/d8yj7pTD2uBbpp38P3UXDPLgrt2Vm49cF776u94tqkC3AjIIXqDzksW5TSYzy25MEDnh/c8aI4VfC5rYIgDmxfW6UouskMF+X0ISLLtd7gPbLPmy8S1Z4e2rvudf2MZ+2PLidPfm6Ytu+vVSBzZ50KkHbppbJPPCMKokiQMjKpcNEV1LT8dj0/uLP+SkSSApl01oqneMhAlhTl0RHVpboAo/UnqGq66bNbvxqi22PHvWDrwToo4EMREAW/0IE2Kp0/h0q/PicKYrC1jQ688S7V/8m6v15pCc/HJF8GjZs9cE7jyiugrBmz9Cao7a0uoZXJ2pNEI8XRQmchx59Bt/7gAppz6tE0LhtnkoVR0y1bjH1KYC2c4+98uIU+3Rp+KiPZJ86opak15WE+kUS7V05HWC3y0TVLZxNpZ0Dg2VoodIbkcUeLevLHZURv+iK+svBcRWhm24aN1IKfCC6flzq3N/RRmV/EBId3HtSJBFhCA5MFAXDXB+/qWVpvQKA9QCsHAJldFsL0ctDxeUB9AsTXV90R3/E6fvijAzqgiJ7ws/9YQ+XF+XRijLencU8LvfC3jfSvyKsoLqAVdy6LK/zC3z6kh9a8Su2d3ZSTFe7jnXsOUHlJIR1ZW6Y/SCfOgBJxqoormHCz8ZqlCyCcOEa5AMSwjYx8peMpFsSEItFboX1dXzTAxAbIP6mSCk4Na40g6PqinrY98EiU1ijiyswkJQrkiUZkJLTSlZtPSlsLBXc2VCcjHACkq9urUPIHI1n5aNqKO6+IBzGag4ixQtLdN18WS6nHjZsUJq0A8L+75TLqfnU9tX+yGR3vI++UqcQnVtBmdM4Ta9+hsgkFVFFSEFabhBr+vmBGrlw5/U3Mg6YmZJHL4jLJk5dLnuPjx8UILznDolFTFL2IAMlTYn6YIeOI6dTx9mtiecJan1tTmbgUGQBka+u29qLMmohMlq7Cdyo0YKTCkZMraPv9f9ZNmCvLT148sSKwhl00vSCP5lx3MUleD5SN5wrQAiz7SwBtNgzDDMb5DMWfXeWSkk4PyJUDg2QzCPPa/vFmav90E/U07SWMqZY4KC37dTqx1BgsSL5+7Wo72HYe6JfHlhkAJDI5ptktGOz12VkssVH8wrmD6ZBRSWfpwnSJcUhM3b15hVEmXFUpsKuJdj35V9JCvRRobDo2JPlbcahZpwlbekZuf060TGLEV2a4VEskpc5t9RRqOUhd23aQ0NDCU2YBxDba/cxL+qx2QIGYBJd/XPTOU2yujYLQWzUZf419NMmAFOW24tdv9EWKSSgvzjPJTX1WM2aGIrj8fmKyDNBCFNi9E08gHF4Igb1N+hV/+mcskRRcZcwQjYLbomkV5f3VlUQYsfKOPzrKLosqIctezF7fi6Yli3iFKQ32JMtylJYUSGjj5+BmGcgja517brai0W6Xi7IxrmS43fDHeijQG8S7dwHa395BJbk5VJQTr0GB3Xv0xkoer35VujqiIDrqhb5CkseDMdLviIUwqV3Q0F5oZNuGkd8OTQokce11YtI3rLbo062NjsdIHzqvpatL/yXWl+31Uh60LjGEWtv1JMkVVjgxc0xFyJw0+FgVW48wrWLpISZcYpwcztC7Y4sp+6RA8qCylonJgsXQjg1ep6EckxPxE1rYHQxSCOOcjHcA/F5oqEUZJGi02mtdAk0JUeQhiC2VUTY+9tY0Dtcc7Xz8GVMas8zepgbyFYbr03q6zUj1vFga1hvYnFgg6dStubmxCRMecyMfw2kd1nyDh/CEw4hOgFYAs1acO043pWYg+kr6OgCAiCC8InaCkQZnVlvXSPhX7VQ5gFYMB5EQcdFF7pNde3eIaQtm5njAq7511fOJNEmB1Atw9udEYqP7F3HcQizITQP2b1IVxk0TLlTh7+zbGcAs1F9ZQ1nlVZQxvpQqL7mQJpx1GuWffDy2lAiLL/yo/4WeUHv88sAzjlPBrAJy+9p0voP9EdqYClPqHh8+KtzTB5JRvUrrAd1xLvJdxaXhCUICcVLTKmh6KLAKxu2+BPqkt8K0Cg9Nogcmjjh1OMKLMpPEzFXp6iQNi+mImRQz2LwTvkQ5R0+hHPEGHqdXp9ywfGmsHO8uOHmG2hvM7e1ovTSrOPfyiQtDWMIIit36z00SqVoppgllpPFS0rT4N/MEgEPVxog82J2JRKn7s42UObV/9hvNQKTrg3eit+7cwueiNzER0+4tKqv+OZT55hh606hwDNz940vhciswpUtFZrBFrNfWkdbdQ2pQw1ZQDuVMq6XcWdOj7NFPd2bXnf2TaEJCJLh5yTuSj89KSI671eC5U9VyAFtH3bu6aNuKR1KijaISb1ExuTPDDggf1onjv/XDuLrFjdjKalp+B/Ylu3WzWnbanKpEr46gMwVSf+fDm1OP5YitheL1l3+NrrpsruA/qgHWfHF27dmrkwkRalh0F9p1S7I8o7Q9L/VS47P7jLIdpeedeDqFdu/Sy4otq0StbF37hO6aEwQZU7/0Wu2y62brxAl/DMdIQSe+sgGX1o0JZQa9LZtg2Sk0KK+hELhkejtZ+VDj4rl2QRR8is/ykm+CL6Wn83qadkZFbPnLSurZ3r/M6HjrlSiIzOPlBWWV34wSJ0RMNTJCi6Mf72GP8vjIvdk1w+ehDU/9G2VmhBfrZrTDm8frcWanKrEOvue88WrQ9wnS+317iUQG99hNuVmuXPULnHA7HQ/CXG+ea6HkYrXBdpW03v7xzqC4YXLeCdDKvs1lQSTMrNjpUNr6J5BZ02deUf3tq/7TiInhZCe+AL8UE4f34dwMG/T4zLi7i+adNAZAhEicvRInmEhqXJihBOXn8PTaBhETk/WuytW/EDxPefLN13F5PdSw+MKIJmi9GgXbVAoB1ODB8E/EQ4iLnwA7eBDvUCsDAW9953XKO2k2hZoaBXvqiVn8i+WGr3baT8xAFGUsAdnctH1Tfumk82ViL4hCRiETm6pXf+Nso+wRTWcSfzqxQpXLj0CTLFmWhLLNLkYXxaaF6i/CZ2FIeLL1IHklmF3xS+rejZBR69vtx29Z2RKUGCvRmFQOHiWYqZS1vPlKac60Y2e4cnIqcGpAYm5PyD2hpMFfWrW09Pwlb0QZGEQsASnKtjRtfxGfY/m+2edYfvHPSwzfrjKof1iSoT0dWbUZz8Yyh/bcjfvzY9MsxjV8IuZCVrE66okX5bDzcrXF8lEy6OJrRRc9935fwkfRjEjkqfWRmO2r6WQnkduBXdt+D+Pw/cR0cfD4N7d9m+Z/+bjErFG6Z48ydqYSqVytX7QMT/71kXs7V5T7KatY9VpsGZho4QKaH5tmJY4T8L+1QueExrJGRpgLMC9ZfGHN1u1N1xcV5NDsWUfSQoyLRu85RsrZvW7bt5888KH6Ya69sivO76poGrV3B2jfwQ7yuGWqHl/Uz56T4pbleyIJvOGi01Wi30Xu7V35Wnni6rsSyyjcdS0Axj8bgdNuV+Vjj9soYYvUNpCC+8pVT9yA09cnoSX4aODwBbGNJX5GwYP1xfic+OMWMF9/8NXM0efwvOniqWpIegZdbvsQEnzNm10+NW5cFHLwfQv9aoCW2YQRm0l8hVE7UpHuCEhRMXrmBmzjvpkKIZLxiGhZJ1xiAeyKqPi8VWwQmur3+WKTMLHm7X6ecatI5DsuLVEV5SV0ePxmZlwJoxve7SI+j41/vDORQut1XQme2YnppvecByW36tAqmHKOZtozD9Fi4Qi08nYwuC0hedRucS7nvKzaeX/BGJavcPktzFDrHAmDyY2rYtWaxLKcL5TVRrkBVtXmgV3+B9fE1d9J5JfKe1uTncSK8W7F7Xj2Vyamj8Y99PUOHcSm+ZmKJq9zDCLx+5OBKNoEEC9zACKXJf7L4e6TIQEphMtyZXwHJs35vDkFLcRyY414qDi/XVKVrDXwQh3rhC2c7G/JFXuuNirLbfpmdT743g8rf2zARrBRHU7Thwwk3gMUZxzOA5ivORViaOX4o353xqWCh9q4CS4sNtcRP07bXMTOYezV6LIllo/SuPgCDCOTY9OsxHHa4VdW6IZKM6QxMrHyjs1/vRPj1L8kpg/PPe+G5l2bVXv2A4I/Fvz3oDHXOaqLU4fsplmsdJU4dJY0hOoXvW9X0/Fwb3BPXD0zKcMUJw5ZI2Plya6be6vEpFNgogw7JJbeaVxov+xi0yIgqvWLf+wYROKqzNSvmYGoNC46zy6Iom2Mj4w26nU57UyzcnzL895uopswAbkFzUnZNggekAM4l3VTZu3cP6Jj9fWI2rDoFoxdAxbtZvLF5uHw8lVY9Bt6XFAnUxoWf4bX9HDkwEbgfAf4VqOcLqeNko5IU2paEyXgjX/N7+7hYvy8ABOSr8I/adsC4Jty4sleA3/n6ows9XlWOh/PSDhgcxjfyGPXRu7tX/nvsSwY4HKM5aM0LFqKh/Gh2DQrcXTsD+SJq4Z17RgrR8qADPsf5XLysB1swqq9sZWIeMOmpz5xM+lION3hTAj/EmnEPTSAVDzEGhQOu3ykIsE9LtNfXPzVrlh6pX7RCniul8Wm2YvzpwDioE50aKPYWyq3xZvzPdjyCp+sslXQObFjz46okh+4JEfp0u5An1+M/w0gLHgv3q6vX7wNr2WskCtWxa2flL4j/ZbENTFI4P8gngXHC2w8G2+5KwcHEc52MTzYAxGNgzm9x1IbU0jkWCN547knqFrG05B6gqE8XH2Ja/vvFPn7Qpc9qPE829N3UbbI8/Bcmdr1o9xMKryemGu+SHcSAOImLDNOYpWPmn7Vi9cvyYNlqMcDk22rHk6tstdVwYr/HGdBbPFwQOwISH3h3fD5ToA4qPnQtC7SlA4YSbfWrs6XQjYf8BxpHXnZZ3rTJFcu/pOfeP+qzTY3yy7lOFb6eMNg5bCcwWdX6JrB6BLzUeZWjI2OJ1+J/Kze2558CMZqw6bLrIAoaCUpC55/L06LhqRc+UkSwEjULrJMA6NenVaAiBkqzq7i/OpQQOTUg/e25lkBUeyaOAERQ/s+yeNabtqwYcp0OkZafsFHyC3JOTjs24yJDNe1yyt/RgovpCDVUIiLPdr+IBH2GNlO/L6AVQviIZBRPg9gOhW1j7fEl7CKx97rr8k4poTYg3h2bAesNO4aaZMaEdJZ7zB+BIb0CI9BrwxftWdSNpbe/ZroYs04MIQvWpiwkfBf2zEdRNvbiQky8W+5KlY/lZCY9BaL/yU4uHVK0kyzRLFurFx9nxnJcOY5Mq1oqEn3JxdXkjNJdg16CC9aWJIzoIkF0MShgQhBb8Uy4+EoY5MINo2L0TZnYEj8VhPWw57lDEjGP3AimdBKyWW+zyteJ5ddeQARr2bbf17ixeKKcKvcCe/PDfEZye/UgPwIcgqS5xqnwuHxtqvisZXGFMOf4wxIif7oVDRJygSYBZi4uAewEGmSq1CfHA3ItJnAVfEWM07rIMDL9qtQ/eI3+c4lM4zY6GAz9k9G+WbpLhf/gVn+SOTZNpERobAb0IXZZGbk3smVaz2YBOHVOC0E8DzQwlwnbAaUESCqapeu2WLGHBugPXfjLM4dscc4eOOSWfAg9b/yFFtgsDjnD8CL893ByIY73zGQwfoL/iQxD3bMx1aIgCikEiY6EciwtPpXS36CpcJ/YXkrq53qhzDjVeE8G385tWH7azJ2TjBrG93gzLRCZk1re5RrYdM1uk3or13MioUmDh5YPsbO36pBpVPt0rDOYVWDlxlIgQ2Ym8cCiEIyx0ByRe3hmvFRxYHNHt4UTQcxujFip7JCO8QRWiyJ38c21f2R+9G+OgZSCK5hjOOq7gId1XYIEDXVEYiO5XYx7UrHhYeh4JCAFPKo6kHMDnHCdRSC8BSpodYRBxETpnvZRGteopHqliEDKSb3qtIy4mCKpYWqHEC92DcbwaDvnlTk3DSCVVqqKgVAoh6x8FbbLFWYCiJNC2JH5YBebyr4WeaBE+MwqRcy9kDIcpkRIkwNkBBWdK4SEhpisiOcgkZpePFCGwULIETHWu1GmNSPU9CMlLNIGZC6ZDwEzWyBpgwPmFzF51gwJo9O4E+OplN8sDanFkjUpsFLo4SwZYVrqoIYD4W2qwByNAIey49wZPLS0ajbap0pBzJccd9EBK4yMRkaStC0ACY1cJxA2x2EFCDPG/GR3nms4nH4EsduGCYgww0WXhY1tN/R8oBjAiVmw5oCU+rQVKtq68XY7P0pyju0x3jR1SWdxspX7hy7EIYlG1YgRRVijSkW7Epor34VkyKzIEyyAE/VzbM5rRkfkcc13g3vy89kzVdMTLsAwjyB1EE1CzbkDZjzr2AfE0dDHq0frJ6xkO/shIATyaFVYe9LN3Hs/IfP33h0TmIMZOIUK8yoiKc6sEkPC/fTk+LHty/1KXLgdJyvnQbAqpFWg/mo+ErOJ/CdfirL6htWzvWkWsah8hs5IGMk5RrMppaC4SuGp9VoH6gvgl78Dpsw7Kb1sOmpMd6QNJBjHCCr4qWBtNpTY5wuDeQYB8iqeGkgrfbUGKdLAznGAbIqXhpIqz01xunSQI5xgKyKlwbSak+Ncbo0kGMcIKviHbZAwne6l2u9W6x2xKFOdxgCyT/CZ0Cu8cm8KnPKW7sOdYCsyj8qTnOrwlmlg/Y1YNvkUXyDZ6W39tUxeabGaluc0v0/HccqvEEClkkAAAAASUVORK5CYII=" alt="Have something in mind icon"></div>
                        </div>
                     </div>
                  </div>
               </div>
          
                <div style="display:block" id="FirstPage" >
               <div class="productCatalog-list">
                  <div class="products-catalog" id="0">
                     <div class="catalog-title"></div>
                     <div class="catalog-body">


                           <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
               </div>
                      </div>
                   </div>
               <div class="product-bottom">
                  <div class="havesomething-in-mind__box">
                     <div class="havesomething-in-mind"><span>Have some specific thing in mind ?</span><span>Make a list of items you need</span></div>
                     <div class="havesomething-in-mind__box-image"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHIAAABqCAYAAABgWAWLAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAcqADAAQAAAABAAAAagAAAAADJmxvAAAZLklEQVR4Ae1dCXgcxZV+1T2XpJGs09ZpyZIsY4yJOWxuMFniGBMHcxhsA4kTghMSSDiXwJIAG8jmS2BxyCbBsAlsgrFZMITlcMAsVzCn2YA5fWBbki1fsiTrGmmmu2v/6tGMZkbTre7W6LAz9XnU1VWvXr2qv9+rqlfVbaJ0SPdAugfSPZDugXQPpHsg3QPpHkj3QLoH0j2Q7oF0D6R7IN0D6R5I90C6B0axB9hw111QPGkWI54p6uEkdUkuagsoUlvnnq37h7vufyT+KQeysLjqDJKkLxOjU8H8ZCLmM+5QvpETvUycv0zBjlebm5s7jGnTOWY9kDIgC0qrzmUk38YYHWNWoWke5+sB7DqNqS+27Kp/y5Q2LrMKD8uOntikgtJJvz7QtP1HsWmHc3zIQI4bX1Hjcbl/T4x9JZUdxYm3MU5/UTk9KysdL+zfv78zGf+C0uqzGLGrm5u+ODeSX1hacyMeqF+qpJxs74GIcDj0rq6hiFxQVn2lROx3Q+FhVBbg5MI8L5XxI08OFZZmb4KZ3oTx9jNci5E3BYPuUbj6cX0zwqeotOaHSPuluGeaVIOLDc2OcDn0ro6BRIc9jOZ+c6SazBibgrrwY1+P1tlnTzROLxcUV89kMt2GvHOi+RLLjsYP84gjIGG6VuOpv3is9I3E6HsksVsT5YFpPpiYdrje2wayqLT6obEEog4Mo8JkAGnE/2GWOFKyDjBKKyypvgaTmqVG+WMtPajJH4w1mYZLHsuz1sLimjMxBr08XIKkmi+WMbuad31Rnmq+Y5WfJY0sLCzMJpmvHKuNSCoXp+eTph+midbGSM+4f4fqlhxKfaCR9vihJO9QZR3UtOYX106TZf7xUCsyKj9lUglNqhiv/w60ddLuva20cVMDHezoNioyaDrnvL65aVvVoISHEcGgGilJ/OfD0d6TjplMv7ntcirI9Sdl/0XDHvr7pzvoA/z+tuFzamhqTkqXNJHRr5OmH8aJphpZVFY1g0j+e6rbf/IxdbTy3h/aYiu09Nn/fZ+eWvcONbcm9dbp/P7RJjmRTpQjkWTXzOyC5fBZHpUsz2lacVEurV5+DXm9blssJhSOo9NmTqVli86i446qplBIoc07dg/gMak0//tNTU0fDcg4zBMMNTI3tyrXnSW3prr9q5f/iE6YMTklbDs6A/TC+o30fx9vox0799ElXz+NzjnzmMf8k+ctSkkFhxATQyALS2q+yyS6P5Vt+fH3FtB3oVHDHTiTpmfXfnXYJmjDLb8T/obrSMb4+U4YGpWZfcKRIwKiqJ9x7W4jOQ6F9HcXzixef/7JlXZkTaqRRUVFfmwdpWy3/qi6Cvrv31xHGTbHRTsNiaPlFMrivlI25UwbU904DpZv+PZXcrvUnnOxlbZUL8RpR5bbdy2bdGabGZMP5xydFcjMOUIjqoPlq8NBmDpfycRjMqpqp4SaGnQFk3PyFE9x2braK2+cZ8ZL5CUFMr+kaq4syWsHK2wlv2biBHrit9dRbnaWFfIU0vBrMVYuTwXDji1rZ6Nnz+BcftpfNyfqv+3cvHYpZ/xefe80WhGvJ+5aIOg2HHecOzTRXctIqtOI1aGz6zgDcIiDvDhaBBH/tGOJhYLEewOxyXrcXTghMGHGCVPzzrmgfkBmX0JSIAtLq+/A/t9PjQrZSf+fFTfR9CkVdoqkhBbLkKezJ5+9YCjMurY+v4Br7F487lVRPoz/mjTXw0QKdoHYDJEOB0R7+8dbHtj93Et7Qy2torE6YLhWooNNVwaifMbEapKxPIgEaCG58Qt8/hFpPWFg3cXlzUfcdFdRhCbxauQQODaR0Mn9eXNmjgqIuqycTkMH43nErqSDoGsbJ7FlFw04fvIaBZWvMDfDWaBwRuuGjdruZ9blyBneG/zVE8lz3FGkBHrowPr3ouXMIgx21ZOTR2pHm06WN/d8yj7pTD2uBbpp38P3UXDPLgrt2Vm49cF776u94tqkC3AjIIXqDzksW5TSYzy25MEDnh/c8aI4VfC5rYIgDmxfW6UouskMF+X0ISLLtd7gPbLPmy8S1Z4e2rvudf2MZ+2PLidPfm6Ytu+vVSBzZ50KkHbppbJPPCMKokiQMjKpcNEV1LT8dj0/uLP+SkSSApl01oqneMhAlhTl0RHVpboAo/UnqGq66bNbvxqi22PHvWDrwToo4EMREAW/0IE2Kp0/h0q/PicKYrC1jQ688S7V/8m6v15pCc/HJF8GjZs9cE7jyiugrBmz9Cao7a0uoZXJ2pNEI8XRQmchx59Bt/7gAppz6tE0LhtnkoVR0y1bjH1KYC2c4+98uIU+3Rp+KiPZJ86opak15WE+kUS7V05HWC3y0TVLZxNpZ0Dg2VoodIbkcUeLevLHZURv+iK+svBcRWhm24aN1IKfCC6flzq3N/RRmV/EBId3HtSJBFhCA5MFAXDXB+/qWVpvQKA9QCsHAJldFsL0ctDxeUB9AsTXV90R3/E6fvijAzqgiJ7ws/9YQ+XF+XRijLencU8LvfC3jfSvyKsoLqAVdy6LK/zC3z6kh9a8Su2d3ZSTFe7jnXsOUHlJIR1ZW6Y/SCfOgBJxqoormHCz8ZqlCyCcOEa5AMSwjYx8peMpFsSEItFboX1dXzTAxAbIP6mSCk4Na40g6PqinrY98EiU1ijiyswkJQrkiUZkJLTSlZtPSlsLBXc2VCcjHACkq9urUPIHI1n5aNqKO6+IBzGag4ixQtLdN18WS6nHjZsUJq0A8L+75TLqfnU9tX+yGR3vI++UqcQnVtBmdM4Ta9+hsgkFVFFSEFabhBr+vmBGrlw5/U3Mg6YmZJHL4jLJk5dLnuPjx8UILznDolFTFL2IAMlTYn6YIeOI6dTx9mtiecJan1tTmbgUGQBka+u29qLMmohMlq7Cdyo0YKTCkZMraPv9f9ZNmCvLT148sSKwhl00vSCP5lx3MUleD5SN5wrQAiz7SwBtNgzDDMb5DMWfXeWSkk4PyJUDg2QzCPPa/vFmav90E/U07SWMqZY4KC37dTqx1BgsSL5+7Wo72HYe6JfHlhkAJDI5ptktGOz12VkssVH8wrmD6ZBRSWfpwnSJcUhM3b15hVEmXFUpsKuJdj35V9JCvRRobDo2JPlbcahZpwlbekZuf060TGLEV2a4VEskpc5t9RRqOUhd23aQ0NDCU2YBxDba/cxL+qx2QIGYBJd/XPTOU2yujYLQWzUZf419NMmAFOW24tdv9EWKSSgvzjPJTX1WM2aGIrj8fmKyDNBCFNi9E08gHF4Igb1N+hV/+mcskRRcZcwQjYLbomkV5f3VlUQYsfKOPzrKLosqIctezF7fi6Yli3iFKQ32JMtylJYUSGjj5+BmGcgja517brai0W6Xi7IxrmS43fDHeijQG8S7dwHa395BJbk5VJQTr0GB3Xv0xkoer35VujqiIDrqhb5CkseDMdLviIUwqV3Q0F5oZNuGkd8OTQokce11YtI3rLbo062NjsdIHzqvpatL/yXWl+31Uh60LjGEWtv1JMkVVjgxc0xFyJw0+FgVW48wrWLpISZcYpwcztC7Y4sp+6RA8qCylonJgsXQjg1ep6EckxPxE1rYHQxSCOOcjHcA/F5oqEUZJGi02mtdAk0JUeQhiC2VUTY+9tY0Dtcc7Xz8GVMas8zepgbyFYbr03q6zUj1vFga1hvYnFgg6dStubmxCRMecyMfw2kd1nyDh/CEw4hOgFYAs1acO043pWYg+kr6OgCAiCC8InaCkQZnVlvXSPhX7VQ5gFYMB5EQcdFF7pNde3eIaQtm5njAq7511fOJNEmB1Atw9udEYqP7F3HcQizITQP2b1IVxk0TLlTh7+zbGcAs1F9ZQ1nlVZQxvpQqL7mQJpx1GuWffDy2lAiLL/yo/4WeUHv88sAzjlPBrAJy+9p0voP9EdqYClPqHh8+KtzTB5JRvUrrAd1xLvJdxaXhCUICcVLTKmh6KLAKxu2+BPqkt8K0Cg9Nogcmjjh1OMKLMpPEzFXp6iQNi+mImRQz2LwTvkQ5R0+hHPEGHqdXp9ywfGmsHO8uOHmG2hvM7e1ovTSrOPfyiQtDWMIIit36z00SqVoppgllpPFS0rT4N/MEgEPVxog82J2JRKn7s42UObV/9hvNQKTrg3eit+7cwueiNzER0+4tKqv+OZT55hh606hwDNz940vhciswpUtFZrBFrNfWkdbdQ2pQw1ZQDuVMq6XcWdOj7NFPd2bXnf2TaEJCJLh5yTuSj89KSI671eC5U9VyAFtH3bu6aNuKR1KijaISb1ExuTPDDggf1onjv/XDuLrFjdjKalp+B/Ylu3WzWnbanKpEr46gMwVSf+fDm1OP5YitheL1l3+NrrpsruA/qgHWfHF27dmrkwkRalh0F9p1S7I8o7Q9L/VS47P7jLIdpeedeDqFdu/Sy4otq0StbF37hO6aEwQZU7/0Wu2y62brxAl/DMdIQSe+sgGX1o0JZQa9LZtg2Sk0KK+hELhkejtZ+VDj4rl2QRR8is/ykm+CL6Wn83qadkZFbPnLSurZ3r/M6HjrlSiIzOPlBWWV34wSJ0RMNTJCi6Mf72GP8vjIvdk1w+ehDU/9G2VmhBfrZrTDm8frcWanKrEOvue88WrQ9wnS+317iUQG99hNuVmuXPULnHA7HQ/CXG+ea6HkYrXBdpW03v7xzqC4YXLeCdDKvs1lQSTMrNjpUNr6J5BZ02deUf3tq/7TiInhZCe+AL8UE4f34dwMG/T4zLi7i+adNAZAhEicvRInmEhqXJihBOXn8PTaBhETk/WuytW/EDxPefLN13F5PdSw+MKIJmi9GgXbVAoB1ODB8E/EQ4iLnwA7eBDvUCsDAW9953XKO2k2hZoaBXvqiVn8i+WGr3baT8xAFGUsAdnctH1Tfumk82ViL4hCRiETm6pXf+Nso+wRTWcSfzqxQpXLj0CTLFmWhLLNLkYXxaaF6i/CZ2FIeLL1IHklmF3xS+rejZBR69vtx29Z2RKUGCvRmFQOHiWYqZS1vPlKac60Y2e4cnIqcGpAYm5PyD2hpMFfWrW09Pwlb0QZGEQsASnKtjRtfxGfY/m+2edYfvHPSwzfrjKof1iSoT0dWbUZz8Yyh/bcjfvzY9MsxjV8IuZCVrE66okX5bDzcrXF8lEy6OJrRRc9935fwkfRjEjkqfWRmO2r6WQnkduBXdt+D+Pw/cR0cfD4N7d9m+Z/+bjErFG6Z48ydqYSqVytX7QMT/71kXs7V5T7KatY9VpsGZho4QKaH5tmJY4T8L+1QueExrJGRpgLMC9ZfGHN1u1N1xcV5NDsWUfSQoyLRu85RsrZvW7bt5888KH6Ya69sivO76poGrV3B2jfwQ7yuGWqHl/Uz56T4pbleyIJvOGi01Wi30Xu7V35Wnni6rsSyyjcdS0Axj8bgdNuV+Vjj9soYYvUNpCC+8pVT9yA09cnoSX4aODwBbGNJX5GwYP1xfic+OMWMF9/8NXM0efwvOniqWpIegZdbvsQEnzNm10+NW5cFHLwfQv9aoCW2YQRm0l8hVE7UpHuCEhRMXrmBmzjvpkKIZLxiGhZJ1xiAeyKqPi8VWwQmur3+WKTMLHm7X6ecatI5DsuLVEV5SV0ePxmZlwJoxve7SI+j41/vDORQut1XQme2YnppvecByW36tAqmHKOZtozD9Fi4Qi08nYwuC0hedRucS7nvKzaeX/BGJavcPktzFDrHAmDyY2rYtWaxLKcL5TVRrkBVtXmgV3+B9fE1d9J5JfKe1uTncSK8W7F7Xj2Vyamj8Y99PUOHcSm+ZmKJq9zDCLx+5OBKNoEEC9zACKXJf7L4e6TIQEphMtyZXwHJs35vDkFLcRyY414qDi/XVKVrDXwQh3rhC2c7G/JFXuuNirLbfpmdT743g8rf2zARrBRHU7Thwwk3gMUZxzOA5ivORViaOX4o353xqWCh9q4CS4sNtcRP07bXMTOYezV6LIllo/SuPgCDCOTY9OsxHHa4VdW6IZKM6QxMrHyjs1/vRPj1L8kpg/PPe+G5l2bVXv2A4I/Fvz3oDHXOaqLU4fsplmsdJU4dJY0hOoXvW9X0/Fwb3BPXD0zKcMUJw5ZI2Plya6be6vEpFNgogw7JJbeaVxov+xi0yIgqvWLf+wYROKqzNSvmYGoNC46zy6Iom2Mj4w26nU57UyzcnzL895uopswAbkFzUnZNggekAM4l3VTZu3cP6Jj9fWI2rDoFoxdAxbtZvLF5uHw8lVY9Bt6XFAnUxoWf4bX9HDkwEbgfAf4VqOcLqeNko5IU2paEyXgjX/N7+7hYvy8ABOSr8I/adsC4Jty4sleA3/n6ows9XlWOh/PSDhgcxjfyGPXRu7tX/nvsSwY4HKM5aM0LFqKh/Gh2DQrcXTsD+SJq4Z17RgrR8qADPsf5XLysB1swqq9sZWIeMOmpz5xM+lION3hTAj/EmnEPTSAVDzEGhQOu3ykIsE9LtNfXPzVrlh6pX7RCniul8Wm2YvzpwDioE50aKPYWyq3xZvzPdjyCp+sslXQObFjz46okh+4JEfp0u5An1+M/w0gLHgv3q6vX7wNr2WskCtWxa2flL4j/ZbENTFI4P8gngXHC2w8G2+5KwcHEc52MTzYAxGNgzm9x1IbU0jkWCN547knqFrG05B6gqE8XH2Ja/vvFPn7Qpc9qPE829N3UbbI8/Bcmdr1o9xMKryemGu+SHcSAOImLDNOYpWPmn7Vi9cvyYNlqMcDk22rHk6tstdVwYr/HGdBbPFwQOwISH3h3fD5ToA4qPnQtC7SlA4YSbfWrs6XQjYf8BxpHXnZZ3rTJFcu/pOfeP+qzTY3yy7lOFb6eMNg5bCcwWdX6JrB6BLzUeZWjI2OJ1+J/Kze2558CMZqw6bLrIAoaCUpC55/L06LhqRc+UkSwEjULrJMA6NenVaAiBkqzq7i/OpQQOTUg/e25lkBUeyaOAERQ/s+yeNabtqwYcp0OkZafsFHyC3JOTjs24yJDNe1yyt/RgovpCDVUIiLPdr+IBH2GNlO/L6AVQviIZBRPg9gOhW1j7fEl7CKx97rr8k4poTYg3h2bAesNO4aaZMaEdJZ7zB+BIb0CI9BrwxftWdSNpbe/ZroYs04MIQvWpiwkfBf2zEdRNvbiQky8W+5KlY/lZCY9BaL/yU4uHVK0kyzRLFurFx9nxnJcOY5Mq1oqEn3JxdXkjNJdg16CC9aWJIzoIkF0MShgQhBb8Uy4+EoY5MINo2L0TZnYEj8VhPWw57lDEjGP3AimdBKyWW+zyteJ5ddeQARr2bbf17ixeKKcKvcCe/PDfEZye/UgPwIcgqS5xqnwuHxtqvisZXGFMOf4wxIif7oVDRJygSYBZi4uAewEGmSq1CfHA3ItJnAVfEWM07rIMDL9qtQ/eI3+c4lM4zY6GAz9k9G+WbpLhf/gVn+SOTZNpERobAb0IXZZGbk3smVaz2YBOHVOC0E8DzQwlwnbAaUESCqapeu2WLGHBugPXfjLM4dscc4eOOSWfAg9b/yFFtgsDjnD8CL893ByIY73zGQwfoL/iQxD3bMx1aIgCikEiY6EciwtPpXS36CpcJ/YXkrq53qhzDjVeE8G385tWH7azJ2TjBrG93gzLRCZk1re5RrYdM1uk3or13MioUmDh5YPsbO36pBpVPt0rDOYVWDlxlIgQ2Ym8cCiEIyx0ByRe3hmvFRxYHNHt4UTQcxujFip7JCO8QRWiyJ38c21f2R+9G+OgZSCK5hjOOq7gId1XYIEDXVEYiO5XYx7UrHhYeh4JCAFPKo6kHMDnHCdRSC8BSpodYRBxETpnvZRGteopHqliEDKSb3qtIy4mCKpYWqHEC92DcbwaDvnlTk3DSCVVqqKgVAoh6x8FbbLFWYCiJNC2JH5YBebyr4WeaBE+MwqRcy9kDIcpkRIkwNkBBWdK4SEhpisiOcgkZpePFCGwULIETHWu1GmNSPU9CMlLNIGZC6ZDwEzWyBpgwPmFzF51gwJo9O4E+OplN8sDanFkjUpsFLo4SwZYVrqoIYD4W2qwByNAIey49wZPLS0ajbap0pBzJccd9EBK4yMRkaStC0ACY1cJxA2x2EFCDPG/GR3nms4nH4EsduGCYgww0WXhY1tN/R8oBjAiVmw5oCU+rQVKtq68XY7P0pyju0x3jR1SWdxspX7hy7EIYlG1YgRRVijSkW7Epor34VkyKzIEyyAE/VzbM5rRkfkcc13g3vy89kzVdMTLsAwjyB1EE1CzbkDZjzr2AfE0dDHq0frJ6xkO/shIATyaFVYe9LN3Hs/IfP33h0TmIMZOIUK8yoiKc6sEkPC/fTk+LHty/1KXLgdJyvnQbAqpFWg/mo+ErOJ/CdfirL6htWzvWkWsah8hs5IGMk5RrMppaC4SuGp9VoH6gvgl78Dpsw7Kb1sOmpMd6QNJBjHCCr4qWBtNpTY5wuDeQYB8iqeGkgrfbUGKdLAznGAbIqXhpIqz01xunSQI5xgKyKlwbSak+Ncbo0kGMcIKviHbZAwne6l2u9W6x2xKFOdxgCyT/CZ0Cu8cm8KnPKW7sOdYCsyj8qTnOrwlmlg/Y1YNvkUXyDZ6W39tUxeabGaluc0v0/HccqvEEClkkAAAAASUVORK5CYII=" alt="Have something in mind icon"></div>
                  </div>
                  <div class="product-bottom__btnBox">
                      <button class="catalog-custom-btn" onclick="myFunction()" >Create list</button>

                  </div>
               <div id="Result"></div> 
               </div>
                </div>
                     
                <div style="display:none" id="SecondPage">
                     <div class="cart-container">
            <div class="cart-header"><span><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAUGVYSWZNTQAqAAAACAACARIAAwAAAAEAAQAAh2kABAAAAAEAAAAmAAAAAAADoAEAAwAAAAEAAQAAoAIABAAAAAEAAABgoAMABAAAAAEAAABgAAAAALvucVAAAAICaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA1LjQuMCI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyI+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj45NjwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj45NjwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgrL2SuWAAACQklEQVR4Ae3cQU7DMBCF4RSxQ0gcjguwY8cJEOUIXIsNG66DwhgRqYqg9th+zbT+I1Vt1cnE/l6sVqVkmtgQQAABBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAgcAC8zzv7fYceIiXO7RffLv72Z4ud6YBZ2bk6cxfb4RwiqxM/S/8JYzHU4xh2GNk8JcQHoYFUk68EH8J4V45luF6O/E/rf5uOCTVhMFXyRb0Bb8ASVUCvkq2oC/4BUiqEvBVsgV9nfgfVs+nnQLXopIK/NuixhTlBcDPG8kqwJfR5huDnzeSVYAvo803Bj9vJKsAX0abbwx+3khW4cR/t3o+5/dKowL/ptexh+8D/oanAPjgbyiw4aE588HfUMB36J2v/Hh1OvOt4uV41WW9urOtZUZXLTsf7jsi/uH8ax93CQD8Wv5pag4A/Hr8tGdzAG2HZ+/mAOw9aG+Mr1DWCTQHkA5LCHX4aa8uAaRGhJAUAmzpTdlupVv62plvPnvnZqiE0BvV248QvGKCekIQoHpbEoJXTFBfEQJ/D+6dgzOE9MtnQiCE3gIB+rESCCGAQIAhsBIIIYBAgCGwEgghgECAIbASCCGAQIAhVKwE/le4d27OELg8Te8AUj9CUKg6exKCE0xRTggKVWdPQnCCKcqdIXDRvg1D4LKVCvylZ2YlcOHWBUp5/08IXLpYib7uvQrhbPGv1xM7l+fpt6gWQhrulz1+O5dxM04EEEAAAQQQQAABBBBAAAEEEEAAAQQQQAABBBAYT+AbOn/y2KLqAkoAAAAASUVORK5CYII=" alt="back"></span> <span class="ml10">Items in cart</span></div>
            <div class="cart">
               <div class="cart-item__header">
                  <div class="cart-item__box">
                     <div class="cart-item__box-head"><span>Item name</span><span>Quantity</span></div>
                     <div class="cart-item__box-content">

                                <%--<asp:PlaceHolder ID="PlaceHolde2" runat="server"></asp:PlaceHolder>--%>
                         <div id="PlaceHolde2"></div>


                       <%-- <div class="catalog-item-container">
                           <div class="cart-item">
                              <div class="cart-item__left"><span class="cart-item__left-name">लॅपटॉप 1</span><label class="cart-item__left-amount">₹ 1500</label></div>
                              <div class="cart-item__right">
                                 <div class="addtocart-btn" style="background-color: rgb(255, 255, 255);">
                                    <div class="addtocart-btn__changeIcon"onclick="buttonClicks2()" ><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAFCAYAAABFA8wzAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAABQAAAADMlWz1AAAAGUlEQVQYGWN8FRj4n4GKgImKZoGNGoEGAgAJJgKVJxPQJQAAAABJRU5ErkJggg==" alt="Decreament button"></div>
                                    <div class="addtocart-btn__text" id="Value">0</div>
                                    <div class="addtocart-btn__changeIcon" onclick="buttonClick2()"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC" alt="Increment button"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="catalog-item-container">
                           <div class="cart-item">
                              <div class="cart-item__left"><span class="cart-item__left-name">Dkiyeraabhay</span><label class="cart-item__left-amount">₹ 39291</label></div>
                              <div class="cart-item__right">
                                 <div class="addtocart-btn" style="background-color: rgb(255, 255, 255);">
                                    <div class="addtocart-btn__changeIcon"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAFCAYAAABFA8wzAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAABQAAAADMlWz1AAAAGUlEQVQYGWN8FRj4n4GKgImKZoGNGoEGAgAJJgKVJxPQJQAAAABJRU5ErkJggg==" alt="Decreament button"></div>
                                    <div class="addtocart-btn__text">2</div>
                                    <div class="addtocart-btn__changeIcon"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC" alt="Increment button"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="catalog-item-container">
                           <div class="cart-item">
                              <div class="cart-item__left"><span class="cart-item__left-name">Pencil</span><label class="cart-item__left-amount">₹ 5</label></div>
                              <div class="cart-item__right">
                                 <div class="addtocart-btn" style="background-color: rgb(255, 255, 255);">
                                    <div class="addtocart-btn__changeIcon"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAFCAYAAABFA8wzAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAABQAAAADMlWz1AAAAGUlEQVQYGWN8FRj4n4GKgImKZoGNGoEGAgAJJgKVJxPQJQAAAABJRU5ErkJggg==" alt="Decreament button"></div>
                                    <div class="addtocart-btn__text">2</div>
                                    <div class="addtocart-btn__changeIcon"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC" alt="Increment button"></div>
                                 </div>
                              </div>
                           </div>
                        </div>--%>
                        <div class="create-paperlist"></div>
                     </div>
                  </div>
                  <div>
                     <div class="havesomething-in-mind__box">
                        <div class="havesomething-in-mind"><span>Have some specific thing in mind ?</span><span>Make a list of items you need</span></div>
                        <div class="havesomething-in-mind__box-image"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHIAAABqCAYAAABgWAWLAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAcqADAAQAAAABAAAAagAAAAADJmxvAAAZLklEQVR4Ae1dCXgcxZV+1T2XpJGs09ZpyZIsY4yJOWxuMFniGBMHcxhsA4kTghMSSDiXwJIAG8jmS2BxyCbBsAlsgrFZMITlcMAsVzCn2YA5fWBbki1fsiTrGmmmu2v/6tGMZkbTre7W6LAz9XnU1VWvXr2qv9+rqlfVbaJ0SPdAugfSPZDugXQPpHsg3QPpHkj3QLoH0j2Q7oF0D6R7IN0D6R5I90C6B0axB9hw111QPGkWI54p6uEkdUkuagsoUlvnnq37h7vufyT+KQeysLjqDJKkLxOjU8H8ZCLmM+5QvpETvUycv0zBjlebm5s7jGnTOWY9kDIgC0qrzmUk38YYHWNWoWke5+sB7DqNqS+27Kp/y5Q2LrMKD8uOntikgtJJvz7QtP1HsWmHc3zIQI4bX1Hjcbl/T4x9JZUdxYm3MU5/UTk9KysdL+zfv78zGf+C0uqzGLGrm5u+ODeSX1hacyMeqF+qpJxs74GIcDj0rq6hiFxQVn2lROx3Q+FhVBbg5MI8L5XxI08OFZZmb4KZ3oTx9jNci5E3BYPuUbj6cX0zwqeotOaHSPuluGeaVIOLDc2OcDn0ro6BRIc9jOZ+c6SazBibgrrwY1+P1tlnTzROLxcUV89kMt2GvHOi+RLLjsYP84gjIGG6VuOpv3is9I3E6HsksVsT5YFpPpiYdrje2wayqLT6obEEog4Mo8JkAGnE/2GWOFKyDjBKKyypvgaTmqVG+WMtPajJH4w1mYZLHsuz1sLimjMxBr08XIKkmi+WMbuad31Rnmq+Y5WfJY0sLCzMJpmvHKuNSCoXp+eTph+midbGSM+4f4fqlhxKfaCR9vihJO9QZR3UtOYX106TZf7xUCsyKj9lUglNqhiv/w60ddLuva20cVMDHezoNioyaDrnvL65aVvVoISHEcGgGilJ/OfD0d6TjplMv7ntcirI9Sdl/0XDHvr7pzvoA/z+tuFzamhqTkqXNJHRr5OmH8aJphpZVFY1g0j+e6rbf/IxdbTy3h/aYiu09Nn/fZ+eWvcONbcm9dbp/P7RJjmRTpQjkWTXzOyC5fBZHpUsz2lacVEurV5+DXm9blssJhSOo9NmTqVli86i446qplBIoc07dg/gMak0//tNTU0fDcg4zBMMNTI3tyrXnSW3prr9q5f/iE6YMTklbDs6A/TC+o30fx9vox0799ElXz+NzjnzmMf8k+ctSkkFhxATQyALS2q+yyS6P5Vt+fH3FtB3oVHDHTiTpmfXfnXYJmjDLb8T/obrSMb4+U4YGpWZfcKRIwKiqJ9x7W4jOQ6F9HcXzixef/7JlXZkTaqRRUVFfmwdpWy3/qi6Cvrv31xHGTbHRTsNiaPlFMrivlI25UwbU904DpZv+PZXcrvUnnOxlbZUL8RpR5bbdy2bdGabGZMP5xydFcjMOUIjqoPlq8NBmDpfycRjMqpqp4SaGnQFk3PyFE9x2braK2+cZ8ZL5CUFMr+kaq4syWsHK2wlv2biBHrit9dRbnaWFfIU0vBrMVYuTwXDji1rZ6Nnz+BcftpfNyfqv+3cvHYpZ/xefe80WhGvJ+5aIOg2HHecOzTRXctIqtOI1aGz6zgDcIiDvDhaBBH/tGOJhYLEewOxyXrcXTghMGHGCVPzzrmgfkBmX0JSIAtLq+/A/t9PjQrZSf+fFTfR9CkVdoqkhBbLkKezJ5+9YCjMurY+v4Br7F487lVRPoz/mjTXw0QKdoHYDJEOB0R7+8dbHtj93Et7Qy2torE6YLhWooNNVwaifMbEapKxPIgEaCG58Qt8/hFpPWFg3cXlzUfcdFdRhCbxauQQODaR0Mn9eXNmjgqIuqycTkMH43nErqSDoGsbJ7FlFw04fvIaBZWvMDfDWaBwRuuGjdruZ9blyBneG/zVE8lz3FGkBHrowPr3ouXMIgx21ZOTR2pHm06WN/d8yj7pTD2uBbpp38P3UXDPLgrt2Vm49cF776u94tqkC3AjIIXqDzksW5TSYzy25MEDnh/c8aI4VfC5rYIgDmxfW6UouskMF+X0ISLLtd7gPbLPmy8S1Z4e2rvudf2MZ+2PLidPfm6Ytu+vVSBzZ50KkHbppbJPPCMKokiQMjKpcNEV1LT8dj0/uLP+SkSSApl01oqneMhAlhTl0RHVpboAo/UnqGq66bNbvxqi22PHvWDrwToo4EMREAW/0IE2Kp0/h0q/PicKYrC1jQ688S7V/8m6v15pCc/HJF8GjZs9cE7jyiugrBmz9Cao7a0uoZXJ2pNEI8XRQmchx59Bt/7gAppz6tE0LhtnkoVR0y1bjH1KYC2c4+98uIU+3Rp+KiPZJ86opak15WE+kUS7V05HWC3y0TVLZxNpZ0Dg2VoodIbkcUeLevLHZURv+iK+svBcRWhm24aN1IKfCC6flzq3N/RRmV/EBId3HtSJBFhCA5MFAXDXB+/qWVpvQKA9QCsHAJldFsL0ctDxeUB9AsTXV90R3/E6fvijAzqgiJ7ws/9YQ+XF+XRijLencU8LvfC3jfSvyKsoLqAVdy6LK/zC3z6kh9a8Su2d3ZSTFe7jnXsOUHlJIR1ZW6Y/SCfOgBJxqoormHCz8ZqlCyCcOEa5AMSwjYx8peMpFsSEItFboX1dXzTAxAbIP6mSCk4Na40g6PqinrY98EiU1ijiyswkJQrkiUZkJLTSlZtPSlsLBXc2VCcjHACkq9urUPIHI1n5aNqKO6+IBzGag4ixQtLdN18WS6nHjZsUJq0A8L+75TLqfnU9tX+yGR3vI++UqcQnVtBmdM4Ta9+hsgkFVFFSEFabhBr+vmBGrlw5/U3Mg6YmZJHL4jLJk5dLnuPjx8UILznDolFTFL2IAMlTYn6YIeOI6dTx9mtiecJan1tTmbgUGQBka+u29qLMmohMlq7Cdyo0YKTCkZMraPv9f9ZNmCvLT148sSKwhl00vSCP5lx3MUleD5SN5wrQAiz7SwBtNgzDDMb5DMWfXeWSkk4PyJUDg2QzCPPa/vFmav90E/U07SWMqZY4KC37dTqx1BgsSL5+7Wo72HYe6JfHlhkAJDI5ptktGOz12VkssVH8wrmD6ZBRSWfpwnSJcUhM3b15hVEmXFUpsKuJdj35V9JCvRRobDo2JPlbcahZpwlbekZuf060TGLEV2a4VEskpc5t9RRqOUhd23aQ0NDCU2YBxDba/cxL+qx2QIGYBJd/XPTOU2yujYLQWzUZf419NMmAFOW24tdv9EWKSSgvzjPJTX1WM2aGIrj8fmKyDNBCFNi9E08gHF4Igb1N+hV/+mcskRRcZcwQjYLbomkV5f3VlUQYsfKOPzrKLosqIctezF7fi6Yli3iFKQ32JMtylJYUSGjj5+BmGcgja517brai0W6Xi7IxrmS43fDHeijQG8S7dwHa395BJbk5VJQTr0GB3Xv0xkoer35VujqiIDrqhb5CkseDMdLviIUwqV3Q0F5oZNuGkd8OTQokce11YtI3rLbo062NjsdIHzqvpatL/yXWl+31Uh60LjGEWtv1JMkVVjgxc0xFyJw0+FgVW48wrWLpISZcYpwcztC7Y4sp+6RA8qCylonJgsXQjg1ep6EckxPxE1rYHQxSCOOcjHcA/F5oqEUZJGi02mtdAk0JUeQhiC2VUTY+9tY0Dtcc7Xz8GVMas8zepgbyFYbr03q6zUj1vFga1hvYnFgg6dStubmxCRMecyMfw2kd1nyDh/CEw4hOgFYAs1acO043pWYg+kr6OgCAiCC8InaCkQZnVlvXSPhX7VQ5gFYMB5EQcdFF7pNde3eIaQtm5njAq7511fOJNEmB1Atw9udEYqP7F3HcQizITQP2b1IVxk0TLlTh7+zbGcAs1F9ZQ1nlVZQxvpQqL7mQJpx1GuWffDy2lAiLL/yo/4WeUHv88sAzjlPBrAJy+9p0voP9EdqYClPqHh8+KtzTB5JRvUrrAd1xLvJdxaXhCUICcVLTKmh6KLAKxu2+BPqkt8K0Cg9Nogcmjjh1OMKLMpPEzFXp6iQNi+mImRQz2LwTvkQ5R0+hHPEGHqdXp9ywfGmsHO8uOHmG2hvM7e1ovTSrOPfyiQtDWMIIit36z00SqVoppgllpPFS0rT4N/MEgEPVxog82J2JRKn7s42UObV/9hvNQKTrg3eit+7cwueiNzER0+4tKqv+OZT55hh606hwDNz940vhciswpUtFZrBFrNfWkdbdQ2pQw1ZQDuVMq6XcWdOj7NFPd2bXnf2TaEJCJLh5yTuSj89KSI671eC5U9VyAFtH3bu6aNuKR1KijaISb1ExuTPDDggf1onjv/XDuLrFjdjKalp+B/Ylu3WzWnbanKpEr46gMwVSf+fDm1OP5YitheL1l3+NrrpsruA/qgHWfHF27dmrkwkRalh0F9p1S7I8o7Q9L/VS47P7jLIdpeedeDqFdu/Sy4otq0StbF37hO6aEwQZU7/0Wu2y62brxAl/DMdIQSe+sgGX1o0JZQa9LZtg2Sk0KK+hELhkejtZ+VDj4rl2QRR8is/ykm+CL6Wn83qadkZFbPnLSurZ3r/M6HjrlSiIzOPlBWWV34wSJ0RMNTJCi6Mf72GP8vjIvdk1w+ehDU/9G2VmhBfrZrTDm8frcWanKrEOvue88WrQ9wnS+317iUQG99hNuVmuXPULnHA7HQ/CXG+ea6HkYrXBdpW03v7xzqC4YXLeCdDKvs1lQSTMrNjpUNr6J5BZ02deUf3tq/7TiInhZCe+AL8UE4f34dwMG/T4zLi7i+adNAZAhEicvRInmEhqXJihBOXn8PTaBhETk/WuytW/EDxPefLN13F5PdSw+MKIJmi9GgXbVAoB1ODB8E/EQ4iLnwA7eBDvUCsDAW9953XKO2k2hZoaBXvqiVn8i+WGr3baT8xAFGUsAdnctH1Tfumk82ViL4hCRiETm6pXf+Nso+wRTWcSfzqxQpXLj0CTLFmWhLLNLkYXxaaF6i/CZ2FIeLL1IHklmF3xS+rejZBR69vtx29Z2RKUGCvRmFQOHiWYqZS1vPlKac60Y2e4cnIqcGpAYm5PyD2hpMFfWrW09Pwlb0QZGEQsASnKtjRtfxGfY/m+2edYfvHPSwzfrjKof1iSoT0dWbUZz8Yyh/bcjfvzY9MsxjV8IuZCVrE66okX5bDzcrXF8lEy6OJrRRc9935fwkfRjEjkqfWRmO2r6WQnkduBXdt+D+Pw/cR0cfD4N7d9m+Z/+bjErFG6Z48ydqYSqVytX7QMT/71kXs7V5T7KatY9VpsGZho4QKaH5tmJY4T8L+1QueExrJGRpgLMC9ZfGHN1u1N1xcV5NDsWUfSQoyLRu85RsrZvW7bt5888KH6Ya69sivO76poGrV3B2jfwQ7yuGWqHl/Uz56T4pbleyIJvOGi01Wi30Xu7V35Wnni6rsSyyjcdS0Axj8bgdNuV+Vjj9soYYvUNpCC+8pVT9yA09cnoSX4aODwBbGNJX5GwYP1xfic+OMWMF9/8NXM0efwvOniqWpIegZdbvsQEnzNm10+NW5cFHLwfQv9aoCW2YQRm0l8hVE7UpHuCEhRMXrmBmzjvpkKIZLxiGhZJ1xiAeyKqPi8VWwQmur3+WKTMLHm7X6ecatI5DsuLVEV5SV0ePxmZlwJoxve7SI+j41/vDORQut1XQme2YnppvecByW36tAqmHKOZtozD9Fi4Qi08nYwuC0hedRucS7nvKzaeX/BGJavcPktzFDrHAmDyY2rYtWaxLKcL5TVRrkBVtXmgV3+B9fE1d9J5JfKe1uTncSK8W7F7Xj2Vyamj8Y99PUOHcSm+ZmKJq9zDCLx+5OBKNoEEC9zACKXJf7L4e6TIQEphMtyZXwHJs35vDkFLcRyY414qDi/XVKVrDXwQh3rhC2c7G/JFXuuNirLbfpmdT743g8rf2zARrBRHU7Thwwk3gMUZxzOA5ivORViaOX4o353xqWCh9q4CS4sNtcRP07bXMTOYezV6LIllo/SuPgCDCOTY9OsxHHa4VdW6IZKM6QxMrHyjs1/vRPj1L8kpg/PPe+G5l2bVXv2A4I/Fvz3oDHXOaqLU4fsplmsdJU4dJY0hOoXvW9X0/Fwb3BPXD0zKcMUJw5ZI2Plya6be6vEpFNgogw7JJbeaVxov+xi0yIgqvWLf+wYROKqzNSvmYGoNC46zy6Iom2Mj4w26nU57UyzcnzL895uopswAbkFzUnZNggekAM4l3VTZu3cP6Jj9fWI2rDoFoxdAxbtZvLF5uHw8lVY9Bt6XFAnUxoWf4bX9HDkwEbgfAf4VqOcLqeNko5IU2paEyXgjX/N7+7hYvy8ABOSr8I/adsC4Jty4sleA3/n6ows9XlWOh/PSDhgcxjfyGPXRu7tX/nvsSwY4HKM5aM0LFqKh/Gh2DQrcXTsD+SJq4Z17RgrR8qADPsf5XLysB1swqq9sZWIeMOmpz5xM+lION3hTAj/EmnEPTSAVDzEGhQOu3ykIsE9LtNfXPzVrlh6pX7RCniul8Wm2YvzpwDioE50aKPYWyq3xZvzPdjyCp+sslXQObFjz46okh+4JEfp0u5An1+M/w0gLHgv3q6vX7wNr2WskCtWxa2flL4j/ZbENTFI4P8gngXHC2w8G2+5KwcHEc52MTzYAxGNgzm9x1IbU0jkWCN547knqFrG05B6gqE8XH2Ja/vvFPn7Qpc9qPE829N3UbbI8/Bcmdr1o9xMKryemGu+SHcSAOImLDNOYpWPmn7Vi9cvyYNlqMcDk22rHk6tstdVwYr/HGdBbPFwQOwISH3h3fD5ToA4qPnQtC7SlA4YSbfWrs6XQjYf8BxpHXnZZ3rTJFcu/pOfeP+qzTY3yy7lOFb6eMNg5bCcwWdX6JrB6BLzUeZWjI2OJ1+J/Kze2558CMZqw6bLrIAoaCUpC55/L06LhqRc+UkSwEjULrJMA6NenVaAiBkqzq7i/OpQQOTUg/e25lkBUeyaOAERQ/s+yeNabtqwYcp0OkZafsFHyC3JOTjs24yJDNe1yyt/RgovpCDVUIiLPdr+IBH2GNlO/L6AVQviIZBRPg9gOhW1j7fEl7CKx97rr8k4poTYg3h2bAesNO4aaZMaEdJZ7zB+BIb0CI9BrwxftWdSNpbe/ZroYs04MIQvWpiwkfBf2zEdRNvbiQky8W+5KlY/lZCY9BaL/yU4uHVK0kyzRLFurFx9nxnJcOY5Mq1oqEn3JxdXkjNJdg16CC9aWJIzoIkF0MShgQhBb8Uy4+EoY5MINo2L0TZnYEj8VhPWw57lDEjGP3AimdBKyWW+zyteJ5ddeQARr2bbf17ixeKKcKvcCe/PDfEZye/UgPwIcgqS5xqnwuHxtqvisZXGFMOf4wxIif7oVDRJygSYBZi4uAewEGmSq1CfHA3ItJnAVfEWM07rIMDL9qtQ/eI3+c4lM4zY6GAz9k9G+WbpLhf/gVn+SOTZNpERobAb0IXZZGbk3smVaz2YBOHVOC0E8DzQwlwnbAaUESCqapeu2WLGHBugPXfjLM4dscc4eOOSWfAg9b/yFFtgsDjnD8CL893ByIY73zGQwfoL/iQxD3bMx1aIgCikEiY6EciwtPpXS36CpcJ/YXkrq53qhzDjVeE8G385tWH7azJ2TjBrG93gzLRCZk1re5RrYdM1uk3or13MioUmDh5YPsbO36pBpVPt0rDOYVWDlxlIgQ2Ym8cCiEIyx0ByRe3hmvFRxYHNHt4UTQcxujFip7JCO8QRWiyJ38c21f2R+9G+OgZSCK5hjOOq7gId1XYIEDXVEYiO5XYx7UrHhYeh4JCAFPKo6kHMDnHCdRSC8BSpodYRBxETpnvZRGteopHqliEDKSb3qtIy4mCKpYWqHEC92DcbwaDvnlTk3DSCVVqqKgVAoh6x8FbbLFWYCiJNC2JH5YBebyr4WeaBE+MwqRcy9kDIcpkRIkwNkBBWdK4SEhpisiOcgkZpePFCGwULIETHWu1GmNSPU9CMlLNIGZC6ZDwEzWyBpgwPmFzF51gwJo9O4E+OplN8sDanFkjUpsFLo4SwZYVrqoIYD4W2qwByNAIey49wZPLS0ajbap0pBzJccd9EBK4yMRkaStC0ACY1cJxA2x2EFCDPG/GR3nms4nH4EsduGCYgww0WXhY1tN/R8oBjAiVmw5oCU+rQVKtq68XY7P0pyju0x3jR1SWdxspX7hy7EIYlG1YgRRVijSkW7Epor34VkyKzIEyyAE/VzbM5rRkfkcc13g3vy89kzVdMTLsAwjyB1EE1CzbkDZjzr2AfE0dDHq0frJ6xkO/shIATyaFVYe9LN3Hs/IfP33h0TmIMZOIUK8yoiKc6sEkPC/fTk+LHty/1KXLgdJyvnQbAqpFWg/mo+ErOJ/CdfirL6htWzvWkWsah8hs5IGMk5RrMppaC4SuGp9VoH6gvgl78Dpsw7Kb1sOmpMd6QNJBjHCCr4qWBtNpTY5wuDeQYB8iqeGkgrfbUGKdLAznGAbIqXhpIqz01xunSQI5xgKyKlwbSak+Ncbo0kGMcIKviHbZAwne6l2u9W6x2xKFOdxgCyT/CZ0Cu8cm8KnPKW7sOdYCsyj8qTnOrwlmlg/Y1YNvkUXyDZ6W39tUxeabGaluc0v0/HccqvEEClkkAAAAASUVORK5CYII=" alt="Have something in mind icon"></div>
                     </div>
                  </div>
               </div>
               <div class="cart-estimation__box">
                  <div class="cart-estimation__amount"><span>Estimate amount</span><span id="idamount">₹ 00</span></div>
                  <span>“Merchant will confirm final amount”</span>
               </div>
            </div>
            <footer style="position: fixed; bottom: 0px; width: 100%; padding: 10px; left: 0px; background: transparent;">
               <div class="cart-footer-wrap">
                  <div><button class="catalog-custom-btn-outline " onclick="CallDataSave()">I Will Pick Up</button></div>
                  <div><button class="catalog-custom-btn-fill ">Deliver This</button></div>
               </div>
            </footer>
            <div class="rodal rodal-fade-leave bottom-modal" tabindex="-1" style="display: none; animation-duration: 300ms;">
               <div class="rodal-mask"></div>
               <div class="rodal-dialog rodal-slideUp-leave" style="width: 100%; min-height: 300px; animation-duration: 300ms;">
                  <div class="whatapp-modal-wrap">
                     <h2>Confirm your <br> WhatsApp number</h2>
                     <span class="closeicon"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAArFJREFUWAntVv1d2zAQPZkByscChgWawgC4E9QbECZou0E3aDtBwwYwQcwAKekCJAtQYID6+p6D/JMUyXaS/lf0hy2dnu496U4fIv97MdsuwOMoL8SI2vEHd8tbW9/kP1gACHPJsgs1WoqYUZxE50bNtdT11cF8uYxjfGuvABDv11n21RgZ+0O7W6oyyer6M4Q8dSE7BYB8pJmZijH7XU6SfapPptb3EDFPYbJUx+NpPta97G5rcjqGcPqgrxRPdAWamZP8Hxbzp34XW4k1AYw5ln2x08xjwlfhOA5zYi0E9Z75liB/FtGbmG/f1mCADQrCgWT+EljFE4DZ50bMRQhC+xlLWBzOFqWKXkX6GxP7iCGWY0IcdtJHcrh2TwAUfnI7nfrUxu9othjHRNDGPo5ZYbViPSzgGLs2TwAOtsLtdOrlw+nJD9sORbjkxDycHU+wBT5YvPs3Rj27l4S/z07ao9UdZOs8XI5+3l/a9opIxM6cdtoSYbTD5HB23/K2FcSmwJ6dtqhEJRThwoaQE48c4eFUse6FgIa+wiPZDYfFDyW3ePvfWEAzEDeOdbDrf2MBYcJZAWFiWnvf35tJbxI6W42OmyTE/Rwm5iZJ6K+A6q+U4nDmNuZhTvSuRMDhCVDBYyJa9KZrq8VEpI7tkMMTgAfEJM4v59imI/bZmYc4V0SDVTkPMWyHHF4OEJAiENxmuKQqQEriOso1sEXsQsMZ8h354h33awKgno+IJQjedJBs08ULLe+9jgl4uc22IUmOoc+QnGAvB+xoAPG6rS9te9c/fdFnzM9aCFwQkwnhqGDbNhzNOyJFTq7oCrCDpVkJxI3Js7IM/3LMS8yjM7eeOlfAgvjHauR8TODNUCLD37p9bR2HDPc5txrEL1t7R2WwgNAHBBWuDYSV236tD12Bv6trRs7+ButJAAAAAElFTkSuQmCC" alt="cancel"></span><span class="small-heading">to place order</span>
                     <div class="form-group"><input type="tel" class="form-control" placeholder="Whatsapp Number" value=""><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAKKADAAQAAAABAAAAKAAAAABZLkSWAAAHxUlEQVRYCbVZa2xcxRX+5u7aTna9L3vXdmwTGUITip03MURA82ghSgIifSBU9QclCJEGo0ooCVXKqy0/aCGiIqikLS1UqiK1FW2RSEGC4pQYUPMqSWw3aSnZpnH82LX37q6fu947PXOTmb27d3ft2GYse87jm7nnnjlzzsw1wwxbJJFYAsPYAIOtY8CXwFBvTsV5kvowGDvPgc9Id7LCqb3j8XgiM3kUjZ9+G+Tcy/XE/fTgNhq4ePojCcn5GUA7hHnOl4MuV890x07LwGQyGZpIGz8gwx5gDPOmO3kJ3H7y6rPk1YESGFM1pYERXX+YGfgJLZm30GQnoyfxWeI/0FNxDKeTSGVS8JR74SnzoHZ+DVprW+Er89uGco5xjWFvdcD3ok1pEZQ0MDoUf4Via4cFb5Jdehc6ejtwInIMKSOdr7bx13u/gNsX3IZ1C9bbdCR4PRjwPVBIIWQFDeScuwdjiT+S9k7rwI6+D/H2hUPoGZ12CFmHk0fr8I3rvo41odYcOTHHyx1ss9frjeYrbAaScY6oHv8rA1snwWOZMRzoPoBTg59I0az6ZVXL0NbyKMq1cjUPB/8g6PdtZIxllJAIm4HRWHw/ydskqH+sH/tOv4CBsSnjWQ6ZVt/gbsCu5bsRKA9Y8S/Rcn/XKsgxMBpL3EP54M8SIIL+mRPPIDpu87yEzKr3lvnw1OqnEJwXVPPQ5vlWqMp3UAo0SYgcBxivSX6Sgv8F8tznZZx4TiIdx75T+yBCKNv4Lyit1UheGYhY/AlaceXvg58eRDgZljjVN3mbcN/ie9FS3axksyF6xy5hf+dLagqKQfd4OvMjKTCXWNf1QNpADynnC8XFkYt44tj3JUb122/8Nu66dovid7S3YWB0bmLz4S9+B2trb5FzG5pTW17l8XSaHpw0sFMaJxCvnv2lBKpeeMxqnFBsaFAbXeFmSvwp/AYM+rnSNGPS2CNo00DO8KDUnNPPFVzarU1Zz0lsvsFSPpNeZInjkeOWofxuSnlMo1J2E+W8a6XmWOSoJHP6m+vW5PCCcZe5saFxvU0+U8FHfR9ZhjJ/NB5fpVGdvcMixdGBY1Z2SnrjHBrYHeuCyB6yMc42a5SqlWv6xvrMrS8B1n5gtPBx7q3wX6ywWdEpI4VziX9Z5uArNHC2WEpiE0OStPVdQ902WfvFv+HvfYVDwgaepqBnOFvnKQTraJNwdRbSJxJFp/ndv/9g07VfPGyTzVYwaHUS4ws02sEpOelwuriBIt8Jj1nb1qbNVnZO6CSdK1XjqNUYx7AUOLUySRbsf939OiXmbCzeXNdKuXFrQexMhS6nyzpUIw8yXUoqKW2UaiPpETx34nmMpEcVbPuN989pqvFXqGpLlZcl6NTNlUvEgXKqFk6EITxpbY8u31nUSFGBHl+9i47+KllYh9po6/GLVve8k4OdoYL8NYFsrGyEgzmQ4TlnRtskcnMIw2QTdGvtTXj59Cvk4RFTLA4WwjiR0EU49FMci812OC+W5Ryib65qVixnvMux53t7vWTgN4WUKgrO6WcRGVdOVeB8QnhyZHIUK0MrlKqxsgGbFt4Bf4Uf6UwaO5Y+hBpXjdKLELoct1vwYe/H6kUkoMFVj60L75IsNKYdcDrh+SCD7O69pW4tunV7zlOjLMRb5w+ZD7F6Unjrbto44rdYE5hCbWVwVY7YyXi7FggwnU6xb0vN6uBquiuU3s0SK3qx3I8d2YNw4r9WcUlaZIJCx7SvNGarLt1RPvH5fJ+apxma7bdyRrfTjS0WN0t5qV4s92NHdmP/qZ/lpKFiY177529sqjsbN8FfrmoGqA7/SoAo/MRXCa7RNbObuCWCT/MUdn+8y7yMC/5qm4iz9XRWzD8BCS+LDNA52JUzpch9z7X+GF668JuNvu/Qhb6WzqhjTiEgwhjU9Sep9v1e8GWsHC1VS9HR1yHYq26iPssaLa8GA2OFl1VM/khLW9Y44smep4VxQmcaKAhwrZL+mKT40zWU+5ZKcZVEvrfyh29r+iqa/ZbUAn46GPC/KHEyBmFwvkkKe0cvIZaKSfZz6+9p2oZt9GtplJax3cJnPUhfrdQWOj10xoqZc9rJyrCz+RGsCq7MmZvK7uMhv++EVWguMR2t19B9pUoqzgzmGjjfMR/NgRb0j/fjf8MXJGxG/Q3+G7B9yYOooS9f1kbB9cOQ3/u8VSboyzFoQC2v2MFnqZos8l6PZbRRWqqXYpFnkRrXful9vBl+k3a4OmMoXSkiRAaJ5by19lY7jOMAfU142q64kmYiQ/EjtMS3CcCEMYGMYcDlNK/IhcaYMvGFq3OoE/+g74M9I5eQzDtLVtL3wXrXAiyll1xevQILKxcWm+tZ+h7zZDEli3DuYXoiW+uKIaeQj2cmqIYP0KVnEvXuelRoFSVHUPXqdQAPVVX5DpUCOpme3FgKQLqjdG95n2nsOOeZ+8jp9xbCz3NU4Br3NYVUuTLOL1CO+2l1wPtz6rMHy1yU4pxURjZdridXZOJjN2PtlLTfQ8BzOMRYUqGBN+gzyXVpjr1Ugr5MxjZZdMVJzhP0jHfpJV+t9vnfKQ60a1g0pr9L7j6vaXiv3OFov5p/FwxwXumMJVdkGHM6NMNlZNjtnBlrxWPoBWIcWgeYcTjk9+ekDrsZxSX/B5PYtklr0yQzAAAAAElFTkSuQmCC" alt="whatapp"></div>
                     <span class="bottom-txt">Yes, I want to receive important information &amp; updates on my WhatsApp</span><button class="custom-btn block" disabled="">Confirm</button>
                  </div>
               </div>
            </div>
         </div>

</div>

               <div  style="display:block" id="footer"  class="footerStoreInfo">
                  <div class="footerStoreInfo__name">Nitish Shop</div>
                  <div class="footerStoreInfo__address"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAKKADAAQAAAABAAAAKAAAAABZLkSWAAAC40lEQVRYCe1XPW7bMBSOahvo6JygOkJgHyDq1tE5QTXYQLe2J0hygjabAduIOnWsc4LkBK5yA2XraG0BbNj9XkAClCI+PlKAs1iAQfL9fvz4nkydnByfIwNvy0AUmj5N03632z2Pomi03+9jjImKVWBdYL3sdDp30+m0CM1BfkEAJ5NJCt9L/GL8XE8GoNehQL0AKtb+GGy5wL3owegak4v5fP4gcjCMxAAJXK/Xu4fvmeHvNd3tdheLxWLp4/ROagxwFDgYHOUB87coD68YIoCq5s6lm7HZAWAfx/3Dpm+Sd5qEpoyOFkX+G7K+KQ+dA2Q8HA6fVqtVLonhZBDgEgSKuWBg5RfVF8aP+F3DtnTYjzi9qYvMRdN8PB5n2PXnJh3JmgpfdXsOvw82v81mc5pl2dqm13Ing3Qk2rg+EnNNXakSp3V7c42X/Jm5ts2dAOFobQ4AXNoCq3cee9Q2X1MuAWjaV+Zglz0ibMCq507GTCIByLGQmMHMOdUhV4MAX5j2trkEYG5zhvwrAWnSo8Z+Nsm1DPpCz7nRCRA7fbAFAEP09/cXnZ5oGwKMF/stdNbOh20pvTx0dWDbiERL6C5tesjpqnUPkGuMa1ozti8qrrnqvk4GZ7NZjoBPdcf6GuDoqOO63LLOLPJXYidA5cHW06uojIA263PtEgHcbrcZcnLdzECqqsD0VVXCr5yXBXLP8/x5MBi8R/CED8drFXspb1XVihgkF7BIx9yKRV/2KK+IQTJULD4jySdaBzyPaLgvvn5iBikwiptYfPRNQvY43m8hfl4AWyS68elccyPeAFWiGzMIN6fGwN3virPhdN4AKZhKKD3qVHIxtYEMAii5kKqEwUerAYu7WDvoER89//BuLJmupq4dafvQMYhBnYy6GjV2p9fGWOJjqzU4itcKIAXACzzFUKlHfEil0usUxeCe1gCNeiwpERj93vQhxYE4iI4urfSJepBkxyRHBg7IwH88ZgSRKcsS4gAAAABJRU5ErkJggg=="><span>Kharadi bypass</span></div>
                  <div class="footerStoreInfo__phone"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAdCAYAAABWk2cPAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAHaADAAQAAAABAAAAHQAAAAD9szRrAAABbklEQVRIDcWWgW2DMBBFcRdougEjZIMyAt2ATpCMwgalG7ABdIKwQdMJSiag7yqIrMg2l/bSnvTls/1z74gBkU3TtEUHFIuOjTyzDAoOMZq33loynRRWFBydcw8Kn8qihWZAnaqiwnSH56TwmVoEOmgqcgqFxqfxCPSoMeIplL5V279B+9XWbmHwnsdUWpqyIbUpGnuNKVCKUbRMQOWNtTGHzuAxAJa1rTVQ7t4l6iXxxnvy0ZvbpvIXotDVdraki2pA9ygU1YXVdgpxCFA/WTM/23PnUjwAlaWbg2N/cxRMUznqkMQ7ekE79DgrP19ZLMHYoFjs/N9hKpE0tBYHDGk4hhRYChSoRtdE4zcczKmWAl8DW7y9/3IIQvlKqdh4DW7+cHEVKnVn8DOpyaeNCjqDG0Z5Vt9k/ptQQwXCFR9RQfqEPtDfB3dGhYblDlGOvUmnwHIkj46mgb0zoXpFAG+YytmL8nlk+I6W46m/AKHs+QnYrSMzAAAAAElFTkSuQmCC">9552202776</div>
               </div>

                <div style="display:none" id="item" class="viewCart-nav animate-Up">
                    <div class="viewCart-nav__body">
                    <div class="viewCart-nav__left">
                        <span>
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAYAAABYQRdDAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFaADAAQAAAABAAAAGgAAAACpg6flAAABb0lEQVRIDbWT4U3EMAyFr+j+0xG6Ad2AjHAjdAQ2gA3oBsAEdAPKBBwTcExwMEF5X1WjKErSVAhLT4nt51fHSXe7jE3T1AjPwlnA3oTbTEk+peKDgNiXMAi9cBQwxOu8QpClQEAQkcZPy78RsN6Pr+5VYIVtjKz8I6qy8m5FHoVTTJCYcgcBcykOpFZgTmZOG5CyETElx4XAqO7tAxfLhvlEj2rElZUxMDIHz0T/IoiOmWOzX7zLZbWFL+fM+FGeiYYCD2Eg8LmDD8WaID67eyVjiWgHgUCsbh4jM40lg/pid27GLqq4SsRP4T1XsLVTxDhilxC9Ir4m6nfE3lEkS13k6vFfq6qiqyfBF3yRTzxtuv1eSFlHpZL1Av9XTtU0/v+bInUbBNFwJbfP/M5C/sgcabESUeOWrjWi16XsQl77H53O79R/i4XNZGknOh2ylG3Jb9FHe4PH1HvaGO9+e1Ahj/tOGDeKQD8Jg+BM8AfpggTG1iPS8gAAAABJRU5ErkJggg==" alt="Shopping bag icon">
                            <span id="span">0</span></span>Items</div>
                    <%--  <a href="../Admin/AddToCard.aspx"> --%>
                        
                             <div onclick="myFunction();" class="viewCart-nav__right"> View Cart
                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAYKADAAQAAAABAAAAYAAAAACK+310AAAB9UlEQVR4Ae3cQUrDUBAG4MatCB7OC7hz1xOI8Xpu3HifOIEUSijFpK+dMf0CoVRJZvz+ebTY8nY7BwECBAgQIECAAAECBAgQIECAAAECBAgQIECgsMAwDO9x9oVb3G5rAb+P83AI4ZZRh/oxvhBujP92ED/xaCVcM4wAfz2BPv+REK4RQii/zKXPPBdC6xAC+znOnzPo818JQQitBQrcz0oQQgGBAi1YCXVC+J6/+p553hdoe1stBPZTnELIjFUImfpTbSEIoYBAgRasBCEUECjQgpUghAICBVqwEoRQQKBAC9NK+IrHvx59gba31ULIP8YphMxYhZCpP9UWghAKCBRoocJK6C51iD9iuPQe/+z6z67r+lY9P7S60R3d5yNmTgDJgTcLwQpYn2STEASwPoAmVwpgPWOTF2MBrAugCf5YWgDLA2iGv7z0xq6It5P+OZeVKfws+agLH36iQGJpkw8/USCxdEz++A1qH0VmZDDh+/o6/AyBxJomH36iQGJpkw8/USCxtMmHnyiQWDomf9zEw/v8jAwmfDuowM8QSKxp8uEnCiSWNvnwEwWSS8f027QvOYPxw3TbVhYIwcatBULYx2qYH312X3dVP/SPQ4CfkX6EYPv6DHg1CRAgQIAAAQIECBAgQIAAAQIECBAgQIAAgUUCv/mA9bof+IauAAAAAElFTkSuQmCC" alt="View Cart arrow icon">

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                        </div> <%--</a>--%></div></div>
               <div class="viewCart-nav ">
                  <div class="viewCart-nav__body">
                     <div class="viewCart-nav__left"><span><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAaCAYAAABYQRdDAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFaADAAQAAAABAAAAGgAAAACpg6flAAABb0lEQVRIDbWT4U3EMAyFr+j+0xG6Ad2AjHAjdAQ2gA3oBsAEdAPKBBwTcExwMEF5X1WjKErSVAhLT4nt51fHSXe7jE3T1AjPwlnA3oTbTEk+peKDgNiXMAi9cBQwxOu8QpClQEAQkcZPy78RsN6Pr+5VYIVtjKz8I6qy8m5FHoVTTJCYcgcBcykOpFZgTmZOG5CyETElx4XAqO7tAxfLhvlEj2rElZUxMDIHz0T/IoiOmWOzX7zLZbWFL+fM+FGeiYYCD2Eg8LmDD8WaID67eyVjiWgHgUCsbh4jM40lg/pid27GLqq4SsRP4T1XsLVTxDhilxC9Ir4m6nfE3lEkS13k6vFfq6qiqyfBF3yRTzxtuv1eSFlHpZL1Av9XTtU0/v+bInUbBNFwJbfP/M5C/sgcabESUeOWrjWi16XsQl77H53O79R/i4XNZGknOh2ylG3Jb9FHe4PH1HvaGO9+e1Ahj/tOGDeKQD8Jg+BM8AfpggTG1iPS8gAAAABJRU5ErkJggg==" alt="Shopping bag icon"><span>0</span></span>Items</div>
                     <div class="viewCart-nav__right" >View Cart<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAYKADAAQAAAABAAAAYAAAAACK+310AAAB9UlEQVR4Ae3cQUrDUBAG4MatCB7OC7hz1xOI8Xpu3HifOIEUSijFpK+dMf0CoVRJZvz+ebTY8nY7BwECBAgQIECAAAECBAgQIECAAAECBAgQIECgsMAwDO9x9oVb3G5rAb+P83AI4ZZRh/oxvhBujP92ED/xaCVcM4wAfz2BPv+REK4RQii/zKXPPBdC6xAC+znOnzPo818JQQitBQrcz0oQQgGBAi1YCXVC+J6/+p553hdoe1stBPZTnELIjFUImfpTbSEIoYBAgRasBCEUECjQgpUghAICBVqwEoRQQKBAC9NK+IrHvx59gba31ULIP8YphMxYhZCpP9UWghAKCBRoocJK6C51iD9iuPQe/+z6z67r+lY9P7S60R3d5yNmTgDJgTcLwQpYn2STEASwPoAmVwpgPWOTF2MBrAugCf5YWgDLA2iGv7z0xq6It5P+OZeVKfws+agLH36iQGJpkw8/USCxdEz++A1qH0VmZDDh+/o6/AyBxJomH36iQGJpkw8/USCxtMmHnyiQWDomf9zEw/v8jAwmfDuowM8QSKxp8uEnCiSWNvnwEwWSS8f027QvOYPxw3TbVhYIwcatBULYx2qYH312X3dVP/SPQ4CfkX6EYPv6DHg1CRAgQIAAAQIECBAgQIAAAQIECBAgQIAAgUUCv/mA9bof+IauAAAAAElFTkSuQmCC" alt="View Cart arrow icon"></div>
                  </div>
               </div>
               <div class="productDescription"></div>
            
         </div>
      </div>
          
      <script>window.storeinfo = {"store_id":39291,"store_info":{"name":"Nitish Shop","domain":"d-1518613","store_url":"https://d-1518613.dotpe.in","store_type":"dukaan","logo_image":""},"services":{"store_flag":1,"delivery_flag":1,"pickup_flag":1},"address":{"address_1":"Kharadi bypass","latitude":18.56112018,"longitude":73.94094156,"google_address":"Lane Number 5"},"owner":{"user_id":75127,"phone":"9552202776"},"configs":{"store_id":39291,"rewards":1,"promos":1,"payouts":1,"bank_update":1}}</script>
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K93GVKM" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <script>!function(e){function t(t){for(var n,o,c=t[0],i=t[1],f=t[2],l=0,s=[];l<c.length;l++)o=c[l],Object.prototype.hasOwnProperty.call(a,o)&&a[o]&&s.push(a[o][0]),a[o]=0;for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(e[n]=i[n]);for(d&&d(t);s.length;)s.shift()();return u.push.apply(u,f||[]),r()}function r(){for(var e,t=0;t<u.length;t++){for(var r=u[t],n=!0,o=1;o<r.length;o++){var i=r[o];0!==a[i]&&(n=!1)}n&&(u.splice(t--,1),e=c(c.s=r[0]))}return e}var n={},o={1:0},a={1:0},u=[];function c(t){if(n[t])return n[t].exports;var r=n[t]={i:t,l:!1,exports:{}};return e[t].call(r.exports,r,r.exports,c),r.l=!0,r.exports}c.e=function(e){var t=[];o[e]?t.push(o[e]):0!==o[e]&&{3:1,4:1,5:1,6:1,7:1,8:1,9:1,10:1,11:1,12:1,13:1,14:1,15:1,16:1,17:1,18:1}[e]&&t.push(o[e]=new Promise((function(t,r){for(var n="static/css/"+({}[e]||e)+"."+{3:"aedd136d",4:"0aaee4f2",5:"4e51261c",6:"d561fb8e",7:"6b2490f2",8:"65d3ec80",9:"d2886c58",10:"10726621",11:"74f0c807",12:"3cf8092d",13:"94952936",14:"e615dac1",15:"079d3a94",16:"ad8ec09e",17:"0d343615",18:"56005307"}[e]+".chunk.css",a=c.p+n,u=document.getElementsByTagName("link"),i=0;i<u.length;i++){var f=(d=u[i]).getAttribute("data-href")||d.getAttribute("href");if("stylesheet"===d.rel&&(f===n||f===a))return t()}var l=document.getElementsByTagName("style");for(i=0;i<l.length;i++){var d;if((f=(d=l[i]).getAttribute("data-href"))===n||f===a)return t()}var s=document.createElement("link");s.rel="stylesheet",s.type="text/css",s.onload=t,s.onerror=function(t){var n=t&&t.target&&t.target.src||a,u=new Error("Loading CSS chunk "+e+" failed.\n("+n+")");u.code="CSS_CHUNK_LOAD_FAILED",u.request=n,delete o[e],s.parentNode.removeChild(s),r(u)},s.href=a,document.getElementsByTagName("head")[0].appendChild(s)})).then((function(){o[e]=0})));var r=a[e];if(0!==r)if(r)t.push(r[2]);else{var n=new Promise((function(t,n){r=a[e]=[t,n]}));t.push(r[2]=n);var u,i=document.createElement("script");i.charset="utf-8",i.timeout=120,c.nc&&i.setAttribute("nonce",c.nc),i.src=function(e){return c.p+"static/js/"+({}[e]||e)+"."+{3:"13a61dde",4:"39010235",5:"66e6a9bf",6:"a926a5e3",7:"1e27781c",8:"d198a642",9:"208efd51",10:"f6384124",11:"93b552f9",12:"7de49878",13:"cf063582",14:"16ccbb94",15:"403f5b26",16:"73a070e2",17:"c3fd4f1b",18:"bcea5d3a"}[e]+".chunk.js"}(e);var f=new Error;u=function(t){i.onerror=i.onload=null,clearTimeout(l);var r=a[e];if(0!==r){if(r){var n=t&&("load"===t.type?"missing":t.type),o=t&&t.target&&t.target.src;f.message="Loading chunk "+e+" failed.\n("+n+": "+o+")",f.name="ChunkLoadError",f.type=n,f.request=o,r[1](f)}a[e]=void 0}};var l=setTimeout((function(){u({type:"timeout",target:i})}),12e4);i.onerror=i.onload=u,document.head.appendChild(i)}return Promise.all(t)},c.m=e,c.c=n,c.d=function(e,t,r){c.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},c.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},c.t=function(e,t){if(1&t&&(e=c(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(c.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var n in e)c.d(r,n,function(t){return e[t]}.bind(null,n));return r},c.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return c.d(t,"a",t),t},c.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},c.p="/",c.oe=function(e){throw console.error(e),e};var i=this.webpackJsonpdigitaldukan=this.webpackJsonpdigitaldukan||[],f=i.push.bind(i);i.push=t,i=i.slice();for(var l=0;l<i.length;l++)t(i[l]);var d=f;r()}([])</script><script src="/static/js/2.511b162f.chunk.js"></script><script src="/static/js/main.35168d19.chunk.js"></script>
   </body>
<link href="../ImagesCSS/css/jquery-ui.min.css" rel="stylesheet" />
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <script src="../ImagesCSS/js/jquery-3.3.1.min.js"></script>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
       <script src="jquery-3.5.1.min.js"></script>--%>

   <%-- <script>
        debugger;
        $(document).ready(function(){
    
        $(document).on('click', '#idadd', function () {
             alert('Atish')
        //$(this).attr('value',$(this).val());
    });
        //$(document).ready(function(){
        //    $("#idadd").click(function () {

        //        alert()
            });
        </script>--%>
    <script>
             debugger;
             $(document).ready(function ()
            {
                debugger;
                 $(".addtocart-btn__changeIcon12").click(function () {
                     debugger;
                     var imgID = $(this).prop('id').replace("Add", "");
                     // you can directly use value imgID variable if its same. or you 
                     //can use from ItemID inner html
                     var yourValue = $("#ItemID" + imgID).html();
                     var ValueName = $("#ItemName" + imgID).html();
                 })
              });
    </script>
       <script>
           var a = 1;
            var b = 1;
           var all = new Array();
           var aryName = new Array();
           var Amount="0";
        


            debugger;
            $(document).ready(function ()
            {
                debugger;
                   $(".addtocart-btn__changeIcon").click(function () {
                    debugger;
                    var imgID = $(this).prop('id').replace("btn", "");
                    // you can directly use value imgID variable if its same. or you 
                    //can use from ItemID inner html
                       var yourValue = $("#ItemID" + imgID).html();
                       var ValueName = $("#ItemName" + imgID).html();
                      // Amount = Amount + yourValue;
                       yourValue = yourValue.replace("₹", "");
                       if (Amount == "0") {
                           Amount = parseInt(yourValue);
                       }
                       else
                       {
                           Amount = Amount + parseInt(yourValue);
                       }
                              
                     
                       all[a] = yourValue;
                       aryName[b] = ValueName;
                     //  a = a + 1;
                       document.getElementById('span').innerHTML = a;
                        var x = document.getElementById("item");
                       if (x.style.display === "none")
                       {
                        x.style.display = "block";
                       }
                       a++;
                       b++;

                       document.getElementById('idamount').innerHTML = Amount;

//                       if (a = 3)
//                       {
//                           alert(all[1]);
//alert(all[2]);
//alert(all[3]);
//                       }

                })
            });
             </script>
       <script>
           
          
         
    var i = 0;
    function buttonClick2(obj) {
        //i++;
        debugger;

          debugger;
        var obj1 = obj.replace("btn", "");
        var yourValue1 = $("#ItemID" + obj1).html();
          yourValue1 = yourValue1.replace("₹", "");
        obj1 = 'Value' + obj1;
      
          var a = parseInt(document.getElementById(obj1).innerHTML);
        if (0 < a)
        {
            a = a + 1;
            Amount = Amount + parseInt(yourValue1);
             document.getElementById('idamount').innerHTML = Amount;
                       document.getElementById(obj1).innerHTML = a;
        }
        else
        {
                       document.getElementById(obj1).innerHTML = '0';
        }

        //document.getElementById('id2').value = i;
        //// var imgID = $(this).prop('id');//;;.replace("Add", "");
        ////var a = parseInt(document.getElementById('Value').innerHTML);
        ////a = a + 1;
        ////document.getElementById('Value').innerHTML = a;
       // var b= parseInt(document.getElementById('idAmt2').value);

       // var C = document.getElementById('idCard2')
       // C.value = a * b;

        
       //  var D = parseInt(document.getElementById('subtotal').innerHTML);
       //document.getElementById('subtotal').innerHTML = D + C.value;
       // var E = parseInt(document.getElementById('total').innerHTML);
       //document.getElementById('total').innerHTML= E + C.value;
         
       
             }
               function buttonClicks2(obj) {
        //i++;
                   debugger;
                   var obj1 = obj.replace("div", "");
                    var yourValue1 = $("#ItemID" + obj1).html();
          yourValue1 = yourValue1.replace("₹", "");
                    obj1 = 'Value' + obj1;
                var a = parseInt(document.getElementById(obj1).innerHTML);
                   if (0 < a)
                   {
                       a = a - 1;
                       Amount = Amount - parseInt(yourValue1);
             document.getElementById('idamount').innerHTML = Amount;
                       document.getElementById(obj1).innerHTML = a;
                   }
                   else
                   {
                       document.getElementById(obj1).innerHTML = '0';
                   }
       // var a = parseInt(document.getElementById('Value').innerHTML);
       //           var b = a - 1;
       //             document.getElementById('Value').innerHTML = b;
       // var b= parseInt(document.getElementById('idAmt2').value);

       // var C = document.getElementById('idCard2')
       // C.value = a * b;

        
       //  var D = parseInt(document.getElementById('subtotal').innerHTML);
       //document.getElementById('subtotal').innerHTML = D + C.value;
       // var E = parseInt(document.getElementById('total').innerHTML);
       //document.getElementById('total').innerHTML= E + C.value;
         
       
    }
</script>
        <script>

            function  myFunction() {
                debugger;
                var x = document.getElementById("FirstPage");
                var z = document.getElementById("item");
                var v = document.getElementById("footer");
                
        if (x.style.display === "block") {
            x.style.display = "none";

             if (z.style.display === "block") {
                 z.style.display = "none";

                     } 
                 if (v.style.display === "block") {
                 v.style.display = "none";

        }
            var Y = document.getElementById("SecondPage");

             if (Y.style.display === "none") {
                 Y.style.display = "block";
                  

                 	var i = 1,
				arrLen = all.length - 1
				str = "";
			// loop through all elements in the array, building a form for each object
			for (; i <= arrLen; i++ ) {
                str = str + '<div class="catalog-item-container">'
                    + '<div class="cart-item">'
                    + ' <div class="cart-item__left"><span class="cart-item__left-name">'+aryName[i]+'</span><label class="cart-item__left-amount">'+all[i]+'</label></div>'
                    + '  <div class="cart-item__right">'
                     + '<div class="shoping__cart__item__close">'
                            + '      <span class="icon_close"></span>'
                             + '         </div>'
                    + '  <div class="addtocart-btn" style="background-color: rgb(255, 255, 255);">'
                    + '   <div class="addtocart-btn__changeIcon" id="div'+i+'"  onclick="buttonClicks2(\'div'+i+'\')" ><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAFCAYAAABFA8wzAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAABQAAAADMlWz1AAAAGUlEQVQYGWN8FRj4n4GKgImKZoGNGoEGAgAJJgKVJxPQJQAAAABJRU5ErkJggg==" alt="Decreament button"></div>'
                    + '   <div class="addtocart-btn__text" id="Value'+ i +'">1</div>'
                    + '   <div class="addtocart-btn__changeIcon12"  id="btn'+i+'"  onclick="buttonClick2(\'btn'+i+'\')"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC" alt="Increment button"></div>'
                    + ' </div>'
                      
                    + ' </div>'
                    + ' </div>'
                    + ' </div>';

                
			};
			
			$("#PlaceHolde2").html(str);
		
         }
        }
            }
            </script>
  

    <script type="text/javascript">    
        function CallDataSave() {
            debugger;
            var i = 1,
                arrLen = all.length - 1

            // loop through all elements in the array, building a form for each object
            for (; i <= arrLen; i++)
            {
        //        var obj1 = obj.replace("Value", "");
        //var yourValue1 = $("#ItemID" + obj1).html();
        //  yourValue1 = yourValue1.replace("₹", "");
        obj1 = 'Value' + i;
                var ItemName = aryName[i];

                var Amount = all[i];
                 Amount = Amount.replace("₹", "");
          var Quntity = parseInt(document.getElementById(obj1).innerHTML);
            //    var UserName = document.getElementById("TxtName").value;
            //    var UserEmail = document.getElementById("TxtEmail").value;
            //    var UserMessage = document.getElementById("TxtMessage").value;
            //    tempuri.org.IDataService.InsertData(UserName, UserEmail, UserMessage, ShowMessage, null, null);

            }
        }
     
        function ShowMessage() {    
            LblMessage.innerHTML = "Data Inserted Successfully";                
        }    
    </script>
</html>
