﻿using System;
using DAL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NeedlyApp.Static
{
    public partial class Cart : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
                                                                
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public void Getdata()
        {
            try
            {
                //    SqlCommand cmd = new SqlCommand("sp_ShowData");
                SqlCommand cmd = new SqlCommand("sp_ShowDataimages");

                cmd.Connection = con;
                //  con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Parameters.AddWithValue("@Id", ViewState["Id"].ToString());
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                    {
                        string Image = Convert.ToString(ds.Tables[0].Rows[r]["Image"]);
                        string title = Convert.ToString(ds.Tables[0].Rows[r]["title"]);
                        string helpkeyID = Convert.ToString(ds.Tables[0].Rows[r]["helpkeyID"]);

                        string Html = string.Empty;


                        Html = "   <div class='catalog - item - container'>" +
                          " <div class='cart-item'>" +
                          "<div class='cart-item__left'><span class='cart-item__left-name'>लॅपटॉप</span><label class='cart-item__left-amount'>₹ 1500</label></div>" +
                          "<div class='cart-item__right'>" +
                           "  <div class='addtocart-btn' style='background-color: rgb(255, 255, 255);'>" +
                             "   <div class='addtocart-btn__changeIcon'><img src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAFCAYAAABFA8wzAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAABQAAAADMlWz1AAAAGUlEQVQYGWN8FRj4n4GKgImKZoGNGoEGAgAJJgKVJxPQJQAAAABJRU5ErkJggg==' alt='Decreament button'></div>" +
                            "    <div class='addtocart-btn__text'>1</div>" +
                              "  <div class='addtocart-btn__changeIcon'><img src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAFKADAAQAAAABAAAAFAAAAACRFdLHAAAASElEQVQ4EWNkIABeBQb+R1Yitn49IzIfnc2ELkApf9RASkOQgWHwhyEjejqj1NOD38tUdyHefAkKT/QwHs3LGKmM6pEyAg0EAE/NCrEB0dGsAAAAAElFTkSuQmCC' alt='Increment button'></div>" +
                             "</div>" +
                          "</div>" +
                       "</div>" +
                    "</div>";
                    }





                }
            }
            catch (Exception ex)
            { }
        }
    }
}