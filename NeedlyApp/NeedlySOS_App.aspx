﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NeedlySOS_App.aspx.cs" Inherits="NeedlyApp.DownloadApp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   	<title>NEEDLY APP</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="log.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/bootstrap/css/bootstrap.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/animate/animate.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/css-hamburgers/hamburgers.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/select2/select2.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/css/util.css"/>
	<link rel="stylesheet" type="text/css" href="Logincss/css/main.css"/>
<!--===============================================================================================-->
</head>
	<body>
		 <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="Logincss/images/Needly_Logo-Playstore.png" alt="IMG"/>
				</div>
    <form id="form1" runat="server" class="login100-form validate-form">
        <span class="login100-form-title">
						Needly Application
					</span>
		<div class="container-login100-form-btn">
						
							  <asp:Button ID="btnDownloadNeedlyApp" OnClick="btnDownloadNeedlyApp_Click"  runat="server" Text="Click here to download Application" />
       
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							
						</span>
						<a class="txt2">
						
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
								</a>
					</div>
    </form>
				</div>
		</div>
	</div>
		<!--===============================================================================================-->	
	<script src="Logincss/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="Logincss/vendor/bootstrap/js/popper.js"></script>
	<script src="Logincss/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="Logincss/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="Logincss/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>
<!--===============================================================================================-->
	<script src="Logincss/js/main.js"></script>
</body>
</html>
