﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="privacy-policy.aspx.cs" Inherits="NeedlyApp.privacy_policy" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../Logincss/images/Needly_Logo-Playstore.png" type="image/ico" />

    <title>NEEDLY </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet"/>
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"/>
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"/>
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet"/>

                    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>


</head>
<body>
    <form id="form1" runat="server">
        <div>

       
           
           <div class="container" style=" height:600px ; width: 1400px; margin-top:50px; padding-left: 30px; padding-right: 30px">
                <center>
                <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-10" style="">
            <div class="x_panel">
              
                <div class="row">
                    <div class="panel-body">
                        <%--<div class="col-md-12">--%>
                            <div class="box-body col-12">
                                <div class="form-group">
                                    <h1>Privacy Policy</h1>
                                    <p style="text-align:justify">
                                       This privacy policy (“Policy”) was last changed on August 11th, 2020. We may occasionally make changes to the Policy.<br />
                                      We sincerely believe that you should always know what data we collect from you, the purposes for which such data is used, and that you should have the ability to make informed decisions about what you want to share with us.
                                    </p>
                                </div>
                            </div>
                       
                        </div>
                    </div>
              <div class="row">
                    <div class="panel-body">
                        <div class="col-12">
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-12">
                             <div class="box-body col-6">
                               
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <center>
                      <div class="col-md-12">
                   <%-- <div class="box-body col-sm-5">
                        <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>
                    </div>--%>
                   <%-- <div class="box-body col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" />

                        <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                    </div>--%>
                           
                                          
                  <%--  <div class="box-body col-sm-6">
                        <asp:Label ID="Lblerrer" runat="server"></asp:Label>
                    </div>--%>
                </div>
                </center>
              

                <div class="col-md-12">
                    <div class="box-body col-sm-offset-2  col-sm-4">
                        <asp:Label ID="Label1" runat="server" Text="Count" Visible="false" Font-Bold="true"></asp:Label>
                        <asp:Label ID="lalCount" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:Label ID="lblMessage" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                </div>
                <br />
                <br />

                 <footer>
          <div>
           © 2018 - Abhinav IT Solutions Pvt. Ltd, Pune <a href="https://"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
  </div>
            </div>
        </div>
                     </center>
               </div>
          
            <!-- /content -->
       
        <!-- /footer content -->
      </div>
  
    </form>
     <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../Logincss/vendor/bootstrap/js/bootstrap-material-datetimepicker.js"></script>
    <link href="../Logincss/vendor/bootstrap/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link href="../Logincss/vendor/bootstrap/css/style.css" rel="stylesheet" />
<%--    <script src="../Logincss/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="../Logincss/vendor/bootstrap/js/moment-2.10.3.js"></script>--%>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script></body>

</body>
</html>
