﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KeywordDetails.aspx.cs" Inherits="NeedlyApp.KeywordDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../Logincss/images/Needly_Logo-Playstore.png" type="image/ico" />

    <title>NEEDLY </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet"/>
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"/>
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"/>
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet"/>

</head>
<body>
    <form id="form1" runat="server">
        
               <div class="container" style=" height:1200px ; width: 700px; margin-top:50px; padding-left: 30px">
                
                <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-10" style="">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i> Keyword Contact Excel Uplaod <small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="panel-body">
                        <%--<div class="col-md-12">--%>
                            <div class="box-body col-12">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" for="input-name2">
                                        App User Number:
                                    </label>
                                    <div class="col-sm-7">
                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control" MaxLength="10" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        <br />
                        <br />
                        <br />
                            
                        </div>
                    </div>

                 <div class="row">
                    <div class="panel-body">
                        <%--<div class="col-md-12">--%>
                            <div class="box-body col-12">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" for="input-name2">
                                        Select keyword:
                                    </label>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="ddlKeyword" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        <br />
                        <br />
                        <br />
                            
                        </div>
                    </div>
              <div class="row">
                    <div class="panel-body">
                        <div class="col-12">
                             <div class="box-body">
                                <asp:Button ID="btnDownloadExcel" OnClick="btnDownloadExcel_Click" runat="server" CssClass="btn" Font-Bold="true" Font-Size="Medium" ForeColor="Red" Text="Click Here to Download Excel Format." ></asp:Button>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-12">
                             <div class="box-body col-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Upload Excel:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:FileUpload ID="Fileupload1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <asp:Label ID="Lblerrer" runat="server" Text=""></asp:Label>
                      <div class="col-md-12">
                   <%-- <div class="box-body col-sm-5">
                        <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>
                    </div>--%>
                   <%-- <div class="box-body col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" />

                        <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                    </div>--%>
                           
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" class="btn btn-success"/>

                        <asp:Button ID="btnreset" OnClick="btnreset_Click" runat="server" Text="Reset" class="btn btn-info" />
                   
                  <%--  <div class="box-body col-sm-6">
                        <asp:Label ID="Lblerrer" runat="server"></asp:Label>
                    </div>--%>
                </div>
               
              

                <div class="col-md-12">
                    <div class="box-body col-sm-offset-2  col-sm-4">
                        <asp:Label ID="Label1" runat="server" Text="Count" Visible="false" Font-Bold="true"></asp:Label>
                        <asp:Label ID="lalCount" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:Label ID="lblMessage" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                </div>
                <br />
                <br />

                 <footer>
          <div>
           © 2018 - Abhinav IT Solutions Pvt. Ltd, Pune <a href="https://"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
  </div>
            </div>
        </div>
                    
               </div>
       
    </form>
</body>
</html>
