﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="WhatsAppMessage.aspx.cs" Inherits="WebApplication3.WhatsAppMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 100%">
            <table style="width: 100%">
                <tr>
                    <td style="width: 30%; float: left">
                        <asp:Label Font-Size="x-large" runat="server" Text="" ID="lblshowinstance"></asp:Label>
                    </td>
                    <td style="width: 50%; float: left">
                        <asp:Label runat="server" Font-Size="x-large" Text="" ID="lblloggeninMNo"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 2px" colspan="2"></td>
                </tr>
                <tr>
                    <td style="width: 100%" colspan="2">
                        <asp:Label runat="server" Text="" ID="lblmessage"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%" colspan="2">
                        <asp:Button ID="btnSendSms" runat="server" Text="Send Message" OnClick="btnSendSms_Click" />
                        <asp:Button ID="btnAddinstance" runat="server" Text="Add Instance" OnClick="btnAddinstance_Click" />

                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 5px" colspan="2"></td>
                </tr>
                <tr>
                    <td style="width: 20%; float: left">
                        <asp:Label Font-Size="x-large" runat="server" ForeColor="Blue" Text="" ID="lblbtotalsms"></asp:Label>
                    </td>
                    <td style="width: 70%; float: left">
                        <asp:Label runat="server" Font-Size="x-large" ForeColor="Green" Text=" " ID="lbltotsentsms"></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                        <asp:Label runat="server" ForeColor="Red" Font-Size="x-large" Text="" ID="lblpendingmsg"></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; height: 5px" colspan="2"></td>
                </tr>
                <tr>
                    <td style="width: 100%" colspan="2">
                        <asp:Label runat="server" Text="Send Message List" ID="lbltext"></asp:Label>
                    </td>
                </tr>
               <tr>
                  
                    <td style="width: 100%" colspan="2">
                        <asp:GridView ID="gridmsglist" runat="server" AutoGenerateColumns="False" Width="100%"
                            CellSpacing="2" Style="table-layout: fixed; border-width: 1px; border-style: solid; font-size: 9pt; font-family: arial,helvetica,sans-serif;"
                            
                            GridLines="Both" ShowFooter="false">
                            <Columns>
                                <%--  From No--%>
                                <asp:TemplateField HeaderStyle-Width="25%" HeaderText="From Number" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblbfrmno" runat="server" CssClass=" textalignleft form-control"
                                            Text='<%#Eval("mobile_number")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left" CssClass="textalignleft" Width="20%" Height="24px" />
                                    <HeaderStyle BackColor="#ffcc66" Width="20%" />
                                </asp:TemplateField>

                                <%--   To No--%>
                                <asp:TemplateField HeaderText="To Phone No" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lbltono" runat="server" CssClass=" textalignleft form-control"
                                            Text='<%#Eval("To_number")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left" CssClass="textalignleft" Width="20%" Height="24px" />
                                    <HeaderStyle BackColor="#ffcc66" Width="20%" />
                                </asp:TemplateField>
                                <%--   Message--%>
                                <asp:TemplateField HeaderText="Message" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblmesage" runat="server" CssClass=" textalignleft form-control"
                                            Text='<%#Eval("Message")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="textalignleft" Width="60%" Height="24px" />
                                    <HeaderStyle BackColor="#ffcc66" Width="60%" />
                                </asp:TemplateField>                             

                            </Columns>
                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" CssClass="header" />
                            <SelectedRowStyle BackColor="#f5f5f5" Font-Bold="True" />
                        </asp:GridView>
                    </td>
               </tr>


            </table>
        </div>
    </form>
</body>
</html>
