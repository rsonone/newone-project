﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.IO;

namespace NeedlyApp
{
    public partial class ContactDetails : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;

        public CommandType CommandType { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/UploadExcel/Contacts.xls", false);
           // Response.Redirect("~/UploadExcel/Contacts.xls", false);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = ReadCsvFile();
                InsertData(dt);
                //InsertCSVRecords(dt);
               // GridView1.DataSource = dt;
               //GridView1.DataBind();
            }
            catch (Exception ex)
            {
                lblerror.Text = ex.Message;
            }
            //try
            //{
            //    if ((Fileupload1.HasFile))
            //    {
            //        try
            //        {
            //            if (!Convert.IsDBNull(Fileupload1.PostedFile) &
            //                Fileupload1.PostedFile.ContentLength > 0)
            //            {
            //                //  string dirPath = System.Web.HttpContext.Current.Server.MapPath("~") + "/Admin/UploadExcel/" + Fileupload1.FileName;

            //              // string dirPath = System.Web.HttpContext.Current.Server.MapPath("~/Admin/UploadExcel") + Fileupload1.FileName;
            //                //  string dirPath = Server.MapPath("C:\\Inetpub\\vhosts\\needly.in\\httpdocs\\Admin\\") + Fileupload1.FileName;
            //                 string dirPath = Fileupload1.FileName; //local path working
            //          //       string dirPath = Server.MapPath("./Admin") + Fileupload1.FileName;

            //                Fileupload1.SaveAs(dirPath);

            //                SqlBulkCopy oSqlBulk = null;


            //                OleDbConnection myExcelConn = new OleDbConnection
            //                    ("Provider=Microsoft.ACE.OLEDB.12.0; " +
            //                        "Data Source=" + dirPath +
            //                        ";Extended Properties=Excel 12.0;");
            //                myExcelConn.Open();

            //                OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

            //                OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

            //                ds = new DataSet();

            //                objAdapter1.Fill(ds);

            //                dt = ds.Tables[0];

            //                myExcelConn.Close();

            //                InsertData(dt);

            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            EL.SendErrorToText(ex);
            //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    EL.SendErrorToText(ex);
            //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            //}


        }

        public void InsertData(DataTable dt)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    DataBaseConnection.Open();
                    //SqlCommand cmd = new SqlCommand();
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string MobileNo = dt.Rows[i]["MobileNo"].ToString();
                        // MobileNo = MobileNo.Trim();

                        string CustomerName = dt.Rows[i]["CustomerName"].ToString();
                        //CustomerName = CustomerName.Trim();

                        string Address = dt.Rows[i].ToString();

                      
                        SqlCommand cmd = new SqlCommand("SP_UploadCustomerContactDetails", DataBaseConnection);
                        CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@CustomerName", CustomerName);

                        //cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@CreatedBy", txtMobileNumber.Text);
                        //cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                        cmd.Parameters.AddWithValue("@ModifiedBy", txtMobileNumber.Text);
                        // cmd.Parameters.AddWithValue("@Level", Level);
                        // cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        DataBaseConnection.Close();

                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }

                    }
                }

                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }
        private void InsertCSVRecords(DataTable dt)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    //connection();
                    //creating object of SqlBulkCopy    
                    SqlBulkCopy objbulk = new SqlBulkCopy(DataBaseConnection);
                    //assigning Destination table name    
                    objbulk.DestinationTableName = "tbl_CustomerContactDetails";
                    //Mapping Table column    
                    objbulk.ColumnMappings.Add("MobileNo", "MobileNo");
//objbulk.ColumnMappings.Add("CustomerName", "CustomerName");
                    //objbulk.ColumnMappings.Add("Address", "Address");
                    //objbulk.ColumnMappings.Add("Designation", "Designation");
                    //inserting Datatable Records to DataBase    
                    DataBaseConnection.Open();
                    objbulk.WriteToServer(dt);
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        private void GetExcelData()
        {
            //Coneection String by default empty  
            string ConStr = "";
            //Extantion of the file upload control saving into ext because   
            //there are two types of extation .xls and .xlsx of Excel   
            string ext = Path.GetExtension(Fileupload1.FileName).ToLower();
            //getting the path of the file   
            //string path = Server.MapPath("~/UploadExcel/" + Fileupload1.FileName);
            string path = Server.MapPath("~/Admin/UploadExcel/" + Fileupload1.FileName);
            //saving the file inside the MyFolder of the server  
            Fileupload1.SaveAs(path);
            // Label1.Text = Fileupload1.FileName + "\'s Data showing into the GridView";
            //checking that extantion is .xls or .xlsx  
            if (ext.Trim() == ".xls")
            {
                //connection string for that file which extantion is .xls  
                ConStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            else if (ext.Trim() == ".xlsx")
            {
                //connection string for that file which extantion is .xlsx  
                ConStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }
            else if (ext.Trim() == ".csv")
            {
                //connection string for that file which extantion is .xlsx  
                ConStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }
            //making query  
            string query = "SELECT * FROM [Sheet1$]";
            //Providing connection  
            OleDbConnection conn = new OleDbConnection(ConStr);
            //checking that connection state is closed or not if closed the   
            //open the connection  
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            //create command object  
            OleDbCommand cmd = new OleDbCommand(query, conn);
            // create a data adapter and get the data into dataadapter  
            OleDbDataAdapter da = new OleDbDataAdapter(cmd);
            DataSet ds = new DataSet();
            //fill the Excel data to data set  
            da.Fill(ds);

            // objAdapter1 = new OleDbDataAdapter(cmd);

            // = new DataSet();

            //.Fill(ds);

            dt = ds.Tables[0];

            //InsertData(dt);
            //set data source of the grid view  
            //  gvExcelFile.DataSource = ds.Tables[0];
            //binding the gridview  
            // gvExcelFile.DataBind();
            //close the connection  
            conn.Close();
        }
        public DataTable ReadCsvFile()
        {

            DataTable dtCsv = new DataTable();
            string Fulltext;
            if (Fileupload1.HasFile)
            {
                string FileSaveWithPath = Server.MapPath("." + System.DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".csv");
                Fileupload1.SaveAs(FileSaveWithPath);
                using (StreamReader sr = new StreamReader(FileSaveWithPath))
                {
                    while (!sr.EndOfStream)
                    {
                        Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                        string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                        for (int i = 0; i < rows.Count() - 1; i++)
                        {
                            string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                            {
                                if (i == 0)
                                {
                                    for (int j = 0; j < rowValues.Count(); j++)
                                    {
                                        dtCsv.Columns.Add(rowValues[j]); //add headers  
                                    }
                                }
                                else
                                {
                                    DataRow dr = dtCsv.NewRow();
                                    for (int k = 0; k < rowValues.Count(); k++)
                                    {
                                        dr[k] = rowValues[k].ToString();
                                    }
                                    dtCsv.Rows.Add(dr); //add other rows  
                                }
                            }
                        }
                    }
                }
            }
            return dtCsv;
        }
    }
}