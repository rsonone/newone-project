﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerContactDetails.aspx.cs" Inherits="NeedlyApp.CustomerContactDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../Logincss/images/Needly_Logo-Playstore.png" type="image/ico" />

    <title>NEEDLY </title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet"/>
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet"/>
	
    <!-- bootstrap-progressbar -->
    <link href="../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"/>
    <!-- JQVMap -->
    <link href="../vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet"/>

                    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>


</head>
<body>
    <form id="form1" runat="server">
        <div>
 <%-- <div class="container body">
      <div class="main_container">--%>
       <%-- <div class="col-md-3 left_col">--%>
         <%-- <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="../Admin/Home.aspx" class="site_title"> 
                 
                <img src="../Logincss/images/Needly_Logo-Playstore.png"  height="30px" width="30px"/>
              <span>Needly</span></a>
            </div>
              
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
         
              </div>--%>
        <%-- <div class="profile_info">
                <span>Welcome </span>
                <h2>
              <asp:Label ID="lblUsername"  runat="server"></asp:Label><br />
                    <asp:Label ID="lblMobilNo"  runat="server"></asp:Label>
              </h2>
             
                </div> 
            </div>--%>
            <!-- /menu profile quick info -->


              
            <!-- sidebar menu -->
          <%--    <asp:Panel ID="ShowAll" runat="server">
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul class="nav side-menu">
                 
                  
                   <li><a><i class="fa fa-user"></i> Upload Contacts <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                     
                       <li id="liCustContact" runat="server"><a href="CustomerContactDetails.aspx">Add Contact Excel </a></li>
                        </ul>
                  </li>
                  

                </ul>
              </div>

            </div>
                  </asp:Panel>--%>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          <%--  <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="../Login.aspx">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>--%>
            <!-- /menu footer buttons -->
        <%--  </div>--%>
       

        <!-- top navigation -->
      <%--  <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt=""/>
                       <asp:Label ID="lblUsername2" runat="server"></asp:Label>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                                  <li><a href="../GroupCode.aspx"><i class="fa fa-sign-out pull-right"></i> Change Group Code</a></li>
                    <li><a href="../Login.aspx"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

           
              </ul>
            </nav>
          </div>
        </div>--%>
        <!-- /top navigation -->
         <!-- content -->
           
           <div class="container" style=" height:600px ; width: 500px; margin-top:50px; padding-left: 30px">
                <center>
                <div class="row">
        <div class="col-md-10 col-sm-12 col-xs-10" style="">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Customer Contact Excel Uplaod <small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="panel-body">
                        <%--<div class="col-md-12">--%>
                            <div class="box-body col-12">
                                <div class="form-group">
                                    <label class="col-sm-6 control-label" for="input-name2">
                                       Enter App User Mobile Number:
                                    </label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        <br />
                        <br />
                        <br />
                            
                        </div>
                    </div>
              <div class="row">
                    <div class="panel-body">
                        <div class="col-12">
                             <div class="box-body">
                                <asp:Button ID="btnDownloadExcel" runat="server" CssClass="btn" Font-Bold="true" Font-Size="Medium" ForeColor="Red" Text="Click Here!!! Download Excel Format." OnClick="btnDownloadExcel_Click"></asp:Button>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-12">
                             <div class="box-body col-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Upload Excel:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:FileUpload ID="Fileupload1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <center>
                      <div class="col-md-12">
                   <%-- <div class="box-body col-sm-5">
                        <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>
                    </div>--%>
                   <%-- <div class="box-body col-sm-6">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" />

                        <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                    </div>--%>
                           
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />

                        <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                   
                  <%--  <div class="box-body col-sm-6">
                        <asp:Label ID="Lblerrer" runat="server"></asp:Label>
                    </div>--%>
                </div>
                </center>
              

                <div class="col-md-12">
                    <div class="box-body col-sm-offset-2  col-sm-4">
                        <asp:Label ID="Label1" runat="server" Text="Count" Visible="false" Font-Bold="true"></asp:Label>
                        <asp:Label ID="lalCount" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:Label ID="lblMessage" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                </div>
                <br />
                <br />

                 <footer>
          <div>
           © 2018 - Abhinav IT Solutions Pvt. Ltd, Pune <a href="https://"></a>
          </div>
          <div class="clearfix"></div>
        </footer>
  </div>
            </div>
        </div>
                     </center>
               </div>
          
            <!-- /content -->
       
        <!-- /footer content -->
      </div>
  
    </form>

 <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="../vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="../vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="../vendors/Flot/jquery.flot.js"></script>
    <script src="../vendors/Flot/jquery.flot.pie.js"></script>
    <script src="../vendors/Flot/jquery.flot.time.js"></script>
    <script src="../vendors/Flot/jquery.flot.stack.js"></script>
    <script src="../vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="../vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="../vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="../vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="../vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="../Logincss/vendor/bootstrap/js/bootstrap-material-datetimepicker.js"></script>
    <link href="../Logincss/vendor/bootstrap/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link href="../Logincss/vendor/bootstrap/css/style.css" rel="stylesheet" />
<%--    <script src="../Logincss/vendor/bootstrap/js/bootstrap-datetimepicker.js"></script>
    <script src="../Logincss/vendor/bootstrap/js/moment-2.10.3.js"></script>--%>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.min.js"></script></body>
</html>
