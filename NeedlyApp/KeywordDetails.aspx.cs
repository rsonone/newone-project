﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DAL;
using System.Web.Configuration;
using System.Data.OleDb;

namespace NeedlyApp
{
    public partial class KeywordDetails : System.Web.UI.Page
    {
        DataSet ObjDataSet = new DataSet();

        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
       CommonCode cc = new CommonCode();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
      //  Errorlogfil EL = new Errorlogfil();
        string mob = string.Empty;
        DataTable dt;
        DataTable dt1;
        protected void Page_Load(object sender, EventArgs e)
        {
          txtMobileNumber.Text=  Convert.ToString(Session["username"]);
            if (Request.QueryString["ID"] != null)
            {
                txtMobileNumber.ReadOnly = true;
                ViewState["Id"] = Request.QueryString["ID"];
                txtMobileNumber.Text = ViewState["Id"].ToString();
                
            }

            KeywordBind();

        }
       
            public void Exceluplode()
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                        Fileupload1.PostedFile.ContentLength > 0)
                    {
                        Fileupload1.SaveAs(Server.MapPath(".") + "\\" + Fileupload1.FileName);
                        // SqlBulkCopy oSqlBulk = null;

                        OleDbConnection myExcelConn = new OleDbConnection


                            ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                "Data Source=" + Server.MapPath(".") + "\\" + Fileupload1.FileName +
                                ";Extended Properties=Excel 12.0;");
                        try
                        {
                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);
                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);
                            ds = new DataSet();
                            objAdapter1.Fill(ds);
                            dt = ds.Tables[0];
                            dt1 = ds.Tables[0];
                            InsertData();
                        }
                        catch (Exception ex)
                        {
                          //  EL.SendErrorToText(ex);
                        }
                    }
                }
                else
                {
                    Lblerrer.Text = "Please  Upload Excel File.";
                    // Lblerrer.BackColor = Color.Blue;

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void InsertData()
        {
            try
            {
                string Field11 = string.Empty;
                string Field12 = string.Empty;
                string Field13 = string.Empty;
                string Field14 = string.Empty;
                string Field15 = string.Empty;
                string Field16 = string.Empty;
                string Field17 = string.Empty;
                string Field18 = string.Empty;
                string Field19 = string.Empty;
                string Field110 = string.Empty;
                string Field111 = string.Empty;
                string Field112 = string.Empty;
                string Field113 = string.Empty;
                string Field114 = string.Empty;
                string Field115 = string.Empty;

                for (int i = 0; i >= 0; i--)
                {
                    string MobileNo = dt1.Columns[i].ToString();
                    MobileNo = MobileNo.Trim();
                    

                    string Prefix = dt1.Columns[i + 1].ToString();
                    Prefix = Prefix.Trim();

                    string CustName = dt1.Columns[i + 2].ToString();
                    CustName = CustName.Trim();

                    string Address = dt1.Columns[i + 3].ToString();
                    Address = Address.Trim();

                    string Category = dt1.Columns[i + 4].ToString();
                    Category = Category.Trim();

                    Field11 = dt1.Columns[i + 5].ToString();
                    Field11 = Field11.Trim();
                   // string ChnageField1 = "Field1";

                    Field12 = dt1.Columns[i + 6].ToString();
                    Field12 = Field12.Trim();
                   // string ChnageField2 = "Field2";

                    Field13 = dt1.Columns[i + 7].ToString();
                    Field13 = Field13.Trim();

                    Field14 = dt1.Columns[i + 8].ToString();
                    Field14 = Field14.Trim();

                    Field15 = dt1.Columns[i + 9].ToString();
                    Field15 = Field15.Trim();

                    Field16 = dt1.Columns[i + 10].ToString();
                    Field16 = Field16.Trim();

                    Field17 = dt1.Columns[i + 11].ToString();
                    Field17 = Field17.Trim();

                    Field18 = dt1.Columns[i + 12].ToString();
                    Field18 = Field18.Trim();

                    Field19 = dt1.Columns[i + 13].ToString();
                    Field19 = Field19.Trim();

                    Field110 = dt1.Columns[i + 14].ToString();
                    Field110 = Field110.Trim();

                    Field111 = dt1.Columns[i + 15].ToString();
                    Field111 = Field111.Trim();

                    Field112 = dt1.Columns[i + 16].ToString();
                    Field112 = Field112.Trim();

                    Field113 = dt1.Columns[i + 17].ToString();
                    Field113 = Field113.Trim();

                    Field114 = dt1.Columns[i + 18].ToString();
                    Field114 = Field114.Trim();

                    Field115 = dt1.Columns[i + 19].ToString();
                    Field115 = Field115.Trim();

                    SqlCommand cmd = new SqlCommand("SP_UploadkeywordExcelDHeading", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                    cmd.Parameters.AddWithValue("@Prefix", Prefix);
                    cmd.Parameters.AddWithValue("@CustomerName", CustName);
                    cmd.Parameters.AddWithValue("@Address", Address);
                    cmd.Parameters.AddWithValue("@Category", Category);

                    cmd.Parameters.AddWithValue("@CreatedBy", txtMobileNumber.Text);

                    cmd.Parameters.AddWithValue("@ModifiedBy", txtMobileNumber.Text);

                    cmd.Parameters.AddWithValue("@Field1", Field11);
                    cmd.Parameters.AddWithValue("@Field2", Field12);
                    cmd.Parameters.AddWithValue("@Field3", Field13);
                    cmd.Parameters.AddWithValue("@Field4", Field14);
                    cmd.Parameters.AddWithValue("@Field5", Field15);
                    cmd.Parameters.AddWithValue("@Field6", Field16);
                    cmd.Parameters.AddWithValue("@Field7", Field17);
                    cmd.Parameters.AddWithValue("@Field8", Field18);
                    cmd.Parameters.AddWithValue("@Field9", Field19);
                    cmd.Parameters.AddWithValue("@Field10", Field110);

                    cmd.Parameters.AddWithValue("@Field11", Field111);
                    cmd.Parameters.AddWithValue("@Field12", Field112);
                    cmd.Parameters.AddWithValue("@Field13", Field113);
                    cmd.Parameters.AddWithValue("@Field14", Field114);
                    cmd.Parameters.AddWithValue("@Field15", Field115);



                    con.Open();
                    int A = cmd.ExecuteNonQuery();
                    con.Close();


                }
               // int Total = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //if (i == 1600)
                    //{
                    //    Lblerrer1.Text = "1600 Records allowed to insert at a one time";
                    //    break;
                    //}
                    string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_KeywordExcelUpload]  WHERE CreatedBy='" + txtMobileNumber.Text + "'";
                    int SRNO = Convert.ToInt32(cc.ExecuteScalar(SQl1));

                    SRNO = SRNO + 1;

                    string MobileNo = dt.Rows[i]["MobileNo"].ToString();
                    MobileNo = MobileNo.Trim();

                    string Prefix = dt.Rows[i]["Prefix"].ToString();
                    Prefix = Prefix.Trim();

                    string CustomerName = dt.Rows[i]["CustomerName"].ToString();
                    CustomerName = CustomerName.Trim();

                    string Address = dt.Rows[i]["Address"].ToString();
                    Address = Address.Trim();

                    string Category = dt.Rows[i]["Category"].ToString();
                    Category = Category.Trim();

                    string Field1 = dt.Rows[i][Field11].ToString();
                    Field1 = Field1.Trim();

                    string Field2 = dt.Rows[i][Field12].ToString();
                    Field2 = Field2.Trim();

                    string Field3 = dt.Rows[i][Field13].ToString();
                    Field3 = Field3.Trim();

                    string Field4 = dt.Rows[i][Field14].ToString();
                    Field4 = Field4.Trim();

                    string Field5 = dt.Rows[i][Field15].ToString();
                    Field5 = Field5.Trim();

                    string Field6 = dt.Rows[i][Field16].ToString();
                    Field6 = Field6.Trim();

                    string Field7 = dt.Rows[i][Field17].ToString();
                    Field7 = Field7.Trim();

                    string Field8 = dt.Rows[i][Field18].ToString();
                    Field8 = Field8.Trim();

                    string Field9 = dt.Rows[i][Field19].ToString();
                    Field9 = Field9.Trim();

                    string Field10 = dt.Rows[i][Field110].ToString();
                    Field10 = Field10.Trim();

                    string NField11 = dt.Rows[i][Field111].ToString();
                    NField11 = NField11.Trim();

                    string NField12 = dt.Rows[i][Field112].ToString();
                    NField12 = NField12.Trim();

                    string NField13 = dt.Rows[i][Field113].ToString();
                    NField13 = NField13.Trim();

                    string NField14 = dt.Rows[i][Field114].ToString();
                    NField14 = NField14.Trim();

                    string NField15 = dt.Rows[i][Field115].ToString();
                    NField15 = NField15.Trim();



                    //string Category = dt.Rows[i]["Category"].ToString();
                    //Category = Category.Trim();


                    SqlCommand cmd = new SqlCommand("SP_UploadKeywordtDetails", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@SRNO", SRNO);
                    cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                    cmd.Parameters.AddWithValue("@Prefix", Prefix);
                    cmd.Parameters.AddWithValue("@CustomerName", CustomerName);

                    cmd.Parameters.AddWithValue("@Address", Address);
                    cmd.Parameters.AddWithValue("@Category", Category);
                    cmd.Parameters.AddWithValue("@Keyword", ddlKeyword.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtMobileNumber.Text);
                    ////cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    cmd.Parameters.AddWithValue("@ModifiedBy", txtMobileNumber.Text);
                    cmd.Parameters.AddWithValue("@Field1", Field1);
                    cmd.Parameters.AddWithValue("@Field2", Field2);
                    cmd.Parameters.AddWithValue("@Field3", Field3);
                    cmd.Parameters.AddWithValue("@Field4", Field4);
                    cmd.Parameters.AddWithValue("@Field5", Field5);
                    cmd.Parameters.AddWithValue("@Field6", Field6);
                    cmd.Parameters.AddWithValue("@Field7", Field7);
                    cmd.Parameters.AddWithValue("@Field8", Field8);
                    cmd.Parameters.AddWithValue("@Field9", Field9);
                    cmd.Parameters.AddWithValue("@Field10", Field10);

                    cmd.Parameters.AddWithValue("@Field11", NField11);
                    cmd.Parameters.AddWithValue("@Field12", NField12);
                    cmd.Parameters.AddWithValue("@Field13", NField13);
                    cmd.Parameters.AddWithValue("@Field14", NField14);
                    cmd.Parameters.AddWithValue("@Field15", NField15);

                    con.Open();
                    int A = cmd.ExecuteNonQuery();

                    //string SQl2 = "SELECT COUNT(SrNo) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy" + txtMobileNumber.Text + "'";
                    //Total = Convert.ToInt32(cc.ExecuteScalar(SQl1));

                    con.Close();
                }

                Lblerrer.Text = "DATA IMPORTED SUCCESSFULLY. Total Contacts uploaded for No. ";
                //Lblerrer.Attributes.Add("style", "color:black");
            }
            catch (Exception ex)
            {
                //EL.SendErrorToText(ex);
                Lblerrer.Text = "DATA IMPORTED UNSUCCESSFULLY.";
                Lblerrer.Attributes.Add("style", "color:red");
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Exceluplode();
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Response.Redirect("./KeywordDetails.aspx");
        }

        protected void btnDownloadExcel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UploadExcel/Needly_Keywords.xls");
        }

        public void KeywordBind()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadKeyword");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MobileNo", txtMobileNumber.Text);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlKeyword.DataSource = ds.Tables[0];
                        ddlKeyword.DataTextField = "Keyword";
                        ddlKeyword.DataValueField = "Id";

                        ddlKeyword.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlKeyword.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlKeyword.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlKeyword.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

    }
}