﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication3
{
    public class WhatsAppConfig
    {

        public string link = @"https://web.whatsapp.com";
        public string linkSendSMS = @"https://web.whatsapp.com/send?phone=";
        public string ID { get; set; }
        public string msg_content { get; set; }
        public string mobile_number { get; set; }
        public string To_number { get; set; }
        public string Resultcode { get; set; }
        public string Message { get; set; }


    }
}
