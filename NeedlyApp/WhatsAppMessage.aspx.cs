﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Text;
using OpenQA.Selenium.Remote;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Xml;
using System.Net;
using System.Globalization;
using System.Security.Cryptography;

using System.Data.OleDb;
using System.Data.SqlClient;
using DAL;


namespace WebApplication3
{
    public partial class WhatsAppMessage : System.Web.UI.Page
    {

        WhatsAppConfig wa = new WhatsAppConfig();
        IWebDriver driver;
        Comman cmn = new Comman();
        int argFirstBrowserType = 2, delay = 4;
        string browserType = string.Empty;
       
        public void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    lblshowinstance.Text = string.Empty;
                    Session["WebDriver"] = null;
                    lblmessage.Text = string.Empty;
                    lblloggeninMNo.Text = string.Empty;
                    gridmsglist.DataSource = null;
                    gridmsglist.DataBind();
                    lbltotsentsms.Text = string.Empty;
                    lblpendingmsg.Text = string.Empty;
                    lblbtotalsms.Text = string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void btnAddinstance_Click(object sender, EventArgs e)
        {
            bool flag = false;
            string mobieNo = string.Empty;


            driver = new ChromeDriver(@"E:\\CHROMEExe");
            driver.Navigate().GoToUrl("https://web.whatsapp.com");


            if (Session["WebDriver"] == null)
            {

                browserType = Comman.getBrowser(argFirstBrowserType);
                // driver = Comman.SetBrowser(browserType);
                //  driver.Navigate().GoToUrl(wa.link);

                

                // driver  = new ChromeDriver(@"D:\\Tools");
                // driver.Navigate().GoToUrl("https://web.whatsapp.com");
                driver.Manage().Window.Maximize();
                #region login section
                do
                {
                    try
                    {
                        driver.FindElement(By.ClassName("_2MwRD"));
                        flag = true;
                    }
                    catch (Exception ex)
                    {
                    }
                } while (flag == false);

                #endregion login section
                if (flag)
                {
                    lblshowinstance.Text = "Instance Added";
                    lblshowinstance.ForeColor = System.Drawing.Color.LightCoral;
                    lblloggeninMNo.Text = "";
                    Session["WebDriver"] = driver;
                    mobieNo = cmn.GetMobileNumber(driver);
                    lblloggeninMNo.Text = "You Logged In: " + mobieNo;
                    lblloggeninMNo.ForeColor = System.Drawing.Color.Blue;
                    driver.Manage().Window.Minimize();
                }
                else
                {
                    driver.Manage().Window.Maximize();
                    lblloggeninMNo.Text = "";
                    lblshowinstance.Text = "Instance Not added.Please add your whatsapp instance code";
                    lblshowinstance.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                driver = (IWebDriver)Session["WebDriver"];
                lblshowinstance.Text = "Instance Added";
                lblshowinstance.ForeColor = System.Drawing.Color.LightCoral;
                mobieNo = mobieNo = cmn.GetMobileNumber(driver);
                lblloggeninMNo.Text = "You Logged In: " + mobieNo;
                lblloggeninMNo.ForeColor = System.Drawing.Color.Blue;
                driver.Manage().Window.Minimize();
            }
        }

        protected void btnSendSms_Click(object sender, EventArgs e)
        {
            lblmessage.Text = string.Empty;
            WhatsAppConfig wa = new WhatsAppConfig();
            List<WhatsAppConfig> sentList = new List<WhatsAppConfig>();
            int lstcnt = 2, sendMessage = 1, pendingMessage = 0;
            string mobileNumber = "918411835832", Message = "test msg";
            lbltotsentsms.Text = "0";
            lblpendingmsg.Text = string.Empty;
            lblbtotalsms.Text = "Total Message: " + lstcnt.ToString();       

            if (lblshowinstance.Text == "Instance Added")
            {
                driver = (IWebDriver)Session["WebDriver"];
                try
                {
                    Console.WriteLine("Start send whats App Message");
                    for (int i = 1; i <= lstcnt; i++)
                    {
                        wa = cmn.SendMessage(driver, mobileNumber, Message, delay);
                        if (wa.Resultcode == "0")
                        {
                            wa.To_number = mobileNumber;
                            wa.Message = Message;
                            wa.mobile_number = lblloggeninMNo.Text;
                            sentList.Add(wa);
                            lbltotsentsms.Text = "Send Message: " + Convert.ToString(sendMessage);
                            pendingMessage = lstcnt - sendMessage;
                            lblpendingmsg.Text = "Pending Message " + Convert.ToString(pendingMessage);
                            sendMessage++;
                            Thread.Sleep(300);
                            continue;
                        }
                        else
                        {
                            break;

                        }
                    }

                    if (wa.Resultcode == "0")
                    {
                        lblmessage.Text = "Message send successfully. Total count: =  " + sentList.Count;
                        lblmessage.ForeColor = System.Drawing.Color.Green;
                    }
                    else if (wa.Resultcode == "99")
                    {
                        lblmessage.Text = "Error while sending:" + wa.Message;
                        lblmessage.ForeColor = System.Drawing.Color.Red;
                    }
                    else
                    {
                        lblmessage.Text = "Error while sending:" + wa.Message;
                        lblmessage.ForeColor = System.Drawing.Color.Red;
                    }

                    Console.WriteLine("End send whats App Message");
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Exception while sending whats App Message");
                    throw ex;
                }
                finally
                {
                    if (sentList.Count > 0)
                    {
                        //You can maintain here your database script for updated send list.
                        //Also store the message list for historical purpose.

                        gridmsglist.DataSource = sentList;
                        gridmsglist.DataBind();
                    }
                }
            }
            else
            {
                lblloggeninMNo.Text = "";
                lblshowinstance.Text = "Instance Not added.Please add your whatsapp instance code";
                lblshowinstance.ForeColor = System.Drawing.Color.Red;
                lbltotsentsms.Text = string.Empty;
                lblpendingmsg.Text = string.Empty;
                lblbtotalsms.Text = string.Empty;
                gridmsglist.DataSource = null;
                gridmsglist.DataBind();
            }
        }
    }
}