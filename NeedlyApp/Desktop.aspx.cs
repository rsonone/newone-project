﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using System.IO.Compression;
using NPOI.Util;
using System.IO;

using System.Data;
using Ionic.Zip;
using Ionic.Zlib;

namespace NeedlyApp
{
    public partial class Desktop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            //Response.Clear();
            //Response.BufferOutput = false;
            //Response.ContentType = "application/zip";
            //Response.AddHeader("content-disposition", "attachment; filename=pauls_chapel_audio.zip");

            //using (ZipFile zip = new ZipFile("pauls_chapel_audio.zip"))
            //{
            //    zip.CompressionLevel = CompressionLevel.None;
            //    zip.AddSelectedFiles("*.mp3", Server.MapPath("~/content/audio/"), "", false);
            //    zip.Save(Response.OutputStream);
            //}

            //Response.Close();
        }
       
          

        protected void Button1_Click(object sender, EventArgs e)
        {
            // Response.ContentType = "application/exe";
            // string filename = "setup.exe";
            //// zipfile.AddFile(filePath, "");

            // Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            // using (Ionic.Zip.ZipFile zipfile = new ZipFile())
            // {
            //     zipfile.AddSelectedFiles("~/MasterPage/uploadExcel/setup.zip", true);
            //     zipfile.Save(Response.OutputStream);
            // }


            //// string fileName;
            // var stream = new MemoryStream();
            // //  Response.Redirect("~/MasterPage/uploadExcel/WhatApp_Final_Setup.zip");
            // string filePath = "~/setup.zip";

            // Response.ContentType = "file/zip";
            // Response.AddHeader("Content-Disposition", "attachment;filename=\"" + filePath + "\"");
            // //Response.TransmitFile(Server.MapPath(filePath));
            // Response.End();

            string filename = "Needly V.10.msi";
            if (filename != "")
            {
                string path = Server.MapPath(filename);
                System.IO.FileInfo file = new System.IO.FileInfo(path);
                if (file.Exists)
                {
                    Response.Clear();
                    //Content-Disposition will tell the browser how to treat the file.(e.g. in case of jpg file, Either to display the file in browser or download it)
                    //Here the attachement is important. which is telling the browser to output as an attachment and the name that is to be displayed on the download dialog
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                    //Telling length of the content..
                    Response.AddHeader("Content-Length", file.Length.ToString());

                    //Type of the file, whether it is exe, pdf, jpeg etc etc
                    Response.ContentType = "application/octet-stream";

                    //Writing the content of the file in response to send back to client..
                    Response.WriteFile(file.FullName);
                    Response.End();
                }
                else
                {
                    Response.Write("This file does not exist.");
                }
            }

        }

    }
}