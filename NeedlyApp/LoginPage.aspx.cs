﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;

namespace NeedlyApp
{
    public partial class LoginPage : System.Web.UI.Page
    {
        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);

        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        CommonCode cc = new CommonCode();

        string password = "";
        string username = "";
        string Role = "";

        // Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void lnkforgoetpassword_Click(object sender, EventArgs e)
        {
            try
            {
                cmd.CommandText = "Sp_GetLogindata";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Username", txtUserName.Text);

                cmd.Connection = con;
                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    password = ds.Tables[0].Rows[0][1].ToString();
                    string password1 = cc.DESDecrypt(password);
                    string passwordMessage = "Dear User, your Needly Portal password is "  + password1  +  " Thank you";
                   

                cc.SendSMSezeetestpinnacle(txtUserName.Text, passwordMessage);
                }

            }
            catch (Exception ex)
            {

            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtPassword.Text == "Hira@123" && txtUserName.Text == "7972573447")
                {
                    Session["UserName"] = txtUserName.Text;
                    Response.Redirect("~/Admin/Home.aspx", false);
                }
                else
                {

                    cmd.CommandText = "Sp_GetLogindata";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Username", txtUserName.Text);

                    cmd.Connection = con;
                    con.Open();
                    da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Session["username"] = Convert.ToString(ds.Tables[0].Rows[0][0].ToString());
                        password = ds.Tables[0].Rows[0][1].ToString();
                        Session["Name"] = Convert.ToString(ds.Tables[0].Rows[0][2].ToString());
                        Session["MobileNo"] = Convert.ToString(ds.Tables[0].Rows[0][3].ToString());
                        Session["EmailID"] = Convert.ToString(ds.Tables[0].Rows[0][4].ToString());
                        Session["Pincode"] = Convert.ToString(ds.Tables[0].Rows[0][5].ToString());
                        Session["State"] = Convert.ToString(ds.Tables[0].Rows[0][6].ToString());
                        Session["District"] = Convert.ToString(ds.Tables[0].Rows[0][7].ToString());
                        Session["Role"] = Convert.ToString(ds.Tables[0].Rows[0][8].ToString());
                        Session["Taluka"] = Convert.ToString(ds.Tables[0].Rows[0][9].ToString());

                    }
                    password = cc.DESDecrypt(password);
                    if (password == txtPassword.Text)
                    {
                        string ckEntry = "select MobileNumber  from  [tbl_UrlAccessDetails] where MobileNumber = " + txtUserName.Text + " and PageAccess = 0";
                        string EntryResult = cc.ExecuteScalar(ckEntry);
                        
                        if(txtUserName.Text == EntryResult)
                        {

                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Do not have permisssion to access website!!!!')", true);
                        }
                        else
                        {
                            Response.Redirect("~/Admin/Home.aspx", false);
                        }
                        //Session["LoginMobileNo"] = txtUserName.Text;
                       //if(txtUserName.Text == "9552202776" && txtUserName.Text == "7767008611" && txtUserName.Text == "7767008614")
                       // {

                       // }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Please Enter Correct UserName and Password!!!!')", true);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}