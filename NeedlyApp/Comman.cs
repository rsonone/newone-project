﻿using System;
using System.IO;
using System.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace WebApplication3
{
    public class Comman
    {
        internal static string getBrowser(int value)
        {
            string browserType = "";
            if (value == 1)
            {
                browserType = "MOZILLA";
            }
            else
                if (value == 2)
            {
                browserType = "CHROME";
            }
            else
                    if (value == 3)
            {
                browserType = "IE";
            }

            return browserType;
        }
        
        internal static IWebDriver SetBrowser(string browserType)
        {
            IWebDriver browser = null;
            string uCaseBrowser = browserType.ToUpperInvariant();
            string driverPath = string.Empty, fullpath = string.Empty;
            driverPath = HttpContext.Current.Server.MapPath("~/CHROMEExe");
            fullpath = Path.Combine(driverPath, "chromedriver.exe");
            switch (uCaseBrowser)
            {
                case "MOZILLA":

                    FirefoxOptions fireFoxOptions = new FirefoxOptions();
                    fireFoxOptions.AddArgument("--start-maximized");
                    fireFoxOptions.AddArgument("--disable-extensions");
                    browser = new FirefoxDriver(driverPath, fireFoxOptions);
                    break;

                case "CHROME":

                    ChromeOptions options = new ChromeOptions();
                    options.AddArgument("--start-maximized");
                    options.AddArgument("--disable-extensions");
                    //options.BinaryLocation = @"C:\Program Files\Google\Chrome\Application\chrome.exe";
                    browser = new ChromeDriver(driverPath, options);
                    break;

                case "IE":

                    InternetExplorerOptions opt = new InternetExplorerOptions();
                    opt.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    browser = new InternetExplorerDriver(opt);
                    break;
            }
            return browser;
        }

        public WhatsAppConfig SendMessage(IWebDriver driver, string number, string message, int delay)
        {
            bool flag = false;
            WhatsAppConfig wa = new WhatsAppConfig();
            int count = 0;
            do
            {
                try
                {
                    driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message) + "&app_absent=1");
                    driver.FindElement(By.CssSelector("div._8ibw>a")).Click();
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(delay); //Wait for maximun of 10 seconds if any element is not found                                       
                    driver.FindElement(By.CssSelector("button._1E0Oz>span")).Click();//Click SEND Arrow Button
                    flag = true;
                    wa.Resultcode = "0";
                    wa.Message = "Sent";
                }
                catch (Exception ex)
                {

                    string m = ex.Message;
                    count++;
                    if (count == 100)
                    {
                        wa.Resultcode = "100";
                        wa.Message = "Error while sending whatsApp message.Please check the contact list" + m;
                        return wa;
                    }
                    try
                    {
                        if (driver.FindElement(By.ClassName("_3SRfO")).Text.Contains("Phone number shared via url is invalid."))
                        {
                            wa.Resultcode = "99";
                            wa.Message = "Phone number shared via url is invalid.Please make sure number having started 91";
                            return wa;
                        }
                    }
                    catch (Exception esx)
                    {

                    }
                }
            } while (flag == false);

            return wa;
        }
        public string GetMobileNumber(IWebDriver driver)
        {
            string mobileNumber = string.Empty;
            bool flag = false; string src = string.Empty;
            try
            {
                Console.WriteLine("Start Getting getting a mobile no");
                do
                {
                    try
                    {
                        src = driver.FindElement(By.CssSelector("div.-y4n1>img")).GetAttribute("src");
                        flag = true;
                    }
                    catch (Exception ex)
                    {
                    }
                } while (flag == false);

                if (flag)
                {
                    string[] splitedstr = src.Split('&');
                    mobileNumber = splitedstr[2].Substring(2, 12);
                }
                Console.WriteLine("End Getting getting a mobile no");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while getting a mobile no" + ex.Message);
            }
            return mobileNumber;
        }
    }
}