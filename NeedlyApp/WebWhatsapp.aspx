﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" ValidateRequest="false" AutoEventWireup="true" CodeBehind="WebWhatsapp.aspx.cs" Inherits="NeedlyApp.WebWhatsapp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-list"></i>Direct WhatsApp Message<small></small></h2>
                            <div class="container-fluid">
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div style="width: 100%">
                            <table style="width: 100%">
                                <%-- <tr>
                    <td style="width: 30%; float: left">
                        <asp:Label Font-Size="x-large" runat="server" Text="" ID="lblshowinstance"></asp:Label>
                    </td>
                    <td style="width: 50%; float: left">
                        <asp:Label runat="server" Font-Size="x-large" Text="" ID="lblloggeninMNo1"></asp:Label>
                    </td>
                </tr>--%>
                                <tr>
                                    <td style="width: 100%; height: 2px" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td style="width: 100%" colspan="2">
                                        <asp:Label runat="server" Text="" ID="lblmessage"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; height: 5px" colspan="2"></td>
                                </tr>
                               <%-- <tr>
                                    <td style="width: 20%; float: left">
                                        <asp:Label Font-Size="x-large" runat="server" ForeColor="Blue" Text="" ID="lblbtotalsms"></asp:Label>
                                    </td>
                                    <td style="width: 70%; float: left">
                                        <asp:Label runat="server" Font-Size="x-large" ForeColor="Green" Text=" " ID="lbltotsentsms"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;
                        <asp:Label runat="server" ForeColor="Red" Font-Size="x-large" Text="" ID="lblpendingmsg"></asp:Label>

                                    </td>
                                </tr>--%>
                                <tr>
                                    <td style="width: 100%; height: 5px" colspan="2"></td>
                                </tr>

                            </table>
                        </div>

                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <asp:Button ID="btnAddinstance" CssClass="btn btn-success" runat="server" Text="Scan WhatsApp" OnClick="btnAddinstance_Click" />
                                                <asp:Label Font-Size="x-large" runat="server" Text="" ID="lblshowinstance"></asp:Label>
                                            </div>
                                            <div class="col-sm-6">
                                                <asp:Label runat="server" Font-Size="x-large" Text="" ID="lblloggeninMNo"></asp:Label>
                                                <b></b>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-5">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                                Select Contact
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:RadioButtonList ID="rdbSelectContact" runat="server" OnSelectedIndexChanged="rdbSelectContact_SelectedIndexChanged" RepeatDirection="Vertical" AutoPostBack="true">
                                                    <asp:ListItem Value="1">&nbsp;&nbsp;Imported From Excel&nbsp;&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="2">&nbsp;&nbsp;imported From PhoneBook&nbsp;&nbsp;</asp:ListItem>

                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="box-body col-sm-7">

                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Select Template
                                        </label>
                                        <div class="col-sm-9">
                                            <div class="table-responsive">
                                                <div class="row" style="height: 200px; overflow: auto">
                                                    <div class="panel-body">

                                                        <asp:GridView ID="GvTemplte" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>

                                                                    <ItemTemplate>

                                                                        <asp:RadioButton ID="RadioButton1" runat="server" />

                                                                  <%--  <input name ="RadioButton1" type="radio" value ='<%# Eval("Id") %>' onclick="javascript.myselection(this.id)"/>
                                                                  --%>  </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="TemplateContent" />



                                                            </Columns>
                                                            <EditRowStyle BackColor="#2461BF" />
                                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />

                                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#EFF3FB" />
                                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                        </asp:GridView>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-10">
                                        <div class="form-group">
                                            <asp:Label ID="lblExcelCountMsg" Font-Bold="true" Font-Size="Larger" ForeColor="Black" runat="server" Visible="false" Text="Total Contacts Uploded :"></asp:Label>

                                            <asp:Label ID="LblContactCount" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <asp:Panel ID="PnlSearch" runat="server" Visible="false">
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="box-body col-sm-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="input-name2">
                                                    Select Sr.No
                                                </label>

                                                <div class="col-sm-8 ">
                                                    <div class="input-group">
                                                        <asp:DropDownList ID="DdlSrno1" class="form-control" Width="100px" runat="server">
                                                            <asp:ListItem Value="0">-- Sr.No --</asp:ListItem>
                                                        </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:DropDownList ID="DdlSrno2" class="form-control" Width="100px" runat="server" Style="padding-left: 2px">
                                                    <asp:ListItem Value="0">-- Sr.No --</asp:ListItem>
                                                </asp:DropDownList>
                                                        <asp:Button ID="BtnSearchBySrNo" CssClass="btn btn-success" OnClick="BtnSearchBySrNo_Click" runat="server" Text="Search" />
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="box-body col-sm-6">

                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtMobileNumber" placeholder="Enter Mobile Number" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-5">

                                                <asp:Button ID="btnSearchByMobile" CssClass="btn btn-success" OnClick="btnSearchByMobile_Click" runat="server" Text="Search" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PnlPhoneContacts" runat="server" Visible="False">
                            <asp:GridView ID="GvPhoneContact" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">

                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="MobileNo" HeaderText="Mobile" />
                                    <asp:BoundField DataField="FirstName" HeaderText="Name" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAllSelect" AutoPostBack="true" runat="server" Text=" Select All" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />

                            </asp:GridView>
                        </asp:Panel>
                        <asp:Panel ID="PnlShowExclData" runat="server" Visible="false">
                            <div class="table-responsive">
                                <div class="row" style="height: 400px; overflow: auto">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <asp:GridView ID="GvShowExcelData" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:BoundField DataField="SrNo" HeaderText="Sr.No" />
                                                    <asp:BoundField DataField="MobileNo" HeaderText="Mobile Number" />
                                                    <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" />
                                                    <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" />

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" onclick="CheckAll(this);" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                                <EditRowStyle BackColor="#2461BF" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlCB" runat="server" Visible="true">
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="box-body col-sm-10">
                                            <div class="form-group">
                                                <div class="col-sm-5">
                                                    <asp:CheckBox ID="cbIagree" AutoPostBack="true" OnCheckedChanged="cbIagree_CheckedChanged" runat="server" />
                                                    <asp:Label ID="lblIagree" runat="server" Text="click on checkbox to agree and furthr process"></asp:Label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:Button ID="btnSendSms" Visible="false" runat="server" class="btn btn-success" Text="Send WhatsApp Message" OnClick="btnSendSms_Click" />
                                                    <%-- <asp:Button ID="btnSendWhatsappmsg"  runat="server" Text="Send WhatsApp Message" class="btn btn-success" />
                                                    --%>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

<script language="javascript" type ="text/javascript" >
    function myselection(rbtnid) {
        var rbtn = document.getElementById(rbtnid);
        var rbtnlist = document.getElementByIdTagname("input");
        for (i = 0; i < rbtnlist.lenght; i++) {
            if (rbtnlist[i].text == "radio" && rbtnlist[i].id != rbtn.id) {
                rbtnlist[i].checked = false;
            }
        }
    }
    </script>



</asp:Content>
