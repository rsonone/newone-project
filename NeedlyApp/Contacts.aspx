﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Contact.Master" AutoEventWireup="true" CodeBehind="Contacts.aspx.cs" Inherits="EzeeForms.Contacts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
         
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2 style="color: #0033CC"><i class="fa fa-list"></i> Contact  Master<small></small></h2>
                    <div class="container">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
     <asp:Panel ID="panelExcel" Visible="True" runat="server">


                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
         <div class="col-md-12">
                        <br />

                        <div class="box-body col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Enter App user Mobile Number
                                </label>
                                <div class="col-sm-9">
                                    
                                            <asp:TextBox CssClass="form-control" ID="txtMobileNumber"  required="required" runat="server"></asp:TextBox>
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                       
                        <div class="box-body col-sm-2">
                        </div>
                    </div>

                 <div class="col-md-12">
                        <br />

                        <div class="box-body col-sm-8">
                            <div class="form-group">
                                <label class="col-sm-12 control-label"  for="input-name2" style="color:red">
                                   * Not allowed to change MobileNo, Name, Address and Category Fields heading 
                                </label>
                               
                            </div>
                        </div>
                       
                        <div class="box-body col-sm-2">
                        </div>
                    </div>



                    <div class="col-md-12">
                        <br />

                        <div class="box-body col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Excel :
                                </label>
                                <div class="col-sm-9">
                                    <asp:FileUpload ID="flUploadExcel" runat="server" />
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." OnClick="lnkbtnDownload_Click"  ForeColor="Blue"></asp:LinkButton><br />
                            <br />
                        </div>
                        <div class="box-body col-sm-2">
                        </div>
                    </div>
         <br />
         <br />
                </asp:Panel>
                      <div class="col-md-12">
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Lblerrer" runat="server"></asp:Label><br />
                        <asp:Label ID="Lblerrer1" ForeColor="Red" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="btnExcelUplod" runat="server" Text="Save" Visible="false" class="btn btn-round btn-success" />

                        <asp:Button ID="btnSave" runat="server" Visible="true" Text="Save" class="btn btn-round btn-success" OnClick="btnSave_Click"  />
                        <asp:Button ID="btnReset" runat="server" Text="Reset" class="btn btn-round btn-primary" />
                    </div>
                    <div class="box-body col-sm-4">
                    </div>
                </div>
                  </div>
        </div>
    </div>
</asp:Content>
