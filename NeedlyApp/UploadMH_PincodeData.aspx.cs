﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;

namespace NeedlyApp
{
    public partial class UploadMH_PincodeData : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("~/UploadExcel/") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName); 

                             SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }

        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string MoileNumber = dt.Rows[i]["MobileNumber"].ToString();
                        MoileNumber = MoileNumber.Trim();

                        //string ShopType = dt.Rows[i]["ShopType"].ToString();
                        //ShopType = ShopType.Trim();

                        string Name = dt.Rows[i]["Name"].ToString();
                        Name = Name.Trim();
                      

                        string ServiceProvider = dt.Rows[i]["ServiceProvider"].ToString();
                        ServiceProvider = ServiceProvider.Trim();

                       
                        SqlCommand cmd = new SqlCommand("SP_UploadMHPincodeData", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@MobileNumber", MoileNumber);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@ServiceProvider", ServiceProvider);
                        cmd.Parameters.AddWithValue("@Pincode", txtPincode.Text);
                        
                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        DataBaseConnection.Close();
                        //GridView();
                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }

        protected void btnBulkSubmit_Click(object sender, EventArgs e)
        {
            //Upload and save the file
            string excelPath = Server.MapPath("~/UploadExcel/") + Path.GetFileName(Fileupload1.PostedFile.FileName);
            Fileupload1.SaveAs(excelPath);

            string conString = string.Empty;
            string extension = Path.GetExtension(Fileupload1.PostedFile.FileName);
            switch (extension)
            {
                case ".xls": //Excel 97-03
                    conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                    break;
                case ".xlsx": //Excel 07 or higher
                    conString = ConfigurationManager.ConnectionStrings["Excel07+ConString"].ConnectionString;
                    break;

            }
            conString = string.Format(conString, excelPath);
            using (OleDbConnection excel_con = new OleDbConnection(conString))
            {
                excel_con.Open();
                string sheet1 = excel_con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null).Rows[0]["TABLE_NAME"].ToString();
                DataTable dtExcelData = new DataTable();

                
                
                using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT * FROM [" + sheet1 + "]", excel_con))
                {
                    oda.Fill(dtExcelData);
                }
                excel_con.Close();

                //string consString = ConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString;
                //using (SqlConnection connn = new SqlConnection(consString))
                  SqlConnection conNew = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conNew))
                    {
                        try
                        { //Set the database table name
                            sqlBulkCopy.DestinationTableName = "dbo.tbl_MH_PIncodeData";
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandTimeout = 950;
                        //[OPTIONAL]: Map the Excel columns with that of the database table


                        conNew.Open();
                            sqlBulkCopy.WriteToServer(dtExcelData);
                        conNew.Close();
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                        }
                        catch (Exception ex)
                        {

                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Failed To Submit data.!!!')", true);
                            cc.WriteError(ex);
                        }
                       
                    }
                
            }
        }
    }
}