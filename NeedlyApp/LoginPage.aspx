﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="NeedlyApp.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   	<title>NEEDLY LOGIN</title>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="log.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/bootstrap/css/bootstrap.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/animate/animate.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/css-hamburgers/hamburgers.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/vendor/select2/select2.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="Logincss/css/util.css"/>
	<link rel="stylesheet" type="text/css" href="Logincss/css/main.css"/>
<!--===============================================================================================-->
</head>
<body>
    
    
       <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="Logincss/images/Needly_Logo-Playstore.png" alt="IMG"/>
				</div>
           

				<form runat="server" class="login100-form validate-form">
					<span class="login100-form-title">
						 LOGIN
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid Mobile is required:9422325020">
						
                        <asp:TextBox ID="txtUserName" class="input100" placeholder="Mobile No" runat="server"></asp:TextBox>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-mobile" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <asp:TextBox ID="txtPassword" class="input100" placeholder="Password" runat="server" TextMode="Password"></asp:TextBox>
						
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						
							<asp:Button ID="btnLogin"  class="login100-form-btn" runat="server" OnClick="btnLogin_Click" Text="Login" />
							
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2">
						<asp:LinkButton ID="lnkforgoetpassword" OnClick="lnkforgoetpassword_Click" runat="server">Password?</asp:LinkButton>
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
   
    	
<!--===============================================================================================-->	
	<script src="Logincss/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="Logincss/vendor/bootstrap/js/popper.js"></script>
	<script src="Logincss/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="Logincss/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="Logincss/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
	</script>
<!--===============================================================================================-->
	<script src="Logincss/js/main.js"></script>

</body>
</html>

