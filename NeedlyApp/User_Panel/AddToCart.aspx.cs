﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace NeedlyApp.User_Panel
{
    public partial class AddToCart : System.Web.UI.Page
    {
      
            SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                DataTable dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("Sr.No");
                dt.Columns.Add("Id");
                dt.Columns.Add("ItemName");
                dt.Columns.Add("Image_Path");
                dt.Columns.Add("Rate");
                dt.Columns.Add("quantity");

                if(Request.QueryString["id"] != null)
                {
                    if(Session["BuyItems"] == null)
                    {
                        dr = dt.NewRow();
                        //  string DBConnection = con;
                        string myQuery = "Select * from [tbl_InventoryItem] where Id = " + Request.QueryString["Id"] ;
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = myQuery;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        dr["sr.no"] = 1;
                        dr["Id"] = ds.Tables[0].Rows[0]["Id"].ToString();
                        dr["ItemName"] = ds.Tables[0].Rows[0]["ItemName"].ToString();
                        dr["Image_Path"] = ds.Tables[0].Rows[0]["Image_Path"];
                        dr["Rate"] = ds.Tables[0].Rows[0]["Rate"].ToString();
                        dr["quantity"] = ds.Tables[0].Rows[0]["quantity"].ToString();
                        //  dr["totalprice"] = ds.Tables[0].Rows[0]["totalprice"].ToString();
                        int price = Convert.ToInt32(ds.Tables[0].Rows[0]["Rate"].ToString());
                        int quantity = Convert.ToInt32(Request.QueryString["quantity"].ToString());
                        int totalprice = price * quantity;
                        dr["totalprice"] = totalprice;

                        dt.Rows.Add(dr);
                        GvAddToCart.DataSource = dt;
                        GvAddToCart.DataBind();

                        Session["BuyItems"] = dt;
                        GvAddToCart.FooterRow.Cells[0].Text = "total Amount";
                        GvAddToCart.FooterRow.Cells[0].Text = grandtotal().ToString();
                        Response.Redirect("AddToCart.aspx");
                    }
                    else
                    {
                        dt = (DataTable)Session["BuyItems"];
                        int sr;
                        sr = dt.Rows.Count;

                        dr = dt.NewRow();
                        string myQuery1 = "Select * from [tbl_InventoryItem] where Id = " + Request.QueryString["Id"];
                        SqlCommand cmd = new SqlCommand();
                        cmd.CommandText = myQuery1;
                        cmd.Connection = con;
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        dr["sr.no"] = 1;
                        dr["Id"] = ds.Tables[0].Rows[0]["Id"].ToString();
                        dr["ItemName"] = ds.Tables[0].Rows[0]["ItemName"].ToString();
                        dr["Image_Path"] = ds.Tables[0].Rows[0]["Image_Path"].ToString();
                        dr["Rate"] = ds.Tables[0].Rows[0]["Rate"].ToString();
                        dr["quantity"] = ds.Tables[0].Rows[0]["quantity"].ToString();
                        //  dr["totalprice"] = ds.Tables[0].Rows[0]["totalprice"].ToString();
                        int price = Convert.ToInt32(ds.Tables[0].Rows[0]["Rate"].ToString());
                        int quantity = Convert.ToInt32(Request.QueryString["quantity"].ToString());
                        int totalprice = price * quantity;
                        dr["totalprice"] = totalprice;

                        dt.Rows.Add(dr);
                        GvAddToCart.DataSource = dt;
                        GvAddToCart.DataBind();

                        Session["BuyItems"] = dt;
                        GvAddToCart.FooterRow.Cells[5].Text = "total Amount";
                        GvAddToCart.FooterRow.Cells[6].Text = grandtotal().ToString();
                        Response.Redirect("AddToCart.aspx");

                    }
                }
                else
                {
                    dt = (DataTable)Session["BuyItems"];
                    GvAddToCart.DataSource = dt;
                    GvAddToCart.DataBind();
                    if(GvAddToCart.Rows.Count > 0)
                    {
                        GvAddToCart.FooterRow.Cells[0].Text = "total Amount";
                        GvAddToCart.FooterRow.Cells[0].Text = grandtotal().ToString();
                    }
                }
              Label1.Text =  GvAddToCart.Rows.Count.ToString();


            }
        }

        public int grandtotal()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["BuyItems"];
            int nrow = dt.Rows.Count;
            int i = 0;
            int gtotal = 0;
            while(i < nrow)
            {
                gtotal = gtotal + Convert.ToInt32(dt.Rows[i]["totalprice"].ToString());
                i = i + 1;
            }
            return gtotal;
        }
    }
}