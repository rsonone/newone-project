﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AddToCart.aspx.cs" Inherits="NeedlyApp.User_Panel.AddToCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" runat="server" Font-Bold="true" ForeColor="Black" Font-Size="Medium" Text="You have added following product in your cart"></asp:Label>
    <br />

    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/User_Panel/ShoppingCart.aspx" Font-Bold="true" ForeColor="Blue" Font-Size="Large" runat="server">Continue shopping</asp:HyperLink>

    <br />
    <asp:GridView ID="GvAddToCart" CssClass="table-responsive table-hover" runat="server" AutoGenerateColumns="False" Height="87px" CellPadding="4" CellSpacing="4" ForeColor="#333333" GridLines="None" ShowFooter="True" Width="623px">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            
            <asp:BoundField DataField="Id" HeaderText="Product Id">
            <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ItemName" HeaderText="Product Name">
            <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Rate" HeaderText="Price">
            <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:ImageField DataImageUrlField="Image_Path" HeaderText="Product image">
                <ItemStyle HorizontalAlign="Center" />
            </asp:ImageField>
            <asp:BoundField DataField="quantity" HeaderText="Quantity" />
            <asp:BoundField DataField="totalprice" HeaderText="Total Price" />
            <asp:CommandField DeleteText="Remove" ShowDeleteButton="True" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" Height="55px" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
</asp:Content>
