﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace NeedlyApp.User_Panel
{
    public partial class ShoppingCart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["BuyItems"];
            if(dt != null)
            {
                Label7.Text = dt.Rows.Count.ToString();
            }
            else
            {
                Label7.Text = "0";
            }
        }

        protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
        {
            //if(e.CommandName == "addtocart")
            //{
            //    Response.Redirect("addtocart.aspx?id=" + e.CommandArgument.ToString());
            //}
            DropDownList dlist = (DropDownList)(e.Item.FindControl("ddlQuantity"));
            Response.Redirect("addtocart.aspx?id=" + e.CommandArgument.ToString() + "&quantity=" + dlist.SelectedItem.ToString());
        }
    }
}