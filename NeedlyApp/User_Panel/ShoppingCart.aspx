﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="NeedlyApp.User_Panel.ShoppingCart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: center;
            height: 33px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Label ID="Label5" runat="server" ForeColor="Green" Font-Bold="true" Font-Size="Medium" Text="Welcome to MyDukan"></asp:Label>

    <asp:Label ID="Label6" runat="server" Text="No of Products in your Cart"></asp:Label><asp:Label ID="Label7" runat="server" Font-Bold="true" Font-Size="X-Large" Text=" "></asp:Label>
    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/User_Panel/AddToCart.aspx" runat="server">Show Cart</asp:HyperLink>
    
<asp:DataList ID="DataList1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataSourceID="SqlDataSource1" GridLines="Both" RepeatColumns="5" RepeatDirection="Horizontal" OnItemCommand="DataList1_ItemCommand" >
    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
    <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
    <ItemTemplate>
        <table border="1" class="nav-justified">
            <tr>
                <td class="text-center">Product Id
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("ItemName") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Image ID="Image2" runat="server" Height="200px" ImageUrl='<%# Eval("Image_Path") %>'/>
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:Label ID="Label3" runat="server" Text="Rs"></asp:Label>
                    &nbsp;<asp:Label ID="Label4" runat="server" Text='<%# Eval("Rate") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label8" runat="server" Text="Quanity"></asp:Label>
                    &nbsp;<asp:DropDownList ID="ddlQuantity" runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <asp:ImageButton ID="ImageBtnAddToCart" runat="server" Height="42px" ImageAlign="Middle" ImageUrl="~/img/NewAddToCart1.png" Width="151px" CommandName="addtocart" CommandArgument='<%# Eval("Id") %>' />
                </td>
            </tr>
        </table>
        <br />
    </ItemTemplate>
    
    <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
    </asp:DataList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DBNeedlyConnectionString %>" SelectCommand="sp_ImageShow" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
</asp:Content>