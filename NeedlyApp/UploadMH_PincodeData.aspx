﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="UploadMH_PincodeData.aspx.cs" Inherits="NeedlyApp.UploadMH_PincodeData" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  MH Pincode Data Excel Uplaod <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                   
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>

                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Pincode : </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtPincode" runat ="server" CssClass="form-control"></asp:TextBox>
                             </div></div></div>
                     
                    </div>
               </div></div> 

                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Submit" class="btn btn-success" />
                              <asp:Button ID="btnBulkSubmit" OnClick="btnBulkSubmit_Click" runat="server" Text="Bulk Submit" class="btn btn-success" />
                              
                              <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                     <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVInvTtem" CssClass="table table-hover table-bordered" runat="server"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" DataKeyNames="Id">
                    <Columns>   
                    <%--    <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%> 
                          <asp:BoundField DataField="Id" HeaderText="ID" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ItemName" HeaderText="ItemName">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Brand" HeaderText="Brand">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="ProductType" HeaderText="Product">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ShopType" HeaderText="ShopType">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="MRP" HeaderText="MRP">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       <%--  <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>--%>
                          <asp:CommandField ShowEditButton="true" />  
                      <%--  <asp:CommandField ShowDeleteButton="true" />--%>
                       
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div>
                          <asp:Label ID="lblEdit" runat="server" Visible="false"></asp:Label>
                     </div>
           

                    
                  </div>
                </div></div>
</asp:Content>
