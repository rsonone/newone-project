﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace NeedlyApp.Admin
{
    public partial class ReferealReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView();
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowReferalDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReferealReport.DataSource = ds.Tables[0];
                        GVReferealReport.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void GVReferealReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReferealReport.PageIndex = e.NewPageIndex;
            GridView();
        }
    }
}