﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AddTheme.aspx.cs" Inherits="NeedlyApp.Admin.AddTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div class="clearfix"></div>
             <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Banner Details<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
               

              

              
                   

                   
               

                <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Upload Image: </label>
                                   <div class="col-sm-9">
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                       </div></div></div>

                           
                         </div>
               <br />
               <br />
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Discription: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtdescription" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>
           </div>

                </div> 
                     
                   
                <div class="col-md-12">
           <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" class="btn btn-success"/>
                          
           </div>
 <div class="box-body col-sm-4">
     <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
           </div>



        </div>


                       <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVAddTemplate" CssClass="table table-hover table-bordered" runat="server"  
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                     
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="Image"   HeaderText="Image">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Description" HeaderText="Discription">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        <%-- <asp:ImageField DataImageUrlField="BackgroundImage" ItemStyle-Width="50" ControlStyle-Height = "100"  HeaderText="Image">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:ImageField> 
                         <asp:TemplateField HeaderText="Action"  >
                             <ItemTemplate>
                                 <asp:Button ID="btnDelete"  runat="server" CssClass=" btn-danger" Text="Delete" />
                                   
                             </ItemTemplate>
                         </asp:TemplateField>--%>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    
          </div></div></div>
</asp:Content>
