﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AddHelp.aspx.cs" Inherits="NeedlyApp.Admin.AddHelp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add Help Master <small></small></h2>
                       <div class="container-fluid">
            <div class="pull-right">
                  <a href="ViewMultipleEntriesForms.aspx" data-toggle="tooltip" title="View report" class="btn btn-success" > List
                   </a>
                
               <%--  <asp:Button ID="lnkbtnView" CssClass="btn btn-round btn-success" runat="server" OnClick="lnkbtnView_Click"  Text="List"></asp:Button>
                       <button type="button"  onclick=""  >List</button>   --%>                                        
               
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
         
          <div class="row">
            <div class="panel-body"> 

                 <div class="row">
    <div class="col-md-12">
                      Fields marked with * are mandatory.
        </div></div><br />
                 <div class="row">
    <div class="col-md-12">
        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt"
                       Text="Group Code"></asp:Label><span class="warning1" style="color: Red; font-style: normal">*&nbsp;</span>
                                    
                              
                                   <asp:DropDownList ID="ddlgroupcode" CssClass="form-control"   runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div>

          <div class="col-md-6">
                    <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt"
                       Text="Enter Title"></asp:Label><span class="warning1" style="color: Red; font-style: normal">*&nbsp;</span>
                    <asp:TextBox ID="txtTitle" runat="server" Height="32px" class="form-control select2"  Width="400px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTitle"
                           ErrorMessage="Please Enter Title" SetFocusOnError="True" 
                            Width="225px" ValidationGroup="other" Font-Size="Small"></asp:RequiredFieldValidator>
            </div>
       
       

        
    </div></div>
                    <br />

                 <div class="row">
    <div class="col-md-12">
       
      <%--    <div class="col-md-6">
                       <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Names="Arial"
                              Font-Size="11pt" Text="Enter Helpkey"></asp:Label>
                        <span class="warning1" style="color: Red;">*&nbsp;</span>
               <asp:TextBox ID="txtHelpkey" runat="server" Height="32px" class="form-control select2"  Width="400px"></asp:TextBox>
                   
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtHelpkey"
                              ErrorMessage="Please Enter Helpkey" Font-Size="Small" SetFocusOnError="True"
                             ValidationGroup="other" Width="225px"></asp:RequiredFieldValidator>
                                                            </div> --%>  
        <div class="col-md-6">
                    <asp:Label ID="Label2" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt"
                       Text="Enter Description"></asp:Label><span class="warning1" style="color: Red; font-style: normal">*&nbsp;</span>
                                    
                           <asp:TextBox ID="txtDesc" runat="server" Height="50px" Width="400px" class="form-control select2" TextMode="MultiLine"></asp:TextBox>
                     
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDesc"
                            ErrorMessage="Please Enter Description" SetFocusOnError="True"
                            Width="225px" ValidationGroup="other" Font-Size="Small"></asp:RequiredFieldValidator>
            </div>


         <div class="col-md-6">

                    <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt"
                       Text="Enter Youtube Url"></asp:Label><span class="warning1" style="color: Red; font-style: normal">&nbsp;</span>
                                                           
                    <asp:TextBox ID="txtYoutube" runat="server" Height="32px" class="form-control select2" Width="400px"></asp:TextBox>

<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtYoutube"
                            ErrorMessage="Please enter youtube Url" SetFocusOnError="True"
                            Width="225px" ValidationGroup="other" Font-Size="Small"></asp:RequiredFieldValidator>--%>
            </div>

    </div></div>
                    <br />
                 <div class="row">
    <div class="col-md-12">
<%--          <div class="col-md-6">
                       <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Names="Arial"
                              Font-Size="11pt" Text="Enter Image Url"></asp:Label>
                        <span class="warning1" style="color: Red;">*&nbsp;</span>
                        <asp:TextBox ID="txtImgUrl" runat="server" Height="32px" class="form-control select2" Width="400px"></asp:TextBox>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtImgUrl"
                              ErrorMessage="Please Enter Image Url" Font-Size="Small" SetFocusOnError="True"
                             ValidationGroup="other" Width="225px"></asp:RequiredFieldValidator>
                                                            </div>   --%>

        <div class="box-body col-sm-6">
                      <div class="form-group">
                          <label class="col-sm-3 control-label" for="input-name2">
                              Image Upload :
                          </label>
                          <div class="col-sm-9">
                              <asp:FileUpload ID="flUploadExcel" runat="server" AllowMultiple="true" />
                              <br />
                              <br />
                          </div>
                      </div>
                  </div>
       

    </div></div>
                <br />

<div class="col-md-12">
     <asp:Panel ID="PanelParent" runat="server">
      <div class="col-md-6">
      

                    <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt"
                       Text="Parent_Id Change"></asp:Label><span class="warning1" style="color: Red; font-style: normal">&nbsp;</span>
                                                           
                    <asp:TextBox ID="txtParent_Id" runat="server" Height="32px" class="form-control select2" Width="400px" value="-1"></asp:TextBox>

                       
            </div>
          <div class="col-md-6">
      
                    <asp:Label ID="lblLanguage" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt"
                       Text="Select Language "></asp:Label>

              <asp:RadioButtonList ID="rbLanguage" RepeatDirection="Horizontal" runat="server">
                  <asp:ListItem Value="0">&nbsp; Marathi &nbsp;&nbsp;</asp:ListItem>
                  <asp:ListItem Value="1">&nbsp; Hindi &nbsp;&nbsp;</asp:ListItem>
                  <asp:ListItem Value="2">&nbsp; English</asp:ListItem>
              </asp:RadioButtonList> 
            </div>
            </asp:Panel>
</div>

               

                 <div class="row">
    <div class="col-md-12">
        <div class="col-md-4">
            </div>
           <div class="col-md-6">
                <br />
                 <br />
            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-success" Text="Submit" ValidationGroup="other" OnClick="btnSubmit_Click" />
             <asp:Button ID="btnUpdate" runat="server" Visible="false" CssClass="btn btn-success" Text="Update" OnClick="btnUpdate_Click"/>                                                     &nbsp;
            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-success" Text="Cancel" ValidationGroup="other" OnClick="btnCancel_Click"/>
                                                             &nbsp;
           </div>
               <asp:Label ID="lblerrer" runat="server" ForeColor="#009900"></asp:Label>
                      
                              </div>
                     </div>
               
                <br />
                            <div class="table-responsive">
                <asp:GridView ID="GVAddHelp" CssClass="table table-hover table-bordered" runat="server"
                        OnPageIndexChanging="GVAddHelp_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
             <Columns>   


                 <asp:TemplateField HeaderText="Sr.No">
    <ItemTemplate>
        <%# Container.DataItemIndex + 1 %>
    </ItemTemplate>
</asp:TemplateField>     
                         <asp:BoundField DataField="ID" HeaderText="ID">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="GroupCode" HeaderText="GroupCode">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="title" HeaderText="Title">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       <%--  <asp:BoundField DataField="help_key" HeaderText="HelpKey">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>--%>
                         <asp:BoundField DataField="Description" HeaderText="Description">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>

                  <asp:BoundField DataField="Parent_Id" HeaderText="Parent_Id">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>

                        <%-- <asp:BoundField DataField="ImageUrl" HeaderText="Image Url">
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="YouTubeUrl" HeaderText="YouTube Url">	
                             <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                             <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>--%>
                   
                 
                 
                    <asp:TemplateField HeaderText="Update">
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnUpdate" CssClass="btn btn-round btn-success" runat="server" CommandArgument='<%#Bind("ID")%>'
                                                                    CommandName="Modify" OnClick="btnEdit_Click" Text="Edit"></asp:Button>
                                                             
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                <%--  <asp:TemplateField HeaderText="Delete">
                                                            <ItemTemplate>
                                                                <asp:Button ID="lnkbtndelete" CssClass="btn btn-round btn-warning" runat="server" CommandArgument='<%#Bind("ID")%>'
                                                                    CommandName="Modify" OnClick="lnkbtndelete_Click" Text="delete"></asp:Button>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>

                 
                 
   <%--              
                 <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate>
                                <a>
                                    <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-success" Text="Edit" OnClick="btnEdit_Click" />
                                    <i></i></a>
                                </a>
                                    <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                       </asp:TemplateField>--%>
                 </Columns>
                         <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                </asp:GridView>
                </div>
                 <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>

</div></div>

             </div>
                  </div>
                </div>
</asp:Content>
