﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class SpreadsheetReport : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if(!IsPostBack)
            {
                SpreadsheetForm();
               // GridView();
            }
        }
        public void SpreadsheetForm()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadSpreadSheetData");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlFormId.DataSource = ds.Tables[0];
                        ddlFormId.DataTextField = "Keyword";
                        ddlFormId.DataValueField = "Id";

                        ddlFormId.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlFormId.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlFormId.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlFormId.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReport1");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormName", ddlFormId.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVSpreadsheetReport.DataSource = ds.Tables[0];
                        GVSpreadsheetReport.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where CreatedBy = '"+ Session["username"].ToString() + "' and FormId = '" + ddlFormId.SelectedValue+"'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;
                    
                }

                catch (Exception ex)
                {

                }
            }

        }

        protected void GVSpreadsheetReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVSpreadsheetReport.Visible = true;
            GvSpreadsheet2.Visible = false;
            GVSpreadsheetReport.PageIndex = e.NewPageIndex;
            if (txtDistrict.Text != null && txtPRONumber.Text != null && txtPRONumber.Text != "" && txtDistrict.Text != "")
            {
                BothConditionGrid();
                // both condition // both text box have value
            }
            if (txtDistrict.Text != "")
            {
                MemberGrid();
            }
            if (txtPRONumber.Text != "")
            {
                ProNumberGrid();
            }

            if (txtPRONumber.Text == "" && txtDistrict.Text == "")
            {
                GridView();
                // both condition // both text box have value
            }
        }

        public void BothConditionGrid()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReportByMemberNoANDproNo");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Member", txtDistrict.Text.Trim());
                    cmd.Parameters.AddWithValue("@proNumber", txtPRONumber.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVSpreadsheetReport.DataSource = ds.Tables[0];
                        GVSpreadsheetReport.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where MemberNo = '" + txtDistrict.Text.Trim() + "' and Form = '"+txtPRONumber.Text+"'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;

                }

                catch (Exception ex)
                {

                }
            }

        }

        public void MemberGrid()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReportByMemberNo");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Member", txtDistrict.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVSpreadsheetReport.DataSource = ds.Tables[0];
                        GVSpreadsheetReport.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where MemberNo = '" + txtDistrict.Text.Trim() + "'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;

                }

                catch (Exception ex)
                {

                }
            }

        }

        public void ProNumberGrid()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReportByproNumber");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@proNumber", txtPRONumber.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVSpreadsheetReport.DataSource = ds.Tables[0];
                        GVSpreadsheetReport.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where From = '" + txtPRONumber.Text.Trim() + "'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;

                }

                catch (Exception ex)
                {

                }
            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GVSpreadsheetReport.Visible = true;
            GvSpreadsheet2.Visible = false;

            if (txtDistrict.Text != null && txtPRONumber.Text != null && txtPRONumber.Text != "" && txtDistrict.Text != "")
            {
                BothConditionGrid();
                txtPRONumber.Text = "";
                txtDistrict.Text = "";

                btnExportMemberAndPro.Visible = true;
                btnExportMember.Visible = false;
                btnExportPRO.Visible = false;

                ExportDisAndTaluka.Visible = false;
                btnexportDistict.Visible = false;
                btnExportTaluka.Visible = false;    
                // both condition // both text box have value
            }
            if(txtDistrict.Text != "")
            {
                MemberGrid();
                btnExportMemberAndPro.Visible = false;
                btnExportMember.Visible = true;
                btnExportPRO.Visible = false;

                ExportDisAndTaluka.Visible = false;
                btnexportDistict.Visible = false;
                btnExportTaluka.Visible = false;
            }
            if(txtPRONumber.Text != "")
            {
                ProNumberGrid();
                btnExportMemberAndPro.Visible = false;
                btnExportMember.Visible = false;
                btnExportPRO.Visible = true;

                ExportDisAndTaluka.Visible = false;
                btnexportDistict.Visible = false;
                btnExportTaluka.Visible = false;
            }
           
        }
       
        protected void btnSearchByDistrict_Click(object sender, EventArgs e)
        {
            GVSpreadsheetReport.Visible = false;
            GvSpreadsheet2.Visible = true;
            if (txtDis.Text != null && txtTaluka.Text != null && txtTaluka.Text != "" && txtDis.Text != "")
            {
                BothConditionGridTalAndDistrict();
                txtDis.Text = "";
                txtTaluka.Text = "";

                btnExportMemberAndPro.Visible = false;
                btnExportMember.Visible = false;
                btnExportPRO.Visible = false;

                ExportDisAndTaluka.Visible = true;
                btnexportDistict.Visible = false;
                btnExportTaluka.Visible = false;
                // both condition // both text box have value
            }
            if (txtDis.Text != "")
            {
                DistricrGrid();
                btnExportMemberAndPro.Visible = false;
                btnExportMember.Visible = false;
                btnExportPRO.Visible = false;

                ExportDisAndTaluka.Visible = false;
                btnexportDistict.Visible = true;
                btnExportTaluka.Visible = false;
            }
            if (txtTaluka.Text != "")
            {
                TalukaGrid();

                btnExportMemberAndPro.Visible = false;
                btnExportMember.Visible = false;
                btnExportPRO.Visible = false;

                ExportDisAndTaluka.Visible = false;
                btnexportDistict.Visible = false;
                btnExportTaluka.Visible = true;
            }
            if(txtDis.Text == "" && txtTaluka.Text =="")
            {
                GVSpreadsheetReport.Visible = true;
                GridView();
            }
        }

        public void BothConditionGridTalAndDistrict()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReportByDisAndTaluka");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Dis", txtDis.Text.Trim());
                    cmd.Parameters.AddWithValue("@Taluka", txtTaluka.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvSpreadsheet2.DataSource = ds.Tables[0];
                        GvSpreadsheet2.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where ANSWER_1 LIKE '%" + txtDis.Text.Trim() + "%' and ANSWER_2 LIKE '%" + txtTaluka.Text + "%'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;

                }
      

                catch (Exception ex)
                {

                }
            }

        }

        public void DistricrGrid()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReportByDis");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Dis", txtDis.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvSpreadsheet2.DataSource = ds.Tables[0];
                        GvSpreadsheet2.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where ANSWER_1 LIKE '%" + txtDis.Text.Trim() + "%'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;

                }

                catch (Exception ex)
                {

                }
            }

        }

        public void TalukaGrid()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetReportByTaluka");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Taluka", txtTaluka.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvSpreadsheet2.DataSource = ds.Tables[0];
                        GvSpreadsheet2.DataBind();
                    }
                    string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where ANSWER_2 LIKE '%" + txtTaluka.Text.Trim() + "%'";
                    string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    lblcntrecords.Text = spreadsheetcnt;

                }

                catch (Exception ex)
                {

                }
            }

        }

        protected void GvSpreadsheet2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            GVSpreadsheetReport.PageIndex = e.NewPageIndex;
            GVSpreadsheetReport.Visible = false;
            GvSpreadsheet2.Visible = true;
            if (txtDis.Text != null && txtTaluka.Text != null && txtTaluka.Text != "" && txtDis.Text != "")
            {
                BothConditionGridTalAndDistrict();
                txtDis.Text = "";
                txtTaluka.Text = "";
                // both condition // both text box have value
            }
            if (txtDis.Text != "")
            {
                DistricrGrid();
            }
            if (txtTaluka.Text != "")
            {
                TalukaGrid();
            }
            if (txtDis.Text == "" && txtTaluka.Text == "")
            {
                GVSpreadsheetReport.Visible = true;
                GridView();
                // both condition // both text box have value
            }
        }

        protected void btnExportMemberAndPro_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Member_And_PRO_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVSpreadsheetReport.AllowPaging = false;
            GVSpreadsheetReport.PageIndex = 0;

            BothConditionGrid();
            //Change the Header Row back to white color
            GVSpreadsheetReport.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVSpreadsheetReport.HeaderRow.Cells.Count; i++)
            {
                GVSpreadsheetReport.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVSpreadsheetReport.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void btnExportMember_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Member_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVSpreadsheetReport.AllowPaging = false;
            GVSpreadsheetReport.PageIndex = 0;

            MemberGrid();
            //Change the Header Row back to white color
            GVSpreadsheetReport.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVSpreadsheetReport.HeaderRow.Cells.Count; i++)
            {
                GVSpreadsheetReport.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVSpreadsheetReport.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void btnExportPRO_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "PRO_Number_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVSpreadsheetReport.AllowPaging = false;
            GVSpreadsheetReport.PageIndex = 0;

            ProNumberGrid();
            //Change the Header Row back to white color
            GVSpreadsheetReport.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVSpreadsheetReport.HeaderRow.Cells.Count; i++)
            {
                GVSpreadsheetReport.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVSpreadsheetReport.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //required to avoid the runtime error "  
            //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
        }
        protected void ExportDisAndTaluka_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Dis_And_Taluka_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GvSpreadsheet2.AllowPaging = false;
            GvSpreadsheet2.PageIndex = 0;

            BothConditionGridTalAndDistrict();
            //Change the Header Row back to white color
            GvSpreadsheet2.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GvSpreadsheet2.HeaderRow.Cells.Count; i++)
            {
                GvSpreadsheet2.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GvSpreadsheet2.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void btnexportDistict_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "District_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GvSpreadsheet2.AllowPaging = false;
            GvSpreadsheet2.PageIndex = 0;

            DistricrGrid();
            //Change the Header Row back to white color
            GvSpreadsheet2.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GvSpreadsheet2.HeaderRow.Cells.Count; i++)
            {
                GvSpreadsheet2.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GvSpreadsheet2.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void btnExportTaluka_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Taluka_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GvSpreadsheet2.AllowPaging = false;
            GvSpreadsheet2.PageIndex = 0;

            TalukaGrid();
            //Change the Header Row back to white color
            GvSpreadsheet2.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GvSpreadsheet2.HeaderRow.Cells.Count; i++)
            {
                GvSpreadsheet2.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GvSpreadsheet2.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void GVSpreadsheetReport_RowEditing(object sender, GridViewEditEventArgs e)
        {
            //NewEditIndex property used to determine the index of the row being edited.  
            GVSpreadsheetReport.EditIndex = e.NewEditIndex;
            GridView();
        }

        protected void GVSpreadsheetReport_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
            GVSpreadsheetReport.EditIndex = -1;
           
            GridView();

        }

        protected void GVSpreadsheetReport_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
           
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    long userid = Convert.ToInt64(GVSpreadsheetReport.DataKeys[e.RowIndex].Value.ToString());
                    GridViewRow row = (GridViewRow)GVSpreadsheetReport.Rows[e.RowIndex];
                    Label lblID = (Label)row.FindControl("lblId");

                    TextBox FormId = (TextBox)row.Cells[1].Controls[0];
                    TextBox Answer1 = (TextBox)row.Cells[2].Controls[0];
                    TextBox Answer2 = (TextBox)row.Cells[3].Controls[0];
                    TextBox Answer3 = (TextBox)row.Cells[4].Controls[0];
                    TextBox Answer4 = (TextBox)row.Cells[5].Controls[0];
                    TextBox Answer5 = (TextBox)row.Cells[6].Controls[0];
                    TextBox Answer6 = (TextBox)row.Cells[7].Controls[0];
                    TextBox Answer7 = (TextBox)row.Cells[8].Controls[0];
                    TextBox Answer8 = (TextBox)row.Cells[9].Controls[0];
                    TextBox Answer9 = (TextBox)row.Cells[10].Controls[0];
                    TextBox Answer10 = (TextBox)row.Cells[11].Controls[0];
                    TextBox Answer11 = (TextBox)row.Cells[12].Controls[0];
                    TextBox MemberNo = (TextBox)row.Cells[13].Controls[0];
                    TextBox AdminNo = (TextBox)row.Cells[14].Controls[0];
                    TextBox PropNumber = (TextBox)row.Cells[15].Controls[0];


                    //SqlCommand cmd = new SqlCommand("SP_UpdateInvTtem", DataBaseConnection);
                    SqlCommand cmd = new SqlCommand("SP_UpdateSpreadsheetReport", DataBaseConnection);
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Id", userid);
                    cmd.Parameters.AddWithValue("@FormId", FormId.Text);
                    cmd.Parameters.AddWithValue("@Answer1", Answer1.Text);
                    cmd.Parameters.AddWithValue("@Answer2", Answer2.Text);
                    cmd.Parameters.AddWithValue("@Answer3", Answer3.Text);
                    cmd.Parameters.AddWithValue("@Answer4", Answer4.Text);
                    cmd.Parameters.AddWithValue("@Answer5", Answer5.Text);
                    cmd.Parameters.AddWithValue("@Answer6", Answer6.Text);
                    cmd.Parameters.AddWithValue("@Answer7", Answer7.Text);
                    cmd.Parameters.AddWithValue("@Answer8", Answer8.Text);
                    cmd.Parameters.AddWithValue("@Answer9", Answer9.Text);
                    cmd.Parameters.AddWithValue("@Answer10", Answer10.Text);
                    cmd.Parameters.AddWithValue("@Answer11", Answer11.Text);
                    cmd.Parameters.AddWithValue("@MemberNo", MemberNo.Text);
                    cmd.Parameters.AddWithValue("@AdminNo", AdminNo.Text);
                    cmd.Parameters.AddWithValue("@PropNumber", PropNumber.Text);

                    int A = cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    GVSpreadsheetReport.EditIndex = -1;
                    GridView();

                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Record Updated Successfully ... ');", true);
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void btnSearchByFormId_Click(object sender, EventArgs e)
        {
            GridView();

        }
        string Que1 = string.Empty;
        public void GridViewHeading()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                   
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetQuestion");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormName", ddlFormId.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                   if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                        {
                            Que1 = Convert.ToString(ds.Tables[0].Rows[r]["Description"]);
                        }
                    //    GVSpreadsheetReport.DataSource = ds.Tables[0];
                    //    GVSpreadsheetReport.DataBind();
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' No record found ... ');", true);
                    }
                    //string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where CreatedBy = '" + Session["username"].ToString() + "' and FormId = '" + ddlFormId.SelectedValue + "'";
                    //string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                    //lblcntrecords.Text = spreadsheetcnt;

                }

                catch (Exception ex)
                {

                }
            }

        }


        protected void GVSpreadsheetReport_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewHeading();
                GVSpreadsheetReport.Columns[5].HeaderText = Que1;

            }
        }
    }
}