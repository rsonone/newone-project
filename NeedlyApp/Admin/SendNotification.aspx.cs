﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace NeedlyApp.Admin
{
    public partial class SendNotification : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                GridView();
                BindState();
            }
        }

        protected void GVAddQues_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var firstCell1 = e.Row.Cells[2];
                // ViewState["Customers"] = e.Row.Cells[2];
                firstCell1.Controls.Clear();
                firstCell1.Controls.Add(new HyperLink { NavigateUrl = firstCell1.Text, Text = firstCell1.Text });

            }
        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlstate.DataSource = ds.Tables[0];
                    ddlstate.DataTextField = "STATE_NAME";
                    ddlstate.DataValueField = "MDDS_STC";
                    ddlstate.DataBind();
                    ddlstate.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlstate.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddldistrict.DataSource = ds.Tables[0];
                    ddldistrict.DataTextField = "DISTRICT_NAME";
                    ddldistrict.DataValueField = "MDDS_DTC";

                    ddldistrict.DataBind();
                    ddldistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddldistrict.SelectedIndex = 0;
                }
                else
                {
                    ddldistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddldistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }
        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }
        protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView1();
        }
        protected void ddldistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        public void Notification()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    
                    SqlCommand cmd = new SqlCommand("SP_SendNotification");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@District",ddldistrict.SelectedValue);
                   cmd.Parameters.AddWithValue("@Taluka",ddlTaluka .SelectedValue);
                    cmd.Parameters.AddWithValue("@MobNo",txtMob.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                        {
                            //string MobileNo = Convert.ToString(ds.Tables[0].Rows[r]["MobileNo"]);
                            string MobileNo = Convert.ToString(9763180445);
                            string fcmid = Convert.ToString(ds.Tables[0].Rows[r]["FcmId"]);
                            string mobileno = Convert.ToString(ds.Tables[0].Rows[r]["MobileNo"]);
                            string chk = NeedlySendNotificationSimple(fcmid, MobileNo, txtMsg.Text, mobileno);

                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND UNSUCCESSFULLY...!');", true);
                }
            }

        }
        public void Notification2()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("SP_SendNotificationtosingleuser");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                   
                    cmd.Parameters.AddWithValue("@MobileNo", txtMob.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                        {
                            //string MobileNo = Convert.ToString(ds.Tables[0].Rows[r]["MobileNo"]);
                            string MobileNo = Convert.ToString(9763180445);
                            string fcmid = Convert.ToString(ds.Tables[0].Rows[r]["FcmId"]);
                            string mobileno = Convert.ToString(ds.Tables[0].Rows[r]["MobileNo"]);
                            string chk = NeedlySendNotificationSimple(fcmid, MobileNo, txtMsg.Text,mobileno);

                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND UNSUCCESSFULLY...!');", true);
                }
            }

        }

        public void Notification1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_SendNotificationtoall");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                   

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                        {
                            //string MobileNo = Convert.ToString(ds.Tables[0].Rows[r]["MobileNo"]);
                            string MobileNo = Convert.ToString(9763180445);
                            string fcmid = Convert.ToString(ds.Tables[0].Rows[r]["FcmId"]);
                            
                           
                           string chk = NeedlySendNotificationSimple1(fcmid, MobileNo, txtMsg.Text);


                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND UNSUCCESSFULLY...!');", true);
                }
            }

        }
        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (ddlstate.SelectedValue == "0" && ddldistrict.SelectedValue == "0" && ddlTaluka.SelectedValue == "0")
            {
                Notification2();
            }
            else
            {
                Notification();
            }

        }
        protected void btnSendall_Click(object sender, EventArgs e)
        {
            Notification1();
        }

        protected void btnsendtokeyword_Click(object sender, EventArgs e)
        {
            try
            {


                if (ddlkeyword.SelectedValue == "1")
                {
                    string chk = NeedlySendNotificationtoKeyword(txtMsg.Text);
                }
                if (ddlkeyword.SelectedValue == "2")
                {
                    string chk = NeedlySendNotificationtoGovernmentUser(txtMsg.Text, txtimg.Text);
                }
                if (ddlkeyword.SelectedValue == "3")
                {
                    string chk = NeedlySendNotificationtoLeaderUser(txtMsg.Text,txtimg.Text);
                }
                if (ddlkeyword.SelectedValue == "4")
                {
                    string chk = NeedlySendNotificationtoUSER(txtMsg.Text,txtimg.Text);
                }
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND SUCCESSFULLY...!');", true);
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' NOTIFICATION SEND UNSUCCESSFULLY...!');", true);
            }
        }
        public string NeedlySendNotification(string DeviRegId, string Mobileno, string Massage)
        {
            string str = string.Empty;
            try
            {
                string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "801244826424";

                string deviceId = DeviRegId;
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = deviceId,
                    data = new
                    {
                        testdata = new
                        {
                            body = Massage,
                            title = "Needly",
                            key_2 = Massage,
                        }
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }
        public string NeedlySendNotificationtoKeyword(string Massage)
        {
            string str = string.Empty;
            try
            {
                string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "801244826424";

               
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = "/topics/NeedlyUsers",
                    data = new
                    {
                        testdata3 = new
                        {
                            body = Massage,
                            title = "Needly",
                            key_1 = "Needly",
                            key_2 = Massage,
                            Key_3 = "",
                            DataCode = ""

                           
                        }
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }

        public string NeedlySendNotificationtoGovernmentUser(string Massage,string Image)
        {
            string str = string.Empty;
            try
            {
                string SERVER_API_KEY = "AAAAxoXVJKg:APA91bETgwJrBrPCWNDK72UV90Ejm6q30Dq2hFRq-owFxvKFFa0_80jhzSKkTY_GL7475B08UuYRS8TjsQ00WMVGklG6T3PZAdVwhGTmNEEikTQsRDqIxi5hypeSJ7T4Jx1EB5SXrPm9UiUv8Mk8QS4FLe2A4T6ozQ";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "852648862888";


                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = "/topics/GovrenmentUser",
                    data = new
                    {
                        notification = new
                        {
                            body = Massage,
                            title = "TrueVoter",
                            key_1 = "TrueVoter",
                            imageUrl = Image,
                            // DataCode = ""


                        }
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }

        public string NeedlySendNotificationtoLeaderUser(string Massage,string Image)
        {
            string str = string.Empty;
            try
            {
                string SERVER_API_KEY = "AAAAxoXVJKg:APA91bETgwJrBrPCWNDK72UV90Ejm6q30Dq2hFRq-owFxvKFFa0_80jhzSKkTY_GL7475B08UuYRS8TjsQ00WMVGklG6T3PZAdVwhGTmNEEikTQsRDqIxi5hypeSJ7T4Jx1EB5SXrPm9UiUv8Mk8QS4FLe2A4T6ozQ";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "852648862888";


                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = "/topics/LeaderUser",
                    data = new
                    {
                        notification = new
                        {
                            body = Massage,
                            title = "TrueVoter",
                            key_1 = "TrueVoter",
                             imageUrl=Image,
                            // DataCode = ""


                        }
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }

        public string NeedlySendNotificationtoUSER(string Massage,string Image)
        {
            string str = string.Empty;
            try
            {
                string SERVER_API_KEY = "AAAAxoXVJKg:APA91bETgwJrBrPCWNDK72UV90Ejm6q30Dq2hFRq-owFxvKFFa0_80jhzSKkTY_GL7475B08UuYRS8TjsQ00WMVGklG6T3PZAdVwhGTmNEEikTQsRDqIxi5hypeSJ7T4Jx1EB5SXrPm9UiUv8Mk8QS4FLe2A4T6ozQ";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "852648862888";


                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = "/topics/USER",
                    data = new
                    {
                        notification = new
                        {
                            body = Massage,
                            title = "TrueVoter",
                            key_1 = "TrueVoter",
                            imageUrl = Image,
                            // DataCode = ""

                        }
                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }
        public string NeedlySendNotificationSimple(string DeviRegId, string Mobileno, string Massage,string mobileno)
        {
            string str = string.Empty;
            try
            {
                string str12 = Massage;
                string RegMobileNO = str.Substring(str.IndexOf("(") + 1);
                string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "801244826424";

                string deviceId = DeviRegId;
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = deviceId,
                    data = new
                    {

                        testdata3 = new
                        {
                            body = Massage,
                            title = "Needly",
                            key_1 = "Needly",
                            key_2 = Massage,
                            Key_3 = RegMobileNO,
                            DataCode = Mobileno
                        }


                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                string sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                                dynamic json1 = JsonConvert.DeserializeObject(str);
                                string  value=json1["success"];
                                if(value=="1")
                                {
                                    string Query = "Update [ezeeFormsFcmReg] set FCMStatus='Active User' where MobileNo=" + mobileno + "";
                                    int ds = cc.ExecuteNonQueryNeedly(Query);
                                }
                                else
                                {
                                    string Query = "Update [ezeeFormsFcmReg] set FCMStatus='Deactive User' where MobileNo=" + mobileno + "";
                                    int ds = cc.ExecuteNonQueryNeedly(Query);
                                }



                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }

        public string NeedlySendNotificationSimple11(string DeviRegId, string Mobileno, string Massage)
        {
            string str = string.Empty;
            try
            {
                string str12 = Massage;
                string RegMobileNO = str.Substring(str.IndexOf("(") + 1);
                string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "801244826424";

                string deviceId = DeviRegId;
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = deviceId,
                    data = new
                    {

                        testdata3 = new
                        {
                            body = Massage,
                            title = "Needly",
                            key_1 = "Needly",
                            key_2 = Massage,
                            Key_3 = RegMobileNO,
                            DataCode = Mobileno
                        }


                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                string sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                                



                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }
        public string NeedlySendNotificationSimple1(string DeviRegId, string Mobileno, string Massage)
        {
            string str = string.Empty;
            try
            {
                string str12 = Massage;
                string RegMobileNO = str.Substring(str.IndexOf("(") + 1);
                string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                string SENDER_ID = "801244826424";

                string deviceId = DeviRegId;
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data1 = new
                {
                    to = deviceId,
                    data = new
                    {

                        testdata3 = new
                        {
                            body = Massage,
                            title = "Needly",
                            key_1 = "Needly",
                            key_2 = "",
                            Key_3 = "",
                            DataCode = "",

                        }


                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data1);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                tRequest.ContentLength = byteArray.Length;
                #region add
                tRequest.UseDefaultCredentials = true;
                tRequest.PreAuthenticate = true;
                tRequest.Credentials = CredentialCache.DefaultCredentials;
                #endregion

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
                str = "SUCCESS:" + str;
            }
            catch (Exception ex)
            {
                str = "ERROR:" + ex.Message;
            }
            return str;
        }

        protected void GVAddQues_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddQues.PageIndex = e.NewPageIndex;
            GridView();
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_DownloadFCMStatus");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                       
                        GVAddQues.DataSource = ds;
                        GVAddQues.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadFCMStatususerwise");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserStatus", ddlstatus.SelectedItem.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddQues.DataSource = ds;
                        GVAddQues.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void GridView2()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadFCMStatusMobileNowise");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MobileNo", txtMob.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddQues.DataSource = ds;
                        GVAddQues.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            GridView2();
        }

    }
}