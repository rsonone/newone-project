﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class BannerDetails : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        string MobileNo = string.Empty;
        string dirPath = string.Empty;
        string fullName = string.Empty;
        string base64String = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindState();
            }
        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlState.DataSource = ds.Tables[0];
                    ddlState.DataTextField = "STATE_NAME";
                    ddlState.DataValueField = "MDDS_STC";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlState.SelectedIndex = 0;
                }
            }
            catch(Exception ex)
            {
               
            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);                
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddlDistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
               
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }
        public void Clear()
        {
            txtBannerName.Text = "";
            txtpincode.Text = "";
            txtUrl.Text = "";
            txtWardNo.Text = "";
            ddlState.SelectedValue = "0";
            ddlDistrict.SelectedValue = "0";
            ddlTaluka.SelectedValue = "0";
            ddlType.SelectedValue = "0";
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }
        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UploadBannerDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    if ((FileUpload1.HasFile))
                    {
                        foreach (var file in FileUpload1.PostedFiles)
                        {
                            BinaryReader br = new BinaryReader(file.InputStream);
                            byte[] bytes = br.ReadBytes((int)file.InputStream.Length);
                            base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                        }

                        if (ddlType.SelectedValue == "1")
                        {
                            dirPath = "E:\\Needly_Image\\Banner_Image\\" + Session["username"].ToString() + "\\";
                            fullName = "PanCard_" + DateTime.Now.ToString("dd_m_yyy_HH_mm _tt") + ".jpeg";
                        }
                        if (ddlType.SelectedValue == "2")
                        {
                            dirPath = "E:\\Needly_Image\\Banner_Image\\" + Session["username"].ToString() + "\\";
                            fullName = "SelfPhoto_" + DateTime.Now.ToString("dd_m_yyy_HH_mm _tt") + ".jpeg";
                        }
                        if (ddlType.SelectedValue == "3")
                        {
                            dirPath = "E:\\Needly_Image\\Banner_Image\\" + Session["username"].ToString() + "\\";
                            fullName = "StoreActImg_" + DateTime.Now.ToString("dd_m_yyy_HH_mm _tt") + ".jpeg";
                        }
                      
                        byte[] imgByteArray = Convert.FromBase64String(base64String);

                        if (!Directory.Exists(dirPath))
                        {
                            Directory.CreateDirectory(dirPath);
                            File.WriteAllBytes(dirPath + fullName, imgByteArray);
                        }
                        else
                        {
                            File.WriteAllBytes(dirPath + fullName, imgByteArray);
                        }
                        dirPath = dirPath + fullName;
                    }

                    cmd.Parameters.AddWithValue("@Banner_Name", txtBannerName.Text);
                    cmd.Parameters.AddWithValue("@Banner_Type", ddlType.SelectedValue);
                    cmd.Parameters.AddWithValue("@ImageURL", dirPath);
                    cmd.Parameters.AddWithValue("@StateId", ddlState.SelectedValue);
                    cmd.Parameters.AddWithValue("@DistrictId", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@TalukaId", ddlTaluka.SelectedValue);
                    cmd.Parameters.AddWithValue("@PinCode", txtpincode.Text);
                    cmd.Parameters.AddWithValue("@WardNo", txtWardNo.Text);
                    cmd.Parameters.AddWithValue("@RedirectURL", txtUrl.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    Clear();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA NOT SUBMITTED SUCCESSFULLY.!!!')", true);
                }
            }               
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Clear();
        }
    }
}