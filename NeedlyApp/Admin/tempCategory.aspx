﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master"  EnableEventValidation="false"  AutoEventWireup="true" CodeBehind="tempCategory.aspx.cs" Inherits="NeedlyApp.Admin.tempCategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add Template Category <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                   <a href="AddTemplate.aspx" data-toggle="tooltip" title="Back" class="btn btn-primary">
                    Back</a><a href="tempCategory.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
               
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
                 
                  
                       
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Day Special: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtCategoryName" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div></div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                    Day : </label>
                              <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlDay" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                       <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                       <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                       <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                       <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                       <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                       <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                       <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                       <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                       <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                       <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                       <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                       <asp:ListItem Value="13" Text="13"></asp:ListItem>
                                       <asp:ListItem Value="14" Text="14"></asp:ListItem>
                                       <asp:ListItem Value="15" Text="15"></asp:ListItem>
                                       <asp:ListItem Value="16" Text="16"></asp:ListItem>
                                       <asp:ListItem Value="17" Text="17"></asp:ListItem>
                                       <asp:ListItem Value="18" Text="18"></asp:ListItem>
                                       <asp:ListItem Value="19" Text="19"></asp:ListItem>
                                       <asp:ListItem Value="20" Text="20"></asp:ListItem>
                                       <asp:ListItem Value="21" Text="21"></asp:ListItem>
                                       <asp:ListItem Value="22" Text="22"></asp:ListItem>
                                       <asp:ListItem Value="23" Text="23"></asp:ListItem>
                                       <asp:ListItem Value="24" Text="24"></asp:ListItem>
                                       <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                       <asp:ListItem Value="26" Text="26"></asp:ListItem>
                                       <asp:ListItem Value="27" Text="27"></asp:ListItem>
                                       <asp:ListItem Value="28" Text="28"></asp:ListItem>
                                       <asp:ListItem Value="29" Text="29"></asp:ListItem>
                                       <asp:ListItem Value="30" Text="30"></asp:ListItem>
                                       <asp:ListItem Value="31" Text="31"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div>
                             </div></div>

                    </div>
               </div></div> 

                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Month: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlMonth" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       <asp:ListItem Value="1" Text="January"></asp:ListItem>
                                       <asp:ListItem Value="2" Text="February"></asp:ListItem>
                                       <asp:ListItem Value="3" Text="March"></asp:ListItem>
                                       <asp:ListItem Value="4" Text="April"></asp:ListItem>
                                       <asp:ListItem Value="5" Text="May"></asp:ListItem>
                                       <asp:ListItem Value="6" Text="June"></asp:ListItem>
                                       <asp:ListItem Value="7" Text="July"></asp:ListItem>
                                       <asp:ListItem Value="8" Text="August"></asp:ListItem>
                                       <asp:ListItem Value="9" Text="September"></asp:ListItem>
                                       <asp:ListItem Value="10" Text="October"></asp:ListItem>
                                       <asp:ListItem Value="11" Text="November"></asp:ListItem>
                                       <asp:ListItem Value="12" Text="December"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Dependancy: </label>
                             <div class="col-sm-9">
                                  <asp:RadioButtonList ID="rbDependancy" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1" Selected="True">&nbsp;&nbsp;Date Dependancy&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Tithi Dependancy&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="3">&nbsp;&nbsp;None&nbsp;&nbsp;</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          </div>
                             </div></div>

                    </div>
               </div></div> 
                   
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Description: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtDescription" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Language: </label>
                             <div class="col-sm-9">
                                  <asp:RadioButtonList ID="rbLanguage" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                      <asp:ListItem Value="4" Selected="True"> &nbsp;&nbsp;All&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Marathi&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Hindi&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="3">&nbsp;&nbsp;English&nbsp;&nbsp;</asp:ListItem>
                                    
                                    
                                </asp:RadioButtonList>
                          </div>
                             </div></div>

                    </div>
               </div></div> 
                     

         <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">

              
                <div class="box-body col-sm-6">
                          <div class="form-group">
                              <label class="col-sm-2 control-label" for="input-name2">
                                 Type: </label>
                             <div class="col-sm-10">
                                  <asp:RadioButtonList ID="rbTemplateType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="4" Selected="True">&nbsp;&nbsp;Daily &nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Visiting Card&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="3">&nbsp;&nbsp;Certificate&nbsp;&nbsp;</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          </div>
                             </div>

                </div>

                <div class="box-body col-sm-6">
                          <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Sub-Category: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlParentId" DataTextField="DaySpecial" DataValueField="Id" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="-1" Text="--Select--"></asp:ListItem>
                                      
                                         
                                    </asp:DropDownList><br />
                          </div></div>
                </div>

                 

                    </div>
               </div>
         </div>
                              <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                          <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Image: </label>
                                <div class="col-sm-9">
                                    <asp:FileUpload ID="FileUpload2" runat="server" />
                                   
                                    <asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true" Text=""></asp:Label>
                          </div></div>
                       
                     </div>
                     <div class="box-body col-sm-6">
                          <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Display Sequence: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtDisplaySequence" runat ="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                         ControlToValidate="txtDisplaySequence" ValidationExpression="^\d+$"
                                        ErrorMessage="Value must be numeric!" ForeColor="Red"></asp:RegularExpressionValidator>
                          </div></div>
                      </div>

               

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                
                         </div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                             <div class="box-body col-sm-4">
                              <asp:Button ID="btnTempCategory" runat="server" Text="Add" class="btn btn-primary" OnClick="btnTempCategory_Click" />
                              <div class="box-body col-sm-4">
                              <asp:Button ID="btnUpdate"  runat="server" OnClick="btnUpdate_Click" Text="Update" class="btn btn-success"  />
                             
                        </div> 
                        </div> 
                            
                             </div></div>

                    </div>
               </div></div> 
                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                     </label>
                                <div class="col-sm-9">
                                
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                             
                             <div class="col-sm-6">
                               <asp:TextBox ID="txtSearchCategory" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                              <div class="col-sm-4">
                               <asp:Button ID="btnSearchCategory"  runat="server" Text="Search" class="btn btn-success"  />
                          </div>
                             </div></div>

                    </div>
               </div></div>
                    <div class="col-md-12">
                         <br />
                         <br />
                           <div class="table-responsive">
                    <asp:GridView ID="GVUserLogin" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVUserLogin_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="DaySpecial" HeaderText="Day Special">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Day" HeaderText="Day">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Month" HeaderText="Month">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Dependancy" HeaderText="Dependancy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Description" HeaderText="Description">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Language" HeaderText="Language">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="TemplateType" HeaderText="Template Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="DisplaySequence" HeaderText="Sequence">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Action">
                              <ItemTemplate>
                                  <a> <asp:Button ID="Edit" runat="server" OnClick="Edit_Click" CssClass="btn-info" Text="Edit"   />
                                  <a> <asp:Button ID="Delete" runat="server" OnClick="Delete_Click" CssClass="btn-danger" Text="Delete" />
                                      <i></i></a></a>
                                     <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>
</asp:Content>
