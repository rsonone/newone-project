﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="RechargeWalletReport.aspx.cs" Inherits="NeedlyApp.Admin.RechargeWalletReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>

     <div class="row">
           <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Recharge Wallet Report page <small></small></h2>
           <div class="clearfix"></div>
                       <div class="container-fluid">
                        
                            
                          <div class="col-md-12">
                   
                    </div>                   
                     </div>          
                  </div>

     </div>


      <div class="col-md-12">
                            <div class="box-body col-sm-6">
                               
                                  <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                                Select Option
                                            </label>
                                            <div class="col-sm-8">
                                                <asp:RadioButtonList ID="rdbSelectContact"  runat="server" OnSelectedIndexChanged="rdbSelectContact_SelectedIndexChanged"  RepeatDirection="Vertical" AutoPostBack="true">
                                                    <asp:ListItem Value="1">&nbsp;&nbsp;Recharge Attempt Report&nbsp;&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="2">&nbsp;&nbsp;Recharge Complete Status Report&nbsp;&nbsp;</asp:ListItem>
                                                   
                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                    </div>
              
                        </div>

  

      <asp:Panel ID="PnlOldData" runat="server">
           <div class="col-md-12">
                                                Fields marked with * are mandatory.
                    </div>
          <br />
          <br />
            <div class="col-md-12">
                       
             <div class="box-body col-sm-6">
              <asp:Label ID="Label11" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="11pt" Text="Enter Mobile No"></asp:Label>
                <asp:TextBox ID="txtMob" runat="server" Height="32px" class="form-control select2" Width="400px"></asp:TextBox>
         
             </div>
                              <div class="box-body col-sm-6">
                         <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Names="Arial" Font-Size="11pt" Text="Enter Amount"></asp:Label>
                                                    <asp:TextBox ID="txtAmt" runat="server" Height="32px" class="form-control select2" Width="400px"></asp:TextBox>


                              </div>
                          </div>
           <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="Select PurchaseType"></asp:Label>
                                                    <asp:DropDownList ID="ddlPType" runat="server" CssClass="form-control select2" Width="400px">
                                                        <asp:ListItem Value="">--Select--</asp:ListItem>
                                                        <asp:ListItem Value="1">Course Purchase</asp:ListItem>
                                                        <asp:ListItem Value="2">Open Schooling</asp:ListItem>
                                                        <asp:ListItem Value="3">Virtual School</asp:ListItem>
                                                        <asp:ListItem Value="4">Advance training</asp:ListItem>
                                                        <asp:ListItem Value="5">Student Security</asp:ListItem>
                                                        <asp:ListItem Value="8">WhatsApp Premium</asp:ListItem>
                                                        <asp:ListItem Value="9">WhatsApp Pro</asp:ListItem>
                                                        <asp:ListItem Value="10">Proplus</asp:ListItem>
                                                        <asp:ListItem Value="11">Platinum</asp:ListItem>
                                                        <asp:ListItem Value="12">Child Safety</asp:ListItem>
														<asp:ListItem Value="16">Platinum Add On</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="Select Status"></asp:Label>
                                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2" Width="400px">
                                                       <%-- <asp:ListItem Value="">-- Select --</asp:ListItem>--%>
                                                        <asp:ListItem Value="1">success</asp:ListItem>
                                                        <asp:ListItem Value="2">failed</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                    <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="Referral Code"></asp:Label>
                                                    <asp:TextBox ID="txtReferralCode" runat="server" Height="32px" class="form-control select2" Width="400px"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="Coupon Code"></asp:Label>
                                                    <asp:TextBox ID="txtCouponCode" runat="server" Height="32px" class="form-control select2" Width="400px"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                            <br />
                                        <div class="row">
                                            <div class="col-md-12">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-success" Height="39px" Width="137px" Text="Search..!" OnClick="btnSearch_Click" />
                                                <asp:Button Text="Export To Excel" ID="btnExport" runat="server" CssClass="btn btn-success" Height="39px" Width="137px" OnClick="btnExport_Click" />
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row" style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                                            <div class="col-md-12">
                                                <asp:Label ID="lblTotalCount" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                                                <asp:Label ID="lblNodata" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                                                &nbsp;&nbsp;&nbsp;&nbsp
                        <asp:Label ID="lblCuntDisplay" runat="server" Text="" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>

                                            </div>
                                        </div>
                                        <br />
                                             <div class="row" style="height: 400px; overflow: auto; overflow-x:auto;width:1100px">
                                                <asp:GridView ID="GVRechWall" style="height: 400px; overflow:auto; overflow-x:auto;width:800px" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">

                                <Columns>

                                                        <asp:BoundField DataField="Id" HeaderText="ID">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="OrderID" HeaderText="OrderID">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="UserMobile" HeaderText="UserMobile">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FullName" HeaderText="FullName">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TransactionDate" HeaderText="TransactionDate">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="RechargeAmount" HeaderText="RechargeAmount">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TotalAmount" HeaderText="TotalAmount">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="UsedAmount" HeaderText="UsedAmount">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PaymentStatus" HeaderText="PaymentStatus">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Payment_Mode" HeaderText="Payment_Mode">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="PurchaseType" HeaderText="PurchaseType">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ReferralCode" HeaderText="Referral Code">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CouponCode" HeaderText="Coupon Code">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <%--<asp:TemplateField HeaderText="Cashfree">
                                     <ItemTemplate>
                                          <asp:Button ID="lnkCash" CssClass="btn btn-round btn-warning" runat="server" Text="CashFree" OnClick="lnkCash_Click" ></asp:Button>
                                          <asp:Label ID="lblView" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                                     </ItemTemplate>
                                </asp:TemplateField> --%>
                                                    </Columns>
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="White" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />

                            </asp:GridView>
                                      
</div>
                                        <asp:Label ID="Label6" runat="server" Visible="false"></asp:Label>
          </asp:Panel>
     

    
        <asp:Panel ID="PnlAttemptedData" runat="server">
                                             <div class="row">
                                            
                                        </div>
                                        <br />

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-6">
                                                   
                                                    <asp:Label ID="Label7" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="Enter Mobile No"></asp:Label>
                                                    <div class="input-group">
                                                         <asp:TextBox ID="txtAttemptedMobileNumber" runat="server" Height="32px" class="form-control select2" Width="300px"></asp:TextBox>
                                                    &nbsp  &nbsp<asp:Button ID="btnSearchAttemptedData" runat="server" CssClass="btn" Height="35px" Width="135px" Text="Search..!" OnClick="btnSearchAttemptedData_Click" />
                                               
                                                    </div>
                                                   
                                                </div>
                                                <div class="col-md-6">
                                                     <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="From Date"></asp:Label>
                                                  &nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp  <asp:Label ID="Label9" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="11pt" Text="To Date" style="margin-left:28px"></asp:Label>
                                                    <div class="input-group">
                                                         <asp:TextBox ID="txtFromDate" TextMode="Date" runat="server" Height="32px" class="form-control select2" Width="150px"></asp:TextBox>
                                                      
                                                          &nbsp&nbsp&nbsp&nbsp   <asp:TextBox ID="txtToDate" TextMode="Date" runat="server" Height="32px" class="form-control select2" Width="150px" style="margin-left:4px"></asp:TextBox>
                                                    &nbsp  &nbsp<asp:Button ID="btnSearchByDate" runat="server" CssClass="btn" Height="35px" Width="137px" Text="Search..!" OnClick="btnSearchByDate_Click"  />
                                               
                                                    </div>
                                            </div>
                                                </div>
                                        </div>

                                       
                                        <br />
                                      
                                            <br />
                                      <%--  <div class="row">
                                            <div class="col-md-12">
                                               <asp:Button Text="Export To Excel" ID="Button2" runat="server" CssClass="btn" Height="39px" Width="137px" />
                                            </div>
                                        </div>--%>
                                        <br />
                                        <%--<div class="row" style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label13" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                                                <asp:Label ID="Label14" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                                                &nbsp;&nbsp;&nbsp;&nbsp
                        <asp:Label ID="Label15" runat="server" Text="" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>

                                            </div>
                                        </div>--%>
                                        <br />
                                  <div class="row" style="height: 400px; overflow: auto; overflow-x:auto;width:1100px">
                                                      
                            <asp:GridView ID="GvAttemptedData" style="height: 400px; overflow: auto" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="Horizontal" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px">

                                <Columns>

                                                        <asp:BoundField DataField="Id" HeaderText="ID">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="OrderId" HeaderText="OrderID">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="OrderAmount" HeaderText="Ammount">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                     <asp:BoundField DataField="MobileNo" HeaderText=" Mobile No. ">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="DateTime" HeaderText="Date">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ReferralCode" HeaderText="Referral Code.">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CouponCode" HeaderText=" CouponCode ">
                                                            <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                                            <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                                        </asp:BoundField>
                                                       
                                                      
                                                       
                                                        <%--<asp:TemplateField HeaderText="Cashfree">
                                     <ItemTemplate>
                                          <asp:Button ID="lnkCash" CssClass="btn btn-round btn-warning" runat="server" Text="CashFree" OnClick="lnkCash_Click" ></asp:Button>
                                          <asp:Label ID="lblView" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                                     </ItemTemplate>
                                </asp:TemplateField> --%>
                                                    </Columns>
                                <FooterStyle BackColor="White" ForeColor="#333333" />
                                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="White" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#487575" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#275353" />

                            </asp:GridView>
                     
                                      </div>
                                       
                                     
                                        </asp:Panel>

</asp:Content>
