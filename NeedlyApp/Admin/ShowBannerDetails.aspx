﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="ShowBannerDetails.aspx.cs" Inherits="NeedlyApp.Admin.ShowBannerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  View Banner Details <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                    <a href="BannerDetails.aspx" data-toggle="tooltip" title="Add New" class="btn btn-primary">
                    <i class="fa fa-plus"></i></a><a href="ShowBannerDetails.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
               
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
          <div class="row">
            <div class="panel-body">
                 
                 <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlState" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddlDistrict" CssClass="form-control"  AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div>

                 <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                   Taluka: </label>
                               <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTaluka" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                        
                                   </asp:DropDownList><br />
                                   </div></div></div>
        </div> </div></div> 
                  <div class="col-md-12">
           <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-success" OnClick="btnSearch_Click" />
                             
                         
           </div>
 <div class="box-body col-sm-4">
     <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
           </div>



        </div>

                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVBanner"  runat="server" OnPageIndexChanging="GVBanner_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:BoundField DataField="Id" HeaderText="ID" Visible="false">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Banner_Name" HeaderText="Banner_Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Banner_Type" HeaderText="Banner_Type">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="StateId" HeaderText="State">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="DistrictId" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="TalukaId" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="PinCode" HeaderText="PinCode">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="WardNo" HeaderText="WardNo">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="CreatedBy" HeaderText="Mobile No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                </div>
</div></div></div></div>
</asp:Content>

