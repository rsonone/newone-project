﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AddMember.aspx.cs" Inherits="NeedlyApp.Admin.AddMember" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:Panel runat="server" ID="addreferal" Visible="true">
      <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Add Needly Family Member  <small></small></h2>
                       <div class="container-fluid">
            <div class="pull-right">
                  <a href="ViewMultipleEntriesForms.aspx" data-toggle="tooltip" title="View report" class="btn btn-success" > List
                   </a>
                
               <%--  <asp:Button ID="lnkbtnView" CssClass="btn btn-round btn-success" runat="server" OnClick="lnkbtnView_Click"  Text="List"></asp:Button>
                       <button type="button"  onclick=""  >List</button>   --%>                                        
               
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
         
          <div class="row">
            <div class="panel-body"> 

                 <div class="row">
    <div class="col-md-12">
                     
        </div></div><br />
                 <div class="row">
    <div class="col-md-12">
        <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">Select Group Code</label>
                               <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlgroupcode" CssClass="form-control"   runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                                   </div>
                          </div></div>

        
         <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">Select Level</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddllevel" CssClass="form-control"  runat="server" AutoPostBack="true">
                                            <asp:ListItem Value="1" Text="Public Level"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="National Lavel"></asp:ListItem>
                                            <asp:ListItem Value="3" Text="State Level"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="Region Level"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="District Level"></asp:ListItem>
                                            <asp:ListItem Value="6" Text="Block Level"></asp:ListItem>
                                            <asp:ListItem Value="7" Text="Village/Area Level"></asp:ListItem>
                                        </asp:DropDownList>


                                    </div>
                                </div>
                            </div>

    </div>  


                        <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlState" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged">
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddlDistrict"  CssClass="form-control" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddldistrict_SelectedIndexChanged">
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
                   

                     


                 </div>
                   

              
                
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Taluka: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTaluka" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList><br />
                          </div></div></div>


                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
<%--                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Ward_No: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtWard" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>--%>

                    </div>
               </div></div> 

                  <div class="col-md-12">
                     
                     <div class="box-body col-sm-offset-2  col-sm-4">
                           <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." Font-Bold="true" Font-Size="Medium" OnClick="lnkbtnDownload_Click"   ForeColor="Red"></asp:LinkButton>
                          </div>
                    </div>

               <div class="col-md-12">
                     <div class="box-body col-sm-5">
                        
                    </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" class="btn btn-success"  />
                            <%-- <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false"  />--%>
                        </div>
                 </div>

                
               
               
                              <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVAddMember" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVAddreferal_PageIndexChanging"  
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                         --%>
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="Group_Id" HeaderText=" Group_Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="MemberLevel" HeaderText="MemberLevel">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        
                         <asp:BoundField DataField="FirstName" HeaderText="FirstName">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>

                          <asp:BoundField DataField="LastName" HeaderText="LastName">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="MobileNo" HeaderText="MobileNo">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Designation" HeaderText="Designation">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="State" HeaderText="State">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                          <asp:BoundField DataField="blocklevel" HeaderText="blocklevel">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        
                        
                      
                       
                         <asp:TemplateField HeaderText="Action"  >
                             <ItemTemplate>
                                 <asp:Button ID="btnDelete" runat="server" CssClass=" btn-danger" Text="Delete" />
                                   
                             </ItemTemplate>
                         </asp:TemplateField>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                 <asp:Label ID="lblID" runat="server" Visible="false"></asp:Label>

</div></div>

             </div>
                  </div>
                </div>
         </asp:Panel>
</asp:Content>
