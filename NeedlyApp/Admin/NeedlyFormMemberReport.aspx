﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="NeedlyFormMemberReport.aspx.cs" Inherits="NeedlyApp.Admin.NeedlyFormMemberReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Needly Form Member Report <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                     <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                               <label class="col-sm-3 control-label" for="input-name2">
                                                   Enter Number: </label>
                                               <div class="col-sm-9">
                                                   <asp:DropDownList ID="ddlFormname" CssClass=" form-control" runat="server">

                                                   </asp:DropDownList>
                                         </div></div>
                                    </div>
                                     <div class="box-body col-sm-4">
                                           <asp:Button ID="btnSearchFormname" runat="server" Text="Search Form" class="btn btn-success" OnClick="btnSearchFormname_Click"  />
                                         </div>
                            </div>
                        </div></div> 
                     <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                               <label class="col-sm-3 control-label" for="input-name2">
                                                   Enter Number: </label>
                                               <div class="col-sm-9">
                                                 <asp:TextBox ID="txtMobileNo" runat ="server" CssClass="form-control"></asp:TextBox>
                                         </div></div>
                                    </div>
                                     <div class="box-body col-sm-4">
                                           <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-success" OnClick="btnSearch_Click"  />
                                         </div>
                            </div>
                        </div></div> 
                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                   Total Records </label>
                                <div class="col-sm-4">
                                    <asp:Label ID="lblFormMembercnt" ForeColor="Green" Font-Bold="true" Font-Size="Large" runat="server" Text=""></asp:Label><br />
                          </div></div></div>
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                               
                                <div class="col-sm-6">
                                    <%--<asp:Button ID="btnExportData"  runat="server" CssClass="btn btn-success" Text="Export Data" />--%>
                          </div></div></div>
                    </div>
               </div></div> 
       <br />
                    <br />
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVFormMemberReport"  runat="server"  OnPageIndexChanging="GVFormMemberReport_PageIndexChanging"    PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                     <%--   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     --%>
                 
                         <asp:BoundField DataField="Id" HeaderText="Id" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Form_Name" HeaderText="Form Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Form_Id" HeaderText="Form Id">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Member_No" HeaderText="Member No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Member_Name" HeaderText="Member Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:BoundField DataField="Job_Role" HeaderText="Job Role">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:BoundField DataField="Status" HeaderText="Status">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:BoundField DataField="Update_Field" HeaderText="Update Field">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:BoundField DataField="CreatedBy" HeaderText="Created By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:BoundField DataField="CreatedDate" HeaderText="Created Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ModifiedDate" HeaderText="Modified Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ModifiedDate" HeaderText="Modified Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Description" HeaderText="Description">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:BoundField DataField="EmailId" HeaderText="Email Id">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                       
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                         </div>
                    </div>
                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>
