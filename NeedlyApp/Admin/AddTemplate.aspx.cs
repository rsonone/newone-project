﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddTemplate : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        //CommonCode cc = new CommonCode();
        string ShopType = string.Empty;
        string DataCode = "0";
        string Image2 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                ViewState["DataCode"] = "0";
                Panel1.Visible = false;
                Panel2.Visible = false;
                GridView();


            }
        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowTemplate");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddTemplate.DataSource = ds.Tables[0];
                        GVAddTemplate.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void Shop()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadShopType");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlShop.DataSource = ds.Tables[0];
                        ddlShop.DataTextField = "ItemName";
                        ddlShop.DataValueField = "Id";

                        ddlShop.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlShop.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlShop.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlShop.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string ImageNew = string.Empty;
                string imageUrl1 = string.Empty;




            if (fuimage.HasFile)
            {
                //   Image2 = "https://needly.in/TemplateImage/" + str.ToString(); TemplateImage

                string filename = Path.GetFileName(fuimage.PostedFile.FileName);
                //FileUpload2.SaveAs(Server.MapPath("C:\\Inetpub\\vhosts\\needly.in\\httpdocs\\TemplateImage\\" + filename));
                fuimage.SaveAs(Server.MapPath("~/TemplateImage/" + filename));
                string filepath = "TemplateImage/" + filename;

                //dirPath = dirPath + fullName;
                ImageNew = "https://needly.in/" + filepath.ToString();

                //ImageNew = "https://needly.in/TemplateImage/" + filepath.ToString();
                //Image1.ImageUrl = ImageNew;





                string Red = "0", Blue = "0", Green = "0";
                if (RadioButtonList1.SelectedValue == "1")
                {
                    Red = "1";
                }
                else if (RadioButtonList1.SelectedValue == "2")
                {
                    Green = "1";
                }
                else
                {
                    Blue = "1";

                }

                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("SP_uploadTemplatedetails");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@TemplateName", txtTemplate.Text);
                        cmd.Parameters.AddWithValue("@ShopType", ddlShop.SelectedValue);
                        cmd.Parameters.AddWithValue("@ddlType", ddlType.SelectedValue);
                        cmd.Parameters.AddWithValue("@IsImages", rdbWork.SelectedValue);
                        cmd.Parameters.AddWithValue("@Images", ImageNew);
                        //cmd.Parameters.AddWithValue("@XCoordinate", txtXCoordinate.Text);
                        //cmd.Parameters.AddWithValue("@YCoordinate", txtYCoordinate.Text);
                        //cmd.Parameters.AddWithValue("@Height", txtHeight.Text);
                        //cmd.Parameters.AddWithValue("@Width", txtWidth.Text);
                        //cmd.Parameters.AddWithValue("@Size", txtSize.Text);
                        cmd.Parameters.AddWithValue("@Red", Red);
                        cmd.Parameters.AddWithValue("@Green", Green);
                        cmd.Parameters.AddWithValue("@Blue", Blue);
                        cmd.Parameters.AddWithValue("@TemplateType", rbTemplate.SelectedValue);
                        //cmd.Parameters.AddWithValue("@ShopkeeperName", txtSkName.Text);
                        //cmd.Parameters.AddWithValue("@ShopkeeperEmail", txtSkEmail.Text);
                        cmd.Parameters.AddWithValue("@Id", ViewState["DataCode"].ToString());
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("@CategoryId", ddlTemplateCategory.SelectedValue);

                        cmd.Parameters.AddWithValue("@BasePrice", txtBasePrice.Text);
                        cmd.Parameters.AddWithValue("@Offerprice", txtOfferPrice.Text);


                        cmd.Parameters.AddWithValue("@ImageType", rbtnImageType.SelectedValue);

                        cmd.Parameters.Add("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                        //con.Open();

                        int A = cmd.ExecuteNonQuery();
                        DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                        ViewState["DataCode"] = DataCode;
                        if (A == -1)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                        }
                        GridView();
                        DataBaseConnection.Close();
                    }

                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);

                    }
                }
            }
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        protected void rdbWork_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(rdbWork.SelectedValue=="1")
            {

                Panel1.Visible = false;
                Panel2.Visible = false;
            }
            else
            {

                Panel1.Visible = false;
                Panel2.Visible = false;
            }
        }

        protected void btnreset_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Admin/AddTemplate.aspx");
        }

        protected void rbTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbTemplate.SelectedValue == "1")
            {
                Panel3.Visible = false; // category
                Panel4.Visible = true;  //shop dropdown
                Shop();
                pnlPrice.Visible = false;
                pnlImageType.Visible = false;
            }
            if (rbTemplate.SelectedValue == "2")
            {
                Panel3.Visible = true;
                TemplateCategoryforBusinessCard();
                Panel4.Visible = false;
                pnlPrice.Visible = true;
                pnlImageType.Visible = false;
            }
            if (rbTemplate.SelectedValue == "3")
            {
                Panel3.Visible = true;
                TemplateCategoryforCertificate();
                Panel4.Visible = false;
                pnlPrice.Visible = false;
                pnlImageType.Visible = false;
            }
            if (rbTemplate.SelectedValue =="4")
            {
                Panel4.Visible = false;
                Panel3.Visible = true;
                TemplateCategoryforDaySpecial();
                pnlPrice.Visible = true;
                pnlImageType.Visible = true;

            }
        }

        public void TemplateCategoryforBusinessCard()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadTemplateCategoryForBusinessCard");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlTemplateCategory.DataSource = ds.Tables[0];
                        ddlTemplateCategory.DataTextField = "DaySpecial";
                        ddlTemplateCategory.DataValueField = "Id";

                        ddlTemplateCategory.DataBind();
                        ddlTemplateCategory.Items.Insert(0, new ListItem(" --Select--  ", ""));
                        ddlTemplateCategory.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlTemplateCategory.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }
        public void TemplateCategoryforCertificate()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadTemplateCategoryForCertificate");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlTemplateCategory.DataSource = ds.Tables[0];
                        ddlTemplateCategory.DataTextField = "DaySpecial";
                        ddlTemplateCategory.DataValueField = "Id";

                        ddlTemplateCategory.DataBind();
                        ddlTemplateCategory.Items.Insert(0, new ListItem(" --Select--  ", ""));
                        ddlTemplateCategory.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlTemplateCategory.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        public void TemplateCategoryforDaySpecial()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadTemplateCategoryForDaySpecial");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlTemplateCategory.DataSource = ds.Tables[0];
                        ddlTemplateCategory.DataTextField = "DaySpecial";
                        ddlTemplateCategory.DataValueField = "Id";

                        ddlTemplateCategory.DataBind();
                        ddlTemplateCategory.Items.Insert(0, new ListItem(" --Select--  ", ""));
                        ddlTemplateCategory.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlTemplateCategory.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        protected void GVAddTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddTemplate.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void lbkbtnDelete_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gr = (GridViewRow)btn.NamingContainer;
                int i = Convert.ToInt32(gr.RowIndex);
                string ID = Convert.ToString(GVAddTemplate.Rows[i].Cells[0].Text);

                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_DeleteAddTemplate");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", ID);

                    int A = cmd.ExecuteNonQuery();

                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Deleted Successfully...!');", true);
                        GridView();
                    }
                    DataBaseConnection.Close();

                }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
            }
        }
    }
}