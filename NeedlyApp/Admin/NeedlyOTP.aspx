﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="NeedlyOTP.aspx.cs" Inherits="NeedlyApp.Admin.NeedlyOTP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Needly OTP<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                
       <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Mobile No: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtMob" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>
               </div></div> 
             
                <div class="col-md-12">
                    <div class="box-body col-sm-4">
                             <asp:Button ID="btnSearch" runat="server" Text="Search..!" class="btn btn-success" OnClick="btnSearch_Click" />
                            
                    </div>
                </div>
                      <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label4" runat="server"><b>Count:</b></asp:Label>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVOTP"  runat="server" OnPageIndexChanging="GVOTP_PageIndexChanging" PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                          </asp:TemplateField>     
                           <asp:BoundField DataField="UserMobileNumber" HeaderText="MobileNo">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Name" HeaderText="Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="firmName" HeaderText="ShopName">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="eMailId" HeaderText="Mail ID">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="IMEINumber" HeaderText="DeviceID">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="OTP" HeaderText="OTP">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="State" HeaderText="State">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>



