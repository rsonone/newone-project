﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class RegisterNewsPaper : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
                District();
                BindState();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetNewsPaperdetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVNewsPaper.DataSource = ds.Tables[0];
                        GVNewsPaper.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState", con);
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlstate.DataSource = ds.Tables[0];
                    ddlstate.DataTextField = "STATE_NAME";
                    ddlstate.DataValueField = "MDDS_STC";
                    ddlstate.DataBind();
                    ddlstate.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlstate.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", 27);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {  
                    SqlCommand cmd = new SqlCommand("SP_UploadNewsPaper", DataBaseConnection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Paper_Name", txtPaperName.Text);
                    cmd.Parameters.AddWithValue("@RegionalName", txtRigiName.Text);
                    cmd.Parameters.AddWithValue("@Language", ddlLang.SelectedValue);
                    cmd.Parameters.AddWithValue("@Paper_Origin", ddlstate.SelectedValue);
                    cmd.Parameters.AddWithValue("@Frequency", ddlFreq.SelectedValue);
                    cmd.Parameters.AddWithValue("@Website", txtWebsite.Text);
                    cmd.Parameters.AddWithValue("@Head_OfcAddress", txtHAdd.Text);
                    cmd.Parameters.AddWithValue("@MobileNo", txtRMobno.Text);
                    cmd.Parameters.AddWithValue("@ContactPersonName", txtCntName.Text);
                    cmd.Parameters.AddWithValue("@ContactMobileNo", txtCntMob.Text);
                    cmd.Parameters.AddWithValue("@CompanyName", txtCompName.Text);
                    cmd.Parameters.AddWithValue("@District", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    con.Close();
                    DataBaseConnection.Close();
                    GridView();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA SAVE SUCCESSFULLY...!');", true);
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA SAVE UNSUCCESSFULLY...! ');", true);

                }
            }
        }
        protected void GVNewsPaper_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVNewsPaper.PageIndex = e.NewPageIndex;
            GridView();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    District();
                    Button btn = sender as Button;
                    GridViewRow gv = btn.NamingContainer as GridViewRow;
                    lblId.Text = (btn.FindControl("lblId1") as Label).Text;
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = DataBaseConnection;
                    DataBaseConnection.Open();

                    command4.CommandText = "SP_EditNewsPaperdetails";
                    command4.CommandType = System.Data.CommandType.StoredProcedure;
                    command4.Parameters.AddWithValue("@ID", lblId.Text);

                    SqlDataAdapter da4 = new SqlDataAdapter();
                    da4.SelectCommand = command4;
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        txtPaperName.Text = ds4.Tables[0].Rows[0][1].ToString();
                        txtRigiName.Text = ds4.Tables[0].Rows[0][2].ToString();
                        ddlLang.SelectedItem.Text = ds4.Tables[0].Rows[0][3].ToString();
                        ddlFreq.SelectedValue = ds4.Tables[0].Rows[0][4].ToString();

                        //txtOrigin.Text = ds4.Tables[0].Rows[0][5].ToString();
                        //ddlFreq.SelectedItem.Text = ds4.Tables[0].Rows[0][6].ToString();
                        txtWebsite.Text = ds4.Tables[0].Rows[0][7].ToString();
                        txtHAdd.Text = ds4.Tables[0].Rows[0][8].ToString();

                        txtRMobno.Text = ds4.Tables[0].Rows[0][9].ToString();
                        txtCntName.Text = ds4.Tables[0].Rows[0][10].ToString();
                        txtCntMob.Text = ds4.Tables[0].Rows[0][11].ToString();
                        txtCompName.Text = ds4.Tables[0].Rows[0][12].ToString();
                    }
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
                }
            }
                
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UpdateNewsPaper", DataBaseConnection);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Id", lblId.Text);
                    cmd.Parameters.AddWithValue("@Paper_Name", txtPaperName.Text);
                    cmd.Parameters.AddWithValue("@RegionalName", txtRigiName.Text);
                    cmd.Parameters.AddWithValue("@Language", ddlLang.SelectedValue);
                    cmd.Parameters.AddWithValue("@Paper_Origin", ddlstate.SelectedValue);
                    cmd.Parameters.AddWithValue("@Frequency", ddlFreq.SelectedValue);
                    cmd.Parameters.AddWithValue("@Website", txtWebsite.Text);
                    cmd.Parameters.AddWithValue("@Head_OfcAddress", txtHAdd.Text);
                    cmd.Parameters.AddWithValue("@MobileNo", txtRMobno.Text);
                    cmd.Parameters.AddWithValue("@ContactPersonName", txtCntName.Text);
                    cmd.Parameters.AddWithValue("@ContactMobileNo", txtCntMob.Text);
                    cmd.Parameters.AddWithValue("@CompanyName", txtCompName.Text);
                    cmd.Parameters.AddWithValue("@District", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    con.Close();
                    DataBaseConnection.Close();
                    GridView();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA SAVE SUCCESSFULLY...!');", true);
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA SAVE UNSUCCESSFULLY...! ');", true);

                }
            }
        }
        protected void btnexcel_Click(object sender, EventArgs e)
        {
            PnlNews.Visible = false;
            Pnlexcel.Visible = true;
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Admin/Newspaper.xlsx");
        }
        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string Paper_Name = dt.Rows[i]["Paper_Name"].ToString();
                        Paper_Name = Paper_Name.Trim();

                        string RegionalName = dt.Rows[i]["RegionalName"].ToString();
                        RegionalName = RegionalName.Trim();

                        string Language = dt.Rows[i]["Language"].ToString();
                        Language = Language.Trim();

                        string Paper_Origin = dt.Rows[i]["Paper_Origin"].ToString();
                        Paper_Origin = Paper_Origin.Trim();

                        string Frequency = dt.Rows[i]["Frequency"].ToString();
                        Frequency = Frequency.Trim();

                        string Website = dt.Rows[i]["Website"].ToString();
                        Website = Website.Trim();

                        string Head_OfcAddress = dt.Rows[i]["Head_OfcAddress"].ToString();
                        Head_OfcAddress = Head_OfcAddress.Trim();

                        string MobileNo = dt.Rows[i]["MobileNo"].ToString();
                        MobileNo = MobileNo.Trim();

                        string ContactPersonName = dt.Rows[i]["ContactPersonName"].ToString();
                        ContactPersonName = ContactPersonName.Trim();

                        string ContactMobileNo = dt.Rows[i]["ContactMobileNo"].ToString();
                        ContactMobileNo = ContactMobileNo.Trim();

                        string CompanyName = dt.Rows[i]["CompanyName"].ToString();
                        CompanyName = CompanyName.Trim();

                        string District = dt.Rows[i]["District"].ToString();
                        District = District.Trim();

                        SqlCommand cmd = new SqlCommand("SP_UploadNewsPaper", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@Paper_Name", Paper_Name);
                        cmd.Parameters.AddWithValue("@RegionalName", RegionalName);
                        cmd.Parameters.AddWithValue("@Language", Language);
                        cmd.Parameters.AddWithValue("@Paper_Origin", Paper_Origin);
                        cmd.Parameters.AddWithValue("@Frequency", Frequency);
                        cmd.Parameters.AddWithValue("@Website", Website);
                        cmd.Parameters.AddWithValue("@Head_OfcAddress", Head_OfcAddress);
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@ContactPersonName", ContactPersonName);
                        cmd.Parameters.AddWithValue("@ContactMobileNo", ContactMobileNo);
                        cmd.Parameters.AddWithValue("@CompanyName", CompanyName);
                        cmd.Parameters.AddWithValue("@District", District);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        DataBaseConnection.Close();

                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }
        protected void btnUploadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) & Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("AllExcelUoload") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;

                            OleDbConnection myExcelConn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName + ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }
    }
}