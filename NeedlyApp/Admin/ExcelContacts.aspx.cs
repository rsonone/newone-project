﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class ExcelContacts : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GridView();
               BindCreatedBy();

                lblExcelCountMsg.Visible = true;
            }
            

        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetExcelContactDetils1");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvExcelContacts.DataSource = ds.Tables[0];
                        GvExcelContacts.DataBind();
                    }

                    string SQl1 = "select count(MobileNo) from [EzeeFormsDB].[dbo].[tbl_ContactDetails]";
                    String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;
                    //lblWhatsAppMsgCount.Visible = false;
                    //lblPhoneContactCount.Visible = false;
                    // PnlSearch.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void brnSearchByMobile_Click(object sender, EventArgs e)
        {
            GridViewForSerch();
        }

        public void BindCreatedBy()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("select DISTINCT(CreatedBy) from [EzeeFormsDB].[dbo].[tbl_ContactDetails]");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    ddlCreatedBy.DataSource = dt;
                    ddlCreatedBy.DataBind();
                    ddlCreatedBy.DataTextField = "CreatedBy";
                    //ddlCreatedBy.DataValueField = "ID";
                    ddlCreatedBy.DataBind();


                }
                catch (Exception ex)
                {

                }
            }




        }

        public void GridViewForSerch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchExcelNumberNew");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Mobile", txtMobileNumber.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvExcelContacts.DataSource = ds.Tables[0];
                        GvExcelContacts.DataBind();
                    }
                    else
                    {
                        lblNoData.Text = "No rocord found for CreatedBy  '" + txtMobileNumber.Text + "'";
                    }


                }
                catch (Exception ex)
                {

                }
            }
        }
        public void GridViewForSerchByCreatedBy()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchExcelByCreatedBy");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CreatedBy", ddlCreatedBy.SelectedItem.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvExcelContacts.DataSource = ds.Tables[0];
                        GvExcelContacts.DataBind();

                    }
                    else
                    {
                        lblNoData.Text = "No rocord found for CreatedBy  '" + ddlCreatedBy.SelectedItem.Text + "'";
                    }

                    string SQl1 = "select count(MobileNo) from [EzeeFormsDB].[dbo].[tbl_ContactDetails] where CreatedBy = '"+ddlCreatedBy.SelectedItem.Text+"'";
                    String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;


                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnSearchByreatedBy_Click(object sender, EventArgs e)
        {
            GridViewForSerchByCreatedBy();
        }
    }
}