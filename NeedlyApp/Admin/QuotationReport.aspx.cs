﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class QuotationReport : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString);
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CustomerName();
            }
            GridDate();
            Grid2();
            Grid3();
        }
        protected void GvQuotationDate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GvQuotationDate.PageIndex = e.NewPageIndex;
            GridDate();
        }
        protected void NumberGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            NumberGrid.PageIndex = e.NewPageIndex;
            Grid2();
        }
        protected void CustomerGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomerGrid.PageIndex = e.NewPageIndex;
            Grid3();
        }
        
        public void GridDate()
        {

             SqlCommand cmd = new SqlCommand("sp_showquotationdata");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
            //String.Format("{0:yyyy-mm-dd}", ViewState["txtDate1"].ToString());
            //String.Format("{0:yyyy-mm-dd}", ViewState["txtDate2"].ToString());

            //cmd.Parameters.AddWithValue("@fromdate", ViewState["txtDate1"].ToString());
            //cmd.Parameters.AddWithValue("@todate", ViewState["txtDate2"].ToString());
            cmd.Parameters.AddWithValue("@fromdate", txtDate1.Text);
            cmd.Parameters.AddWithValue("@todate", txtDate2.Text);
            SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                DataSet ds = new DataSet();
                 //DataTable dt =  New   taTable();
                da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GvQuotationDate.DataSource = ds;
                GvQuotationDate.DataBind();
                Label2.Text = (ds.Tables[0].Rows.Count.ToString());
            }
            con.Close();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        public void CustomerName()
        {
           
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_getcustomername");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlCustomerName.DataSource = ds.Tables[0];
                        ddlCustomerName.DataTextField = "Customer_Name";
                        ddlCustomerName.DataValueField = "Id";

                        ddlCustomerName.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlCustomerName.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlCustomerName.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlCustomerName.SelectedItem.Text = "";


                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                   con.Close();
                }
            

        }

        public void Grid2()
        {
            SqlCommand cmd = new SqlCommand("sp_showquotationNo");
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@QuotationNo", txtquotationNo.Text);

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            DataSet ds = new DataSet();
            //  DataTable dt =  New   taTable();
            da.Fill(ds);
           
            if (ds.Tables[0].Rows.Count > 0)
            {
                NumberGrid.DataSource = ds;
                NumberGrid.DataBind();
                Label4.Text = (ds.Tables[0].Rows.Count.ToString());
            }
            con.Close();
        }
       
        public void Grid3()
        {
            SqlCommand cmd = new SqlCommand("sp_showquotationCustomerName1");
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CustomerName", ddlCustomerName.SelectedItem.Text);
           
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            DataSet ds = new DataSet();
            //  DataTable dt =  New   taTable();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                CustomerGrid.DataSource = ds;
                CustomerGrid.DataBind();
                Label6.Text = (ds.Tables[0].Rows.Count.ToString());
            }
            con.Close();
        }


        protected void btnshow_OnClick(object sender,EventArgs e)
        {
            GridDate();
            Grid2();
            Grid3();
            
        }
        protected void btnExcel_Click0(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "QuotationReport.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GvQuotationDate.AllowPaging = false;
            GvQuotationDate.PageIndex = 0;

            GridDate(); 
            //Change the Header Row back to white color
            GvQuotationDate.HeaderRow.Style.Add("background-color", "#FFFFFF"); 
            //Applying stlye to gridview header cells
            for (int i = 0; i < GvQuotationDate.HeaderRow.Cells.Count; i++)
            {
                GvQuotationDate.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GvQuotationDate.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        protected void btnExcel_Click1(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "QuotationReport.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            NumberGrid.AllowPaging = false;
            NumberGrid.PageIndex = 0;

            Grid2();
            //Change the Header Row back to white color
            NumberGrid.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < NumberGrid.HeaderRow.Cells.Count; i++)
            {
                NumberGrid.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            NumberGrid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        protected void btnExcel_Click2(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "QuotationReport.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            CustomerGrid.AllowPaging = false;
            CustomerGrid.PageIndex = 0;

            Grid3();
            //Change the Header Row back to white color
            CustomerGrid.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < CustomerGrid.HeaderRow.Cells.Count; i++)
            {
                CustomerGrid.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            CustomerGrid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        protected void RadioButtonList1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbQotation.SelectedValue == "1")
            {
                PanelDateWise.Visible = true;
                PanelNumberWise.Visible = false;
                PanelCustomer.Visible = false;


            }
            if (rdbQotation.SelectedValue == "2")
            {
                PanelNumberWise.Visible = true;
                PanelCustomer.Visible = false;
                PanelDateWise.Visible = false;



            }
            if (rdbQotation.SelectedValue == "3")
            {
                PanelCustomer.Visible = true;
                PanelNumberWise.Visible = false;
                PanelDateWise.Visible = false;

            }
        }


    }
}