﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddTemplate.aspx.cs"  MasterPageFile="~/MasterPage/NeedlyMaster.Master" Inherits="NeedlyApp.Admin.AddTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function showpreview(input) {

            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imgpreview').css('visibility', 'visible');
                    $('#imgpreview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add Template <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                    <a href="tempCategory.aspx" data-toggle="tooltip" title="Add Template" class="btn btn-primary">
                    Add Category</a><a href="AddTemplate.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
               
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
                   <div  class="box-header with-border">

                        <div class="col-md-12">
       
                    <div class="box-body col-sm-10">
                            <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                      Template Type :</label>
                                   <div class="col-sm-9">
                                
                                   <asp:RadioButtonList ID="rbTemplate" runat="server"  OnSelectedIndexChanged="rbTemplate_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true">
                                   <asp:ListItem Value="4">&nbsp;&nbsp;Day Special&nbsp;&nbsp;</asp:ListItem>
                                       <asp:ListItem Value="1">&nbsp;&nbsp;Promotional & Offeres&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Visiting  Card&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="3">&nbsp;&nbsp;Certificate&nbsp;&nbsp;</asp:ListItem>
                                    
                                    
                                </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ForeColor="#CC0000"
                        ValidationGroup="Validform" runat="server" ControlToValidate="rbTemplate" ErrorMessage=" Template Type is Required"
                        InitialValue="0"></asp:RequiredFieldValidator>
                                          <br />
                          <%--      <asp:TextBox ID="TextBox1" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>--%>
                            </div></div>
                      </div>
                   
                    </div>

                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <asp:Panel ID="Panel4" Visible="false" runat="server">
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Shop: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlShop" CssClass="form-control"  runat="server" AutoPostBack="true">
                                <%-- <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>--%>
                                 <asp:ListItem Value="0" Text="-- Select --"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div>

                         </asp:Panel>
                         <asp:Panel ID="Panel3" runat="server" Visible="false">
                              <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Template Category: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTemplateCategory" CssClass="form-control"  runat="server" AutoPostBack="true">
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div>
                         </asp:Panel>
                        
                     </div>
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Template Name: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtTemplate" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>
               </div>
                    </div>

                       <asp:Panel ID="pnlPrice" Visible="false" runat="server">
                        <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Base Price: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtBasePrice" runat ="server"  CssClass="form-control"></asp:TextBox>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                    Offer Price : </label>
                              <div class="col-sm-9">
                               <asp:TextBox ID="txtOfferPrice" runat ="server" CssClass="form-control"> </asp:TextBox>
                                  <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtBasePrice"   
ControlToValidate="txtOfferPrice" Display="Dynamic" ErrorMessage="Offer Price Should not be greater than Base Price" ForeColor="Red"   
Operator="LessThanEqual" Type="Integer" ValidationGroup="B"></asp:CompareValidator >  
                                 
                          </div>
                             </div></div>
                                        </div>
               </div></div> 
                       </asp:Panel>

                         <asp:Panel ID="pnlImageType" Visible="false" runat="server">
                             <div class="col-md-12">
       
                    <div class="box-body col-sm-10">
                            <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                      Template Type :</label>
                                   <div class="col-sm-9">
                                
                                   <asp:RadioButtonList ID="rbtnImageType" runat="server"  RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1" Selected="True">  &nbsp;&nbsp; Normal &nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp; Advance &nbsp;&nbsp;</asp:ListItem>
                                 
                                </asp:RadioButtonList>
                                       
                                          <br />
                          <%--      <asp:TextBox ID="TextBox1" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>--%>
                            </div></div>
                      </div>
                   
                    </div>
                       </asp:Panel>
                       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                           <ContentTemplate>
                                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Image: '</label>
                                <div class="col-sm-9">
                                   <%-- <asp:FileUpload ID="FileUpload1" Onchanged="showpreview(this);" runat="server" />--%>
                                      <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />
                          </div></div>

                     </div>
                   
                    <div class="box-body col-sm-offset-2 col-sm-4">
                      <%--<asp:Image ID="Image1" Height="200px" Width="300px" runat="server" />--%>
                         <img id="imgpreview" height="200" width="200" src="" style="border-width: 0px; visibility: hidden;" />
</div>       
                    
                    <%-- <asp:FileUpload ID="fuimage" runat="server" onchange="showpreview(this);" />--%>
                    </div> </div></div>
                           </ContentTemplate>
                       </asp:UpdatePanel>

                       </div>
             <br />
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                              <ContentTemplate>

                                   <asp:Panel ID="Panel5" Visible="false" runat="server">
                                            <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    Shopkeeper Name: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtSkName" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>

                      <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                      Shopkeeper Email:: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtSkEmail" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>

               </div>
                    </div>
                
                <div class="col-md-12">
           
      <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Input Type: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlType" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       <asp:ListItem Value="1" Text="Shop Name"></asp:ListItem>
                                       <asp:ListItem Value="2" Text="Shop Address"></asp:ListItem>
                                       <asp:ListItem Value="3" Text="Mobile"></asp:ListItem>
                                       <asp:ListItem Value="4" Text="QR"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>   

                    <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                               Is Image: </label>
                              <div class="col-sm-9">
                                   <asp:RadioButtonList ID="rdbWork" runat="server" OnSelectedIndexChanged="rdbWork_SelectedIndexChanged"   RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Yes&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;No&nbsp;&nbsp;</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          <%--      <asp:TextBox ID="TextBox1" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div>--%>
                              </div></div>
                </div>

                    <asp:Panel ID="Panel1" Visible="false" runat="server">
                        <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    X_Coordinate: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtXCoordinate" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>

                      <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                      Y_Coordinate:: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtYCoordinate" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>

               </div>
                    </div>

                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    Height: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtHeight" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>

                      <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                      Width:: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtWidth" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>
                    </div>
               </div>
                    </div>

                        </asp:Panel>
                    
                    <asp:Panel ID="Panel2" Visible="false" runat="server">
                                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                    Text Size: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtSize" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </div></div></div>

                      <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                      Color:: </label>
                                   <div class="col-sm-9">
                                
                                   <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Red&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Grren&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="3">&nbsp;&nbsp;Blue&nbsp;&nbsp;</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          <%--      <asp:TextBox ID="TextBox1" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>--%>
                            </div></div></div>
                    </div>

               </div>
                    </div>
                        </asp:Panel>
                     </asp:Panel>
                                  </ContentTemplate>
                          </asp:UpdatePanel>
                  
               <div class="box-body col-sm-4">

                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" ValidationGroup="B" />
                   
                              <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-success" OnClick="btnreset_Click" ValidationGroup="B" />
                   <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" OnClick="btnUpdate_Click" ValidationGroup="B" />
                   
           </div>

        </div>
                    <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVAddTemplate" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVAddTemplate_PageIndexChanging" 
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                         --%>
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="ShopType" HeaderText="Shop Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="TemplateName" HeaderText="Template Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                         <asp:BoundField DataField="TemplateType" HeaderText="Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="CategoryId" HeaderText="CategoryId">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        
                         <asp:BoundField DataField="BasePrice" HeaderText="Base price">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="OfferPrice" HeaderText="Offer Price">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:ImageField DataImageUrlField="BackgroundImage" ItemStyle-Width="50" ControlStyle-Height = "100"  HeaderText="Image">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:ImageField> 
                         <asp:TemplateField HeaderText="Action"  >
                             <ItemTemplate>
                                 <asp:Button ID="btnDelete" OnClick="btnDelete_Click" runat="server" CssClass=" btn-danger" Text="Delete" />
                                   
                             </ItemTemplate>
                         </asp:TemplateField>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
         
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div>
</asp:Content>

