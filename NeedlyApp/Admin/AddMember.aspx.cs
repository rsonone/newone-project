﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace NeedlyApp.Admin
{
    public partial class AddMember : System.Web.UI.Page
    {

        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlConnection conn = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        CommonCode cc = new CommonCode();
        SqlDataAdapter da = new SqlDataAdapter();
        int updateCount = 0, insertCount = 0, totalCount = 0;
        //CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        DataSet ds = new DataSet();
        DataTable dt;
        Errorlogfil EL = new Errorlogfil();
        string Loginmobile = string.Empty;
        string StringImage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Loginmobile = Convert.ToString(Session["username"]);

            if (!IsPostBack)
            {
               // GridView();
                BindGroupCode();
                BindState();
                GridView();
            }
        }
        public void GridView()
        {
          
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowMember");
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddMember.DataSource = ds.Tables[0];
                        GVAddMember.DataBind();

                        //lalCount.Text = Convert.ToString(ds.Tables[0].Rows.Count);
                        //Label1.Visible = true;
                    }
                }
                catch (Exception ex)
                {

                }
            

        }

        public void BindGroupCode()
        {
           

                try
                {
                    SqlCommand cmd = new SqlCommand("[sp_GroupCode]");
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AdminMobNo", Loginmobile);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlgroupcode.DataSource = ds.Tables[0];
                       ddlgroupcode.DataTextField = "GroupCode";
                        //ddlgroupcode.DataValueField = "MDDS_STC";
                        ddlgroupcode.DataBind();
                        ddlgroupcode.Items.Insert(0, new ListItem("--Select--", "0"));
                        ddlgroupcode.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {

                }
                conn.Close();
            
        }

        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlState.DataSource = ds.Tables[0];
                    ddlState.DataTextField = "STATE_NAME";
                    ddlState.DataValueField = "MDDS_STC";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlState.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddlDistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }



           
        }

        public void InsertData(DataTable dt = null)
        {
            try
            {
                for (int i = 0; i <= dt.Rows.Count; i++)
                {
                    string BodyType = dt.Rows[i]["BodyType"].ToString();
                    BodyType = BodyType.Trim();

                    string FirstName = dt.Rows[i]["FirstName"].ToString();
                    FirstName = FirstName.Trim();

                    string LastName = dt.Rows[i]["LastName"].ToString();
                    LastName = LastName.Trim();


                    string MobileNo = dt.Rows[i]["MobileNo"].ToString();
                    MobileNo = MobileNo.Trim();

                    string Designation = dt.Rows[i]["Designation"].ToString();
                    Designation = Designation.Trim();






                    SqlCommand cmd = new SqlCommand("SP_UploadAddMember", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 120;
                    conn.Open();

                    cmd.Parameters.AddWithValue("@Group_Id", ddlgroupcode.SelectedValue);
                    cmd.Parameters.AddWithValue("@MemberLevel", ddllevel.SelectedValue);
                    cmd.Parameters.AddWithValue("@State", ddlState.SelectedValue);
                    cmd.Parameters.AddWithValue("@District", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                    cmd.Parameters.AddWithValue("@blocklevel", BodyType);
                    cmd.Parameters.AddWithValue("@FirstName", FirstName);
                    cmd.Parameters.AddWithValue("@LastName", LastName);
                    cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                    cmd.Parameters.AddWithValue("@Designation", Designation);


                    cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                    cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                    //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                   
                    int A = cmd.ExecuteNonQuery();
                    conn.Close();
                    string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                    conn.Close();


                    // int A = cmd.ExecuteNonQuery();
                    conn.Close();
                    cmd.Parameters.Clear();
                    conn.Close();
                    GridView();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);
            }
        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AddMember.xlsx", false);

        }

        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }

        protected void ddldistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        protected void GVAddreferal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddMember.PageIndex = e.NewPageIndex;
            GridView();
        }
    }
}