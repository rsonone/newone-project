﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class NeedlyOTP : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetRegDataNew");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MobNo", txtMob.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVOTP.DataSource = ds;
                        GVOTP.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GridView();
        }
        protected void GVOTP_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVOTP.PageIndex = e.NewPageIndex;
            GridView();
        }
    }
}