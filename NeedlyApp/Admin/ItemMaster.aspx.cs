﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace NeedlyApp.Admin
{
    public partial class ItemMaster : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        string Shoptype = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
                ShopType();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UploadInvTtem");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVInvTtem.DataSource = ds.Tables[0];
                        GVInvTtem.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void ShopType()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetShopCategoryNew");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        chkShoptype.DataSource = ds.Tables[0];
                        chkShoptype.DataTextField = "ItemName";
                        chkShoptype.DataValueField = "Id";
                        chkShoptype.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }

        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string ItemName = dt.Rows[i]["ItemName"].ToString();
                        ItemName = ItemName.Trim();

                        //string ShopType = dt.Rows[i]["ShopType"].ToString();
                        //ShopType = ShopType.Trim();

                        string Brand = dt.Rows[i]["BrandName"].ToString();
                        Brand = Brand.Trim();

                        string ProductCategory = dt.Rows[i]["ProductCategory"].ToString();
                        ProductCategory = ProductCategory.Trim();

                        string ProductCode = dt.Rows[i]["ProductCode"].ToString();
                        ProductCode = ProductCode.Trim();

                        string Quantity = dt.Rows[i]["Quantity"].ToString();
                        Quantity = Quantity.Trim();

                        string Unit = dt.Rows[i]["Unit"].ToString();
                        Unit = Unit.Trim();

                        string MRP = dt.Rows[i]["MRP/Rate"].ToString();
                        MRP = MRP.Trim();

                      
                        for (int c = 0; c < chkShoptype.Items.Count; c++)
                        {
                            if (chkShoptype.Items[c].Selected == true)
                            {
                                Shoptype = Shoptype + "," + chkShoptype.Items[c].Value;
                                //SubjectName = SubjectName + "," + chkShoptype.Items[c].Text;
                            }
                        }
                        if (Shoptype.Length > 1)
                        {
                            Shoptype = Shoptype.Substring(1);
                        }
                        SqlCommand cmd = new SqlCommand("SP_InventoryItemMasterNew", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@ItemName", ItemName);
                        cmd.Parameters.AddWithValue("@ShopType", Shoptype);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@ProductCategory", ProductCategory);
                        cmd.Parameters.AddWithValue("@Quantity", Quantity);
                        cmd.Parameters.AddWithValue("@Unit", Unit);
                        cmd.Parameters.AddWithValue("@MRP", MRP);
                        cmd.Parameters.AddWithValue("@ProductCode", ProductCode);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        DataBaseConnection.Close();
                        GridView();
                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/InventoryItem.xlsx", false);
        }

        protected void GVInvTtem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVInvTtem.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void GVInvTtem_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GVInvTtem.EditIndex = e.NewEditIndex;
            this.GridView();
        }
        protected void GVInvTtem_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GVInvTtem.EditIndex = -1;
            this.GridView();
        }
        protected void GVInvTtem_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    int userid = Convert.ToInt32(GVInvTtem.DataKeys[e.RowIndex].Value.ToString());
                    GridViewRow row = (GridViewRow)GVInvTtem.Rows[e.RowIndex];
                    Label lblID = (Label)row.FindControl("lblEdit");

                    TextBox ItemName = (TextBox)row.Cells[1].Controls[0];
                    TextBox Brand = (TextBox)row.Cells[2].Controls[0];
                    TextBox PrdType = (TextBox)row.Cells[3].Controls[0];
                    TextBox Shptyp = (TextBox)row.Cells[4].Controls[0];
                    TextBox Qunty = (TextBox)row.Cells[5].Controls[0];
                    TextBox mrp = (TextBox)row.Cells[6].Controls[0];


                    //SqlCommand cmd = new SqlCommand("SP_UpdateInvTtem", DataBaseConnection);
                    SqlCommand cmd = new SqlCommand("SP_UpdateInvTtemNew", DataBaseConnection);
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@ID", userid);
                    cmd.Parameters.AddWithValue("@ItemName", ItemName.Text);
                    cmd.Parameters.AddWithValue("@ShopType", Shptyp.Text);
                    cmd.Parameters.AddWithValue("@Brand", Brand.Text);
                    cmd.Parameters.AddWithValue("@ProductCategory", PrdType.Text);
                    cmd.Parameters.AddWithValue("@Quantity", Qunty.Text);
                    cmd.Parameters.AddWithValue("@MRP", mrp.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    GVInvTtem.EditIndex = -1;
                    GridView();

                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Record Updated Successfully ... ');", true);
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        protected void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gr = (GridViewRow)btn.NamingContainer;
                int i = Convert.ToInt32(gr.RowIndex);
                string ID = Convert.ToString(GVInvTtem.Rows[i].Cells[0].Text);

                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SP_DeleteItemDetails", DataBaseConnection);
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", ID);

                    int A = cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    GridView();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Deleted Successfully...!');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
            }
        }

    }
}
