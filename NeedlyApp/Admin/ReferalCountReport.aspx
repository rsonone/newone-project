﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="ReferalCountReport.aspx.cs" Inherits="NeedlyApp.Admin.ReferalCountReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Referal data list <small></small></h2>
                       <div class="container-fluid">
                       
        </div>
                    <div class="clearfix"></div>
                  </div>
                 
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
      
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Mobile Number: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtmobileNumber" runat ="server" CssClass="form-control"></asp:TextBox>
                                   <%--  <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                                      ControlToValidate="txtmobileNumber" 
                                      ErrorMessage="Mobile No. is a required field."
                                      ForeColor="Red">
                                    </asp:RequiredFieldValidator>--%>
                                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                         ValidationExpression="[0-9]{10}" ForeColor="Red" ControlToValidate="txtmobileNumber"
                                        ErrorMessage="Mobile No. must be 10 digits"></asp:RegularExpressionValidator>--%>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         
                         <div class="form-group">
                             <div class="box-body col-sm-4">
                              <asp:Button ID="btnUrlAccess"   runat="server" Text="Search" class="btn btn-success"  />
                              <div class="box-body col-sm-4">
                            
                        </div> 
                        </div> 
                            
                             </div> 

                </div>

                    </div>
               </div></div> 

                   
                   

                         
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                
                         </div>

                     </div>

              

                    </div>
               </div></div>

                  
                              </ContentTemplate>
                    </asp:UpdatePanel>

                     <div class="col-md-12">
                         <br />
                         <br />
                           <div class="table-responsive">
                    <asp:GridView ID="GVUrlAceess" Visible="false" CssClass="table table-hover table-bordered" runat="server"  
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> 
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ReferalCount" HeaderText="Referal Count">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                      
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                     <div class="col-md-12">
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVReferealReport" Visible="false"  CssClass="table table-hover table-bordered"  runat="server" 
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
                
    
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Customer_Mobile" HeaderText="Customer No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Customer_Name" HeaderText="Customer Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Shop_Mobile" HeaderText="Shop Mobile">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Shop_Name" HeaderText="Shop Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ShopUser_Name" HeaderText="ShopUser Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="App_Install_Date" HeaderText="App_Install_Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Type" HeaderText="Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                       
                       
                       
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>
</asp:Content>
