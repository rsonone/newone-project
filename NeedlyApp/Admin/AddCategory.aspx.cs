﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddCategory : System.Web.UI.Page
    {
        //SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        //CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShopType1();
                GridView1();
            }
        }
        public void ShopType1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadShopType");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlShop.DataSource = ds.Tables[0];
                        ddlShop.DataTextField = "ItemName";
                        ddlShop.DataValueField = "Id";

                        ddlShop.DataBind();
                        ddlShop.Items.Insert(0, new ListItem("--Select--", ""));
                        ddlShop.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlShop.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }
            
        }
        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_DownloadCategory");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddCat.DataSource = ds;
                        GVAddCat.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch(Exception ex)
                {

                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_InsertCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@CategoryName", txtCatName.Text);
                    cmd.Parameters.AddWithValue("@ShopType", ddlShop.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    GridView1();
                    DataBaseConnection.Close();
                }
                catch(Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
        protected void GVAddCat_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddCat.PageIndex = e.NewPageIndex;
            GridView1();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    Button btn = sender as Button;
                    GridViewRow gv = btn.NamingContainer as GridViewRow;
                    lblId.Text = (btn.FindControl("lblId1") as Label).Text;
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = DataBaseConnection;
                    DataBaseConnection.Open();

                    command4.CommandText = "sp_EditCategory";
                    command4.CommandType = System.Data.CommandType.StoredProcedure;
                    command4.Parameters.AddWithValue("@Id", lblId.Text);

                    SqlDataAdapter da4 = new SqlDataAdapter();
                    da4.SelectCommand = command4;
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        txtCatName.Text = ds4.Tables[0].Rows[0][0].ToString();
                    }
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                catch(Exception ex)
                {

                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UpdateCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Id", lblId.Text);
                    cmd.Parameters.AddWithValue("@CategoryName", txtCatName.Text);
                    cmd.Parameters.AddWithValue("@ShopType", ddlShop.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED SUCCESSFULLY.!!!')", true);
                    }
                    GridView1();
                    DataBaseConnection.Close();
                }
                catch(Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
    }
}