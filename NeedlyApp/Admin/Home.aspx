﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" MasterPageFile="~/MasterPage/NeedlyMaster.Master" Inherits="NeedlyApp.Admin.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      

     <script type="text/javascript" src="https://www.google.com/jsapi"></script>  
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load('visualization', '1', { packages: ['corechart'] });
    </script>

   
     <div class="content-wrapper">
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>

               
                     
                <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange" >
            <div class="inner">
              <h3> <asp:Label ID="lblTodaycount" runat="server"></asp:Label></h3>

              <p> Today's Needly App Installation Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green" >
            <div class="inner">
              <h3><asp:Label ID="lbltodayregistration" runat="server"></asp:Label></h3>

              <p>Website Creation Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue" >
            <div class="inner">
              <h3><asp:Label ID="lblTotalRegistration" runat="server"></asp:Label></h3>

              <p>Needly Family Count </p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->

                    
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple" >
            <div class="inner">
              <h3><asp:Label ID="lblSharedCnt" runat="server"></asp:Label></h3>

              <p>Needly Group Of Group Count </p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
               <br />
              <div class="row">
              <div class="col-lg-12">
                      
                           <div class="pull-right">
              

                             <a href="Dashboardcount.aspx"  title="More Count" class="btn btn-secondary">More Count</a>
                          
                        </div>
                         
                      </div>  
                  </div>
             
              <%-- </div>--%>
      <div class="row">
           
        <div class="col-md-6">
     <div class="box-header with-border">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>

             
            </div>
            <div class="box-body">
              <div class="chart">
            <div >
                 
			 <asp:Literal ID="ltScripts" runat="server"></asp:Literal>  
        <div id="piechart_3d" style="width:450px; height: 300px;">  
		</div></div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
          <div class="col-md-6">
    
              <div class="box-header with-border">
            <div class="box-header with-border" >
              <h3 class="box-title"></h3>

             
            </div>
            <div class="box-body">
              <div class="chart">
                  <asp:Literal ID="Literalbar" runat="server"></asp:Literal>  
        <div id="chart_div" style="width: 450px; height: 300px;"> 
             
              </div> </div>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
      </div>
              <br />
              <br />
               <br />
              <br />
              <br />
              <br />
               <br />
              <br />
               <br />
              <br />
              <div class="row">
        
          <div class="col-md-7">
     <div class="box-header with-border">
            <div class="box-header with-border" >
              <h3 class="box-title"></h3>
                 <div class="small-box">
          <%--  <div class="box-header with-border">
              <h3 class="box-title">Today Pass/Fail</h3>

             
            </div>--%>
            <div class="box-header with-border" style="width: 500px; height: 200px">
              <div class="chart">
                 <asp:Literal ID="ltScriptspin" runat="server"></asp:Literal>  
        <div id="piechart_3d1" style="width: 450px; height: 300px;"> 
            
              </div> </div>
            </div>
            <!-- /.box-body -->
          </div>
             
            </div>
            <div class="box-body">
              <div class="chart">
            <div style="width: 100%">
			  <asp:Literal ID="Literalbarpassfailbar" runat="server"></asp:Literal>  
        <div id="PassFailbar" style="width: 500px; height: 300px;" > </div>
		</div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
            </div>
      </div>
            <%--</div>  --%>
          </section></div>
 <script type="text/javascript" src="../css/bower_components/chart.js/Chart.js"></script>
    


            
           
</asp:Content>



