﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="MD_CategoryLink.aspx.cs" Inherits="NeedlyApp.Admin.MD_CategoryLink" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="m2">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title></title>

<style>
.freebirdFormviewerViewFormContentWrapper {
    display: -webkit-box;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: column;
    flex-direction: column;
}
.freebirdViewerHeaderCard {
    max-height: 22.5vw;
    max-width: 90vw;
    height: 160px;
    width: 640px;
    background-position: center;
}
.freebirdFormviewerViewHeaderCard {
    border: 1px solid #dadce0;
    border-radius: 8px;
    margin-top: 12px;
    background-position: center;
}
.freebirdFormviewerViewCenteredContent {
    margin: auto;
    max-width: 90vw;
    width: 900px;
    background-position: center;
}
.m2 {
    display: block;
}
.freebirdFormviewerViewFormContentWrapper {
    display: -webkit-box;
    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: column;
    flex-direction: column;
}
.freebirdFormviewerViewFormCard {
    background-color: #fff;
    margin-bottom: 48px;
    -webkit-box-shadow: 0 1px 4px 0 rgba(0,0,0,0.37);
    box-shadow: 0 1px 4px 0 rgba(0,0,0,0.37);
    padding-bottom: 6px;
    word-wrap: break-word;
}
.freebirdFormviewerViewFormContent {
    color: #202124;
    padding: 0;
}
.freebirdFormviewerViewNoPadding {
    padding: 0;
}
.freebirdFormviewerViewHeaderHeader {
    margin-top: 12px;
    background-color: #fff;
    border: 1px solid #dadce0;
    border-radius: 8px;
    margin-bottom: 12px;
    padding: 24px;
    padding-top: 22px;
    position: relative;
}
.freebirdFormviewerViewHeaderThemeStripe {
    border-top-left-radius: 8px;
    border-top-right-radius: 8px;
    height: 10px;
    left: -1px;
    position: absolute;
    top: -1px;
    width: calc(100% + 2px);
}
.freebirdSolidBackground {
    background-color: rgb(237, 189, 160);
    color: rgba(0, 0, 0, 1);
}
div {
    display: block;
}
body {
    background-color: #ede7f6;
    font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;
    margin: 0;
}
.cards {
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  margin: 0;
  padding: 0;
}
.cities {
  font-size:25px;
  font-weight:bold;
  font-family: "Times New Roman", Times, serif;
  width: 1000PX;
  margin: 20px;
  padding: 20px;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
}
.table {
   border-collapse: collapse;
  border: 1px solid black;
}
           </style>

<style type="text/css">
.gray-overlay {
    height: 124px;
    width: 100%;
    position: relative;
    border-radius: 8px;
    background-image: linear-gradient(180deg,transparent,rgba(0,0,0,.4) 99%);
}
.category-text {
    font-family: GalanoMedium;
    position: absolute;
    bottom: 8px;
    padding-left: 12px;
    padding-right: 12px;
    white-space: pre-wrap;
    line-height: 20px;
    color: #fff;
    font-size: 15px;
    overflow-wrap: break-word;
    display: -webkit-box;
    font-size: 14px;
    line-height: 18px;
    margin: 0 auto;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: normal;
}
.category-image {
    position: absolute;
    top: 0;
    left: 0;
    width: 50%;
    object-fit: cover;
}
.rounded-xl {
    border-radius: .5rem!important;
}
.thumbnail-container {
    position: relative;
    overflow: hidden;
    object-position: center;
}
.rounded-xl {
    border-radius: .5rem!important;
}
.thumbnail {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    object-fit: cover;
}
.thumbnail-padding {
    padding-bottom: 100%;
}
.row {
  margin-right: -15px;
  margin-left: -15px;
}
.col-md-12 {
    width: 100%;
  }
.col-md-6 {
    width: 50%;
  }
.col-sm-3 {
    width: 25%;
  }
.col-md-3 {
    width: 25%;
  }
.text-center {
    text-align: center!important;
}
.text-prime {
    color: #146eb4!important;
    font-family: GalanoMedium;
}
.navbar-label {
    font-size: 11px;
    line-height: 1;
    margin-top: 4px;
}
.mb-10 {
    margin-bottom: 10px;
}
    </style>
</head>
<body >
<form id="form1" runat="server">

<div class="freebirdFormviewerViewFormContentWrapper">
  
     <div class="freebirdFormviewerViewCenteredContent">
         <div class="freebirdFormviewerViewFormCard">
             <div class="freebirdFormviewerViewFormContent">
                 <div class="freebirdFormviewerViewNoPadding">
                     <div class="freebirdFormviewerViewHeaderHeader">
                         <div class="freebirdFormviewerViewHeaderThemeStripe freebirdSolidBackground exportThemeStripe"></div>  
                            
                    
                         <div class="row">
                             <asp:TextBox ID="TextBox1" runat="server" placeholder="Serach for product.." BorderStyle="Solid" Height="30px"></asp:TextBox> 
                             <asp:LinkButton runat="server" ID="btnSubmit">
                              <i class="fa fa-search" aria-hidden="true" style="font-size:24px;color:black"></i>
                             </asp:LinkButton>
                         </div>  <br /><hr />
                         <div class="thumbnail-container">
                          
                             <div class="thumbnail-container">
                                 <table>
                                     <tr>
                                         <td>
                                              <img id="img3" src="~/Logincss/images/Shop.jpg" width="250" runat="server" /><br />
                                             <%--<asp:Button ID="Button1" runat="server" Text='<%#Eval("UrlDetails_1") %>' class="category-text" />--%>
                                         </td>
                                          <td>
                                              <img id="img4" src="~/Logincss/images/Shop.jpg" width="250" runat="server" /><br />
                                             <%--<asp:Button ID="Button1" runat="server" Text='<%#Eval("UrlDetails_1") %>' class="category-text" />--%>
                                         </td>
                                     </tr>
                                 </table>
                             </div>
                         </div> <br /><hr />
                           <div class="thumbnail-container">
                               <asp:Label ID="lblItem" runat="server" Text='<%#Eval("UrlDetails_1") %>' ><b>Medical</b></asp:Label>
                           </div> <br />
                      
                      
                            <table >
                             <tr>
                                 <td>
                                     <div class="text-center">
                                        <a class="text-prime " id="nav_home" href="MyDukan.aspx" ><br />
                                        <img src="Logincss/images/home.png" width="30" height="30" id="icon-home" alt="icon" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <p class="navbar-label mb-10">Home</p></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     </div>
                                     </td>
                                 <td>
                                      <div class="text-center">
                                        <a class="text-prime " id="nav_home1" href="MD_Category.aspx"><br />
                                        <img src="Logincss/images/menu.jpg" width="30" height="30" id="icon-home1" alt="icon" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <p class="navbar-label mb-10">Categories</p></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     </div>
                                     </td>
                                 <td>
                                      <div class="text-center">
                                        <a class="text-prime " id="nav_home2" href="Nitis3">
                                        <img src="Logincss/images/Bag.png" width="30" height="30" id="icon-hom2e" alt="icon" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <p class="navbar-label mb-10">Bag</p></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     </div>
                                     </td>
                                 <td>
                                      <div class="text-center">
                                        <a class="text-prime " id="nav_home3" href="Nitis3">
                                        <img src="Logincss/images/list.png" width="30" height="30" id="icon-home3" alt="icon" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <p class="navbar-label mb-10">Orders</p></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                     </div>
                                 </td>
                             </tr>
                         </table>
                     </div>
                 </div>
             </div>
          </div>  
     </div>
</div>
  
</form>

</body>
</html>



