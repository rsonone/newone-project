﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="SelfEmploymentBusiness.aspx.cs" Inherits="NeedlyApp.Admin.Self_EmploymentBusiness" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Registration for Self Employment / Business<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                    Mobile Number of Beneficiary / Unemployed Person: </label>
                                <div class="col-sm-6">
                            <asp:TextBox ID="txtmobilenumber" CssClass="form-control" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Name of Business: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtbusinessname" CssClass="form-control" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Business Type: </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rbtnbusinesstype" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="New Business &nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="Existing Business &nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
<br />
                          </div></div></div>
                    </div>
               </div>
                         </div>

                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Business Description: </label>
                                <div class="col-sm-6">
                                <asp:TextBox ID="txtdescription" CssClass="form-control" TextMode="MultiLine" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Establishment Year: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtyear" CssClass="form-control" TextMode="Month" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Own Fund / Investment Done till now: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtfund" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Owned land: </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rdbCom" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes&nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="No&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Land Area: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtlandarea" CssClass="form-control" PlaceHolder="(In Sq.ft)"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Expected amount of Fund Required for Expansion: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtexpectedamtoffund" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 What about marketing facilities available?: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtmarketing" CssClass="form-control" Textmode="MultiLine" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Expected / Existing annual Income from this Business: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtanualincome" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Do you have any special skills?: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtskills" CssClass="form-control" Textmode="MultiLine" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="col-md-12">
           <div class="box-body col-sm-5">
                         
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" OnClick="btnUpdate_Click" />
           </div>

        </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddQues"  runat="server"  OnPageIndexChanging="GVAddQues_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%>    
                           <asp:BoundField DataField="Id" HeaderText="ID">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="NameofBusiness" HeaderText="Business Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="BusinessDescription" HeaderText="Business Description">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="EstablishmentYear" HeaderText="Est.Year">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="OwnFundInvestmentDonetillnow" HeaderText="Investment Till">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="LandArea" HeaderText="Land Area">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ExpectedamountofFundRequiredforExpansion" HeaderText="Fund For Expansion">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Whataboutmarketingfacilitiesavailable" HeaderText="Marketing Facilities">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="ExpectedExistingannualIncomefromthisBusiness" HeaderText="Annual Income">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Doyouhaveanyspecialskills" HeaderText="Special Skills">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Createdby" HeaderText="CreatedBy">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                         <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit"  runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>

                           
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>

                    </div>
                  </div>
                </div>
</asp:Content>
