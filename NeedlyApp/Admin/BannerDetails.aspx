﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="BannerDetails.aspx.cs" Inherits="NeedlyApp.Admin.BannerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Banner Details<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlState" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddlDistrict"  CssClass="form-control" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                   Taluka: </label>
                               <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTaluka" CssClass="form-control" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                        
                                   </asp:DropDownList><br />
                                   </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     pinCode: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtpincode" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>
        </div> </div></div> 

                <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                          <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Ward No: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtWardNo" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                   Banner Name: </label>
                               <div class="col-sm-9">
                                   <asp:TextBox ID="txtBannerName" CssClass="form-control" runat="server"></asp:TextBox>
                                   </div></div></div>

        </div> </div></div> 

                <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                   Banner Type: </label>
                               <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlType" CssClass="form-control" runat="server" AppendDataBoundItems="True" AutoPostBack="true">
                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                       <asp:ListItem Value="1"> 1</asp:ListItem>
                                       <asp:ListItem Value="2"> 2</asp:ListItem>
                                       <asp:ListItem Value="3"> 3</asp:ListItem>
                                   </asp:DropDownList>
                                   </div></div></div>

                        <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Image Url: </label>
                                   <div class="col-sm-9">
                                    <asp:TextBox ID="txtUrl" CssClass="form-control" runat="server"></asp:TextBox>
                                       </div></div></div>
        </div> </div></div> 

                <div class="row">
           <div class="panel-body">
                     <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                   <label class="col-sm-3 control-label" for="input-name2">
                                     Upload Image: </label>
                                   <div class="col-sm-9">
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                       </div></div></div>
                         </div></div></div> 

                <div class="col-md-12">
           <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                              <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" />
                             <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" OnClick="btnreset_Click" />
                         
           </div>
 <div class="box-body col-sm-4">
     <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
           </div>



        </div>

                    
          </div></div></div>
</asp:Content>
