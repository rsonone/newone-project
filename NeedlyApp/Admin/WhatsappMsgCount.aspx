﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="WhatsappMsgCount.aspx.cs" Inherits="NeedlyApp.Admin.WhatsappMsgCount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Whatsapp Message Count <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                   
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddCat"  runat="server"    PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                     <%--   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     --%>
                         <asp:BoundField DataField="Id" HeaderText="ID" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="JAN_COUNT" HeaderText="JAN">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="FEB_COUNT" HeaderText="FEB">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="MARCH_COUNT" HeaderText="MARCH">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="APRIL_COUNT" HeaderText="APRIL">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="MAY_COUNT" HeaderText="MAY">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="JUNE_COUNT" HeaderText="JUNE">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="JULY_COUNT" HeaderText="JULY">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="AUG_COUNT" HeaderText="AUG">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="SEPT_COUNT" HeaderText="SEPT">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="OCT_COUNT" HeaderText="OCT">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="NOV_COUNT" HeaderText="NOV">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="DEC_COUNT" HeaderText="DEC">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="CreatedDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
     
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                         </div>
                    </div>
                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>



