﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class Reg_ExcelUpload : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        string DataCode = string.Empty;
        int updateCount = 0, insertCount = 0, totalCount = 0;
        string inupcount = string.Empty;
        string returnString = string.Empty;
        int NotinsertCount = 0;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindState();
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowRegAddressDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVRegistration.DataSource = ds.Tables[0];
                        GVRegistration.DataBind();

                        lalCount.Text = Convert.ToString(ds.Tables[0].Rows.Count);
                        Label1.Visible = true;
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlstate.DataSource = ds.Tables[0];
                    ddlstate.DataTextField = "STATE_NAME";
                    ddlstate.DataValueField = "MDDS_STC";
                    ddlstate.DataBind();
                    ddlstate.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlstate.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddldistrict.DataSource = ds.Tables[0];
                    ddldistrict.DataTextField = "DISTRICT_NAME";
                    ddldistrict.DataValueField = "MDDS_DTC";

                    ddldistrict.DataBind();
                    ddldistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddldistrict.SelectedIndex = 0;
                }
                else
                {
                    ddldistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddldistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }
        protected void ddldistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();

        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }
        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string MobNo = dt.Rows[i]["Mobile no"].ToString();
                        MobNo = MobNo.Trim();

                        if (MobNo.Length ==10 || MobNo.Length == 12)
                        {
                            string FirmName = dt.Rows[i]["Firm name"].ToString();
                            FirmName = FirmName.Trim();

                            string ShopKepName = dt.Rows[i]["shopkeeper name"].ToString();
                            ShopKepName = ShopKepName.Trim();

                            string ShopType = dt.Rows[i]["shop type"].ToString();
                            ShopType = ShopType.Trim();

                            string Alt_MobNo = dt.Rows[i]["alternate mob no"].ToString();
                            Alt_MobNo = Alt_MobNo.Trim();

                            string Email = dt.Rows[i]["email id"].ToString();
                            Email = Email.Trim();

                            string PrabhagNo = dt.Rows[i]["prabhag no"].ToString();
                            PrabhagNo = PrabhagNo.Trim();
                            
                            string Address = dt.Rows[i]["detail address"].ToString();
                            Address = Address.Trim();

                            string area = dt.Rows[i]["area"].ToString();
                            area = area.Trim();

                            string landmark = dt.Rows[i]["landmark"].ToString();
                            landmark = landmark.Trim();

                            string pincode = dt.Rows[i]["pincode"].ToString();
                            pincode = pincode.Trim();

                            if (pincode == "")
                            {
                                pincode = "0";
                            }
                            // int pincode = Int32.Parse(dt.Rows[i]["pincode"].ToString());
                            //pincode = pincode.ToString();

                            string Query = "Select top 1 [EzeeDrugAppId] from [DBNeedly].[dbo].[EzeeDrugsAppDetail] where mobileNo ='" + MobNo + "' and keyword='NEEDLY'";
                            string Res = cc.ExecuteScalarDrug(Query);

                            if (Res != "")
                            {
                                updateCount++;
                            }

                            //SqlCommand cmd = new SqlCommand("SP_UploadRegDetailsNew", DataBaseConnection);
                            SqlCommand cmd = new SqlCommand("SP_UploadRegDetails", DataBaseConnection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            DataBaseConnection.Open();

                            cmd.Parameters.AddWithValue("@firmName", FirmName);
                            cmd.Parameters.AddWithValue("@MobNo", MobNo);
                            cmd.Parameters.AddWithValue("@Fullname", ShopKepName);
                            cmd.Parameters.AddWithValue("@PrabhagNo", PrabhagNo);
                            cmd.Parameters.AddWithValue("@Address", Address);
                            cmd.Parameters.AddWithValue("@EmailId", Email);
                            cmd.Parameters.AddWithValue("@PinCode", pincode);
                            cmd.Parameters.AddWithValue("@Ref_MobNo", Alt_MobNo);
                            cmd.Parameters.AddWithValue("@Area", area);
                            cmd.Parameters.AddWithValue("@ShopType", ShopType);
                            cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                            cmd.Parameters.AddWithValue("@State", ddlstate.SelectedValue);
                            cmd.Parameters.AddWithValue("@District", ddldistrict.SelectedValue);
                            cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);
                            cmd.Parameters.AddWithValue("@Landmark", landmark);

                            cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                            cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                            int A = cmd.ExecuteNonQuery();
                            con.Close();
                            string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                            DataBaseConnection.Close();
                            GridView();

                            insertCount++;

                            if (A == -1)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                            }

                       
                            totalCount = insertCount + updateCount + NotinsertCount;
                            lblMessage.Text = "NUMBER OF RECORDS UPLOADED : " + totalCount + " INSERTED : " + insertCount + " AND UPDATED : " + updateCount + " AND NOTINSERTED : " + NotinsertCount;
                        }
                        else
                        {
                            NotinsertCount++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }
        protected void btnreset_Click(object sender, EventArgs e)
        {

        }

        protected void GVRegistration_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVRegistration.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/UploadExcel/Needly.xlsx", false);
        }

    }
}