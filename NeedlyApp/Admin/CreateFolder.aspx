﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="CreateFolder.aspx.cs" Inherits="NeedlyApp.Admin.CreateFolder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="clearfix"></div>
   
    <div class="row">
       
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i> Save Url <small></small></h2>
                    <div class="container-fluid">
                       
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="panel-body">

                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Select Category
                                            </label>
                                            <div class="col-sm-9">
                                               
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Monile Number
                                            </label>
                                            <div class="col-sm-9">
                                              <asp:TextBox ID="txtMobileNumber" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Select District
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlDistrict" AppendDataBoundItems="true" CssClass="form-control" runat="server" >
                                                    <asp:ListItem Text="select" value="0"></asp:ListItem>
                                                </asp:DropDownList><br />
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Approved
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:RadioButtonList ID="RadioButtonList1" RepeatDirection="Horizontal" runat="server">
                                                    <asp:ListItem Value="0">&nbsp; Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                                      <asp:ListItem Value="1">&nbsp; No</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <br />
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box-body col-sm-5">
                                <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>
                            </div>
                            <div class="box-body col-sm-4">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" class="btn btn-success btn-round" />
                            </div>
                            <div class="box-body col-sm-4">
                                <asp:Label ID="Lblerrer" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                 
      
         
    </div>
</asp:Content>
