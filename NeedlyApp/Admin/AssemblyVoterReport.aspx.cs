﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp
{
    public partial class AssemblyVoterReport : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ShopType1();
                //GridView1();
            }
        }
        public void ShopType1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBTrueVoter"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("DownloadAssembly");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlShop.DataSource = ds.Tables[0];
                        ddlShop.DataTextField = "name";
                      //  ddlShop.DataValueField = "Id";

                        ddlShop.DataBind();
                        ddlShop.Items.Insert(0, new ListItem("--Select--", ""));
                        ddlShop.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlShop.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }
        protected void GVAddCat_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddCat.PageIndex = e.NewPageIndex;
            GridView1();

        }
        protected void ddlShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridView1();
            Voterdetails();
            partdetails();
        }
            public void Voterdetails()
            {
                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBTrueVoter"].ConnectionString))
                {

                    try
                    {
                        SqlCommand cmd = new SqlCommand("GetUserDBs");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AC", ddlShop.SelectedItem.Text);
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                        lbltotalvotercount.Text = ds.Tables[0].Rows[0][0].ToString();
                        
                        
                        }
                        else
                        {
                            
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        DataBaseConnection.Close();
                    }
                }
            }

        public void partdetails()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBTrueVoter"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("GetUserDBs1");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AC", ddlShop.SelectedItem.Text);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                       
                        lblpartnocount.Text = ds.Tables[0].Rows[0][0].ToString();

                    }
                    else
                    {

                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }
        }


        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBTrueVoter"].ConnectionString))
            {
                try
                {
                    
                    SqlCommand cmd = new SqlCommand("VoterCountSurveyCount1");
                   // select ACNO, PART_NO, Count(*)SurveyCount from[DBTrueVoter].[dbo].[tbl_SurveyVoterDetails] group by ACNO,PART_NO
                    cmd.Connection = DataBaseConnection;
                    
                    DataBaseConnection.Open();
                     
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AC", ddlShop.SelectedItem.Text);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddCat.DataSource = ds;
                        GVAddCat.DataBind();
                    }

                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void GridViewSearchPartwise()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBTrueVoter"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("VoterCountSurveyCount");
                    // select ACNO, PART_NO, Count(*)SurveyCount from[DBTrueVoter].[dbo].[tbl_SurveyVoterDetails] group by ACNO,PART_NO
                    cmd.Connection = DataBaseConnection;

                    DataBaseConnection.Open();

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AC", ddlShop.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@PARTNO", txtpartno.Text);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddCat.DataSource = ds;
                        GVAddCat.DataBind();
                    }

                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            GridViewSearchPartwise();
        }



    }

}