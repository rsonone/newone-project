﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="Dashboardcount.aspx.cs" Inherits="NeedlyApp.Admin.Dashboardcount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Dashboard Count <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                      </div>
                    </div>
                  </div>
                <div class="row">
            <div class="panel-body">
                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Mobile No: </label>
                               <div class="col-sm-9">
                                    <asp:TextBox ID="txtmobileno"  CssClass="form-control"  PlaceHolder="Enter Mobile Number" runat="server"></asp:TextBox>
                                                        
                                                                 </div></div></div>
                                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <asp:Button ID="btnsearch" runat="server"  class="btn btn-primary" OnClick="btnsearch_Click" Text="Search Count"  />
                             
                                    </div>
               </div>
                    <div class="box-body col-sm-6">

                    </div>
                </div>
               </div>
                      </div>
                </div>
                    </div>
               
              
              
                <div class="content-wrapper">
                     <section class="content">
              <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3> <asp:Label ID="lblfullname" runat="server"></asp:Label></h3>

              <p>FullName</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lblstate" runat="server"></asp:Label></h3>

              <p>State</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lbldistrict" runat="server"></asp:Label></h3>

              <p>District</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><asp:Label ID="lbltaluka" runat="server"></asp:Label></h3>

              <p>Taluka</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

                   
      </div>
              </section>
                </div>

                <br />
     <div class="content-wrapper">
       
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>
                <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lblTodaycount"  runat="server"></asp:Label></h3>

              <p>Whatsapp Message Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="WhatsappMsgCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lbltodayregistration" runat="server"></asp:Label></h3>

              <p>Media Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="MediaCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><asp:Label ID="lblTotalRegistration" runat="server"></asp:Label></h3>

              <p>Template Share Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="TemplateShareCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><asp:Label ID="lblSharedCnt" runat="server"></asp:Label></h3>

              <p>Needly Project Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="NeedlyProjectCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

                     <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lbldemandjobcount" runat="server"></asp:Label></h3>

              <p>Demand Job Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="DemandJobCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

                      <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblchildcount" runat="server"></asp:Label></h3>

              <p>Child Added in Child Safety Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="ChildAddedinChildSafetyCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </section>
              </div>
    <br />
               <div class="content-wrapper">
          <section class="content">
              <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3> <asp:Label ID="lblautoreplykeywordcount" runat="server"></asp:Label></h3>

              <p>Total WA Autoreply Keyword count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="TotalWAAutoreplyKeywordcount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lblautoreplywausedcount" runat="server"></asp:Label></h3>

              <p>Total WAB Autoreply Keyword count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="TotalWABAutoreplyKeywordcount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblautoreplytelusedcount" runat="server"></asp:Label></h3>

              <p>Telegram Autoreply Keyword count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="TelegramAutoreplyKeywordcount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><asp:Label ID="lbldistributedcouponcodecount" runat="server"></asp:Label></h3>

              <p>Distributed Coupon Code Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="DistributedCouponCodeCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

                     <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><asp:Label ID="lblneedlygrpcount" runat="server"></asp:Label></h3>

              <p>Needly Group Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="NeedlyGroupCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

                    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><asp:Label ID="lbldailybannercount" runat="server"></asp:Label></h3>

              <p>Daily Banner Used Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="DailyBannerUsedCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </section>
              </div>
    <br />
     <div class="content-wrapper">
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>
                <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lblneedlymembergroupcount" runat="server"></asp:Label></h3>

              <p>Needly Member Group Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="NeedlyMemberGroupCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblallotedcouponcodecount" runat="server"></asp:Label></h3>

              <p>Alloted Coupon Code Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="AllotedCouponCodeCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><asp:Label ID="lblreferralcount" runat="server"></asp:Label></h3>

              <p>Total Refferal Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="TotalRefferalCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><asp:Label ID="lbltemplatecount" runat="server"></asp:Label></h3>

              <p>Total Template Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="TotalTemplateCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

                      <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3> <asp:Label ID="lblprofileurlname" runat="server"></asp:Label></h3>

              <p>Website Created or Not</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="WebsiteCreation.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lblappusedate" runat="server"></asp:Label></h3>

              <p>Last App Used Date</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </section>
              </div>
    <br />
               <div class="content-wrapper">
          <section class="content">
              <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3> <asp:Label ID="lblmergedtempcount" runat="server"></asp:Label></h3>

              <p>Total Merged Template count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="TotalMergedTemplatecount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lbldoctempcount" runat="server"></asp:Label></h3>

              <p>Total Document Template count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="TotalDocumentTemplatecount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblnormaltempcount" runat="server"></asp:Label></h3>

              <p>Total Normal Template count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="TotalNormalTemplatecount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><asp:Label ID="lblvideotempcount" runat="server"></asp:Label></h3>

              <p>Total Video Template Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="TotalVideoTemplateCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


                    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblappversion" runat="server"></asp:Label></h3>

              <p>App Version</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="AppInformation.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><asp:Label ID="lblappinstallationdate" runat="server"></asp:Label></h3>

              <p>App Installation Date</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </section>
              </div>
               <br />
                <div class="content-wrapper">
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>
                <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lblimagetempcount" runat="server"></asp:Label></h3>

              <p>Total Image Template Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="TotalImageTemplateCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblschedulemsgcount" runat="server"></asp:Label></h3>

              <p>Total Schedule Message Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="TotalScheduleMessageCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><asp:Label ID="lblneedlyfamilycount" runat="server"></asp:Label></h3>

              <p>Needly Family Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="NeedlyFamilyCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><asp:Label ID="lblwebsitevisitcount" runat="server"></asp:Label></h3>

              <p>Website Visit Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="WebsiteCreation.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>


                       <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lblpostjobcount" runat="server"></asp:Label></h3>

              <p>Post Job Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="PostJobCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
                     <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lbluserrole" runat="server"></asp:Label></h3>

              <p>User Role</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

      </div>
              </section>
              </div>
    <br />
<div class="content-wrapper">
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>
                <div class="row">
        <div class="col-lg-2 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lblneedlysoscount" runat="server"></asp:Label></h3>

              <p>Needly SOS Referral Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="NeedlySOSReferralCount.aspx" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
              <div class="row">
              <div class="col-lg-12">
                      
                           <div class="pull-right">
              

                             <a href="Dashboardcount1.aspx"  title="More Count" class="btn btn-secondary">More Count</a>
                          
                        </div>
                         
                      </div>  
                  </div>

</asp:Content>
