﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AddRefferal.aspx.cs" Inherits="NeedlyApp.Admin.AddRefferal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>
    $(function () {
        $(".phonecell").click(function () {
            var PhoneNumber = $(this).text();
            PhoneNumber = PhoneNumber.replace("Phone:", "");
            window.location.href = "tel://" + PhoneNumber;
            
        });
    });
        
    </script>
      <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add Referal <small></small></h2>
           <div class="clearfix"></div>
                       <div class="container-fluid">
                        
                            
                          <div class="col-md-12">
                   
                    </div>
               
                      
                     </div>
          
                  
          
                  </div>
   

    <asp:Panel runat="server" ID="addreferal" Visible="true">
           <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <asp:RadioButtonList ID="rdSelect" runat="server"  RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem   Value="1">&nbsp;&nbsp;Marketing Person &nbsp;&nbsp;</asp:ListItem>
                                <asp:ListItem  Value="2">&nbsp;&nbsp;Customer &nbsp;&nbsp;</asp:ListItem>
                              
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
        <br >
        <br >
        <div>
              <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Mobile Number
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtmobNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                               <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            User Name
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
        </div>
           <br >
        <br >
         <br >
        <br >

              <div>
                    <div class="panel-body">                                     
               <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                     <div class="box-body col-sm-offset-2  col-sm-4">
                           <asp:linkbutton id="lnkbtndownload" runat="server" text="click here!!! download excel format." font-bold="true" font-size="medium" onclick="lnkbtnDownload_Click"   forecolor="red"></asp:linkbutton>
                          </div>
                    </div>
    
                    </div>

              </div>


          <div class="col-md-12">
                     <div class="box-body col-sm-5">
                        
                    </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" class="btn btn-success"  />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false"  />
                        </div>
                 </div>

          <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVAddreferal" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVAddreferal_PageIndexChanging"  
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" OnRowDataBound="GVAddreferal_RowDataBound" >
                    <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                         --%>
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="Customer_Mobile" HeaderText=" Customer_Mobile" ItemStyle-ForeColor="DarkBlue">
                         <ControlStyle CssClass="phonecell"></ControlStyle>
                        </asp:BoundField>

                    <asp:HyperLinkField DataTextField="Customer_Mobile" HeaderText="Customer_Mobile" target="_blank" DataNavigateUrlFields="Customer_Mobile" DataNavigateUrlFormatString="https://needly.in/Admin/Dashboardcount.aspx?Customer_Mobile={0}" Text="Customer_Mobile" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server"   NavigateUrl=' <%Bind("Customer_Mobile") + Request.QueryString("Customer_Mobile")%> ' Text=""></asp:HyperLink>
                </ItemTemplate>
             </asp:TemplateField>

                         <asp:BoundField DataField="Customer_Name" HeaderText="Customer_Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        
                         <asp:BoundField DataField="Type" HeaderText="Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        
                        
                      
                       
                         <asp:TemplateField HeaderText="Action"  >
                             <ItemTemplate>
                                 <asp:Button ID="btnDelete" runat="server" CssClass=" btn-danger" Text="Delete" />
                                   
                             </ItemTemplate>
                         </asp:TemplateField>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
         

    </asp:Panel>
</asp:Content>
