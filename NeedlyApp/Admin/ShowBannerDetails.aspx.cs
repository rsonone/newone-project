﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class ShowBannerDetails : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
                BindState();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowBannerDetails");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@State",ddlState.SelectedValue);
                    cmd.Parameters.AddWithValue("@District", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    GVBanner.DataSource = ds;
                    GVBanner.DataBind();
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlState.DataSource = ds.Tables[0];
                    ddlState.DataTextField = "STATE_NAME";
                    ddlState.DataValueField = "MDDS_STC";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlState.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddlDistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }
        protected void GVBanner_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }
        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GridView();
        }
    }
}