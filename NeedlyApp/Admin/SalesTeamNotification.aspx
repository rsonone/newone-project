﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="SalesTeamNotification.aspx.cs" Inherits="NeedlyApp.Admin.SalesTeamNotification" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Sales Team Notification <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Sales Person Name: </label>
                                <div class="col-sm-9">
                                        <asp:TextBox ID="txtName" runat ="server" CssClass="form-control"></asp:TextBox>
         
                                <%--   <asp:DropDownList ID="ddlstate" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                  </asp:DropDownList><br />--%>  
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Address: </label>
                              <div class="col-sm-9">
                                       <asp:TextBox ID="txtAddress" runat ="server" CssClass="form-control"></asp:TextBox>
         
                                <%-- <asp:DropDownList ID="ddldistrict"  CssClass="form-control" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddldistrict_SelectedIndexChanged" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>--%>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    MobileNo: </label>
                                <div class="col-sm-9">
                                         <asp:TextBox ID="txtmobile" runat ="server" MaxLength="15" CssClass="form-control"></asp:TextBox>
         
                                   <%--<asp:DropDownList ID="ddlTaluka" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList><br />--%>
                          </div></div></div>

                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Under Sales Head: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtHead" runat ="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div> 

                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Sequence No: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtSequenceNo" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Capacity: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtCapacity" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>
                    </div>
               </div></div> 
                     

<%--                                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                Total Assiged Status: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtAssigedStatus" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Current Assignment Status: </label>
                              <div class="col-sm-9">
                                <asp:RadioButtonList ID="rdAssign" runat="server"   RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Active&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;DeActive&nbsp;&nbsp;</asp:ListItem>
                                    
                                </asp:RadioButtonList>          </div></div></div>
                    </div>
               </div></div> 
                    --%>
                                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                               In Working: </label>
                              <div class="col-sm-9">
                                   <asp:RadioButtonList ID="rdbWork" runat="server"   RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Yes&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="0">&nbsp;&nbsp;No&nbsp;&nbsp;</asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          <%--      <asp:TextBox ID="TextBox1" MaxLength="5" runat ="server" CssClass="form-control"></asp:TextBox>--%>
                           </div></div></div>

                   
                    </div>
               </div></div> 
      <br />
                    <br />
                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblId" Visible="false" runat="server"></asp:Label>  

                        </div>
                         <div class="box-body col-sm-4">
                                  <asp:Button ID="btnCancle" runat="server" Text="Reset" class="btn btn-success" OnClick="btnCancle_Click" />
                             <asp:Button ID="btnSend" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSend_Click1" />
                          
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" Visible="false" class="btn btn-success" OnClick="btnUpdate_Click" />
                          
                        </div>
                       
                    </div>
                     <div class="row">
                <div style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                    <div >
                        <asp:Label ID="lblTotalCount" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp
                        <asp:Label ID="lblCuntDisplay" runat="server" Text="" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>
                        <br />
                    </div>
                 </div>
                         </div>
                         <div class="row">
                   <div class="col-md-12">
                     <div class="table-responsive">
            
                    <asp:GridView ID="gvAdvCourse" runat="server" OnPageIndexChanging="gvAdvCourse_PageIndexChanging" 
                          PagerStyle-VerticalAlign="Middle" HorizontalAlign="Center"  AutoGenerateColumns="False" Font-Names="Arial" Font-Size="11pt"  AllowPaging="True" ShowFooter="True  " 
                             PageSize="20" BackColor="White" BorderColor="#000000" BorderStyle="None" BorderWidth="1px" Width="100%"  Height="20px"  >
                        <Columns> 
       
                                <asp:BoundField DataField="id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SalesPersonName" HeaderText="Sales Person Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Address" HeaderText="Address">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                           
                                <asp:BoundField DataField="MobileNo" HeaderText="MobileNo">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                              <asp:BoundField DataField="UnderSalesHead" HeaderText="Under Sales Head">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="SequenceNo" HeaderText="Sequence No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Capacity" HeaderText="Capacity">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>

                            
                                <asp:BoundField DataField="TotalAssignment" HeaderText="Total Assignment">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                              <asp:BoundField DataField="CurrentAssignment" HeaderText="Current Assignment">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CurrentAssignmentStatus" HeaderText="Current Assignment Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="InWorking" HeaderText="InWorking">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>

                              <asp:TemplateField HeaderText="Update"  >
                                                            <ItemTemplate>
                                                                <asp:Button ID="lnkbtnupdate" CssClass="btn btn-round btn-success" runat="server" CommandArgument='<%#Bind("id")%>'
                                                                    CommandName="Modify" OnClick="lnkbtnupdate_Click" Text="Edit"></asp:Button>
                                                                   <asp:Label ID="lblId1" runat="server" Text='<%#Eval("id") %>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                <asp:TemplateField HeaderText="Active">
                                     <ItemTemplate>
                                          <asp:Button ID="lnkProfile" CssClass="btn btn-round btn-warning" runat="server" Text="Delete" OnClick="lnkProfile_Click" ></asp:Button>
                                          <asp:Label ID="lblView" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                                     </ItemTemplate>
                                </asp:TemplateField> 
                        </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                </div>
                       <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                       </div>

                       </div>

                   

                  </div>
                </div></div>
    </asp:Content>
