﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="RegistrationReport.aspx.cs" Inherits="NeedlyApp.Admin.RegistrationReport" %>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Registration Details<small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        State:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>

                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        District:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlDistrict" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Taluka:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlTaluka" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>
                          <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        User Role:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlRole" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                             <asp:ListItem Text="Customer" Value="1"></asp:ListItem>
                                             <asp:ListItem Text="Shopkeeper" Value="2"></asp:ListItem>
                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Last Digit:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddldigit" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                             <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                             <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                             <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                             <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                             <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                             <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                             <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                             <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                             <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                             <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>

                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="btnSearch" runat="server" class="btn btn-success" OnClick="btnSearch_Click" Text="Search" />&nbsp;&nbsp;
                        <asp:Button ID="btnExcel" runat="server" Visible="false" class="btn btn-success" OnClick="btnExcel_Click" Text="Export To Excel" />
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </div>
                </div>

                 <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label4" runat="server">Count:</asp:Label>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
                 <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="btnsearchbyname" CssClass=" btn-primary" OnClick="btnsearchbyname_Click"  runat="server" Text="Search By Name" />
                    </div>
                    <div class="box-body col-sm-3">
                        <asp:Button ID="btnRefresh" OnClick="btnRefresh_Click" CssClass="btn-success" runat="server" Text="Refresh" />
                    </div>
                </div>
                <br />
                <br />
                 <div class="col-md-12">
                     <br />
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report"  runat="server" OnPageIndexChanging="GVReg_Report_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:BoundField DataField="FullName" HeaderText="FullName">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="firmName" HeaderText="Firm Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="mobileNo" HeaderText="Mobile No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="EntryDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="State" HeaderText="State">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="District" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="UserRole" HeaderText="UserRole">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                   <br /> 
                  <div class="col-md-12">
                      <br />
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report1"  runat="server"  OnPageIndexChanging="GVReg_Report1_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                          <%-- <asp:BoundField DataField="Id" HeaderText="Id">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>--%>
                        <asp:BoundField DataField="FullName" HeaderText="FullName">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="firmName" HeaderText="Firm Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="mobileNo" HeaderText="Mobile No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="EntryDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="State" HeaderText="State">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="District" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="RoleID" HeaderText="RoleID">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                  
            </div>
        </div>
    </div>
</asp:Content>

