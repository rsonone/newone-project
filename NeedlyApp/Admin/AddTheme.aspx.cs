﻿using DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddTheme : System.Web.UI.Page
    {
        string dirPath = string.Empty;
        string fullName = string.Empty;
        string base64String = string.Empty;
        CommonCode cc = new CommonCode();

        string DataCode = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowTheme");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddTemplate.DataSource = ds.Tables[0];
                        GVAddTemplate.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }





        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    string ImageNew = string.Empty;
                    string imageUrl1 = string.Empty;
                if ((FileUpload1.HasFile))
                {

                    string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);

                    FileUpload1.SaveAs(Server.MapPath("~/img/" + filename));
                    string filepath = "img/" + filename;

                    ImageNew = "https://needly.in/" + filepath.ToString();

                }



                SqlCommand cmd = new SqlCommand("SP_uploadAddTheme1");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    //cmd.Parameters.AddWithValue("@Id", ViewState["DataCode"].ToString());
                    cmd.Parameters.AddWithValue("@Image", ImageNew);
                    cmd.Parameters.AddWithValue("@Desription", txtdescription.Text);


                    //cmd.Parameters.AddWithValue("@XCoordinate", txtXCoordinate.Text);
                    //cmd.Parameters.AddWithValue("@YCoordinate", txtYCoordinate.Text);
                    //cmd.Parameters.AddWithValue("@Height", txtHeight.Text);
                    //cmd.Parameters.AddWithValue("@Width", txtWidth.Text);
                    //cmd.Parameters.AddWithValue("@Size", txtSize.Text);

                    //cmd.Parameters.AddWithValue("@ShopkeeperName", txtSkName.Text);
                    //cmd.Parameters.AddWithValue("@ShopkeeperEmail", txtSkEmail.Text);

                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());


                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
    }
}