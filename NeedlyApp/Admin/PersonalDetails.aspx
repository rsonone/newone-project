﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="PersonalDetails.aspx.cs" Inherits="NeedlyApp.Admin.PersonalDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Personal Details<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                    Mobile number: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtmobileno" CssClass="form-control" placeholder="Enter Your MobileNo" OnTextChanged="txtmobileno_OnTextChanged"  AutoPostBack="true" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Name: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtname" CssClass="form-control" placeholder="Enter Your Name"  runat="server"></asp:TextBox> 

                          </div></div></div>
                    </div>
               </div>
                         </div>
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Age: </label>
                                <div class="col-sm-6">
                                <asp:TextBox ID="txtage" CssClass="form-control" placeholder="Enter Your Age"  runat="server"></asp:TextBox>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Chest (cm): </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtchest" CssClass="form-control"  placeholder="Enter Your Chest" runat="server"></asp:TextBox>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Height (cm): </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtheight" CssClass="form-control" placeholder="Enter Your Height"  runat="server"></asp:TextBox>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                Weight(Kg): </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtweight" CssClass="form-control"  placeholder="Enter Your Weight" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Caste: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtcaste" CssClass="form-control"  placeholder="Enter Your Caste" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        Category  : </label>
                                <div class="col-sm-6">
                                 <asp:DropDownList ID="ddlcategory" CssClass="form-control"  runat="server" AutoPostBack="true"  >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList>




                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        Education level  : </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rbtneducationlevel" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="NonMatric&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                          <asp:ListItem Text="Matric&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Higher Secondary&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Diploma&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Graduate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Post Graduate&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" Value="5"></asp:ListItem>
                                    </asp:RadioButtonList>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        Educational Details  : </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtedudetails" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        State  : </label>
                                <div class="col-sm-6">
                                 <asp:DropDownList ID="ddlState" CssClass="form-control" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"  runat="server" AutoPostBack="true"  >
                                 <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        District  : </label>
                                <div class="col-sm-6">
                                 <asp:DropDownList ID="ddlDistrict"  OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true"  runat="server"  >
                               <asp:ListItem Value="" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>


                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        Taluka  : </label>
                                <div class="col-sm-6">
                                 <asp:DropDownList ID="ddlTaluka"   CssClass="form-control" AutoPostBack="true"  runat="server"  >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>


                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        City/Village  : </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtcity" CssClass="form-control"  runat="server"></asp:TextBox>


                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        Detail Address  : </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtaddress" CssClass="form-control"  runat="server"></asp:TextBox>


                          </div></div></div>
                    </div>
               </div>
                         </div>


                     
                     <div class="col-md-12">
           <div class="box-body col-sm-5">
                         
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" OnClick="btnUpdate_Click"  />
           </div>


        </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddQues"  runat="server"  OnPageIndexChanging="GVAddQues_PageIndexChanging"  PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%>    
                           <asp:BoundField DataField="Id" HeaderText="ID">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="Mobilenumber" HeaderText="Mobile No.">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Name" HeaderText="Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Age" HeaderText="Age">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Chest" HeaderText="Chest">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Height" HeaderText="Height">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Weight" HeaderText="Weight">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Caste" HeaderText="Caste">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Educationlevel" HeaderText="Education Level">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Educationaldetails" HeaderText="Education Details">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="State" HeaderText="State">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="City" HeaderText="City">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Detailaddress" HeaderText="Address">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="Created By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                         <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>

                    </div>
                  </div>
                </div>
</asp:Content>
