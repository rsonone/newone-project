﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class TrueVoterUserDetails : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_DownloadTrueVoterUserDetails");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVTruevoterUserDetails.DataSource = ds;
                        GVTruevoterUserDetails.DataBind();
                    }
                    string SQl1Count = "select count(*) from [tbl_TrueVoterUserDetails]";
                    string cnt = Convert.ToString(cc.ExecuteScalar(SQl1Count));

                    
                    lblTrueVoterCnt.Text = cnt;
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void GVTruevoterUserDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVTruevoterUserDetails.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void btnExportData_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "TruVoter_User_Details.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVTruevoterUserDetails.AllowPaging = false;
            GVTruevoterUserDetails.PageIndex = 0;

            GridView();
            //Change the Header Row back to white color
            GVTruevoterUserDetails.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVTruevoterUserDetails.HeaderRow.Cells.Count; i++)
            {
                GVTruevoterUserDetails.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVTruevoterUserDetails.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //required to avoid the runtime error "  
            //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
        }
    }
}