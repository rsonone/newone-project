﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="QuotationReport.aspx.cs" Inherits="NeedlyApp.Admin.QuotationReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="clearfix"></div>
        <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Quotations Report <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                 <a href="CreateQuotation.aspx" data-toggle="tooltip" title="Quotation Report" class="btn btn-success"> Go To Create Quotation Page</a>
                            <a href="QuotationReport.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>

     <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-name2">
                                    Quotations Report: </label>
                                <div class="col-sm-10">
                                <asp:RadioButtonList ID="rdbQotation" runat="server" OnSelectedIndexChanged="RadioButtonList1_OnSelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Date Wise Report&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Quotation Number Wise Report&nbsp;&nbsp;</asp:ListItem>
                                      <asp:ListItem Value="3">&nbsp;&nbsp;Customer Wise Report&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                          </div>

                         </div></div>
                    </div>
               </div>
     <asp:Panel runat="server" ID="PanelDateWise" Visible="false"> 
      <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
              
                     <div class="row">
        </div> 

                     <asp:Panel ID="PnlCat" runat="server" Visible="true">
                         <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                                     <div class="box-body col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                           From Date
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtDate1" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                      <div class="box-body col-sm-5">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            To Date
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtDate2" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                      <div class="box-body col-sm-2">
                              <asp:Button ID="btnshow" runat="server" OnClick="btnshow_OnClick" Text="Show" class="btn btn-success"  />
                              
                        </div>


                            </div>
                        </div></div> 
                    </asp:Panel>

                      <div class="panel-body">
                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                               
                            </div>
                            <div class="box-body col-sm-6">
                                <div class="box-body col-sm-offset-4  col-sm-8">
                                      <asp:Button ID="Button1" runat="server" Text="Export To Excel" OnClick="btnExcel_Click0" class="btn btn-success"  />
                                </div>
                            </div>
                        </div>
                    </div>
                   
                       <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4" style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" ForeColor="Blue" Font-Size="20px"><b>Count:</b></asp:Label>
                        <asp:Label ID="Label2" runat="server" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>

                      <div class="col-md-12">
                        <div class="table-responsive">
                            <asp:GridView ID="GvQuotationDate" runat="server" PagerStyle-VerticalAlign="Middle" OnPageIndexChanging="GvQuotationDate_PageIndexChanging" 
                                PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered">
                                <Columns>
                                    <%--<asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <%--<asp:BoundField DataField="Sr.No" HeaderText="Sr.No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>--%>
                                    <asp:BoundField DataField="Quotation_No" HeaderText="Quotation No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Quotation_Grp_Name" HeaderText="Quotation Group Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Customer_Name" HeaderText="Customer Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="Item_Name" HeaderText="Product">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Price" HeaderText="Rate">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Unit" HeaderText="Unit">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                     <asp:BoundField DataField="Amount" HeaderText="Total Amount">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                   

                                </Columns>
                                <HeaderStyle BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                            </asp:GridView>
                        </div>
                    </div>

                   
                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                    
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                   
           

                    
                  </div>
                </div></div>
         </asp:Panel>

    <asp:Panel ID="PanelNumberWise" runat="server" visible="false">
         <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Quotations Number
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtquotationNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                              <asp:Button ID="NumberShow" runat="server" Text="Show" class="btn btn-success"  />
                              
                        </div>

                            </div>


                              <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                               
                            </div>
                            <div class="box-body col-sm-6">
                                <div class="box-body col-sm-offset-4  col-sm-8">
                                      <asp:Button ID="Button2" runat="server" Text="Export To Excel" OnClick="btnExcel_Click1" class="btn btn-excel"  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                              <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4" style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                        <asp:Label ID="Label3" runat="server" Font-Bold="true" ForeColor="Blue" Font-Size="20px"><b>Count:</b></asp:Label>
                        <asp:Label ID="Label4" runat="server" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
                      <div class="col-md-12">
                        <div class="table-responsive">
                            <asp:GridView ID="NumberGrid" runat="server" PagerStyle-VerticalAlign="Middle" OnPageIndexChanging="NumberGrid_PageIndexChanging"
                                PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered">
                                <Columns>
                                    
                                     <asp:BoundField DataField="Quotation_No" HeaderText="Quotation No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Quotation_Grp_Name" HeaderText="Quotation Group Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Customer_Name" HeaderText="Customer Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="Item_Name" HeaderText="Product">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Price" HeaderText="Rate">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Unit" HeaderText="Unit">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                     <asp:BoundField DataField="Amount" HeaderText="Total Amount">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    

                                </Columns>
                                <HeaderStyle BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                            </asp:GridView>
                        </div>
                    </div>
                        </div>
                    </div>
    </asp:Panel>

    <asp:Panel ID="PanelCustomer" runat="server" visible="false" >
        <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Customer Name
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlCustomerName" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--"></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                           
                                    </div>
                                    
                                </div>
             <div class="box-body col-sm-6">
                              <asp:Button ID="CustomerShow"   runat="server" Text="Show" class="btn btn-success"  />
                              
                        </div>
             <div class="col-md-12">
                            <div class="box-body col-sm-6">
                               
                            </div>
                            <div class="box-body col-sm-6">
                                <div class="box-body col-sm-offset-4  col-sm-8">
                                      <asp:Button ID="CustExcel" runat="server" Text="Export To Excel" OnClick="btnExcel_Click2"  class="btn btn-excel"  />
                                </div>
                            </div>
                        </div>

                             
                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4" style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                        <asp:Label ID="Label5" runat="server" Font-Bold="true" ForeColor="Blue" Font-Size="20px"><b>Count:</b></asp:Label>
                        <asp:Label ID="Label6" runat="server" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
              <div class="col-md-12">

                 
                        <div class="table-responsive">
                            <asp:GridView ID="CustomerGrid" runat="server" PagerStyle-VerticalAlign="Middle" OnPageIndexChanging="CustomerGrid_PageIndexChanging"
                                PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered">
                                <Columns>
                                    
                                     <asp:BoundField DataField="Quotation_No" HeaderText="Quotation No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Quotation_Grp_Name" HeaderText="Quotation Group Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Customer_Name" HeaderText="Customer Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="Item_Name" HeaderText="Product">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Price" HeaderText="Rate">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Unit" HeaderText="Unit">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                     <asp:BoundField DataField="Amount" HeaderText="Total Amount">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    

                                </Columns>
                                <HeaderStyle BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                            </asp:GridView>
                        </div>
                    </div>
                            </div>
    </asp:Panel>
</asp:Content>
