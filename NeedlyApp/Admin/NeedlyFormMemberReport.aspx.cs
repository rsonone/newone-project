﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class NeedlyFormMemberReport : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                GridView();
                DropFormName();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadFormMembers1");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVFormMemberReport.DataSource = ds;
                        GVFormMemberReport.DataBind();
                    }
                    string SQl1Count = "select count(*) from [tbl_FormMembers]";
                    string cnt = Convert.ToString(cc.ExecuteScalar(SQl1Count));


                    lblFormMembercnt.Text = cnt;
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void GVFormMemberReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVFormMemberReport.PageIndex = e.NewPageIndex;
            if (!IsPostBack)
            {

                GridView();
            }
            else
            {
                GridViewForSearch();
            }
          
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GridViewForSearch();
        }
        public void GridViewForSearch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadFormMembersNew");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CreatedBy", txtMobileNo.Text.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVFormMemberReport.DataSource = ds;
                        GVFormMemberReport.DataBind();
                    }
                    string SQl1Count = "select count(*) from [tbl_FormMembers] where CreatedBy = '"+ txtMobileNo.Text.Trim()+"'";
                    string cnt = Convert.ToString(cc.ExecuteScalar(SQl1Count));


                    lblFormMembercnt.Text = cnt;
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void DropFormName()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadFormName");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlFormname.DataSource = ds.Tables[0];
                        ddlFormname.DataTextField = "Form_Name";
                        //ddlFormname.DataValueField = "Id";

                        ddlFormname.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlFormname.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlFormname.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlFormname.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        protected void btnSearchFormname_Click(object sender, EventArgs e)
        {
            GridViewForSearchFormName();
        }
        public void GridViewForSearchFormName()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadFormName");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormName", ddlFormname.SelectedItem.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVFormMemberReport.DataSource = ds;
                        GVFormMemberReport.DataBind();
                    }
                    //string SQl1Count = "select count(*) from [tbl_FormMembers] where CreatedBy = '" + txtMobileNo.Text.Trim() + "'";
                    //string cnt = Convert.ToString(cc.ExecuteScalar(SQl1Count));


                    //lblFormMembercnt.Text = cnt;
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

    }
}