﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="CustContact_ExcelUpload.aspx.cs" Inherits="NeedlyApp.Admin.CustContact_ExcelUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Customer  Contact Excel Uplaod <small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="panel-body">
                        <%--<div class="col-md-12">--%>
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Mobile Number:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body col-sm-offset-2  col-sm-4">
                                <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." OnClick="lnkbtnDownload_Click" Font-Bold="true" Font-Size="Medium" ForeColor="Red"></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                             <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Upload Excel:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:FileUpload ID="Fileupload1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />

                        <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Lblerrer" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box-body col-sm-offset-2  col-sm-4">
                        <asp:Label ID="Label1" runat="server" Text="Count" Visible="false" Font-Bold="true"></asp:Label>
                        <asp:Label ID="lalCount" runat="server" Font-Bold="true"></asp:Label>
                        <br />
                        <asp:Label ID="lblMessage" runat="server" Font-Bold="true"></asp:Label>
                    </div>
                </div>

            </div>
        </div>
   
</asp:Content>
