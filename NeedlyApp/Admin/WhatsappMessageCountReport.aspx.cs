﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class WhatsappMessageCountReport : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Gridview();
                BindState();
            }
        }

        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlState.DataSource = ds.Tables[0];
                    ddlState.DataTextField = "STATE_NAME";
                    ddlState.DataValueField = "MDDS_STC";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlState.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", ""));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddlDistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", ""));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }
        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        protected void GVReg_Report_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report.PageIndex = e.NewPageIndex;
            this.Gridview();
        }
        public void Gridview()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadWhatsappMsgCountReport");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@State", ddlState.SelectedValue);
                    cmd.Parameters.AddWithValue("@District", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);
                    cmd.Parameters.AddWithValue("@Digit", ddldigit.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReg_Report.DataSource = ds;
                        GVReg_Report.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());
                    }
                    //Roshan Sonone

                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int sum = Convert.ToInt32(dt.Compute("SUM(WhatsAppMsgCount)", string.Empty));
                    Label6.Text = sum.ToString();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
           
            Gridview();
            GVReg_Report1.Visible = false;
            GVReg_Report3.Visible = false;
            GVReg_Report.Visible = true;
            
        }
        public void GridviewSearchbyName()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchbyNameWhatsAppMsgCount");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@name", txtName.Text);
                    

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReg_Report1.DataSource = ds;
                        GVReg_Report1.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());
                    }

                    //Roshan Sonone

                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int sum = Convert.ToInt32(dt.Compute("SUM(WhatsAppMsgCount)", string.Empty));
                    Label6.Text = sum.ToString();

                    // DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnsearchbyname_Click(object sender, EventArgs e)
        {

            GridviewSearchbyName();
            GVReg_Report.Visible = false;
            GVReg_Report3.Visible = false;
            GVReg_Report1.Visible = true;

           
        }
        public void GridviewSearchbyMobileNo()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchbyMobileNoWhatsAppMsgCount");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Mobileno", txtMobileNo.Text);


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReg_Report3.DataSource = ds;
                        GVReg_Report3.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());

                    }

                    //Roshan Sonone

                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int sum = Convert.ToInt32(dt.Compute("SUM(WhatsAppMsgCount)", string.Empty));
                    Label6.Text = sum.ToString();

                    // DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnsearchbymobileno_Click(object sender, EventArgs e)
        {

            GridviewSearchbyMobileNo();
            GVReg_Report.Visible = false;
            GVReg_Report1.Visible = false;
            GVReg_Report3.Visible = true;

        }
        protected void GVReg_Report1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report1.PageIndex = e.NewPageIndex;
            this.GridviewSearchbyName();
        }
        protected void GVReg_Report3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report3.PageIndex = e.NewPageIndex;
            this.GridviewSearchbyMobileNo();
        }
    }
}