﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Linq;
using System.Text;
using System.Web;

using System.Web.UI.WebControls;




namespace NeedlyApp.Admin
{
    public partial class Home : System.Web.UI.Page
    {
        DataSet ObjDataSet = new DataSet();
        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        CommonCode EL = new CommonCode();
        string mob = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                datadashbord();
                BindChart();
                BindChartbar();
                BindChart1();
               // BindChartbarpassf();
                mob = Convert.ToString(Session["username"]);

                //if (!IsPostBack)
                //{
                //    BindChart();
                //    // dashboard();
                //    // UserMaster();
                //}
            }
        }

        public void BindChart()
        {
            DataTable dsChartData = new DataTable();
            StringBuilder strScript = new StringBuilder();

            try
            {
                dsChartData = GetChartData();

                strScript.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']}); </script>  
                      
                    <script type='text/javascript'>  
                     
                    function drawChart() {         
                    var data = google.visualization.arrayToDataTable([  
                     ['DateValue', 'Registration'],");

                foreach (DataRow row in dsChartData.Rows)
                {
                    strScript.Append("['" + row["DateValue"] + "'," + row["Registration"] + "],");

                }
                strScript.Remove(strScript.Length - 1, 1);
                strScript.Append("]);");

                strScript.Append(@" var options = {     
                                    title: 'Date wise App Installation Count-Line Graph',            
                                    is3D: true,          
                                    };   ");

                strScript.Append(@"var chart = new google.visualization.AreaChart(document.getElementById('piechart_3d'));          
                                chart.draw(data, options);        
                                }    
                            google.setOnLoadCallback(drawChart);  
                            ");
                strScript.Append(" </script>");

                ltScripts.Text = strScript.ToString();
            }
            catch
            {
            }
            finally
            {
                dsChartData.Dispose();
                strScript.Clear();
            }
        }


        public DataTable GetChartData()
        {
            DataSet dsData1 = new DataSet();
            try
            {
                SqlCommand cmd1 = new SqlCommand("SP_GetAppinstallation_Score");
                cmd1.Connection = con;
                cmd1.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da1 = new SqlDataAdapter();
                da1.SelectCommand = cmd1;
                DataSet ds1 = new DataSet();
                da1.Fill(dsData1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dsData1.Tables[0];
        }


        public void BindChart1()
        {
            DataTable dsChartData1 = new DataTable();
            StringBuilder strScript1 = new StringBuilder();

            try
            {
                dsChartData1 = GetChartDatapin();

                strScript1.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']}); </script>  
                      
                    <script type='text/javascript'>  
                     
                    function drawChart() {         
                    var data = google.visualization.arrayToDataTable([  
                    ['Status', 'Hours of Day'],");

                foreach (DataRow row in dsChartData1.Rows)
                {
                    strScript1.Append("['" + row["AppInstallation"] + "'," + row["count1"] + "],");
                }
                strScript1.Remove(strScript1.Length - 1, 1);
                strScript1.Append("]);");

                strScript1.Append(@" var options = {     
                                    title: 'App Installation-Pie Graph',            
                                    is3D: true,          
                                    };   ");

                strScript1.Append(@"var chart = new google.visualization.PieChart(document.getElementById('piechart_3d1'));          
                                chart.draw(data, options);        
                                }    
                            google.setOnLoadCallback(drawChart);  
                            ");
                strScript1.Append(" </script>");


               

                ltScriptspin.Text = strScript1.ToString();
            }
            catch
            {
            }
            finally
            {
                dsChartData1.Dispose();
                strScript1.Clear();
            }
        }
        public DataTable GetChartDatapin()
        {
            DataSet dsDatap = new DataSet();
            try
            {
                SqlCommand cmdp = new SqlCommand("SP_GetbarDashboard_Score");//  sp_schooledistrictcode1
                cmdp.Connection = con;
                cmdp.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter dap = new SqlDataAdapter();
                dap.SelectCommand = cmdp;
                DataSet ds = new DataSet();
                dap.Fill(dsDatap);
            }
            catch
            {
                throw;
            }
            return dsDatap.Tables[0];
        }

        public DataTable GetChartDatabar()
        {
            DataSet dsData = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("SP_GetbarDashboard_usertype");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(dsData);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dsData.Tables[0];
        }
        public void BindChartbar()
        {

            DataTable ChartDatabar = new DataTable();
            StringBuilder strScriptbar = new StringBuilder();

            try
            {
                ChartDatabar = GetChartDatabar();

                strScriptbar.Append(@"<script type='text/javascript'>  
                    google.load('visualization', '1', {packages: ['corechart']});</script>  
  
                    <script type='text/javascript'>  
                    function drawVisualization() {         
                    var data = google.visualization.arrayToDataTable([  
                ['dateValue', 'OWNER','USER'],");
                foreach (DataRow row in ChartDatabar.Rows)
                {
                    strScriptbar.Append("['" + row["dateValue"] + "'," + row["OWNER"] + "," + row["USER"] + "],");

                    
                }
                strScriptbar.Remove(strScriptbar.Length - 1, 1);
                strScriptbar.Append("]);");

                strScriptbar.Append("var options = { title : 'Date wise App Installation Count-Bar Graph', vAxis: {title: 'Count'},  hAxis: {title: 'Date'}, seriesType: 'bars', series: {3: {type: 'area'}} };");
                strScriptbar.Append(" var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
                strScriptbar.Append(" </script>");

               

                Literalbar.Text = strScriptbar.ToString();
            }
            catch
            {
            }
            finally
            {
                ChartDatabar.Dispose();
                strScriptbar.Clear();
            }
        }



        public void datadashbord()
        {
            try
            {
                cmd.CommandText = "sp_dashboardcount";
                // cmd.CommandText = "Sp_ezeetestuspGetcountDashdataweb";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = con;

                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblTodaycount.Text = ds.Tables[0].Rows[0][0].ToString();
                    lbltodayregistration.Text = ds.Tables[0].Rows[0][1].ToString();

                    lblTotalRegistration.Text = ds.Tables[0].Rows[0][2].ToString();
                    lblSharedCnt.Text = ds.Tables[0].Rows[0][3].ToString();


                }
            }
            catch (Exception ex)
            {
               // EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }
        //public void BindChart()
        //{
        //    try
        //    {
        //        DataTable dsChartData = new DataTable();
        //        StringBuilder strScript = new StringBuilder();

        //        try
        //        {
        //            dsChartData = GetChartData();

        //            strScript.Append(@"<script type='text/javascript'>  
        //            google.load('visualization', '1', {packages: ['corechart']}); </script>  

        //            <script type='text/javascript'>  

        //            function drawChart() {         
        //            var data = google.visualization.arrayToDataTable([  
        //            ['DateValue', 'Score'],");

        //            foreach (DataRow row in dsChartData.Rows)
        //            {
        //                strScript.Append("['" + row["DateValue"] + "'," + row["Score"] + "],");
        //            }
        //            strScript.Remove(strScript.Length - 1, 1);
        //            strScript.Append("]);");

        //            strScript.Append(@" var options = {     
        //                            title: 'Date wise Test Count',            
        //                            is3D: true,          
        //                            };   ");

        //            strScript.Append(@"var chart = new google.visualization.AreaChart(document.getElementById('piechart_3d'));          
        //                        chart.draw(data, options);        
        //                        }    
        //                    google.setOnLoadCallback(drawChart);  
        //                    ");
        //            strScript.Append(" </script>");

        //            //ltScripts.Text = strScript.ToString();
        //        }
        //        catch
        //        {
        //        }
        //        finally
        //        {
        //            dsChartData.Dispose();
        //            strScript.Clear();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
        //    }
        //}       
        //public DataTable GetChartData()
        //{
        //    DataSet dsData1 = new DataSet();
        //    try
        //    {
        //        SqlCommand cmd1 = new SqlCommand("SP_GetlineDashboard_Score");
        //        cmd1.Connection = con;
        //        cmd1.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter da1 = new SqlDataAdapter();
        //        da1.SelectCommand = cmd1;
        //        DataSet ds1 = new DataSet();
        //        da1.Fill(dsData1);
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //        throw;
        //    }
        //    return dsData1.Tables[0];
        //}      
        //public void BindChart1()
        //{
        //    DataTable dsChartData1 = new DataTable();
        //    StringBuilder strScript1 = new StringBuilder();

        //    try
        //    {
        //        dsChartData1 = GetChartDatapin();

        //        strScript1.Append(@"<script type='text/javascript'>  
        //            google.load('visualization', '1', {packages: ['corechart']}); </script>  

        //            <script type='text/javascript'>  

        //            function drawChart() {         
        //            var data = google.visualization.arrayToDataTable([  
        //            ['Status', 'Hours of Day'],");

        //        foreach (DataRow row in dsChartData1.Rows)
        //        {
        //            strScript1.Append("['" + row["Status"] + "'," + row["count1"] + "],");
        //        }
        //        strScript1.Remove(strScript1.Length - 1, 1);
        //        strScript1.Append("]);");

        //        strScript1.Append(@" var options = {     
        //                            title: 'Pass/Fail',            
        //                            is3D: true,          
        //                            };   ");

        //        strScript1.Append(@"var chart = new google.visualization.PieChart(document.getElementById('piechart_3d1'));          
        //                        chart.draw(data, options);        
        //                        }    
        //                    google.setOnLoadCallback(drawChart);  
        //                    ");
        //        strScript1.Append(" </script>");

        //        //ltScriptspin.Text = strScript1.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //    }
        //    finally
        //    {
        //        dsChartData1.Dispose();
        //        strScript1.Clear();
        //    }
        //}
        //public DataTable GetChartDatapin()
        //{
        //    DataSet dsDatap = new DataSet();
        //    try
        //    {
        //        SqlCommand cmdp = new SqlCommand("sp_passfaildatatoday");//  sp_schooledistrictcode1
        //        cmdp.Connection = con;
        //        cmdp.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter dap = new SqlDataAdapter();
        //        dap.SelectCommand = cmdp;
        //        DataSet ds = new DataSet();
        //        dap.Fill(dsDatap);
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //        throw;
        //    }
        //    return dsDatap.Tables[0];
        //}
        //public DataTable GetChartDatabar()
        //{
        //    DataSet dsData = new DataSet();
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand("SP_GetbarDashboard_Score");
        //        cmd.Connection = con;
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter da = new SqlDataAdapter();
        //        da.SelectCommand = cmd;
        //        DataSet ds = new DataSet();
        //        da.Fill(dsData);
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //        throw;
        //    }
        //    return dsData.Tables[0];
        //}
        //public void BindChartbar()
        //{

        //    DataTable ChartDatabar = new DataTable();
        //    StringBuilder strScriptbar = new StringBuilder();

        //    try
        //    {
        //        ChartDatabar = GetChartDatabar();

        //        strScriptbar.Append(@"<script type='text/javascript'>  
        //            google.load('visualization', '1', {packages: ['corechart']});</script>  

        //            <script type='text/javascript'>  
        //            function drawVisualization() {         
        //            var data = google.visualization.arrayToDataTable([  
        //            ['ExamId', 'count1'],");

        //        foreach (DataRow row in ChartDatabar.Rows)
        //        {
        //            strScriptbar.Append("['" + row["ExamId"] + "'," + row["count1"] + "],");
        //        }
        //        strScriptbar.Remove(strScriptbar.Length - 1, 1);
        //        strScriptbar.Append("]);");

        //        strScriptbar.Append("var options = { title : 'Date wise Test Count', vAxis: {title: 'Count'},  hAxis: {title: 'Date'}, seriesType: 'bars', series: {3: {type: 'area'}} };");
        //        strScriptbar.Append(" var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
        //        strScriptbar.Append(" </script>");

        //        //Literalbar.Text = strScriptbar.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //    }
        //    finally
        //    {
        //        ChartDatabar.Dispose();
        //        strScriptbar.Clear();
        //    }
        //}
        //public DataTable GetChartDatabarPass()
        //{
        //    DataSet dsData = new DataSet();
        //    try
        //    {
        //        SqlCommand cmd = new SqlCommand("SP_GetbarDashboard_passfail");
        //        cmd.Connection = con;
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        SqlDataAdapter da = new SqlDataAdapter();
        //        da.SelectCommand = cmd;
        //        DataSet ds = new DataSet();
        //        da.Fill(dsData);
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //        throw;
        //    }
        //    return dsData.Tables[0];
        //}
        //public void BindChartbarpassf()
        //{

        //    DataTable ChartDatabarpass = new DataTable();
        //    StringBuilder strScriptbarpass = new StringBuilder();

        //    try
        //    {
        //        ChartDatabarpass = GetChartDatabarPass();

        //        strScriptbarpass.Append(@"<script type='text/javascript'>  
        //            google.load('visualization', '1', {packages: ['corechart']});</script>  

        //            <script type='text/javascript'>  
        //            function drawVisualization() {         
        //            var data = google.visualization.arrayToDataTable([  
        //            ['dateValue', 'Pass','fail'],");

        //        foreach (DataRow row in ChartDatabarpass.Rows)
        //        {
        //            strScriptbarpass.Append("['" + row["dateValue"] + "'," + row["Pass"] + "," + row["fail"] + "],");
        //        }
        //        strScriptbarpass.Remove(strScriptbarpass.Length - 1, 1);
        //        strScriptbarpass.Append("]);");

        //        strScriptbarpass.Append("var options = { title : 'Date wise Pass/Fail Count', vAxis: {title: 'Count'},  hAxis: {title: 'Date'}, seriesType: 'bars', series: {3: {type: 'area'}} };");
        //        strScriptbarpass.Append(" var chart = new google.visualization.ComboChart(document.getElementById('PassFailbar'));  chart.draw(data, options); } google.setOnLoadCallback(drawVisualization);");
        //        strScriptbarpass.Append(" </script>");

        //        //Literalbarpassfailbar.Text = strScriptbarpass.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //    }
        //    finally
        //    {
        //        ChartDatabarpass.Dispose();
        //        strScriptbarpass.Clear();
        //    }
        //}

        //public void dashboard()
        //{
        //    try
        //    {
        //        // string District = Session["District"].ToString();
        //        //cmd.CommandText = "Sp_ezeecountdashboardRegistration";
        //        //cmd.CommandText = "Sp_ezeecountdashboardRegistrationNEW";
        //        cmd.CommandText = "Sp_ezeedashboardRegistrationNEW";
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@GroupCode", Session["GroupCode"]);
        //        cmd.Parameters.AddWithValue("@Mobile", mob);

        //        cmd.Connection = con;

        //        da = new SqlDataAdapter(cmd);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {

        //            lblcountRegistration.Text = ds.Tables[0].Rows[0][0].ToString();
        //            lblOfficecount.Text = ds.Tables[0].Rows[0][1].ToString();
        //            lblStaffCount.Text = ds.Tables[0].Rows[0][2].ToString();
        //            lblUsercount.Text = ds.Tables[0].Rows[0][3].ToString();

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //    }
        //}
        //public void UserMaster()
        //{
        //    try
        //    {
        //        string GroupCode = Session["GroupCode"].ToString();
        //        cmd.CommandText = "sp_getGVUsercountMasternewModify";
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@GroupCode", GroupCode);
        //        cmd.Connection = con;
        //        da = new SqlDataAdapter(cmd);
        //        da.Fill(ds);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {

        //            lblUsercount.Text = ds.Tables[0].Rows.Count.ToString();

        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        EL.SendErrorToText(ex);
        //    }

        //}

        //[WebMethod]
        //public List<GroupCodeDropDown> PopulateDropDownList()
        //{

        //    mob = Convert.ToString(Session["username"]);
        //    DataTable dt = new DataTable();
        //    List<GroupCodeDropDown> objDept = new List<GroupCodeDropDown>();

        //    using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["conStr"].ConnectionString))
        //    {
        //        using (SqlCommand cmd = new SqlCommand("UspDownloadGrupcode", con))
        //        {
        //            cmd.Connection = con;

        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //            cmd.CommandText = "UspDownloadGrupcode";
        //            cmd.Parameters.AddWithValue("@MobileNo", mob);
        //            cmd.Parameters.AddWithValue("@type", 1);
        //            con.Open();
        //            SqlDataAdapter da = new SqlDataAdapter(cmd);
        //            da.Fill(dt);
        //            if (dt.Rows.Count > 0)
        //            {
        //                for (int i = 0; i < dt.Rows.Count; i++)
        //                {
        //                    objDept.Add(new GroupCodeDropDown
        //                    {
        //                        GroupCode = Convert.ToString(dt.Rows[i]["meaning"]),
        //                        Meaning = dt.Rows[i]["groupCode"].ToString()
        //                    });
        //                }
        //            }
        //            return objDept;
        //        }
        //    }
        //}

    }
    //public class GroupCodeDropDown
    //{
    //    public string GroupCode { get; set; }
    //    public string Meaning { get; set; }

    //}
}