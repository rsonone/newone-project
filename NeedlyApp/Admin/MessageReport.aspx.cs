﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class MessageReport : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }
        public void GridViewWhatsappMsgCount()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("select * FROM [DBNeedly].[dbo].[tbl_WhatsappMsgCount] order by Id desc");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvWhatsappMsgCount.DataSource = ds.Tables[0];
                        GvWhatsappMsgCount.DataBind();
                    }

                    string SQl1 = "select SUM (Convert(int, PreviousMsgCount)) from tbl_WhatsappMsgCount";
                    string WhatsAppmsgCount = cc.ExecuteScalar(SQl1);
                    LblContactCount.Text = WhatsAppmsgCount;
                    lblExcelCountMsg.Visible = false;
                    lblWhatsAppMsgCount.Visible = true;
                    lblPhoneContactCount.Visible = false;
                    // PnlCB.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetExcelContactDetils");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvShowExcelData.DataSource = ds.Tables[0];
                        GvShowExcelData.DataBind();
                    }

                    string SQl1 = " select count(MobileNo) from [EzeeFormsDB].[dbo].[tbl_ContactDetails] ";
                    String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;
                    lblWhatsAppMsgCount.Visible = false;
                    lblPhoneContactCount.Visible = false;
                    // PnlSearch.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void rdbSelectContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbSelectContact.SelectedValue == "1")
            {
                PnlShowExclData.Visible = true;
                PnlPhoneContacts.Visible = false;
                PnlWhatsappMsgCount.Visible = false;

                PnlSearchForExcelData.Visible = true;
                PnlSearchForMsgCount.Visible = false;
                PnlSearchForPhoneContacts.Visible = false;
                GridView();
            }
            if (rdbSelectContact.SelectedValue == "2")
            {
                PnlShowExclData.Visible = false;
                PnlPhoneContacts.Visible = false;
                PnlWhatsappMsgCount.Visible = true;

                PnlSearchForExcelData.Visible = false;
                PnlSearchForMsgCount.Visible = true;
                PnlSearchForPhoneContacts.Visible = false;
                GridViewWhatsappMsgCount();
            }
            if (rdbSelectContact.SelectedValue == "3")
            {
                PnlShowExclData.Visible = false;
                PnlPhoneContacts.Visible = true;
                PnlWhatsappMsgCount.Visible = false;

                PnlSearchForExcelData.Visible = false;
                PnlSearchForMsgCount.Visible = false;
                PnlSearchForPhoneContacts.Visible = true;

                GridViewPhoneContact();
            }
        }

        public void GridViewPhoneContact()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetPhoneContacts");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvPhoneContact.DataSource = ds.Tables[0];
                        GvPhoneContact.DataBind();
                    }

                    string SQl1 = "SELECT COUNT(*) FROM tbl_PhoneContacts ";
                    String PhoneContactCount = cc.ExecuteScalar(SQl1);
                    LblContactCount.Text = PhoneContactCount;
                    lblPhoneContactCount.Visible = true;
                    lblExcelCountMsg.Visible = false;
                    lblWhatsAppMsgCount.Visible = false;
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void btnSearchFromExcelData_Click(object sender, EventArgs e)
        {
            GridViewForSerch();
        }

        protected void btnSearchFromMsgCount_Click(object sender, EventArgs e)
        {
            BindSearchForMsgCount();
        }

        protected void btnSearchFromContact_Click(object sender, EventArgs e)
        {
            BindSearchForPhoneContact();
        }
        public void GridViewForSerch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchExcelNumber");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Mobile", txtMobileExcelNumber.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvShowExcelData.DataSource = ds.Tables[0];
                        GvShowExcelData.DataBind();
                    }
                    else
                    {
                        lblNoData.Text = "No rocord found for CreatedBy  '" + txtMobileExcelNumber.Text + "'";
                    }


                }
                catch (Exception ex)
                {

                }
            }
        }

        public void BindSearchForMsgCount()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchWhatsAppmsg");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Mobile", txtmsgcountMobile.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvWhatsappMsgCount.DataSource = ds.Tables[0];
                        GvWhatsappMsgCount.DataBind();
                    }

                    //string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
                    //String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    //LblContactCount.Text = ExcelContactCount;
                    //lblExcelCountMsg.Visible = true;
                    //PnlCB.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void BindSearchForPhoneContact()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(" select * FROM [DBNeedly].[dbo].[tbl_PhoneContacts] where CreatedBy ='" + txtContactMobile.Text + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvPhoneContact.DataSource = ds.Tables[0];
                        GvPhoneContact.DataBind();
                    }

                    //string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
                    //String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    //LblContactCount.Text = ExcelContactCount;
                    //lblExcelCountMsg.Visible = true;
                    //PnlCB.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}