﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Web.Configuration;
using DAL;

namespace NeedlyApp.Admin
{
    public partial class Change_Password : System.Web.UI.Page
    {
        DataSet ObjDataSet = new DataSet();

        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        CommonCode cc = new CommonCode();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        Errorlogfil EL = new Errorlogfil();
        string ReportID = string.Empty;
        string mob = string.Empty;
        string SRNOID = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            mob = Convert.ToString(Session["username"]);

            if (!IsPostBack)
            {
                userdata();
            }
        }
        public void userdata()
        {
            try
            {

                cmd.CommandText = "Sp_ezeeuserdataweb";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MobileNo", mob);
                cmd.Connection = con;

                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    SRNOID = ds.Tables[0].Rows[0][0].ToString();
                    txtLoginId.Text = ds.Tables[0].Rows[0][1].ToString();
                    txtusername.Text = ds.Tables[0].Rows[0][3].ToString();
                    txtmobileno.Text = ds.Tables[0].Rows[0][5].ToString();
                    txtadderss.Text = ds.Tables[0].Rows[0][14].ToString();
                }
            }
            catch(Exception ex)
            {
                 EL.SendErrorToText(ex);
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text==txtRePassword.Text)
                {
                  string  Password = cc.DESEncrypt(txtPassword.Text);
                    SqlCommand cmd3 = new SqlCommand("Sp_updateuserdataweb", con);
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.Parameters.AddWithValue("@username", txtLoginId.Text);
                    cmd3.Parameters.AddWithValue("@Password", Password);
                    cmd3.Parameters.AddWithValue("@FirstName", txtusername.Text);
                    cmd3.Parameters.AddWithValue("@MobileNo", txtmobileno.Text);
                    cmd3.Parameters.AddWithValue("@Village", txtadderss.Text);
                    
                    con.Open();
                    cmd3.ExecuteNonQuery();
                    con.Close();

                    lbldata.Text = " Password Changed Successfully... ";
                }
                else
                {

                }
               
            }
            catch(Exception ex)
            {
                 EL.SendErrorToText(ex);
            }
        }
    }
}