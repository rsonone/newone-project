﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class LadiesSafety : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindState();
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowLadiesSafetyDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVLadiesSafety.DataSource = ds.Tables[0];
                        GVLadiesSafety.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlstate.DataSource = ds.Tables[0];
                    ddlstate.DataTextField = "STATE_NAME";
                    ddlstate.DataValueField = "MDDS_STC";
                    ddlstate.DataBind();
                    ddlstate.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlstate.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddldistrict.DataSource = ds.Tables[0];
                    ddldistrict.DataTextField = "DISTRICT_NAME";
                    ddldistrict.DataValueField = "MDDS_DTC";

                    ddldistrict.DataBind();
                    ddldistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddldistrict.SelectedIndex = 0;
                }
                else
                {
                    ddldistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddldistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }

        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }

        protected void ddldistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {


            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }

                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }



            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_Uploadladysafety");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();
                    cmd.Parameters.AddWithValue("@State", ddlstate.SelectedValue);
                    cmd.Parameters.AddWithValue("@District", ddldistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);
                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                    cmd.Parameters.AddWithValue("@ContactNumber", txtContactNo.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                    cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                    cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                    //string sql = "select firstname from [DBNeedly].[dbo].[EzeeDrugsAppDetail]  where mobileNo='" + Session["username"].ToString() + "'";
                    //string result = Convert.ToString(cc.ExecuteScalar(sql));
                    //string sql1 = "select mobileNo from [DBNeedly].[dbo].[EzeeDrugsAppDetail]  where mobileNo='" + Session["username"].ToString() + "'";
                    //string result1 = Convert.ToString(cc.ExecuteScalar(sql1));


                    //cmd.Parameters.AddWithValue("@ShopUser_Name", result);
                    //cmd.Parameters.AddWithValue("@Shop_Mobile", result1);

                    int A = cmd.ExecuteNonQuery();
                    string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }

        }





        public void InsertData(DataTable dt = null)
        {

            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                      
                        string Name = dt.Rows[i]["Name"].ToString();
                        Name = Name.Trim();

                        string Address = dt.Rows[i]["Address"].ToString();
                        Address = Address.Trim();

                        string ContactNumber = dt.Rows[i]["ContactNumber"].ToString();
                        ContactNumber = ContactNumber.Trim();








                        SqlCommand cmd = new SqlCommand("SP_Uploadladysafety", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@State", ddlstate.SelectedValue);
                        cmd.Parameters.AddWithValue("@District", ddldistrict.SelectedValue);
                        cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@ContactNumber", ContactNumber);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;


                        //cmd.Parameters.AddWithValue("@Shop_Mobile", result1);
                        //cmd.Parameters.AddWithValue("@ShopUser_Name", result);

                        int A = cmd.ExecuteNonQuery();
                      
                        DataBaseConnection.Close();
                        // string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                        // DataBaseConnection.Close();
                        string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                        DataBaseConnection.Close();
                        // int A = cmd.ExecuteNonQuery();
                         DataBaseConnection.Close();
                        cmd.Parameters.Clear();
                          DataBaseConnection.Close();
                        GridView();
                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);
                }
            }
        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/LadiesSafety.xlsx", false);

        }

        protected void GVAddreferal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVLadiesSafety.PageIndex = e.NewPageIndex;
            GridView();
        }
    }


      

      

    
}