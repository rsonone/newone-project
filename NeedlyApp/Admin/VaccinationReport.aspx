﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="VaccinationReport.aspx.cs" Inherits="NeedlyApp.Admin.VaccinationReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
    
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-list"></i> Beed Covid-19 Registration Report <small></small></h2>
                            <div class="container-fluid">
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                     <%--   <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                                Select Option
                                            </label>
                                            <div class="col-sm-8">
                                                <asp:RadioButtonList ID="rdbSelectContact" OnSelectedIndexChanged="rdbSelectContact_SelectedIndexChanged" runat="server"  RepeatDirection="Vertical" AutoPostBack="true">
                                                    <asp:ListItem Value="1">&nbsp;&nbsp;Check  Excel Report&nbsp;&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="2">&nbsp;&nbsp;Check Message Count Report&nbsp;&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="3">&nbsp;&nbsp;Check Contact Report&nbsp;&nbsp;</asp:ListItem>

                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>--%>


                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-10">
                                        <div class="form-group">
                                          <asp:Label ID="lblShow" Font-Bold="true" Font-Size="Larger" ForeColor="Black" runat="server"  Text="Total  No. of Registration for vaccination :"></asp:Label>
                                       
                                            <asp:Label ID="lblVcCount" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblNoData" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                             </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                          <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-7">
                                     &nbsp;&nbsp;&nbsp;   <asp:Label ID="Label1" runat="server" Font-Size="Medium" Font-Bold="true" ForeColor="Red" Text="Select vaccination Center "></asp:Label>

                                </div>
                            </div>
                               
                    </div>
                                               <asp:Panel ID="Panel1" runat="server" >
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                      
<%--                                        <div class="box-body col-sm-5">

                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtMobileExcelNumber" Visible="false" runat="server" placeholder="Enter Mobile Number" CssClass="form-control" ></asp:TextBox>

                                            </div>
                                            <div class="col-sm-5">

                                                <asp:Button ID="btnSearchByMobileNo" Visible="false" runat="server"  OnClick="btnSearchByMobileNo_Click" CssClass="btn btn-success"  Text="Search" />
                                            </div>

                                        </div>--%>
                                       
                                       
                                           
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlVaccinationCenter"  CssClass="form-control" runat="server">
                                                     <asp:ListItem Value="0" Text="-- Select --"></asp:ListItem>
                                                    <asp:ListItem Value="45212" Text="Adas"></asp:ListItem>
                                                    <asp:ListItem Value="45213" Text="Amalner"></asp:ListItem>
                                                    <asp:ListItem Value="45214" Text="Ambajogai UPHC"></asp:ListItem>
                                                    <asp:ListItem Value="45215" Text="Apegaon"></asp:ListItem>
                                                    <asp:ListItem Value="45216" Text="Ashti RH"></asp:ListItem>
                                                    <asp:ListItem Value="45217" Text="Bansarola"></asp:ListItem>
                                                    <asp:ListItem Value="45218" Text="Bardapur"></asp:ListItem>
                                                    <asp:ListItem Value="45219" Text="Beed DH(Champavati Primary School)"></asp:ListItem>
                                                    <asp:ListItem Value="45220" Text="Bhavthana"></asp:ListItem>
                                                    <asp:ListItem Value="45221" Text="Bhogalwadi"></asp:ListItem>
                                                    <asp:ListItem Value="45222" Text="Chakalamba"></asp:ListItem>
                                                    <asp:ListItem Value="45223" Text="Charhata"></asp:ListItem>
                                                    <asp:ListItem Value="45224" Text="Chausala"></asp:ListItem>
                                                    <asp:ListItem Value="45225" Text="Chincholi Mali"></asp:ListItem>
                                                    <asp:ListItem Value="45226" Text="Chinchwan RH"></asp:ListItem>
                                                    <asp:ListItem Value="45227" Text="Dhamangaon"></asp:ListItem>
                                                    <asp:ListItem Value="45228" Text="Dhanora RH"></asp:ListItem>
                                                    <asp:ListItem Value="45229" Text="Dharmapuri"></asp:ListItem>
                                                    <asp:ListItem Value="45230" Text="Dharur RH"></asp:ListItem>
                                                    <asp:ListItem Value="45231" Text="Dongarkini"></asp:ListItem>
                                                    <asp:ListItem Value="45232" Text="Gangamasala"></asp:ListItem>
                                                    <asp:ListItem Value="45233" Text="Georai SDH"></asp:ListItem>
                                                    <asp:ListItem Value="45234" Text="Ghatnandur"></asp:ListItem>
                                                    <asp:ListItem Value="45235" Text="Jategaon"></asp:ListItem>
                                                    <asp:ListItem Value="45236" Text="Kada"></asp:ListItem>
                                                    <asp:ListItem Value="45237" Text="Kaij SDH"></asp:ListItem>
                                                    <asp:ListItem Value="45238" Text="Khalapuri SC"></asp:ListItem>
                                                    <asp:ListItem Value="45239" Text="Kitti Adgaon"></asp:ListItem>
                                                    <asp:ListItem Value="45240" Text="Kuntefal"></asp:ListItem>
                                                    <asp:ListItem Value="45241" Text="Kuppa"></asp:ListItem>
                                                    <asp:ListItem Value="45242" Text="Limbaganesh"></asp:ListItem>

                                                    <asp:ListItem Value="45243" Text="Madalmohi"></asp:ListItem>
                                                    <asp:ListItem Value="45244" Text="Majalgaon RH"></asp:ListItem>
                                                    <asp:ListItem Value="45245" Text="Moha"></asp:ListItem>
                                                    <asp:ListItem Value="45246" Text="Mohkhed"></asp:ListItem>
                                                    <asp:ListItem Value="45247" Text="Nagapur"></asp:ListItem>
                                                    <asp:ListItem Value="45248" Text="Naigaon"></asp:ListItem>
                                                    <asp:ListItem Value="45249" Text="Nalwandi"></asp:ListItem>
                                                    <asp:ListItem Value="45250" Text="Nandurghat RH"></asp:ListItem>
                                                    <asp:ListItem Value="45251" Text="Navgan Rajuri"></asp:ListItem>
                                                    <asp:ListItem Value="45252" Text="Neknoor WH"></asp:ListItem>
                                                    <asp:ListItem Value="45253" Text="Nipani Jawalaka"></asp:ListItem>
                                                    <asp:ListItem Value="45255" Text="Parali SDH (Loknete Kailasvasi Gopinath Munde Natraj Rangmandir)"></asp:ListItem>
                                                    <asp:ListItem Value="45256" Text="Parali UPHC"></asp:ListItem>
                                                    <asp:ListItem Value="45257" Text="Pathrud"></asp:ListItem>
                                                    <asp:ListItem Value="45258" Text="Patoda RH"></asp:ListItem>




                                                    <asp:ListItem Value="45259" Text="Pimpalner"></asp:ListItem>
                                                    <asp:ListItem Value="45260" Text="Pohner"></asp:ListItem>
                                                    <asp:ListItem Value="45261" Text="Raimoha RH"></asp:ListItem>
                                                    <asp:ListItem Value="45262" Text="Rajegaon"></asp:ListItem>
                                                    <asp:ListItem Value="45263" Text="Rui Dharur"></asp:ListItem>
                                                    <asp:ListItem Value="45264" Text="Sadola"></asp:ListItem>
                                                    <asp:ListItem Value="45265" Text="Sakshal pimpri"></asp:ListItem>
                                                    <asp:ListItem Value="45266" Text="Shirur"></asp:ListItem>
                                                    <asp:ListItem Value="45267" Text="Sirsala"></asp:ListItem>
                                                    <asp:ListItem Value="45268" Text="SRTR Medical College (Yogeshwari Nutan Vidyalaya Primary Department)"></asp:ListItem>
                                                    <asp:ListItem Value="45269" Text="Suleman Devala"></asp:ListItem>
                                                    <asp:ListItem Value="45270" Text="Tadasonna"></asp:ListItem>
                                                    <asp:ListItem Value="45271" Text="Takalsing"></asp:ListItem>
                                                    <asp:ListItem Value="45272" Text="Takarwan"></asp:ListItem>

                                                    <asp:ListItem Value="45273" Text="Talkhed RH"></asp:ListItem>
                                                    <asp:ListItem Value="45274" Text="Talwada"></asp:ListItem>
                                                    <asp:ListItem Value="45275" Text="TPS Hospital"></asp:ListItem>
                                                    <asp:ListItem Value="45276" Text="Ujani"></asp:ListItem>
                                                    <asp:ListItem Value="45277" Text="Umapur"></asp:ListItem>
                                                    <asp:ListItem Value="45278" Text="UPHC Mominpura"></asp:ListItem>
                                                    <asp:ListItem Value="45279" Text="UPHC Pethbeed"></asp:ListItem>
                                                    <asp:ListItem Value="45280" Text="Wadwani"></asp:ListItem>
                                                    <asp:ListItem Value="45281" Text="Wahali"></asp:ListItem>
                                                    <asp:ListItem Value="45282" Text="Wida"></asp:ListItem>
                                                    <asp:ListItem Value="45283" Text="Yellambghat"></asp:ListItem>
                                                    <asp:ListItem Value="45284" Text="Yusuf Wadgaon"></asp:ListItem>
                                                    <asp:ListItem Value="45289" Text="Police Hospital Beed"></asp:ListItem>
                                                   
                                                </asp:DropDownList>
                                               

                                            </div>
                                            <div class="col-sm-3">

                                                <asp:Button ID="btnSearchByCenter" OnClick="btnSearchByCenter_Click" CssClass="btn btn-success" runat="server" Text="Search" />
                                            </div>
                                               <div class="col-sm-2">

                                                <asp:Button ID="btnExcelExport" OnClick="btnExcelExport_Click"  CssClass="btn btn-success" runat="server"  Text="Export Excel File" />
                                            </div>

                                       
                                    </div>
                                </div>
                            </div>
                       </asp:Panel>
                       
                         <asp:Panel ID="PnlSearchByCentereAndToken" runat="server" Visible="false" >
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                      
                                        <div class="box-body col-sm-12">

                                            <div class="col-sm-4">
                                                <asp:DropDownList ID="ddlVcCentre" CssClass="form-control"   runat="server"></asp:DropDownList>
                                            </div>
                                             <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlFromToken" CssClass="form-control"  runat="server"></asp:DropDownList>
                                            </div>
                                             <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlToToken" CssClass="form-control"  runat="server"></asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2">

                                                <asp:Button ID="btnSearchByCentreAndToken" OnClick="btnSearchByCentreAndToken_Click" CssClass="btn btn-success" runat="server"  Text="Search" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                          

                           <%-- <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                       &nbsp; &nbsp;Select Vccination Center to download Excel
                                        <div class="box-body col-sm-12">
                                           
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddlExportExcelVCname" CssClass="form-control"  runat="server">
                                                     <asp:ListItem Value="0" Text="-- Select --"></asp:ListItem>

                                                    <asp:ListItem Value="45212" Text="Adas"></asp:ListItem>
                                                    <asp:ListItem Value="45213" Text="Amalner"></asp:ListItem>
                                                    <asp:ListItem Value="45214" Text="Ambajogai UPHC"></asp:ListItem>
                                                    <asp:ListItem Value="45215" Text="Apegaon"></asp:ListItem>
                                                    <asp:ListItem Value="45216" Text="Ashti RH"></asp:ListItem>
                                                    <asp:ListItem Value="45217" Text="Bansarola"></asp:ListItem>
                                                    <asp:ListItem Value="45218" Text="Bardapur"></asp:ListItem>
                                                    <asp:ListItem Value="45219" Text="Beed DH(Champavati Primary School)"></asp:ListItem>
                                                    <asp:ListItem Value="45220" Text="Bhavthana"></asp:ListItem>
                                                    <asp:ListItem Value="45221" Text="Bhogalwadi"></asp:ListItem>
                                                    <asp:ListItem Value="45222" Text="Chakalamba"></asp:ListItem>
                                                    <asp:ListItem Value="45223" Text="Charhata"></asp:ListItem>
                                                    <asp:ListItem Value="45224" Text="Chausala"></asp:ListItem>
                                                    <asp:ListItem Value="45225" Text="Chincholi Mali"></asp:ListItem>
                                                    <asp:ListItem Value="45226" Text="Chinchwan RH"></asp:ListItem>
                                                    <asp:ListItem Value="45227" Text="Dhamangaon"></asp:ListItem>
                                                    <asp:ListItem Value="45228" Text="Dhanora RH"></asp:ListItem>
                                                    <asp:ListItem Value="45229" Text="Dharmapuri"></asp:ListItem>
                                                    <asp:ListItem Value="45230" Text="Dharur RH"></asp:ListItem>
                                                    <asp:ListItem Value="45231" Text="Dongarkini"></asp:ListItem>
                                                    <asp:ListItem Value="45232" Text="Gangamasala"></asp:ListItem>
                                                    <asp:ListItem Value="45233" Text="Georai SDH"></asp:ListItem>
                                                    <asp:ListItem Value="45234" Text="Ghatnandur"></asp:ListItem>
                                                    <asp:ListItem Value="45235" Text="Jategaon"></asp:ListItem>
                                                    <asp:ListItem Value="45236" Text="Kada"></asp:ListItem>
                                                    <asp:ListItem Value="45237" Text="Kaij SDH"></asp:ListItem>
                                                    <asp:ListItem Value="45238" Text="Khalapuri SC"></asp:ListItem>
                                                    <asp:ListItem Value="45239" Text="Kitti Adgaon"></asp:ListItem>
                                                    <asp:ListItem Value="45240" Text="Kuntefal"></asp:ListItem>
                                                    <asp:ListItem Value="45241" Text="Kuppa"></asp:ListItem>
                                                    <asp:ListItem Value="45242" Text="Limbaganesh"></asp:ListItem>

                                                    <asp:ListItem Value="45243" Text="Madalmohi"></asp:ListItem>
                                                    <asp:ListItem Value="45244" Text="Majalgaon RH"></asp:ListItem>
                                                    <asp:ListItem Value="45245" Text="Moha"></asp:ListItem>
                                                    <asp:ListItem Value="45246" Text="Mohkhed"></asp:ListItem>
                                                    <asp:ListItem Value="45247" Text="Nagapur"></asp:ListItem>
                                                    <asp:ListItem Value="45248" Text="Naigaon"></asp:ListItem>
                                                    <asp:ListItem Value="45249" Text="Nalwandi"></asp:ListItem>
                                                    <asp:ListItem Value="45250" Text="Nandurghat RH"></asp:ListItem>
                                                    <asp:ListItem Value="45251" Text="Navgan Rajuri"></asp:ListItem>
                                                    <asp:ListItem Value="45252" Text="Neknoor WH"></asp:ListItem>
                                                    <asp:ListItem Value="45253" Text="Nipani Jawalaka"></asp:ListItem>
                                                    <asp:ListItem Value="45255" Text="Parali SDH (Loknete Kailasvasi Gopinath Munde Natraj Rangmandir)"></asp:ListItem>
                                                    <asp:ListItem Value="45256" Text="Parali UPHC"></asp:ListItem>
                                                    <asp:ListItem Value="45257" Text="Pathrud"></asp:ListItem>
                                                    <asp:ListItem Value="45258" Text="Patoda RH"></asp:ListItem>




                                                    <asp:ListItem Value="45259" Text="Pimpalner"></asp:ListItem>
                                                    <asp:ListItem Value="45260" Text="Pohner"></asp:ListItem>
                                                    <asp:ListItem Value="45261" Text="Raimoha RH"></asp:ListItem>
                                                    <asp:ListItem Value="45262" Text="Rajegaon"></asp:ListItem>
                                                    <asp:ListItem Value="45263" Text="Rui Dharur"></asp:ListItem>
                                                    <asp:ListItem Value="45264" Text="Sadola"></asp:ListItem>
                                                    <asp:ListItem Value="45265" Text="Sakshal pimpri"></asp:ListItem>
                                                    <asp:ListItem Value="45266" Text="Shirur"></asp:ListItem>
                                                    <asp:ListItem Value="45267" Text="Sirsala"></asp:ListItem>
                                                    <asp:ListItem Value="45268" Text="SRTR Medical College (Yogeshwari Nutan Vidyalaya Primary Department)"></asp:ListItem>
                                                    <asp:ListItem Value="45269" Text="Suleman Devala"></asp:ListItem>
                                                    <asp:ListItem Value="45270" Text="Tadasonna"></asp:ListItem>
                                                    <asp:ListItem Value="45271" Text="Takalsing"></asp:ListItem>
                                                    <asp:ListItem Value="45272" Text="Takarwan"></asp:ListItem>

                                                    <asp:ListItem Value="45273" Text="Talkhed RH"></asp:ListItem>
                                                    <asp:ListItem Value="45274" Text="Talwada"></asp:ListItem>
                                                    <asp:ListItem Value="45275" Text="TPS Hospital"></asp:ListItem>
                                                    <asp:ListItem Value="45276" Text="Ujani"></asp:ListItem>
                                                    <asp:ListItem Value="45277" Text="Umapur"></asp:ListItem>
                                                    <asp:ListItem Value="45278" Text="UPHC Mominpura"></asp:ListItem>
                                                    <asp:ListItem Value="45279" Text="UPHC Pethbeed"></asp:ListItem>
                                                    <asp:ListItem Value="45280" Text="Wadwani"></asp:ListItem>
                                                    <asp:ListItem Value="45281" Text="Wahali"></asp:ListItem>
                                                    <asp:ListItem Value="45282" Text="Wida"></asp:ListItem>
                                                    <asp:ListItem Value="45283" Text="Yellambghat"></asp:ListItem>
                                                    <asp:ListItem Value="45284" Text="Yusuf Wadgaon"></asp:ListItem>
                                                    <asp:ListItem Value="45289" Text="Police Hospital Beed"></asp:ListItem>
                                                </asp:DropDownList>
                                            
                                               </div>
                                            
                                          

                                        </div>
                                    </div>
                                </div>
                            </div>--%>
                        

                       <%-- GridView Code--%>
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>   
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                   <asp:Panel ID="pnlShowGrid" runat="server">
                              <div class="table-responsive">
                                <div class="row" style="width: 1000px; overflow: auto">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                            <asp:GridView ID="GvVaccinationData" runat="server" OnPageIndexChanging="GvVaccinationData_PageIndexChanging"  PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                               
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="Age" HeaderText="Age" />
                                    <asp:BoundField DataField="MobileNumber" HeaderText="Mobile Number" />
                                    <asp:BoundField DataField="Address" HeaderText="Address" />
                                    <asp:BoundField DataField="VcName" HeaderText="Vaccination centre Name" />
                                    <asp:BoundField DataField="VcId" HeaderText="VaccinationCentre Id" />
                                    <asp:BoundField DataField="Gender" HeaderText="Gender" />
                                    <asp:BoundField DataField="VcToken" HeaderText="Vaccination Token" />
                                    <asp:BoundField DataField="DoseDetails" HeaderText="Dose Details" />
                                    <asp:BoundField DataField="Regisered_MobileNo" HeaderText="Regisered MobileNo" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    <asp:BoundField DataField="Modified_Date" HeaderText="Modified Date" />
                                    <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" />
                                   <%-- <asp:BoundField DataField="FirstName" HeaderText="Name" />
                                    <asp:BoundField DataField="CreatedBy" HeaderText="App User No." />
                                   --%>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />

                            </asp:GridView>
                                 </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                     

                   

                      
                </div>
            </div>
</asp:Content>
