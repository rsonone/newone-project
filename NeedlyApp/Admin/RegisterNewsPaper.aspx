﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="RegisterNewsPaper.aspx.cs" Inherits="NeedlyApp.Admin.RegisterNewsPaper" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <style >
      .container {
   overflow: scroll;
        min-height: 100%;
}
    </style>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Registration News paper <small></small></h2>

                       <div class="container-fluid">
                        <div class="pull-right">
              <asp:Button ID="btnexcel" runat="server" class="btn btn-success" OnClick="btnexcel_Click"
                                                    Text="Upload Excel"/>
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 <asp:Panel ID="PnlNews" runat="server">
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    News paper Name: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtPaperName" runat="server" CssClass="form-control"></asp:TextBox>
                          </div></div></div>
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    News paper language: </label>
                                <div class="col-sm-9">
                                  <asp:DropDownList ID="ddlLang" runat="server" AutoPostBack="true" CssClass="form-control">
                                       <asp:ListItem Value="">--Select--</asp:ListItem>
                                       <asp:ListItem Value="1">Marathi</asp:ListItem>
                                       <asp:ListItem Value="2">Hindi</asp:ListItem>
                                       <asp:ListItem Value="3">English</asp:ListItem>
                                   </asp:DropDownList>
                          </div></div></div>
              
                    </div>
               </div></div> 
                    
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                      <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 News paper Name(in regional): </label>
                              <div class="col-sm-9">
                               <asp:TextBox ID="txtRigiName" runat="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                New paper Origin: </label>
                              <div class="col-sm-9">
                              <%-- <asp:TextBox ID="txtOrigin" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                  <asp:DropDownList ID="ddlstate" CssClass="form-control" runat="server">
                                       <asp:ListItem Value="">--Select--</asp:ListItem>
                                   </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Paper Frequency: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlFreq" runat="server" AutoPostBack="true" CssClass="form-control">
                                       <asp:ListItem Value="">--Select--</asp:ListItem>
                                       <asp:ListItem Value="1">Daily</asp:ListItem>
                                       <asp:ListItem Value="2">Weekly</asp:ListItem>
                                       <asp:ListItem Value="3">Twice a week</asp:ListItem>
                                       <asp:ListItem Value="4">Thrice a week</asp:ListItem>
                                       <asp:ListItem Value="5">Fortnightly</asp:ListItem>
                                       <asp:ListItem Value="6">Monthly</asp:ListItem>
                                       <asp:ListItem Value="7">Bi-Monthly</asp:ListItem>
                                       <asp:ListItem Value="8">Quarterly</asp:ListItem>
                                       <asp:ListItem Value="9">Four Monthly</asp:ListItem>
                                       <asp:ListItem Value="10">Half yearly</asp:ListItem>
                                       <asp:ListItem Value="11">Annual</asp:ListItem>
                                   </asp:DropDownList>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 WebSite : </label>
                              <div class="col-sm-9">
                                 <asp:TextBox ID="txtWebsite" runat="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div> 
                     
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Head Office Addresss: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtHAdd" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Registration Mobile No.: </label>
                              <div class="col-sm-9">
                                 <asp:TextBox ID="txtRMobno" runat="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Contact Person Name: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtCntName" runat="server" CssClass="form-control"></asp:TextBox>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Contact Mobile No.: </label>
                              <div class="col-sm-9">
                                 <asp:TextBox ID="txtCntMob" runat="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div>
                   
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Owner/Company Name: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtCompName" runat="server" CssClass="form-control"></asp:TextBox>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Available District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddlDistrict" CssClass="form-control" runat="server">
                                       <asp:ListItem Value="">--Select--</asp:ListItem>
                                   </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                        
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                              <asp:Button ID="btnUpdate" runat="server" Text="Update" Visible="false" class="btn btn-success" OnClick="btnUpdate_Click" />
                              <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                </asp:Panel>
                    <asp:Panel ID="Pnlexcel" runat="server" Visible="false">
                          <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">Upload Excel: </label>
                                        <div class="col-sm-9">
                                            <asp:FileUpload ID="Fileupload1" runat="server" />
                                        </div></div></div>
                                      <div class="box-body col-sm-6">
                         <div class="form-group">
                              <asp:LinkButton ID="LinkButton1" runat="server" Text=" Download ExcelFile Format New..!!!" ForeColor="Red" Font-Size="Medium" OnClick="LinkButton1_Click"></asp:LinkButton>
                              <div id="txtblnk"><font color="red"></font></div> 
                         </div></div>
                                </div>
                            </div></div> 
                         <div class="col-md-12">
                        <div class="box-body col-sm-5">
                        
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnUploadExcel" runat="server" Text="Upload Excel" class="btn btn-success" OnClick="btnUploadExcel_Click" />
                        </div>
                        <div class="box-body col-sm-4">
                            
                        </div>
                    </div>
                    </asp:Panel>
                     <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVNewsPaper" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVNewsPaper_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial">
                    <Columns>   
                          <asp:BoundField DataField="Id" HeaderText="ID" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Paper_Name" HeaderText="Paper Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="RegionalName" HeaderText="RegionalName">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Language" HeaderText="Language">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Frequency" HeaderText="Frequency">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Website" HeaderText="Website">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="MobileNo" HeaderText="MobileNo">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                      
                         <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-success" Text="Edit" OnClick="btnEdit_Click" />
                                      <i></i></a></a>
                                   <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                     
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div>
                          <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                     </div>
           
                     
                    
                  </div>
                </div></div>
    </asp:Content>