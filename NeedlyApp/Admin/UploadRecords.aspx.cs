﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class UploadRecords : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                SenderNo();
                SenderNo2();
                GridViewHeading();
                //Panel1.Visible = true;
            }

        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("GetDataByIndex");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Start", txtFromId.Text);
                    cmd.Parameters.AddWithValue("@PageLimit", txtToId.Text);


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUploadRecordNeedly.DataSource = ds.Tables[0];
                        GVUploadRecordNeedly.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public void GridViewHeading()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("GetUploadrecordsHeading");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@Start", txtFromId.Text);
                    //cmd.Parameters.AddWithValue("@PageLimit", txtToId.Text);


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUplodRecordsHeading.DataSource = ds.Tables[0];
                        GVUplodRecordsHeading.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public void GridViewPincodeData()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("GetPincodeDataByIndex");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Start", txtFromId.Text);
                    cmd.Parameters.AddWithValue("@PageLimit", txtToId.Text);
                    cmd.Parameters.AddWithValue("@Pincode", ddlPincode.SelectedItem.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUploadRecordsPincodeData.DataSource = ds.Tables[0];
                        GVUploadRecordsPincodeData.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void SenderNo()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadSenderNo");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlSenderNo.DataSource = ds.Tables[0];
                        ddlSenderNo.DataTextField = "Name";
                        ddlSenderNo.DataValueField = "Id";

                        ddlSenderNo.DataBind();
                        ddlSenderNo.Items.Insert(0, new ListItem("--Select--", ""));
                        //ddlSenderNo.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlSenderNo.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        public void BindPincodDropdown()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadPincode");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlPincode.DataSource = ds.Tables[0];
                        ddlPincode.DataTextField = "Pincode";
                        //ddlPincode.DataValueField = "Id";

                        ddlPincode.DataBind();
                        ddlPincode.Items.Insert(0, new ListItem("--Select--", ""));
                        //ddlSenderNo.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlSenderNo.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }
        public void SenderNo2()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadSenderNo");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlSenderNo2.DataSource = ds.Tables[0];
                        ddlSenderNo2.DataTextField = "MobileNo";
                        ddlSenderNo2.DataValueField = "Id";

                        ddlSenderNo2.DataBind();
                        ddlSenderNo2.Items.Insert(0, new ListItem("--Select--", ""));
                        //ddlSenderNo.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlSenderNo.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        protected void btnShowRecords_Click(object sender, EventArgs e)
        {
            //if(ddlReceiver.SelectedValue == "1")
            //{
            //   // GridView();
            //    GVUploadRecordNeedly.Visible = true;
            //    GVUploadRecordsPincodeData.Visible = false;
            //    GVUplodRecordsHeading.Visible = false;
            //}
            //if (ddlReceiver.SelectedValue == "2")
            //{

            //}
            //if (ddlReceiver.SelectedValue == "3")
            //{
            //   // GridViewPincodeData();
            //    GVUploadRecordNeedly.Visible = false;
            //    GVUploadRecordsPincodeData.Visible = true;
            //    GVUplodRecordsHeading.Visible = false;
            //}
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("GetUploadrecordsHeadingByName");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                    string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                    cmd.Parameters.AddWithValue("@SenderMobileNo", CreatedBy);
                    //cmd.Parameters.AddWithValue("@PageLimit", txtToId.Text);


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUplodRecordsHeading.DataSource = ds.Tables[0];
                        GVUplodRecordsHeading.DataBind();
                    }
                    string SQl1Count = "select count(*) from [tbl_UploadRecordsHeading] where SenderMobileNo = '" + CreatedBy + "'";
                    string cnt = Convert.ToString(cc.ExecuteScalar(SQl1Count));

                    lblmsg.Text = "Total records for person '" + ddlSenderNo.SelectedItem.Text + "' :";
                    lblCount.Text = cnt;

                }
                catch (Exception ex)
                {

                }
            }


        }
        public void HeadingByname()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("GetUploadrecordsHeadingByName");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                    string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                    cmd.Parameters.AddWithValue("@SenderMobileNo", CreatedBy);
                    //cmd.Parameters.AddWithValue("@PageLimit", txtToId.Text);


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUplodRecordsHeading.DataSource = ds.Tables[0];
                        GVUplodRecordsHeading.DataBind();
                    }
                    string SQl1Count = "select count(*) from [tbl_UploadRecordsHeading] where SenderMobileNo = '" + CreatedBy + "'";
                    string cnt = Convert.ToString(cc.ExecuteScalar(SQl1Count));

                    lblmsg.Text = "Total records for person '" + ddlSenderNo.SelectedItem.Text + "' :";
                    lblCount.Text = cnt;

                }
                catch (Exception ex)
                {

                }
            }

        }
        protected void btnSendRecords_Click(object sender, EventArgs e)
        {
            //Panel1.Visible = false;
            //Panel2.Visible = true;
            //string CustomerID = textBox1.Text;
           int  NewSrNo;
            using (SqlConnection DataBaseConnectionNeedly = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    if (ddlReceiver.SelectedValue == "1")
                    {
                        SqlCommand cmd2 = new SqlCommand("SP_InsetUploadRecordsHeading");
                        cmd2.Connection = DataBaseConnectionNeedly;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        //string createdById = ddlSenderNo.SelectedValue.ToString();

                        string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                        string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                        DataBaseConnectionNeedly.Open();
                        cmd2.Parameters.AddWithValue("@DataReceivedFrom", ddlReceiver.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@SenderMobileNo", CreatedBy);
                        cmd2.Parameters.AddWithValue("@MarketingPersonName", ddlSenderNo.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@Category",txtCategory.Text );
                        cmd2.Parameters.AddWithValue("@Prefix", txtPrefix.Text);

                        cmd2.Parameters.AddWithValue("@FromId", txtFromId.Text);
                        cmd2.Parameters.AddWithValue("@ToId", txtToId.Text);
                        cmd2.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd2.Parameters.AddWithValue("@AdminNumber", txtAdminNo.Text);
                        cmd2.Parameters.AddWithValue("@Status", "1");
                         
                        // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                        cmd2.ExecuteNonQuery();
                        DataBaseConnectionNeedly.Close();
                        //ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done','Record inserted !..','success')", true);

                        SqlCommand cmd1 = new SqlCommand("GetDataByIndex");
                        cmd1.Connection = DataBaseConnectionNeedly;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@Start", txtFromId.Text);
                        cmd1.Parameters.AddWithValue("@PageLimit", txtToId.Text);
                        DataBaseConnectionNeedly.Open();
                        SqlDataReader rdr = cmd1.ExecuteReader();

                        string NewFName;
                        while (rdr.Read())
                        {

                            string keyword = rdr["keyword"].ToString();
                            string firstName = rdr["firstName"].ToString();
                            // string lastName = rdr["lastName"].ToString();
                            if (firstName.Length < 5)
                            {
                                NewFName = firstName;
                            }
                            else
                            {
                                NewFName = firstName.Substring(0, 5);
                            }

                            string mobileNo = rdr["mobileNo"].ToString();
                            string address = rdr["address"].ToString();




                            SqlConnection DataBaseConnectionEzeeForm = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringEzeeForm"].ConnectionString);
                            //
                            SqlCommand cmd3 = new SqlCommand("SP_InsetRecordsFromRegi");
                            cmd3.Connection = DataBaseConnectionEzeeForm;
                            cmd3.CommandType = CommandType.StoredProcedure;
                            //string createdById = ddlSenderNo.SelectedValue.ToString();

                            string SQl1CreatedBy1 = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                            string CreatedBy1 = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy1));


                            string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                            string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                            //int NewSrNo = Convert.ToInt32(SRNO);
                            if (SRNO == "")
                            {
                                SRNO = "0";
                            }
                            else
                            {

                            }
                            NewSrNo = Convert.ToInt32(SRNO);

                            NewSrNo = NewSrNo + 1;
                            DataBaseConnectionEzeeForm.Open();
                            cmd3.Parameters.AddWithValue("@SrNo", NewSrNo);
                            cmd3.Parameters.AddWithValue("@MobileNo", mobileNo);
                            //cmd2.Parameters.AddWithValue("@Prefix", "ms");
                            cmd3.Parameters.AddWithValue("@CustomerName", NewFName);
                            cmd3.Parameters.AddWithValue("@Address", address);
                            cmd3.Parameters.AddWithValue("@CreatedBy", CreatedBy1);
                            cmd3.Parameters.AddWithValue("@Keyword", keyword);
                            cmd3.Parameters.AddWithValue("@Category", txtCategory.Text);
                            cmd3.Parameters.AddWithValue("@Prefix", txtPrefix.Text);
                            // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                            cmd3.ExecuteNonQuery();
                            DataBaseConnectionEzeeForm.Close();
                        }



                    }
                    if (ddlReceiver.SelectedValue == "2")
                    {

                    }
                    if (ddlReceiver.SelectedValue == "3")
                    {
                        SqlCommand cmd2 = new SqlCommand("SP_InsetUploadRecordsHeading1");
                        cmd2.Connection = DataBaseConnectionNeedly;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        //string createdById = ddlSenderNo.SelectedValue.ToString();

                        string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                        string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                        //string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                        //string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                        //int NewSrNo = Convert.ToInt32(SRNO);


                        DataBaseConnectionNeedly.Open();
                        cmd2.Parameters.AddWithValue("@DataReceivedFrom", ddlReceiver.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@SenderMobileNo", CreatedBy);
                        cmd2.Parameters.AddWithValue("@MarketingPersonName", ddlSenderNo.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@Pincode", ddlPincode.SelectedItem.Text);
                        //cmd2.Parameters.AddWithValue("@Prefix", txtPrefix.Text);

                        cmd2.Parameters.AddWithValue("@FromId", txtFromId.Text);
                        cmd2.Parameters.AddWithValue("@ToId", txtToId.Text);
                        cmd2.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd2.Parameters.AddWithValue("@AdminNumber", txtAdminNo.Text);
                        cmd2.Parameters.AddWithValue("@Status", "1");
                        //cmd2.Parameters.AddWithValue("@AdminName", txtAdminName.Text);

                        // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                        cmd2.ExecuteNonQuery();
                        DataBaseConnectionNeedly.Close();
                        //ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done',' Data inserted !..','success')", true);

                        //SqlCommand cmd1 = new SqlCommand("GetPincodeDataByIndex");
                        //cmd1.Connection = DataBaseConnectionNeedly;
                        //cmd1.CommandType = CommandType.StoredProcedure;
                        //cmd1.Parameters.AddWithValue("@Start", txtFromId.Text);
                        //cmd1.Parameters.AddWithValue("@PageLimit", txtToId.Text);
                        //cmd1.Parameters.AddWithValue("@Pincode", ddlPincode.SelectedItem.Text);
                        //DataBaseConnectionNeedly.Open();
                        //SqlDataReader rdr = cmd1.ExecuteReader();

                        //string NewFName;
                        //while (rdr.Read())
                        //{

                        //    string Mobile_Number = rdr["Mobile_Number"].ToString();
                        //    string Name = rdr["Name"].ToString();
                        //    // string lastName = rdr["lastName"].ToString();
                        //    if (Name.Length < 5)
                        //    {
                        //        NewFName = Name;
                        //    }
                        //    else
                        //    {
                        //        NewFName = Name.Substring(0, 5);
                        //    }


                        //    SqlConnection DataBaseConnectionEzeeForm = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringEzeeForm"].ConnectionString);
                        //    //
                        //    SqlCommand cmd3 = new SqlCommand("SP_InsetRecordsFromRegi1");
                        //    cmd3.Connection = DataBaseConnectionEzeeForm;
                        //    cmd3.CommandType = CommandType.StoredProcedure;
                        //    //string createdById = ddlSenderNo.SelectedValue.ToString();

                        //    string SQl1CreatedBy2 = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                        //    string CreatedBy2 = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                        //    string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                        //    string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                        //    //int NewSrNo = Convert.ToInt32(SRNO);
                        //    if (SRNO == "")
                        //    {
                        //        SRNO = "0";
                        //    }
                        //    else
                        //    {

                        //    }
                        //    NewSrNo = Convert.ToInt32(SRNO);

                        //    NewSrNo = NewSrNo + 1;
                        //    DataBaseConnectionEzeeForm.Open();
                        //    cmd3.Parameters.AddWithValue("@SrNo", NewSrNo);
                        //    cmd3.Parameters.AddWithValue("@MobileNo", Mobile_Number);
                        //    //cmd3.Parameters.AddWithValue("@Pincode",ddlPincode.SelectedValue);
                        //    cmd3.Parameters.AddWithValue("@CustomerName", NewFName);
                        //    cmd3.Parameters.AddWithValue("@Address", ddlPincode.SelectedItem.Text);
                        //    cmd3.Parameters.AddWithValue("@CreatedBy", CreatedBy2);
                        //    cmd3.Parameters.AddWithValue("@Keyword", ddlReceiver.SelectedItem.Text);
                        //    //cmd3.Parameters.AddWithValue("@Category", txtCategory.Text);
                        //    //cmd3.Parameters.AddWithValue("@Prefix", txtPrefix.Text);
                        //    //cmd2.Parameters.AddWithValue("@Category", txtCategory.Text);
                        //    cmd3.ExecuteNonQuery();
                        //    DataBaseConnectionEzeeForm.Close();
                        //}


                    }

                    if (ddlReceiver.SelectedValue == "4")
                    {
                        SqlCommand cmd2 = new SqlCommand("SP_InsetUploadRecordsHeading1");
                        cmd2.Connection = DataBaseConnectionNeedly;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        //string createdById = ddlSenderNo.SelectedValue.ToString();

                        string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                        string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                        //string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                        //string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                        //int NewSrNo = Convert.ToInt32(SRNO);


                        DataBaseConnectionNeedly.Open();
                        cmd2.Parameters.AddWithValue("@DataReceivedFrom", ddlReceiver.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@SenderMobileNo", CreatedBy);
                        cmd2.Parameters.AddWithValue("@MarketingPersonName", ddlSenderNo.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@Pincode", "TrueVoter");
                        //cmd2.Parameters.AddWithValue("@Prefix", txtPrefix.Text);

                        cmd2.Parameters.AddWithValue("@FromId", txtFromId.Text);
                        cmd2.Parameters.AddWithValue("@ToId", txtToId.Text);
                        cmd2.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd2.Parameters.AddWithValue("@AdminNumber", txtAdminNo.Text);
                        cmd2.Parameters.AddWithValue("@Status", "1");
                        //cmd2.Parameters.AddWithValue("@AdminName", txtAdminName.Text);

                        // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                        cmd2.ExecuteNonQuery();
                        DataBaseConnectionNeedly.Close();
                        //ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done',' Data inserted !..','success')", true);

                        SqlCommand cmd1 = new SqlCommand("GetTrueVoterDataByIndex");
                        cmd1.Connection = DataBaseConnectionNeedly;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@Start", txtFromId.Text);
                        cmd1.Parameters.AddWithValue("@PageLimit", txtToId.Text);
                        //cmd1.Parameters.AddWithValue("@Pincode", ddlPincode.SelectedItem.Text);
                        DataBaseConnectionNeedly.Open();
                        SqlDataReader rdr = cmd1.ExecuteReader();

                        string NewFName;
                        while (rdr.Read())
                        {

                            string Mobile_Number = rdr["MobileNo"].ToString();
                            string Name = rdr["Name"].ToString();
                            // string lastName = rdr["lastName"].ToString();
                            if (Name.Length < 5)
                            {
                                NewFName = Name;
                            }
                            else
                            {
                                NewFName = Name.Substring(0, 5);
                            }


                            SqlConnection DataBaseConnectionEzeeForm = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringEzeeForm"].ConnectionString);
                            //
                            SqlCommand cmd3 = new SqlCommand("SP_InsetRecordsFromRegi1");
                            cmd3.Connection = DataBaseConnectionEzeeForm;
                            cmd3.CommandType = CommandType.StoredProcedure;
                            //string createdById = ddlSenderNo.SelectedValue.ToString();

                            string SQl1CreatedBy2 = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                            string CreatedBy2 = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                            string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                            string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                            //int NewSrNo = Convert.ToInt32(SRNO);
                            if (SRNO == "")
                            {
                                SRNO = "0";
                            }
                            else
                            {

                            }
                            NewSrNo = Convert.ToInt32(SRNO);

                            NewSrNo = NewSrNo + 1;
                            DataBaseConnectionEzeeForm.Open();
                            cmd3.Parameters.AddWithValue("@SrNo", NewSrNo);
                            cmd3.Parameters.AddWithValue("@MobileNo", Mobile_Number);
                            //cmd3.Parameters.AddWithValue("@Pincode",ddlPincode.SelectedValue);
                            cmd3.Parameters.AddWithValue("@CustomerName", NewFName);
                            cmd3.Parameters.AddWithValue("@Address","Truevoter");
                            cmd3.Parameters.AddWithValue("@CreatedBy", CreatedBy2);
                            cmd3.Parameters.AddWithValue("@Keyword", ddlReceiver.SelectedItem.Text);
                            //cmd3.Parameters.AddWithValue("@Category", txtCategory.Text);
                            //cmd3.Parameters.AddWithValue("@Prefix", txtPrefix.Text);
                            //cmd2.Parameters.AddWithValue("@Category", txtCategory.Text);
                            cmd3.ExecuteNonQuery();
                            DataBaseConnectionEzeeForm.Close();
                        }


                    }
                    // ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('FAILED TO UPLOAD DATA !!!')", true);
                }
            }

          
        }

        protected void GVUploadRecord_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUploadRecordNeedly.PageIndex = e.NewPageIndex;
            if (ddlReceiver.SelectedValue == "1")
            {
                GridView();
            }
            if (ddlReceiver.SelectedValue == "2")
            {

            }
            if (ddlReceiver.SelectedValue == "3")
            {
                GridViewPincodeData();
            }
        }

        protected void btnShowRecords2_Click(object sender, EventArgs e)
        {
            GridView();
        }

        protected void btnSendRecords2_Click(object sender, EventArgs e)
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            //string CustomerID = textBox1.Text;
            int NewSrNo;
            using (SqlConnection DataBaseConnectionNeedly = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    if(ddlReceiver.SelectedValue == "1")
                    {
                        SqlCommand cmd2 = new SqlCommand("SP_InsetUploadRecordsHeading");
                        cmd2.Connection = DataBaseConnectionNeedly;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        //string createdById = ddlSenderNo.SelectedValue.ToString();

                        //string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                        //string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                        //string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                        //string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                        //int NewSrNo = Convert.ToInt32(SRNO);


                        DataBaseConnectionNeedly.Open();
                        cmd2.Parameters.AddWithValue("@DataReceivedFrom", ddlReceivData2.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@SenderMobileNo", ddlSenderNo2.Text);
                        //cmd2.Parameters.AddWithValue("@Prefix", "ms");
                        cmd2.Parameters.AddWithValue("@Category", txtCategory2.Text);
                        cmd2.Parameters.AddWithValue("@Prefix", txtPrefix2.Text);

                        cmd2.Parameters.AddWithValue("@FromId", txtFromId2.Text);
                        cmd2.Parameters.AddWithValue("@ToId", txtToId2.Text);
                        cmd2.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                        cmd2.ExecuteNonQuery();
                        DataBaseConnectionNeedly.Close();
                        //ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done','Record inserted !..','success')", true);

                        SqlCommand cmd1 = new SqlCommand("GetDataByIndex");
                        cmd1.Connection = DataBaseConnectionNeedly;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@Start", txtFromId2.Text);
                        cmd1.Parameters.AddWithValue("@PageLimit", txtToId2.Text);
                        DataBaseConnectionNeedly.Open();
                        SqlDataReader rdr = cmd1.ExecuteReader();

                        string NewFName;
                        while (rdr.Read())
                        {

                            string keyword = rdr["keyword"].ToString();
                            string firstName = rdr["firstName"].ToString();
                            // string lastName = rdr["lastName"].ToString();
                            if (firstName.Length < 5)
                            {
                                NewFName = firstName;
                            }
                            else
                            {
                                NewFName = firstName.Substring(0, 5);
                            }

                            string mobileNo = rdr["mobileNo"].ToString();
                            string address = rdr["address"].ToString();




                            SqlConnection DataBaseConnectionEzeeForm = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringEzeeForm"].ConnectionString);
                            //
                            SqlCommand cmd3 = new SqlCommand("SP_InsetRecordsFromRegi");
                            cmd3.Connection = DataBaseConnectionEzeeForm;
                            cmd3.CommandType = CommandType.StoredProcedure;
                            //string createdById = ddlSenderNo.SelectedValue.ToString();

                            string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo2.SelectedValue + "'";
                            string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                            string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                            string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                            //int NewSrNo = Convert.ToInt32(SRNO);
                            if (SRNO == "")
                            {
                                SRNO = "0";
                            }
                            else
                            {

                            }
                            NewSrNo = Convert.ToInt32(SRNO);

                            NewSrNo = NewSrNo + 1;
                            DataBaseConnectionEzeeForm.Open();
                            cmd3.Parameters.AddWithValue("@SrNo", NewSrNo);
                            cmd3.Parameters.AddWithValue("@MobileNo", mobileNo);
                            //cmd2.Parameters.AddWithValue("@Prefix", "ms");
                            cmd3.Parameters.AddWithValue("@CustomerName", NewFName);
                            cmd3.Parameters.AddWithValue("@Address", address);
                            cmd3.Parameters.AddWithValue("@CreatedBy", ddlSenderNo2.SelectedItem.Text);
                            cmd3.Parameters.AddWithValue("@Keyword", keyword);
                            cmd3.Parameters.AddWithValue("@Category", txtCategory2.Text);
                            cmd3.Parameters.AddWithValue("@Prefix", txtPrefix2.Text);
                            // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                            cmd3.ExecuteNonQuery();
                            DataBaseConnectionEzeeForm.Close();
                        }



                    }
                    if(ddlReceiver.SelectedValue == "2")
                    {

                    }
                    if(ddlReceiver.SelectedValue == "3")
                    {
                        SqlCommand cmd2 = new SqlCommand("SP_InsetUploadRecordsHeading");
                        cmd2.Connection = DataBaseConnectionNeedly;
                        cmd2.CommandType = CommandType.StoredProcedure;
                        //string createdById = ddlSenderNo.SelectedValue.ToString();

                        //string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                        //string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                        //string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                        //string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                        //int NewSrNo = Convert.ToInt32(SRNO);


                        DataBaseConnectionNeedly.Open();
                        cmd2.Parameters.AddWithValue("@DataReceivedFrom", ddlReceivData2.SelectedItem.Text);
                        cmd2.Parameters.AddWithValue("@SenderMobileNo", ddlSenderNo2.Text);
                        //cmd2.Parameters.AddWithValue("@Prefix", "ms");
                        cmd2.Parameters.AddWithValue("@Category", txtCategory2.Text);
                        cmd2.Parameters.AddWithValue("@Prefix", txtPrefix2.Text);

                        cmd2.Parameters.AddWithValue("@FromId", txtFromId2.Text);
                        cmd2.Parameters.AddWithValue("@ToId", txtToId2.Text);
                        cmd2.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                        cmd2.ExecuteNonQuery();
                        DataBaseConnectionNeedly.Close();
                        //ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);

                        ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done','Record inserted !..','success')", true);

                        SqlCommand cmd1 = new SqlCommand("GetPincodeDataByIndex");
                        cmd1.Connection = DataBaseConnectionNeedly;
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@Start", txtFromId2.Text);
                        cmd1.Parameters.AddWithValue("@PageLimit", txtToId2.Text);
                        cmd1.Parameters.AddWithValue("@Pincode", ddlPincode.SelectedItem.Text);
                        DataBaseConnectionNeedly.Open();
                        SqlDataReader rdr = cmd1.ExecuteReader();

                        string NewFName;
                        while (rdr.Read())
                        {

                            string Mobile_Number = rdr["Mobile_Number"].ToString();
                            string Name = rdr["Name"].ToString();
                            // string lastName = rdr["lastName"].ToString();
                            if (Name.Length < 5)
                            {
                                NewFName = Name;
                            }
                            else
                            {
                                NewFName = Name.Substring(0, 5);
                            }

                           
                            SqlConnection DataBaseConnectionEzeeForm = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringEzeeForm"].ConnectionString);
                            //
                            SqlCommand cmd3 = new SqlCommand("SP_InsetRecordsFromRegi");
                            cmd3.Connection = DataBaseConnectionEzeeForm;
                            cmd3.CommandType = CommandType.StoredProcedure;
                            //string createdById = ddlSenderNo.SelectedValue.ToString();

                            string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo2.SelectedValue + "'";
                            string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));


                            string SQl1 = "SELECT MAX(convert (int, SrNo)) FROM [tbl_ContactDetails]  WHERE CreatedBy = '" + CreatedBy + "'";
                            string SRNO = Convert.ToString(cc.ExecuteScalarContact(SQl1));
                            //int NewSrNo = Convert.ToInt32(SRNO);
                            if (SRNO == "")
                            {
                                SRNO = "0";
                            }
                            else
                            {

                            }
                            NewSrNo = Convert.ToInt32(SRNO);

                            NewSrNo = NewSrNo + 1;
                            DataBaseConnectionEzeeForm.Open();
                            cmd3.Parameters.AddWithValue("@SrNo", NewSrNo);
                            cmd3.Parameters.AddWithValue("@MobileNo", Mobile_Number);
                            //cmd2.Parameters.AddWithValue("@Prefix", "ms");
                            cmd3.Parameters.AddWithValue("@CustomerName", Name);
                            cmd3.Parameters.AddWithValue("@Address", ddlPincode.SelectedItem.Text);
                            cmd3.Parameters.AddWithValue("@CreatedBy", ddlSenderNo2.SelectedItem.Text);
                            cmd3.Parameters.AddWithValue("@Keyword", ddlReceiver.SelectedItem.Text);
                            cmd3.Parameters.AddWithValue("@Category", txtCategory2.Text);
                            cmd3.Parameters.AddWithValue("@Prefix", txtPrefix2.Text);
                            // cmd2.Parameters.AddWithValue("@Category", "Needly data");
                            cmd3.ExecuteNonQuery();
                            DataBaseConnectionEzeeForm.Close();
                        }


                    }

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('FAILED TO UPLOAD DATA !!!')", true);
                }
            }

        }

        protected void ddlReceiver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlReceiver.SelectedValue == "1")//needly
            {
                Panel1.Visible = false;
                PnlCatAndPrefix.Visible = true;
                string SQl1 = "select Count(*) from [EzeeDrugsAppDetail] where keyword = 'Needly'";
                string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                lblmsg.Text = "Total records for Needly";
                lblCount.Text = spreadsheetcnt;
            }
            if (ddlReceiver.SelectedValue == "2")
            {
                Panel1.Visible = false;
                PnlCatAndPrefix.Visible = true;
            }
            if (ddlReceiver.SelectedValue == "3")
            {
                Panel1.Visible = true;
                PnlCatAndPrefix.Visible = false;
                BindPincodDropdown();
                string SQl1 = "select count (*) from [tbl_MH_PIncodeData]";
                string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                lblmsg.Text = "Total records for Pincode Data";
                lblCount.Text = spreadsheetcnt;
            }
            if (ddlReceiver.SelectedValue == "4")
            {
                Panel1.Visible = false;
                PnlCatAndPrefix.Visible = false;
                BindPincodDropdown();
                string SQl1 = "select count (*) from [tbl_NeedlyTrueVoterData]";
                string spreadsheetcnt = cc.ExecuteScalar(SQl1);
                lblmsg.Text = "Total records for Truevoter Data";
                lblCount.Text = spreadsheetcnt;
            }
        }

        protected void ddlPincode_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SQl1 = "select count (*) from [tbl_MH_PIncodeData] where Pincode = '" + ddlPincode.SelectedItem.Text + "'";
            string spreadsheetcnt = cc.ExecuteScalar(SQl1);
            lblmsg.Text = "Total records for Pincode '" + ddlPincode.SelectedItem.Text + "' are :";
            lblCount.Text = spreadsheetcnt;
        }

        protected void ddlSenderNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string SQl1 = "select Count(*) from [tbl_ContactDetails] where CreatedBy = '" + ddlSenderNo.SelectedItem.Text + "'";
            //string spreadsheetcnt = cc.ExecuteScalarContact(SQl1);
            //lblmsg.Text = "Total records for No. '" + ddlSenderNo.SelectedItem.Text + "' are :";
            //lblCount.Text = spreadsheetcnt;
        }

        protected void GVUplodRecordsHeading_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUplodRecordsHeading.PageIndex = e.NewPageIndex;
           
                GridViewHeading();
           

        }

        protected void GVUplodRecordsHeading_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //  DataRow row = ((DataRowView)e.Row.DataItem).Row;

               // ImageButton img = (ImageButton)e.Row.FindControl("ImageButton1");

                Button mybtn = (Button)e.Row.FindControl("btnStatus");

                

                if (e.Row.Cells[10].Text == "1")
                {
                    mybtn.Text = "Active";
                    mybtn.BackColor = System.Drawing.Color.Green;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                    //img.Visible = true;
                    //img.ImageUrl = "~/img_status/imgactive.png";
                    //Mylabel.Text = "Active";
                }
                if (e.Row.Cells[10].Text == "0")
                {
                    //img.ImageUrl = "~/img_status/imgdeactive.png";
                    //Mylabel.Text = "deActive";
                    mybtn.Text = "Deactive";
                    mybtn.BackColor = System.Drawing.Color.Red;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                    //img.Visible = false;
                }

            }
        }

        protected void btnStatus_Click(object sender, EventArgs e)
        {
            int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            Label1.Text = GVUplodRecordsHeading.Rows[rowind].Cells[0].Text;
            //string UId = GVUplodRecordsHeading.Rows[rowind].Cells[1].Text;
            string IsPageAccess = GVUplodRecordsHeading.Rows[rowind].Cells[10].Text;

            if (IsPageAccess == "0")
            {
                string Query2 = "Update  [tbl_UploadRecordsHeading] set Status = 1 where Id = '" + Label1.Text + "' ";
                string Res2 = cc.ExecuteScalar(Query2);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Records Activated !!!')", true);
                GridViewHeading();
            }
            else
            {
                string Query2 = "Update  [tbl_UploadRecordsHeading] set Status = 0 where Id = '" + Label1.Text + "' ";
                string Res2 = cc.ExecuteScalar(Query2);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Records Deactivated')", true);
                GridViewHeading();

            }

        }

        protected void btnExportData_Click(object sender, EventArgs e)
        {
            if(ddlSenderNo.SelectedItem.Text == "--Select--")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Please select Marketing persos name to export data')", true);
            }
            else
            {
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "DownloadExcel.xls"));
                Response.ContentType = "application/ms-excel";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                GvExportData.AllowPaging = false;
                GvExportData.PageIndex = 0;

                getDataForExport();
                //Change the Header Row back to white color
                GvExportData.HeaderRow.Style.Add("background-color", "#FFFFFF");
                //Applying stlye to gridview header cells
                for (int i = 0; i < GvExportData.HeaderRow.Cells.Count; i++)
                {
                    GvExportData.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
                }
                GvExportData.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();
            }
        }

        public void getDataForExport()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringEzeeForm"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("ExportDataByCreatedBy");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    string SQl1CreatedBy = "SELECT MobileNo from [tbl_AddMarketingPerson]  WHERE Id = '" + ddlSenderNo.SelectedValue + "'";
                    string CreatedBy = Convert.ToString(cc.ExecuteScalar(SQl1CreatedBy));

                    cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    //cmd.Parameters.AddWithValue("@PageLimit", txtToId.Text);


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvExportData.DataSource = ds.Tables[0];
                        GvExportData.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            //required to avoid the runtime error "  
            //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
        }
    }
}