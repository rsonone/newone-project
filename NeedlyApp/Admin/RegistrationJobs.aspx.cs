﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Generic;

namespace NeedlyApp.Admin
{
    public partial class RegistrationJobs : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
                BindState();
            }
        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT1");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    Chklist.DataSource = ds.Tables[0];
                    Chklist.DataTextField = "DISTRICT_NAME";
                    Chklist.DataValueField = "MDDS_DTC";
                    Chklist.DataBind();
                    
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowRegistrationJobsdata");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SubmittedBy", Session["username"].ToString());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddQues.DataSource = ds.Tables[0];
                        GVAddQues.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        protected void GVAddQues_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddQues.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    Button btn = sender as Button;
                    GridViewRow gv = btn.NamingContainer as GridViewRow;
                    lblId.Text = (btn.FindControl("lblId1") as Label).Text;
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = DataBaseConnection;
                    DataBaseConnection.Open();

                    command4.CommandText = "sp_EditRegistrationJobsdata";
                    command4.CommandType = System.Data.CommandType.StoredProcedure;
                    command4.Parameters.AddWithValue("@Id", lblId.Text);

                    SqlDataAdapter da4 = new SqlDataAdapter();
                    da4.SelectCommand = command4;
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        chkjobsector.Text = ds4.Tables[0].Rows[0][0].ToString();
                        rbtnexpectjob.SelectedValue = ds4.Tables[0].Rows[0][1].ToString();
                        rbtninterest.SelectedValue = ds4.Tables[0].Rows[0][2].ToString();
                        rbtnpolice.SelectedValue = ds4.Tables[0].Rows[0][3].ToString();
                        txtexams.Text = ds4.Tables[0].Rows[0][4].ToString();
                        txtachive.Text = ds4.Tables[0].Rows[0][5].ToString();
                        //listdistrict.Text = ds4.Tables[0].Rows[0][6].ToString();
                        txtmobileno.Text = ds4.Tables[0].Rows[0][7].ToString();
                        
                    }
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string id = "";
            string sector = "";
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    foreach (ListItem item in chkjobsector.Items)
                    {
                        if (item.Selected)
                        {
                            sector += item.Text + ",";
                        }
                    }

                    foreach (ListItem item1 in Chklist.Items)
                    {
                        if (item1.Selected)
                        {
                            id += item1.Text + ",";
                        }
                    }

                    SqlCommand cmd = new SqlCommand("SP_UpdateRegistrationJobsdata");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Id", lblId.Text);
                    cmd.Parameters.AddWithValue("@Jobsector", sector);
                    cmd.Parameters.AddWithValue("@Governmentjobtraining", rbtnexpectjob.SelectedValue);
                    cmd.Parameters.AddWithValue("@Policebhartiinterest", rbtninterest.SelectedValue);
                    cmd.Parameters.AddWithValue("@PoliceBhartiTraining", rbtnpolice.SelectedValue);
                    cmd.Parameters.AddWithValue("@Competitiveexams", txtexams.Text);
                    cmd.Parameters.AddWithValue("@achievements", txtachive.Text);
                    cmd.Parameters.AddWithValue("@preferabledistrict", id);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtmobileno.Text);

                     
    


                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    string id = "";
                    string sector = "";
                    if (chkjobsector.SelectedIndex >= 0 && Chklist.SelectedIndex >=0)
                    {
                        foreach (ListItem item in chkjobsector.Items)
                        {
                            if (item.Selected)
                            {
                                sector += item.Text + ",";
                            }
                        }

                                foreach (ListItem item1 in Chklist.Items)
                                {
                                    if (item1.Selected)
                                    {
                                        id += item1.Text + ",";
                                    }
                                }
                           
                                            SqlCommand cmd = new SqlCommand("SP_InsertRegistrationJobsdata");
                                            cmd.Connection = DataBaseConnection;
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            DataBaseConnection.Open();

                                            cmd.Parameters.AddWithValue("@Jobsector", sector);
                                            cmd.Parameters.AddWithValue("@Governmentjobtraining", rbtnexpectjob.SelectedValue);
                                            cmd.Parameters.AddWithValue("@Policebhartiinterest", rbtninterest.SelectedValue);
                                            cmd.Parameters.AddWithValue("@PoliceBhartiTraining", rbtnpolice.SelectedValue);
                                            cmd.Parameters.AddWithValue("@Competitiveexams", txtexams.Text);
                                            cmd.Parameters.AddWithValue("@achievements", txtachive.Text);
                                            cmd.Parameters.AddWithValue("@preferabledistrict", id);
                                            cmd.Parameters.AddWithValue("@CreatedBy", txtmobileno.Text);
                                            cmd.Parameters.AddWithValue("@Modifyby", Session["username"].ToString());
                                            cmd.Parameters.AddWithValue("@SubmittedBy", Session["username"].ToString());


                        int A = cmd.ExecuteNonQuery();
                                            if (A == -1)
                                            {
                                                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                                            }
                                            DataBaseConnection.Close();
                                            GridView();
                                        }
                                    }
                                    
                                
                
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA NOT SUBMITTED SUCCESSFULLY.!!!')", true);
                }
            }
        }
    }
}