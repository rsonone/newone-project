﻿using DAL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class MyDukanLink : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            DataFieldLoad();
        }

        public void DataFieldLoad()
        {
            try
            {
                //    SqlCommand cmd = new SqlCommand("sp_ShowData");
                SqlCommand cmd = new SqlCommand("sp_ShowDataimages");

                cmd.Connection = con;
                //  con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Parameters.AddWithValue("@Id", ViewState["Id"].ToString());
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
                    {
                        string Image = Convert.ToString(ds.Tables[0].Rows[r]["Image"]);
                        string title = Convert.ToString(ds.Tables[0].Rows[r]["title"]);
                        string helpkeyID = Convert.ToString(ds.Tables[0].Rows[r]["helpkeyID"]);

                        string Html = "<div class='col-lg-4'>" +
                                    "<div class='product__discount__item'>" +
                                        "<div  id='" + r + "' class='product__discount__item__pic set-bg'" +
                                            "data-setbg='" + Image + "'>" +
                                            "<div class='product__discount__percent'>-" + helpkeyID + "%</div>" +
                                           " <ul class='product__item__pic__hover'>" +
                                             "   <li><a onclick='functions();'><i class='fa fa-heart'></i></a></li>" +
                                              "  <li><a onclick='functions();'><i class='fa fa-retweet'></i></a></li>" +
                                              "  <li><a onclick='functions();'><i class='fa fa-shopping-cart'></i></a></li>" +

                                            "</ul>" +
                                        "</div>" +
                                        "<div class='product__discount__item__text'>" +
                                          "  <span>Dried Fruit</span>" +
                                           " <h5><a href='#'>" + title + "</a></h5>" +
                                            "<div class='product__item__price'>$30.00 <span>$36.00</span></div>" +

                                        "</div>" +
                                   " </div>" +
                              "  </div>";
                        PlaceHolder1.Controls.Add(new Literal() { ID = "literal", Text = Html });

                        string HTML2 = "< div class='col-lg-4 col-md-6 col-sm-6'>" +
                            "< div class='product__item'>" +
                             "   <div  id='" + r + "' class='product__item__pic set-bg' data-setbg='" + Image + "'>" +
                              "      <ul class='product__item__pic__hover'>" +
                               "         <li><a href = '#' >< i class='fa fa-heart'></i></a></li>" +
                                "        <li><a href = '#' >< i class='fa fa-retweet'></i></a></li>" +
                                 "       <li><a onclick = 'functions();' >< i class='fa fa-shopping-cart'></i></a></li>" +
                                  "  </ul>" +
                                "</div>" +
                                "<div class='product__item__text'>" +
                                 "   <h6><a href = '#' > "+title+"</a></h6>" +
                                  "  <h5>$"+helpkeyID+".00</h5>" +
                                "</div>" +
                            "</div>" +
                        "</div>";
                        PlaceHolder2.Controls.Add(new Literal() { ID = "literal", Text = Html });

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}