﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AgricultureRegistration.aspx.cs" Inherits="NeedlyApp.Admin.AgricultureRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Registration for Agriculture Project / Agriculture Ancillary Project <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                    Mobile Number of Beneficiary / Unemployed Person: </label>
                                <div class="col-sm-6">
                            <asp:TextBox ID="txtmobilenumber" CssClass="form-control" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Name of Project / Business: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtbusinessname" CssClass="form-control" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Do you have your own land?: </label>
                                <div class="col-sm-6">
                                <asp:RadioButtonList ID="rbtnland" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes&nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="No&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Area of land: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtarea" CssClass="form-control" Placeholder="In Hector" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Type of Land: </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rbtntypeofland" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Irregated&nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="Non Irregated&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                    
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Annual Agriculture Income: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtannualincome" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Type of business: </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rbtntypebusiness" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Agri Related&nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="Non Agri project&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Do you want benefit of Government Scheme?: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtbenefit" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Details about the scheme needed: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtdetails" CssClass="form-control" Textmode="MultiLine" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Own Fund / Investment Done: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtinvestment" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Amount of Fund required: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtamountfund" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                Available marketing facilities: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtmarketingfacility" CssClass="form-control" TextMode="MultiLine"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                Other: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtother" CssClass="form-control" TextMode="MultiLine"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="col-md-12">
           <div class="box-body col-sm-5">
                         
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click"  />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false"  OnClick="btnUpdate_Click" />
           </div>


        </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddQues"  runat="server" OnPageIndexChanging="GVAddQues_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%>    
                           <asp:BoundField DataField="Id" HeaderText="ID">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="NameofProject" HeaderText="Business Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Areaofland" HeaderText="Area Of Land">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="AnnualAgricultureIncome" HeaderText="Agriculture Annual Income">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="GovernmentSchemes" HeaderText="Government Scheme">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Schemedetails" HeaderText="Scheme Details">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="ownfund" HeaderText="Own Fund/Investment Done">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="fundamount" HeaderText="Amount Of Fund">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="marketingfacilties" HeaderText="Marketing Facility">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="other" HeaderText="Other">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Createdby" HeaderText="Created By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                         <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>

                    </div>
                  </div>
                </div>
</asp:Content>
