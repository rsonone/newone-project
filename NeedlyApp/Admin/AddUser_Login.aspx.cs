﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddUser_Login : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindState();
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowLoginDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUserLogin.DataSource = ds.Tables[0];
                        GVUserLogin.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

       

        protected void txtMob_OnTextChanged(object sender, EventArgs e)
        {
            string selectquotationno = "select MobileNo from tbl_UserLoginDetails where MobileNo='" + txtMob.Text + "'";
            string number = Convert.ToString(cc.ExecuteScalar(selectquotationno));
            if (number == txtMob.Text)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Mobile Number is Already Exist ')", true);
            }
            else
            {

            }
        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlstate.DataSource = ds.Tables[0];
                    ddlstate.DataTextField = "STATE_NAME";
                    ddlstate.DataValueField = "MDDS_STC";
                    ddlstate.DataBind();
                    ddlstate.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlstate.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlstate.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddldistrict.DataSource = ds.Tables[0];
                    ddldistrict.DataTextField = "DISTRICT_NAME";
                    ddldistrict.DataValueField = "MDDS_DTC";

                    ddldistrict.DataBind();
                    ddldistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddldistrict.SelectedIndex = 0;
                }
                else
                {
                    ddldistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    string selectquotationno = "select MobileNo from tbl_UserLoginDetails where MobileNo='" + txtMob.Text + "'";
                    string number = Convert.ToString(cc.ExecuteScalar(selectquotationno));
                    if (number == txtMob.Text)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Mobile Number is Already Exist ')", true);
                    }
                    else
                    {
                        SqlCommand cmd = new SqlCommand("SP_InsetLogindetails");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();


                        cmd.Parameters.AddWithValue("@UserName", txtName.Text);
                        cmd.Parameters.AddWithValue("@MobileNo", txtMob.Text);
                        cmd.Parameters.AddWithValue("@Stae", ddlstate.SelectedValue);
                        cmd.Parameters.AddWithValue("@District", ddldistrict.SelectedValue);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                        if (A == -1)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                        }
                        DataBaseConnection.Close();
                        //Clear();

                    }

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
        protected void GVUserLogin_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUserLogin.PageIndex = e.NewPageIndex;
            GridView();
        }
        protected void btnPswd_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    Button btn = (Button)sender;
                    GridViewRow row = (GridViewRow)btn.NamingContainer;
                    int i = Convert.ToInt32(row.RowIndex);

                    string MobNo = Convert.ToString(GVUserLogin.Rows[i].Cells[2].Text);
                   
                    //string ID1 = cc.DESEncrypt(ID);

                    SqlCommand cmd = new SqlCommand("SP_GetPswd");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MobNo", MobNo);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string password = Convert.ToString(ds.Tables[0].Rows[0]["usrPassword"]);
                        password = cc.DESDecrypt(password);

                        string msg = "Your NEEDLY Portal Password is -" + password + " - Thank You";
                        cc.SendSMSezeetestpinnacle(MobNo, msg);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}