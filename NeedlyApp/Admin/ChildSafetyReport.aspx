﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="ChildSafetyReport.aspx.cs" Inherits="NeedlyApp.ChildSafetyReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Child Safety Installation Report<small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                </div>
            </div>
                   <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                                 <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        User Role:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlRole" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                             <asp:ListItem Text="Parent" Value="1"></asp:ListItem>
                                             <asp:ListItem Text="Child" Value="2"></asp:ListItem>
                                        </asp:DropDownList><br />
                                         
                                
                            </div>
                    <div class="row">
           <div class="panel-body">   
    <div class="col-md-12">
                     <div class="box-body col-sm-9">
                                                      <asp:TextBox ID="txtreferralwise" PlaceHolder="Enter Your Mobile Number" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="box-body col-sm-3">
                                                 <asp:Button ID="btnreferralwise" OnClick="btnreferralwise_Click" CssClass=" btn-primary"   runat="server" Text="Search By Referralwise" />
                     
                    </div>
                                       </div></div>
                                    </div>
         </div>
               </div>

                     <div class="box-body col-sm-6">
                                               <div class="form-group">
                                 
                                    <div class="col-sm-9">
                                     <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" class="btn btn-success"  Text="Search" />
                                      <asp:Button ID="btnExcel" runat="server"  class="btn btn-success" Visible="false"  OnClick="btnExcel_Click" Text="Export To Excel" />
                                        <asp:Button ID="btnExcel1" runat="server"  class="btn btn-success"  Visible="false"  OnClick="btnExcel1_Click" Text="Export To Excel" />
                                        <asp:Button ID="btnExcel2" runat="server"  class="btn btn-success"  Visible="false" OnClick="btnExcel2_Click" Text="Export To Excel" />
                                                                                  <asp:Button ID="btnRefresh" OnClick="btnRefresh_Click" CssClass="btn btn-success"   runat="server" Text="Refresh" />
                      </div>              
                    </div>
                                         
                    <div class="box-body col-sm-6">
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </div>
                                    </div>
               </div>
     <div class="row">
           <div class="panel-body">   
    <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:TextBox ID="txtName" CssClass="form-control" PlaceHolder="Enter Parent Mobile Number" runat="server"></asp:TextBox>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="btnsearchbyname" OnClick="btnsearchbyname_Click" CssClass=" btn-primary"   runat="server" Text="Search By Mobile Number" />
                        
                    </div>
                    
                </div>
                  
               
                   
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </div>
                </div>

                 <div class="col-md-12">
                    <div class="box-body col-sm-5">
                       
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label4" runat="server">Count:</asp:Label>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>
                 
                <br />
                <br />
                 <div class="col-md-12">
                     <br />
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report"  runat="server"  OnPageIndexChanging="GVReg_Report_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>  
                          <asp:BoundField DataField="ParentName" HeaderText="Parent Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        <asp:BoundField DataField="ParentMobNo" HeaderText="Parent Mobile">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        
                         <asp:BoundField DataField="ChildName" HeaderText="Child Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ChildMob" HeaderText="Child Mobile">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="Child Added Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                          <asp:BoundField DataField="ChildJoinDate" HeaderText="Child Join Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="RefMobileNo" HeaderText="Under By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                   <br /> 
                 <div class="col-md-12">
                      <br />
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report1"  runat="server" OnPageIndexChanging="GVReg_Report1_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                          <asp:BoundField DataField="ParentName" HeaderText="Parent Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        <asp:BoundField DataField="ParentMobNo" HeaderText="Parent Mobile">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        
                          <asp:BoundField DataField="ChildName" HeaderText="Child Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ChildMob" HeaderText="Child Mobile">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="Child Added Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ChildJoinDate" HeaderText="Child Join Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                <div class="col-md-12">
                     <br />
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report2"  runat="server"  OnPageIndexChanging="GVReg_Report2_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>

                            </ItemTemplate>
                        </asp:TemplateField>  
                         <asp:BoundField DataField="ParentName" HeaderText="Under by Reffered name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        <asp:BoundField DataField="ParentMobNo" HeaderText="Under by Reffered Mob No">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         
                         <asp:BoundField DataField="ChildName" HeaderText="User name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ChildMob" HeaderText="User Mob No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="Refferal join date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                         
                         
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                  
            </div>
        </div>
    </div>
</asp:Content>
