﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Generic;

namespace NeedlyApp.Admin
{
    public partial class CategoryMaster : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        string Shoptype = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
                ShopType();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowCategoryMaster");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVCategoryMaster.DataSource = ds.Tables[0];
                        GVCategoryMaster.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void ShopType()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetShopCategoryNew");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        chkShoptype.DataSource = ds.Tables[0];
                        chkShoptype.DataTextField = "ItemName";
                        chkShoptype.DataValueField = "Id";
                        chkShoptype.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string CategoryName = dt.Rows[i]["CategoryName"].ToString();
                        CategoryName = CategoryName.Trim();

                        string Description = dt.Rows[i]["Description"].ToString();
                        Description = Description.Trim();

                        //List<string> lblmobile = new List<string>();
                        //for (int c = 0; c < chkShoptype.Items.Count; c++)
                        //{
                        //    if (chkShoptype.Items[c].Selected == true)
                        //    {
                        //        Shoptype = Shoptype + "," + chkShoptype.Items[c].Value;
                        //        //SubjectName = SubjectName + "," + chkShoptype.Items[c].Text;
                        //    }
                        //}
                        List<String> ShopStrList = new List<string>();
                        // Loop through each item.
                        string s1 = string.Empty;
                        foreach (ListItem item in chkShoptype.Items)
                        {
                            if (item.Selected)
                            {
                                // If the item is selected, add the value to the list.
                                s1 = item.Value;
                                SqlCommand cmd = new SqlCommand("SP_InsertCategorymaster", DataBaseConnection);
                                cmd.CommandType = CommandType.StoredProcedure;
                                DataBaseConnection.Open();

                                cmd.Parameters.AddWithValue("@CategoryName", CategoryName);
                                cmd.Parameters.AddWithValue("@ShopType", s1);
                                cmd.Parameters.AddWithValue("@Description", Description);
                                cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                                int A = cmd.ExecuteNonQuery();
                                con.Close();
                                DataBaseConnection.Close();
                                GridView();
                                if (A == -1)
                                {
                                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                                }
                            }
                            else
                            {

                            }
                           
                        }
                        
                       
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/CategoryMaster.xlsx", false);
        }
    }
}