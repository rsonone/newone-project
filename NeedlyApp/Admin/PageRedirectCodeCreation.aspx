﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="PageRedirectCodeCreation.aspx.cs" Inherits="NeedlyApp.Admin.PageRedirectCodeCreation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Page Redirect Code Creation <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                       <div class="clearfix"></div>
                  </div>
                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Project: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlprojectname" CssClass="form-control"  OnSelectedIndexChanged="ddlprojectname_SelectedIndexChanged" runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="Needly"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="TrueVoter"></asp:ListItem>                                        
                                    </asp:DropDownList><br />
                          </div></div></div>
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Topic: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddltopic" CssClass="form-control"  runat="server"  AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="Autocall"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="Automessage"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="Appointment"></asp:ListItem>
                                 <asp:ListItem Value="4" Text="Message Compaign"></asp:ListItem>
                                 <asp:ListItem Value="5" Text="Schedular"></asp:ListItem>
                                 <asp:ListItem Value="6" Text="BPO Solution"></asp:ListItem>
                                         
                                    </asp:DropDownList>
                                     <asp:DropDownList ID="ddltopictruevoter" CssClass="form-control" Visible="false" runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                 <asp:ListItem Value="1" Text="Voter List Search"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="Candidate Expenses"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="Advance Facility"></asp:ListItem>
                                
                                    </asp:DropDownList><br />
                          </div></div></div>
                           
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Compaign Name: </label>
                                <div class="col-sm-9">
                                   <asp:TextBox ID="txtcompaignname" CssClass="form-control"  runat="server"></asp:TextBox><br />
                          </div></div></div>
               <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Compaign Image: </label>
                                <div class="col-sm-9">
                                   <asp:FileUpload ID="FileUpload1" runat="server" /><br />
                          </div></div></div>
               </div></div> </div>
                     <asp:TextBox ID="txttopic" CssClass="form-control"  runat="server" Visible="false"></asp:TextBox>
                    <div class="col-md-12">
           <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click"  />
                             </div>
                        </div>
                    
                             <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddCat"  runat="server" OnPageIndexChanging="GVAddCat_PageIndexChanging" PagerStyle-VerticalAlign="Middle"  PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                     <%--   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     --%>
                         <asp:BoundField DataField="Id" HeaderText="ID" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Topic" HeaderText="Topic Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Compaignname" HeaderText="Compaign Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Compaignimage" HeaderText="Compaign Image">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Compaignpagelink" HeaderText="Compaignpage Link">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Createddate" HeaderText="CreatedDate">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <%-- <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click" />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>--%>
                       
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                         </div>
                    </div>
                    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>

