﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddStaff : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        string MobileNo = string.Empty;
        string dirPath = string.Empty;
        string fullName = string.Empty;
        int updateCount = 0, insertCount = 0, totalCount = 0;
        string inupcount = string.Empty;
        string returnString = string.Empty;
        int NotinsertCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlGrid1.Visible = true;
                pnlGrid2.Visible = false;
                pnlGrid3.Visible = false;
                pnlStaff.Visible = true;
                pnlHawkers.Visible = false;
                pnClient.Visible = false;

                //FillGrid();

                //BindGrupcodesearch();

            }
        }

        public void GridView_1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowStaffDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvStaff.DataSource = ds.Tables[0];
                        GvStaff.DataBind();

                        Label3.Text = Convert.ToString(ds.Tables[0].Rows.Count);
                        Label1.Visible = true;
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public void GridView_2()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowRegAddressDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvClient.DataSource = ds.Tables[0];
                        gvClient.DataBind();

                        Label3.Text = Convert.ToString(ds.Tables[0].Rows.Count);
                        Label3.Visible = true;
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        public void GridView_3()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowRegAddressDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        gvHawker.DataSource = ds.Tables[0];
                        gvHawker.DataBind();

                        Label3.Text = Convert.ToString(ds.Tables[0].Rows.Count);
                        Label1.Visible = true;
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void rdSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdSelect.SelectedValue == "0")
            {
                Hidden1();

            }
            else if (rdSelect.SelectedValue == "1")
            {
                Hidden2();
            }
            else
            {
                Hidden3();
            }

        }
        public void Hidden1()
        {
            pnlGrid1.Visible = true;
            pnlGrid2.Visible = false;
            pnlGrid3.Visible = false;
            pnlStaff.Visible = true;
            pnlHawkers.Visible = false;
            pnClient.Visible = false;
            GridView_1();
        }
        public void Hidden2()
        {
            pnlGrid1.Visible = false;
            pnlGrid2.Visible = true;
            pnlGrid3.Visible = false;
            pnlStaff.Visible = false;
            pnlHawkers.Visible = false;
            pnClient.Visible = true;
            GridView_2();
        }
        public void Hidden3() {
            pnlGrid1.Visible = false;
            pnlGrid2.Visible = false;
            pnlGrid3.Visible = true;
            pnlStaff.Visible = false;
            pnlHawkers.Visible = true;
            pnClient.Visible = false;
            GridView_3();
        }

        protected void lnkbtnView_Click(object sender, EventArgs e)
        {

        }

        protected void lnkbtndelete_Click(object sender, EventArgs e)
        {

        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Name of Labour$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            DataSet ds = new DataSet();

                            objAdapter1.Fill(ds);
                            DataTable dt = new DataTable();
                            dt = ds.Tables[0];

                            myExcelConn.Close();
                            if (rdSelect.SelectedValue == "0")
                            {
                                InsertData(dt);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }

                else {

                    try
                    {
                        if (rdSelect.SelectedValue == "0")
                        {
                            string strCheckValue = "";
                            if (cbMon.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbMon.Text;
                            }
                            if (cbThu.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbThu.Text;
                            }
                            if (cbWed.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbWed.Text;
                            }
                            if (cbTue.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbTue.Text;
                            }
                            if (cbSat.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbSat.Text;
                            }
                            if (cbFri.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbFri.Text;
                            }
                            if (cbNone.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbNone.Text;
                            }

                            strCheckValue = strCheckValue.TrimStart(',');
                            SqlCommand cmd = new SqlCommand("sp_Reportinsert", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Mobile", txtStaffMobile.Text);
                            cmd.Parameters.AddWithValue("@Name", txtName.Text);
                            cmd.Parameters.AddWithValue("@Post", ddlPost.SelectedValue);
                            cmd.Parameters.AddWithValue("@Shift", ddlShift.SelectedValue);
                            cmd.Parameters.AddWithValue("@Dept", ddlDept.SelectedValue);
                            cmd.Parameters.AddWithValue("@Location", ddlLoc.SelectedValue);
                            cmd.Parameters.AddWithValue("@Status", ddlStatus.SelectedValue);
                            cmd.Parameters.AddWithValue("@Under", ddlUnder.SelectedValue);
                            cmd.Parameters.AddWithValue("@Payment", ddlPayment.SelectedValue);
                            cmd.Parameters.AddWithValue("@Basic", txtBasic.Text);
                            cmd.Parameters.AddWithValue("@Date", txtDate.Text);
                            cmd.Parameters.AddWithValue("@TA", txtTA.Text);
                            cmd.Parameters.AddWithValue("@EPF", txtEPF.Text);
                            cmd.Parameters.AddWithValue("@ESIC", txtESIC.Text);
                            cmd.Parameters.AddWithValue("@Petrol", txtPetrol.Text);
                            cmd.Parameters.AddWithValue("@Week", strCheckValue);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            Hidden1();
                        }
                        else if (rdSelect.SelectedValue == "1")
                        {
                            string strCheckValue = "";
                            if (cbSMS.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbSMS.Text;
                            }
                            if (cmMail.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cmMail.Text;
                            }
                            if (CbWhatsApp.Checked)
                            {
                                strCheckValue = strCheckValue + "," + CbWhatsApp.Text;
                            }

                            strCheckValue = strCheckValue.TrimStart(',');
                            SqlCommand cmd = new SqlCommand("sp_Reportinsert", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Mobile", txtMob.Text);
                            cmd.Parameters.AddWithValue("@Name", txtOfc.Text);
                            cmd.Parameters.AddWithValue("@Owner", txtOwner.Text);
                            cmd.Parameters.AddWithValue("@OfcNo", txtOfcNo.Text);
                            cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
                            cmd.Parameters.AddWithValue("@Pincode", txtPincode.Text);
                            cmd.Parameters.AddWithValue("@Sector", txtSector.Text);
                            cmd.Parameters.AddWithValue("@Area", txtArea.Text);
                            cmd.Parameters.AddWithValue("@OfficeType", rdbCom.SelectedValue);
                            cmd.Parameters.AddWithValue("@Msg", strCheckValue);
                            cmd.Parameters.AddWithValue("@Date", txtDate.Text);
                            cmd.Parameters.AddWithValue("@TA", txtTA.Text);
                            cmd.Parameters.AddWithValue("@EPF", txtEPF.Text);
                            cmd.Parameters.AddWithValue("@ESIC", txtESIC.Text);
                            cmd.Parameters.AddWithValue("@Petrol", txtPetrol.Text);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            Hidden2();
                        }
                        else
                        {

                            string strCheckValue = "";
                            if (cbSMS.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cbSMS.Text;
                            }
                            if (cmMail.Checked)
                            {
                                strCheckValue = strCheckValue + "," + cmMail.Text;
                            }
                            if (CbWhatsApp.Checked)
                            {
                                strCheckValue = strCheckValue + "," + CbWhatsApp.Text;
                            }

                            strCheckValue = strCheckValue.TrimStart(',');
                            SqlCommand cmd = new SqlCommand("sp_Reportinsert", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@Mobile", txtmobHawker.Text);
                            cmd.Parameters.AddWithValue("@Name", txtOfcNamehakers.Text);
                            cmd.Parameters.AddWithValue("@HawkerName", txtHawker.Text);
                            cmd.Parameters.AddWithValue("@Flat", txtFlat.Text);
                            cmd.Parameters.AddWithValue("@Email", txtmail.Text);
                            cmd.Parameters.AddWithValue("@StartBooking", txtStartBooking.Text);
                            cmd.Parameters.AddWithValue("@Delivery", txtDelivery.Text);
                            cmd.Parameters.AddWithValue("@Collection", txtCollection.Text);
                            cmd.Parameters.AddWithValue("@rdHouseType", rdHouseType.SelectedValue);
                            cmd.Parameters.AddWithValue("@Msg", strCheckValue);


                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                            Hidden3();
                        }
                    }
                    catch (Exception ex)
                    {
                        // EL.SendErrorToText(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                ///EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
            Response.Redirect("AddStaff.aspx");

        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/UploadExcel/Staff_Excel_N.S.P.L .xls");

        }
        
        protected void btnCancle_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddStaff.aspx");
        }


        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {

                        string FullName = dt.Rows[i]["Full Name"].ToString();
                        FullName = FullName.Trim();


                        string MobileNo = dt.Rows[i]["Mobile No"].ToString();
                        MobileNo = MobileNo.Trim();

                        //string UserRole = dt.Rows[i]["UserRole"].ToString();
                        // UserRole = UserRole.Trim();
                        string UserRole = "1";

                        string AddUnder = dt.Rows[i]["Add Under"].ToString();
                        AddUnder = AddUnder.Trim();

                        string PaymentType = dt.Rows[i]["Payment Type (1- Monthly, 2- per Hour Basis, 3- Daily, 4-Work Ba"].ToString();
                        PaymentType = PaymentType.Trim();

                        //string Status = dt.Rows[i]["Status"].ToString();
                        //if (Status == "Active") { Status = "1"; } else { Status = "2"; }
                        //Status = Status.Trim();
                        string Status = "1";

                        //string RegId = dt.Rows[i]["RegId"].ToString();
                        //RegId = RegId.Trim(); // Created By basic Reg id
 
                        string ShopType = dt.Rows[i]["Shop type"].ToString();
                        ShopType = ShopType.Trim();

                        //string Salary = dt.Rows[i]["Salary"].ToString();
                        //Salary = Salary.Trim();

                        //string SalaryCycle = dt.Rows[i]["SalaryCycle"].ToString();
                        //SalaryCycle = SalaryCycle.Trim();

                        //string AmountType = dt.Rows[i]["AmountType"].ToString();
                        //AmountType = AmountType.Trim();

                        //string Amount = dt.Rows[i]["Amount"].ToString();
                        //Amount = Amount.Trim();
                        string JoiningDate = dt.Rows[i]["Joining_date"].ToString();
                        JoiningDate = JoiningDate.Trim();


                        string Shop_OwnweNo = dt.Rows[i]["Shop Owner Mobile No"].ToString();
                        Shop_OwnweNo = Shop_OwnweNo.Trim();

                        string Shift = dt.Rows[i]["Shift (1- First Shift, 2-Second Shift, 3-third Shift, 4- General"].ToString();
                        Shift = Shift.Trim();
                        //string OfficeId = dt.Rows[i]["OfficeId"].ToString();
                        //OfficeId = OfficeId.Trim();
                        string DeptId = dt.Rows[i]["DepID"].ToString();
                        DeptId = DeptId.Trim();
                        string Basic_Pay = dt.Rows[i]["Basic_Pay"].ToString();
                        Basic_Pay = Basic_Pay.Trim();
                        //string Joining_date = dt.Rows[i]["Joining_date"].ToString();
                        //Joining_date = Joining_date.Trim();
                        string Shift_hours = dt.Rows[i]["Shift_hours"].ToString();
                        Shift_hours = Shift_hours.Trim();
                        string Weekly_off = dt.Rows[i]["Week_off (0-None, 1- Sunday, ---- 7- Saturday) "].ToString();
                        Weekly_off = Weekly_off.Trim();
                        string TA = dt.Rows[i]["TA"].ToString();
                        TA = TA.Trim();
                        string PetrolAllowance = dt.Rows[i]["PetrolAllowance"].ToString();
                        PetrolAllowance = PetrolAllowance.Trim();

                        string EPF = dt.Rows[i]["EPF"].ToString();
                        EPF = EPF.Trim();
                        string ESIC = dt.Rows[i]["ESIC"].ToString();
                        ESIC = ESIC.Trim();
                        string HSN_SAC_NO = dt.Rows[i]["HSN SAC_No"].ToString();
                        HSN_SAC_NO = HSN_SAC_NO.Trim();
                        string ClientMobile = dt.Rows[i]["Client Office Head Mobile No"].ToString();
                        ClientMobile = ClientMobile.Trim();
                        //string Permissions = dt.Rows[i]["Permissions"].ToString();
                        //Permissions = Permissions.Trim();

                        string Query = "Select top 1 [EzeeDrugAppId] from [DBNeedly].[dbo].[EzeeDrugsAppDetail] where mobileNo ='" + Session["username"].ToString() + "' and keyword='NEEDLY'";
                        string RegId = cc.ExecuteScalarDrug(Query);
                        //if (Res != "")
                        //{
                        //    //updateCount++;
                        //}

                        //SqlCommand cmd = new SqlCommand("SP_UploadRegDetailsNew", DataBaseConnection);
                        //SqlCommand cmd = new SqlCommand("SP_UploadSTAFFDetailsExcel", DataBaseConnection);
                        SqlCommand cmd = new SqlCommand("SP_UploadSTAFFDetailsExcelNew", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@FullName", FullName.ToString());
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo.ToString());
                        cmd.Parameters.AddWithValue("@UserRole", UserRole.ToString());
                        cmd.Parameters.AddWithValue("@AddUnder", AddUnder.ToString());
                        cmd.Parameters.AddWithValue("@PaymentType", PaymentType.ToString());
                        cmd.Parameters.AddWithValue("@RegId", RegId.ToString());
                        cmd.Parameters.AddWithValue("@ShopType", ShopType.ToString());
                        cmd.Parameters.AddWithValue("@Joining_date", JoiningDate.ToString());
                        cmd.Parameters.AddWithValue("@Status", Status.ToString());
                        cmd.Parameters.AddWithValue("@Shop_OwnweNo", Shop_OwnweNo.ToString());
                        cmd.Parameters.AddWithValue("@Shift", Shift.ToString());
                        cmd.Parameters.AddWithValue("@DeptId", DeptId.ToString());
                        cmd.Parameters.AddWithValue("@Basic_Pay", Basic_Pay.ToString());
                        //cmd.Parameters.AddWithValue("@Joining_date", JoiningDate.ToString());
                        cmd.Parameters.AddWithValue("@Shift_hours", Shift_hours.ToString());
                        cmd.Parameters.AddWithValue("@Weekly_off", Weekly_off.ToString());
                        cmd.Parameters.AddWithValue("@TA", TA.ToString());
                        cmd.Parameters.AddWithValue("@PetrolAllowance", PetrolAllowance.ToString());
                        cmd.Parameters.AddWithValue("@EPF", EPF.ToString());
                        cmd.Parameters.AddWithValue("@ESIC", ESIC.ToString());
                        cmd.Parameters.AddWithValue("@HSN_SAC_NO", HSN_SAC_NO.ToString());
                        cmd.Parameters.AddWithValue("@ClientMobile", ClientMobile.ToString());
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());



                        //cmd.Parameters.AddWithValue("@Salary", Salary.ToString());
                        // cmd.Parameters.AddWithValue("@SalaryCycle", SalaryCycle.ToString());
                        //cmd.Parameters.AddWithValue("@AmountType", AmountType.ToString());
                        // cmd.Parameters.AddWithValue("@Amount", Amount.ToString());
                        //cmd.Parameters.AddWithValue("@Permissions", Permissions.ToString());
                        // cmd.Parameters.AddWithValue("@OfficeId", OfficeId.ToString());



                        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                        DataBaseConnection.Close();
                        //GridView();

                        insertCount++;

                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }


                        totalCount = insertCount + updateCount + NotinsertCount;
                        lblMessage.Text = "NUMBER OF RECORDS UPLOADED : " + totalCount + " INSERTED : " + insertCount + " AND UPDATED : " + updateCount + " AND NOTINSERTED : " + NotinsertCount;

                    }
                }
                catch (Exception ex)
                {
                    //EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }


        }
    }
}