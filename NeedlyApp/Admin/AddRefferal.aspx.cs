﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddRefferal : System.Web.UI.Page
    {

        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        string DataCode = string.Empty;
        //int updateCount = 0, insertCount = 0, totalCount = 0;
        string inupcount = string.Empty;
        string returnString = string.Empty;
       // int NotinsertCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView();


        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_Showreferal");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddreferal.DataSource = ds.Tables[0];
                        GVAddreferal.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                            Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");
                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                        EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }



            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UploadAddreferal");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Type", rdSelect.SelectedValue);
                    cmd.Parameters.AddWithValue("@Customer_Mobile", txtmobNo.Text);
                    cmd.Parameters.AddWithValue("@Customer_Name", TextBox1.Text);
                    cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                    cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                    //cmd.Parameters.AddWithValue("@CategoryId", ddlCat.SelectedValue);
                    //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    string sql = "select firstname from [DBNeedly].[dbo].[EzeeDrugsAppDetail]  where mobileNo='" + Session["username"].ToString() + "'";
                    string result = Convert.ToString(cc.ExecuteScalar(sql));
                    string sql1 = "select mobileNo from [DBNeedly].[dbo].[EzeeDrugsAppDetail]  where mobileNo='" + Session["username"].ToString() + "'";
                    string result1 = Convert.ToString(cc.ExecuteScalar(sql1));


                    cmd.Parameters.AddWithValue("@ShopUser_Name", result);
                    cmd.Parameters.AddWithValue("@Shop_Mobile", result1);

                    int A = cmd.ExecuteNonQuery();
                    string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }

        }

        public void InsertData(DataTable dt = null)
        {
            try
            {
                for (int i = 0; i <= dt.Rows.Count; i++)
                {

                    string Type = dt.Rows[i]["Type"].ToString();
                    Type = Type.Trim();
                    string Customer_Mobile = dt.Rows[i]["Customer_Mobile"].ToString();
                    Customer_Mobile = Customer_Mobile.Trim();

                    string Customer_Name = dt.Rows[i]["Customer_Name"].ToString();
                    Customer_Name = Customer_Name.Trim();

                   

                    
                    //string[] Name = Fullname.Split(' ', ' ');
                    //if (Name.Length == 1)
                    //{
                    //    FirstName = Name[0];
                    //}
                    //if (Name.Length == 2)
                    //{
                    //    FirstName = Name[0];
                    //    MiddleName = Name[1];
                    //}
                    //if (Name.Length >= 3)
                    //{
                    //    FirstName = " " + Name[0] + " " + Name[1] + " ";
                    //    LastName = Name[2];
                    //}

                  

                    SqlCommand cmd = new SqlCommand("SP_UploadAddreferal", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 120;
                    con.Open();

                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@Customer_Mobile", Customer_Mobile);
                    cmd.Parameters.AddWithValue("@Customer_Name", Customer_Name);
                   

                    cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                    cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    string sql = "select firstname from [DBNeedly].[dbo].[EzeeDrugsAppDetail]  where mobileNo='" + Session["username"].ToString() + "'";
                    string result = Convert.ToString(cc.ExecuteScalar(sql));
                    string sql1 = "select mobileNo from [DBNeedly].[dbo].[EzeeDrugsAppDetail]  where mobileNo='" + Session["username"].ToString() + "'";
                    string result1 = Convert.ToString(cc.ExecuteScalar(sql1));

                    cmd.Parameters.AddWithValue("@Shop_Mobile", result1);
                    cmd.Parameters.AddWithValue("@ShopUser_Name", result);
                    int A = cmd.ExecuteNonQuery();
                    con.Close();
                    string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                    con.Close();


                   // int A = cmd.ExecuteNonQuery();
                    con.Close();
                    cmd.Parameters.Clear();
                    con.Close();
                    GridView();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);
            }
        }

        protected void GVAddreferal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var firstCell1 = e.Row.Cells[1];
                // ViewState["Customers"] = e.Row.Cells[2];
                firstCell1.Controls.Clear();
                firstCell1.Controls.Add(new HyperLink { NavigateUrl = firstCell1.Text, Text = firstCell1.Text });

            }
        }


        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/AddReferal.xlsx", false);

        }

        protected void GVAddreferal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddreferal.PageIndex = e.NewPageIndex;
            GridView();
        }

    }
}