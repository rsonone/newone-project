﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;



namespace NeedlyApp.Admin
{
    public partial class CreateQuotation : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        string MobileNo = string.Empty;
        string dirPath = string.Empty;
        string fullName = string.Empty;
        string base64String = string.Empty;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dt;
                CustomerName();
                categoryname();
                itemname();
                GridView1();

                dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[6] { new DataColumn("Quotation_Grp_Name"), new DataColumn("ItemName"), new DataColumn("Rate"), new DataColumn("Unit"), new DataColumn("Quantity"), new DataColumn("Amount") });
                ViewState["Customers"] = dt;
                this.GridView();

               
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    Button btn = sender as Button;
                    GridViewRow gv = btn.NamingContainer as GridViewRow;
                    lblId.Text = (btn.FindControl("lblId1") as Label).Text;
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = DataBaseConnection;
                    DataBaseConnection.Open();

                    command4.CommandText = "sp_EditQuotationdata";
                    command4.CommandType = System.Data.CommandType.StoredProcedure;
                    command4.Parameters.AddWithValue("@Id", lblId.Text);

                    SqlDataAdapter da4 = new SqlDataAdapter();
                    da4.SelectCommand = command4;
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        txtdate.Text = ds4.Tables[0].Rows[0][2].ToString();
                        ddlcustomer.SelectedItem.Text = ds4.Tables[0].Rows[0][3].ToString();
                        ddlquotationgroup.SelectedItem.Text = ds4.Tables[0].Rows[0][17].ToString();
                        txtquotationid.Text = ds4.Tables[0].Rows[0][1].ToString();
                        txtheadernote.Text = ds4.Tables[0].Rows[0][6].ToString();
                        txtfooternote.Text = ds4.Tables[0].Rows[0][7].ToString();
                        ddlproduct.SelectedItem.Text = ds4.Tables[0].Rows[0][19].ToString();
                        txtrate.Text = ds4.Tables[0].Rows[0][21].ToString();
                        txtunit.Text = ds4.Tables[0].Rows[0][23].ToString();
                        txtquantity.Text = ds4.Tables[0].Rows[0][26].ToString();
                        txttotal.Text = ds4.Tables[0].Rows[0][27].ToString();
                        //txtrate.Text = ds4.Tables[0].Rows[0][21].ToString();
                       // txtquantity.Text = ds4.Tables[0].Rows[0][26].ToString();
                        //txttotal.Text = ds4.Tables[0].Rows[0][8].ToString();
                    }
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void CustomerName()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("sp_getcustomername");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlcustomer.DataSource = ds.Tables[0];
                        ddlcustomer.DataTextField = "Customer_Name";
                        ddlcustomer.DataValueField = "Id";

                        ddlcustomer.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlcustomer.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlcustomer.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlcustomer.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

        public void btnaddproductdata_Click(object sender, EventArgs args)
        {
            pnladdproduct.Visible = true;
            itemname();



        }
        public void btnrefresh_Click(object sender, EventArgs args)
        {
            pnladdproduct.Visible = false;


        }
        protected void GVProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVProduct.PageIndex = e.NewPageIndex;
            GridView();
        }
        protected void GVBanner_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVBanner.PageIndex = e.NewPageIndex;
            GridView1();
        }
        public void categoryname()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("sp_getcategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("Category", 11);
                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlquotationgroup.DataSource = ds.Tables[0];
                        ddlquotationgroup.DataTextField = "Name";
                        //ddlcustomer.DataValueField = "Id";

                        ddlquotationgroup.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlquotationgroup.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlquotationgroup.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlquotationgroup.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }
        protected void txtquotationid_OnTextChanged(object sender, EventArgs e)
        {
            string selectquotationno = "select Quotation_No from [tbl_NewQuotationBasicDetails] where Quotation_No='" + txtquotationid.Text + "' and   CreatedBy='" + Session["username"].ToString() + "'";
            string number = Convert.ToString(cc.ExecuteScalar(selectquotationno));
            if (number == txtquotationid.Text)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Quotation number is already exist ')", true);
            }
            else
            {

            }
        }
        public void itemname()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("sp_getitemname");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlproduct.DataSource = ds.Tables[0];
                        ddlproduct.DataTextField = "ItemName";
                        ddlproduct.DataValueField = "Id";

                        ddlproduct.DataBind();
                        //ddlShop.Items.Insert(01, new ListItem("--Select--", ""));
                        ddlproduct.Items.Insert(0, new ListItem("-- Select --", ""));
                        ddlproduct.SelectedIndex = 0;

                    }
                    else
                    {
                        ddlproduct.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }
        protected void ddlproduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectmrprate = "select Rate from [tbl_InventoryItem] where ItemName = '" + ddlproduct.SelectedItem.Text + "' and CreatedBy='" + Session["username"].ToString() + "'";

            string unit = "select Unit from [tbl_InventoryItem] where ItemName = '" + ddlproduct.SelectedItem.Text + "' and CreatedBy='" + Session["username"].ToString() + "'";

            string amt = Convert.ToString(cc.ExecuteScalar(selectmrprate));

            string unit1 = Convert.ToString(cc.ExecuteScalar(unit));

            string tempunit = Convert.ToString(unit1);

            txtrate.Text = Convert.ToString(amt);

            if (tempunit == "0")
            {
                txtunit.Text = "Gm";
            }
            if (tempunit == "1")
            {
                txtunit.Text = "Kg";
            }
            if (tempunit == "2")
            {
                txtunit.Text = "Litre";
            }
            if (tempunit == "3")
            {
                txtunit.Text = "Item";
            }
            if (tempunit == "4")
            {
                txtunit.Text = "Dozen";
            }
            if (tempunit == "5")
            {
                txtunit.Text = "Packet";
            }
            if (tempunit == "6")
            {
                txtunit.Text = "Meter";
            }
            if (tempunit == "7")
            {
                txtunit.Text = "ft";
            }
            if (tempunit == "8")
            {
                txtunit.Text = "day";
            }
            if (tempunit == "9")
            {
                txtunit.Text = "hour";
            }
            if (tempunit == "10")
            {
                txtunit.Text = "sq.meter";
            }
            if (tempunit == "11")
            {
                txtunit.Text = "sq.ft";
            }
            if (tempunit == "12")
            {
                txtunit.Text = "sq.ft";
            }
            if (tempunit == "13")
            {
                txtunit.Text = "sq.inch";
            }
            if (tempunit == "14")
            {
                txtunit.Text = "sq.cm";
            }
            if (tempunit == "15")
            {
                txtunit.Text = "ml";
            }
            if (tempunit == "16")
            {
                txtunit.Text = "km";
            }
            if (tempunit == "17")
            {
                txtunit.Text = "bunch";
            }
            if (tempunit == "18")
            {
                txtunit.Text = "bundle";
            }
            if (tempunit == "19")
            {
                txtunit.Text = "month";
            }
            if (tempunit == "20")
            {
                txtunit.Text = "year";
            }
            if (tempunit == "21")
            {
                txtunit.Text = "service";
            }
            if (tempunit == "22")
            {
                txtunit.Text = "work";
            }
            if (tempunit == "23")
            {
                txtunit.Text = "box";
            }
            if (tempunit == "24")
            {
                txtunit.Text = "pound";
            }
            if (tempunit == "25")
            {
                txtunit.Text = "gunta";
            }
            if (tempunit == "26")
            {
                txtunit.Text = "minute";
            }
            if (tempunit == "27")
            {
                txtunit.Text = "quintal";
            }
            if (tempunit == "28")
            {
                txtunit.Text = "ton";
            }
            if (tempunit == "29")
            {
                txtunit.Text = "capsul";
            }
            if (tempunit == "30")
            {
                txtunit.Text = "tablet";
            }
            if (tempunit == "31")
            {
                txtunit.Text = "plate";
            }
            if (tempunit == "32")
            {
                txtunit.Text = "inch";
            }
            if (tempunit == "33")
            {
                txtunit.Text = "numbers";
            }
            if (tempunit == "34")
            {
                txtunit.Text = "running ft";
            }


        }




        
        protected void btnaddproduct_Click(object sender, EventArgs e)
        {

            DataTable dt = (DataTable)ViewState["Customers"];
            dt.Rows.Add(ddlquotationgroup.SelectedItem.Text.Trim(), ddlproduct.SelectedItem.Text.Trim(), txtrate.Text.Trim(), txtunit.Text.Trim(), txtquantity.Text.Trim(), txttotal.Text.Trim());
            ViewState["Customers"] = dt;
            this.GridView();
            ddlproduct.SelectedIndex = 0;
            txtrate.Text = string.Empty;
            txtunit.Text = string.Empty;
            txtquantity.Text = string.Empty;
           // txttotal.Text = string.Empty;

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
                {

                    for (int r = 0; r < GVProduct.Rows.Count; r++)
                    {

                        string sqlQuer2 = "update  tbl_NewQuotationItemDetails set Quotation_Server_Id='" + txtquotationid.Text + "',Quotation_Grp_Name='" + ddlquotationgroup.SelectedItem.Text + "',Item_Name='" + GVProduct.Rows[r].Cells[2].Text + "',Price='" + GVProduct.Rows[r].Cells[3].Text + "',Unit='" + GVProduct.Rows[r].Cells[4].Text + "',Quantity='" + GVProduct.Rows[r].Cells[5].Text + "',Amount='" + GVProduct.Rows[r].Cells[6].Text + "',ModifiedBy='" + Session["username"].ToString() + "',ModifiedDate= getdate() where Id='" + lblId.Text + "' ";
                        cc.ExecuteScalarNeedly(sqlQuer2);

                        con.Close();
                    }

                    string sqlQuer3 = "update  [tbl_NewQuotationBasicDetails] set Quotation_No='" + txtquotationid.Text + "',Date='" + txtdate.Text + "',Customer_Name='" + ddlcustomer.SelectedItem.Text + "',Header_Note='" + txtheadernote.Text + "',Footer_Note='" + txtfooternote.Text + "',Grand_Total='" + txttotal.Text + "',ModifiedBy='" + Session["username"].ToString() + "' ,ModifiedDate= getdate() where Id='" + lblId.Text + "'";
                    cc.ExecuteScalarNeedly(sqlQuer3);

                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED SUCCESSFULLY.!!!')", true);

                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED UNSUCCESSFULLY.!!!')", true);
                }
                GridView1();
                ddlproduct.SelectedIndex = 0;
                txtrate.Text = string.Empty;
                txtunit.Text = string.Empty;
                txtquantity.Text = string.Empty;
                txttotal.Text = string.Empty;
                txtdate.Text = string.Empty;
                ddlcustomer.SelectedIndex = 0;
                txtheadernote.Text = string.Empty;
                txtfooternote.Text = string.Empty;
                ddlquotationgroup.SelectedIndex = 0;


            }
            catch (Exception ex)
            {

            }
        }

        DateTime currentdate = System.DateTime.Now;

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_insertitem");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Quotation_No", txtquotationid.Text);
                    cmd.Parameters.AddWithValue("@Date", txtdate.Text);
                    cmd.Parameters.AddWithValue("@Customer_Name", ddlcustomer.SelectedItem.Text);
                    cmd.Parameters.AddWithValue("@Header_Note", txtheadernote.Text);
                    cmd.Parameters.AddWithValue("@Footer_Note", txtfooternote.Text);
                    cmd.Parameters.AddWithValue("@Grand_Total", txttotal.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                    cmd.Parameters.Add("@returnValue", System.Data.SqlDbType.Int);
                    cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    string DataCode = cmd.Parameters["@returnValue"].Value.ToString();



                    for (int r = 0; r < GVProduct.Rows.Count; r++)
                    {

                        string sqlQuer2 = "insert into  tbl_NewQuotationItemDetails (Quotation_Server_Id,Quotation_Grp_Name,Item_Name,Price,Unit,Quantity,Amount,CreatedBy) values('" + DataCode + "','" + ddlquotationgroup.SelectedItem.Text + "','" + GVProduct.Rows[r].Cells[2].Text + "','" + GVProduct.Rows[r].Cells[3].Text + "','" + GVProduct.Rows[r].Cells[4].Text + "','" + GVProduct.Rows[r].Cells[5].Text + "','" + GVProduct.Rows[r].Cells[6].Text + "','" + Session["username"].ToString() + "')";       //'" + txtAssigntoMktperson.Text +"')";
                        cc.ExecuteScalarNeedly(sqlQuer2);

                        con.Close();
                    }


                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    GridView1();


                    ddlproduct.SelectedIndex = 0;
                    txtrate.Text = string.Empty;
                    txtunit.Text = string.Empty;
                    txtquantity.Text = string.Empty;
                    txttotal.Text = string.Empty;
                    //billno++;
                }


                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);

                }
            }
        }

        protected void txtquantity_OnTextChanged(object sender, EventArgs e)
        {
            int a = Convert.ToInt32(txtrate.Text);
            int b = Convert.ToInt32(txtquantity.Text);

            txttotal.Text = (a * b).ToString();
        }
        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_showquotationdata1");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVBanner.DataSource = ds.Tables[0];
                        GVBanner.DataBind();
                    }


                }

                catch (Exception ex)
                {

                }
            }

        }
        protected void GVProduct_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int Id = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["Customers"] as DataTable;
            dt.Rows[Id].Delete();
            ViewState["Customers"] = dt;
            this.GridView();
        }

        protected void GVProduct_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string item = e.Row.Cells[1].Text;
                foreach (Button button in e.Row.Cells[6].Controls.OfType<Button>())
                {
                    if (button.CommandName == "Delete")
                    {
                        button.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item + "?')){ return false; };";
                    }
                }
            }
        }
        public void GridView()
        {
            GVProduct.DataSource = (DataTable)ViewState["Customers"];
            GVProduct.DataBind();
            //using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            //{
            //    try
            //    {
            //        SqlCommand cmd = new SqlCommand("sp_ShowProductdata");
            //        cmd.Connection = DataBaseConnection;
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.AddWithValue("ItemName", ddlproduct.SelectedItem.Text);
            //        SqlDataAdapter da = new SqlDataAdapter();
            //        da.SelectCommand = cmd;
            //        DataSet ds = new DataSet();
            //        da.Fill(ds);
            //        if (ds.Tables[0].Rows.Count > 0)
            //        {
            //            GVProduct.DataSource = ds.Tables[0];
            //            GVProduct.DataBind();
            //        }
            //        //string SQl1 = "select count(*) FROM [DBNeedly].[dbo].[tbl_NeedlyFormSpreadsheetData] where CreatedBy = '" + Session["username"].ToString() + "' and FormId = '" + ddlFormId.SelectedValue + "'";
            //        //string spreadsheetcnt = cc.ExecuteScalar(SQl1);
            //        //lblcntrecords.Text = spreadsheetcnt;

            //    }

            //    catch (Exception ex)
            //    {

            //    }
            //}

        }
    }
}