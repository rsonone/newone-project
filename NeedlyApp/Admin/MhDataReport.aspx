﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="MhDataReport.aspx.cs" Inherits="NeedlyApp.Admin.MhDataReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  MySql Maharashtra Data Report <small></small></h2>
                       <div class="container-fluid">
                       <%-- <div class="pull-right">
                    <a href="tempCategory.aspx" data-toggle="tooltip" title="Add Category" class="btn btn-primary">
                    <i class="fa fa-plus"></i></a><a href="AddTemplate.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
               
                    </div>--%>
        </div>
                    <div class="clearfix"></div>
                  </div>
                       
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     

                <div class="box-body col-sm-3">
                        <div class="form-group">
                               
                              
                                  <asp:TextBox ID="txtFromId" placeholder="Enter from Id" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>


                </div>
                    <asp:Label ID="lbl1" runat="server" Text=""></asp:Label>        
                  <div class="box-body col-sm-3">
                        <div class="form-group">
                               
                                  <asp:TextBox ID="txtToId" placeholder="Enter To Id" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>


                </div>
                      <div class="box-body col-sm-2">
                        <div class="form-group">
                            <asp:Label ID="lbl2" runat="server" Text=""></asp:Label>
                                
                          </div>
                </div>
                     <div class="box-body col-sm-2">
                        <div class="form-group">
                                <asp:Button ID="btnExportToExcel"  OnClick="btnExportToExcel_Click" runat="server" Text="Export To Excel" class="btn btn-success" />
                                
                          </div>
                </div>
                    </div>
               </div></div>
                    <asp:Label ID="lbl3" runat="server" Text="Label"></asp:Label>
                    <asp:Label ID="lbl4" runat="server" Text="Label"></asp:Label>
                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-5">
                        <div class="form-group">
                                <label class="col-sm-5 control-label" for="input-name2">
                                    Enter PIN Code: </label>
                                <div class="col-sm-7">
                                  <asp:TextBox ID="txtPinCode" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div></div>

                     </div>

                <div class="box-body col-sm-2">
                         <asp:Button ID="btnSearchByPinCode" OnClick="btnSearchByPinCode_Click" runat="server" Text="Search" class="btn btn-primary" />
                </div>
                   
                    </div>
               </div></div>

                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-5">
                        <div class="form-group">
                            <asp:Label ID="lblmsh" runat="server" Font-Bold="true" Font-Size="Medium" Text="total records :"></asp:Label>
                              <asp:Label ID="lblCount" Font-Bold="true" Font-Size="Large" ForeColor="Green" runat="server" Text=""></asp:Label>
                               </div>

                     </div>

                
                   
                    </div>
               </div></div>
                  
                    <div class="col-md-12">
                         <br />
                         <br />
                           <div class="table-responsive">
                    <asp:GridView ID="GVMHData" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVMHData_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="20" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> 
                         <asp:BoundField DataField="MOBILE_NUMBER" HeaderText="No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="CUSTOMER_NAME" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CUSTOMER_ADDRESS" HeaderText="Address">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="SERVICE_PROVIDER" HeaderText="SERVICE PROVIDER">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div>

                    </div>
                  
                  </div>
                </div></div>
</asp:Content>
