﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class VaccinationReport : System.Web.UI.Page
    {
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
           {

                GridViewVaccinationData(); 
                BindVaccinationCentre1();
                
           }
           
        }

        public void GridViewVaccinationData()
        {
            using (
                SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ShowVaccinationDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvVaccinationData.DataSource = ds.Tables[0];
                        GvVaccinationData.DataBind();
                    }

                    string SQl1 = "SELECT COUNT(*) FROM Tbl_GenerateToken ";
                    string PhoneContactCount = cc.ExecuteScalarContact(SQl1);
                    lblVcCount.Text = PhoneContactCount;
                    lblVcCount.Visible = true;
                   //lblShow.Visible = true;
                  //  lblExcelCountMsg.Visible = false;
                  // lblWhatsAppMsgCount.Visible = false;
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void GridViewVaccinationDataFoeExcelExport()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_ExportVaccinationDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VaccinationCentreName", ddlVaccinationCenter.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;

                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvVaccinationData.DataSource = ds.Tables[0];
                        GvVaccinationData.DataBind();
                    }

                   
                    //  lblExcelCountMsg.Visible = false;
                    // lblWhatsAppMsgCount.Visible = false;
                }
                catch (Exception ex)
                {

                }
            }
        }

        //public void BindVaccinationCentre()
        //{
        //    using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
        //    {
        //        try
        //        {
        //            SqlCommand cmd = new SqlCommand("Select * from Tbl_GenerateToken");
        //            cmd.Connection = DataBaseConnection;


        //            SqlDataAdapter da = new SqlDataAdapter();
        //            da.SelectCommand = cmd;
        //            DataTable dt = new DataTable();
        //            da.Fill(dt);

        //            ddlVaccinationCenter.DataSource = dt;
        //            ddlVaccinationCenter.DataBind();
        //            ddlVaccinationCenter.DataTextField = "VaccinationCentreName";
        //            ddlVaccinationCenter.DataValueField = "Id";
        //            ddlVaccinationCenter.DataBind();


        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }




        //}

        public void BindVaccinationCentre1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select * from Tbl_GenerateToken");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    ddlVcCentre.DataSource = dt;
                    ddlVcCentre.DataBind();
                    ddlVcCentre.DataTextField = "VaccinationCentreName";
                    ddlVcCentre.DataValueField = "Id";
                    ddlVcCentre.DataBind();

                   



                }
                catch (Exception ex)
                {

                }
            }




        }
        public void BindVaccinationCentreFromToken()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_BindFromToken");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VaccinationCentre", ddlVcCentre.SelectedItem.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    ddlFromToken.DataSource = dt;
                    ddlFromToken.DataBind();
                    ddlFromToken.DataTextField = "VaccinationToken";
                    ddlFromToken.DataValueField = "Id";
                    ddlFromToken.DataBind();


                }
                catch (Exception ex)
                {

                }
            }




        }

        public void BindVaccinationCentreToToken()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select * from Tbl_GenerateToken where VaccinationCentreName = '" + ddlVcCentre.SelectedItem.Text + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    ddlToToken.DataSource = dt;
                    ddlToToken.DataBind();
                    ddlToToken.DataTextField = "VaccinationToken";
                    ddlToToken.DataValueField = "Id";
                    ddlToToken.DataBind();


                }
                catch (Exception ex)
                {
                   // Response.Redirect("");
                }
            }




        }

       
       
        //protected void btnSearchByMobileNo_Click(object sender, EventArgs e)
        //{
        //    //GridViewForSerch();
        //    //break;
        //    using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
        //    {
        //        try
        //        {
        //            SqlCommand cmd = new SqlCommand("SP_GetSearchByMobileNo");
        //            cmd.Connection = DataBaseConnection;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Mobile", txtMobileExcelNumber.Text);

        //            SqlDataAdapter da = new SqlDataAdapter();
        //            da.SelectCommand = cmd;
        //            DataSet ds = new DataSet();
        //            da.Fill(ds);
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                GvVaccinationData.DataSource = ds.Tables[0];
        //                GvVaccinationData.DataBind();
        //            }
        //            else
        //            {
        //                lblNoData.Text = "No rocord found for Mobile No.  '" + txtMobileExcelNumber.Text + "'";
        //            }


        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }
        //}

        protected void btnSearchByCenter_Click(object sender, EventArgs e)
        {
            GridViewForSerchByCentre();
            //GridViewVaccinationData();
        }

        protected void btnSearchByCentreAndToken_Click(object sender, EventArgs e)
        {

        }

        //public void GridViewForSerch()
        //{
        //    using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
        //    {
        //        try
        //        {
        //            SqlCommand cmd = new SqlCommand("SP_GetSearchByMobileNo");
        //            cmd.Connection = DataBaseConnection;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@Mobile", txtMobileExcelNumber.Text);

        //            SqlDataAdapter da = new SqlDataAdapter();
        //            da.SelectCommand = cmd;
        //            DataSet ds = new DataSet();
        //            da.Fill(ds);
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                GvVaccinationData.DataSource = ds.Tables[0];
        //                GvVaccinationData.DataBind();
        //            }
        //            else
        //            {
        //                lblNoData.Text = "No rocord found for Mobile No.  '" + txtMobileExcelNumber.Text + "'";
        //            }

                    
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }
        //}

        public void GridViewForSerchByCentre()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetSearchByCentre");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@VaccinationCentre", ddlVaccinationCenter.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvVaccinationData.DataSource = ds.Tables[0];
                        GvVaccinationData.DataBind();
                    }
                    else
                    {
                        lblNoData.Text = "No rocord found for VaccinationCentre name  '" + ddlVaccinationCenter.SelectedItem.Text + "'";
                    }


                }
                catch (Exception ex)
                {

                }
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //required to avoid the runtime error "  
            //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
        }
        protected void btnExcelExport_Click(object sender, EventArgs e)
        {

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=VaccinationReportDetails.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "image/jpeg";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

            //Response.AddHeader("content-disposition",
            //    string.Format("attachment; filename={0}", "AppScoreResult.xls"));
            //Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GvVaccinationData.AllowPaging = false;
            GvVaccinationData.PageIndex = 0;
            //gvUserInfo.Columns[12].Visible = false;
            //GVScoreResult.Columns[13].Visible = false;
            GetData1();
            //Change the Header Row back to white color
            GvVaccinationData.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GvVaccinationData.HeaderRow.Cells.Count; i++)
            {
                GvVaccinationData.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GvVaccinationData.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        public void GetData1()
        {
            GridViewVaccinationDataFoeExcelExport();
        }

        //public void BindVaccinationCentreFoeExcelExport()
        //{
        //    using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
        //    {
        //        try
        //        {
        //            SqlCommand cmd = new SqlCommand("Select * from Tbl_GenerateToken");
        //            cmd.Connection = DataBaseConnection;


        //            SqlDataAdapter da = new SqlDataAdapter();
        //            da.SelectCommand = cmd;
        //            DataTable dt = new DataTable();
        //            da.Fill(dt);

        //            ddlExportExcelVCname.DataSource = dt;
        //            ddlExportExcelVCname.DataBind();
        //            ddlExportExcelVCname.DataTextField = "VaccinationCentreName";
        //            ddlExportExcelVCname.DataValueField = "Id";
        //            ddlExportExcelVCname.DataBind();


        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }




        //}

        

        protected void GvVaccinationData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GvVaccinationData.PageIndex = e.NewPageIndex;
            //GridViewVaccinationData();
            GridViewForSerchByCentre();
        }

       
    }
}