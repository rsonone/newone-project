﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="NeedlyApp.Admin.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <header class="masthead bg-primary text-white text-center" style="background-image:url(assets/img/portfolio/bg3.png)">
            <div class="container d-flex align-items-center flex-column">
            
                <!-- Masthead Avatar Image-->
                <img class="masthead-avatar mb-5" src="assets/img/portfolio/qr.jpg" alt="..." />
                <!-- Masthead Heading-->
                <%--<h1 class="masthead-heading text-uppercase mb-0">Abhinav It Solutions</h1>--%>
                <h1 class="masthead-heading text-uppercase mb-0">
                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>

                </h1>
                <!-- Icon Divider-->
                <div class="divider-custom divider-light">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Masthead Subheading-->
                <p class="masthead-subheading font-weight-light mb-0">Dot Net Developer</p>

                 <!-- Footer Social Icons-->
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <h4 class="text-uppercase mb-4"></h4>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fas fa-phone"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fas fa-envelope-open"></i></a>
                        <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="far fa-comment-alt-dots"></i></a>
                       <%-- <a class="btn btn-outline-light btn-social mx-1" href="#!"><i class="fab fa-fw fa-dribbble"></i></a>--%>
                    </div>
                    <!-- Footer About Text-->
            </div>
        </header>
</asp:Content>
