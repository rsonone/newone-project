﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" enableEventValidation="false" AutoEventWireup="true" CodeBehind="NeedlyMarketingPerson.aspx.cs" Inherits="NeedlyApp.Admin.NeedlyMarketingPerson" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
 <link href="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js" rel="stylesheet"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Add Customer and Marketing Person<small></small></h2>
                       <div class="container-fluid">
                       
        </div>
                    <div class="clearfix"></div>
                  </div>
                 
                   
                        <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select: </label>
                                <div class="col-sm-9">
                                    <asp:RadioButtonList ID="rbtnRole" OnSelectedIndexChanged="rbtnRole_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true"  runat="server">
                                        <asp:ListItem Selected="True" Value="2">&nbsp;&nbsp; Customer &nbsp;&nbsp;</asp:ListItem>
                                         <asp:ListItem Value="1">&nbsp;&nbsp; Marketing Person &nbsp;&nbsp;</asp:ListItem>           
                                    </asp:RadioButtonList>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         

                </div>

                    </div>
               </div></div> 


                           
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Mobile Number: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtmobileNumber" runat ="server" CssClass="form-control"></asp:TextBox>
                                  
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Name: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtname" runat ="server" CssClass="form-control"></asp:TextBox>
                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                         ControlToValidate="txtname" ForeColor="Red"
                                        ErrorMessage="Name is required field"></asp:RequiredFieldValidator>--%>
                                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                         ControlToValidate="txtname" ForeColor="Red" ValidationExpression="^[A-Za-z]*$"
                                        ErrorMessage="only characters allowed"></asp:RegularExpressionValidator>--%>
                          </div></div>     

                </div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Address: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtAddress" runat ="server" CssClass="form-control"></asp:TextBox>
                                 
                          </div></div></div>

                <div class="box-body col-sm-6">
                         

                </div>

                    </div>
               </div></div>
                    <asp:Panel ID="pnlMarketingPerson" Visible="false"  runat="server">
                   
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Sequence No.: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtSequenceNo" runat ="server" CssClass="form-control"></asp:TextBox>
                                 
                          </div></div></div>

                <div class="box-body col-sm-6">
                        <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Capacity: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtCapacity" runat ="server" CssClass="form-control"></asp:TextBox>
                                 
                          </div></div>  

                </div>

                    </div>
               </div></div> 

                          <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Is Working: </label>
                                <div class="col-sm-9">
                                    <asp:RadioButtonList ID="rbtnIsWorking" RepeatDirection="Horizontal"  runat="server">
                                         <asp:ListItem Value="1">&nbsp;&nbsp; Yes &nbsp;&nbsp;</asp:ListItem>
                                         <asp:ListItem Value="2">&nbsp;&nbsp; No &nbsp;&nbsp;</asp:ListItem>
                                                   
                                    </asp:RadioButtonList>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Add Under: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtAddUnder" runat ="server" CssClass="form-control"></asp:TextBox>
                                 
                          </div></div>

                </div>

                    </div>
               </div></div> 
                    </asp:Panel>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                
                         </div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                             <div class="box-body col-sm-4">
                              <asp:Button ID="btnaddmarketingperson" OnClick="btnaddmarketingperson_Click"  runat="server" Text="Submit" class="btn btn-success"  />
                              <div class="box-body col-sm-4">
                            
                        </div> 
                        </div> 
                            
                             </div></div>

                    </div>
               </div></div>

                    <asp:Panel ID="pnlCount" Visible="false" runat="server">
                   <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total Customers Added </label>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblCustCount" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Access Permitted customers: </label>
                                <div class="col-sm-3">
                                  <asp:Label ID="lblAccessCount" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                                 
                          </div></div>     

                </div>

                    </div>
               </div></div> 
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total Active Customers </label>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblActiveCust" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total DeActive customers: </label>
                                <div class="col-sm-3">
                                  <asp:Label ID="lblDeActive" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                                 
                          </div></div>     

                </div>

                    </div>
               </div></div> 
                        </asp:Panel>
                             

                   
                     <div class="col-md-12">
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVAddMarketingPerson" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVAddMarketingPerson_PageIndexChanging"
                      OnRowDataBound="GVAddMarketingPerson_RowDataBound"    PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="MobileNo" HeaderText="Mobile No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Name" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Address" HeaderText="Address">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>   
                       
                         <asp:BoundField DataField="SequenceNo" HeaderText="Sequence No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Capacity" HeaderText="Capacity">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="TotalAssignment" HeaderText="Total Assignment">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="CurrentAssignment" HeaderText="Current Assignment">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="CurrentAssignmentStatus" HeaderText="Current Assignment Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField> 
                        <asp:BoundField DataField="IsWorking" HeaderText="IsWorking">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="AddUnder" HeaderText="Under">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Status" HeaderText="Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                        <asp:TemplateField HeaderText="Action">
                              <ItemTemplate>
                                  <a> 
                                      <asp:Button ID="btnStatus" OnClick="btnStatus_Click" CssClass="btn" runat="server" Text=""   />
                                      </a>
                                    
                              </ItemTemplate>
                         </asp:TemplateField>
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>
</asp:Content>
