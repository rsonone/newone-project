﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace NeedlyApp.Admin
{
    public partial class CustContact_ExcelUpload : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString);
        private DataSet ds;
        private DataTable dt;
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string MobileNo = dt.Rows[i]["MobileNo"].ToString();
                        MobileNo = MobileNo.Trim();

                        if (MobileNo.Length == 10 || MobileNo.Length == 12)
                        {
                            string CustomerName = dt.Rows[i]["CustomerName"].ToString();
                            CustomerName = CustomerName.Trim();

                            string Address = dt.Rows[i]["Address"].ToString();
                            Address = Address.Trim();
                            // int pincode = Int32.Parse(dt.Rows[i]["pincode"].ToString());
                            //pincode = pincode.ToString();

                            //SqlCommand cmd = new SqlCommand("SP_UploadRegDetailsNew", DataBaseConnection);
                            SqlCommand cmd = new SqlCommand("SP_UploadCustomerContactDetails", DataBaseConnection);
                            cmd.CommandType = CommandType.StoredProcedure;
                            DataBaseConnection.Open();
                            cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                            cmd.Parameters.AddWithValue("@CustomerName", CustomerName);
                            cmd.Parameters.AddWithValue("@Address", Address);
                            cmd.Parameters.AddWithValue("@CreatedBy", txtMobileNumber.Text);

                            cmd.Parameters.AddWithValue("@ModifiedBy", txtMobileNumber.Text);
                            //cmd.Parameters.AddWithValue("@CreatedBy", txtMobileNumber.Text);
                            cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                            cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                            int A = cmd.ExecuteNonQuery();
                            con.Close();
                            string DataCode = cmd.Parameters["@returnValue"].Value.ToString();
                            DataBaseConnection.Close();
                            // GridView();

                            //  insertCount++;

                            if (A == -1)
                            {
                                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                            }
                            //totalCount = insertCount + updateCount + NotinsertCount;
                            //lblMessage.Text = "NUMBER OF RECORDS UPLOADED : " + totalCount + " INSERTED : " + insertCount + " AND UPDATED : " + updateCount + " AND NOTINSERTED : " + NotinsertCount;
                        }
                        else
                        {
                            // NotinsertCount++;
                        }

                    }
                    
                   
                }
                catch (Exception ex)
                {
                   // EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if ((Fileupload1.HasFile))
                {
                    try
                    {
                        if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                            Fileupload1.PostedFile.ContentLength > 0)
                        {
                            string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;


                            //Fileupload1.SaveAs(FileName);

                            SqlBulkCopy oSqlBulk = null;


                            OleDbConnection myExcelConn = new OleDbConnection
                                ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                    "Data Source=" + FileName +
                                    ";Extended Properties=Excel 12.0;");

                            //OleDbConnection myExcelConn = new OleDbConnection
                            //    ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                            //        "Data Source=" + Server.MapPath(".")+ "\\"+ Fileupload1.FileName +
                            //        ";Extended Properties=Excel 12.0;");

                            myExcelConn.Open();

                            OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                            ds = new DataSet();

                            objAdapter1.Fill(ds);

                            dt = ds.Tables[0];

                            myExcelConn.Close();

                            InsertData(dt);

                        }
                    }
                    catch (Exception ex)
                    {
                       // EL.SendErrorToText(ex);
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                    }
                }
            }
            catch (Exception ex)
            {
              //  EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }

        protected void lnkbtnDownload_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/CustomerContactDetails.xlsx", false);

        }
    }
}