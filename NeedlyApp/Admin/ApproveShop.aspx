﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="ApproveShop.aspx.cs" Inherits="NeedlyApp.Admin.ApproveShop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="clearfix"></div>
   
    <div class="row">
       
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i> Approve Shops <small></small></h2>
                    <div class="container-fluid">
                       
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="panel-body">

                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Select Category
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlCategory" AppendDataBoundItems="true" CssClass="form-control" runat="server">
                                                  <asp:ListItem  Text="select" value="0"></asp:ListItem>
                                                </asp:DropDownList><br />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Monile Number
                                            </label>
                                            <div class="col-sm-9">
                                              <asp:TextBox ID="txtMobileNumber" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Select District
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:DropDownList ID="ddlDistrict" AppendDataBoundItems="true" CssClass="form-control" runat="server" >
                                                    <asp:ListItem Text="select" value="0"></asp:ListItem>
                                                </asp:DropDownList><br />
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                               Approved
                                            </label>
                                            <div class="col-sm-9">
                                                <asp:RadioButtonList ID="RadioButtonList1" RepeatDirection="Horizontal" runat="server">
                                                    <asp:ListItem Value="0">&nbsp; Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                                      <asp:ListItem Value="1">&nbsp; No</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <br />
                                            </div>
                                        </div>
                                    </div>--%>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="box-body col-sm-5">
                                <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>
                            </div>
                            <div class="box-body col-sm-4">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" class="btn btn-success btn-round" />
                            </div>
                            <div class="box-body col-sm-4">
                                <asp:Label ID="Lblerrer" runat="server"></asp:Label>
                            </div>



                        </div>

                        
                    </div>
                </div>
            </div>
        </div>
                 
        <div class="col-md-12 col-sm-12 col-xs-12">
            <asp:GridView ID="GVApproveShop" runat="server"  CssClass="table table-striped" OnRowDataBound="GVApproveShop_RowDataBound"  CellPadding="4" ForeColor="#333333" AutoGenerateColumns="False"  OnSelectedIndexChanging="GVApproveShop_SelectedIndexChanging" PageSize="10" AllowPaging="true" OnPageIndexChanging="GVApproveShop_PageIndexChanging">
                <AlternatingRowStyle BackColor="White"   />
                <Columns>
                   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                            <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                         <asp:BoundField DataField="Id" HeaderText="Server ID" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="ItemName" HeaderText="Item Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ApprovedStatus" HeaderText="Approved Status">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="ApprovedBy" HeaderText="Approved Mobile">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="ApprovedDate" HeaderText=" Approved Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                    
                        
                         
                          <asp:TemplateField HeaderText="Active / DeActive">
                              <ItemTemplate>
                                   <a>
                                      <%-- <asp:Label ID="lblStatus" runat="server" Text="Active"></asp:Label>--%>
                                <%--  <asp:ImageButton ID="img_user" runat="server"   CommandName="Select" ImageUrl='<%# Eval("ApprovedStatus" ) %>' Width="20px" Height="20px" />--%>
                                      <%-- <asp:LinkButton ID="linkbtnStatus" OnClick="linkbtnStatus_Click" runat="server">Active</asp:LinkButton>--%>
                                       <asp:Button ID="btnStatus" CssClass="btn btn-round btn-danger" OnClick="btnStatus_Click" runat="server" />
                                     
                                      <i></i></a></a>
                                     </ItemTemplate>
                         </asp:TemplateField>
                </Columns>
                
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />

               

            </asp:GridView>
        </div>
         
    </div>
</asp:Content>
