﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp
{
    public partial class ChildSafetyReport : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"].ToString() == "9422325020")
            {
               
            }
            if (!IsPostBack)
            {
               
                
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Gridview();
            GVReg_Report.Visible = true;
            GVReg_Report2.Visible = false;
            GVReg_Report1.Visible = false;
            btnExcel.Visible = true;
            btnExcel1.Visible = false;
            btnExcel2.Visible = false;
                      
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ChildSafetyReport.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVReg_Report.AllowPaging = false;
            GVReg_Report.PageIndex = 0;

            Gridview();
               
                GVReg_Report.HeaderRow.Style.Add("background-color", "#FFFFFF");
                //Applying stlye to gridview header cells
                for (int i = 0; i < GVReg_Report.HeaderRow.Cells.Count; i++)
                {
                    GVReg_Report.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
                }
                GVReg_Report.RenderControl(htw);
                Response.Write(sw.ToString());
                Response.End();
            }

        protected void btnExcel1_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ChildSafetyReport.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVReg_Report1.AllowPaging = false;
            GVReg_Report1.PageIndex = 0;

            GridViewForSerch();

            GVReg_Report1.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVReg_Report1.HeaderRow.Cells.Count; i++)
            {
                GVReg_Report1.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVReg_Report1.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void btnExcel2_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ChildSafetyReport.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVReg_Report2.AllowPaging = false;
            GVReg_Report2.PageIndex = 0;

            GridviewforReferralwise();

            GVReg_Report2.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVReg_Report2.HeaderRow.Cells.Count; i++)
            {
                GVReg_Report2.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVReg_Report2.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }




        protected void GVReg_Report1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report1.PageIndex = e.NewPageIndex;
            this.GridViewForSerch();
        }
        protected void btnsearchbyname_Click(object sender, EventArgs e)
        {
            GridViewForSerch();
            GVReg_Report1.Visible = true;
            GVReg_Report2.Visible = false;
            GVReg_Report.Visible = false;
            btnExcel.Visible = false;
            btnExcel1.Visible = true;
            btnExcel2.Visible = false;

           

        }
        public void GridViewForSerch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
            {
                
                SqlCommand cmd = new SqlCommand("SP_GetParentSearchByMobilenumber");
                cmd.Connection = DataBaseConnection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ParentMobNo", txtName.Text);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                DataSet ds = new DataSet();
                //  DataTable dt =  New   taTable();
                da.Fill(ds);
                GVReg_Report1.DataSource = ds;
                GVReg_Report1.DataBind();
                Label3.Text = (ds.Tables[0].Rows.Count.ToString());

                
            }
        }

        protected void GVReg_Report_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report.PageIndex = e.NewPageIndex;
            this.Gridview();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/ChildSafetyReport.aspx");
        }

        public void GridviewforReferralwise()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetParentReferralwise");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MobileNo", txtreferralwise.Text);
                    

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReg_Report2.DataSource = ds;
                        GVReg_Report2.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());

                    }

                   
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void GVReg_Report2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report2.PageIndex = e.NewPageIndex;
            this.GridviewforReferralwise();
        }
        protected void btnreferralwise_Click(object sender, EventArgs e)
        {
            GridviewforReferralwise();
            GVReg_Report.Visible = false;
            GVReg_Report1.Visible = false;
            GVReg_Report2.Visible = true;
            btnExcel.Visible = false;
            btnExcel1.Visible = false;
            btnExcel2.Visible = true;

            
        }
        public void Gridview()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_Downloaddataall1");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Role", ddlRole.SelectedValue);
                    //cmd.Parameters.AddWithValue("@Digit", ddldigit.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReg_Report.DataSource = ds;
                        GVReg_Report.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());

                    }

                    // DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}