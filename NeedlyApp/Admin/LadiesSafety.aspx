﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="LadiesSafety.aspx.cs" Inherits="NeedlyApp.Admin.LadiesSafety" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
 <link href="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js" rel="stylesheet"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Ladies Safety Help line <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlstate" CssClass="form-control"  OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddldistrict" OnSelectedIndexChanged="ddldistrict_SelectedIndexChanged"  CssClass="form-control" AutoPostBack="true"  runat="server" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Taluka: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTaluka" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList><br />
                          </div></div></div>

                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                Police Station Name: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtName" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div> 

                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                Police Station Address: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtAddress" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                               Police Station Contact No: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtContactNo" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div> 

                      <div>
                    <div class="panel-body">                                     
               <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                     <div class="box-body col-sm-offset-2  col-sm-4">
                           <asp:linkbutton id="lnkbtndownload" runat="server" text="click here!!! download excel format." font-bold="true" font-size="medium" onclick="lnkbtnDownload_Click"  forecolor="red"></asp:linkbutton>
                          </div>
                    </div>
    
                    </div>

              </div>
                     
                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                        
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" class="btn btn-success" />
                              
                        </div>
                       
                    </div>
                    <br />
                    <br />
                     <div class="col-md-12">
                        <br />
                           <div class="table-responsive">
                    <asp:GridView ID="GVLadiesSafety" CssClass="table table-hover table-bordered" runat="server"
                       PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" OnPageIndexChanging="GVAddreferal_PageIndexChanging"   Font-Names="Arial" >
                    <Columns>   
                   
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="State" HeaderText="State">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Name" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Address" HeaderText="Address">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>   
                         <asp:BoundField DataField="ContactNumber" HeaderText="Contact Number">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                       
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                  </div>
                </div></div>
</asp:Content>
