﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddStaff.aspx.cs" MasterPageFile="~/MasterPage/NeedlyMaster.Master" Inherits="NeedlyApp.Admin.AddStaff" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i>Add Staff<small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <asp:RadioButtonList ID="rdSelect" runat="server" OnSelectedIndexChanged="rdSelect_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="True">
                                <asp:ListItem Text="Add Staff" Selected="true" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Add Client" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Add Hawkers" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlStaff" Visible="true" runat="server">

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Mobile No :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtStaffMobile" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Name :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtName" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Select Post :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlPost" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Select Shift :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlShift" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Select Dept :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlDept" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Job Location :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlLoc" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Select Status :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Add Under :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlUnder" runat="server" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                            </asp:DropDownList><br />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <%-- <div class="box-body col-sm-12">--%>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Select Type of salary payment :
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlPayment" runat="server" AutoPostBack="true" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>

                            <%--     <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                    Add Under : </label>
                               <div class="col-sm-9">
                                 <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="true" CssClass="form-control" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList><br />                                   </div></div></div>--%>
                        </div>
                    </div>



                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Basis Pay :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtBasic" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Joining Date :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtDate" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Week Off :
                                    </label>
                                    <div class="col-sm-9">
                                        &nbsp;&nbsp;&nbsp;    
                                        <asp:CheckBox ID="cbMon" runat="server" />&nbsp;&nbsp;Monday   
           &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="cbTue" runat="server" />&nbsp;&nbsp;Tuesday  
           &nbsp;&nbsp; &nbsp;&nbsp;<asp:CheckBox ID="cbWed" runat="server" />
                                        &nbsp;&nbsp;Wednesday
                                         &nbsp;&nbsp;&nbsp;    
                                        <asp:CheckBox ID="cbThu" runat="server" />&nbsp;&nbsp;Thursday   
           &nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="cbFri" runat="server" />&nbsp;&nbsp;Friday  
           &nbsp;&nbsp; &nbsp;&nbsp;<asp:CheckBox ID="cbSat" runat="server" />
                                        &nbsp;&nbsp;Saturday
                                         &nbsp;&nbsp;&nbsp;    
                                        <asp:CheckBox ID="cnSun" runat="server" />&nbsp;&nbsp;Sunday  
                                   &nbsp;&nbsp;&nbsp;    
                                        <asp:CheckBox ID="cbNone" runat="server" />&nbsp;&nbsp;None  
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="panel-body">


                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            TA :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtTA" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            EPF:
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtEPF" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="panel-body">


                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Petrol Allowances :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtPetrol" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            ESIC:
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtESIC" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>




                </asp:Panel>
                <asp:Panel ID="pnClient" Visible="true" runat="server">
                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Office Name :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtOfc" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            MobileNo :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtMob" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Owner Name:
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtOwner" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Office No :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtOfcNo" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Email Id :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Pincode :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtPincode" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Area /Locality Name :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtArea" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Street Sector Name :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtSector" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Office Type :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:RadioButtonList ID="rdbCom" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Text="Owned" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Rent" Value="1"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Send Message By :
                                        </label>
                                        <div class="col-sm-9">
                                            &nbsp;&nbsp;&nbsp;    
                                            <asp:CheckBox ID="cbSMS" runat="server" />&nbsp;&nbsp;SMS   
           &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:CheckBox ID="cmMail" runat="server" />&nbsp;&nbsp;E-mail  
           &nbsp;&nbsp; &nbsp;&nbsp;<asp:CheckBox ID="CbWhatsApp" runat="server" />
                                            &nbsp;&nbsp;WhatsApp
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </asp:Panel>

                <asp:Panel ID="pnlHawkers" Visible="true" runat="server">
                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Office Name :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtOfcNamehakers" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            MobileNo :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtmobHawker" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Hawker Name:
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtHawker" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Flat No :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtFlat" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Email Id :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtmail" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Paper Start Booking Deposite :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtStartBooking" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Delivery Charges :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtDelivery" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Collection Pending :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtCollection" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            House Type :
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:RadioButtonList ID="rdHouseType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem Text="Owned" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Rent" Value="1"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-body col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="input-name2">
                                            Send Message By :
                                        </label>
                                        <div class="col-sm-9">
                                            &nbsp;&nbsp;&nbsp;    
                                            <asp:CheckBox ID="CheckBox1" runat="server" />&nbsp;&nbsp;SMS   
           &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:CheckBox ID="CheckBox2" runat="server" />&nbsp;&nbsp;E-mail  
           &nbsp;&nbsp; &nbsp;&nbsp;<asp:CheckBox ID="CheckBox3" runat="server" />
                                            &nbsp;&nbsp;WhatsApp
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </asp:Panel>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Upload Excel:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:FileUpload ID="Fileupload1" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="box-body col-sm-6">
                                <div class="box-body col-sm-offset-4  col-sm-8">
                                    <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." Font-Bold="true" Font-Size="Medium" OnClick="lnkbtnDownload_Click" ForeColor="Red"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Button ID="BtnSave" runat="server" class="btn btn-success" OnClick="BtnSave_Click" Text="Save" />&nbsp;&nbsp;
                        <asp:Button ID="btnCancle" runat="server" class="btn btn-success" OnClick="btnCancle_Click" Text="Reset" />&nbsp;&nbsp;

                 <%--       <asp:Button ID="btnExcel" runat="server" class="btn btn-success" OnClick="btnExcel_Click" Text="Export To Excel" />--%>
                    </div>
                    <div class="box-body col-sm-4">
                        <asp:Label ID="Label2" runat="server"></asp:Label>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box-body col-sm-5">
                    </div>
                    <div class="col-md-12">
                        <div class="box-body col-sm-offset-2  col-sm-4">
                            <asp:Label ID="Label3" runat="server" Text="Count" Visible="false" Font-Bold="true"></asp:Label>
                            <asp:Label ID="lalCount" runat="server" Font-Bold="true"></asp:Label>
                            <br />
                            <asp:Label ID="lblMessage" runat="server" Font-Bold="true"></asp:Label>
                        </div>
                    </div>

                    <div class="box-body col-sm-4">
                    </div>
                </div>
                <asp:Panel ID="pnlGrid1" Visible="true" runat="server">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <asp:GridView ID="GvStaff" runat="server" PagerStyle-VerticalAlign="Middle"
                                PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Id" HeaderText="Id">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FullName" HeaderText="FullName">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MobileNo" HeaderText="Mobile No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UserRole" HeaderText="User Role">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AddUnder" HeaderText="Add Under">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PaymentType" HeaderText="Payment Type">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ShopType" HeaderText="Shop Type">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Shop_OwnweNo" HeaderText="Shop OwnweNo">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Shift" HeaderText="Shift">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Update">
                                        <ItemTemplate>
                                            <asp:Button ID="lnkbtnView" CssClass="btn btn-round btn-success" runat="server" CommandArgument='<%#Bind("Id")%>'
                                                CommandName="Modify" OnClick="lnkbtnView_Click" Text="Edit"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:Button ID="lnkbtndelete" CssClass="btn btn-round btn-warning" runat="server" CommandArgument='<%#Bind("Id")%>'
                                                CommandName="Modify" OnClick="lnkbtndelete_Click" Text="Delete" OnClientClick="Confirm()"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <HeaderStyle BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlGrid2" Visible="true" runat="server">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvClient" runat="server" PagerStyle-VerticalAlign="Middle"
                                PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="OfficeName" HeaderText="OfficeName">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OfficeNo" HeaderText="Office No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OwnerName" HeaderText="Owner Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MobileNo" HeaderText="Mobile No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Area" HeaderText="Area">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="SectorName" HeaderText="Street / Sector Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="OfficeType" HeaderText="Office Type">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Msg" HeaderText="Send Message By">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>

                                </Columns>
                                <HeaderStyle BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Panel ID="pnlGrid3" Visible="true" runat="server">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvHawker" runat="server" PagerStyle-VerticalAlign="Middle"
                                PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.No">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FullName" HeaderText="FullName">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="firmName" HeaderText="Firm Name">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="mobileNo" HeaderText="Mobile No">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EntryDate" HeaderText="Date">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="State" HeaderText="State">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="District" HeaderText="District">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UserRole" HeaderText="UserRole">
                                        <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                        <asp:ItemStyle HorizontalAlign="Center"></asp:ItemStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Update">
                                        <ItemTemplate>
                                            <asp:Button ID="lnkbtnView" CssClass="btn btn-round btn-success" runat="server" CommandArgument='<%#Bind("Id")%>'
                                                CommandName="Modify" OnClick="lnkbtnView_Click" Text="Edit"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:Button ID="lnkbtndelete" CssClass="btn btn-round btn-warning" runat="server" CommandArgument='<%#Bind("Id")%>'
                                                CommandName="Modify" OnClick="lnkbtndelete_Click" Text="Delete" OnClientClick="Confirm()"></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
