﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddShopCategory : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetShopCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVShopCat.DataSource = ds.Tables[0];
                        GVShopCat.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void Category()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_GetShopCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlCat.DataSource = ds.Tables[0];
                        ddlCat.DataTextField = "ItemName";
                        ddlCat.DataValueField = "Id";
                        ddlCat.DataBind();
                        ddlCat.Items.Insert(0, new ListItem("--Select--", "0"));
                        ddlCat.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gr = (GridViewRow)btn.NamingContainer;
                int i = Convert.ToInt32(gr.RowIndex);
                string ID = Convert.ToString(GVShopCat.Rows[i].Cells[0].Text);

                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SP_DeleteShopCategory", DataBaseConnection);
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", ID);

                    int A = cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    GridView();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Deleted Successfully...!');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
            }
        }

        protected void GVShopCat_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GVShopCat.EditIndex = e.NewEditIndex;
            this.GridView();
        }
        protected void GVShopCat_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GVShopCat.EditIndex = -1;
            this.GridView();
        }
        protected void GVShopCat_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    int userid = Convert.ToInt32(GVShopCat.DataKeys[e.RowIndex].Value.ToString());
                    GridViewRow row = (GridViewRow)GVShopCat.Rows[e.RowIndex];
                    Label lblID = (Label)row.FindControl("lblEdit");

                    TextBox ItemName = (TextBox)row.Cells[1].Controls[0];

                    SqlCommand cmd = new SqlCommand("SP_UpdateShopCategory", DataBaseConnection);
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Id", userid);
                    cmd.Parameters.AddWithValue("@ItemName", ItemName.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    GVShopCat.EditIndex = -1;
                    GridView();

                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Record Updated Successfully ... ');", true);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void GVShopCat_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVShopCat.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    if(rdbCat.SelectedValue=="1")
                    {
                        SqlCommand cmd = new SqlCommand("SP_UploadShopCategory", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@ItemName", txtCat.Text);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        int A = cmd.ExecuteNonQuery();
                        DataBaseConnection.Close();
                        GridView();
                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                    if (rdbCat.SelectedValue == "2")
                    {
                        SqlCommand cmd = new SqlCommand("SP_UploadShopCategoryNew", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@ItemName", txtSubCat.Text);
                        cmd.Parameters.AddWithValue("@ParentID", ddlCat.SelectedValue);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        int A = cmd.ExecuteNonQuery();
                        DataBaseConnection.Close();
                        GridView();
                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                    if (rdbCat.SelectedValue == "3")
                    {
                        try
                        {
                            if ((Fileupload1.HasFile))
                            {
                                try
                                {
                                    if (!Convert.IsDBNull(Fileupload1.PostedFile) &
                                        Fileupload1.PostedFile.ContentLength > 0)
                                    {
                                        string FileName = Server.MapPath("../Admin/UploadExcel") + Fileupload1.FileName;

                                        Fileupload1.SaveAs(FileName);

                                        SqlBulkCopy oSqlBulk = null;


                                        OleDbConnection myExcelConn = new OleDbConnection
                                            ("Provider=Microsoft.ACE.OLEDB.12.0; " +
                                                "Data Source=" + FileName +
                                                ";Extended Properties=Excel 12.0;");
                                        myExcelConn.Open();

                                        OleDbCommand objOleDB = new OleDbCommand("SELECT * FROM [Sheet1$]", myExcelConn);

                                        OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(objOleDB);

                                        ds = new DataSet();

                                        objAdapter1.Fill(ds);

                                        dt = ds.Tables[0];

                                        myExcelConn.Close();

                                        InsertData(dt);

                                    }
                                }
                                catch (Exception ex)
                                {
                                    EL.SendErrorToText(ex);
                                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' Please check Excel file Name.Filename must be (Sheet1$)');", true);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            EL.SendErrorToText(ex);
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
                        }
                    }
                }
                catch (Exception ex)
                {

                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }
        }
        public void InsertData(DataTable dt = null)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    for (int i = 0; i <= dt.Rows.Count; i++)
                    {
                        string ItemName = dt.Rows[i]["ItemName"].ToString();
                        ItemName = ItemName.Trim();

                        string ParentID = dt.Rows[i]["ParentID"].ToString();
                        ParentID = ParentID.Trim();

                        string ShopType = dt.Rows[i]["ShopType"].ToString();
                        ShopType = ShopType.Trim();

                        string AppKeyword = dt.Rows[i]["AppKeyword"].ToString();
                        AppKeyword = AppKeyword.Trim();

                        //string Rate = dt.Rows[i]["Rate"].ToString();
                        //Rate = Rate.Trim();
                        //string CreatedBy = dt.Rows[i]["CreatedBy"].ToString();
                        //CreatedBy = CreatedBy.Trim();

                        string Status = dt.Rows[i]["Status"].ToString();
                        Status = Status.Trim();

                        string Level = dt.Rows[i]["Level"].ToString();
                        Level = Level.Trim();



                        SqlCommand cmd = new SqlCommand("SP_CategoryWiseShops", DataBaseConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@ItemName", ItemName);
                        cmd.Parameters.AddWithValue("@ShopType", ShopType);
                        cmd.Parameters.AddWithValue("@ParentID", ParentID);
                        cmd.Parameters.AddWithValue("@AppKeyword", AppKeyword);
                        //cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@Level", Level);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        DataBaseConnection.Close();
                        GridView();
                        if (A == -1)
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

                }
            }

        }
        protected void rdbCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(rdbCat .SelectedValue=="1")
            {
                PnlCat.Visible = true;
                Panel1.Visible = false;
                PnlSubCat.Visible = false;
            }
            if (rdbCat.SelectedValue == "2")
            {
                PnlCat.Visible = false;
                Panel1.Visible = false;
                PnlSubCat.Visible = true;
               
                Category();
            }
            if (rdbCat.SelectedValue == "3")
            {
                PnlCat.Visible = false;
                PnlSubCat.Visible = false;
                Panel1.Visible = true;
            }
        }

    }
}