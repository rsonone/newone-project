﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="NeedlyGroupCount.aspx.cs" Inherits="NeedlyApp.Admin.NeedlyGroupCount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Needly Group Count <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                   
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddCat"  runat="server"    PagerStyle-VerticalAlign="Middle" OnPageIndexChanging="GVAddCat_PageIndexChanging"
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                     <%--   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     --%>
                         <asp:BoundField DataField="Id" HeaderText="ID" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="GroupName" HeaderText="Group Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="GroupDescription" HeaderText="Group Description">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="GroupStatus" HeaderText="Group Status">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          
                         <asp:BoundField DataField="CreatedBy" HeaderText="Created By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                          <asp:BoundField DataField="CreatedDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
   

                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                         </div>
                    </div>
                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>

