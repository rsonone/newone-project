﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace NeedlyApp.Admin
{
    public partial class AddHelp : System.Web.UI.Page
    {
        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        CommonCode cc = new CommonCode();
        SqlDataAdapter da = new SqlDataAdapter();
        //CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        DataSet ds = new DataSet();
        string Loginmobile = string.Empty;
        string StringImage = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Loginmobile = Convert.ToString(Session["username"]);

            if (!IsPostBack)
            {
                GridView();
                BindGroupCode();
            }
            if (rbLanguage.SelectedIndex < 0)
            {
                rbLanguage.SelectedIndex = 0;
                //  Label1.Text += "radiobuttonlist shows default selected item.";
            }
            //if (ddlApp.SelectedValue == "12")
            //{
            //    Label6.Visible = true;
            //    txtgroupcode.Visible = true;
            //}
            //else
            //{
            //    Label6.Visible = false;
            //    txtgroupcode.Visible = false;
            //}
        }
        public void GridView()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SP_ShowHelp");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GVAddHelp.DataSource = ds.Tables[0];
                    GVAddHelp.DataBind();
                }
            }
            catch (Exception ex)
            {
                //    EL.SendErrorToText(ex);
            }
        }

        public void BindGroupCode()
        {
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("[sp_GroupCode]");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AdminMobNo", Loginmobile);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlgroupcode.DataSource = ds.Tables[0];
                        ddlgroupcode.DataTextField = "GroupCode";
                        //ddlgroupcode.DataValueField = "MDDS_STC";
                        ddlgroupcode.DataBind();
                        ddlgroupcode.Items.Insert(0, new ListItem("--Select--", "0"));
                        ddlgroupcode.SelectedIndex = 0;
                    }
                }
                catch (Exception ex)
                {

                }
                con.Close();
            }
        }
        protected void txtgroupcode_OnTextChanged(object sender, EventArgs e)
        {
            string selectquotationno = "select GroupCode from tbl_Group where GroupCode='" + ddlgroupcode.SelectedValue + "'";
            string number = Convert.ToString(cc.ExecuteScalarEzeetest(selectquotationno));
            if (number == ddlgroupcode.SelectedValue)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert(' Group Code is Available')", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert(' Group Code is Not Available')", true);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (HttpPostedFile postedFile in flUploadExcel.PostedFiles)
                {
                    if (flUploadExcel.HasFile)
                    {
                        string str = Path.GetFileName(postedFile.FileName);
                        //string contentType = postedFile.ContentType;
                        //using (Stream fs = postedFile.InputStream)
                        //{
                        //    using (BinaryReader br = new BinaryReader(fs))
                        //    {
                        //        byte[] bytes = br.ReadBytes((Int32)fs.Length);

                        //  string str1 = flUploadExcel(postedFile.FileName);
                        postedFile.SaveAs(Server.MapPath("~/img/" + str));
                        string Image = "~/img/" + str.ToString();

                        string Image1 = "http://ezeeforms.com/img/" + str.ToString();
                        // Image = "http://webapi.ezeeforms.com/img/" + str.ToString();

                        //SqlCommand cmd = new SqlCommand("SP_InsertHelpKeyOrImg", con);
                        SqlCommand cmd = new SqlCommand("SP_InsertHelpKeyOrGroup", con);

                        con.Open();
                        cmd.CommandTimeout = 500;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VideoUrl", txtYoutube.Text);
                        cmd.Parameters.AddWithValue("@Description", txtDesc.Text);
                        cmd.Parameters.AddWithValue("@Image", Image1);
                        cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                        cmd.Parameters.AddWithValue("@GroupCode", ddlgroupcode.SelectedValue);
                        //if (ddlApp.SelectedValue == "12")
                        //{
                        //    cmd.Parameters.AddWithValue("@Keyword", txtgroupcode.Text);
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue("@Keyword", ddlApp.SelectedItem.Text);
                        //}
                        cmd.Parameters.AddWithValue("@createdBy", Loginmobile);
                        cmd.Parameters.AddWithValue("@Language", rbLanguage.SelectedValue);

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        GridView();

                        if (A == -1)
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully!!')", true);
                        else
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Failed To Upload Data, Please Try again!')", true);
                    }
                    else
                    {
                        // ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select the image..!')", true);
                        SqlCommand cmd = new SqlCommand("SP_InsertHelpKeyOrGroup", con);

                        con.Open();
                        cmd.CommandTimeout = 500;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VideoUrl", txtYoutube.Text);
                        cmd.Parameters.AddWithValue("@Description", txtDesc.Text);
                        cmd.Parameters.AddWithValue("@Image", "");
                        cmd.Parameters.AddWithValue("@title", txtTitle.Text);
                        cmd.Parameters.AddWithValue("@GroupCode", ddlgroupcode.SelectedValue);
                        //if (ddlApp.SelectedValue == "12")
                        //{
                        //    cmd.Parameters.AddWithValue("@Keyword", txtgroupcode.Text);
                        //}
                        //else
                        //{
                        //    cmd.Parameters.AddWithValue("@Keyword", ddlApp.SelectedItem.Text);
                        //}
                        cmd.Parameters.AddWithValue("@createdBy", Loginmobile);
                        cmd.Parameters.AddWithValue("@Language", rbLanguage.SelectedValue);

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        GridView();
                        Clear();
                        if (A == -1)
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Saved Successfully!!')", true);
                        else
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Failed To Upload Data, Please Try again!')", true);
                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Failed To Upload Data, Please Try again!')", true);
            }

        }
        public void Clear()
        {
            PanelParent.Visible = true;
            //ddlApp.SelectedIndex = 0;

            txtTitle.Text = "";
            //txtHelpkey.Text = "";
            txtDesc.Text = "";
            // txtImgUrl.Text = "";
            txtYoutube.Text = "";
           // txtgroupcode.Text = "";
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string Image = string.Empty;
            try
            {
                if (flUploadExcel.HasFile)
                {
                    string str = flUploadExcel.FileName;
                    flUploadExcel.PostedFile.SaveAs(Server.MapPath("~/img/" + str));

                    Image = "~/img/" + str.ToString();

                    Image = "http://webapi.ezeeforms.com/img/" + str.ToString();
                }

                SqlCommand cmd = new SqlCommand("SP_UpdatehelpKeyDetails", con);
                con.Open();
                cmd.CommandTimeout = 500;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Id", lblID.Text);
                cmd.Parameters.AddWithValue("@YouTubeUrl", txtYoutube.Text);
                cmd.Parameters.AddWithValue("@Description", txtDesc.Text);
                if (Image != "")
                {
                    ViewState["StringImage"] = Image;
                }

                Image = ViewState["StringImage"].ToString();

                cmd.Parameters.AddWithValue("@ImageID", ViewState["imageId"]);
                cmd.Parameters.AddWithValue("@ImageUrl", Image);
                cmd.Parameters.AddWithValue("@Parent_Id", txtParent_Id.Text);
                cmd.Parameters.AddWithValue("@Title", txtTitle.Text);
                //cmd.Parameters.AddWithValue("@HelpKey", txtHelpkey.Text);
                //if (ddlApp.SelectedValue == "12")
                //{
                //    cmd.Parameters.AddWithValue("@AppName", txtgroupcode.Text);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("@AppName", ddlApp.SelectedItem.Text);
                //}

                //cmd.Parameters.AddWithValue("@CreatedBy", Session["LoginId"].ToString());

                int A = cmd.ExecuteNonQuery();
                con.Close();
                GridView();
                Clear();
                if (A == -1)
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Updated Successfully!!')", true);
                else
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Failed To Upload Data, Please Try again!')", true);
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Please Select the image..!')", true);

                //}
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Failed To Upload Data, Please Try again!')", true);
            }
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                GridViewRow row = btn.NamingContainer as GridViewRow;

                int i = Convert.ToInt32(row.RowIndex);

                string ID = Convert.ToString(GVAddHelp.Rows[i].Cells[1].Text);

                lblID.Text = ID;
                SqlCommand command4 = new SqlCommand();
                command4.Connection = con;
                con.Open();
                command4.CommandText = "SP_LoadHelpKey";
                command4.CommandType = System.Data.CommandType.StoredProcedure;
                command4.Parameters.AddWithValue("@Id", lblID.Text);

                SqlDataAdapter da4 = new SqlDataAdapter();
                da4.SelectCommand = command4;
                DataSet ds4 = new DataSet();
                da4.Fill(ds4);
                if (ds4.Tables[0].Rows.Count > 0)
                //{
                //    if (ddlApp.SelectedValue == "12")
                //    {
                //        txtgroupcode.Visible = true;
                //        txtgroupcode.Text = ds4.Tables[0].Rows[0]["Keyword"].ToString();
                //    }
                //    else
                //    {
                //        txtgroupcode.Visible = false;
                //        ddlApp.SelectedItem.Text = ds4.Tables[0].Rows[0]["Keyword"].ToString();
                //    }
                    txtTitle.Text = ds4.Tables[0].Rows[0]["title"].ToString();
                   // txtHelpkey.Text = ds4.Tables[0].Rows[0]["help_key"].ToString();
                    txtDesc.Text = ds4.Tables[0].Rows[0]["Description"].ToString();
                    ViewState["StringImage"] = ds4.Tables[0].Rows[0]["Image"].ToString();
                    txtYoutube.Text = ds4.Tables[0].Rows[0]["Video_url"].ToString();
                    txtParent_Id.Text = ds4.Tables[0].Rows[0]["Parent_Id"].ToString();
                    ViewState["imageId"] = ds4.Tables[0].Rows[0]["ID"].ToString();
                

                con.Close();
                PanelParent.Visible = true;
                btnSubmit.Visible = false;
                btnUpdate.Visible = true;
            }
            catch (Exception ex)
            {

            }
        }
        protected void GVAddHelp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddHelp.PageIndex = e.NewPageIndex;

            //if (ddlApp.SelectedValue != "0")
            //{

            //    GridViewKeyword();
            //}
            //else
            //{
            //    GridView();
            //}
        }

        protected void ddlApp_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewKeyword();

        }

        //protected void lnkbtndelete_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Button btn = sender as Button;
        //        GridViewRow row = btn.NamingContainer as GridViewRow;

        //        int i = Convert.ToInt32(row.RowIndex);

        //        string ID = Convert.ToString(GVAddHelp.Rows[i].Cells[1].Text);

        //        lblID.Text = ID;
        //        SqlCommand command4 = new SqlCommand();
        //        command4.Connection = con;
        //        con.Open();
        //        command4.CommandText = "SP_DeleteHelpKey";
        //        command4.CommandType = System.Data.CommandType.StoredProcedure;
        //        command4.Parameters.AddWithValue("@Id", lblID.Text);
        //       int A =  command4.ExecuteNonQuery();


        //        con.Close();

        //        if (A == -1)
        //        {

        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Delete Successfully!!')", true);
        //            GridView();
        //        }
        //        else
        //            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Failed To Delete Data, Please Try again!')", true);


        //        //PanelParent.Visible = true;
        //        //btnSubmit.Visible = false;
        //        //btnUpdate.Visible = true;
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}


        public void GridViewKeyword()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SP_ShowHelpKeyword");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                // cmd.Parameters.Add("")
               // cmd.Parameters.AddWithValue("@title", ddlApp.SelectedItem.Text);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GVAddHelp.DataSource = ds.Tables[0];
                    GVAddHelp.DataBind();
                }
            }
            catch (Exception ex)
            {
                //    EL.SendErrorToText(ex);
            }
        }

    }
}