﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="UploadRecords.aspx.cs" Inherits="NeedlyApp.Admin.UploadRecords" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
 <link href="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js" rel="stylesheet"/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Upload Records to Contact Excel table <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                 <a href="NeedlyMarketingPerson.aspx" data-toggle="tooltip" title="Add Marketing Person" class="btn btn-primary">
                    <i class=""> Add Marketing Person </i></a>
                            <a href="UploadRecords.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
              
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
                       <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select: </label>
                                <div class="col-sm-9">
                                    <asp:RadioButtonList ID="RadioButtonList1" RepeatDirection="Horizontal" runat="server">
                                         <asp:ListItem Selected="True" Value="2">&nbsp;&nbsp; Customer &nbsp;&nbsp;</asp:ListItem>
                                         <asp:ListItem Value="1">&nbsp;&nbsp; Marketing Persone &nbsp;&nbsp;</asp:ListItem>
                                    </asp:RadioButtonList>
                          </div></div></div>

                <div class="box-body col-sm-6">
                         

                </div>

                    </div>
               </div></div> 

                 
                                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                    Data Received from: </label>
                                <div class="col-sm-8">
                                   <asp:DropDownList ID="ddlReceiver" CssClass="form-control" OnSelectedIndexChanged="ddlReceiver_SelectedIndexChanged"  runat="server" AutoPostBack="true" >
                                        <asp:ListItem Value="0" Text="-- Select --"></asp:ListItem>
                                       <asp:ListItem Value="1" Text="Needly"></asp:ListItem>
                                      <%-- <asp:ListItem Value="2" Text="Ezeetest"></asp:ListItem>--%>
                                       <asp:ListItem Value="3" Text="Pincode Data"></asp:ListItem>
                                       <asp:ListItem Value="4" Text="TrueVoter Data"></asp:ListItem>
                                       
                                         
                                    </asp:DropDownList><br />
                          </div></div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-4 control-label" for="input-name2">
                                 Sender Name : </label>
                             <div class="col-sm-8">
                                 <asp:DropDownList ID="ddlSenderNo"  OnSelectedIndexChanged="ddlSenderNo_SelectedIndexChanged"  CssClass="form-control"  runat="server"  AutoPostBack="false">
                                      <asp:ListItem Value="0"> --Select-- </asp:ListItem>
                                 </asp:DropDownList>
                                
                          </div>
                             </div></div>

                    </div>
               </div></div>
                    <asp:Panel ID="Panel1" Visible="false" runat="server">
                            <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                   Select Pincode: </label>
                                <div class="col-sm-8">
                                  <asp:DropDownList ID="ddlPincode"  OnSelectedIndexChanged="ddlPincode_SelectedIndexChanged" CssClass="form-control"  runat="server"  AutoPostBack="false">
                                      <asp:ListItem Value="0"> --Select-- </asp:ListItem>
                                 </asp:DropDownList>
                          </div>
                             

                         </div></div>
                <div class="box-body col-sm-4">
                        
                             <div class="form-group">
                                 
                            <label class="col-sm-6 control-label" for="input-name2">
                                    </label>
                                <div class="col-sm-6">
                                 
                          </div>
                            
                       
                            
                             </div>
                         </div>

                    </div>
               </div></div> 
                     </asp:Panel>
                    <asp:Panel ID="PnlCatAndPrefix" Visible="false" runat="server">
                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                 
                            <label class="col-sm-4 control-label" for="input-name2">
                                   Prefix: </label>
                                <div class="col-sm-8">
                                  <asp:TextBox ID="txtPrefix" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                            
                       
                            
                             </div>

                    </div>
                <div class="box-body col-sm-6">
                        
                            
                     <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                   Category </label>
                                <div class="col-sm-8">
                                  <asp:TextBox ID="txtCategory" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                             

                         </div>
                         </div>

                    </div>
               </div></div> 
                    </asp:Panel>
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                 
                            <label class="col-sm-4 control-label" for="input-name2">
                                   Admin No-Name: </label>
                                <div class="col-sm-8">
                                  <asp:TextBox ID="txtAdminNo" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                            
                       
                            
                             </div>

                    </div>
            <%--    <div class="box-body col-sm-6">
                        
                            
                     <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                   Admin Name </label>
                                <div class="col-sm-8">
                                  <asp:TextBox ID="txtAdminName" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                             

                         </div>
                         </div>--%>

                    </div>
               </div></div> 
                     
                  
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                      <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-name2">
                                   Starting Sr.no. : </label>
                                <div class="col-sm-4">
                                  <asp:TextBox ID="txtFromId" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                              <label class="col-sm-2 control-label" for="input-name2">
                                 No. of Records: </label>
                             <div class="col-sm-4">
                                  <asp:TextBox ID="txtToId" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>

                         </div></div>

                <div class="box-body col-sm-5">
                        
                             <div class="form-group">
                                  <div class="col-sm-4">
                                 <asp:Button ID="btnShowRecords" OnClick="btnShowRecords_Click" runat="server" Text="Show Records" class="btn btn-primary" />
                          </div>
                             <div class="col-sm-1" >
                              
                                  </div> 
                              <div class="col-sm-2">
                              <asp:Button ID="btnSendRecords" OnClick="btnSendRecords_Click"  runat="server" Text="Send Records" class="btn btn-success" />
                             
                        </div> 
                                 <div class="col-sm-2" >
                              
                                  </div>
                        <div class="col-sm-2" >
                               <asp:Button ID="btnExportData" OnClick="btnExportData_Click"   runat="server" Text="Export Data" class="btn btn-success" />
                             
                                  </div> 
                            
                             </div>
                         </div>

                    </div>
               </div></div> 
                         
                    
                   
                               <asp:Panel ID="Panel2" Visible="false" runat="server">
                                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                    Data Received from: </label>
                                <div class="col-sm-8">
                                   <asp:DropDownList ID="ddlReceivData2" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                
                                       <asp:ListItem Value="1" Text="Needly"></asp:ListItem>
                                       <asp:ListItem Value="2" Text="Ezeetest"></asp:ListItem>
                                       
                                         
                                    </asp:DropDownList><br />
                          </div></div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-4 control-label" for="input-name2">
                                 Sender Mobile Number: </label>
                             <div class="col-sm-8">
                                 <asp:DropDownList ID="ddlSenderNo2"  CssClass="form-control"  runat="server"  AutoPostBack="false">
                                      <asp:ListItem Value="0"> --Select-- </asp:ListItem>
                                 </asp:DropDownList>
                                
                          </div>
                             </div></div>

                    </div>
               </div></div> 
                          
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                   Category </label>
                                <div class="col-sm-8">
                                  <asp:TextBox ID="txtCategory2" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                             

                         </div></div>
                <div class="box-body col-sm-4">
                        
                             <div class="form-group">
                                 
                            <label class="col-sm-6 control-label" for="input-name2">
                                   Prefix: </label>
                                <div class="col-sm-6">
                                  <asp:TextBox ID="txtPrefix2" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                            
                       
                            
                             </div>
                         </div>

                    </div>
               </div></div> 
                     
                  
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                      <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-name2">
                                    From Id: </label>
                                <div class="col-sm-4">
                                  <asp:TextBox ID="txtFromId2" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>
                              <label class="col-sm-2 control-label" for="input-name2">
                                 To Id: </label>
                             <div class="col-sm-4">
                                  <asp:TextBox ID="txtToId2" runat ="server" CssClass="form-control"></asp:TextBox>
                          </div>

                         </div></div>

                <div class="box-body col-sm-4">
                        
                             <div class="form-group">
                                  <div class="col-sm-4">
                                 <asp:Button ID="btnShowRecords2" OnClick="btnShowRecords2_Click" runat="server" Text="Show Records" class="btn btn-primary" />
                          </div>
                             <div class="col-sm-2" >
                              
                                  </div> 
                              <div class="col-sm-2">
                              <asp:Button ID="btnSendRecords2"  OnClick="btnSendRecords2_Click" runat="server" Text="Send Records2" class="btn btn-success" />
                             
                        </div> 
                       
                            
                             </div>
                         </div>

                    </div>
               </div></div> 
                               </asp:Panel>

                    
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                      <div class="box-body col-sm-6">
                         <div class="form-group">
                             <asp:Label ID="lblmsg" Font-Bold="true" Font-Size="Large" ForeColor="Black" runat="server" Text=""></asp:Label>
                                  <asp:Label ID="lblCount" Font-Bold="true" Font-Size="Large" ForeColor="Green" runat="server" Text=""></asp:Label>
                          

                         </div></div>

              

                    </div>
               </div></div>
                   
                   
                    <br />
                             
                    <div class="col-md-12">
                         <br />
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVUploadRecordNeedly" Visible="false" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVUploadRecord_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>   
      
                         <asp:BoundField DataField="EzeeDrugAppId" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="keyword" HeaderText="keyword">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="firstName" HeaderText="firstName">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                         <asp:BoundField DataField="mobileNo" HeaderText="mobileNo">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="address" HeaderText="address">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                       
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                     <div class="col-md-12">
                         <br />
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVUploadRecordsPincodeData" Visible="false" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVUploadRecord_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>   
                        
      
                         <asp:BoundField DataField="Mobile_Number" HeaderText="Mobile Number">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Name" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                     <div class="col-md-12">
                         <br />
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVUplodRecordsHeading" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVUplodRecordsHeading_PageIndexChanging"
                      OnRowDataBound="GVUplodRecordsHeading_RowDataBound"   PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>   
            
      
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="DataReceivedFrom" HeaderText="Data Source">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="SenderMobileNo" HeaderText="Marketing Persone No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="MarketingPersonName" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="pincode" HeaderText="Pincode">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         
                         <asp:BoundField DataField="FromId" HeaderText="From Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ToId" HeaderText="To Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="AdminNumber" HeaderText="Admin Number - Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <%-- <asp:BoundField DataField="Adminname" HeaderText="Admin Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>--%>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Status" HeaderText="Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="Action">
                              <ItemTemplate>
                                  <a> 
                                      <asp:Button ID="btnStatus" CssClass="btn" OnClick="btnStatus_Click" runat="server" Text=""   />
                                      </a>
                                  <%--   <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> --%>
                              </ItemTemplate>
                         </asp:TemplateField>
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                     <div class="col-md-12">
                         <br />
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GvExportData" CssClass="table table-hover table-bordered" runat="server"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>   
            
                       
                         <asp:BoundField DataField="MobileNo" HeaderText="Mobile No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="Marketing Person">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>
</asp:Content>
