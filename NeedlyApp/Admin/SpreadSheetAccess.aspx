﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="SpreadSheetAccess.aspx.cs" Inherits="NeedlyApp.Admin.SpreadSheetAccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> URL Access Member list <small></small></h2>
                       <div class="container-fluid">
                       
        </div>
                    <div class="clearfix"></div>
                  </div>
                 
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>

                      

                            <asp:Label ID="lblMandatoryDate" runat="server" ForeColor="Red" Text="All fields are mandatory *"></asp:Label>   
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Mobile Number: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtmobileNumber" runat ="server" CssClass="form-control"></asp:TextBox>
                                   <%--  <asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server"
                                      ControlToValidate="txtmobileNumber" 
                                      ErrorMessage="Mobile No. is a required field."
                                      ForeColor="Red">
                                    </asp:RequiredFieldValidator>--%>
                                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                         ValidationExpression="[0-9]{10}" ForeColor="Red" ControlToValidate="txtmobileNumber"
                                        ErrorMessage="Mobile No. must be 10 digits"></asp:RegularExpressionValidator>--%>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Name: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtname" runat ="server" CssClass="form-control"></asp:TextBox>
                                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                         ControlToValidate="txtname" ForeColor="Red"
                                        ErrorMessage="Name is required field"></asp:RequiredFieldValidator>--%>
                                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                         ControlToValidate="txtname" ForeColor="Red" ValidationExpression="^[A-Za-z]*$"
                                        ErrorMessage="only characters allowed"></asp:RegularExpressionValidator>--%>
                          </div></div>     

                </div>

                    </div>
               </div></div> 

                   
                   

                         
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                
                         </div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                             <div class="box-body col-sm-4">
                              <asp:Button ID="btnSpreadsheetAcess" OnClick="btnSpreadsheetAcess_Click"  runat="server" Text="Submit" class="btn btn-success"  />
                              <div class="box-body col-sm-4">
                            
                        </div> 
                        </div> 
                            
                             </div></div>

                    </div>
               </div></div>

                    <asp:Panel ID="pnlCount" Visible="false" runat="server">
                   <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total Customers Added </label>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblCustCount" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Access Permitted customers: </label>
                                <div class="col-sm-3">
                                  <asp:Label ID="lblAccessCount" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                                 
                          </div></div>     

                </div>

                    </div>
               </div></div> 
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total Active Customers </label>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblActiveCust" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total DeActive customers: </label>
                                <div class="col-sm-3">
                                  <asp:Label ID="lblDeActive" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                                 
                          </div></div>     

                </div>

                    </div>
               </div></div> 
                        </asp:Panel>
                              </ContentTemplate>
                    </asp:UpdatePanel>

                     <div class="col-md-12">
                         <br />
                         <br />
                           <div class="table-responsive">
                    <asp:GridView ID="GVSpreadSheetAccess" CssClass="table table-hover table-bordered" runat="server" 
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
                       
      
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="MobileNo" HeaderText="Customer No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Name" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                        
                         <asp:BoundField DataField="CreatedBy" HeaderText="Admin No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Status" HeaderText="Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:TemplateField HeaderText="Action">
                              <ItemTemplate>
                                 <a>
                                     <asp:Button ID="btnGenaratePass" OnClick="btnGenaratePass_Click" CssClass="btn-success" runat="server" Text="Genarate Paaword" />
                                   <asp:Button ID="btnSendPass" OnClick="btnSendPass_Click"  runat="server" CssClass="btn-primary" Text="Send Password" />
                                 </a>
                                 
                                     <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                       
                      
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                     <div class="col-md-12">
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVURLAccessUser" Visible="false" CssClass="table table-hover table-bordered"  runat="server" 
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="MobileNumber" HeaderText="Customer No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Name" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="FromDate" HeaderText="From Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ToDate" HeaderText="To Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="Admin No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Status" HeaderText="Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                       
                        <asp:TemplateField HeaderText="Action">
                              <ItemTemplate>
                                  <a> 
                                      <asp:Button ID="btnStatus" CssClass="btn" runat="server" Text=""   />
                                      </a>
                                    <%-- <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> --%>
                              </ItemTemplate>
                         </asp:TemplateField>
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>

    <script type="text/javascript">    
    
        function userValid() {    
           var MobileNumber = document.getElementById("txtMobileNumber").value;    
           var Name = document.getElementById("txtName").value;    
           var FromDate= document.getElementById("txtFromDate").value;    
           var ToDate= document.getElementById("txtToDate").value;    
    
            if (MobileNumber == '')
           {    
            alert("Please enter Mobile Number");    
            return false;    
           }    
    
            if (Name == '')
           {    
           alert("Please enter name ");    
           return false;    
            }
            if (FromDate == '') {
                alert("Please enter From Date ");
                return false;
            }
            if (ToDate == '') {
                alert("Please enter To Date ");
                return false;
            }
        }    
    </script>
</asp:Content>
