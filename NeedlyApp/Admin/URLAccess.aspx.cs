﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace NeedlyApp.Admin
{
    public partial class URLAccess : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    GridView();
            //     GridViewData();

            //}

            if (Session["username"].ToString() == "9422325020")
            {
                GridView();
                GridViewData();


                GVURLAccessUser.Visible = false;

            }
            else
            {


                GVUrlAceess.Visible = false;
                GVURLAccessUser.Visible = true;
                GridViewForCreatedBy();

            }


        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowUrlAccessDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUrlAceess.DataSource = ds.Tables[0];
                        GVUrlAceess.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }
        public void GridViewData()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_InsertFormTemplate");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GridTemplate.DataSource = ds.Tables[0];
                        GridTemplate.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void GridViewForCreatedBy()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowUrlAccessDetailsByCreatedBy");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVURLAccessUser.DataSource = ds.Tables[0];
                        GVURLAccessUser.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void btnUrlAccess_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    string AppRegistrationQuery = "SELECT EzeeDrugAppId FROM [DBNeedly].[dbo].[EzeeDrugsAppDetail] where keyword='NEEDLY' and mobileNo='" + txtmobileNumber.Text.Trim() + "'";
                    string AppRegistrationRes = cc.ExecuteScalar(AppRegistrationQuery);

                    if (AppRegistrationRes == "" || AppRegistrationRes == null)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Need to install Needly Application for No " + txtmobileNumber.Text + "!!!')", true);

                    }
                    else
                    {
                        string Status = "1";
                        //string date1Day = this.txtFromDate.Text.Remove(8, 9);
                        //string date2Day = this.txtToDate.Text.Remove(8);
                        //string date1Month = this.txtFromDate.Text.Substring(6,7);
                        //string date2Month = this.txtToDate.Text.Substring(6, 7);
                        //string date1Year = this.txtFromDate.Text.Substring(0,6);
                        //string date2Year = this.txtToDate.Text.Substring(0,6);
                        //DateTime d1 = Convert.ToDateTime(date1Month + "/" + date1Day + "/" + date1Year);
                        //DateTime d2 = Convert.ToDateTime(date2Month + "/" + date2Day + "/" + date2Year);
                        //if (d1 > d2)
                        //{
                        //    //  ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('From Date is greater')", true);
                        //    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('From Date is greater.!!!')", true);

                        //}

                        //else if (d1 == d2)
                        //{
                        //    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Both dates are same.!!!')", true);

                        //    //ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Both dates are same')", true);
                        //}
                        //else
                        //{
                        //    Status = "1";
                        //   // ClientScript.RegisterStartupScript(this.GetType(), "Alert", "alert('Date2 is greater')", true);
                        //}
                        if (Status == "1")
                        {
                            string Query = "select Id  from  [tbl_UrlAccessDetails] where MobileNumber = " + txtmobileNumber.Text + "  ";
                            string Res = cc.ExecuteScalar(Query);
                            if (Res == null || Res == "")
                            {
                                Status = "2";
                            }
                            if (Res != "")
                            {
                                Status = "3";
                            }


                            if (Status == "2")
                            {
                               
                                SqlCommand cmd = new SqlCommand("SP_InsetUrlAccessDetails");
                                cmd.Connection = DataBaseConnection;
                                cmd.CommandType = CommandType.StoredProcedure;
                                DataBaseConnection.Open();


                                cmd.Parameters.AddWithValue("@MobileNumber", txtmobileNumber.Text);
                                cmd.Parameters.AddWithValue("@Name", txtname.Text);
                                cmd.Parameters.AddWithValue("@FromDate", txtFromDate.Text);
                                cmd.Parameters.AddWithValue("@ToDate", txtToDate.Text);
                                cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                                cmd.Parameters.AddWithValue("@Status", "1");
                                //cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue);

                                //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                                cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                                cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                                int A = cmd.ExecuteNonQuery();
                                con.Close();
                                string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                                if (A == -1)
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                                }

                                DataBaseConnection.Close();
                                txtmobileNumber.Text = "";
                                txtname.Text = "";
                                txtFromDate.Text = "";
                                txtToDate.Text = "";
                                GridView();
                                
                                //Clear();

                            }

                            if (Status == "3")
                            {
                                string ckEntry = "select CreatedBy  from  [tbl_UrlAccessDetails] where MobileNumber = " + txtmobileNumber.Text + "  ";
                                string EntryResult = cc.ExecuteScalar(ckEntry);

                                if (EntryResult == Session["username"].ToString())
                                {
                                    SqlCommand cmd = new SqlCommand("SP_InsetUrlAccessDetails");
                                    cmd.Connection = DataBaseConnection;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    DataBaseConnection.Open();


                                    cmd.Parameters.AddWithValue("@MobileNumber", txtmobileNumber.Text);
                                    cmd.Parameters.AddWithValue("@Name", txtname.Text);
                                    cmd.Parameters.AddWithValue("@FromDate", txtFromDate.Text);
                                    cmd.Parameters.AddWithValue("@ToDate", txtToDate.Text);
                                    cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                                    cmd.Parameters.AddWithValue("@Status", "1");
                                    //cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue);

                                    //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                                    cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                                    cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                                    int A = cmd.ExecuteNonQuery();
                                    con.Close();
                                    string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                                    if (A == -1)
                                    {
                                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                                    }

                                    DataBaseConnection.Close();
                                    txtmobileNumber.Text = "";
                                    txtname.Text = "";
                                    txtFromDate.Text = "";
                                    txtToDate.Text = "";
                                    GridView();
                                    //Clear();



                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('This No Is Already Added')", true);

                                }
                            }
                        }
                        else
                        {

                        }

                        
                    }

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }

        }

        protected void GVUrlAceess_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //  DataRow row = ((DataRowView)e.Row.DataItem).Row;

                ImageButton img = (ImageButton)e.Row.FindControl("ImageButton1");

                Button mybtn = (Button)e.Row.FindControl("btnStatus");

                if (e.Row.Cells[8].Text == "0")
                {
                    img.ImageUrl = "~/img/deactive.png";
                    //Mylabel.Text = "deActive";

                }
                if (e.Row.Cells[8].Text == "1")
                {
                    img.ImageUrl = "~/img/active.png";
                    //Mylabel.Text = "deActive";

                }

                if (e.Row.Cells[7].Text == "1")
                {
                    mybtn.Text = "Active";
                    mybtn.BackColor = System.Drawing.Color.Green;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                    img.Visible = true;
                    //img.ImageUrl = "~/img_status/imgactive.png";
                    //Mylabel.Text = "Active";
                }
                if (e.Row.Cells[7].Text == "0")
                {
                    //img.ImageUrl = "~/img_status/imgdeactive.png";
                    //Mylabel.Text = "deActive";
                    mybtn.Text = "Deactive";
                    mybtn.BackColor = System.Drawing.Color.Red;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                    img.Visible = false;
                }
              
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            Label1.Text = GVUrlAceess.Rows[rowind].Cells[0].Text;
            string UId = GVUrlAceess.Rows[rowind].Cells[1].Text;
            string  IsPageAccess = GVUrlAceess.Rows[rowind].Cells[8].Text;

            if(IsPageAccess == "0")
            {
                string Query2 = "Update  [tbl_UrlAccessDetails] set PageAccess = 1 where Id = '" + Label1.Text + "' ";
                string Res2 = cc.ExecuteScalar(Query2);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Website access is permited to Number  "+ UId + ".!!!')", true);
                GridView();
                GridViewData();
            }
            else
            {
                string Query2 = "Update  [tbl_UrlAccessDetails] set PageAccess = 0 where Id = '" + Label1.Text + "' ";
                string Res2 = cc.ExecuteScalar(Query2);
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Website Access denied to number "+ UId + "')", true);
                GridView();
                GridViewData();

            }



        }

        protected void GVUrlAceess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUrlAceess.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void GridTemplate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridTemplate.PageIndex = e.NewPageIndex;
            GridViewData();
        }

        protected void GVURLAccessUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //  DataRow row = ((DataRowView)e.Row.DataItem).Row;

                //ImageButton img = (ImageButton)e.Row.FindControl("ImageButton1");

                Button mybtn = (Button)e.Row.FindControl("btnStatus");

               

                if (e.Row.Cells[7].Text == "1")
                {
                    mybtn.Text = "Active";
                    mybtn.BackColor = System.Drawing.Color.Green;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                   // img.Visible = true;
                    //img.ImageUrl = "~/img_status/imgactive.png";
                    //Mylabel.Text = "Active";
                }
                if (e.Row.Cells[7].Text == "0")
                {
                    //img.ImageUrl = "~/img_status/imgdeactive.png";
                    //Mylabel.Text = "deActive";
                    mybtn.Text = "Deactive";
                    mybtn.BackColor = System.Drawing.Color.Red;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                   // img.Visible = false;
                }

            }
        }

        protected void GVURLAccessUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUrlAceess.PageIndex = e.NewPageIndex;
            GridViewForCreatedBy();
        }


        protected void rdbQotation_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbQotation.SelectedValue == "1")
            {
                UrlMember.Visible = true;
                TemplatePanel.Visible = false;


            }

            else
            {

                TemplatePanel.Visible = true;
                UrlMember.Visible = false;
                GridTemplate.Visible = true;





            }
           
        }



        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_FormTemplate");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@MobileNo", txtnumber.Text);

                    cmd.Parameters.AddWithValue("@UserName", TextBox2.Text);
                   
                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    GridViewData();
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }

    }
}