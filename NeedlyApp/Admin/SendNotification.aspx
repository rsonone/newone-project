﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="SendNotification.aspx.cs" Inherits="NeedlyApp.Admin.SendNotification" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>
    $(function () {
        $(".phonecell").click(function () {
            var PhoneNumber = $(this).text();
            PhoneNumber = PhoneNumber.replace("Phone:", "");
            window.location.href = "tel://" + PhoneNumber;
            
        });
    });
        
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Send Notification <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlstate" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddldistrict"  CssClass="form-control" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddldistrict_SelectedIndexChanged" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Taluka: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTaluka" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList><br />
                          </div></div></div>

                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Mobile No: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtMob" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>

                    </div>
               </div></div> 

                    <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Message: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtMsg" runat ="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                           </div></div></div>
                    <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 User Status: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddlstatus"  CssClass="form-control" AutoPostBack="true"  OnSelectedIndexChanged="ddlstatus_SelectedIndexChanged" runat="server"  >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                               <asp:ListItem Value="1" Text="Active User"></asp:ListItem>
                               <asp:ListItem Value="2" Text="Deactive User"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>
                    </div>
               </div></div> 
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                       <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Keyword: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddlkeyword"  CssClass="form-control" AutoPostBack="true"   runat="server"  >
                                     <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                               <asp:ListItem Value="1" Text="Needly Users"></asp:ListItem>
                               <asp:ListItem Value="2" Text="Government User"></asp:ListItem>
                               <asp:ListItem Value="3" Text="Leader User"></asp:ListItem>
                               <asp:ListItem Value="4" Text="USER"></asp:ListItem>
                                     </asp:DropDownList>
                           </div></div></div>
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Image URL: </label>
                              <div class="col-sm-9">
                <asp:TextBox ID="txtimg" runat ="server"  CssClass="form-control"></asp:TextBox>

                           </div></div></div>
                    </div>
               </div></div> 
                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                         <div class="box-body col-sm-5">
                              <asp:Button ID="btnSend" runat="server" Text="Send Notification" class="btn btn-success" OnClick="btnSend_Click" />
                              <asp:Button ID="btnsendall" runat="server" Text="Send Notification All" class="btn btn-success" OnClick="btnSendall_Click" />
                             <asp:Button ID="btnsendtokeyword" runat="server" Text="Send To Keyword" Class="btn btn-success" OnClick="btnsendtokeyword_Click" />
                             <asp:Button ID="btnsearch" runat="server" Text="Search By Mobile No" Class="btn btn-success"  OnClick="btnsearch_Click" />
                        </div>                      
                    </div>
                  </div>
                </div></div>
    <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddQues"  runat="server"  OnPageIndexChanging="GVAddQues_PageIndexChanging" OnRowDataBound="GVAddQues_RowDataBound"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%>   
                         <asp:BoundField DataField="EzeeDrugAppId" HeaderText="ID">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                           <asp:BoundField DataField="Fullname" HeaderText="Full Name">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        
                          <asp:BoundField DataField="MobileNo" HeaderText="Mobile No" ItemStyle-ForeColor="DarkBlue">
                              <ControlStyle CssClass="phonecell"></ControlStyle>
                             
                          </asp:BoundField>
                         <asp:BoundField DataField="FCMStatus" HeaderText="FCM Status">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                
    </asp:Content>
