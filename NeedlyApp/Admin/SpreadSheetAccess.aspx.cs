﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class SpreadSheetAccess : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();

       // SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        SqlConnection con1 = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
       
        DownloadData download = new DownloadData();
        private WebProxy objProxy1 = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView();
        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowSpreadsheetAccessDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVSpreadSheetAccess.DataSource = ds.Tables[0];
                        GVSpreadSheetAccess.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void btnSpreadsheetAcess_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    string AppRegistrationQuery = "SELECT EzeeDrugAppId FROM [DBNeedly].[dbo].[EzeeDrugsAppDetail] where keyword='NEEDLY' and mobileNo='" + txtmobileNumber.Text.Trim() + "'";
                    string AppRegistrationRes = cc.ExecuteScalar(AppRegistrationQuery);

                    if (AppRegistrationRes == "" || AppRegistrationRes == null)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Need to install Needly Application for No " + txtmobileNumber.Text + "!!!')", true);

                    }
                    else
                    {
                        
                        
                            //string Query = "select Id  from  [tbl_UrlAccessDetails] where MobileNumber = " + txtmobileNumber.Text + "  ";
                            //string Res = cc.ExecuteScalar(Query);
                            

                                SqlCommand cmd = new SqlCommand("SP_InsetSpreadSheetAccessDetails");
                                cmd.Connection = DataBaseConnection;
                                cmd.CommandType = CommandType.StoredProcedure;
                                DataBaseConnection.Open();


                                cmd.Parameters.AddWithValue("@MobileNumber", txtmobileNumber.Text);
                                cmd.Parameters.AddWithValue("@Name", txtname.Text);
                               // cmd.Parameters.AddWithValue("@FromDate", txtFromDate.Text);
                                //cmd.Parameters.AddWithValue("@ToDate", txtToDate.Text);
                                cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                                cmd.Parameters.AddWithValue("@Status", "1");
                                //cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue);

                                //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                                cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                                cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                                int A = cmd.ExecuteNonQuery();
                                con.Close();
                                string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                                if (A == -1)
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                                }

                                DataBaseConnection.Close();
                                txtmobileNumber.Text = "";
                                txtname.Text = "";
                               
                                GridView();
                                //Clear();

                            

                            //if (Status == "3")
                            //{
                            //    string ckEntry = "select CreatedBy  from  [tbl_UrlAccessDetails] where MobileNumber = " + txtmobileNumber.Text + "  ";
                            //    string EntryResult = cc.ExecuteScalar(ckEntry);

                            //    if (EntryResult == Session["username"].ToString())
                            //    {
                            //        SqlCommand cmd = new SqlCommand("SP_InsetUrlAccessDetails");
                            //        cmd.Connection = DataBaseConnection;
                            //        cmd.CommandType = CommandType.StoredProcedure;
                            //        DataBaseConnection.Open();


                            //        cmd.Parameters.AddWithValue("@MobileNumber", txtmobileNumber.Text);
                            //        cmd.Parameters.AddWithValue("@Name", txtname.Text);
                            //        cmd.Parameters.AddWithValue("@FromDate", txtFromDate.Text);
                            //        cmd.Parameters.AddWithValue("@ToDate", txtToDate.Text);
                            //        cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                            //        cmd.Parameters.AddWithValue("@Status", "1");
                            //        //cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue);

                            //        //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                            //        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                            //        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                            //        int A = cmd.ExecuteNonQuery();
                            //        con.Close();
                            //        string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                            //        if (A == -1)
                            //        {
                            //            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                            //        }

                            //        DataBaseConnection.Close();
                            //        txtmobileNumber.Text = "";
                            //        txtname.Text = "";
                                   
                            //        //GridView();
                            //        //Clear();



                            //    }
                            //    else
                            //    {
                            //        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('This No Is Already Added')", true);

                            //    }
                            //}
                        
                       


                    }

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }

        }

        protected void btnGenaratePass_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
                    Label1.Text = GVSpreadSheetAccess.Rows[rowind].Cells[0].Text;
                    string UId = GVSpreadSheetAccess.Rows[rowind].Cells[1].Text;

                    // string Query = "select ID from [Login] where Username = " + UId + " and CreatedBy = " + Session["username"].ToString() + " ";
                    string Query = "select ID from [Login] where Username = " + UId + "  ";
                    string Res = cc.ExecuteScalar(Query);
                    if (Res == null || Res == "")
                    {


                        Random rnd = new Random();
                        string Password = cc.DESEncrypt(Convert.ToString(rnd.Next(10001, 99999)));

                        SqlCommand cmd = new SqlCommand("SP_InsetGeneratedPass");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();


                        cmd.Parameters.AddWithValue("@Username", UId);
                        cmd.Parameters.AddWithValue("@Password", Password.ToString());
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                        if (A == -1)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('PASSWORD GENERATED SUCCESSFULLY.!!!')", true);
                        }
                        GridView();
                        DataBaseConnection.Close();
                        //Clear();

                    }
                    else
                    {
                        // string Query1 = "select top 1 Name from [tbl_UrlAccessDetails] where MobileNumber = "+ UId.ToString() + " order by Id desc  ";
                        // string Res1 = cc.ExecuteScalar(Query1);
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('PASSWORD ALREADY GENERATED . ')", true);

                    }

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Failed to generate password.!!!')", true);
                }
            }

        }

        protected void btnSendPass_Click(object sender, EventArgs e)
        {
            try
            {
                //---- new code---
                Button btn = (Button)sender;
                GridViewRow row = (GridViewRow)btn.NamingContainer;
                int i = Convert.ToInt32(row.RowIndex);

                string MobNo = Convert.ToString(GVSpreadSheetAccess.Rows[i].Cells[1].Text);

                //string ID1 = cc.DESEncrypt(ID);

                SqlCommand cmd = new SqlCommand("SP_GetPswdFromLogin");
                cmd.Connection = con1;
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MobNo", MobNo);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string password = Convert.ToString(ds.Tables[0].Rows[0]["Password"]);
                    password = cc.DESDecrypt(password);
                    string s = "PassWord : " + password;
                    //string refMobile = "9763180445";
                    string msg = "Your OTP for Needly App Registration is " + s + " - NEEDLY APP";
                    //string msg = "Your UserName is : " + MobNo +" Password is " + password + " for Automatic Whatsapp Service - NEEDLY APP";
                    // MessageSend(firstName, usrMobile, otpstr.ToString(), refMobile, appKeyword);
                    MessageSend("abc", MobNo, password.ToString(), "NEEDLY", msg);

                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Password send successfully to  " + MobNo + " .')", true);

                    // SendSMSezeetestpinnacle(MobNo, msg ,"NEEDLY");
                }
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Failed to  send password  ')", true);

                throw;
            }
        }

        public void MessageSend(string fName, string userMob, string otp, string appKeyword, string Message)
        {
            string passwordMessage = string.Empty;
            // string senderid = "eZiCLS";
            string senderid = "NEEDLY";

            int smslength = passwordMessage.Length;

            if (appKeyword == "ShivBhojan")
            {
                // passwordMessage = "" + otp + " is your OTP for verification of Ezee Class installation";
                passwordMessage = "Your OTP for Needly App Registration is  " + otp + " - NEEDLY APP";
                //cc.TransactionalSMSCountry("OnlineExam", userMob, passwordMessage, smslength, 22);
                //cc.API_SMSTransactional("OnlineExam", userMob, passwordMessage, smslength, 22, "eZiCLS"); // "eZiCLS");
                // passwordMessage = "" + otp + " OTP for verification of Ezee Class installation";
                SendSMSezeetestpinnacle(userMob, passwordMessage, senderid);
            }


            else if (appKeyword == "NEEDLY")
            {
                //string sqlQuerysms = "SELECT [AttemptCount] FROM  [Come2myCityDB].[dbo].[tblAllAppOTP] WHERE  [UserMobileNumber] = '" + userMob + "' AND [AppKeyword]='" + appKeyword + "'";
                //string attmptCount1 = Convert.ToString(cc.ExecuteScalar(sqlQuerysms));



                //if (attmptCount1 != "0")
                //{
                //passwordMessage = "" + otp + " is your OTP for verification of " + appKeyword + " installation";
                //cc.API_SMSTransactional("OnlineExam", userMob, passwordMessage, smslength, 22, "MyCity");// "MyCTin");

                // passwordMessage = "" + otp + " OTP for verification of " + appKeyword + " installation";
                //passwordMessage = "Your password for Needly App Registration is " + otp + " - NEEDLY APP";
                //cc.OtpSubAccountSMSCountry("OnlineExam", userMob, passwordMessage, smslength, 22, "MyCTin");
                // cc.OtpSubAccountSMSCountry("OnlineExam", userMob, passwordMessage, smslength, 22, "MyCity");
                SendSMSezeetestpinnacle(userMob, Message, "NEEDLY");
                //}
                //else
                //{
                //    passwordMessage = "" + otp + " OTP for verification of " + appKeyword + " installation";
                //    //cc.OtpSubAccountSMSCountry("OnlineExam", userMob, passwordMessage, smslength, 22, "MyCTin");
                //    cc.OtpSubAccountSMSCountry("OnlineExam", userMob, passwordMessage, smslength, 22, "MyCity");
                //}
            }

            // cc.OtpSubAccountSMSCountry("OnlineExam", userMob, passwordMessage, smslength, 22);
        }
        public string SendSMSezeetestpinnacle(string Mobile_Number, string Message, string senderid)
        {

            Mobile_Number = "91" + Mobile_Number;
            string smsUserName = "abhinavitsol";
            string smsPassword = "@3yPR$1m";
            //   System.Object stringpost = "username=" + smsUserName + "&pass=" + smsPassword + "&senderid=" + senderid + "&dest_mobileno=" + Mobile_Number + "&message=" + Message + "&msgType=TXT&response=Y";
            System.Object stringpost = "username=" + smsUserName + "&pass=" + smsPassword + "&senderid=NEEDLY&dest_mobileno=" + Mobile_Number + "&message=" + Message + "&msgType=TXT&response=Y";

            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = null;
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;

            try
            {
                string stringResult = null;

                objWebRequest = (HttpWebRequest)WebRequest.Create("http://www.smsjust.com/sms/user/urlsms.php?");

                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return (stringResult);
            }
            catch (Exception ex)
            {
                cc.WriteError(ex);
                return (ex.Message);
            }
            finally
            {

                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }
        }

    }
}