﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="AddShopCategory.aspx.cs" Inherits="NeedlyApp.Admin.AddShopCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Delete Short Name?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>

  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add Shop Category <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>

                     <div class="row">
            <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Category: </label>
                                <div class="col-sm-9">
                                <asp:RadioButtonList ID="rdbCat" runat="server"  OnSelectedIndexChanged="rdbCat_SelectedIndexChanged" RepeatDirection="Horizontal" AutoPostBack="true">
                                    <asp:ListItem Value="1">&nbsp;&nbsp;Category&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem Value="2">&nbsp;&nbsp;Sub-Category&nbsp;&nbsp;</asp:ListItem>
                                      <asp:ListItem Value="3">&nbsp;&nbsp;Excel Upload&nbsp;&nbsp;</asp:ListItem>
                                </asp:RadioButtonList>
                          </div>

                         </div></div>
                    </div>
               </div></div> 

                     <asp:Panel ID="PnlCat" runat="server" Visible="false">
                         <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                               <label class="col-sm-3 control-label" for="input-name2">
                                                   Enter Category: </label>
                                               <div class="col-sm-9">
                                                 <asp:TextBox ID="txtCat" runat ="server" CssClass="form-control"></asp:TextBox>
                                         </div></div>
                                    </div>
                            </div>
                        </div></div> 
                    </asp:Panel>

                     <asp:Panel ID="PnlSubCat" runat="server" Visible="false">
                         <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                                     <div class="box-body col-sm-6">
                                        <div class="form-group">
                                               <label class="col-sm-3 control-label" for="input-name2">
                                                   Select Category: </label>
                                               <div class="col-sm-9">
                                                 <asp:DropDownList ID="ddlCat" runat="server" AutoPostBack="true" CssClass="form-control">
                                                     <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                 </asp:DropDownList>
                                         </div></div>
                                    </div>
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                               <label class="col-sm-3 control-label" for="input-name2">
                                                   Enter Sub-Category: </label>
                                               <div class="col-sm-9">
                                                 <asp:TextBox ID="txtSubCat" runat ="server" CssClass="form-control"></asp:TextBox>
                                         </div></div>
                                    </div>
                            </div>
                        </div></div> 
                    </asp:Panel>
                     <asp:Panel ID="Panel1" runat="server" Visible="false">
                         <div class="row">
                            <div class="panel-body">                                     
                                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Category Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                                    </div>
                                </div>
                             </div>
                         </asp:Panel>
                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                              
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                     <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVShopCat" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVShopCat_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" DataKeyNames="Id"
                        OnRowEditing="GVShopCat_RowEditing" OnRowCancelingEdit="GVShopCat_RowCancelingEdit" OnRowUpdating="GVShopCat_RowUpdating" >
                    <Columns>   
                          <asp:BoundField DataField="Id" HeaderText="ID" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ItemName" HeaderText="ItemName">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ParentID" HeaderText="ParentID" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" ReadOnly="true" >
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:CommandField ShowEditButton="true" />  
                      <%--  <asp:CommandField ShowDeleteButton="true" />--%>
                        <asp:TemplateField HeaderText="Delete">
	                        <ItemTemplate>
		                        <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete" OnClick="deleteButton_Click"
                                  OnClientClick="return confirm('Are you sure you want to delete this data?');" />
                                 <asp:Label ID="lblDelete" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
	                        </ItemTemplate>
                        </asp:TemplateField>
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div>
                          <asp:Label ID="lblEdit" runat="server" Visible="false"></asp:Label>
                     </div>
           

                    
                  </div>
                </div></div>
    </asp:Content>
