﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="AssemblyVoterReport.aspx.cs" Inherits="NeedlyApp.AssemblyVoterReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Assembly Wise Voter Report <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select Assembly: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlShop" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlShop_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>
                   <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                   Part No: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtpartno"  CssClass="form-control"  runat="server" AutoPostBack="true"></asp:TextBox><br />
                          </div></div></div>
                    </div>
               </div></div> 
                     
                          <div class="col-md-12">
                         <div class="box-body col-sm-5">
                             </div>
                    <div class="box-body col-sm-4">
                              <asp:Button ID="btnsearch" runat="server" Text="Search" class="btn btn-success" OnClick="btnsearch_Click"  />
                        </div>
                
                         </div>
                 <br />
                    <br />
       <div class="box-body col-sm-4">
                        <asp:Label ID="lblcount" runat="server">Total Voter Count:</asp:Label>
                        <asp:Label ID="lbltotalvotercount" runat="server"></asp:Label>
                    </div>
                     <div class="box-body col-sm-4">
                        <asp:Label ID="lblpartno" runat="server">Total Part Number:</asp:Label>
                        <asp:Label ID="lblpartnocount" runat="server"></asp:Label>
                    </div>
                    <br />
             
                
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddCat"  runat="server"  OnPageIndexChanging="GVAddCat_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                     <%--   <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     --%>
                         <asp:BoundField DataField="AC_NO" HeaderText="AC NO" >
                              <HeaderStyle HorizontalAlign="Center" Width=""></HeaderStyle>
                              <ItemStyle HorizontalAlign="Center" Width=""></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="PART_NO" HeaderText="PART NO">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="VOTER_COUNT" HeaderText="VOTER COUNT">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="SurveyCount" HeaderText="Survey Count">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                       
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                         </div>
                    </div>
                    
                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
          </div></div></div>
</asp:Content>

