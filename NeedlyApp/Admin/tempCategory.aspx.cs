﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;


namespace NeedlyApp.Admin
{
    public partial class tempCategory : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView();
                btnUpdate.Visible = false;
                TemplateCategoryforDaySpecial();
            }
               
        }

        protected void btnTempCategory_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    string imageUrl1 = string.Empty;

                    //   Image2 = "https://needly.in/TemplateImage/" + str.ToString();

                    string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);
                    //FileUpload2.SaveAs(Server.MapPath("C:\\Inetpub\\vhosts\\needly.in\\httpdocs\\TemplateImage\\" + filename));
                    FileUpload2.SaveAs(Server.MapPath("~/UploadExcel/" + filename));
                    string filepath = "UploadExcel/" + filename;

                    //dirPath = dirPath + fullName;
                        imageUrl1 = "https://needly.in/" + filepath.ToString();
                        //  Image1.ImageUrl = imageUrl1;

                        SqlCommand cmd = new SqlCommand("SP_InsetTemplateCategory");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();


                        cmd.Parameters.AddWithValue("@CategoryName", txtCategoryName.Text);
                        cmd.Parameters.AddWithValue("@Day", ddlDay.SelectedValue);
                        cmd.Parameters.AddWithValue("@Month", ddlMonth.SelectedValue);
                        cmd.Parameters.AddWithValue("@Dependancy", rbDependancy.SelectedValue);
                        cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                        cmd.Parameters.AddWithValue("@Language", rbLanguage.SelectedValue);
                        cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue);
                        cmd.Parameters.AddWithValue("@Image", imageUrl1);
                        cmd.Parameters.AddWithValue("@ParentId", ddlParentId.SelectedValue);
                        cmd.Parameters.AddWithValue("@DisplaySequence", txtDisplaySequence.Text );

                        //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                        if (A == -1)
                        {
                            ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                        }
                        GridView();
                        DataBaseConnection.Close();
                        //Clear();

                    

                   

                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowTemplateCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVUserLogin.DataSource = ds.Tables[0];
                        GVUserLogin.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void GVUserLogin_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVUserLogin.PageIndex = e.NewPageIndex;
            GridView();
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            // lblMessage.Text = "";
           // Label Mylabel = (Label)e.Row.FindControl("lblId1");
            int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            Label1.Text = GVUserLogin.Rows[rowind].Cells[0].Text;
            string A = GVUserLogin.Rows[rowind].Cells[1].Text;
            //male ='0' and female='1'
            txtCategoryName.Text = GVUserLogin.Rows[rowind].Cells[1].Text;
            ddlDay.SelectedValue = GVUserLogin.Rows[rowind].Cells[2].Text;
            ddlMonth.SelectedValue = GVUserLogin.Rows[rowind].Cells[3].Text;
            rbDependancy.SelectedValue = GVUserLogin.Rows[rowind].Cells[4].Text;
           
            txtDescription.Text = GVUserLogin.Rows[rowind].Cells[5].Text;
            rbLanguage.Text = GVUserLogin.Rows[rowind].Cells[6].Text;
            rbTemplateType.Text = GVUserLogin.Rows[rowind].Cells[7].Text;
           
            btnTempCategory.Visible = false;
            btnUpdate.Visible = true;
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gr = (GridViewRow)btn.NamingContainer;
                int i = Convert.ToInt32(gr.RowIndex);
                string ID = Convert.ToString(GVUserLogin.Rows[i].Cells[0].Text);

                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("sp_DeleteDaySpecial");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", ID);

                    int A = cmd.ExecuteNonQuery();

                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Deleted Successfully...!');", true);
                        GridView();
                    }
                    DataBaseConnection.Close();
                   
                }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
           
            // int i = Convert.ToInt32(gr.RowIndex);
            //string ID = Convert.ToString(GVUserLogin.Rows[i].Cells[0].Text);

            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    string imageUrl1 = string.Empty;

                    //   Image2 = "https://needly.in/TemplateImage/" + str.ToString();

                    string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);
                    //FileUpload2.SaveAs(Server.MapPath("C:\\Inetpub\\vhosts\\needly.in\\httpdocs\\TemplateImage\\" + filename));
                    FileUpload2.SaveAs(Server.MapPath("~/UploadExcel/" + filename));
                    string filepath = "UploadExcel/" + filename;

                    //dirPath = dirPath + fullName;
                    imageUrl1 = "https://needly.in/" + filepath.ToString();
                    //  Image1.ImageUrl = imageUrl1;

                    SqlCommand cmd = new SqlCommand("SP_UpdateTemplateCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", Label1.Text);
                    DataBaseConnection.Open();


                    cmd.Parameters.AddWithValue("@CategoryName", txtCategoryName.Text);
                    cmd.Parameters.AddWithValue("@Day", ddlDay.SelectedValue);
                    cmd.Parameters.AddWithValue("@Month", ddlMonth.SelectedValue);
                    cmd.Parameters.AddWithValue("@Dependancy", rbDependancy.SelectedValue);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    cmd.Parameters.AddWithValue("@Language", rbLanguage.SelectedValue);
                    cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue); 
                    cmd.Parameters.AddWithValue("@Image", imageUrl1);

                    //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    //cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                    //cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                    int A = cmd.ExecuteNonQuery();
                    con.Close();
                    //string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED SUCCESSFULLY.!!!')", true);
                        GridView();
                    }
                    DataBaseConnection.Close();
                    //Clear();
                   
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATE FAILED.!!!')", true);
                }
            }
        }


        public void TemplateCategoryforDaySpecial()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadTemplateSubCategoryForDaySpecial");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlParentId.DataSource = ds.Tables[0];
                        //ddlParentId.DataTextField = "DaySpecial";
                        //ddlParentId.DataValueField = "Id";

                        ddlParentId.DataBind();
                      // ddlParentId.Items.Insert(0, new ListItem(" --Select--  ", ""));
                        //ddlTemplateCategory.SelectedIndex = 0;
                    }
                    ddlParentId.Items.Insert(0, new ListItem("--Select --", "-1"));


                   
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }

    }
}