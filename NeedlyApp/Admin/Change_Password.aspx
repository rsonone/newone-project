﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="Change_Password.aspx.cs" Inherits="NeedlyApp.Admin.Change_Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>   Change Password <small></small></h2>
                       <div class="container-fluid">
            <div class="pull-right">
                
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
           
     
      
       
          <div class="row">
           <div class="panel-body">  
               <div class="box-body col-sm-12">
                <div class="box-body col-sm-6">
                         <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                             Login Id </label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtLoginId" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                        </div>
                         </div>
                   <div class="box-body col-sm-6">
                         <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                            User Name :</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtusername" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                        </div>
                         </div>
                   
                   </div>
               <br />
               <br />
               <div class="box-body col-sm-12">
                <div class="box-body col-sm-6">
                         <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                            Password :</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtPassword" ErrorMessage="Enter New Password" ForeColor="Red" 
                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </div>
                                        </div>
                         </div>
                   <div class="box-body col-sm-6">
                         <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                           Re-Password :</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtRePassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txtRePassword" ErrorMessage="Enter New Re-Password" ForeColor="Red" 
                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="txtPassword" ControlToValidate="txtRePassword" 
                        ErrorMessage="password not same !!" ForeColor="Red"></asp:CompareValidator>
            
                                    </div>
                                        </div>
                         </div>
                   
                   </div>
               
             

               <div class="box-body col-sm-12">
                <div class="box-body col-sm-6">
                         <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                            Mobile NO :</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtmobileno" onkeypress="if(event.keyCode!=9) return CheckNumeric(event);" MaxLength="10" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                        </div>
                         </div>
                   <div class="box-body col-sm-6">
                         <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                          Address :</label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtadderss" runat="server" CssClass="form-control"></asp:TextBox><br /><br />
                                    </div>
                                        </div>
                         </div>
                   
                   </div>
               <br />
               <br /><br />
               <br />

                <div class="box-body col-sm-12">
                    <div class="box-body col-sm-4"></div>
                <div class="box-body col-sm-4">
                    <asp:Button ID="btnUpdate" runat="server" data-toggle="tooltip" Text="Update" OnClick="btnUpdate_Click" class="btn btn-primary"/>

                    </div>
                    <div class="box-body col-sm-4">

                           <asp:Label ID="lbldata" runat="server"></asp:Label>
                    </div>
                </div>
               </div></div>
                    </div></div>
                </div>

    <script language="javascript" type="text/javascript">
        function CheckNumeric(e) {

            if (window.event) // IE 
            {
                if ((e.keyCode < 48 || e.keyCode > 57) & e.keyCode != 8) {
                    event.returnValue = false;
                    return false;

                }
            }
            else { // Fire Fox
                if ((e.which < 48 || e.which > 57) & e.which != 8) {
                    e.preventDefault();
                    return false;

                }
            }
        }
     
    </script>
</asp:Content>
