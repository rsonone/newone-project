﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="Newsandtestimonial.aspx.cs" Inherits="NeedlyApp.Admin.Newsandtestimonial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <div class="clearfix"></div>

     <div class="row">
           <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  News and testimonial page <small></small></h2>
           <div class="clearfix"></div>
                       <div class="container-fluid">
                        
                            
                          <div class="col-md-12">
                   
                    </div>                   
                     </div>          
                  </div>

     </div>


      <div class="box-body col-sm-12">
                            <div class="box-body col-sm-6">
                                <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                                State : </label>
                                            <div class="col-sm-9">
                                        <asp:DropDownList runat="server" ID="ddlState" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                  </asp:DropDownList>
                                    </div>
                                        </div>
                    </div>
                <div class="box-body col-sm-6">
                                <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                                District :</label>
                                            <div class="col-sm-9">
                                               <asp:DropDownList runat="server" ID="ddlDistrict" CssClass="form-control" AutoPostBack="true">
                          
                  </asp:DropDownList>
                                    </div>
             </div>
                    </div>

                        </div>

    <br />
    <br />
       <br />
    <br />
       <div class="box-body col-sm-12">
                         <%--   <div class="box-body col-sm-6">
                                 <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                              Udise Code :</label>
                                            <div class="col-sm-9">
                                                  <asp:TextBox runat="server" ID="txtUDISEcode" CssClass="form-control"/>
                                                </div></div>
                                </div>--%>
             <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                              Heading :</label>
                                            <div class="col-sm-9">
                                                  <asp:TextBox runat="server" ID="txtHeading" CssClass="form-control"/>
                                                </div></div></div>
                              <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                               Priority:</label>
                                            <div class="col-sm-9">
                             <asp:DropDownList runat="server" ID="ddlPriority" CssClass="form-control">
                            <asp:ListItem Text="------Select------" Value="0"></asp:ListItem>
                            <asp:ListItem Text="High" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Medium" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Low" Value="3"></asp:ListItem>
                          
                        </asp:DropDownList>
                             </div></div></div>
                          </div>

     <br />
    <br />
       <br />
        <div class="box-body col-sm-12">
                <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                                Select Type :</label>
                                            <div class="col-sm-9">
                             <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control"  AutoPostBack="true"  OnSelectedIndexChanged="ddlType_SelectedIndexChanged" >
                            <asp:ListItem Text="------Select------" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Image" Value="1"></asp:ListItem>
                            <asp:ListItem Text="News" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Video" Value="3"></asp:ListItem>
                            <asp:ListItem Text="HM Letter" Value="4"></asp:ListItem>
                            <asp:ListItem Text="TeacherRemark Image" Value="5"></asp:ListItem>
                            <asp:ListItem Text="TeacherRemark Video" Value="6"></asp:ListItem>
                            
                        </asp:DropDownList>
                             </div></div></div>
             <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                                App Keyword :</label>
                                            <div class="col-sm-9">
                             <asp:DropDownList runat="server" ID="ddlkeyword" CssClass="form-control"  AutoPostBack="true" OnSelectedIndexChanged="ddlkeyword_SelectedIndexChanged" >
                            <asp:ListItem Text="------Select------" Value="0"></asp:ListItem>
                            <asp:ListItem Value="1">EzeeTest</asp:ListItem>
                            <asp:ListItem Value="2">CLASSAPP</asp:ListItem>
                            <asp:ListItem Value="3">EZEEFORMS</asp:ListItem>
                            <asp:ListItem Value="4">EzeeExam</asp:ListItem>
                            <asp:ListItem Value="5">NEEDLY</asp:ListItem>
                            
                        </asp:DropDownList>
                             </div></div></div>
                 
                    </div>

    
     <br />
    <br />
       <br />

                        <div class="box-body col-sm-12">
                <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                                Details :</label>
                                            <div class="col-sm-9">
                             <asp:TextBox runat="server" ID="txtDetails" CssClass="form-control" TextMode="MultiLine"/>
                             </div></div></div>
                               <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                             Select Image :</label>
                                            <div class="col-sm-9">
                                                  <asp:FileUpload ID="Imageupload" runat="server" CssClass="form-control"/><br />
                        <asp:FileUpload ID="Imageupload1" runat="server" CssClass="form-control"  Visible="false" /><br />
                        <asp:FileUpload ID="Imageupload2" runat="server" CssClass="form-control"  Visible="false" />
                                                
                 <asp:Image ID="Image1" runat="server" Height="100px" Width="150px" Visible="false" />
                                                </div></div></div>
                              
                            
     <br />
    <br />
       <br />
                             <asp:Panel ID="Panelimg" runat="server">
                                                         
     <br />
   
                
                                 </asp:Panel>
                            </div>


       <asp:Panel ID="Panelvideo" runat="server" Visible="false">
                        <div class="box-body col-sm-12">
                <div class="box-body col-sm-6">
                         <div class="form-group">
                            
                              <label class="col-sm-3 control-label" for="input-name2">
                                                Video URL :</label>
                                            <div class="col-sm-9">
                             <asp:TextBox runat="server" ID="txtvideourl" CssClass="form-control" />
                             </div></div></div>
                            </div></asp:Panel>
                             <div class="box-body col-sm-12">
                <div class="box-body col-sm-4">
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                    </div>
                                 <div class="box-body col-sm-4">
                                      <asp:Button Text="Submit" ID="btnSubmit" runat="server" OnClick="btnSubmit_Click"  class="btn btn-round btn-success" />
                        <asp:Button Text="Update" ID="btnUpdate" OnClick="btnUpdate_Click" runat="server"  class="btn btn-round btn-success" Visible="false"/>
                        <asp:Button Text="Reset" ID="btnAddNew" runat="server"  class="btn btn-round btn-primary" OnClick="btnAddNew_Click" />
                    </div>
                                 <div class="box-body col-sm-4">
                    </div>
                             </div>
                         <div class="box-body col-sm-12">
                            <div align="center">
            <div id="datatable-responsive"> 


            </div>
        </div>
                    </div>


      <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="gvNewsDetailsReport" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanged="gvNewsDetailsReport_PageIndexChanged" OnPageIndexChanging="gvNewsDetailsReport_PageIndexChanging" visible="true"
                        PagerStyle-VerticalAlign="Middle" EmptyDataText="No Data Found..." ShowHeaderWhenEmpty="true" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                         --%>
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="Type" HeaderText=" Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Heading" HeaderText="Heading">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                        
                         <asp:BoundField DataField="Details" HeaderText="Details">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                       
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        
                      
                       
                         <asp:TemplateField HeaderText="Update"  >
                                                            <ItemTemplate>
                                                                <asp:Button ID="lnkbtnupdate" CssClass="btn btn-round btn-success" runat="server" CommandArgument='<%#Bind("Id")%>'
                                                                    CommandName="Modify" OnClick="lnkbtnupdate_Click" Text="Edit"></asp:Button>
                                                                   <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Delete">
	                        <ItemTemplate>
		                        <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete" OnClick="deleteButton_Click"
                                  OnClientClick="return confirm('Are you sure you want to delete this data?');" />
                                 <asp:Label ID="lblDelete" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
	                        </ItemTemplate>
                        </asp:TemplateField>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>



</asp:Content>
