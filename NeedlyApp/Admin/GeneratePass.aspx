﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="GeneratePass.aspx.cs" Inherits="NeedlyApp.Admin.GerneratePass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Active Customer list <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                   <%-- <a href="tempCategory.aspx" data-toggle="tooltip" title="Add Category" class="btn btn-primary">
                    <i class="fa fa-plus"></i></a><a href="AddTemplate.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
               --%>
                    </div>
        </div>
                    <div class="clearfix"></div>
                  </div>
                 
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlsearchbymobile" Visible="false" runat="server">

                           
                       
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-8">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                  Enter  Mobile No.: </label>
                                <div class="col-sm-7">
                                  <asp:TextBox ID="txtmobileNumber" runat ="server" CssClass="form-control"></asp:TextBox>
                                   
                          </div></div></div>

                <div class="box-body col-sm-4">
                     <div class="form-group">
                                 <asp:Button ID="btnSearchByMobile" OnClick="btnSearchByMobile_Click"  runat="server" Text="Search" class="btn btn-success"  />
                               </div>     

                </div>

                    </div>
               </div></div> 
 
                            <asp:Label ID="lblerror" runat="server" Font-Bold="true" ForeColor="Red" Font-Size="Large" Text=""></asp:Label>
                          </asp:Panel>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                
                         </div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                             <div class="box-body col-sm-4">
                            
                              <div class="box-body col-sm-4">
                            
                        </div> 
                        </div> 
                            
                             </div></div>

                    </div>
               </div></div> 
                   
                   </ContentTemplate>
                    </asp:UpdatePanel>
                     <div class="col-md-12">
                         <br />
                         <br />
                           <div class="table-responsive">
                    <asp:GridView ID="GVGeneratePass" CssClass="table table-hover table-bordered" runat="server" 
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="MobileNumber" HeaderText="Customer">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Name" HeaderText="Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="FromDate" HeaderText="From Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ToDate" HeaderText="To Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="Admin No.">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Status" HeaderText="Status">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                        <asp:TemplateField HeaderText="Action">
                              <ItemTemplate>
                                 <a>
                                     <asp:Button ID="btnGenaratePass" OnClick="btnGenaratePass_Click" CssClass="btn-success" runat="server" Text="Genarate Paaword" />
                                   <asp:Button ID="btnSendPass"  runat="server" OnClick="btnSendPass_Click" CssClass="btn-primary" Text="Send Password" />
                                 </a>
                                 
                                     <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                        
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>

                                         
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>
</asp:Content>
