﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class NeedlyMarketingPerson : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView();
        }
       
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_ShowMarketingPersonDetails");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddMarketingPerson.DataSource = ds.Tables[0];
                        GVAddMarketingPerson.DataBind();
                    }
                }
                catch (Exception ex)
                {

                }
            }

        }

        protected void GVAddMarketingPerson_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

       
        protected void btnaddmarketingperson_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {   if(rbtnRole.SelectedValue =="1") //marketing
                    {
                        SqlCommand cmd = new SqlCommand("SP_InsetMarketingPersonDetails1");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@MobileNo", txtmobileNumber.Text);
                        cmd.Parameters.AddWithValue("@Name", txtname.Text);
                        cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("Status", "1");
                        cmd.Parameters.AddWithValue("SequenceNo", txtSequenceNo.Text); //sequenc
                        cmd.Parameters.AddWithValue("Capacity", txtCapacity.Text); //capacity
                        cmd.Parameters.AddWithValue("IsWorking", rbtnIsWorking.SelectedValue); //isworking
                        cmd.Parameters.AddWithValue("AddUnder", txtAddUnder.Text); //ddunder
                        
      
                        //cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        //cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        //string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                        if (A == -1)
                        {
                            // ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done','DATA SUBMITTED SUCCESSFULLY.!!!','success')", true);
                        }

                        DataBaseConnection.Close();

                        GridView();
                    }
                    if(rbtnRole.SelectedValue == "2") //cust
                    {
                        SqlCommand cmd = new SqlCommand("SP_InsetMarketingPersonDetails");
                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();

                        cmd.Parameters.AddWithValue("@MobileNo", txtmobileNumber.Text);
                        cmd.Parameters.AddWithValue("@Name", txtname.Text);
                        cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                        cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("Status", "1");
                        // cmd.Parameters.AddWithValue("@Status", "1");
                        //cmd.Parameters.AddWithValue("@TemplateType", rbTemplateType.SelectedValue);

                        //cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                        //cmd.Parameters.AddWithValue("@returnValue", System.Data.SqlDbType.Int);
                        //cmd.Parameters["@returnValue"].Direction = System.Data.ParameterDirection.Output;

                        int A = cmd.ExecuteNonQuery();
                        con.Close();
                        //string DataCode = cmd.Parameters["@returnValue"].Value.ToString();

                        if (A == -1)
                        {
                            // ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                            ClientScript.RegisterClientScriptBlock(this.GetType(), "K", "swal('Done','DATA SUBMITTED SUCCESSFULLY.!!!','success')", true);
                        }

                        DataBaseConnection.Close();

                        GridView();
                    }
                               
                }
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }

        protected void GVAddMarketingPerson_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //  DataRow row = ((DataRowView)e.Row.DataItem).Row;

                //ImageButton img = (ImageButton)e.Row.FindControl("ImageButton1");

                Button mybtn = (Button)e.Row.FindControl("btnStatus");



                if (e.Row.Cells[6].Text == "1")
                {
                    mybtn.Text = "Active";
                    mybtn.BackColor = System.Drawing.Color.Green;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                    // img.Visible = true;
                    //img.ImageUrl = "~/img_status/imgactive.png";
                    //Mylabel.Text = "Active";
                }
                if (e.Row.Cells[6].Text == "0")
                {
                    //img.ImageUrl = "~/img_status/imgdeactive.png";
                    //Mylabel.Text = "deActive";
                    mybtn.Text = "Deactive";
                    mybtn.BackColor = System.Drawing.Color.Red;
                    mybtn.ForeColor = System.Drawing.Color.Black;
                    mybtn.Font.Bold = true;
                    // img.Visible = false;
                }

            }
        }

        protected void btnStatus_Click(object sender, EventArgs e)
        {
            try
            {
                int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
                Label1.Text = GVAddMarketingPerson.Rows[rowind].Cells[0].Text;
               
                string IsActive = GVAddMarketingPerson.Rows[rowind].Cells[6].Text;

                if (IsActive == "0")
                {
                    string Query2 = "update [tbl_AddMarketingPerson] set Status = 1 where Id = '" + Label1.Text + "' ";
                    string Res2 = cc.ExecuteScalar(Query2);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('User Activated successfully.!!!')", true);
                    GridView();
                }
                else
                {
                    string Query2 = "update [tbl_AddMarketingPerson] set Status = 0 where Id = '" + Label1.Text + "' ";
                    string Res2 = cc.ExecuteScalar(Query2);
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('User Deactiveted')", true);
                    GridView();

                }


            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
               
            }
        }

        
        protected void rbtnRole_SelectedIndexChanged(object senSubmider, EventArgs e)
        {
            if (rbtnRole.SelectedValue == "1")
            {

                pnlMarketingPerson.Visible = true;
                //Panel2.Visible = false;
            }
            if (rbtnRole.SelectedValue == "2")
            {
                pnlMarketingPerson.Visible = false;
            }
        }
    }
}