﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.Services;
using System.Web.UI;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class Dashboardcount1 : System.Web.UI.Page
    {
        DataSet ObjDataSet = new DataSet();
        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        DataSet ds = new DataSet();
        CommonCode cc = new CommonCode();
        //Errorlogfil EL = new Errorlogfil();
        CommonCode EL = new CommonCode();
        string mob = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["username"].ToString() == "9422325020")
                {
                    datadashbord1();
                }
                else
                {

                }


                // BindChartbarpassf();
                mob = Convert.ToString(Session["username"]);

                //if (!IsPostBack)
                //{
                //    BindChart();
                //    // dashboard();
                //    // UserMaster();
                //}
            }

        }

        public void datadashbord()
        {
            try
            {
                cmd.CommandText = "sp_dashboardcount1";
                // cmd.CommandText = "Sp_ezeetestuspGetcountDashdataweb";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CreatedBy", txtmobileno.Text);
                cmd.Connection = con;

                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lbldemandjobcount.Text = ds.Tables[0].Rows[0][20].ToString();
                    lblchildcount.Text = ds.Tables[0].Rows[0][21].ToString();
                    lblneedlygrpcount.Text = ds.Tables[0].Rows[0][22].ToString();
                    lbldailybannercount.Text = ds.Tables[0].Rows[0][23].ToString();
                    lblprofileurlname.Text = ds.Tables[0].Rows[0][24].ToString();
                    lblappusedate.Text = ds.Tables[0].Rows[0][25].ToString();
                    lblappversion.Text = ds.Tables[0].Rows[0][26].ToString();
                    lblappinstallationdate.Text = ds.Tables[0].Rows[0][27].ToString();
                    lblpostjobcount.Text = ds.Tables[0].Rows[0][28].ToString();
                    lbluserrole.Text = ds.Tables[0].Rows[0][29].ToString();
                    if(lbluserrole.Text=="2")
                    {
                        lbluserrole.Text = "Business User";

                    }
                    else
                    {
                        lbluserrole.Text = "User";
                    }

                                    }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }
        public void btnsearch_Click(object sender, EventArgs args)
        {

            datadashbord();

        }
        public void datadashbord1()
        {
            try
            {
                cmd.CommandText = "sp_dashboardcount11";
                // cmd.CommandText = "Sp_ezeetestuspGetcountDashdataweb";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Connection = con;

                da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lbldemandjobcount.Text = ds.Tables[0].Rows[0][20].ToString();
                    lblchildcount.Text = ds.Tables[0].Rows[0][21].ToString();
                    lblneedlygrpcount.Text = ds.Tables[0].Rows[0][22].ToString();
                    lbldailybannercount.Text = ds.Tables[0].Rows[0][23].ToString();
                   



                }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Error '" + ex + "' Saved....wor ');", true);
            }
        }
    }
}