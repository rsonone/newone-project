﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class CallReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_DownloadStaff");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@MobileNo", txtmobileno.Text);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddCat.DataSource = ds;
                        GVAddCat.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void txtmobileno_OnTextChanged(object sender, EventArgs e)
        {
            GridView1();
        }

        protected void GVAddCat_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            string lblId1 = GVAddCat.SelectedRow.Cells[2].Text;
            
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT * FROM [tbl_CallLogCountDetails] WHERE CreatedBy = @CreatedBy", DataBaseConnection))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        cmd.Parameters.AddWithValue("@CreatedBy", lblId1);
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        gridcall.Visible = true;
                        gridcall.DataSource = dt;
                        gridcall.DataBind();
                    }
                }
            }
        }
    }
}