﻿using DAL;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace NeedlyApp.Admin
{
    public partial class RechargeWalletReport : System.Web.UI.Page
    {
        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString);

        Errorlogfil EL = new Errorlogfil();
        CommonCode cc = new CommonCode();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PnlOldData.Visible = true;
                PnlAttemptedData.Visible = false;

                lblTotalCount.Visible = false;
                lblCuntDisplay.Visible = false;
                //FillGrid();
            }
        }
        public void FillGrid()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SP_GetRechargeData");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Mobile", txtMob.Text);
                cmd.Parameters.AddWithValue("@Amount", txtAmt.Text);
                cmd.Parameters.AddWithValue("@PurType", ddlPType.SelectedValue);
                cmd.Parameters.AddWithValue("@PayStatus", ddlStatus.SelectedItem.Text);
                cmd.Parameters.AddWithValue("@CouponCode", txtCouponCode.Text);
                cmd.Parameters.AddWithValue("@ReferralCode", txtReferralCode.Text);



                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GVRechWall.DataSource = ds;
                    GVRechWall.DataBind();
                    lblTotalCount.Visible = true;
                    lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
                    lblNodata.Visible = false;
                }
                else
                {
                    lblNodata.Visible = true;
                    lblNodata.Text = "No records found for Purchase Type: '" + ddlPType.SelectedItem.Text + "'  and Status '" + ddlStatus.SelectedItem.Text + "'";
                }
            }
            catch (Exception ex)
            {

            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            FillGrid();
        }
        protected void GVRechWall_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVRechWall.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        public void FillGridAttempted()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetRechargeAttemptedDat");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GvAttemptedData.DataSource = ds;
                    GvAttemptedData.DataBind();
                    lblTotalCount.Visible = true;
                    lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
                    lblNodata.Visible = false;
                }
                else
                {
                    //lblNodata.Visible = true;
                    //lblNodata.Text = "No records found for Purchase Type: '" + ddlPType.SelectedItem.Text + "'  and Status '" + ddlStatus.SelectedItem.Text + "'";
                }
            }
            catch (Exception ex)
            {

            }

        }

        //protected void lnkCash_Click(object sender, EventArgs e)
        //{
        //    Button btn = (Button)sender;
        //    GridViewRow row = (GridViewRow)btn.NamingContainer;
        //    lblId.Text = (btn.FindControl("lblView") as Label).Text;

        //    SqlCommand command4 = new SqlCommand();
        //    command4.Connection = con;
        //    con.Open();

        //    command4.CommandText = "SP_LoadRechargeData";
        //    command4.CommandType = System.Data.CommandType.StoredProcedure;
        //    command4.Parameters.AddWithValue("@Id", lblId.Text);

        //    SqlDataAdapter da4 = new SqlDataAdapter();
        //    da4.SelectCommand = command4;
        //    DataSet ds4 = new DataSet();
        //    da4.Fill(ds4);

        //    String appId = "244795b2ffb42d790e6c20bb997442";
        //    String orderId = ds4.Tables[0].Rows[0][0].ToString();
        //    String orderAmount = ds4.Tables[0].Rows[0][1].ToString();
        //    // String returnUrl = "<return_url>";
        //    // String paymentModes = "";
        //    String secret = "f72ace2922c41f76d63ec51a133834c81fbe848d";

        //    String data = "appId=" + appId + "&secretKey=" + secret + "&orderId=" + orderId ;

        //    WebRequest tRequest = WebRequest.Create("https://api.cashfree.com/api/v1/order/info/status/");
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        //    tRequest.Method = "POST";
        //    tRequest.Headers.Add("cache-control", "no-cache");
        //    tRequest.ContentType = "application/x-www-form-urlencoded";

        //    //tRequest.Headers.Add("X-client-secret", "9e5ba64c06aa56979ce1d844f0a1bf68a7f0ea58");


        //    using (Stream dataStream = tRequest.GetRequestStream())
        //    {
        //        int length = data.Length;
        //        byte[] bytes = Encoding.ASCII.GetBytes(data);

        //        dataStream.Write(bytes, 0, bytes.Length);
        //        //dataStream.Close();
        //        using (WebResponse tResponse = tRequest.GetResponse())
        //        {
        //            using (Stream dataStreamResponse = tResponse.GetResponseStream())
        //            {
        //                using (StreamReader tReader = new StreamReader(dataStreamResponse))
        //                {
        //                    String sResponseFromServer = tReader.ReadToEnd();
        //                    string str = sResponseFromServer;

        //                    //GenarateToken item = Newtonsoft.Json.JsonConvert.DeserializeObject<GenarateToken>(str);

        //                    //XMLToJson.cftoken = item.cftoken;
        //                    //XMLToJson.Status = item.Status;
        //                    //XMLToJson.Message = item.Message;
        //                }
        //            }
        //        }
        //    }

        //    //CashFreeToken n = new CashFreeToken();
        //    //String signature = n.CreateToken(data, secret);
        //    //Console.WriteLine(signature);
        //}
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Verifies that the control is rendered */
        //}
        protected void btnExport_Click(object sender, EventArgs e)
        {

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "RechargeWallet.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVRechWall.AllowPaging = false;
            GVRechWall.PageIndex = 0;

            FillGrid();
            //Change the Header Row back to white color
            GVRechWall.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVRechWall.HeaderRow.Cells.Count; i++)
            {
                GVRechWall.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVRechWall.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();



            //Response.Clear();
            //Response.Buffer = true;
            //Response.ClearContent();
            //Response.ClearHeaders();
            //Response.Charset = "";
            //string FileName = "RechargeWallet" + DateTime.Now + ".xls";


            //StringWriter strwritter = new StringWriter();
            //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.ContentType = "application/vnd.ms-excel";
            //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);

            //GVRechWall.AllowPaging = false;
            //this.FillGrid();

            //GVRechWall.GridLines = GridLines.Both;
            //GVRechWall.HeaderStyle.Font.Bold = true;
            //GVRechWall.RenderControl(htmltextwrtter);
            //Response.Write(strwritter.ToString());
            //Response.End();






            //Response.Clear();
            //Response.Buffer = true;
            //Response.AddHeader("content-disposition", "attachment;filename=RechargeWallet.xlsx");
            //Response.Charset = "";
            //Response.ContentType = "application/vnd.ms-excel";
            //using (StringWriter sw = new StringWriter())
            //{
            //    HtmlTextWriter hw = new HtmlTextWriter(sw);

            //    //To Export all pages
            //    GVRechWall.AllowPaging = false;
            //    this.FillGrid();

            //    //gvReporsurevey.HeaderRow.BackColor = Color.WHITE;
            //    foreach (TableCell cell in GVRechWall.HeaderRow.Cells)
            //    {
            //        cell.BackColor = GVRechWall.HeaderStyle.BackColor;
            //    }
            //    foreach (GridViewRow row in GVRechWall.Rows)
            //    {
            //        //row.BackColor = Color.White;
            //        foreach (TableCell cell in row.Cells)
            //        {
            //            if (row.RowIndex % 2 == 0)
            //            {
            //                cell.BackColor = GVRechWall.AlternatingRowStyle.BackColor;
            //            }
            //            else
            //            {
            //                cell.BackColor = GVRechWall.RowStyle.BackColor;
            //            }
            //            cell.CssClass = "textmode";
            //        }
            //    }

            //    GVRechWall.RenderControl(hw);

            //    //style to format numbers to string
            //    string style = @"<style> .textmode { } </style>";
            //    Response.Write(style);
            //    Response.Output.Write(sw.ToString());
            //    Response.Flush();
            //    Response.End();
            //}
        }

        protected void rdbSelectContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbSelectContact.SelectedValue == "1")
            {
                PnlOldData.Visible = false;
                PnlAttemptedData.Visible = true;
                FillGridAttempted();


            }
            if (rdbSelectContact.SelectedValue == "2")
            {
                PnlOldData.Visible = true;
                PnlAttemptedData.Visible = false;
                // FillGrid();
            }
        }

        protected void btnSearchByDate_Click(object sender, EventArgs e)
        {
            FillGridAttemptedByDate();
        }

        protected void btnSearchAttemptedData_Click(object sender, EventArgs e)
        {
            FillGridAttemptedByMobileNo();
        }
        public void FillGridAttemptedByDate()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetRechargeDataByDate");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", txtFromDate.Text);
                cmd.Parameters.AddWithValue("@ToDate", txtToDate.Text);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    GvAttemptedData.DataSource = ds;
                    GvAttemptedData.DataBind();
                    lblTotalCount.Visible = true;
                    lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
                    lblNodata.Visible = false;
                }
                else
                {
                    //lblNodata.Visible = true;
                    //lblNodata.Text = "No records found for Purchase Type: '" + ddlPType.SelectedItem.Text + "'  and Status '" + ddlStatus.SelectedItem.Text + "'";
                }
            }
            catch (Exception ex)
            {

            }

        }
        public void FillGridAttemptedByMobileNo()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("GetRechargeAttemptedDataByMobileNo");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MobileNo", txtAttemptedMobileNumber.Text);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    GvAttemptedData.DataSource = ds;
                    GvAttemptedData.DataBind();
                    lblTotalCount.Visible = true;
                    lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
                    lblNodata.Visible = false;

                }
                else
                {
                    //lblNodata.Visible = true;
                    //lblNodata.Text = "No records found for Purchase Type: '" + ddlPType.SelectedItem.Text + "'  and Status '" + ddlStatus.SelectedItem.Text + "'";
                }
            }
            catch (Exception ex)
            {

            }

        }

    }
}