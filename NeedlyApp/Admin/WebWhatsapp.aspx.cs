﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json.Linq;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Text;
using OpenQA.Selenium.Remote;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Xml;
using System.Net;
using System.Globalization;
using System.Security.Cryptography;

using System.Data.OleDb;
using System.Data.SqlClient;
using DAL;

using WebApplication3;
using System.Web.Configuration;

namespace NeedlyApp
{
    public partial class WebWhatsapp : System.Web.UI.Page
    {
        WhatsAppConfig wa = new WhatsAppConfig();
        IWebDriver driver;
        Comman cmn = new Comman();
        int argFirstBrowserType = 2, delay = 4;
        string browserType = string.Empty;
        CommonCode cc = new CommonCode();
        protected void btnAddinstance_Click(object sender, EventArgs e)
        {
            bool flag = false;
            string mobieNo = string.Empty;

            //if (Session["WebDriver"] == null)
            cc.WriteMessage("Start : ");

            if (1 == 1)
            {
                try
                {
                    cc.WriteMessage("Step 1:");

                    //browserType = Comman.getBrowser(argFirstBrowserType);
                    //  driver = Comman.SetBrowser(browserType);

                    string path = HttpContext.Current.Server.MapPath("~/CHROMEExe");

                   

                    cc.WriteMessage("Path : " + path);


                    driver = new ChromeDriver(path);
                    cc.WriteMessage("Driver Set");
                    driver.Navigate().GoToUrl("https://web.whatsapp.com");
                    cc.WriteMessage("Url Set");
                    cc.WriteMessage("Browser set successfully");
                    // driver.Navigate().GoToUrl(wa.link);
                    cc.WriteMessage("Driver open whatsapp link successfully");
                }
                catch (Exception ex)
                {
                    cc.WriteMessage("Error : " + ex.Message.ToString());

                    cc.WriteError(ex);
                    // lblshowinstance.Text = ex.Message;
                    //throw ex.Message;
                }


                driver.Manage().Window.Maximize();
                #region login section
                do
                {
                    try
                    {
                        driver.FindElement(By.ClassName("_2MwRD"));
                        flag = true;
                    }
                    catch (Exception ex)
                    {
                    }
                } while (flag == false);

                #endregion login section
                if (flag)
                {
                    lblshowinstance.Text = "Instance Added";
                    lblshowinstance.ForeColor = System.Drawing.Color.LightCoral;
                    lblloggeninMNo.Text = "";
                    Session["WebDriver"] = driver;
                    mobieNo = cmn.GetMobileNumber(driver);
                    lblloggeninMNo.Text = "You Logged In: " + mobieNo;
                    lblloggeninMNo.ForeColor = System.Drawing.Color.Blue;
                    driver.Manage().Window.Minimize();
                }
                else
                {
                    driver.Manage().Window.Maximize();
                    lblloggeninMNo.Text = "";
                    lblshowinstance.Text = "Instance Not added.Please add your whatsapp instance code";
                    lblshowinstance.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                driver = (IWebDriver)Session["WebDriver"];
                lblshowinstance.Text = "Instance Added";
                lblshowinstance.ForeColor = System.Drawing.Color.LightCoral;
                mobieNo = mobieNo = cmn.GetMobileNumber(driver);
                lblloggeninMNo.Text = "You Logged In: " + mobieNo;
                lblloggeninMNo.ForeColor = System.Drawing.Color.Blue;
                driver.Manage().Window.Minimize();
            }
        }
        //public void GetData()
        //{
        //    int selectedcount = 0;
        //    string str = string.Empty;
        //    string strname = string.Empty;
        //    foreach (GridViewRow gvrow in GvShowExcelData.Rows)
        //    {
        //        CheckBox chk = (CheckBox)gvrow.FindControl("chkSelect");
        //        if (chk != null & chk.Checked)
        //        {
        //            selectedcount++;
        //            str = gvrow.Cells[1].Text;

        //        }
        //    }
        //    int selectedCount = selectedcount;
        //}

        protected void btnSendSms_Click(object sender, EventArgs e)
        {
            // string Newmsg = "";
            string Id = string.Empty;
            string TemplateContent1 = string.Empty;
            foreach (GridViewRow row in GvTemplte.Rows)
            {
                RadioButton rb1 = (RadioButton)row.FindControl("RadioButton1");
                if (rb1.Checked)
                {
                    Id = GvTemplte.Rows[row.RowIndex].Cells[0].Text;
                    TemplateContent1 = GvTemplte.Rows[row.RowIndex].Cells[1].Text;
                    //Add your code
                }
            }
            // RadioButton rb = (RadioButton)row.FindControl("RadioButton1");
            //int srno = Convert.ToInt32(Request.Form["RadioButton1"]);
            //string TemplateContent = GvTemplte.Rows[1].Cells[1].Text;
            string Message = TemplateContent1;

            lblmessage.Text = string.Empty;
            WhatsAppConfig wa = new WhatsAppConfig();
            List<WhatsAppConfig> sentList = new List<WhatsAppConfig>();
            List<string> lblmobile = new List<string>();
            foreach (GridViewRow gvrow in GvShowExcelData.Rows)
            {
                var checkbox = gvrow.FindControl("chkSelect") as CheckBox;
                if (checkbox.Checked)
                {
                    int sendMessage = 0;
                   
                    string mobileNumber = "91" + gvrow.Cells[1].Text;
                   

                    if (lblshowinstance.Text == "Instance Added")
                    {
                        driver = (IWebDriver)Session["WebDriver"];
                        try
                        {
                            Console.WriteLine("Start send whats App Message");
                            for (int i = 1; i <= 1; i++)
                            {
                                wa = cmn.SendMessage(driver, mobileNumber, Message, delay);
                                if (wa.Resultcode == "0")
                                {
                                    wa.To_number = mobileNumber;
                                    wa.Message = Message;
                                    wa.mobile_number = lblloggeninMNo.Text;
                                    sentList.Add(wa);
                                    // lbltotsentsms.Text = "Send Message: " + Convert.ToString(sendMessage);
                                    //  pendingMessage = lstcnt - sendMessage;
                                    // lblpendingmsg.Text = "Pending Message " + Convert.ToString(pendingMessage);
                                    sendMessage++;
                                    Thread.Sleep(300);
                                    continue;
                                }
                                else
                                {
                                    break;

                                }
                            }

                            if (wa.Resultcode == "0")
                            {
                                lblmessage.Text = "Message send successfully. Total count: =  " + sentList.Count;
                                lblmessage.ForeColor = System.Drawing.Color.Green;
                            }
                            else if (wa.Resultcode == "99")
                            {
                                lblmessage.Text = "Error while sending:" + wa.Message;
                                lblmessage.ForeColor = System.Drawing.Color.Red;
                            }
                            else
                            {
                                lblmessage.Text = "Error while sending:" + wa.Message;
                                lblmessage.ForeColor = System.Drawing.Color.Red;
                            }

                            Console.WriteLine("End send whats App Message");
                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine("Exception while sending whats App Message");
                            throw ex;
                        }
                        finally
                        {
                            if (sentList.Count > 0)
                            {
                                //You can maintain here your database script for updated send list.
                                //Also store the message list for historical purpose.

                                //gridmsglist.DataSource = sentList;
                                //gridmsglist.DataBind();
                            }
                        }
                    }
                    else
                    {
                        lblloggeninMNo.Text = "";
                        lblshowinstance.Text = "Instance Not added.Please add your whatsapp instance code";
                        lblshowinstance.ForeColor = System.Drawing.Color.Red;
                        //lbltotsentsms.Text = string.Empty;
                        //lblpendingmsg.Text = string.Empty;
                        //lblbtotalsms.Text = string.Empty;
                        //gridmsglist.DataSource = null;
                        //gridmsglist.DataBind();
                    }

                }

            }



            //int lstcnt = 2, sendMessage = 1, pendingMessage = 0;
            //string mobileNumber = "";
            //lbltotsentsms.Text = "0";
            //lblpendingmsg.Text = string.Empty;
            //lblbtotalsms.Text = "Total Message: " + lstcnt.ToString();

            //if (lblshowinstance.Text == "Instance Added")
            //{
            //    driver = (IWebDriver)Session["WebDriver"];
            //    try
            //    {
            //        Console.WriteLine("Start send whats App Message");
            //        for (int i = 1; i <= lstcnt; i++)
            //        {
            //            wa = cmn.SendMessage(driver, mobileNumber, Message, delay);
            //            if (wa.Resultcode == "0")
            //            {
            //                wa.To_number = mobileNumber;
            //                wa.Message = Message;
            //                wa.mobile_number = lblloggeninMNo.Text;
            //                sentList.Add(wa);
            //                lbltotsentsms.Text = "Send Message: " + Convert.ToString(sendMessage);
            //                pendingMessage = lstcnt - sendMessage;
            //                lblpendingmsg.Text = "Pending Message " + Convert.ToString(pendingMessage);
            //                sendMessage++;
            //                Thread.Sleep(300);
            //                continue;
            //            }
            //            else
            //            {
            //                break;

            //            }
            //        }

            //        if (wa.Resultcode == "0")
            //        {
            //            lblmessage.Text = "Message send successfully. Total count: =  " + sentList.Count;
            //            lblmessage.ForeColor = System.Drawing.Color.Green;
            //        }
            //        else if (wa.Resultcode == "99")
            //        {
            //            lblmessage.Text = "Error while sending:" + wa.Message;
            //            lblmessage.ForeColor = System.Drawing.Color.Red;
            //        }
            //        else
            //        {
            //            lblmessage.Text = "Error while sending:" + wa.Message;
            //            lblmessage.ForeColor = System.Drawing.Color.Red;
            //        }

            //        Console.WriteLine("End send whats App Message");
            //    }
            //    catch (Exception ex)
            //    {

            //        Console.WriteLine("Exception while sending whats App Message");
            //        throw ex;
            //    }
            //    finally
            //    {
            //        if (sentList.Count > 0)
            //        {
            //            //You can maintain here your database script for updated send list.
            //            //Also store the message list for historical purpose.

            //            //gridmsglist.DataSource = sentList;
            //            //gridmsglist.DataBind();
            //        }
            //    }
            //}
            //else
            //{
            //    lblloggeninMNo.Text = "";
            //    lblshowinstance.Text = "Instance Not added.Please add your whatsapp instance code";
            //    lblshowinstance.ForeColor = System.Drawing.Color.Red;
            //    lbltotsentsms.Text = string.Empty;
            //    lblpendingmsg.Text = string.Empty;
            //    lblbtotalsms.Text = string.Empty;
            //    //gridmsglist.DataSource = null;
            //    //gridmsglist.DataBind();
            //}
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {


                    lblshowinstance.Text = string.Empty;
                    Session["WebDriver"] = null;
                    lblmessage.Text = string.Empty;
                    lblloggeninMNo.Text = string.Empty;
                    //gridmsglist.DataSource = null;
                    //gridmsglist.DataBind();
                    //lbltotsentsms.Text = string.Empty;
                    //lblpendingmsg.Text = string.Empty;
                    //lblbtotalsms.Text = string.Empty;
                    BindSrNo1();
                    BindSrNo2();
                    GridViewTemplate();
                }


            }
            catch (Exception ex)
            {
                Response.Redirect(ex.Message);
                // throw ex;
            }

        }
        public void GridViewTemplate()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString1"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select Id, TemplateContent from Tbl_WhatsAppTemplateDetails where CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvTemplte.DataSource = ds.Tables[0];
                        GvTemplte.DataBind();
                    }

                    //string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
                    //String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    //LblContactCount.Text = ExcelContactCount;
                    //lblExcelCountMsg.Visible = true;
                    //PnlCB.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void GridViewForSerch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select SrNo ,MobileNo,CustomerName,CreatedBy from tbl_ContactDetails where MobileNo= '" + txtMobileNumber.Text + "' and CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvShowExcelData.DataSource = ds.Tables[0];
                        GvShowExcelData.DataBind();
                    }
                    else
                    {
                        Response.Redirect("No rocord found for Mobile number '" + txtMobileNumber.Text + "'");
                    }


                }
                catch (Exception ex)
                {

                }
            }
        }
        public void BindSrNo1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select * from tbl_ContactDetails where  CreatedBy = '" + Session["username"].ToString() + "' ");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    DdlSrno1.DataSource = dt;
                    DdlSrno1.DataBind();
                    DdlSrno1.DataTextField = "SrNo";
                    DdlSrno1.DataValueField = "ID";
                    DdlSrno1.DataBind();


                }
                catch (Exception ex)
                {

                }
            }




        }
        public void BindSrNo2()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select * from tbl_ContactDetails where  CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    DdlSrno2.DataSource = dt;
                    DdlSrno2.DataBind();
                    DdlSrno2.DataTextField = "SrNo";
                    DdlSrno2.DataValueField = "ID";
                    DdlSrno2.DataBind();


                }
                catch (Exception ex)
                {

                }
            }




        }

        public void BindBySrNo()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    //SqlCommand cmd = new SqlCommand("Select SrNo ,MobileNo,CustomerName,CreatedBy from tbl_ContactDetails where  CreatedBy = '" + Session["username"].ToString() + "' and SrNo Between '"+ DdlSrno1.SelectedItem.Text+"' and '"+DdlSrno2.SelectedItem.Text+"'");


                    try

                    {
                        SqlCommand cmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;


                        cmd.Connection = DataBaseConnection;
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataBaseConnection.Open();
                        cmd.CommandText = "SP_DownloadCustomerContactDetails1";
                        cmd.Parameters.AddWithValue("CreatedBy", Session["username"].ToString());
                        cmd.Parameters.AddWithValue("SrNo1", DdlSrno1.SelectedItem.Text);
                        cmd.Parameters.AddWithValue("SrNo2", DdlSrno2.SelectedItem.Text);

                        GvShowExcelData.DataSource = cmd.ExecuteReader();
                        GvShowExcelData.DataBind();

                    }

                    catch (Exception ex)

                    {

                        throw ex;

                    }
                    //else
                    //{
                    //    Response.Redirect("No rocord found for Mobile number '" + txtMobileNumber.Text + "'");
                    //}

                }
                catch (Exception ex)
                {

                }
            }




        }

        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("Select SrNo ,MobileNo,CustomerName,CreatedBy from tbl_ContactDetails where CreatedBy = '" + Session["username"].ToString() + "'");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvShowExcelData.DataSource = ds.Tables[0];
                        GvShowExcelData.DataBind();
                    }

                    string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
                    String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;
                    PnlSearch.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }

        protected void BtnSearchBySrNo_Click(object sender, EventArgs e)
        {
            BindBySrNo();
        }

        protected void btnSearchByMobile_Click(object sender, EventArgs e)
        {
            GridViewForSerch();
        }

        protected void rdbSelectContact_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbSelectContact.SelectedValue == "1")
            {
                PnlShowExclData.Visible = true;
                PnlPhoneContacts.Visible = false;
                GridView();
            }
            if (rdbSelectContact.SelectedValue == "2")
            {
                PnlShowExclData.Visible = false;
                PnlPhoneContacts.Visible = true;
                GridViewPhoneContact();
            }
        }

        protected void cbIagree_CheckedChanged(object sender, EventArgs e)
        {
            if (cbIagree.Checked == true)
            {
                btnSendSms.Visible = true;
            }
            if (cbIagree.Checked == false)
            {
                btnSendSms.Visible = false;
            }
        }

        public void GridViewPhoneContact()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["DBNeedlyConnectionString"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("select Id , MobileNo, FirstName from tbl_PhoneContacts where CreatedBy = '" + Session["username"].ToString() + "' ");
                    cmd.Connection = DataBaseConnection;


                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GvPhoneContact.DataSource = ds.Tables[0];
                        GvPhoneContact.DataBind();
                    }

                    string SQl1 = "SELECT COUNT(Id) FROM tbl_PhoneContacts WHERE CreatedBy= '" + Session["username"].ToString() + "'";
                    String ExcelContactCount = cc.ExecuteScalar(SQl1);
                    LblContactCount.Text = ExcelContactCount;
                    lblExcelCountMsg.Visible = true;

                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
