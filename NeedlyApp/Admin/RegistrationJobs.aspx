﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="RegistrationJobs.aspx.cs" Inherits="NeedlyApp.Admin.RegistrationJobs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Registration for Jobs<small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
                
                        </div>
                        </div>
                    <div class="clearfix"></div>
                  </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                    Mobile number: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtmobileno" CssClass="form-control" placeholder="Enter Your MobileNo" MaxLength="10"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 In which sector job is required?: </label>
                                <div class="col-sm-6">
                                 <asp:CheckBoxList ID="chkjobsector" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">  
                <asp:ListItem Value="0">Private&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>  
                <asp:ListItem Value="1">Semi Government&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem>  
                <asp:ListItem Value="2">Government&nbsp;&nbsp;&nbsp;&nbsp;</asp:ListItem></asp:CheckBoxList> 

                          </div></div></div>
                    </div>
               </div>
                         </div>
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 For government job do u expect training?: </label>
                                <div class="col-sm-6">
                                <asp:RadioButtonList ID="rbtnexpectjob" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes&nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="No&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Are you interested for Police Bharti?: </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rbtninterest" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes &nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="No &nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Do you want training for Police Bharti: </label>
                                <div class="col-sm-6">
                                 <asp:RadioButtonList ID="rbtnpolice" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                          <asp:ListItem Text="Yes &nbsp;&nbsp;&nbsp;&nbsp;" Value="1"></asp:ListItem>
                                          <asp:ListItem Text="No&nbsp;&nbsp;&nbsp;&nbsp;" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>

                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                Name of Competitive exams for wish you want coaching/ training: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtexams" TextMode="MultiLine" CssClass="form-control"  runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                 Any special achievements: </label>
                                <div class="col-sm-6">
                                 <asp:TextBox ID="txtachive" CssClass="form-control"  TextMode="MultiLine" Rows="4" runat="server"></asp:TextBox>
                          </div></div></div>
                    </div>
               </div>
                         </div>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-12">
                         <div class="form-group">
                                <label class="col-sm-4 control-label" for="input-name2">
                                        For private Job which District are preferable  : </label>
                                <div class="col-sm-6">
<asp:CheckBoxList ID="Chklist" runat="server"  AutoPostBack="True" CellPadding="5"  CellSpacing="5" RepeatColumns="5" RepeatDirection="Horizontal"  RepeatLayout="Table"   Width="600" TextAlign="Right"
    >  
                <asp:ListItem value="0"></asp:ListItem> </asp:CheckBoxList>     
                                    
                                       


                          </div></div></div>
                    </div>
               </div>
                         </div>
                    
                     <div class="col-md-12">
           <div class="box-body col-sm-5">
                         
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" OnClick="btnUpdate_Click"  />
           </div>


        </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVAddQues"  runat="server"   OnPageIndexChanging="GVAddQues_PageIndexChanging"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                      <%--  <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%>    
                           <asp:BoundField DataField="Id" HeaderText="ID">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="Jobsector" HeaderText="Job Sector">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="Governmentjobtraining" HeaderText="Government Job Training">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Policebhartiinterest" HeaderText="Police Bharti Interest">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="PoliceBhartiTraining" HeaderText="Police Bharti Training">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Competitiveexams" HeaderText="Name of Competitive Exams">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="achievements" HeaderText="Achievements">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="preferabledistrict" HeaderText="Preferable District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="Created By">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         
                         
                         <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" Text="Edit" OnClick="btnEdit_Click"  />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                     <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>

                    </div>
                  </div>
                </div>
</asp:Content>
