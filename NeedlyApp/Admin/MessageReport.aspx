﻿  <%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="MessageReport.aspx.cs" Inherits="NeedlyApp.Admin.MessageReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><i class="fa fa-list"></i>  Message and Excel Upload Report <small></small></h2>
                            <div class="container-fluid">
                                <div class="pull-right">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="input-name2">
                                                Select Option
                                            </label>
                                            <div class="col-sm-8">
                                                <asp:RadioButtonList ID="rdbSelectContact" OnSelectedIndexChanged="rdbSelectContact_SelectedIndexChanged" runat="server"  RepeatDirection="Vertical" AutoPostBack="true">
                                                    <asp:ListItem Value="1">&nbsp;&nbsp;Check  Excel Report&nbsp;&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="2">&nbsp;&nbsp;Check Message Count Report&nbsp;&nbsp;</asp:ListItem>
                                                    <asp:ListItem Value="3">&nbsp;&nbsp;Check Contact Report&nbsp;&nbsp;</asp:ListItem>

                                                </asp:RadioButtonList>
                                            </div>

                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-10">
                                        <div class="form-group">
                                            <asp:Label ID="lblExcelCountMsg" Font-Bold="true" Font-Size="Larger" ForeColor="Black" runat="server" Visible="false" Text="Total Contacts Uploded :"></asp:Label>
                                            <asp:Label ID="lblWhatsAppMsgCount" Font-Bold="true" Font-Size="Larger" ForeColor="Black" runat="server" Visible="false" Text="Total WhatsApp Message count : "></asp:Label>
                                            <asp:Label ID="lblPhoneContactCount" Font-Bold="true" Font-Size="Larger" ForeColor="Black" runat="server" Visible="false" Text="Total  Phone Contacts count : "></asp:Label>

                                            <asp:Label ID="LblContactCount" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblNoData" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <asp:Panel ID="PnlSearchForExcelData" runat="server" Visible="false">
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                      
                                        <div class="box-body col-sm-6">

                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtMobileExcelNumber" placeholder="Enter Mobile Number" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-5">

                                                <asp:Button ID="btnSearchFromExcelData" CssClass="btn btn-success" runat="server" OnClick="btnSearchFromExcelData_Click" Text="Search" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                         <asp:Panel ID="PnlSearchForMsgCount" runat="server" Visible="false">
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                      
                                        <div class="box-body col-sm-6">

                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtmsgcountMobile" placeholder="Enter Mobile Number" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-5">

                                                <asp:Button ID="btnSearchFromMsgCount" CssClass="btn btn-success" runat="server" OnClick="btnSearchFromMsgCount_Click" Text="Search" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                         <asp:Panel ID="PnlSearchForPhoneContacts" runat="server" Visible="false">
                            <div class="row">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                      
                                        <div class="box-body col-sm-6">

                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txtContactMobile" placeholder="Enter Mobile Number" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-5">

                                                <asp:Button ID="btnSearchFromContact" CssClass="btn btn-success" runat="server" OnClick="btnSearchFromContact_Click" Text="Search" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlPhoneContacts" runat="server">
                            <asp:GridView ID="GvPhoneContact" style="height: 400px; overflow: auto" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">

                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                    <asp:BoundField DataField="MobileNo" HeaderText="Mobile" />
                                    <asp:BoundField DataField="FirstName" HeaderText="Name" />
                                    <asp:BoundField DataField="CreatedBy" HeaderText="App User No." />
                                   
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />

                            </asp:GridView>
                        </asp:Panel>

                          <asp:Panel ID="PnlWhatsappMsgCount" runat="server" >
                            <div class="table-responsive">
                                <div class="row" style="height: 400px; overflow: auto">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <asp:GridView ID="GvWhatsappMsgCount" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                
                                                    <asp:BoundField DataField="Id" HeaderText="Id" />
                                                     <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" />
                                                     <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" />
                                                     <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" />
                                                     <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" />
                                                     <asp:BoundField DataField="MessageCount" HeaderText="MessageCount" />
                                                     <asp:BoundField DataField="Status" HeaderText="Status" />
                                                     <asp:BoundField DataField="PreviousMsgCount" HeaderText="PreviousMsgCount" />


                                                </Columns>
                                                <EditRowStyle BackColor="#2461BF" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlShowExclData" runat="server" >
                            <div class="table-responsive">
                                <div class="row" style="height: 400px; overflow: auto">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <asp:GridView ID="GvShowExcelData" CssClass="table table-hover table-bordered" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                   
                                                    <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" />
                                                   
                                                     <asp:BoundField DataField="Contact_Count" HeaderText="No of Contacts" />


                                                </Columns>
                                                <EditRowStyle BackColor="#2461BF" />
                                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                <RowStyle BackColor="#EFF3FB" />
                                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>

                       
                    </div>
                </div>
            </div>

</asp:Content>
