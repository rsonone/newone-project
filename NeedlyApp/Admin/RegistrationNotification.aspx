﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="RegistrationNotification.aspx.cs" Inherits="NeedlyApp.Admin.RegistrationNotification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-list"></i> Registration Notification Access Details<small></small></h2>
                    <div class="container-fluid">
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="box-body col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-name2">
                                        State:
                                    </label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                                </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>

                            <div class="box-body col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        District:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlDistrict" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" >
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>

                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                              <div class="box-body col-sm-4">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                        Taluka:
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlTaluka" runat="server" AppendDataBoundItems="True" CssClass="form-control">
                                            <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        </asp:DropDownList><br />
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="panel-body">
                        <div class="col-md-12">
                          
                           <div class="box-body col-sm-5">
                                <label class="col-sm-3 control-label" for="input-name2">
                                        Mobile No
                                    </label>
                                <div class="col-sm-9">
                          <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                    </div>
                              <div class="box-body col-sm-5">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="input-name2">
                                       Name :
                                    </label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtName" CssClass=" form-control" runat="server"></asp:TextBox> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

              

                 <div class="col-md-12">
                   
                   <div class="box-body col-sm-5">
                         
                    </div>
                    <div class="box-body col-sm-4">
                         <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" CssClass="btn-primary" runat="server" Text="Submit" />
                     
                    </div>
                    
                </div>
                 <div class="col-md-12">
                    <div class="box-body col-sm-5">
                      
                    </div>
                   
                    <div class="box-body col-sm-4">
                       
                    </div>
                </div>

               
                  <div class="col-md-12">
                       <br />
                     <div class="table-responsive">
                     <asp:GridView ID="GVReg_Report"  runat="server"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%>
                          <asp:BoundField DataField="Id" HeaderText="Id">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="State" HeaderText="State">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Name" HeaderText="Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="MobileNo" HeaderText="Mobile No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                       <%--  <asp:BoundField DataField="CreatedBy" HeaderText="User">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>--%>
                          <asp:BoundField DataField="CreatedDate" HeaderText="Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                        <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="Delete" runat="server" Text="Delete"  OnClick="Delete_Click" />
                                      <i></i></a></a>
                                     <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                        
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div> <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
