﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reg_ExcelUpload.aspx.cs" MasterPageFile="~/MasterPage/NeedlyMaster.Master" Inherits="NeedlyApp.Admin.Reg_ExcelUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Registration Excel Uplaod <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlstate" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddldistrict"  CssClass="form-control" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddldistrict_SelectedIndexChanged" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Taluka: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlTaluka" CssClass="form-control"  runat="server" AutoPostBack="true" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList><br />
                          </div></div></div>

<%--                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Ward_No: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtWard" runat ="server" CssClass="form-control"></asp:TextBox>
                           </div></div></div>--%>

                    </div>
               </div></div> 

                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                     <div class="box-body col-sm-offset-2  col-sm-4">
                           <asp:LinkButton ID="lnkbtnDownload" runat="server" Text="Click Here!!! Download Excel Format." Font-Bold="true" Font-Size="Medium" OnClick="lnkbtnDownload_Click"   ForeColor="Red"></asp:LinkButton>
                          </div>
                    </div>
               </div></div> 

                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" />
                              
                              <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" OnClick="btnreset_Click" />
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                      <div class="col-md-12">
                          <div class="box-body col-sm-offset-2  col-sm-4">
                              <asp:Label ID="Label1" runat="server" Text="Count" Visible="false" Font-Bold="true"></asp:Label>
                              <asp:Label ID="lalCount" runat="server" Font-Bold="true" ></asp:Label>
                              <br />
                              <asp:Label ID="lblMessage" runat="server" Font-Bold="true" ></asp:Label>
                          </div>
                      </div>

                     <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVRegistration" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVRegistration_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>  --%>   
                        <asp:BoundField DataField="EzeeDrugAppId" HeaderText="ID">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="firmName" HeaderText="Firm Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="mobileNo" HeaderText="Mobile No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="address" HeaderText="Address">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Taluka" HeaderText="Taluka">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="WardNo" HeaderText="WardNo">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="EntryDate" HeaderText="Entry Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
           

                  </div>
                </div></div>
    </asp:Content>
