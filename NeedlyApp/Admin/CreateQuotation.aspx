﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="CreateQuotation.aspx.cs" Inherits="NeedlyApp.Admin.CreateQuotation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Quotations <small></small></h2>
                   
                       <div class="container-fluid">
                        <div class="pull-right">
                            <a href="QuotationReport.aspx" data-toggle="tooltip" title="Quotation Report" class="btn btn-danger">Quotation Report</a>
                            <a href="CreateQuotation.aspx" data-toggle="tooltip" title="Refresh"
                        class="btn btn-default"><i class="fa fa-refresh"></i></a>
               
                      
        </div>
                    <div class="clearfix"></div>
               
          <div class="row">
            <div class="panel-body">
                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Quotation No: </label>
                               <div class="col-sm-9">
                                    <asp:TextBox ID="txtquotationid" CssClass="form-control" OnTextChanged="txtquotationid_OnTextChanged" AutoPostBack="true" PlaceHolder="Enter Quotation Number" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="requiredforquotationid" runat="server" ForeColor="Red" ControlToValidate="txtquotationid" ErrorMessage="Please Enter Quotation Number" ValidationGroup="B" ></asp:RequiredFieldValidator>

                                                                 </div></div></div>
                                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                Quotation Date: </label>
                              <div class="col-sm-9">
                                    <asp:TextBox ID="txtdate" CssClass="form-control" TextMode="Date" runat="server"></asp:TextBox>
                                       </div></div></div>
                                    </div>
               </div></div>
                 <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Customer: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlcustomer" CssClass="form-control" runat="server" AutoPostBack="true"  >
                                 <asp:ListItem  Text="--Select--" Value="0"></asp:ListItem>
                                                                            </asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="ddlcustomer" InitialValue="--Select--" runat="server" ForeColor="Red" ValidationGroup="B" />
                          </div></div></div>
                                    <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Header Note: </label>
                              <div class="col-sm-9">
                                    <asp:TextBox ID="txtheadernote" PlaceHolder="Enter HeaderNote Here" CssClass="form-control" runat="server"></asp:TextBox>
                                                       <asp:RequiredFieldValidator ID="requiredforheadernote" runat="server" ForeColor="Red" ControlToValidate="txtheadernote" ErrorMessage="Please Enter Header Note" ValidationGroup="B" ></asp:RequiredFieldValidator>

                                       </div></div></div>
                                        </div>
               </div></div>
                                   <div class="row">
           <div class="panel-body" >
                     <div class="col-md-12">
                        <div class="box-body col-sm-6">
                         <div class="form-group">
                               <label class="col-sm-3 control-label" for="input-name2">
                                   Footer Note: </label>
                               <div class="col-sm-9">
                                    <asp:TextBox ID="txtfooternote" PlaceHolder="Enter FooterNote Here" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="requiredforfooternote" runat="server" ControlToValidate="txtfooternote" ForeColor="Red" ErrorMessage="Please Enter Footer Note" ValidationGroup="B" ></asp:RequiredFieldValidator>

                                       </div></div></div>
                        
        </div> </div></div> 
                 <div class="row">
           <div class="panel-body" >
                     <div class="col-md-12">
                           <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Quotation  Group: </label>
                              <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlquotationgroup" CssClass="form-control" runat="server" AutoPostBack="true"  >
                                 <asp:ListItem  Text="--Select--"></asp:ListItem>
                               </asp:DropDownList>
                      <asp:RequiredFieldValidator ID="requiredforquotationgrp" runat="server" ControlToValidate="ddlquotationgroup" ForeColor="Red" InitialValue="0" ErrorMessage="Select Quotation Group" ValidationGroup="B"></asp:RequiredFieldValidator>

                                       </div></div></div>
                       
           <div class="container-fluid">
                        <div class="pull-right">
                     <asp:Button ID="btnaddproductdata" runat="server"  class="btn btn-primary" Text="Add Product" OnClick="btnaddproductdata_Click" ValidationGroup="B" />
                         <asp:Button ID="btnrefresh" runat="server"  class="btn btn-primary" Text="Refresh" OnClick="btnrefresh_Click"  />      
                                         </div>
        </div>
                      </div>
                    </div>
                  </div>
                <div>
                     <asp:Panel ID="pnladdproduct" Visible="false" runat="server"  style="border-style: solid; ">
                                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                     Product: </label>
                               <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlproduct" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlproduct_SelectedIndexChanged"  >
                                 <asp:ListItem  Text="--Select--"></asp:ListItem>
                                                                            </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="requiredforproduct" ForeColor="Red" runat="server" ControlToValidate="ddlproduct" InitialValue="0" ErrorMessage="Select Product" ValidationGroup="B"></asp:RequiredFieldValidator>
                                                   
                                                                 </div></div></div>
                                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                               Rate: </label>
                              <div class="col-sm-9">
                                    <asp:TextBox ID="txtrate" CssClass="form-control" ReadOnly="true"  runat="server"></asp:TextBox>
                                       </div></div></div>
                                    </div>
               </div></div>
                                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Unit: </label>
                                <div class="col-sm-9">
                                   <asp:TextBox ID="txtunit" CssClass="form-control" ReadOnly="true"  runat="server"></asp:TextBox><br />
                          </div></div></div>
                                    <div class="box-body col-sm-4">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Quantity: </label>
                              <div class="col-sm-9">
                                    <asp:TextBox ID="txtquantity" CssClass="form-control" AutoPostBack="true"  OnTextChanged="txtquantity_OnTextChanged" runat="server"></asp:TextBox>
                                       </div></div></div>
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Total Amount: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txttotal" CssClass="form-control" ReadOnly="true" runat="server"></asp:TextBox><br />
                          </div></div></div>

                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                    
                    </div>
               </div>
                  <div class="col-md-12">
           <div class="box-body col-sm-5">
                         <asp:Label ID="Label1" Visible="false" runat="server"></asp:Label>  
           </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnaddproduct" runat="server" Text="Add Product In List"  OnClick="btnaddproduct_Click" class="btn btn-success" ValidationGroup="B"  />
                                                                 </div>
                       <div class="box-body col-sm-4">
     <asp:Label ID="Label2" runat="server"></asp:Label>    
           </div>
        </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVProduct"  runat="server"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" OnRowDataBound = "GVProduct_OnRowDataBound" OnPageIndexChanging="GVProduct_PageIndexChanging" OnRowDeleting="GVProduct_OnRowDeleting" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <asp:TemplateField HeaderText="Id">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>    
                         <asp:BoundField DataField="Quotation_Grp_Name" HeaderText="Quotation Group">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="ItemName" HeaderText="Product">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Rate" HeaderText="Rate">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Unit" HeaderText="Unit">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Amount" HeaderText="Total Amount">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                           <asp:CommandField ShowDeleteButton="True" ButtonType="Button" />
                        
                          
                         
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         </div>
                    </div>
                </div>
                       
                           </div>
                  </asp:Panel>

                </div>
                <br/>
                  <div class="col-md-12">
           <div class="box-body col-sm-5">
                                   </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit"  OnClick="btnSubmit_Click"  ValidationGroup="B"  class="btn btn-success"  />
                             <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" OnClick="btnUpdate_Click" />
                                                                 </div>
 <div class="box-body col-sm-4">
     <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
           </div>
        </div>
                <div class="col-md-12">
                     <div class="table-responsive">
                     <asp:GridView ID="GVBanner"  runat="server"   PagerStyle-VerticalAlign="Middle" 
                        PageSize="10" AutoGenerateColumns="false" OnPageIndexChanging="GVBanner_PageIndexChanging" AllowPaging="true" Font-Names="Arial" CssClass="table table-hover table-bordered" >
                     <Columns>   
                        <%--<asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>  --%>   
                        <asp:BoundField DataField="Id" HeaderText="SrNo" Visible="false">
                              <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                              <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="Quotation_No" HeaderText="Quotation No">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Date" HeaderText="Quotation Date">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Customer_Name" HeaderText="Customer Name">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Quotation_Grp_Name" HeaderText="Quotation Group">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Item_Name" HeaderText="Product">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Price" HeaderText="Rate">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Unit" HeaderText="Unit">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                          <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:BoundField DataField="Amount" HeaderText="Total Amount">
                               <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                               <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                          </asp:BoundField>
                         <asp:TemplateField HeaderText="Edit">
                              <ItemTemplate>
                                  <a> <asp:Button ID="btnEdit" runat="server" Text="Edit"  OnClick="btnEdit_Click"  />
                                      <i></i></a></a>
                                      <asp:Label ID="lblId1" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                              </ItemTemplate>
                         </asp:TemplateField>
                 </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                         </div>
                    </div>
                </div>
</div></div></div></div>
</asp:Content>
