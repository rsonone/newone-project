﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="RechargeWalletScheme.aspx.cs" Inherits="NeedlyApp.Admin.RechargeWalletScheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
     <div class="clearfix"></div>
    <div class="row">
           <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Recharge Wallet Scheme page <small></small></h2>
           <div class="clearfix"></div>
                       <div class="container-fluid">
                        
                            
                          <div class="col-md-12">
                   
                    </div>                   
                     </div>          
                  </div>

     </div>

       <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_OnSelectedIndexChanged" RepeatDirection="Horizontal" CssClass="checkbox">
                                                        <asp:ListItem Value="0" >  Discount Scheme  &nbsp;  &nbsp;</asp:ListItem>
                                                        <asp:ListItem Value="1">  Free Code &nbsp; &nbsp;</asp:ListItem>
                                                       
                                                    </asp:RadioButtonList>

     <div class="box-body">
               <asp:Panel runat="server" ID="PanelDiscount" Visible="false"> 
          <div class="row">
            <div class="panel-body"> 
      
                 <div class="row">
                       <div class="col-md-12">
                              <div class="col-md-6">
                                  <label class="col-sm-5 control-label" for="input-name2"> Select Purchase Type: </label>
                                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="1">Course Purchase</asp:ListItem>
                                                     <asp:ListItem  Value="2">Open Schooling</asp:ListItem>
                                                     <asp:ListItem  Value="3">Virtual School</asp:ListItem>
                                                     <asp:ListItem  Value="4">Advance Training</asp:ListItem>
                                                    <asp:ListItem  Value="5">Child Safety</asp:ListItem>
                                                   <asp:ListItem  Value="6">Super Specility Training </asp:ListItem>
                                                   <asp:ListItem  Value="7"> Parent Webinar </asp:ListItem>
                                                   <asp:ListItem  Value="8"> WhatsApp Premium </asp:ListItem>
                                                    <asp:ListItem  Value="9"> WhatsApp Pro </asp:ListItem>
                                                    <asp:ListItem  Value="10"> Pro Plus </asp:ListItem>
                                                    <asp:ListItem  Value="11"> Platinum </asp:ListItem>
                                                    <asp:ListItem  Value="12"> Wallet Recharge </asp:ListItem>
                                                     <asp:ListItem  Value="14"> Women Safety </asp:ListItem>
                                                   <asp:ListItem  Value="15"> Daily Banner </asp:ListItem>
                                                   <asp:ListItem  Value="16"> Platinum Add On </asp:ListItem>
                                                 
                                                </asp:DropDownList>  
                                  </div>
                           <div class="col-md-6">
                               <label class="col-sm-6 control-label" for="input-name2">Coupon Code: </label>
                               <asp:TextBox ID="txtCoupon" MaxLength="15" ValidateRequestMode="Enabled" CssClass="form-control" runat="server"></asp:TextBox>                                               
                           </div>
                        </div>
                 </div> <br />

                 <asp:Panel runat="server" ID="Panel1" Visible="false">  
                <div class="row">
                       <div class="col-md-12">
                              <div class="col-md-6">
                                  <label class="control-label" for="input-name2"> Select Recharge Period : </label>
                                    <asp:DropDownList ID="DropDownList2" CssClass="form-control" AutoPostBack="true"  runat="server" ValidateRequestMode="Enabled" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="1">One Year </asp:ListItem>
                                                     <asp:ListItem  Value="2">Life Time</asp:ListItem>
                                            </asp:DropDownList>  
                                  </div>
                                                                  <asp:Panel runat="server" ID="pnl1" Visible="false">  
                        
                             <div class="col-md-6">
                             <label class="control-label" for="input-name2"> Select Parent Link: </label>
                                  
                                                <asp:DropDownList ID="ddlfirst" CssClass="form-control" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                    <asp:ListItem  Value="1">0</asp:ListItem>
                                                     <asp:ListItem  Value="2">1</asp:ListItem>
                                                      <asp:ListItem  Value="3">2</asp:ListItem>

                                    </asp:DropDownList>  
                                 
                               </div>
                                 </asp:Panel>  
                             <asp:Panel runat="server" ID="pnl2" Visible="false">
                                  <div class="col-md-6">
                             <label class="control-label" for="input-name2"> Select Parent Link: </label>
                             
                 <asp:DropDownList ID="ddLife" CssClass="form-control" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="4">0</asp:ListItem>
                                                     <asp:ListItem  Value="5">1</asp:ListItem>
                                                      <asp:ListItem  Value="6">2</asp:ListItem>
                                        </asp:DropDownList>  
                               
                         
                        </div>
                                   </asp:Panel> 
                           <br />
                 </div> 
                    </div>
                      </asp:Panel>
                 <br />
                 <div class="row">
                       <div class="col-md-12">
                              <div class="col-md-6">
                                 <label class="col-sm-3 control-label" for="input-name2">Description </label>
                                             <asp:TextBox ID="txtDescription" CssClass="form-control" ValidateRequestMode="Enabled" runat="server"></asp:TextBox>
                             
                               </div>
                           <div class="col-md-6">
                                 <label class="col-sm-3 control-label" for="input-name2">Amount: </label>
                                            <asp:TextBox ID="txtAmount" CssClass="form-control" ValidateRequestMode="Enabled" runat="server"></asp:TextBox>
                             
                               </div>
                     </div> 
                        </div>
                
                <br />
                <div class="row">
    <div class="col-md-12">
        
    <div class="col-md-6">
     
             <label class="col-sm-6 control-label" for="input-name2"> Date From : </label>
                          <asp:TextBox ID="txtDateFrom" TextMode="Date" runat="server" CssClass="form-control" PlaceHolder="YYYY-MM-DD" ></asp:TextBox>
                                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDateFrom" ErrorMessage="*" ValidationGroup="B"><b style="color:red">*</b></asp:RequiredFieldValidator>
                                             </div>   <div class="col-md-6">              
    
                                                      <label class="col-sm-6 control-label" for="input-name2"> Date To:  </label>
                          <asp:TextBox ID="txtToDate" TextMode="Date" runat="server" CssClass="form-control" PlaceHolder="YYYY-MM-DD"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtToDate" ErrorMessage="*" ValidationGroup="B"><b style="color:red">*</b></asp:RequiredFieldValidator>
                                                        </div></div></div>
            
            </div> 
                    
                     <br />
        <div class="col-md-12">
                            
                               <div class="col-md-6">
                                 <label class="control-label" for="input-name2">Select Image: </label>
                                      <asp:FileUpload ID="Fileupload1" runat="server" />            
                                                                        
                               </div>
              <div class="col-md-6">
                  <label class="control-label" for="input-name2">Select waterMark option: </label>
<asp:RadioButtonList ID="RadioButtonList3" RepeatDirection="Horizontal" runat="server">
<asp:ListItem Value="0">&nbsp; Without WaterMark &nbsp;</asp:ListItem>
<asp:ListItem Value="1">&nbsp; with WaterMark &nbsp; </asp:ListItem>
</asp:RadioButtonList>
                        </div>
                        </div>
                 </div> 
            <br />
                                 <div class="row">
                   <div class="col-md-12"> <div class="col-md-6">
                       </div>
                       <div class="col-md-6">
                     <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click"/>
                    &nbsp;&nbsp;&nbsp;&nbsp   <asp:Button ID="btnUpdate" runat="server" Text="Update" class="btn btn-success" Visible="false" />
                    &nbsp;&nbsp;&nbsp;&nbsp   <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" OnClick="btnreset_Click"/>
                          <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                              </div>
                       </div>
                
                 <div class="row">
                <div style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                    <div >
                        <asp:Label ID="lblTotalCount" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp
                        <asp:Label ID="lblCuntDisplay" runat="server" Text="" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>
                        <br />
                    </div>
                 </div>
                     </div>
                      <div class="row">
                   <div class="col-md-12">
                     <div class="table-responsive">
            
                    <asp:GridView ID="gvAdvCourse" runat="server" OnPageIndexChanging="gvAdvCourse_PageIndexChanging" OnRowDataBound="gvAdvCourse_RowDataBound"
                          PagerStyle-VerticalAlign="Middle"  AutoGenerateColumns="False" Font-Names="Arial" Font-Size="11pt"  AllowPaging="True" ShowFooter="True  " 
                             PageSize="20" BackColor="White" BorderColor="#000000" BorderStyle="None" BorderWidth="1px" Width="100%"  Height="20px"  >
                        <Columns> 
                                <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PurchaseType" HeaderText="Purchase Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CouponCode" HeaderText="CouponCode">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                           
                                <asp:BoundField DataField="Amount" HeaderText="Amount">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                              <asp:BoundField DataField="Description" HeaderText="Description">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="FromDate" HeaderText="From Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ToDate" HeaderText="To Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="Active">
                                     <ItemTemplate>
                                          <asp:Button ID="lnkProfile" CssClass="btn btn-round btn-warning" runat="server" Text="Active" OnClick="lnkProfile_Click" ></asp:Button>
                                          <asp:Label ID="lblView" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                                     </ItemTemplate>
                                </asp:TemplateField> 
                        </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                </div>
                       <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                       </div>

                       </div>
                                     

             <%--       <div class="box-body row"> 
                                <div class="box-body col-md-12">
                                    <div class="SpcBwnBtnAndGv" align="center">
                                        <asp:Button ID="btnApproveAll" runat="server" Text="Approve" CssClass="btn btn-success" Width="100px" OnClick="btnApproveAll_Click" />
                                        <asp:Button ID="btnDisApproveAll" runat="server"  Text="DisApprove" CssClass="btn btn-success" Width="100px" OnClick="btnDisApproveAll_Click" />
                                         <asp:Button ID="btnPrint" runat="server" CssClass="btn btn-success" Text="Print Preview" Height="40px"  Width="120px" OnClick="btnPrint_Click"/>
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" Text="Save Image" Visible="false" Height="40px"  Width="120px" OnClick="Button1_Click"  UseSubmitBehavior="false" OnClientClick="return ConvertToImage(this)"/>      
                                        <asp:Label ID="lblId2" runat="server"  Visible="false"></asp:Label><%-- Text='<%#Eval("chkSelectAll") %>' --%>
                                  <%--  </div>
                                </div>
                            </div> --%>

                            <asp:Label ID="Label10" runat="server" Visible="false"></asp:Label>

                  <div id="dvTable">
           <asp:Literal ID="literal" runat="server"></asp:Literal>
        </div>
    <asp:HiddenField ID="hfImageData" runat="server" />

                </div>
                   </asp:Panel>
                   </div>


     <asp:Panel runat="server" ID="PanelFree" Visible="false"> 
               <div class="row">
            <div class="panel-body"> 
      
                 <div class="row">
                       <div class="col-md-12">
                              <div class="col-md-6">
                                  <label class="col-sm-5 control-label" for="input-name2"> Purchase Type: </label>
                                    <asp:DropDownList ID="DropDownList3" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="1">Course Purchase</asp:ListItem>
                                                     <asp:ListItem  Value="2">Open Schooling</asp:ListItem>
                                                     <asp:ListItem  Value="3">Virtual School</asp:ListItem>
                                                     <asp:ListItem  Value="4">Advance Training</asp:ListItem>
                                                    <asp:ListItem  Value="5">Child Safety</asp:ListItem>
                                                   <asp:ListItem  Value="6">Super Specility Training </asp:ListItem>
                                                   <asp:ListItem  Value="7"> Parent Webinar </asp:ListItem>
                                                   <asp:ListItem  Value="8"> WhatsApp Premium </asp:ListItem>
                                                    <asp:ListItem  Value="9"> WhatsApp Pro </asp:ListItem>
                                                    <asp:ListItem  Value="10"> Pro Plus </asp:ListItem>
                                                    <asp:ListItem  Value="11"> Platinum </asp:ListItem>
                                                    <asp:ListItem  Value="12"> Wallet Recharge </asp:ListItem>
                                                     <asp:ListItem  Value="14"> Women Safety </asp:ListItem>
                                                   <asp:ListItem  Value="15"> Daily Banner </asp:ListItem>
                                                  <asp:ListItem  Value="16"> Platinum Add On </asp:ListItem>
                                                </asp:DropDownList>  
                                  </div>
                           <div class="col-md-6">
                               <label class="col-sm-6 control-label" for="input-name2">Coupon Code: </label>
                               <asp:TextBox ID="TextBox1" MaxLength="15" ValidateRequestMode="Enabled" CssClass="form-control" runat="server"></asp:TextBox>                                               
                           </div>
                        </div>
                 </div> <br />

                 <asp:Panel runat="server" ID="Panel3" Visible="false">  
                <div class="row">
                       <div class="col-md-12">
                              <div class="col-md-6">
                                  <label class="control-label" for="input-name2"> Select Recharge Period : </label>
                                    <asp:DropDownList ID="DropDownList4" CssClass="form-control" AutoPostBack="true"  runat="server" ValidateRequestMode="Enabled" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="1">One Year </asp:ListItem>
                                                     <asp:ListItem  Value="2">Life Time</asp:ListItem>
                                            </asp:DropDownList>  
                                  </div>
                                                                  <asp:Panel runat="server" ID="Panel4" Visible="false">  
                        
                             <div class="col-md-6">
                             <label class="control-label" for="input-name2"> Select Parent Link: </label>
                                  
                                                                  <asp:DropDownList ID="DropDownList5" CssClass="form-control" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                    <asp:ListItem  Value="1">0</asp:ListItem>
                                                     <asp:ListItem  Value="2">1</asp:ListItem>
                                                      <asp:ListItem  Value="3">2</asp:ListItem>

                                    </asp:DropDownList>  
                                 
                               </div>
                                 </asp:Panel>  
                             <asp:Panel runat="server" ID="Panel5" Visible="false">
                                  <div class="col-md-6">
                             <label class="control-label" for="input-name2"> Select Parent Link: </label>
                             
                 <asp:DropDownList ID="DropDownList6" CssClass="form-control" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="4">0</asp:ListItem>
                                                     <asp:ListItem  Value="5">1</asp:ListItem>
                                                      <asp:ListItem  Value="6">2</asp:ListItem>
                                        </asp:DropDownList>  
                                                                              </div>
                                   </asp:Panel> 
                           <br />
                 </div> 
                    </div>
                      </asp:Panel>
                 <br />
                 <div class="row">
                       <div class="col-md-12">
                              <div class="col-md-4">
                                 <label class="col-sm-5 control-label" for="input-name2">Description </label>
                                             <asp:TextBox ID="TextBox2" CssClass="form-control" ValidateRequestMode="Enabled" runat="server"></asp:TextBox>
                                                            </div>
                          


                           <div class="col-md-4">
                                 <label class="col-sm-6 control-label" for="input-name2">Number of Code:</label>
                                            <asp:TextBox ID="TextBox3" CssClass="form-control" ValidateRequestMode="Enabled"  runat="server" minimumvalue="1"  ></asp:TextBox>
 
                               <asp:rangevalidator id="rangevalidator1" runat="server" controltovalidate="TextBox3"   
errormessage="enter value in specified range" forecolor="red" maximumvalue="5" minimumvalue="1"   
setfocusonerror="true" type=" integer"></asp:rangevalidator> 
                               
                               
                             
                               </div>

                            <div class="col-md-4">
                                  <label class="col-sm-8 control-label" for="input-name2"> Generate Free Code For: </label>
                                    <asp:DropDownList ID="DropDownList7" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" runat="server" ValidateRequestMode="Enabled" AppendDataBoundItems="True">
                                             <asp:ListItem Selected="True" Value="" Text="--Select--"></asp:ListItem>
                                                     <asp:ListItem  Value="1">Course Purchase</asp:ListItem>
                                                     <asp:ListItem  Value="2">Open Schooling</asp:ListItem>
                                                     <asp:ListItem  Value="3">Virtual School</asp:ListItem>
                                                     <asp:ListItem  Value="4">Advance Training</asp:ListItem>
                                                    <asp:ListItem  Value="5">Child Safety</asp:ListItem>
                                                   <asp:ListItem  Value="6">Super Specility Training </asp:ListItem>
                                                   <asp:ListItem  Value="7"> Parent Webinar </asp:ListItem>
                                                   <asp:ListItem  Value="8"> WhatsApp Premium </asp:ListItem>
                                                    <asp:ListItem  Value="9"> WhatsApp Pro </asp:ListItem>
                                                    <asp:ListItem  Value="10"> Pro Plus </asp:ListItem>
                                                    <asp:ListItem  Value="11"> Platinum </asp:ListItem>
                                                    <asp:ListItem  Value="12"> Wallet Recharge </asp:ListItem>
                                                     <asp:ListItem  Value="14"> Women Safety </asp:ListItem>
                                                   <asp:ListItem  Value="15"> Daily Banner </asp:ListItem>
                                                 
                                                </asp:DropDownList>  
                                  </div>
                            

                     </div> 
                     
                        </div>
                 <label class="col-sm- control-label" for="input-name2"> आपण तयार करत असलेला कोड खालील सिलेक्ट केलेल्या तारखे पर्यंत मर्यादित राहणार आहेत..    <br>त्यानंतर या कोड चा वापर करता येणार नाही </label>
                <br />
                <div class="row">
    <div class="col-md-12">
        
    <div class="col-md-6">
         
     
             <label class="col-sm-6 control-label" for="input-name2"> Date From : </label>
                          <asp:TextBox ID="TextBox4" TextMode="Date" runat="server" CssClass="form-control" PlaceHolder="YYYY-MM-DD" ></asp:TextBox>
                                                              <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDateFrom" ErrorMessage="*" ValidationGroup="B"><b style="color:red">*</b></asp:RequiredFieldValidator>
                                             </div>   
        <div class="col-md-6">              
    
               <label class="col-sm-6 control-label" for="input-name2"> Date To:  </label>
                      <asp:TextBox ID="TextBox5" TextMode="Date" runat="server" CssClass="form-control" PlaceHolder="YYYY-MM-DD"></asp:TextBox>
                                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtToDate" ErrorMessage="*" ValidationGroup="B"><b style="color:red">*</b></asp:RequiredFieldValidator>
                                                        </div></div></div>
            
            </div> 
                    
                     <br />
        <div class="col-md-12">
                            
                               <div class="col-md-6">
                                 <label class="control-label" for="input-name2">Select Image: </label>
                                      <asp:FileUpload ID="Fileupload2" runat="server" />            
                                                                        
                               </div>
              <div class="col-md-6">
                  <label class="control-label" for="input-name2">Select waterMark option: </label>
<asp:RadioButtonList ID="RadioButtonList2" RepeatDirection="Horizontal" runat="server">
<asp:ListItem Value="0">&nbsp; Without WaterMark &nbsp;</asp:ListItem>
<asp:ListItem Value="1">&nbsp; with WaterMark &nbsp; </asp:ListItem>
</asp:RadioButtonList>
                        </div>
                        </div>
                 </div> 
            <br />
                                 <div class="row">
                   <div class="col-md-12"> <div class="col-md-6">
                       </div>
                       <div class="col-md-6">
                     <asp:Button ID="Button1" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click"/>
                    &nbsp;&nbsp;&nbsp;&nbsp   <asp:Button ID="Button2" runat="server" Text="Update" class="btn btn-success" Visible="false" />
                    &nbsp;&nbsp;&nbsp;&nbsp   <asp:Button ID="Button3" runat="server" Text="Reset" class="btn btn-info" OnClick="btnreset_Click"/>
                          <asp:Label ID="Label2" Visible="false" runat="server"></asp:Label>  
                              </div>
                       </div>
                
                 <div class="row">
                <div style="margin-left: 450px; text-shadow: 2px 2px Pink;">
                    <div >
                        <asp:Label ID="Label3" runat="server" Text="Total Count:" Font-Bold="true" ForeColor="Blue" Visible="false"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp
                        <asp:Label ID="Label4" runat="server" Text="" Font-Size="37px" Font-Bold="true" ForeColor="Green"></asp:Label>
                        <br />
                    </div>
                 </div>
                     </div>
                      <div class="row">
                   <div class="col-md-12">
                     <div class="table-responsive">
            
                    <asp:GridView ID="GridView1" runat="server" OnPageIndexChanging="gvAdvCourse_PageIndexChanging" OnRowDataBound="gvAdvCourse_RowDataBound"
                          PagerStyle-VerticalAlign="Middle"  AutoGenerateColumns="False" Font-Names="Arial" Font-Size="11pt"  AllowPaging="True" ShowFooter="True  " 
                             PageSize="20" BackColor="White" BorderColor="#000000" BorderStyle="None" BorderWidth="1px" Width="100%"  Height="20px"  >
                        <Columns> 
                                <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="PurchaseType" HeaderText="Purchase Type">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="CouponCode" HeaderText="CouponCode">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                           
                                <asp:BoundField DataField="NumberOfCode" HeaderText="NumberOfCode">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                              <asp:BoundField DataField="Description" HeaderText="Description">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                              <asp:BoundField DataField="FreeCodeFor" HeaderText="FreeCodeFor">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="FromDate" HeaderText="From Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="ToDate" HeaderText="To Date">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                                </asp:BoundField>

                                <asp:TemplateField HeaderText="Active">
                                     <ItemTemplate>
                                          <asp:Button ID="lnkProfile" CssClass="btn btn-round btn-warning" runat="server" Text="Active" OnClick="lnkProfile_Click" ></asp:Button>
                                          <asp:Label ID="lblView" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> 
                                     </ItemTemplate>
                                </asp:TemplateField> 
                        </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                                   </asp:GridView>
                         
                </div>
                       <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                       </div>

                       </div>

             <%--       <div class="box-body row"> 
                                <div class="box-body col-md-12">
                                    <div class="SpcBwnBtnAndGv" align="center">
                                        <asp:Button ID="btnApproveAll" runat="server" Text="Approve" CssClass="btn btn-success" Width="100px" OnClick="btnApproveAll_Click" />
                                        <asp:Button ID="btnDisApproveAll" runat="server"  Text="DisApprove" CssClass="btn btn-success" Width="100px" OnClick="btnDisApproveAll_Click" />
                                         <asp:Button ID="btnPrint" runat="server" CssClass="btn btn-success" Text="Print Preview" Height="40px"  Width="120px" OnClick="btnPrint_Click"/>
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-success" Text="Save Image" Visible="false" Height="40px"  Width="120px" OnClick="Button1_Click"  UseSubmitBehavior="false" OnClientClick="return ConvertToImage(this)"/>      
                                        <asp:Label ID="lblId2" runat="server"  Visible="false"></asp:Label><%-- Text='<%#Eval("chkSelectAll") %>' --%>
                                  <%--  </div>
                                </div>
                            </div> --%>

                            <asp:Label ID="Label7" runat="server" Visible="false"></asp:Label>

                  <div id="dvTable">
           <asp:Literal ID="literal2" runat="server"></asp:Literal>
        </div>
    <asp:HiddenField ID="HiddenField2" runat="server" />

                </div>
         </asp:Panel>
     <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>

    <script type="text/javascript">
        function ConvertToImage(Button1) {
            html2canvas($("#dvTable")[0]).then(function (canvas) {
                var base64 = canvas.toDataURL();
                $("[id*=hfImageData]").val(base64);
                __doPostBack(Button1.name, "");
            });
            return false;
        }
    </script>
</asp:Content>

