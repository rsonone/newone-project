﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using BAL;
using DAL;

namespace NeedlyApp.MasterPage
{
    public partial class NeedlyMaster : System.Web.UI.MasterPage
    {
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Page.MaintainScrollPositionOnPostBack = true;

                lblUsername.Text = " Welcome  " + Convert.ToString(Session["username"]);
                string a = Convert.ToString(Session["CompanyName"]);

                if ((Convert.ToString(Session["username"]) == null || Convert.ToString(Session["username"]) == "" && Convert.ToString(Session["username"]) != "Administator"))
                {
                    Response.Redirect("../LoginPage.aspx");

                }
                if (!IsPostBack)
                {
                    getAllMainMenu();
                    spreadSheetAccess();
                    getpageaccess();
                    if (Session["username"].ToString() == "7767008611" || Session["username"].ToString() == "7767008614" || Session["username"].ToString() == "9552202776")
                    {
                        pnlmain.Visible = true;
                        pnlUrl.Visible = true;
                    }
                  
                }
              
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
            }
        }
        private void getAllMainMenu()
        {
            try
            {
                
                if(Session["username"].ToString() ==  "9422325020")
                {
                    pnlmain.Visible = true;
                    pnlUrl.Visible = false;
                    li9.Visible = true;
                    li10.Visible = true;
                    liExcelContacts.Visible = true;
                    li11.Visible = true;
                    liRegistrationNotification.Visible = true;
                    liurlaccess.Visible = true;
                    liSendRecords.Visible = true;
                    pnlMysqlReport.Visible = true;
                    liSpreadSheetAccess.Visible = true;
                    liSpreadSheetReport.Visible = true;
                    liTrueVoter.Visible = true;
                    liSendNotification.Visible = true;
                }
                if (Session["username"].ToString() == "9422325020" || Session["username"].ToString() == "9403724039")
                {
                   liVCReport.Visible = true;
                }
                pnlmain.Visible = true;
                liHome.Visible = true;
                liChange.Visible = true;
                liBanner.Visible = true;
                li1Reg.Visible = true;
                liLogin.Visible = true;
               // liSendNotification.Visible = true;
                if (Session["username"].ToString() == "9168047527")
                {

                    pnlmain.Visible = false;
                    pnlMysqlReport.Visible = false;
                    pnlUrl.Visible = false;
                    pnlPromotionalAndOffers.Visible = true;
                }
            }
            catch (Exception ex)
            {
                EL.SendErrorToText(ex);
            }
        }

        private void getpageaccess()
        {
            string Query = "select  Id  from  [tbl_UrlAccessDetails] where  MobileNumber = " + Session["username"].ToString() + " and PageAccess = 1 order by Id desc ";
            string Res = cc.ExecuteScalar(Query);
            if(Res != "")
            {
                int res1 = Convert.ToInt32(Res);
                if (res1 > 0)
                {
                    pnlmain.Visible = false;
                    pnlUrl.Visible = true;
                    li24.Visible = true;
                    string QueryNew = "select  Id  from  [tbl_SpreadSheetAccess] where  MobileNo = " + Session["username"].ToString() + " ";
                    string ResNew = cc.ExecuteScalar(QueryNew);
                    int res1New = Convert.ToInt32(ResNew);
                    if (res1New > 0)
                    {
                        li25.Visible = true;
                    }
                    pnlmain.Visible = false;
                }
                else
                {
                    pnlmain.Visible = true;
                    pnlUrl.Visible = false;
                }
            }
            

        }

        private void spreadSheetAccess()
        {
            string QueryNew = "select  Id  from  [tbl_SpreadSheetAccess] where  MobileNo = " + Session["username"].ToString() + " ";
            string ResNew = cc.ExecuteScalar(QueryNew);
            if(ResNew == "")
            {
                pnlUrl.Visible = false;
                li25.Visible = false;
                li24.Visible = false;
            }
            if(ResNew != "")
            {
                int res1New = Convert.ToInt32(ResNew);
            
            
            if (res1New > 0)
            {
                pnlUrl.Visible = true;
                li25.Visible = true;
                li24.Visible = false;
            }
                pnlmain.Visible = false;
            }
        }
    }
}