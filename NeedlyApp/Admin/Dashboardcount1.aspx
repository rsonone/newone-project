﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="Dashboardcount1.aspx.cs" Inherits="NeedlyApp.Admin.Dashboardcount1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Dashboard Count <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                      </div>
                    </div>
                  </div>
                <div class="row">
            <div class="panel-body">
                  <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Mobile No: </label>
                               <div class="col-sm-9">
                                    <asp:TextBox ID="txtmobileno"  CssClass="form-control"  PlaceHolder="Enter Mobile Number" runat="server"></asp:TextBox>
                                                        
                                                                 </div></div></div>
                                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <asp:Button ID="btnsearch" runat="server"  class="btn btn-primary" OnClick="btnsearch_Click"  Text="Search Count"  />
                             
                                    </div>
               </div></div>
                <br />
     <div class="content-wrapper">
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>
                <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lbldemandjobcount" runat="server"></asp:Label></h3>

              <p>Demand Job Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblchildcount" runat="server"></asp:Label></h3>

              <p>Child Added in Child Safety Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><asp:Label ID="lblneedlygrpcount" runat="server"></asp:Label></h3>

              <p>Needly Group Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3><asp:Label ID="lbldailybannercount" runat="server"></asp:Label></h3>

              <p>Daily Banner Used Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </section>
              </div>
    <br />
               <div class="content-wrapper">
          <section class="content">
              <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
              <h3> <asp:Label ID="lblprofileurlname" runat="server"></asp:Label></h3>

              <p>Website Created or Not</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lblappusedate" runat="server"></asp:Label></h3>

              <p>Last App Used Date</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblappversion" runat="server"></asp:Label></h3>

              <p>App Version</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3><asp:Label ID="lblappinstallationdate" runat="server"></asp:Label></h3>

              <p>App Installation Date</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
              </section>
              </div>
    <br />
                <div class="content-wrapper">
          <section class="content">
              <%-- <div class="box box-default">
               <div class="box-body col-sm-12">--%>
                <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3> <asp:Label ID="lblpostjobcount" runat="server"></asp:Label></h3>

              <p>Post Job Count</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-apps"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
                     <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><asp:Label ID="lbluserrole" runat="server"></asp:Label></h3>

              <p>User Role</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    
</asp:Content>
