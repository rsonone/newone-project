﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;
using DAL;
using System.Web.Configuration;

namespace NeedlyApp.Admin
{
    public partial class RechargeWalletScheme : System.Web.UI.Page
    {


        SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString);

        CommonCode cc = new CommonCode();
        string base64String = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            FillGrid();
            FillGrid1();
        }
        public void FillGrid()
        {


            SqlCommand cmd = new SqlCommand("sp_GetRechargeWalletCoupenCOde");
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter AD = new SqlDataAdapter();
            AD.SelectCommand = cmd;
            DataSet ds = new DataSet();
            AD.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAdvCourse.DataSource = ds.Tables[0];

                gvAdvCourse.DataBind();

                lblTotalCount.Visible = true;
                lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
            }
        }

        public void FillGrid1()
        {
            SqlCommand cmd = new SqlCommand("sp_GetRechargeWalletCoupenCOde1");
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter AD = new SqlDataAdapter();
            AD.SelectCommand = cmd;
            DataSet ds = new DataSet();
            AD.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {

                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
                lblTotalCount.Visible = true;
                lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
            }
        }
        protected void RadioButtonList1_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedValue == "0")
            {
                PanelDiscount.Visible = true;
                PanelFree.Visible = false;
            }
            if (RadioButtonList1.SelectedValue == "1")
            {
                PanelFree.Visible = true;
                PanelDiscount.Visible = false;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string SubType = string.Empty;
            if (Fileupload1.HasFile)
            {
                BinaryReader br = new BinaryReader(Fileupload1.PostedFile.InputStream);
                byte[] bytes = br.ReadBytes((int)Fileupload1.PostedFile.InputStream.Length);
                //Convert the Byte Array to Base64 Encoded string.
                base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            if (DropDownList2.SelectedValue == "1")
            {
                SubType = ddlfirst.SelectedValue;
            }
            else
            {
                SubType = ddLife.SelectedValue;
            }
            try
            {

                if (RadioButtonList1.SelectedValue == "0")
                {


                    SqlCommand cmd = new SqlCommand("SP_UploadInsertRechargeWalletCoupenCode1", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.AddWithValue("@Type", DropDownList1.SelectedValue);
                    cmd.Parameters.AddWithValue("@Coupon", txtCoupon.Text);
                    cmd.Parameters.AddWithValue("@Description", txtDescription.Text);
                    cmd.Parameters.AddWithValue("@Amount", txtAmount.Text);
                    cmd.Parameters.AddWithValue("@DateFrom", txtDateFrom.Text);
                    cmd.Parameters.AddWithValue("@ToDate", txtToDate.Text);
                    cmd.Parameters.AddWithValue("@WaterMarkType", RadioButtonList2.SelectedValue);
                    cmd.Parameters.AddWithValue("@Image", base64String);
                    cmd.Parameters.AddWithValue("@SubType", SubType);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["LoginId"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    con.Close();
                    con.Close();


                    if (A == -1)
                    {
                        FillGrid();
                        ///ClearData();
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                    }
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("SP_UploadInsertRechargeWalletCoupenCode11", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();

                    cmd.Parameters.AddWithValue("@Type", DropDownList3.SelectedValue);
                    cmd.Parameters.AddWithValue("@Coupon", TextBox1.Text);
                    cmd.Parameters.AddWithValue("@Description", TextBox2.Text);
                    cmd.Parameters.AddWithValue("@NumberOfCode", TextBox3.Text);
                    cmd.Parameters.AddWithValue("@FreeCodeFor", DropDownList7.SelectedValue);
                    cmd.Parameters.AddWithValue("@DateFrom", TextBox4.Text);
                    cmd.Parameters.AddWithValue("@ToDate", TextBox5.Text);
                    cmd.Parameters.AddWithValue("@WaterMarkType", RadioButtonList2.SelectedValue);
                    cmd.Parameters.AddWithValue("@Image", base64String);
                    cmd.Parameters.AddWithValue("@SubType", SubType);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["LoginId"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    con.Close();
                    con.Close();

                    if (A == -1)
                    {
                        FillGrid1();
                        ///ClearData();
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                    }

                }
            }

            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
                ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);


            }

        }


        protected void btnreset_Click(object sender, EventArgs e)
        {

        }

        protected void gvAdvCourse_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAdvCourse.PageIndex = e.NewPageIndex;
            FillGrid();
            FillGrid1();
        }

        protected void gvAdvCourse_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void lnkProfile_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            Label1.Text = (btn.FindControl("lblView") as Label).Text;

            string Id = Label1.Text;

            string Query = "update [DBeZeeOnlineExam].[dbo].tbl_RechargeWalletCoupenCode  set ActiveStatus = '1' where Id = '" + Id + "'";
            int Res = cc.ExecuteNonQuery(Query);

            FillGrid();
            FillGrid1();

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue == "5")
            {
                Panel1.Visible = true;
            }
            else
            {
                Panel1.Visible = false;
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList2.SelectedValue == "1")
            {
                pnl1.Visible = true;
                pnl2.Visible = false;
            }
            else
            {
                pnl1.Visible = false;
                pnl2.Visible = true;
            }
        }

    }
}