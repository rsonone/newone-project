﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="SpreadsheetReport.aspx.cs" Inherits="NeedlyApp.Admin.SpreadsheetReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i> Needly Spreadsheet Report <small></small></h2>
                       <div class="container-fluid">
                       
        </div>
                    <div class="clearfix"></div>
                  </div> 
                    
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-5">
                     <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                   Form Id </label>
                                <div class="col-sm-9">
                                    <asp:DropDownList ID="ddlFormId" CssClass="form-control" runat="server">
                                        <asp:ListItem>-- Select --</asp:ListItem>
                                    </asp:DropDownList> 
                          </div>

                           <asp:Button ID="btnSearchByFormId" OnClick="btnSearchByFormId_Click"  runat="server"  Text="Search" class="btn btn-success" />
                              
                     </div>     

                </div>
                    

               
                    
                    </div>
               </div></div>
                 <asp:Panel ID="Panel1" Visible="false" runat="server">
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-5">
                     <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                   Enter PRO Number </label>
                                <div class="col-sm-9">
                                 <asp:TextBox ID="txtPRONumber" runat ="server" CssClass="form-control"></asp:TextBox>
                                  
                          </div></div>     

                </div>
                     <div class="box-body col-sm-5">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Enter Data Receiver Number: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtDistrict" runat ="server" CssClass="form-control"></asp:TextBox>
                                  
                          </div></div></div>

               
                    <div class="box-body col-sm-2">
                              <asp:Button ID="btnSearch"  runat="server" OnClick="btnSearch_Click" Text="Search" class="btn btn-success"  />
                              
                        </div> 
                    </div>
               </div></div>
               
                  
             <div class="row">
            <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-5">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    District: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtDis" runat ="server" CssClass="form-control"></asp:TextBox>
                                 
                          </div></div></div>

                <div class="box-body col-sm-5">
                          <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Taluka: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtTaluka" runat ="server" CssClass="form-control"></asp:TextBox>
                                 
                          </div>

                          </div>

                </div>
                     <div class="box-body col-sm-2">
                              <asp:Button ID="btnSearchByDistrict" OnClick="btnSearchByDistrict_Click"  runat="server"  Text="Search" class="btn btn-success" />
                              
                        </div>
                    </div>
               </div></div> 
                         </asp:Panel>
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-4">
                         <div class="form-group">
                                
                         </div>

                     </div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                             <asp:Button ID="btnExportMemberAndPro" OnClick="btnExportMemberAndPro_Click" Visible="false" runat="server" class="btn btn-success" Text="Export Member and PRO Number Data" />
                             <asp:Button ID="btnExportMember" OnClick="btnExportMember_Click"  Visible="false" runat="server" class="btn btn-success" Text="Export Member Data" />
                             <asp:Button ID="btnExportPRO" OnClick="btnExportPRO_Click"  Visible="false" runat="server" class="btn btn-success" Text="Export PRO Data" />

                             <asp:Button ID="ExportDisAndTaluka" OnClick="ExportDisAndTaluka_Click"  Visible="false" runat="server" class="btn btn-success" Text="Export Dis And Taluka data" />
                             <asp:Button ID="btnexportDistict" OnClick="btnexportDistict_Click"  Visible="false" runat="server" class="btn btn-success" Text="Export District Data" /> 
                             <asp:Button ID="btnExportTaluka" OnClick="btnExportTaluka_Click"  Visible="false" runat="server" class="btn btn-success" Text="Export Taluka Data" />
                            
                             </div></div>

                    </div>
               </div></div>

                    <asp:Panel ID="pnlCount" Visible="false" runat="server">
                   <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total Customers Added </label>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblCustCount" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Access Permitted customers: </label>
                                <div class="col-sm-3">
                                  <asp:Label ID="lblAccessCount" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                                 
                          </div></div>     

                </div>

                    </div>
               </div></div> 
                      <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total Active Customers </label>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblActiveCust" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                          </div></div></div>

                <div class="box-body col-sm-6">
                     <div class="form-group">
                                <label class="col-sm-9 control-label" for="input-name2">
                                    Total DeActive customers: </label>
                                <div class="col-sm-3">
                                  <asp:Label ID="lblDeActive" runat="server" Font-Bold="true" Font-Size="Large" Text=""></asp:Label>
                                 
                          </div></div>     

                </div>

                    </div>
               </div></div> 
                        </asp:Panel>

                     <div class="row">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="box-body col-sm-10">
                                        <div class="form-group">
                                            <asp:Label ID="lblExcelCountMsg" Font-Bold="true" Font-Size="Larger" ForeColor="Black" runat="server" Visible="true" Text="Total Records :"></asp:Label>

                                            <asp:Label ID="lblcntrecords" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblNoData" Font-Bold="true" Font-Size="Larger" ForeColor="Green" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                             <br />

                   
                     <div class="col-md-12">
                        
                           <div class="table-responsive">
                    <asp:GridView ID="GVSpreadsheetReport" CssClass="table table-hover table-bordered" runat="server"  OnRowDataBound="GVSpreadsheetReport_RowDataBound" OnPageIndexChanging="GVSpreadsheetReport_PageIndexChanging"
                          PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial"  DataKeyNames="Id"
                        OnRowEditing="GVSpreadsheetReport_RowEditing" OnRowCancelingEdit="GVSpreadsheetReport_RowCancelingEdit" OnRowUpdating="GVSpreadsheetReport_RowUpdating">
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                         <asp:BoundField DataField="FormId" HeaderText="Form Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_1" HeaderText="District">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_2" HeaderText="Taluka">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_3" HeaderText="Electrol Division No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_4" HeaderText="Electrol College No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_5" HeaderText="Booth No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_6" HeaderText="Total Voters">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_7" HeaderText="7:30 - 9:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_8" HeaderText="7:30 - 11:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_9" HeaderText="7:30 - 1:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_10" HeaderText="7:30 - 3:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_11" HeaderText="7:30 - 5:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="MemberNo" HeaderText="Member No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>   
                         <asp:BoundField DataField="AdminNo" HeaderText="Admin No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Form" HeaderText="PRO Number">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                       <asp:TemplateField>  
                    <ItemTemplate>  
                        <asp:Button ID="btn_Edit" runat="server" Text="Edit" CommandName="Edit" />  
                    </ItemTemplate>  
                    <EditItemTemplate>  
                        <asp:Button ID="btn_Update" runat="server" Text="Update" CommandName="Update"/>  
                        <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CommandName="Cancel"/> 
                        <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                    </EditItemTemplate>  
                </asp:TemplateField>
                       
     
      
                       
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>

                                 <asp:GridView ID="GvSpreadsheet2" Visible="false" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GvSpreadsheet2_PageIndexChanging"
                         PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                       <%-- <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>--%>  
               
                         <asp:BoundField DataField="Id" HeaderText="Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                       
                         <asp:BoundField DataField="FormId" HeaderText="Form Id">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_1" HeaderText="District">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_2" HeaderText="Taluka">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_3" HeaderText="Electrol Division No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_4" HeaderText="Electrol College No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_5" HeaderText="Booth No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_6" HeaderText="Total Voters">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_7" HeaderText="7:30 - 9:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_8" HeaderText="7:30 - 11:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_9" HeaderText="7:30 - 1:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_10" HeaderText="7:30 - 3:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="ANSWER_11" HeaderText="7:30 - 5:30">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="MemberNo" HeaderText="Member No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>   
                         <asp:BoundField DataField="AdminNo" HeaderText="Admin No">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="Form" HeaderText="PRO Number">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                        
                      
      
     
      
                       
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
                    <asp:Label ID="Label1" runat="server" ForeColor="White" Text=""></asp:Label>

                  </div>
                </div></div>
</asp:Content>
