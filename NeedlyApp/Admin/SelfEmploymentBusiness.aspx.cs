﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Collections.Generic;

namespace NeedlyApp.Admin
{
    public partial class Self_EmploymentBusiness : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        DataSet ds;
        DataTable dt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView1();
            }
        }

        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_DownloadSelfEmploymentBusinessdata");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SubmittedBy", Session["username"].ToString());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddQues.DataSource = ds;
                        GVAddQues.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void GVAddQues_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddQues.PageIndex = e.NewPageIndex;
            GridView1();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    Button btn = sender as Button;
                    GridViewRow gv = btn.NamingContainer as GridViewRow;
                    lblId.Text = (btn.FindControl("lblId1") as Label).Text;
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = DataBaseConnection;
                    DataBaseConnection.Open();

                    command4.CommandText = "sp_EditSelfEmploymentBusiness";
                    command4.CommandType = System.Data.CommandType.StoredProcedure;
                    command4.Parameters.AddWithValue("@Id", lblId.Text);

                    SqlDataAdapter da4 = new SqlDataAdapter();
                    da4.SelectCommand = command4;
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        txtbusinessname.Text = ds4.Tables[0].Rows[0][0].ToString();
                        rbtnbusinesstype.SelectedValue = ds4.Tables[0].Rows[0][1].ToString();
                        txtdescription.Text = ds4.Tables[0].Rows[0][2].ToString();
                        txtyear.Text = ds4.Tables[0].Rows[0][3].ToString();
                        txtfund.Text = ds4.Tables[0].Rows[0][4].ToString();
                        rdbCom.SelectedValue = ds4.Tables[0].Rows[0][5].ToString();
                        txtlandarea.Text = ds4.Tables[0].Rows[0][6].ToString();
                        txtexpectedamtoffund.Text = ds4.Tables[0].Rows[0][7].ToString();
                        txtmarketing.Text = ds4.Tables[0].Rows[0][8].ToString();
                        txtanualincome.Text = ds4.Tables[0].Rows[0][9].ToString();
                        txtskills.Text = ds4.Tables[0].Rows[0][10].ToString();
                        txtmobilenumber.Text = ds4.Tables[0].Rows[0][11].ToString();

                    }
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
           
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    

                    SqlCommand cmd = new SqlCommand("SP_UpdateSelfEmploymentBusiness");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Id", lblId.Text);
                    cmd.Parameters.AddWithValue("@NameofBusiness", txtbusinessname.Text);
                    cmd.Parameters.AddWithValue("@BusinessType", rbtnbusinesstype.SelectedValue);
                    cmd.Parameters.AddWithValue("@BusinessDescription", txtdescription.Text);
                    cmd.Parameters.AddWithValue("@EstablishmentYear", txtyear.Text);
                    cmd.Parameters.AddWithValue("@OwnFundInvestmentDonetillnow", txtfund.Text);
                    cmd.Parameters.AddWithValue("@Ownedland", rdbCom.SelectedValue);
                    cmd.Parameters.AddWithValue("@LandArea", txtlandarea.Text);
                    cmd.Parameters.AddWithValue("@ExpectedamountofFundRequiredforExpansion", txtexpectedamtoffund.Text);
                    cmd.Parameters.AddWithValue("@Whataboutmarketingfacilitiesavailable", txtmarketing.Text);
                    cmd.Parameters.AddWithValue("@ExpectedExistingannualIncomefromthisBusiness", txtanualincome.Text);
                    cmd.Parameters.AddWithValue("@Doyouhaveanyspecialskills", txtskills.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtmobilenumber.Text);





                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView1();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_InsertSelfEmploymentBusinessData");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@NameofBusiness", txtbusinessname.Text);
                    cmd.Parameters.AddWithValue("@BusinessType", rbtnbusinesstype.SelectedValue);
                    cmd.Parameters.AddWithValue("@BusinessDescription", txtdescription.Text);
                    cmd.Parameters.AddWithValue("@EstablishmentYear", txtyear.Text);
                    cmd.Parameters.AddWithValue("@OwnFundInvestmentDonetillnow", txtfund.Text);
                    cmd.Parameters.AddWithValue("@Ownedland", rdbCom.SelectedValue);
                    cmd.Parameters.AddWithValue("@LandArea", txtlandarea.Text);
                    cmd.Parameters.AddWithValue("@ExpectedamountofFundRequiredforExpansion", txtexpectedamtoffund.Text);
                    cmd.Parameters.AddWithValue("@Whataboutmarketingfacilitiesavailable", txtmarketing.Text);
                    cmd.Parameters.AddWithValue("@ExpectedExistingannualIncomefromthisBusiness", txtanualincome.Text);
                    cmd.Parameters.AddWithValue("@Doyouhaveanyspecialskills", txtskills.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", txtmobilenumber.Text);
                    cmd.Parameters.AddWithValue("@Modifyby", Session["username"].ToString());
                    cmd.Parameters.AddWithValue("@SubmittedBy", Session["username"].ToString());




                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView1();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
    }
}