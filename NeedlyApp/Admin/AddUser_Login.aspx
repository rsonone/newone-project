﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/NeedlyMaster.Master" CodeBehind="AddUser_Login.aspx.cs" Inherits="NeedlyApp.Admin.AddUser_Login" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Add User Login <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    State: </label>
                                <div class="col-sm-9">
                                   <asp:DropDownList ID="ddlstate" CssClass="form-control"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" >
                                 <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                         
                                    </asp:DropDownList><br />
                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 District: </label>
                              <div class="col-sm-9">
                                 <asp:DropDownList ID="ddldistrict"  CssClass="form-control" AutoPostBack="true"  runat="server" >
                               <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                       
                                  </asp:DropDownList>
                           </div></div></div>

                    </div>
               </div></div> 

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Name: </label>
                                <div class="col-sm-9">
                                  <asp:TextBox ID="txtName" runat ="server" AutoPostBack="true" PlaceHolder="Enter Your Name" CssClass="form-control"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="requiredforquotationid" runat="server" ForeColor="Red" ControlToValidate="txtName" ErrorMessage="Please Enter your Name" ValidationGroup="B" ></asp:RequiredFieldValidator>

                          </div></div></div>

                <div class="box-body col-sm-6">
                         <div class="form-group">
                              <label class="col-sm-3 control-label" for="input-name2">
                                 Mobile No: </label>
                              <div class="col-sm-9">
                                <asp:TextBox ID="txtMob" runat ="server" AutoPostBack="true" CssClass="form-control" PlaceHolder="Enter Your Number"  OnTextChanged="txtMob_OnTextChanged"></asp:TextBox>
                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="txtMob" ErrorMessage="Please Enter your Number" ValidationGroup="B" ></asp:RequiredFieldValidator>

                           </div></div></div>

                    </div>
               </div></div> 

                   
                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-success" OnClick="btnSubmit_Click" ValidationGroup="B"  />
                             
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                    
                     <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVUserLogin" CssClass="table table-hover table-bordered" runat="server" OnPageIndexChanging="GVUserLogin_PageIndexChanging"
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" >
                    <Columns>   
                        <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>     
                        <asp:BoundField DataField="UserName" HeaderText="User Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                        </asp:BoundField>
                         <asp:BoundField DataField="MobileNo" HeaderText="MobileNo">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        <asp:BoundField DataField="Stae" HeaderText="State">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="District" HeaderText="District">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:TemplateField HeaderText="Password"  >
                             <ItemTemplate>
                                 <asp:Button ID="btnPswd" CssClass="btn btn-round btn-success" runat="server" CommandArgument='<%#Bind("MobileNo")%>'
                                      OnClick="btnPswd_Click" Text="Send"></asp:Button>
                                   
                             </ItemTemplate>
                         </asp:TemplateField>
                        
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div></div>
           

                  </div>
                </div></div>
    </asp:Content>
