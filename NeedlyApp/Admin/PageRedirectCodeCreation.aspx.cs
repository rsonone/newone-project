﻿
using BAL;
using DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class PageRedirectCodeCreation : System.Web.UI.Page
    {

        string dirPath = string.Empty;
        string fullName = string.Empty;
        string base64String = string.Empty;
        CommonCode cc = new CommonCode();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                GridView1();
            }
        }
        public void GridView1()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_Downloadpageredirectcodecreationnew");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                   

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddCat.DataSource = ds;
                        GVAddCat.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void GridViewsearchbyname()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_Downloadpageredirectcodecreation");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (ddlprojectname.SelectedValue == "1")
                    {
                        cmd.Parameters.AddWithValue("@projectname", ddlprojectname.SelectedItem.Text);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@projectname", ddlprojectname.SelectedItem.Text);
                    }

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddCat.DataSource = ds;
                        GVAddCat.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void GVAddCat_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddCat.PageIndex = e.NewPageIndex;
            GridView1();
        }

        protected void ddlprojectname_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlprojectname.SelectedValue=="2")
            {
                ddltopictruevoter.Visible = true;
                ddltopic.Visible = false;
            }
            else
            {
                ddltopic.Visible = true;
                ddltopictruevoter.Visible = false;
            }
            GridViewsearchbyname();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string ImageNew = string.Empty;
            string imageUrl1 = string.Empty;
            WebRequest tRequest;
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_Uploadpageredirectcodecreation");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();
                    if (ddlprojectname.SelectedValue == "1")
                    {
                        cmd.Parameters.AddWithValue("@Topic", ddltopic.SelectedItem.Text);


                        if ((FileUpload1.HasFile))
                        {

                            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);

                            FileUpload1.SaveAs(Server.MapPath("~/img/" + filename));
                            string filepath = "img/" + filename;

                            ImageNew = "https://needly.in/" + filepath.ToString();

                        }

                       
                        string Query = "Select top(1)Id From [DBNeedly].[dbo].[tbl_pageredirectcodecreation] where Topic='" + ddltopic.SelectedItem.Text + "' and projectname='"+ddlprojectname.SelectedItem.Text+"' order by Id desc";
                        string Result = cc.ExecuteScalar(Query);
                       
                        string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                        ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                        string SENDER_ID = "801244826424";
                        string str;

                        //string deviceId = DeviRegId;
                        tRequest = WebRequest.Create("https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyAUmCRMb3ea1Nvobpeh0RKwp6PSv2_x2Fs");
                        tRequest.Method = "post";
                        tRequest.ContentType = "application/json";
                        if (ddltopic.SelectedItem.Text == "Autocall")
                        {
                            txttopic.Text = "Auca";
                        }
                        if (ddltopic.SelectedItem.Text == "Automessage")
                        {
                            txttopic.Text = "Amsg";
                        }
                        if (ddltopic.SelectedItem.Text == "Appointment")
                        {
                            txttopic.Text = "Appt";
                        }
                        if (ddltopic.SelectedItem.Text == "Message Compaign")
                        {
                            txttopic.Text = "Msgc";
                        }
                        if (ddltopic.SelectedItem.Text == "Schedular")
                        {
                            txttopic.Text = "Sdul";
                        }
                        if (ddltopic.SelectedItem.Text == "BPO Solution")
                        {
                            txttopic.Text = "BPOS";
                        }
                        string Url2 = String.Concat(txttopic.Text, Result);
                        string ShortURL = Url2;

                        string JsonData = @"{'dynamicLinkInfo':{'domainUriPrefix':'https:\/\/needly.page.link','link':'https:\/\/needly.in\" + ShortURL + "','androidInfo':{'androidPackageName':'ezee.abhinav.needly'}},'suffix':{'option':'SHORT'}}";

                        Byte[] byteArray = Encoding.UTF8.GetBytes(JsonData);
                        tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                        tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                        tRequest.ContentLength = byteArray.Length;
                        #region add
                        tRequest.UseDefaultCredentials = true;
                        tRequest.PreAuthenticate = true;
                        tRequest.Credentials = CredentialCache.DefaultCredentials;
                        #endregion
                        using (var streamWriter = new StreamWriter(tRequest.GetRequestStream()))
                        {
                            string input = JsonData;


                            streamWriter.Write(input);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }

                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@Topic", ddltopictruevoter.SelectedItem.Text);
                        if ((FileUpload1.HasFile))
                        {

                            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);

                            FileUpload1.SaveAs(Server.MapPath("~/img/" + filename));
                            string filepath = "img/" + filename;

                            ImageNew = "https://needly.in/" + filepath.ToString();

                        }

                       
                        string Query = "Select top(1)Id From [DBNeedly].[dbo].[tbl_pageredirectcodecreation] where Topic='" + ddltopictruevoter.SelectedItem.Text + "' and projectname='"+ddlprojectname.SelectedItem.Text+"' order by Id desc";
                        string Result = cc.ExecuteScalar(Query);
                       
                        string SERVER_API_KEY = "AAAAuo3pyzg:APA91bH3ReVwL8anOczulumjER4_WS4aAGYCjqbcDAkpokYvWgti_4sz2c22zm3S2c3nfNU3ZyjY8R5_SKnI3uauZayGTQVsV0O8TrUPzWQfnzZ5dN6SrsbuAjM4dg9LgJO5ZmrPjD87";
                        ///   AIzaSyCyW3elUsUFn7OVgObm27mcsAufukzyCKY
                        string SENDER_ID = "801244826424";
                        string str;

                        //string deviceId = DeviRegId;
                        tRequest = WebRequest.Create("https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyBYClMu3wjmXl0CaHnmC3lhvcuh2VmiIm8");
                        tRequest.Method = "post";
                        tRequest.ContentType = "application/json";
                        if (ddltopictruevoter.SelectedItem.Text == "Voter List Search")
                        {
                            txttopic.Text = "VLSE";
                        }
                        if (ddltopictruevoter.SelectedItem.Text == "Candidate Expenses")
                        {
                            txttopic.Text = "CAEX";
                        }
                        if (ddltopictruevoter.SelectedItem.Text == "Advance Facility")
                        {
                            txttopic.Text = "ADFA";
                        }
                      
                        string Url2 = String.Concat(txttopic.Text, Result);
                        string ShortURL = Url2;

                        string JsonData = @"{'dynamicLinkInfo':{'domainUriPrefix':'https:\/\/truevoter.page.link','link':'https:\/\/truevoter.page.link\" + ShortURL + "','androidInfo':{'androidPackageName':'ezee.abhinav.needly'}},'suffix':{'option':'SHORT'}}";
                   
                        Byte[] byteArray = Encoding.UTF8.GetBytes(JsonData);
                       // tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                       // tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                        tRequest.ContentLength = byteArray.Length;
                        #region add
                        tRequest.UseDefaultCredentials = true;
                        tRequest.PreAuthenticate = true;
                        tRequest.Credentials = CredentialCache.DefaultCredentials;
                        #endregion
                        using (var streamWriter = new StreamWriter(tRequest.GetRequestStream()))
                        {
                            string input = JsonData;


                            streamWriter.Write(input);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }

                       
                       
                    }
                    HttpWebResponse response = (HttpWebResponse)tRequest.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                    dynamic data = JObject.Parse(responseString);
                    
                    
                    cmd.Parameters.AddWithValue("@Compaignname", txtcompaignname.Text);
                    cmd.Parameters.AddWithValue("@Compaignimage", ImageNew);
                    cmd.Parameters.AddWithValue("@Compaignpagelink", data.shortLink.ToString());
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());
                    cmd.Parameters.AddWithValue("@projectname", ddlprojectname.SelectedItem.Text);
                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView1();

                }

                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }

    }
}
