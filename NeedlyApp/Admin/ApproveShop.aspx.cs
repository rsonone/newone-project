﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace NeedlyApp.Admin
{
    public partial class ApproveShop : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        SqlConnection conn = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridBing();
                BindCategory();
                District();
            }

        }

        public void BindCategory()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Sp_CategoryNameDetails");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@ItemName", ddlCategory.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlCategory.DataSource = ds.Tables[0];
                    ddlCategory.DataTextField = "ItemName";
                    ddlCategory.DataValueField = "Id";

                    ddlCategory.DataBind();
                    ddlCategory.Items.Insert(0, new ListItem("--Select Category--", "0"));
                    ddlCategory.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }

        public void District()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", "27");
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("-- Select District --", "0"));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {

            }
            conn.Close();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GridBing();
        }
        public void GridBing()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("SP_DownloadNeedlyCategoryWise");
                cmd.Connection = con;
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Type", ddlCategory.SelectedValue);
                cmd.Parameters.AddWithValue("@Mobile", txtMobileNumber.Text);
                cmd.Parameters.AddWithValue("@Dist", ddlDistrict.SelectedValue);

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GVApproveShop.DataSource = ds;
                    GVApproveShop.DataBind();
                }
                else
                {

                    GVApproveShop.DataSource = ds;
                    GVApproveShop.DataBind();
                    Response.Write("<script>alert('No record Found');</script>");
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.Message + "');</script>");
            }
        }

        protected void GVApproveShop_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

            //string id = GVApproveShop.DataKeys[e.NewSelectedIndex].Value.ToString();
            string id = GVApproveShop.Rows[e.NewSelectedIndex].Cells[1].Text;
            string status = GVApproveShop.Rows[e.NewSelectedIndex].Cells[3].Text;

            if (status == "0")

            {
                status = "1";
                SqlCommand cmd = new SqlCommand("SP_UpdateNeedlyCategoryWise", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ApprovedBy ", Session["username"].ToString());
                cmd.Parameters.AddWithValue("@Reg", id);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                GridBing();

            }
            else
            {
                status = "0";
                //SqlCommand cmd = new SqlCommand("SP_UpdateNeedlyCategoryWise", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@ApprovedBy ", Session["username"].ToString());
                //cmd.Parameters.AddWithValue("@Reg", id);

                //con.Open();
                //cmd.ExecuteNonQuery();
                //con.Close();
                //GridBing();

            }
            //            [Id]
            //	 , (SELECT[ItemName] FROM [DBNeedly].[dbo].[tbl_CategoryWiseShops] where Id = A.[ShopType]) As ItemName
            //,  [ShopType]
            //,[ApprovedStatus]
            //,[ApprovedBy]
            //,[ApprovedDate]

            //            Update tbl_CategoryWiseShops set ApprovedStatus = '" + status + "'where Id = " + id
            //            // update record

        }

        protected void GVApproveShop_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //  DataRow row = ((DataRowView)e.Row.DataItem).Row;

                // Label Mylabel = (Label)e.Row.FindControl("lblStatus");

                Button mybtn = (Button)e.Row.FindControl("btnStatus");

                if (e.Row.Cells[3].Text == "0")
                {
                    mybtn.Text = "Active";
                    mybtn.BackColor = System.Drawing.Color.Green;
                    //img.ImageUrl = "~/img_status/imgactive.png";
                    //Mylabel.Text = "Active";
                }
                if (e.Row.Cells[3].Text == "1")
                {
                    //img.ImageUrl = "~/img_status/imgdeactive.png";
                    //Mylabel.Text = "deActive";
                    mybtn.Text = "Deactive";
                }

            }
        }



        protected void btnStatus_Click(object sender, EventArgs e)
        {
            int rowind = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            string id = GVApproveShop.Rows[rowind].Cells[1].Text;
            string a = GVApproveShop.Rows[rowind].Cells[3].Text;

            if (a == "0")
            {
                a = "1";
                SqlCommand cmd = new SqlCommand("SP_UpdateNeedlyCategoryWise", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ApprovedStatus", a);
                cmd.Parameters.AddWithValue("@ApprovedBy ", Session["username"].ToString());
                cmd.Parameters.AddWithValue("@Reg", id);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                GridBing();

            }
            else
            {

                a = "0";
                SqlCommand cmd = new SqlCommand("SP_UpdateNeedlyCategoryWise", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ApprovedStatus", a);
                cmd.Parameters.AddWithValue("@ApprovedBy ", Session["username"].ToString());
                cmd.Parameters.AddWithValue("@Reg", id);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                GridBing();

            }
        }

        protected void GVApproveShop_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVApproveShop.PageIndex = e.NewPageIndex;
            GridBing();
        }
    }
}