﻿using DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class Newsandtestimonial : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString);
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter da = new SqlDataAdapter();
        CommonCode cc = new CommonCode();
        DataSet ds = new DataSet();
        string MobileNo = string.Empty;
        string imageUrl1 = string.Empty;
        string Imageuploadstr = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            MobileNo = Convert.ToString(Session["LoginId"]);
            if (!IsPostBack)
            {
                FillGrid1();
                BindState();

            }
        }

        protected void BindState()
        {
            try
            {

                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@Mobileno", mob);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlState.DataSource = ds.Tables[0];
                    ddlState.DataTextField = "StateName";
                    ddlState.DataValueField = "StateId";
                    ddlState.DataBind();

                    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));

                }
            }
            catch (Exception ex)
            {
                // Response.Redirect("~/Login.aspx");

            }
        }
        public void District()
        {
            try
            {
                System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand();
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.DataSet XMLDataSet = new System.Data.DataSet();
                command.Connection = con;
                con.Open();
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "UspDownloaddistnew";
                command.Parameters.AddWithValue("@StateId", ddlState.SelectedValue);
                //command.Parameters.AddWithValue("@TAHSIL", ddlTAHSIL.SelectedItem.Text);

                command.Parameters.AddWithValue("@type", 1);
                adapter.SelectCommand = command;
                adapter.Fill(XMLDataSet);
                if (XMLDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = XMLDataSet.Tables[0];
                    ddlDistrict.DataTextField = "DistrictName";
                    ddlDistrict.DataValueField = "DistrictId";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlDistrict.SelectedIndex = 0;
                    ddlDistrict.Visible = true;

                    //ddName.Visible = true;
                }
            }
            catch
            {

            }
        }
        public void FillGrid()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GVNewsdetails");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Keyword", ddlkeyword.SelectedItem.Text);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvNewsDetailsReport.DataSource = ds.Tables[0];
                    gvNewsDetailsReport.DataBind();
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void FillGrid1()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GVNewsdetails1");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvNewsDetailsReport.DataSource = ds.Tables[0];
                    gvNewsDetailsReport.DataBind();
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue == "10")
            {
                Imageupload2.Visible = true;
                Imageupload1.Visible = true;

            }
            if (ddlType.SelectedValue == "3" || ddlType.SelectedValue == "5")
            {
                Panelvideo.Visible = true;
            }
            else
            {
                Panelvideo.Visible = false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {

                if (ddlType.SelectedValue == "0" || txtHeading.Text == string.Empty || txtDetails.Text == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Please Select And Fill All Details')", true);
                }
                else
                {

                    if (ddlType.SelectedValue != "10")
                    {
                        System.IO.Stream fs = Imageupload.PostedFile.InputStream;
                        System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                        Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                        Imageuploadstr = Convert.ToBase64String(bytes, 0, bytes.Length);
                        if (ddlType.SelectedValue == "3" || ddlType.SelectedValue == "5" || ddlType.SelectedValue == "2" || ddlType.SelectedValue == "4")
                        {
                            if (ddlType.SelectedValue == "2" || ddlType.SelectedValue == "4")
                            {
                                if (Imageuploadstr == string.Empty)
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Please Select Image')", true);
                                }
                                else
                                {
                                    insertdata();
                                    FillGrid();
                                }
                            }
                            if (ddlType.SelectedValue == "3" || ddlType.SelectedValue == "5")
                            {
                                if (txtvideourl.Text == string.Empty)
                                {
                                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('Please Enter Video URL')", true);
                                }
                                else
                                {
                                    insertdata();
                                    FillGrid();
                                }
                            }

                        }
                        else
                        {
                            insertdata();
                            FillGrid();
                        }
                    }
                    else
                    {
                        System.IO.Stream fs1 = Imageupload.PostedFile.InputStream;
                        System.IO.BinaryReader br1 = new System.IO.BinaryReader(fs1);
                        Byte[] bytes = br1.ReadBytes((Int32)fs1.Length);
                        string base64String1 = Convert.ToBase64String(bytes, 0, bytes.Length);
                        System.IO.Stream fs2 = Imageupload1.PostedFile.InputStream;
                        System.IO.BinaryReader br2 = new System.IO.BinaryReader(fs2);
                        Byte[] bytes2 = br2.ReadBytes((Int32)fs2.Length);
                        string base64String2 = Convert.ToBase64String(bytes2, 0, bytes2.Length);
                        System.IO.Stream fs3 = Imageupload2.PostedFile.InputStream;
                        System.IO.BinaryReader br3 = new System.IO.BinaryReader(fs3);
                        Byte[] bytes3 = br3.ReadBytes((Int32)fs3.Length);
                        string base64String3 = Convert.ToBase64String(bytes3, 0, bytes3.Length);
                        SqlCommand cmd1 = new SqlCommand("sp_InstrNewsdetailsphotg2", con);
                        cmd1.CommandType = CommandType.StoredProcedure;

                        cmd1.Parameters.AddWithValue("@StateId", SqlDbType.NVarChar).Value = ddlState.SelectedValue;
                        cmd1.Parameters.AddWithValue("@DistrictId", SqlDbType.NVarChar).Value = ddlDistrict.SelectedValue;
                        //cmd1.Parameters.AddWithValue("@UDISEcode", SqlDbType.NVarChar).Value = txtUDISEcode.Text;
                        cmd1.Parameters.AddWithValue("@Createdby", Convert.ToString(Session["LoginId"]));
                        cmd1.Parameters.AddWithValue("@Type", SqlDbType.NVarChar).Value = ddlType.SelectedValue;
                        cmd1.Parameters.AddWithValue("@Heading", SqlDbType.NVarChar).Value = txtHeading.Text;
                        cmd1.Parameters.AddWithValue("@Priority", SqlDbType.NVarChar).Value = ddlPriority.SelectedValue;
                        cmd1.Parameters.AddWithValue("@Details", SqlDbType.NVarChar).Value = txtDetails.Text;
                        cmd1.Parameters.AddWithValue("@Images", SqlDbType.NVarChar).Value = base64String1.ToString();
                        cmd1.Parameters.AddWithValue("@Images1", SqlDbType.NVarChar).Value = base64String2.ToString();
                        cmd1.Parameters.AddWithValue("@Images2", SqlDbType.NVarChar).Value = base64String3.ToString();
                        cmd1.Parameters.AddWithValue("@Keyword", SqlDbType.NVarChar).Value = ddlkeyword.SelectedItem.Text;
                        con.Open();
                        cmd1.ExecuteNonQuery();
                        con.Close();
                        FillGrid();
                    }
                    // Response.Redirect("Newsdetails.aspx");


                }
            }
            catch (Exception ex)
            {
            }

        }
        public void insertdata()
        {
            try
            {
                SqlCommand cmd1 = new SqlCommand("sp_InstrNewsdetails1", con);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.Parameters.AddWithValue("@StateId", SqlDbType.NVarChar).Value = ddlState.SelectedValue;
                cmd1.Parameters.AddWithValue("@DistrictId", SqlDbType.NVarChar).Value = ddlDistrict.SelectedValue;
                //cmd1.Parameters.AddWithValue("@UDISEcode", SqlDbType.NVarChar).Value = txtUDISEcode.Text;
                cmd1.Parameters.AddWithValue("@Createdby", Convert.ToString(Session["LoginId"]));
                cmd1.Parameters.AddWithValue("@Type", SqlDbType.NVarChar).Value = ddlType.SelectedValue;
                cmd1.Parameters.AddWithValue("@Priority", SqlDbType.NVarChar).Value = ddlPriority.SelectedValue;
                cmd1.Parameters.AddWithValue("@Heading", SqlDbType.NVarChar).Value = txtHeading.Text;
                cmd1.Parameters.AddWithValue("@Details", SqlDbType.NVarChar).Value = txtDetails.Text;
                cmd1.Parameters.AddWithValue("@Images", SqlDbType.NVarChar).Value = Imageuploadstr.ToString();
                cmd1.Parameters.AddWithValue("@VodeoURL", SqlDbType.NVarChar).Value = txtvideourl.Text;
                cmd1.Parameters.AddWithValue("@Keyword", SqlDbType.NVarChar).Value = ddlkeyword.SelectedItem.Text;
                con.Open();
                cmd1.ExecuteNonQuery();
                con.Close();
                datacler();
            }
            catch (Exception ex)
            {

            }

        }
        public void datacler()
        {
            txtDetails.Text = "";
            txtHeading.Text = "";
            txtvideourl.Text = "";
            ddlType.SelectedItem.Text = "";

        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                System.IO.Stream fs = Imageupload.PostedFile.InputStream;
                System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                string Imageuploadstr = Convert.ToBase64String(bytes, 0, bytes.Length);

                SqlCommand cmd2 = new SqlCommand("sp_updateNewsdetails", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@ID", lblId.Text);
                cmd2.Parameters.AddWithValue("@Modifiedby", Convert.ToString(Session["LoginId"]));
                cmd2.Parameters.AddWithValue("@Type", SqlDbType.NVarChar).Value = ddlType.SelectedValue;
                cmd2.Parameters.AddWithValue("@Heading", SqlDbType.NVarChar).Value = txtHeading.Text;
                cmd2.Parameters.AddWithValue("@Priority", SqlDbType.NVarChar).Value = ddlPriority.SelectedValue;
                cmd2.Parameters.AddWithValue("@Details", SqlDbType.NVarChar).Value = txtDetails.Text;
                cmd2.Parameters.AddWithValue("@Keyword", SqlDbType.NVarChar).Value = ddlkeyword.SelectedItem.Text;
                //if(Imageuploadstr!="")
                //{

                //    cmd2.Parameters.AddWithValue("@Images", SqlDbType.NVarChar).Value = Imageuploadstr.ToString();
                //}
                //else
                //{
                //    cmd2.Parameters.AddWithValue("@Images", SqlDbType.NVarChar).Value = imageUrl1.ToString();
                //}

                // cmd2.Parameters.AddWithValue("@Images", SqlDbType.NVarChar).Value = Imageuploadstr.ToString();
                con.Open();
                cmd2.ExecuteNonQuery();
                con.Close();
                FillGrid();

            }
            catch (Exception ex)
            {

            }
            //Response.Redirect("Newsdetails.aspx");

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {

        }



        protected void gvNewsDetailsReport_PageIndexChanged(object sender, EventArgs e)
        {

        }

        protected void gvNewsDetailsReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvNewsDetailsReport.PageIndex = e.NewPageIndex;
            FillGrid();
        }

       
        protected void lnkbtnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                Image1.Visible = true;
                btnUpdate.Visible = true;
                btnSubmit.Visible = false;
                Button btn = sender as Button;

                GridViewRow grow = btn.NamingContainer as GridViewRow;
                lblId.Text = (grow.FindControl("lblId1") as Label).Text;

                SqlCommand cmd3 = new SqlCommand();
                SqlDataAdapter da4 = new SqlDataAdapter();
                DataSet ds4 = new DataSet();
                cmd3.CommandText = "sp_EditNewsdetails";
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Connection = con;
                cmd3.Parameters.AddWithValue("@ID", lblId.Text);
                da4 = new SqlDataAdapter(cmd3);
                da4.Fill(ds4);
                if (ds4.Tables[0].Rows.Count > 0)
                {
                    ddlType.SelectedValue = ds4.Tables[0].Rows[0][1].ToString();
                    txtHeading.Text = ds4.Tables[0].Rows[0][2].ToString();
                    txtDetails.Text = ds4.Tables[0].Rows[0][3].ToString();
                    imageUrl1 = "data:image/jpg;base64," + ds4.Tables[0].Rows[0][4].ToString();
                    Image1.ImageUrl = imageUrl1;
                    //txtUDISEcode.Text = ds4.Tables[0].Rows[0][5].ToString();
                    ddlPriority.SelectedValue = ds4.Tables[0].Rows[0][6].ToString();
                    ddlkeyword.SelectedItem.Text = ds4.Tables[0].Rows[0][11].ToString();

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gr = (GridViewRow)btn.NamingContainer;
                int i = Convert.ToInt32(gr.RowIndex);
                string ID = Convert.ToString(gvNewsDetailsReport.Rows[i].Cells[0].Text);

                using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("SP_DeleteNewsAndTestmonials", DataBaseConnection);
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", ID);

                    int A = cmd.ExecuteNonQuery();
                    DataBaseConnection.Close();
                    FillGrid1();
                    if (A == -1)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert('Data Deleted Successfully...!');", true);
                    }
                }
            }
            catch (Exception ex)
            {
                // EL.SendErrorToText(ex);
            }
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }


        protected void ddlkeyword_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillGrid();
        }
    }
}