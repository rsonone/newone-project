﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace NeedlyApp.Admin
{
    public partial class MhDataReport : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            lbl1.Text = "Page Load";
        }

        protected void btnSearchByPinCode_Click(object sender, EventArgs e)
        {
            try
            {
                lbl2.Text = "search btn Try bloack";
                BindGridView();

            }
            catch (Exception ex)
            {
                lbl3.Text = "Search btn catch bloack";
                ShowMessage(ex.Message);
                lbl3.Text = ex.Message;
            }
        }
        private void BindGridView()
        {
            MySqlConnection conn = new
     MySqlConnection(ConfigurationManager.ConnectionStrings["MysqlConnectionString"].ConnectionString);
            try
            {
                lbl4.Text = "Grid try block";
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                MySqlCommand cmd = new MySqlCommand("select MOBILE_NUMBER,CUSTOMER_NAME,CUSTOMER_ADDRESS, SERVICE_PROVIDER from mh_data LIMIT 2000", conn);
                //MySqlCommand cmd = new MySqlCommand("select  MOBILE_NUMBER, CUSTOMER_NAME, CUSTOMER_ADDRESS, SERVICE_PROVIDER from mh_data where CUSTOMER_ADDRESS like '" +txtPinCode.Text+ "' LIMIT 10; ", conn);
                //cmd.Parameters.AddWithValue("@Pincode", txtPinCode.Text);
                MySqlDataAdapter adp = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adp.Fill(ds);
                GVMHData.DataSource = ds;
                GVMHData.DataBind();
                //lblCount.Text = GVMHData.Rows.Count.ToString();
            }
            catch (MySqlException ex)
            {
                lbl4.Text = "grid catch bloack";
                ShowMessage(ex.Message);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }


        void ShowMessage(string msg)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language = 'javascript' > alert('" + msg + "');</ script > ");
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "MH_Data.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVMHData.AllowPaging = false;
            GVMHData.PageIndex = 0;

            BindGridView();
            //Change the Header Row back to white color
            GVMHData.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVMHData.HeaderRow.Cells.Count; i++)
            {
                GVMHData.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVMHData.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        protected void GVMHData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVMHData.PageIndex = e.NewPageIndex;
            BindGridView();
        }
    }
}