﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class RegistrationReport : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["username"].ToString() == "9422325020")
            {
                btnExcel.Visible = true;
            }
            if (!IsPostBack)
            {
                Gridview();
                BindState();
            }
        }
        public void BindState()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getState");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlState.DataSource = ds.Tables[0];
                    ddlState.DataTextField = "STATE_NAME";
                    ddlState.DataValueField = "MDDS_STC";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
                    ddlState.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void District()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getDISTRICT");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlDistrict.DataSource = ds.Tables[0];
                    ddlDistrict.DataTextField = "DISTRICT_NAME";
                    ddlDistrict.DataValueField = "MDDS_DTC";

                    ddlDistrict.DataBind();
                    ddlDistrict.Items.Insert(0, new ListItem("--Select--", ""));
                    ddlDistrict.SelectedIndex = 0;
                }
                else
                {
                    ddlDistrict.SelectedItem.Text = "";
                }
            }
            catch (Exception ex)
            {

            }
            con.Close();
        }
        public void City()
        {
            ddlTaluka.Items.Clear();
            try
            {
                SqlCommand cmd = new SqlCommand("sp_getTaluka");
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MDDS_STC", ddlState.SelectedValue);
                cmd.Parameters.AddWithValue("@MDDS_DTC", ddlDistrict.SelectedValue);
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet();
                da.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTaluka.DataSource = ds.Tables[0];
                    ddlTaluka.DataTextField = "SUB_DISTRICT_NAME";
                    ddlTaluka.DataValueField = "MDDS_Sub_DT";

                    ddlTaluka.DataBind();
                    ddlTaluka.Items.Insert(0, new ListItem("--Select--", ""));
                    ddlTaluka.SelectedIndex = 0;
                }
                else
                {
                    ddlTaluka.SelectedItem.Text = "";
                }
            }
            catch
            {

            }
        }
        public void Gridview()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_DownloadRegDetailsDemo");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@State", ddlState.SelectedValue);
                    cmd.Parameters.AddWithValue("@District", ddlDistrict.SelectedValue);
                    cmd.Parameters.AddWithValue("@Taluka", ddlTaluka.SelectedValue);
                    cmd.Parameters.AddWithValue("@Role", ddlRole.SelectedValue);
                    cmd.Parameters.AddWithValue("@Digit", ddldigit.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVReg_Report.DataSource = ds;
                        GVReg_Report.DataBind();
                        Label3.Text = (ds.Tables[0].Rows.Count.ToString());

                    }

                   // DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            } 
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            GVReg_Report1.Visible = false;
            Gridview();
        }
        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            District();
        }
        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            City();
        }
        protected void GVReg_Report_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report.PageIndex = e.NewPageIndex;
            this.Gridview();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Registration.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            GVReg_Report.AllowPaging = false;
            GVReg_Report.PageIndex = 0;
           
            Gridview();
            //Change the Header Row back to white color
            GVReg_Report.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < GVReg_Report.HeaderRow.Cells.Count; i++)
            {
                GVReg_Report.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            GVReg_Report.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }


       
        //public void searchByMobile()
        //{
        //    using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
        //    {
        //        try
        //        {
        //            SqlCommand cmd = new SqlCommand("select * FROM [DBNeedly].[dbo].[EzeeDrugsAppDetail] where  keyword='NEEDLY' and  ='" + txtName.Text + "'");
        //            cmd.Connection = DataBaseConnection;


        //            SqlDataAdapter da = new SqlDataAdapter();
        //            da.SelectCommand = cmd;
        //            DataSet ds = new DataSet();
        //            da.Fill(ds);
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                GVReg_Report.DataSource = ds.Tables[0];
        //                GVReg_Report.DataBind();
        //            }

        //            //string SQl1 = "SELECT COUNT(Id) FROM [dbo].[tbl_ContactDetails]  WHERE CreatedBy='" + Session["username"].ToString() + "'";
        //            //String ExcelContactCount = cc.ExecuteScalarContact(SQl1);
        //            //LblContactCount.Text = ExcelContactCount;
        //            //lblExcelCountMsg.Visible = true;
        //            //PnlCB.Visible = true;
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //    }
        //}

        protected void btnsearchbyname_Click(object sender, EventArgs e)
        {
            //GVReg_Report.Visible = false;
           // GVReg_Report1.Visible = true;
            GridViewForSerch();

        }
        public void GridViewForSerch()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
            //    try
            //    {
            //        SqlCommand cmd = new SqlCommand("SP_GetSearchbyName");
            //        cmd.Connection = DataBaseConnection;
            //        cmd.CommandType = CommandType.StoredProcedure;
            //        cmd.Parameters.AddWithValue("@Name", txtName.Text);

            //        SqlDataAdapter da = new SqlDataAdapter();
            //        da.SelectCommand = cmd;
            //        DataSet ds = new DataSet();
            //        da.Fill(ds);
            //        if (ds.Tables[0].Rows.Count > 0)
            //        {
            //            GVReg_Report.DataSource = ds.Tables[0];
            //            GVReg_Report.DataBind();
            //        }
            //        else
            //        {
            //           // lblNoData.Text = "No rocord found for CreatedBy  '" + txtMobileExcelNumber.Text + "'";
            //        }


            //    }
            //    catch (Exception ex)
            //    {

            //    }

                SqlCommand cmd = new SqlCommand("SP_GetSearchbyName");
                cmd.Connection = DataBaseConnection;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", txtName.Text);

            SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

               DataSet ds = new DataSet();
              //  DataTable dt =  New   taTable();
                da.Fill(ds);
                GVReg_Report1.DataSource = ds;
                GVReg_Report1.DataBind();
               

                //da.Fill(ds);
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    GVReg_Report.DataSource = ds.Tables[0];
                //    GVReg_Report.DataBind();
                //}
                //DataBaseConnection.Close();
            }
        }

        protected void GVReg_Report1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVReg_Report1.PageIndex = e.NewPageIndex;
            GridViewForSerch();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admin/RegistrationReport.aspx");
        }
    }
}