﻿using DAL;
using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class SalesTeamNotification : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString);
        CommonCode cc = new CommonCode();
        Errorlogfil EL = new Errorlogfil();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillGrid();
            }
        }
   
        protected void btnSend_Click1(object sender, EventArgs e)
        {
            try
            { 
          //  SqlCommand cmd = new SqlCommand("SP_UploadInsertSalesTeams", con);
                SqlCommand cmd = new SqlCommand("SP_UploadInsertSalesTeamsET", con);
                cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
                cmd.Parameters.AddWithValue("@Type","0");
                cmd.Parameters.AddWithValue("@PersonName", txtName.Text);
            cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
            cmd.Parameters.AddWithValue("@Mobile", txtmobile.Text);
            cmd.Parameters.AddWithValue("@Head", txtHead.Text);
            cmd.Parameters.AddWithValue("@Sequence", txtSequenceNo.Text);
            cmd.Parameters.AddWithValue("@Capacity", txtCapacity.Text);
            //cmd.Parameters.AddWithValue("@AssigedStatus", txtAssigedStatus.Text);
            //cmd.Parameters.AddWithValue("@Assigement", rdAssign.SelectedValue);
            cmd.Parameters.AddWithValue("@Work", rdbWork.SelectedValue);
            cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

            int A = cmd.ExecuteNonQuery();
            con.Close();
            con.Close();
           
                if (A == -1)
                {
                    ClearData();
                    FillGrid();
                       ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                }
            }
   
                catch (Exception ex)
                {
                    EL.SendErrorToText(ex);
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED UNSUCCESSFULLY...! ');", true);

  
               }

        }

        protected void btnCancle_Click(object sender, EventArgs e)
        {
            ClearData();
        }

        public void ClearData()
        {
            txtAddress.Text = "";
        //    txtAssigedStatus.Text = "";
            txtCapacity.Text = "";
            txtHead.Text = "";
            txtmobile.Text = "";
            txtName.Text = "";
            txtSequenceNo.Text = "";
         //   rdAssign.SelectedValue ="";
            rdbWork.SelectedValue ="";
        }

        protected void gvAdvCourse_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAdvCourse.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        public void FillGrid()
        {
            SqlCommand cmd = new SqlCommand("sp_GetSalesTeam_Notifictions");
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter AD = new SqlDataAdapter();
            AD.SelectCommand = cmd;
            DataSet ds = new DataSet();
            AD.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAdvCourse.DataSource = ds.Tables[0];
                gvAdvCourse.DataBind();
                  lblTotalCount.Visible = true;
                  lblCuntDisplay.Text = (ds.Tables[0].Rows.Count.ToString());
            }
        }

        protected void lnkbtnupdate_Click(object sender, EventArgs e)
        {
            try
            {
                btnUpdate.Visible = true;
                btnSend.Visible = false;
                 Button btn = sender as Button;

                GridViewRow grow = btn.NamingContainer as GridViewRow;
                lblId.Text = (grow.FindControl("lblId1") as Label).Text;

                SqlCommand cmd3 = new SqlCommand();
                SqlDataAdapter da4 = new SqlDataAdapter();
                DataSet ds4 = new DataSet();
                cmd3.CommandText = "sp_GetshowSalesTeam_Notifictions";
                cmd3.CommandType = CommandType.StoredProcedure;
                cmd3.Connection = con;
                cmd3.Parameters.AddWithValue("@Id", lblId.Text);
                da4 = new SqlDataAdapter(cmd3);
                da4.Fill(ds4);
                if (ds4.Tables[0].Rows.Count > 0)
                {
                    txtName.Text = ds4.Tables[0].Rows[0]["SalesPersonName"].ToString();
                    txtAddress.Text = ds4.Tables[0].Rows[0]["Address"].ToString();
                    txtmobile.Text = ds4.Tables[0].Rows[0]["MobileNo"].ToString();
                    txtHead.Text = ds4.Tables[0].Rows[0]["UnderSalesHead"].ToString();
                    txtSequenceNo.Text = ds4.Tables[0].Rows[0]["SequenceNo"].ToString();
                    txtCapacity.Text = ds4.Tables[0].Rows[0]["Capacity"].ToString();
                    rdbWork.SelectedValue = ds4.Tables[0].Rows[0]["InWorking"].ToString();


                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void lnkProfile_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;

            Label1.Text = (btn.FindControl("lblView") as Label).Text;

            string Id = Label1.Text;

            string Query = "update [DBNeedly].[dbo].tbl_SalesTeam_Notifictions  set InWorking = '0' where Id = '" + Id + "'";
            int Res = cc.ExecuteNonQuery(Query);

            FillGrid();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("SP_UploadInsertSalesTeamsET", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.Parameters.AddWithValue("@Type", lblId.Text);
                cmd.Parameters.AddWithValue("@PersonName", txtName.Text);
                cmd.Parameters.AddWithValue("@Address", txtAddress.Text);
                cmd.Parameters.AddWithValue("@Mobile", txtmobile.Text);
                cmd.Parameters.AddWithValue("@Head", txtHead.Text);
                cmd.Parameters.AddWithValue("@Sequence", txtSequenceNo.Text);
                cmd.Parameters.AddWithValue("@Capacity", txtCapacity.Text);
                cmd.Parameters.AddWithValue("@Work", rdbWork.SelectedValue);
                cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                int A = cmd.ExecuteNonQuery();
                con.Close();
                con.Close();

                if (A == -1)
                {
                    ClearData();
                    FillGrid();
                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "anything", "alert(' DATA IMPORTED SUCCESSFULLY...!');", true);
                    Response.Redirect("../Admin/SalesTeamNotification.aspx");
                }

            }
            catch (Exception ex)
            {

            }
        }
    }
}