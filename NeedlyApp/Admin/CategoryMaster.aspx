﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/NeedlyMaster.Master" AutoEventWireup="true" CodeBehind="CategoryMaster.aspx.cs" Inherits="NeedlyApp.Admin.CategoryMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type = "text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Delete Short Name?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
    <style >
      .container {
   overflow: scroll;
        min-height: 100%;
}
    </style>
    <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> <i class="fa fa-list"></i>  Category Excel Uplaod / Category Master <small></small></h2>
                       <div class="container-fluid">
                        <div class="pull-right">
              
                        </div>
                     </div>
                    <div class="clearfix"></div>
                  </div>
                 
                     <div class="row">                                   
                            <div class="col-md-12">
                                <div class="box-body col-sm-6">
                                 <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Select ShopType: </label>
                                <div class="col-sm-9">
                                   <div class="container" style="width:500px; height:200px">
                                                                              
                                            <asp:CheckBoxList ID="chkShoptype" runat="server" Width="70px" CssClass="form-control" Font-Size="Medium" >
                                            </asp:CheckBoxList>
                                     
                                    </div>
                          </div></div>
                                </div>
                               
                    </div>
               </div>

                     <div class="row">
           <div class="panel-body">                                     
                <div class="col-md-12">
                     <div class="box-body col-sm-6">
                         <div class="form-group">
                                <label class="col-sm-3 control-label" for="input-name2">
                                    Upload Excel: </label>
                                <div class="col-sm-9">
                                  <asp:FileUpload ID="Fileupload1" runat="server" />
                          </div></div></div>
                     <div class="box-body col-sm-offset-2  col-sm-4">
                           <asp:LinkButton ID="lnkbtnDownload" OnClick="lnkbtnDownload_Click" runat="server" Text="Click Here!!! Download Excel Format." Font-Bold="true" Font-Size="Medium"  ForeColor="Red"></asp:LinkButton>
                          </div>
                    </div>
               </div></div> 

                     <div class="col-md-12">
                        <div class="box-body col-sm-5">
                         <asp:Label ID="lblID" Visible="false" runat="server"></asp:Label>  
                        </div>
                         <div class="box-body col-sm-4">
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" class="btn btn-success" />
                              
                              <asp:Button ID="btnreset" runat="server" Text="Reset" class="btn btn-info" />
                        </div>
                        <div class="box-body col-sm-4">
                            <asp:Label ID="Lblerrer" runat="server"></asp:Label>    
                        </div>
                    </div>

                     <div class="col-md-12">
                           <div class="table-responsive">
                    <asp:GridView ID="GVCategoryMaster" CssClass="table table-hover table-bordered" runat="server" 
                        PagerStyle-VerticalAlign="Middle" PageSize="10" AutoGenerateColumns="false" AllowPaging="true" Font-Names="Arial" DataKeyNames="Id" >
                    <Columns>   
                    <%--    <asp:TemplateField HeaderText="Sr.No">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField> --%> 
                        
      
                          <asp:BoundField DataField="Id" HeaderText="ID" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="CategoryName" HeaderText="Category Name">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="CategoryDescription" HeaderText="Category Description">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="ShopType" HeaderText="ShopType">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                          <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>
                        
                       <%--  <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" ReadOnly="true">
                                    <asp:HeaderStyle HorizontalAlign="Center"></asp:HeaderStyle>
                                    <asp:ItemStyle HorizontalAlign="Center" ></asp:ItemStyle>
                         </asp:BoundField>--%>
                         <%-- <asp:CommandField ShowEditButton="true" />  --%>
                      <%--  <asp:CommandField ShowDeleteButton="true" />--%>
                      <%--  <asp:TemplateField HeaderText="Delete">
	                        <ItemTemplate>
		                        <asp:Button ID="deleteButton" runat="server" CommandName="Delete" Text="Delete"                                   OnClientClick="return confirm('Are you sure you want to delete this data?');" />
                                <%-- <asp:Label ID="lblDelete" runat="server" Text='<%#Eval("Id") %>' Visible="false"></asp:Label> --%>
	                       <%-- </ItemTemplate>
                        </asp:TemplateField>--%>
                      </Columns>
                          <HeaderStyle  BackColor="#3399FF" ForeColor="White" Font-Bold="True" Height="10px" />
                      </asp:GridView>
                </div>
                          <asp:Label ID="lblEdit" runat="server" Visible="false"></asp:Label>
                     </div>
           

                    
                  </div>
                </div></div>
</asp:Content>
