﻿using DAL;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NeedlyApp.Admin
{
    public partial class AddQuestion : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        CommonCode cc = new CommonCode();
        string ShopType = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Shop();
                GridView();
            }
        }
        protected void GVAddQues_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVAddQues.PageIndex = e.NewPageIndex;
            GridView();
        }
        public void Shop()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {
                    SqlCommand cmd = new SqlCommand("SP_LoadShopType");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlShop.DataSource = ds.Tables[0];
                        ddlShop.DataTextField = "ItemName";
                        ddlShop.DataValueField = "Id";

                        ddlShop.DataBind();
                        ddlShop.Items.Insert(0, new ListItem("--Select--", ""));
                        ddlShop.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlShop.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    DataBaseConnection.Close();
                }
            }

        }
        public void Category()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_loadCategory");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ShopType", ddlShop.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlCat.DataSource = ds.Tables[0];
                        ddlCat.DataTextField = "CategoryName";
                        ddlCat.DataValueField = "Id";

                        ddlCat.DataBind();
                        ddlCat.Items.Insert(0, new ListItem("--Select--", ""));
                        ddlCat.SelectedIndex = 0;

                        //ViewState["ShopType"] = Convert.ToString(ds.Tables[0].Rows[2]["ShopType"]);
                    }
                    else
                    {
                        ddlCat.SelectedItem.Text = "";
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
        public void GridView()
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("sp_DownloadQuestion");
                    cmd.Connection = DataBaseConnection;
                    DataBaseConnection.Open();
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GVAddQues.DataSource = ds;
                        GVAddQues.DataBind();
                    }
                    DataBaseConnection.Close();
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UploadCateoryQustion");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@QuestionName", txtQues.Text);
                    cmd.Parameters.AddWithValue("@ShopType", ddlShop.SelectedValue);
                    cmd.Parameters.AddWithValue("@CompulsoryFlag", rdbCom.SelectedValue);
                    cmd.Parameters.AddWithValue("@CategoryId", ddlCat.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA SUBMITTED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
        protected void ddlShop_SelectedIndexChanged(object sender, EventArgs e)
        {
            Category();
        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    Button btn = sender as Button;
                    GridViewRow gv = btn.NamingContainer as GridViewRow;
                    lblId.Text = (btn.FindControl("lblId1") as Label).Text;
                    SqlCommand command4 = new SqlCommand();
                    command4.Connection = DataBaseConnection;
                    DataBaseConnection.Open();

                    command4.CommandText = "sp_EditQuestion";
                    command4.CommandType = System.Data.CommandType.StoredProcedure;
                    command4.Parameters.AddWithValue("@Id", lblId.Text);

                    SqlDataAdapter da4 = new SqlDataAdapter();
                    da4.SelectCommand = command4;
                    DataSet ds4 = new DataSet();
                    da4.Fill(ds4);
                    if (ds4.Tables[0].Rows.Count > 0)
                    {
                        txtQues.Text = ds4.Tables[0].Rows[0][0].ToString();
                        rdbCom.SelectedValue = ds4.Tables[0].Rows[0][2].ToString();
                    }
                    btnSubmit.Visible = false;
                    btnUpdate.Visible = true;
                }
                catch (Exception ex)
                {

                }
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            using (SqlConnection DataBaseConnection = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SP_UpdateCateoryQustion");
                    cmd.Connection = DataBaseConnection;
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataBaseConnection.Open();

                    cmd.Parameters.AddWithValue("@Id", lblId.Text);
                    cmd.Parameters.AddWithValue("@QuestionName", txtQues.Text);
                    cmd.Parameters.AddWithValue("@ShopType", ddlShop.SelectedValue);
                    cmd.Parameters.AddWithValue("@CompulsoryFlag", rdbCom.SelectedValue);
                    cmd.Parameters.AddWithValue("@CategoryId", ddlCat.SelectedValue);
                    cmd.Parameters.AddWithValue("@CreatedBy", Session["username"].ToString());

                    int A = cmd.ExecuteNonQuery();
                    if (A == -1)
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED SUCCESSFULLY.!!!')", true);
                    }
                    DataBaseConnection.Close();
                    GridView();
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "msg", "alert('DATA UPDATED UNSUCCESSFULLY.!!!')", true);
                }
            }
        }
    }
}