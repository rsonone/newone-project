﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BAL
{
    public class FcmRegDetail
    {
        public object MobileNo { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public object Id { get; set; }
        public string LocalId { get; set; }
        public object DistID { get; set; }
        public object TalukaId { get; set; }
        public object AppKeyword { get; set; }
        public object Imei { get; set; }
        public object FcmId { get; set; }
    }
    public class DownloadInventoryItem
    {

        public string Image_Path { get; set; }
        public string ItemName_English { get; set; }
        public string Parent_Id { get; set; }
        public string Id { get; set; }
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public string Rate { get; set; }
        public string Brand { get; set; }
        public string OfferPrice { get; set; }
        public string Discount { get; set; }
        public string Mobile_No { get; set; }
        public string IMEI { get; set; }
        public string Registration_Id { get; set; }
        public string DeleteStatus { get; set; }
        public string EnableStatus { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }

    }





    [DataContract]

    public class NeedlyRegistrationDetail
    {


        [DataMember]
        public string DynamicURL { get; set; }
        public string ReferenceMobile { get; set; }

        [DataMember]
        public string WifiRouter { get; set; }

        [DataMember]
        public string ShopImage_Wide { get; set; }

        [DataMember]
        public string Average { get; set; }
        [DataMember]
        public string ShopImage { get; set; }

        [DataMember]
        public string ProfilePic { get; set; }

        [DataMember]
        public string MinOrderAmount { get; set; }

        [DataMember]
        public string AboutShop { get; set; }

        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string HomeDeliveryCharges { get; set; }

        [DataMember]
        public string UserRole { get; set; }
        [DataMember]
        public string ShopType { get; set; }
        [DataMember]
        public string MobileSecurity { get; set; }

        [DataMember]
        public string joincompain { get; set; }
       




        [DataMember]
        public string Middle_Name { get; set; }
        [DataMember]
        public string GSTIN_No { get; set; }
        [DataMember]
        public string Pan_No { get; set; }
        [DataMember]
        public string Aadhar_No { get; set; }
        [DataMember]
        public string Services_Provided { get; set; }
        [DataMember]
        public string DOB { get; set; }


        [DataMember]
        public string passcodelimit { get; set; }
        [DataMember]
        public string GCM_Regid { get; set; }
        [DataMember]
        public string OTP { get; set; }
        [DataMember]
        public string DealerMobNo { get; set; }
        [DataMember]
        public string CreatedDate { get; set; }
        [DataMember]
        public string AlternateMobile { get; set; }
        [DataMember]
        public string AddressLine1 { get; set; }
        [DataMember]
        public string AddressLine2 { get; set; }
        [DataMember]
        public object Area { get; set; }
        [DataMember]
        public string Landmark { get; set; }
        [DataMember]
        public object FromTime { get; set; }
        [DataMember]
        public object ToTime { get; set; }

        [DataMember]

        public string status { get; set; }
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string mobileNo { get; set; }
        [DataMember]
        public object usertype { get; set; }
        [DataMember]
        public string longitude { get; set; }
        [DataMember]
        public object latitude { get; set; }
        [DataMember]
        public object passcode { get; set; }
        [DataMember]
        public string pincode { get; set; }
        [DataMember]
        public string typeOfUse_Id { get; set; }
        [DataMember]
        public string eMailId { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string firmName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public string strSimSerialNo { get; set; }
        [DataMember]
        public string strDevId { get; set; }
        [DataMember]
        public string keyword { get; set; }
        [DataMember]

        public string Error { get; set; }
        [DataMember]
        public string LocalId { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string ServerId { get; set; }
        [DataMember]
        public string UdiseCode { get; set; }
        [DataMember]
        public string RoleID { get; set; }
        [DataMember]
        public string Taluka { get; set; }
        [DataMember]
        public string Spatialization { get; set; }
        [DataMember]
        public string Village { get; set; }
        [DataMember]
        public string Favorite { get; set; }
        [DataMember]
        public string LadlineNo { get; set; }
        [DataMember]
        public string Qualification { get; set; }
        [DataMember]
        public string District { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string RefMobileNo { get; set; }
        [DataMember]
        public string EntryDate { get; set; }
        [DataMember]
        public string UserId { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public string NoData { get; set; }

        [DataMember]
        public string height { get; set; }
        [DataMember]
        public string waight { get; set; }
        [DataMember]
        public string cheast { get; set; }
        [DataMember]
        public string ismarried { get; set; }
        [DataMember]
        public string dom { get; set; }

        [DataMember]
        public string ModifyBy { get; set; }

    }
    public class ReportResultParameter
    {
        public string Mobile { get; set; }
        public string Column_Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Village { get; set; }
        public string Status { get; set; }
        public string ParentTrail_Id { get; set; }
        public string creareddate { get; set; }
        public string Srno { get; set; }
        public string ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string OfficeCode { get; set; }
        public string createdby { get; set; }
        public string fieldstype { get; set; }
        public string value { get; set; }
        public string filled_for { get; set; }
        public string filled_for_date { get; set; }
        public string related_id { get; set; }
        public string related_name { get; set; }
        public string cycle_type { get; set; }
        public string fieldsID { get; set; }
        public string Error { get; set; }
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string IEMI { get; set; }
        public string Relatedto { get; set; }
        public string ReportID { get; set; }
        public string MasterID { get; set; }
        public string NoData { get; set; }
        public string FieldName { get; set; }
        public string ItemName { get; set; }
        public string FieldHint { get; set; }
        public string PageNo { get; set; }
    }

    public class InventoryCategoryDetails
    {
        public string Id { get; set; }
        public string CategoryName { get; set; }
        public string ItemId { get; set; }
        public string ParentId { get; set; }
        public string ItemType { get; set; }
        public string MobileNo { get; set; }
        public string Registration_Id { get; set; }
        public string AppVersion { get; set; }
        public string DeleteStatus { get; set; }
        public string EnableStatus { get; set; }
        public string IMEI { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }

        public string NoData { get; set; }
        public string CreatedDate { get; set; }
        public string InventoryCategory_ID { get; set; }
        public string InventoryCombo_ID { get; set; }
        public List<InvemtoryCtegoryItem> Item_Details { get; set; }
        public string Image { get; set; }

    }

    public class InvemtoryCtegoryItem
    {

        public string ItemId { get; set; }
        public string ItemType { get; set; }
        public string NoData { get; set; }
        public string CreatedDate { get; set; }
        public string InventoryCategory_ID { get; set; }
        public string InventoryCombo_ID { get; set; }
        public string Id { get; set; }
        public string Mobile_No { get; set; }
        public string Registration_Id { get; set; }
        public string ItemName { get; set; }
        public string EnableStatus { get; set; }
        public string DeleteStatus { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public List<InventoryComboDetails> Combo_Details { get; set; }
        public List<InventoryItem> Inventory_Details { get; set; }

    }
    public class InventoryComboDetails
    {
        public string Id { get; set; }
        public string Mobile_No { get; set; }
        public string Registration_Id { get; set; }
        public string Combo_Name { get; set; }
        public string Rate { get; set; }
        public string Quantity { get; set; }
        public string Brand { get; set; }
        public string OfferPrice { get; set; }
        public string Discount { get; set; }
        public string DeleteStatus { get; set; }
        public string EnableStatus { get; set; }
        public string IMEI { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

        public string NoData { get; set; }
        public string CreatedDate { get; set; }
        public string InventoryCategory_ID { get; set; }
        public string InventoryCombo_ID { get; set; }
        public List<InventoryComboItem> Combo_Details { get; set; }
    }
    public class InventoryComboItem
    {
        public string Id { get; set; }
        public string Mobile_No { get; set; }
        public string Registration_Id { get; set; }
        public string ItemName { get; set; }
        public string Quantity { get; set; }
        public string Unit { get; set; }
        public string DeleteStatus { get; set; }
        public string EnaleStatus { get; set; }
        public string CreatedBy { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string CreatedDate { get; set; }
        public string InventoryCategory_ID { get; set; }
        public string InventoryCombo_ID { get; set; }

    }
    public class InventoryItem
    {
        public string Image { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string Id { get; set; }
        public string ItemName { get; set; }
        public string Image_Path { get; set; }

        public string ItemName_English { get; set; }
        public string Parent_Id { get; set; }
        public string Unit { get; set; }
        public string Rate { get; set; }
        public string Brand { get; set; }
        public string OfferPrice { get; set; }
        public string Discount { get; set; }
        public string Mobile_No { get; set; }
        public string IMEI { get; set; }
        public string Registration_Id { get; set; }
        public string DeleteStatus { get; set; }
        public string EnableStatus { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }

    }
    public class UploadOpenAppParameter
    {
        public string Id { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Version_Code { get; set; }
        public string Mobile { get; set; }
        public string Imei { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }


    }

    public class UploadSendOTPParameter
    {
        public string UserMobile { get; set; }
        public string RefMobile { get; set; }
        public string IMEINumber { get; set; }
        public string AppKeyword { get; set; }
        public string UserType { get; set; }
        public string Error { get; set; }
        public object Status { get; set; }
    }
    public class SendNotification
    {
        public string Status { get; set; }
        public string Error { get; set; }
    }

    public class UploadKYCDetails
    {
        public string Id { get; set; }
        public string Mobile_No { get; set; }
        public string ImageType { get; set; }
        public string Image { get; set; }
        public string IMEI { get; set; }
        public string Registration_Status { get; set; }
        //public string Varification_Status{ get; set; }
        //public string Payment_Status     { get; set; }
        //public string Transaction_Id     { get; set; }
        public string CreatedBy { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

    }


    public class DownloadKYCDetails
    {
        public string ServerId { get; set; }
        public string BusinessPartner_MobNo { get; set; }
        public string Type { get; set; }
        public string Images { get; set; }
        public string IMEI { get; set; }
        public string Registration_Status { get; set; }
        //public string Varification_Status{ get; set; }
        public string NoData { get; set; }
        public string Payment_Status { get; set; }
        public string CreatedBy { get; set; }
        public string Varification_Status { get; set; }
        public string Transaction_Id { get; set; }
        public string Error { get; set; }

    }
    public class DownloadBanner
    {
        public string Id { get; set; }
        public string Banner_Name { get; set; }
        public string Banner_Type { get; set; }
        public string ImageURL { get; set; }
        public string StateId { get; set; }
        public string DistrictId { get; set; }
        public string TalukaId { get; set; }
        public string PinCode { get; set; }
        public string WardNo { get; set; }
        public string RedirectURL { get; set; }
        public string ActiveStatus { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }

    }
    public class UploadOrder
    {
        public string State { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string Quotation_To { get; set; }
        public string IsUpdated { get; set; }
        public string QuotationType { get; set; }
        public string ShopType { get; set; }

        public string OrderFrom { get; set; }
        public string IMEI { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string VersionCode { get; set; }

        public string Remark { get; set; }
        public string HomeDeliveryCharges { get; set; }
        public string Status { get; set; }
        public string NoData { get; set; }
        public string UpdateId { get; set; }
        public string ChildServerId { get; set; }
        public string ChildLocalId { get; set; }
        public string Id { get; set; }
        public string User_Name { get; set; }
        public string UserMob_No { get; set; }
        public string User_Address { get; set; }
        public string Shop_Name { get; set; }
        public string Shop_MobNo { get; set; }
        public string Shop_Address { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string OrderId { get; set; }
        public string OrderStatus { get; set; }
        public string Amount { get; set; }
        public List<UploadItem> Item_Details { get; set; }
        public List<OrderInventory> OrderInventory_Details { get; set; }
    }
    public class UploadItem
    {
        public string Brand { get; set; }

        public string QuotationBasic_Id { get; set; }
        public string QuotationItem_Id { get; set; }
        public string GlobalItem_Id { get; set; }

        public string Type { get; set; }
        public string ItemId { get; set; }

        public string Id { get; set; }
        public string UpdateId { get; set; }
        public string OrderId { get; set; }
        public string Item_Name { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public string NoData { get; set; }
        public string Item_Quantity { get; set; }
        public string ChildServerId { get; set; }
        public string ChildLocalId { get; set; }
        public string Item_Unit { get; set; }
    }

    public class OrderInventory
    {
        public string Brand { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public string Item_Unit { get; set; }
        public string Item_Quantity { get; set; }

        public string Item_Name { get; set; }
        public string QuotationBasic_Id { get; set; }
        public string QuotationItem_Id { get; set; }
        public string GlobalItem_Id { get; set; }


        public string Id { get; set; }
        public string Type { get; set; }

        public string Quantity { get; set; }
        public string ServerId { get; set; }
        public string ChildServerId { get; set; }
        public string ChildLocalId { get; set; }
        public string Reg_Id { get; set; }
        public string OrderId { get; set; }
        public string CustomerMobile { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ItemId { get; set; }
        public string ItemType { get; set; }
        public string NoData { get; set; }
        public string Imei { get; set; }

    }
    public class AppUpdateParameter
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string LocalId { get; set; }

        [DataMember]
        public string status { get; set; }

        [DataMember]
        public string AppPlayStoreVersion { get; set; }


        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public string ServerId { get; set; }

        [DataMember]
        public string AppName { get; set; }

        [DataMember]
        public string AppVersion { get; set; }
    }

    public class UploadReviewDetails
    {
        public string Average { get; set; }
        public string Id { get; set; }
        public string Registration_Id { get; set; }
        public string Order_Id { get; set; }
        public string UserMobile_No { get; set; }
        public string ShopMobile_No { get; set; }
        public string Experience_Rating { get; set; }
        public string Service_Rating { get; set; }
        public string MaterialQuality_Rating { get; set; }
        public string RateOfMaterial_Rating { get; set; }
        public string Behaviour_Rating { get; set; }
        public string Suggestions { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

    }
   

        public class UploadReferalNotification
    {
        public string Customer_name { get; set; } 
        public string Customer_number { get; set; } 
        public string Ower_name { get; set; } 
        public string Owner_number { get; set; } 
        public string Id { get; set; } 
        public string LocalId { get; set; } 
        public string Error { get; set; } 
    }
        public class UploadReferalDatails
    {
        public string IsInstallled { get; set; }
        public string Id { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Customer_Mobile { get; set; }
        public string Customer_Name { get; set; }
        public string Shop_Mobile { get; set; }
        public string ShopMobile_No { get; set; }
        public string Shop_Name { get; set; }
        public string Shop_User_Name { get; set; }
        public string Created_Date { get; set; }
        public string App_Install_Date { get; set; }
        public string CreatedBy { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

    }


    public class UploadNotifiction
    {
        public string Id { get; set; }
        public string State { get; set; }
        public string Taluka { get; set; }
        public string District { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string MobileNo { get; set; }

        public string ServerId { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }

    }



    public class DownloadDashobordCount
    {
        public string Id { get; set; }

        public string Error { get; set; }
        public string NoData { get; set; }

        public string TotalOrders { get; set; }
        public string TotalCustomers { get; set; }
        public string Ratings { get; set; }
        public string ToatlItems { get; set; }
        public string MyOrders { get; set; }
        public string SubscriptionType { get; set; }
        public string SubscriptionExpireDate { get; set; }

    }

    public class DownloadReferral
    {

        public string Id { get; set; }
        public string IsInstallled { get; set; }
        public string Customer_Mobile { get; set; }
        public string Customer_Name { get; set; }
        public string Shop_Mobile { get; set; }
        public string Shop_Name { get; set; }
        public string ShopUser_Name { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string App_Install_Date { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string Type { get; set; }

    }

    public class Inventoryitem_Master
    {

        public string Category_Id { get; set; }
        public string Id { get; set; }
        public string ItemName { get; set; }
        public string ItemName_English { get; set; }
        public string ProductType { get; set; }
        public string ShopType { get; set; }
        public string CompanyName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class DownloadCategoryWiseShops
    {
        public string Id { get; set; }
        public string ItemName { get; set; }
        public string ParentID { get; set; }
        public string ShopType { get; set; }
        public string AppKeyword { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string Status { get; set; }

    }

    public class DownloadQuestionCategory
    {
        public string Id { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string ShopType { get; set; }
        public string Categoryname { get; set; }
        public string CreatedBy { get; set; }
        public List<DownloadSubCategory> DownloadSubCategoryDetails { get; set; }

    }



    public class DownloadSubCategory
    {
        public string Id { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string QuestionName { get; set; }
        public string ShopType { get; set; }
        public string CompulsoryFlag { get; set; }
        public string CategoryId { get; set; }
        public string CreatedBy { get; set; }


    }

    public class Appointment_BasicDetail
    {
        public string ShiftCloseTime { get; set; }
        public string ShiftStartTime { get; set; }
        public string ShiftType { get; set; }

        public string Id { get; set; }
        public string Consultation_Fee { get; set; }
        public string Day_Text { get; set; }
        public string Day_Id { get; set; }
        public string StartTime { get; set; }
        public string CloseTime { get; set; }
        public string Time_Interval { get; set; }
        public string NoOf_Bookings { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string CreatedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

    }

    public class AppointmentShopDetails
    {
        public string Id { get; set; }
        public string ColumnType { get; set; }
        public string CategoryId { get; set; }
        public string SubCategoryId { get; set; }
        public string RegId { get; set; }
        public string MobileNo { get; set; }
        public string Value { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string CreatedDate { get; set; }


    }

    public class DownloadAppointmentShopDetails
    {
        public string Id { get; set; }
        public string CategoryId { get; set; }
        public string SubCategoryId { get; set; }
        public string RegId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Column_1 { get; set; }
        public string Column_2 { get; set; }
        public string Column_3 { get; set; }
        public string Column_4 { get; set; }
        public string Column_5 { get; set; }
        public string Column_6 { get; set; }
        public string Column_7 { get; set; }
        public string Column_8 { get; set; }
        public string Column_9 { get; set; }
        public string Column_10 { get; set; }
        public string Column_11 { get; set; }
        public string Column_12 { get; set; }
        public string Column_13 { get; set; }
        public string Column_14 { get; set; }
        public string Column_15 { get; set; }
        public string Column_16 { get; set; }
        public string Column_17 { get; set; }
        public string Column_18 { get; set; }
        public string Column_19 { get; set; }
        public string Column_20 { get; set; }
        public string Column_21 { get; set; }
        public string Column_22 { get; set; }
        public string Column_23 { get; set; }
        public string Column_24 { get; set; }
        public string Column_25 { get; set; }
        public string Column_26 { get; set; }
        public string Column_27 { get; set; }
        public string Column_28 { get; set; }
        public string Column_29 { get; set; }
        public string Column_30 { get; set; }
        public string Column_31 { get; set; }
        public string Column_32 { get; set; }
        public string Column_33 { get; set; }
        public string Column_34 { get; set; }
        public string Column_35 { get; set; }
        public string Column_36 { get; set; }
        public string Column_37 { get; set; }
        public string Column_38 { get; set; }
        public string Column_39 { get; set; }
        public string Column_40 { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }



    public class UploadBankDetails
    {
        public string UPI_ID { get; set; }
        public string Id { get; set; }
        public string Reg_Id { get; set; }
        public string Bank_Name { get; set; }
        public string AcoountHolder_Name { get; set; }
        public string Account_No { get; set; }
        public string IFSCCode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }

    }



    public class UploadAppointmentBooking
    {
        public string Reason { get; set; }

        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string AppointmentBookedFor { get; set; }
        public string BookedForMobile { get; set; }
        public string TimeSlotNumber { get; set; }
        public string Id { get; set; }
        public string RegId { get; set; }
        public string Basic_Id { get; set; }
        public string TimeSlot_Id { get; set; }
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string Email_Id { get; set; }
        public string Gender { get; set; }
        public string StartTime { get; set; }
        public string Shift { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string ShopName { get; set; }
        public string ShopMob_No { get; set; }
        public string ShopAddress { get; set; }
        public string CloseTime { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
    }


    public class UploadTimeSlot
    {

        public string Special_Note { get; set; }
        public string No_Of_BookingsDone { get; set; }
        public string Bookings { get; set; }
        public string Id { get; set; }
        public string RegId { get; set; }
        public string Basic_TableID { get; set; }
        public string Day_Id { get; set; }
        public string Shift { get; set; }
        public string StartTime { get; set; }
        public string CloseTime { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string TimeSlotNumber { get; set; }

    }



    public class AppointmentBooking
    {
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public string IsAttended { get; set; }
        public string Reason { get; set; }
        public string AlternateMobile { get; set; }
        public string Id { get; set; }
        public string RegId { get; set; }
        public string Basic_Id { get; set; }
        public string TimeSlot_Id { get; set; }
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string Email_Id { get; set; }
        public string Gender { get; set; }
        public string StartTime { get; set; }
        public string Shift { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string ShopName { get; set; }
        public string ShopMob_No { get; set; }
        public string ShopAddress { get; set; }
        public string CloseTime { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string TimeSlotNumber { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string BookedForName { get; set; }
        public string BookedForMobile { get; set; }
    }

    public class DownloadReviewDetails
    {
        public string Name { get; set; }

        public string Average { get; set; }
        public string Id { get; set; }
        public string Registration_Id { get; set; }
        public string Order_Id { get; set; }
        public string UserMobile_No { get; set; }
        public string ShopMobile_No { get; set; }
        public string Experience_Rating { get; set; }
        public string Service_Rating { get; set; }
        public string MaterialQuality_Rating { get; set; }
        public string RateOfMaterial_Rating { get; set; }
        public string Behaviour_Rating { get; set; }
        public string Suggestions { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }


    public class DownloadReviewRate
    {
        public string Id { get; set; }

        public string TotalReviews { get; set; }
        public string Average_of_Average { get; set; }
        public string no_of_rating_1 { get; set; }
        public string no_of_rating_2 { get; set; }
        public string no_of_rating_3 { get; set; }
        public string no_of_rating_4 { get; set; }
        public string no_of_rating_5 { get; set; }


        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class DownloadAppointmentBooking
    {
        public string Id { get; set; }
        public string RegId { get; set; }
        public string Basic_Id { get; set; }
        public string TimeSlot_Id { get; set; }
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string Email_Id { get; set; }
        public string Gender { get; set; }
        public string StartTime { get; set; }
        public string CloseTime { get; set; }
        public string Shift { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string ShopName { get; set; }
        public string ShopMob_No { get; set; }
        public string ShopAddress { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string TimeSlotNumber { get; set; }
        public string AppointmentBookedFor { get; set; }
        public string BookedForMobile { get; set; }
        public string Reason { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }



    public class UpdateAppointmentBooking
    {
        public string Id { get; set; }
        public string IsAttended { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string Type { get; set; }
        public string UserName { get; set; }
        public string ShopMob { get; set; }
    }

    public class DownloadAppointmentBookingStatus
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }

    public class QuotationItemParameter

    {
        public string GlobalItem_Id { get; set; }

        public string Status { get; set; }
        public string Quotation_ItemName { get; set; }
        public string Message { get; set; }
        public string CreatedDate { get; set; }
        public string Id { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string Quotation_Basic_Id { get; set; }
        public string Quotation_Item_Id { get; set; }
        public string Is_Global { get; set; }
        public string Brand_Name { get; set; }
        public string Per_Unit_Rate { get; set; }
        public string Quantity { get; set; }
        public string Unit { get; set; }
        public string Sub_Total { get; set; }
        public string Delivery_Charges { get; set; }
        public string Other_Charges { get; set; }
        public string Shop_Name { get; set; }
        public string Shop_Address { get; set; }
        public string Shop_Mob { get; set; }
        public string Shop_Reg_Id { get; set; }
        public string CreatedBy { get; set; }

    }

    public class UploadGroupFields
    {
        public string Id { get; set; }
        public string GroupName { get; set; }
        public string GroupType { get; set; }
        public string ParentGrp_Id { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string RegMob { get; set; }
        public string IMEI { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public object Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string UpdatedId { get; set; }
    }




    public class UploadAppointmentAddQuestion
    {

        public string QuestionType { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string Question { get; set; }
        public string Type { get; set; }
        public string Mandatory { get; set; }
        public string CreatedBy { get; set; }
        public string Id { get; set; }
        public string CreatedDate { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }

    }
    public class UploadSharedContactsParameter
    {

        public string Number { get; set; }
        public string F_Name { get; set; }
        public string L_Name { get; set; }
        public string Share_with { get; set; }
        public string Added_to_referal { get; set; }
        public string Status { get; set; }
        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }

    }


    public class DownloadLedger
    {
        public string Id { get; set; }
        public string Group_Id { get; set; }
        public string Parent_Group_Id { get; set; }
        public string Address { get; set; }
        public string Legder_Acc_No { get; set; }
        public string GST_No { get; set; }
        public string Opening_Balance { get; set; }
        public string Closing_Balance { get; set; }
        public string Institute_Head_MobNo { get; set; }
        public string UDISE_Code { get; set; }
        public string Institute_Code { get; set; }
        public string Open_Type { get; set; }
        public string Close_Type { get; set; }
        public string IMEI { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public string Created_Date { get; set; }
        public string Modify_By { get; set; }
        public string Modify_Date { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string ShopType { get; set; }
        public string Reg_Id { get; set; }
        public string RegMob { get; set; }
        public string PhoneNumber { get; set; }
    }



    public class UploadLegder
    {
        public string Id { get; set; }
        public string Group_Id { get; set; }
        public string PhoneNumber { get; set; }
        public string Parent_Group_Id { get; set; }
        public string Address { get; set; }
        public string Legder_Acc_No { get; set; }
        public string GST_No { get; set; }
        public string Opening_Balance { get; set; }
        public string Closing_Balance { get; set; }
        public string Open_Type { get; set; }
        public string Close_Type { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string RegMob { get; set; }
        public string IMEI { get; set; }
        public string Status { get; set; }
        public string Created_By { get; set; }
        public string Updated_Id { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }

    }
    public class DownloadGroupField
    {
        public string Id { get; set; }
        public string GroupType { get; set; }
        public string GroupName { get; set; }
        public string ParentGrp_Id { get; set; }

        public string ShopType { get; set; }
        public string Reg_Id { get; set; }
        public string RegMob { get; set; }
        public string IMEI { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string GroupId { get; set; }
    }


    public class TransactionDetailsParameter
    {
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string UpdatedId { get; set; }
        public string Id { get; set; }
        public string Voucher_Id { get; set; }
        public string Ledger_Id { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_Details { get; set; }
        public string Group_Id { get; set; }
        public string Voucher_Type { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string RegMob { get; set; }
        public string Created_By { get; set; }
        public string IMEI { get; set; }
        public string Payment_By { get; set; }
        public string Payment_Id { get; set; }
        public string Cheque_no { get; set; }
        public string Cheque_Date { get; set; }
        public string Bank_Details { get; set; }
        public string Ledger_DepositedID { get; set; }
        public string Local_VoucherId { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string Total { get; set; }
        public string Transaction_Type { get; set; }
        public string IH_Mobile { get; set; }
    }



    public class VoucherDetailsParameter
    {
        public string LocalId { get; set; }
        public string Id { get; set; }
        public string Voucher_Date { get; set; }
        public string Voucher_Amount { get; set; }
        public string Voucher_Type { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string RegMob { get; set; }
        public string Created_By { get; set; }
        public string IMEI { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Created_Date { get; set; }
        public string Modified_By { get; set; }
        public string Modify_Date { get; set; }
        public string UpdatedId { get; set; }
    }




    public class Voucher
    {
        public List<DownloadTransactionAndVoucherDetailsNew> VoucherResult { get; set; }
        public string Id { get; set; }
        public string Voucher_Date { get; set; }
        public string Voucher_Amount { get; set; }
        public string Voucher_Type { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string RegMob { get; set; }
        public string Created_By { get; set; }
        public string Created_Date { get; set; }
        public string Modified_By { get; set; }
        public string Modify_Date { get; set; }
        public string IMEI { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }



    public class DownloadTransactionAndVoucherDetailsNew
    {
        public string Error { get; set; }
        public string NoData { get; set; }
        public int Id { get; set; }
        public string Voucher_Id { get; set; }
        public string Ledger_Id { get; set; }
        public string Transaction_Date { get; set; }
        public string Transaction_Details { get; set; }
        public string Group_Id { get; set; }
        public string Voucher_Type { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string ShopType { get; set; }
        public string RegId { get; set; }
        public string RegMob { get; set; }
        public string IMEI { get; set; }
        public string Payment_By { get; set; }
        public string Payment_Id { get; set; }
        public string Cheque_no { get; set; }
        public string Cheque_Date { get; set; }
        public string Bank_Details { get; set; }
        public string Ledger_DepositedID { get; set; }
        public string Local_VoucherId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string Total { get; set; }
        public string Transaction_Type { get; set; }
    }


    public class UploadVoucherBlank
    {
        public string ServerId { get; set; }
        public string Error { get; set; }
    }

    public class UploadAddStaff
    {
        public string OfficeId { get; set; }
        public string DeptId { get; set; }

        public string Id { get; set; }
        public string FullName { get; set; }
        public string MobileNo { get; set; }
        public string UserRole { get; set; }
        public string AddUnder { get; set; }
        public string PaymentType { get; set; }
        public string RegId { get; set; }
        public string ShopType { get; set; }
        public string Salary { get; set; }
        public string SalaryCycle { get; set; }
        public string AmountType { get; set; }
        public string Amount { get; set; }
        public string JoiningDate { get; set; }
        public string Status { get; set; }
        public string Shop_OwnweNo { get; set; }
        public string Shift { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string IMEI { get; set; }

    }
    public class UploadAddAttendance
    {
        public string Amount { get; set; }
        public string ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public string OfficeName { get; set; }
        public string OfficeId { get; set; }
        public string DeptName { get; set; }
        public string DeptId { get; set; }
        public string Shift { get; set; }
        public string Id { get; set; }
        public string Emp_ServerId { get; set; }
        public string Emp_Name { get; set; }
        public string Emp_MobileNo { get; set; }
        public string Attendance { get; set; }
        public string AbsentType { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
        public string OverTime { get; set; }
        public string LateFine { get; set; }
        public string Note { get; set; }
        public string HoursAttended { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string IMEI { get; set; }
        public string OverTime_Amont { get; set; }
    }

    public class UploadStaffDetails
    {

        public string PerDayType { get; set; }
        public string ChatId { get; set; }
        public string NoOfWorkingDays { get; set; }
        public string PerDay { get; set; }
        public string Permissions { get; set; }
        public string TA { get; set; }
        public string PetrolAllowance { get; set; }
        public string EPF { get; set; }
        public string ESIC { get; set; }
        public string Id { get; set; }
        public string Emp_ServerId { get; set; }
        public string Basic_Pay { get; set; }
        public string Joining_date { get; set; }
        public string Shift_hours { get; set; }
        public string Weekly_off { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string WifiAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

    public class AddLoan
    {
        public string Id { get; set; }
        public string Emp_ServerId { get; set; }
        public string Loan_Amount { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string SMS_Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class AddPayment
    {
        // public string ErverId { get; set; }
        public string IMEI { get; set; }
        public string Id { get; set; }
        public string Emp_ServerId { get; set; }
        public string PaidType { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Month { get; set; }
        public string SMS_Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class AddBonus
    {
        public string Id { get; set; }
        public string Emp_ServerId { get; set; }
        public string BonusAmount { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string SMS_Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }

    public class UploadNewsPaper
    {
        public string IMEI { get; set; }
        public string Id { get; set; }
        public string NewsPaperID { get; set; }
        public string Name { get; set; }
        public string Day { get; set; }
        public string CoverPrice { get; set; }
        public string ShortName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class RegisterNewsPaper
    {
        public string Id { get; set; }
        public string Paper_Name { get; set; }
        public string RegionalName { get; set; }
        public string Language { get; set; }
        public string Paper_Origin { get; set; }
        public string Frequency { get; set; }
        public string AppKeyword { get; set; }
        public string Website { get; set; }
        public string Head_OfcAddress { get; set; }
        public string MobileNo { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactMobileNo { get; set; }
        public string CompanyName { get; set; }
        public string District { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }

    }


    public class PaperRequest
    {

        public string HeadQuarter { get; set; }
        public string Website { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Paper_Language { get; set; }
        public string State { get; set; }
        public string Paper_Frequency { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

    }


    public class BuildingDetails
    {

        public string UpdateId { get; set; }
        public string IMEI { get; set; }
        public string Id { get; set; }
        public string BuildingName { get; set; }
        public string BuildingWing { get; set; }
        public string Locality { get; set; }
        public string Street { get; set; }
        public string Pincode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class CustomerDetails
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string WifiRouter { get; set; }

        public string IMEI { get; set; }
        public string Building_Id { get; set; }
        public string PinCode { get; set; }
        public string Street { get; set; }
        public string Id { get; set; }
        public string HouseShop_Number { get; set; }
        public string House_Name { get; set; }
        public string Institute_Type { get; set; }
        public string Customer_Mobile { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Type { get; set; }
        public string Customer_Email { get; set; }
        public string Customer_Address { get; set; }
        public string Deposite { get; set; }
        public string Delivery_Charge { get; set; }
        public string Collection_Type { get; set; }
        public string Collection_Amount { get; set; }
        public string House_Type { get; set; }
        public string Send_Sms { get; set; }
        public string Send_Email { get; set; }
        public string Send_Whatsapp { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }

    public class UploadAddCustomerPaper
    {
        public string IMEI { get; set; }
        public string Id { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Id { get; set; }
        public string Paper_Name { get; set; }
        public string Paper_Id { get; set; }
        public string Payment_Mode { get; set; }
        public string Delivery_Frequency { get; set; }
        public string Day { get; set; }
        public string Price { get; set; }
        public string Quantity { get; set; }
        public string Bill_StartDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }

    public class UploadAddCoupon
    {
        public string Id { get; set; }
        public string PaperID { get; set; }
        public string PaperName { get; set; }
        public string CouponScheme_Name { get; set; }
        public string CouponCust_Price { get; set; }
        public string CouponMy_Price { get; set; }
        public string Validity { get; set; }
        public string Discount_Percentage { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class UploadAddHoliday
    {
        public string Id { get; set; }
        public string IMEI { get; set; }
        public string Holiday_Type { get; set; }
        public string Paper_Name { get; set; }
        public string Paper_Id { get; set; }
        public string Reason { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class UploadMyStock
    {
        public string Id { get; set; }
        public string IMEI { get; set; }
        public string PaperName { get; set; }
        public string PaperId { get; set; }
        public string Quantity { get; set; }
        public string Balance_Quantity { get; set; }
        public string PaperRate { get; set; }
        public string Date { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class UploadCustomerBillDetails
    {

        public string BillType { get; set; }
        public string ReceivedAmt { get; set; }
        public string BalanceAmt { get; set; }

        public string Id { get; set; }
        public string IMEI { get; set; }
        public string CustomerID { get; set; }
        public string DiscountID { get; set; }
        public string Date { get; set; }
        public string Total_Amount { get; set; }
        public string Total_Discount { get; set; }
        public string FinalBill_Amount { get; set; }
        public string Total_Items { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string CustBill_ServerId { get; set; }
        public string CustBill_LocalId { get; set; }
        public List<PaperWiseBillDetails> PaperBill_Details { get; set; }
    }
    public class PaperWiseBillDetails
    {
        public string Id { get; set; }
        public string PaperID { get; set; }
        public string Quantity { get; set; }
        public string PaperRate { get; set; }
        public string Amount { get; set; }
        public string PaperDiscount { get; set; }
        public string CustBill_Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string PaperBill_ServerId { get; set; }
        public string PaperBill_LocalId { get; set; }
    }
    public class UploadAddCouponNew
    {
        public string IMEI { get; set; }
        public string Id { get; set; }
        public string CouponScheme_Name { get; set; }
        public string NoOf_Item { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string Coupon_ServerId { get; set; }
        public string Coupon_LocalId { get; set; }
        public List<UploadAddCouponPaper> CouponPaperDetails { get; set; }
    }
    public class UploadAddCouponPaper
    {
        public string Id { get; set; }
        public string PaperID { get; set; }
        public string PaperName { get; set; }
        public string Coupon_CustPrice { get; set; }
        public string Coupon_MyPrice { get; set; }
        public string Validity { get; set; }
        public string Discount_Percentage { get; set; }
        public string Coupon_ServerID { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string CouponPaper_ServerId { get; set; }
        public string CouponPaper_LocalId { get; set; }
    }


    public class CustomerPaymentDetails
    {

        public string Id { get; set; }
        public string Date { get; set; }
        public string Cust_MobNo { get; set; }
        public string Cust_Name { get; set; }
        public string Cust_Id { get; set; }
        public string Received_Amt { get; set; }
        public string Pending_Amt { get; set; }
        public string Bill_No { get; set; }
        public string PaymentType { get; set; }
        public string ChequeNumber { get; set; }
        public string ChequeDate { get; set; }
        public string Remark { get; set; }
        public string IMEI { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }



    public class DownloadNeedlyRegData
    {
        public string EzeeDrugAppId { get; set; }
        public string keyword { get; set; }
        public string strDevId { get; set; }
        public string strSimSerialNo { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string firmName { get; set; }
        public string mobileNo { get; set; }
        public string address { get; set; }
        public string eMailId { get; set; }
        public string typeOfUse_Id { get; set; }
        public string pincode { get; set; }
        public string passcode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string EntryDate { get; set; }
        public string UserId { get; set; }
        public string RefMobileNo { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string usertype { get; set; }
        public string Qualification { get; set; }
        public string LadlineNo { get; set; }
        public string Favorite { get; set; }
        public string Village { get; set; }
        public string Spatialization { get; set; }
        public string Taluka { get; set; }
        public string passcodelimit { get; set; }
        public string GCM_Regid { get; set; }
        public string OTP { get; set; }
        public string DealerMobNo { get; set; }
        public string RoleID { get; set; }
        public string UdiseCode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Gender { get; set; }
        public string DynamicURL { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }

    public class UploadCategoryMasterParameter
    {
        public string CreatedDate { get; set; }
        public string NoData { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
        public string ShopType { get; set; }
        public string ServerId { get; set; }
        public string Id { get; set; }
        public string Error { get; set; }
        public object NoDate { get; set; }
        public string MobileNo { get; set; }
        public string LocalId { get; set; }
    }

    public class UploadJobDetailsParameter
    {
        public string JobType { get; set; }
        public string JobCategory { get; set; }
        public string Status { get; set; }
        public string JobTitle { get; set; }
        public string JobDescription { get; set; }

        public string Salary { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public string WhatsappNumber { get; set; }
        public string BusinessName { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        public string State { get; set; }
        public string Dist { get; set; }
        public string Taluka { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Id { get; set; }
        public string Error { get; set; }
        public object NoDate { get; set; }

        public string LocalId { get; set; }
    }


    public class UploadJobApplicationDetailsParameter
    {

        public string JobId { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationNumber { get; set; }
        public string PersonalDetails { get; set; }
        public string Experience { get; set; }

        public string ShortDescription { get; set; }
        public string Qualification { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Id { get; set; }
        public string Error { get; set; }
        public object NoDate { get; set; }

        public string LocalId { get; set; }
        public string FilePath { get; set; }
    }

    public class UploadTemplateParameter
    {

        public string TemplateType { get; set; }
        public string CategoryId { get; set; }
        public string BasePrice { get; set; }
        public string OfferPrice { get; set; }
        public string ImageType { get; set; }
        public string Id { get; set; }
        public string ShopType { get; set; }
        public string TemplateName { get; set; }
        public string BackgroundImage { get; set; }
        public string IMEI { get; set; }
        public string AppVersion { get; set; }
        public string CreatedBy { get; set; }

        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string Parent_ServerId { get; set; }
        public string Parent_LocalId { get; set; }
        public List<UploadTemplateFieldParameter> TemplateFieldDetails { get; set; }
    }


    public class UploadTemplateFieldParameter
    {
        public string ShopkeeperEmail { get; set; }
        public string CategoryId { get; set; }
        public string ShopkeeperName { get; set; }

        public string Id { get; set; }
        public string TemplateId { get; set; }
        public string InputType { get; set; }
        public string X_Coordinate { get; set; }
        public string Y_Coordinate { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
        public string Text_Size { get; set; }
        public string Red { get; set; }
        public string Green { get; set; }
        public string Blue { get; set; }
        public string AppVersion { get; set; }
        public string IMEI { get; set; }
        public string EnableStatus { get; set; }

        public string CreatedBy { get; set; }

        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

    }

    public class UpdateRefMobileNoParameter
    {
        public string Error { get; set; }
        public string NoData { get; set; }
        public string Status { get; set; }
        public string RefMobile { get; set; }
        public string Mobile { get; set; }
    }

    public class UpdateAddItemParameter
    {

        public string Unit { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Id { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string IMEI { get; set; }
        public string HSN_SAC_NO { get; set; }
        public string ItemRate { get; set; }
    }


    public class UploadAssignedParameter
    {

        public string ClientId { get; set; }
        public string DeptId { get; set; }
        public string PostId { get; set; }
        public string StaffId { get; set; }

        public string Id { get; set; }
        public string AppVersion { get; set; }
        public string ReatedDate { get; set; }
        public string IMEI { get; set; }
        public string ClientName { get; set; }
        public string DeptName { get; set; }
        public string Post { get; set; }
        public string StaffName { get; set; }
        public string Shift { get; set; }
        public string Posting { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        //  public string MobileNo { get; set; }
        public string LocalId { get; set; }

        public string ServerId { get; set; }
        public string Error { get; set; }
        // public string Id { get; set; }
        public string Status { get; set; }
        public string NoData { get; set; }
        //  public string CreatedDate { get; set; }
    }

    public class UploadAllotManpowerParameter
    {

        public string ClientPerDayRate { get; set; }
        public string EPFPer { get; set; }
        public string EPFAmt { get; set; }
        public string EsicPer { get; set; }
        public string EsicAmt { get; set; }
        public string ServiceChargePer { get; set; }
        public string ServiceChargeAmt { get; set; }
        public string GSTPer { get; set; }
        public string NoOfWorkingDays { get; set; }
        public string ClientId { get; set; }
        public string DeptId { get; set; }
        public string PostId { get; set; }

        public string Post { get; set; }
        public string Id { get; set; }
        public string AppVersion { get; set; }
        public string HsnOrSac { get; set; }
        public string IMEI { get; set; }
        public string ClientName { get; set; }
        public string DeptName { get; set; }
        public string MonthlyRate { get; set; }
        public string GST { get; set; }
        public string FinalRate { get; set; }
        public string ShiftInHr { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ManpowerInHr { get; set; }
        public string StaffSalaryLimit { get; set; }
        public string LocalId { get; set; }

        public string ServerId { get; set; }
        public string Error { get; set; }
        public string RequiredManpower { get; set; }
        public string Status { get; set; }
        public string NoData { get; set; }
        public string SubstituteWadges { get; set; }
        public string DayType { get; set; }
    }

    public class UploadStaffProfileDetailsParameter
    {
        // public string ServerId { get; set; }
        public string Id { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string LocalId { get; set; }

        public string AadharNo { get; set; }
        public string DOB { get; set; }
        public string Gender { get; set; }
        public string EducationLevel { get; set; }
        public string Qualification { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string Chaist { get; set; }
        public string ShoeSize { get; set; }
        public string Waist { get; set; }
        public string MaritalStatus { get; set; }
        public string NomineeName { get; set; }
        public string RelatedWithNominee { get; set; }
        public string PermanentAdd { get; set; }
        public string PerState { get; set; }
        public string PerDist { get; set; }
        public string PerTaluka { get; set; }
        public string LocalAdd { get; set; }
        public string LocalState { get; set; }
        public string LocalDistrict { get; set; }
        public string LocalTaluka { get; set; }
        public string IntroducerName { get; set; }
        public string IntroducerAdd { get; set; }
        public string IntroducerState { get; set; }
        public string IntroducerDistrict { get; set; }
        public string IntroducerTaluka { get; set; }
        public string EmergencyContactNo { get; set; }
        public string EmergencyFamilyNm { get; set; }
        public string EPFNo { get; set; }
        public string PANNo { get; set; }
        public string ESICNo { get; set; }
        public string PPFNo { get; set; }

        public string ServerId { get; set; }
        public string Error { get; set; }

        public string Status { get; set; }
        public string NoData { get; set; }

    }

    public class UploadAddSubstituteDetailsParameter
    {

        public string WadgeAmount { get; set; }
        public string Id { get; set; }
        public string StaffId { get; set; }
        public string StaffName { get; set; }
        public string SubStaffId { get; set; }
        public string SubStaffName { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string Date { get; set; }
        public string OfficeId { get; set; }
        public string DeptId { get; set; }
        public string Shift { get; set; }
        public string UserMobile { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string IMEI { get; set; }

        public string LocalId { get; set; }

        public string ServerId { get; set; }
        public string Error { get; set; }
        // public string Id { get; set; }
        public string Status { get; set; }
        public string NoData { get; set; }
    }

    //   public class UpdateWorkDetailsParameter
    //   {

    //       public string ServerId { get; set; }
    //       public string LocalId { get; set; }
    //       public string Id { get; set; }

    //              public string FinalAmount { get; set; }
    //       public string CreatedBy { get; set; }
    //       public string CreatedDate { get; set; }
    //       public string Error { get; set; }
    //       public string NoData { get; set; }


    //public string StaffName { get; set; }
    //       public string StaffId { get; set; }
    //       public string ItemName { get; set; }
    //       public string ItemId { get; set; }
    //       public string ItemRate { get; set; }
    //       public string Description { get; set; }
    //       public string Quantity { get; set; }
    //       public string Date { get; set; }
    //       public string Shift { get; set; }
    //       public string IMEI { get; set; }
    //   }

    public class UpdateWorkDetailsParameter
    {

        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Id { get; set; }

        public string FinalAmount { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }


        public string StaffName { get; set; }
        public string StaffId { get; set; }
        public string ItemName { get; set; }
        public string ItemId { get; set; }
        public string ItemRate { get; set; }
        public string Description { get; set; }
        public string Quantity { get; set; }
        public string Date { get; set; }
        public string Shift { get; set; }
        public string IMEI { get; set; }
        public string CretaedBy { get; set; }
        public string CretaedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }

    }
    public class UploadExtraStaff
    {
        public string Id { get; set; }
        public string StaffId { get; set; }
        public string StaffName { get; set; }
        public string Shift { get; set; }
        public string Date { get; set; }
        public string UserMobNo { get; set; }
        public string ServerId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string modifiedDate { get; set; }
        public string Imei { get; set; }
        public string IsUpdated { get; set; }
        public string Presenty { get; set; }
        public string OfficeId { get; set; }
        public string DepartmentId { get; set; }
        public string Reason { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

    }

    public class ParameterEmployeeDetails
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeMobile { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string IMEI { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }


    public class ParameterAttendanceDetails
    {
        public string Id { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeServerId { get; set; }
        public string EmployeeMobileNumber { get; set; }
        public string Attendance { get; set; }
        public string AbsentType { get; set; }
        public string Date { get; set; }
        public string Type { get; set; }
        public string OverTime { get; set; }
        public string LateFine { get; set; }
        public string Note { get; set; }
        public string HourseAttended { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ServerId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string IsUpdated { get; set; }
        public string OverTimeAmount { get; set; }
        public string OfficeName { get; set; }
        public string OfficeId { get; set; }
        public string DeptName { get; set; }
        public string DeptId { get; set; }
        public string Shift { get; set; }
        public string Amount { get; set; }
        public string Latitute { get; set; }
        public string Longitute { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }
    public class ClientPaymentHistoryParameter
    {

        public string ServerId { get; set; }
        public string Id { get; set; }
        public string IMEI { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string BillNo { get; set; }
        public string ReceivedAmount { get; set; }
        public string PaymentMode { get; set; }
        public string ChequeNumber { get; set; }
        public string ChequeDate { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string Date { get; set; }

        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }


    public class UploadPetrolReadingParameter
    {

        public string StaffName { get; set; }
        public string StaffId { get; set; }
        public string ItemName { get; set; }
        public string ItemId { get; set; }
        public string ItemRate { get; set; }
        public string Description { get; set; }
        public string Quantity { get; set; }
        public string Date { get; set; }
        public string ReadingType { get; set; }
        public string ServerId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string Id { get; set; }


        public string IsUpdated { get; set; }
        public string CapturedReading { get; set; }
        public string ManuallyReading { get; set; }
        public string Error { get; set; }
        public string Correct { get; set; }

        public string NoData { get; set; }

        public string LocalId { get; set; }
    }

    //11/03/2021
    public class MaterialIssuedParameter
    {

        public string Amount { get; set; }
        public string DeductionForSalary { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string IssueidType_Id { get; set; }
        public string IssueidType_Name { get; set; }
        public string MaterialType_Id { get; set; }
        public string MaterialType_Name { get; set; }
        public string Staff_Id { get; set; }
        public string Staff_Name { get; set; }
        public string MaterialFor { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Id { get; set; }
        public string Server_Id { get; set; }

        public string NoData { get; set; }

        public string LocalId { get; set; }
        public string Error { get; set; }
    }


    public class DownloadDateWiseParameter
    {
        [DataMember]
        public string LocalBodyId { get; set; }
        [DataMember]
        public string cntSeatId { get; set; }
        [DataMember]

        public string NoOfWrd { get; set; }
        [DataMember]
        public string FCnt { get; set; }

        [DataMember]
        public string MICnt { get; set; }
        [DataMember]
        public string ElectionId { get; set; }
        [DataMember]
        public string LBID { get; set; }
        [DataMember]
        public string dy { get; set; }
        [DataMember]
        public string Date { get; set; }
        [DataMember]
        public string NominationFormFillingST { get; set; }
        [DataMember]
        public string NominationFormFillingET { get; set; }

        [DataMember]
        public string NoData { get; set; }
        [DataMember]
        public string Error { get; set; }
        public string LBDistrictID { get; set; }
        public string LBType { get; set; }

    }

    public class CustomerContactDetailsParameter
    {

        public string Id { get; set; }
        public string MobileNo { get; set; }
        public string GroupId { get; set; }
        public string Description { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Status { get; set; }

        public string Server_Id { get; set; }

        public string NoData { get; set; }

        public string LocalId { get; set; }
        public string Error { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public string Field11 { get; set; }
        public string Field12 { get; set; }
        public string Field13 { get; set; }
        public string Field14 { get; set; }
        public string Field15 { get; set; }
        public string Category { get; set; }
        public string Prefix { get; set; }
        public string Email { get; set; }
        public string SrNo { get; set; }
        public string ServerId { get; set; }
        public string UpdatedGroupId { get; set; }
       
    }
    
    //25/03/2021

    public class WhatsappCountParameter
    {
        public string Id { get; set; }
        public string Templateshare_count { get; set; }
        public string AutoReplyKeyword { get; set; }
        public string NeedlyMembersGroup { get; set; }
        public string CreatedBy { get; set; }
        public string Mediacount { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string MessageCount { get; set; }
                public string Status { get; set; }
        public string PreviousMsgCount { get; set; }
        public string Server_Id { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }



        public int JAN_COUNT { get; set; }
        public int FEB_COUNT { get; set; }
        public int MARCH_COUNT { get; set; }
        public int APRIL_COUNT { get; set; }
        public int MAY_COUNT { get; set; }
        public int JUNE_COUNT { get; set; }
        public int JULY_COUNT { get; set; }
        public int AUG_COUNT { get; set; }
        public int SEPT_COUNT { get; set; }
        public int OCT_COUNT { get; set; }
        public int NOV_COUNT { get; set; }
        public int DEC_COUNT { get; set; }

        public int JAN_MEDIA_COUNT { get; set; }
        public int FEB_MEDIA_COUNT { get; set; }
        public int MARCH_MEDIA_COUNT { get; set; }
        public int APRIL_MEDIA_COUNT { get; set; }
        public int MAY_MEDIA_COUNT { get; set; }
        public int JUNE_MEDIA_COUNT { get; set; }
        public int JULY_MEDIA_COUNT { get; set; }
        public int AUG_MEDIA_COUNT { get; set; }
        public int SEPT_MEDIA_COUNT { get; set; }
        public int OCT_MEDIA_COUNT { get; set; }
        public int NOV_MEDIA_COUNT { get; set; }
        public int DEC_MEDIA_COUNT { get; set; }
       
        public int Auto_Reply_wa_used { get; set; }
        public int Auto_Reply_Tel_Used { get; set; }
        public int Needly_Group_Of_Whatsapp_Group { get; set; }
        public int Needly_Project { get; set; }
        public int Joining_Needly_Project { get; set; }
        public int Needly_WhatsApp_Forms_Project { get; set; }
        public int True_Voter { get; set; }
        public int Schedular_With_Normal_Text_Message { get; set; }
        public int Schedular_With_Media_File { get; set; }

        public int truevoter_status_update { get; set; }
        public int truevoter_message { get; set; }
        public int Needly_Form_To_Server { get; set; }
        public int Whatsapp_Group_Search { get; set; }
        public int Whatsapp_Not_Available { get; set; }
        public int Order_Appointment { get; set; }



    }
    //26/03/2021
    public class WhatsAppTemplateParameter
    {

        public string Name { get; set; }
        public string CreatedBy { get; set; }

       
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string TemplateContent { get; set; }
        public string TemplateImage { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string Id { get; set; }
        public string NoData { get; set; }
        public string TemplateType { get; set; }
        public string Status { get; set; }


    }
    //09/04/2021
    public class CompanyDetailsParameter
    {

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string MobileNo { get; set; }
        public string AlternateMobileNo { get; set; }
        public string WhatsappMobileNo { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string MobileApp { get; set; }
        
        public string BusinessLocation { get; set; }
        public string BusinessName { get; set; }
        public string ProfileUrl { get; set; }
        public string Theme { get; set; }
        public string AboutCompany { get; set; }
        public string Pincode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string WebSiteFor { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }

    }
    public class SocialLinksDetailsParameter
    {
        public string Id { get; set; }
        public string FacebookLink { get; set; }
        public string TwitterLink { get; set; }
        public string InstagramLink { get; set; }
        public string LinkedinLink { get; set; }
        public string YoutubeLink { get; set; }
        public string PinterestLink { get; set; }
        public string YoutubeVideoLink1 { get; set; }
        public string YoutubeVideoLink2 { get; set; }
        public string YoutubeVideoLink3 { get; set; }
        public string YoutubeVideoLink4 { get; set; }
        public string YoutubeVideoLink5 { get; set; }
        public string YoutubeVideoLink6 { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string Imei { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class PhoneContactsDetailsParameter
    {

        public string Id { get; set; }
        public string MobileNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ReferenceMobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public string Pincode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string WhatsappStatus { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
    }
    public class PaymentOptionParameter
    {
        public string Id { get; set; }
        public string PaytmNo { get; set; }
        public string GooglePayNo { get; set; }
        public string PhonePeNo { get; set; }
        public string BankName { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public string IfscCode { get; set; }
        public string GstinNo { get; set; }
        public string PaytmQR { get; set; }
        public string GooglePayQR { get; set; }
        public string PhonePeQR { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

        public string NoData { get; set; }
        public string ImageType { get; set; }
        public string Image { get; set; }
        public string QRLink { get; set; }
        public string PhonepeQR_Url { get; set; }
        public string GooglepayQR_Url { get; set; }
        public string PaytmQR_url { get; set; }
        public string ImagePhonePay { get; set; }
        public string ImageGpay { get; set; }
        public string ImagePaytm { get; set; }

    }

    public class ProductServiceParameter
    {
        public string Id { get; set; }
        public string ImageOne { get; set; }
        public string ImageTwo { get; set; }
        public string ImageThree { get; set; }
        public string ImageFour { get; set; }
        public string ImageFive { get; set; }
        public string ImageSix { get; set; }
        public string ImageSeven { get; set; }
        public string ImageEight { get; set; }
        public string ImageNine { get; set; }
        public string ImageTen { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string ProductNameOne { get; set; }
        public string ProductNameTwo { get; set; }
        public string ProductNameThree { get; set; }
        public string ProductNameFour { get; set; }
        public string ProductNameFive { get; set; }
        public string ProductNameSix { get; set; }
        public string ProductNameSeven { get; set; }
        public string ProductNameEight { get; set; }
        public string ProductNameNine { get; set; }
        public string ProductNameTen { get; set; }
        public string DescriptionOne { get; set; }
        public string DescriptionTwo { get; set; }
        public string DescriptionThree { get; set; }
        public string DescriptionFour { get; set; }
        public string DescriptionFive { get; set; }
        public string DescriptionSix { get; set; }
        public string DescriptionSeven { get; set; }
        public string DescriptionEight { get; set; }
        public string DescriptionNine { get; set; }
        public string DescriptionTen { get; set; }
        public string Error { get; set; }
        public string ImageReturnLink { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string ImageType { get; set; }
        public string Image { get; set; }
        public string ImageTen_Url { get; set; }
        public string ImageNine_Url { get; set; }
        public string ImageEight_Url { get; set; }
        public string ImageSeven_Url { get; set; }
        public string ImageSix_Url { get; set; }
        public string ImageFive_Url { get; set; }
        public string ImageFour_Url { get; set; }
        public string ImageThree_Url { get; set; }
        public string ImageTwo_Url { get; set; }
        public string ImageOne_Url { get; set; }
        public string NoData { get; set; }
        public string Description { get; set; }
        public string ProductName { get; set; }


    }

    public class ImageGalleryParameter
    {
        public string Id { get; set; }
        public string ImageOne { get; set; }
        public string ImageTwo { get; set; }
        public string ImageThree { get; set; }
        public string ImageFour { get; set; }
        public string ImageFive { get; set; }
        public string ImageSix { get; set; }
        public string ImageSeven { get; set; }
        public string ImageEight { get; set; }
        public string ImageNine { get; set; }
        public string ImageTen { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string ProductNameOne { get; set; }
        public string ProductNameTwo { get; set; }
        public string ProductNameThree { get; set; }
        public string ProductNameFour { get; set; }
        public string ProductNameFive { get; set; }
        public string ProductNameSix { get; set; }
        public string ProductNameSeven { get; set; }
        public string ProductNameEight { get; set; }
        public string ProductNameNine { get; set; }
        public string ProductNameTen { get; set; }
        public string DescriptionOne { get; set; }
        public string DescriptionTwo { get; set; }
        public string DescriptionThree { get; set; }
        public string DescriptionFour { get; set; }
        public string DescriptionFive { get; set; }
        public string DescriptionSix { get; set; }
        public string DescriptionSeven { get; set; }
        public string DescriptionEight { get; set; }
        public string DescriptionNine { get; set; }
        public string DescriptionTen { get; set; }
        public string Error { get; set; }
        public string ImageReturnLink { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string ImageType { get; set; }
        public string Image { get; set; }
        public string ImageTen_Url { get; set; }
        public string ImageNine_Url { get; set; }
        public string ImageEight_Url { get; set; }
        public string ImageSeven_Url { get; set; }
        public string ImageSix_Url { get; set; }
        public string ImageFive_Url { get; set; }
        public string ImageFour_Url { get; set; }
        public string ImageThree_Url { get; set; }
        public string ImageTwo_Url { get; set; }
        public string ImageOne_Url { get; set; }
        public string NoData { get; set; }
        public string Description { get; set; }
        public string ProductName { get; set; }


    }

    public class ParameterGroupDetails
    {
        public string Id { get; set; }
        public string ServerId { get; set; }
        public string TypeOfGroup { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string LastIndex { get; set; }
        public string FormID { get; set; }
        public string FormName { get; set; }

        public string GroupName { get; set; }
        public string GroupDescription { get; set; }
        public string GroupStatus { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string NoData { get; set; }
    }

    public class ParameterRuleDetails
    {

        public string Id { get; set; }
        public string RuleName { get; set; }
        public string IncomingMessage { get; set; }
        public string RuleType { get; set; }
        public string ResponseMessage { get; set; }
        public string ReceiverType { get; set; }
        public string MaxDelay { get; set; }
        public string MinDelay { get; set; }
        public string WhatsappChannel { get; set; }
        public string WhatsappBusinessChannel { get; set; }
        public string TelegramChannel { get; set; }
        public string Day { get; set; }
        public string Date { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string SpecificTime { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string ProjectId { get; set; }
        public string IsLocal { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
    }
    public class ParameterKeywordDetails
    {

        public string Id { get; set; }
        public string Keyword { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string IsUpdated { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class ParameterKeywordDesription
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string KeywordId { get; set; }
        public string KeywordName { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Heading { get; set; }
        public string NoData { get; set; }
    }
    public class ParameterProjectDetails
    {
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string ProductAdminId { get; set; }
        public string ProductAdminNo { get; set; }
        public string ProductAdminName { get; set; }
        public string ProductCode { get; set; }
        public string PortalName { get; set; }
        public string ProjectType { get; set; }
        public string ProjectDomain { get; set; }
        public string _External { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Status { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }

    }
    public class ParameterProjectMember
    {

        public string Id { get; set; }
        public string ProductName { get; set; }
        public string @Description { get; set; }
        public string ProjectId { get; set; }
        public string EmailId { get; set; }
        public string MemberNo { get; set; }
        public string MemberDetails { get; set; }
        public string MemberName { get; set; }
        public string JobRole { get; set; }
        public string Status { get; set; }
        public string UpdateField { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class CardLinkParameter
    {
        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string TemplateId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ImageReturnLink { get; set; }
        public string ServerId { get; set; }
    }
    public class ParameterAlarmDetails
    {
        public string Id { get; set; }
        public string FilePath { get; set; }
        public string AlarmId { get; set; }
        public string Hours { get; set; }
        public string Minutes { get; set; }
        public string Started { get; set; }
        public string Recurring { get; set; }
        public string Monday { get; set; }
        public string Tuesday { get; set; }
        public string Wednesday { get; set; }
        public string Thursday { get; set; }
        public string Friday { get; set; }
        public string Saturday { get; set; }
        public string Sunday { get; set; }
        public string Title { get; set; }
        public string ServerId { get; set; }
        public string ChildNumber { get; set; }
        public string Status { get; set; }
        public string Contacts { get; set; }
        public string Names { get; set; }
        public string sMsg { get; set; }
        public string Date { get; set; }
        public string TemplateType { get; set; }
        public string Created { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
    }
    public class ParameterFormDetails
    {
        public string Id { get; set; }
        public string Form_Name { get; set; }
        public string Form_Id { get; set; }
        public string Form_Admin_Id { get; set; }
        public string Form_Admin_No { get; set; }
        public string Form_Admin_Name { get; set; }
        public string Form_Code { get; set; }
        public string Portal_Name { get; set; }
        public string Form_Type { get; set; }
        public string Form_Domain { get; set; }
        public string _External { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Status { get; set; }
        public string Join_Status { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }
    public class ParameterVaccinationDetails
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string VaccinationCentreId { get; set; }
        public string First_Dose_Date { get; set; }
        public string Age { get; set; }
        public string MobileNumber { get; set; }
        public string Address { get; set; }
        public string VaccinationCentreName { get; set; }
        public string Gender { get; set; }
        public string VaccinationToken { get; set; }
        public string DoseType { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string VaccinationDate { get; set; }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string InformStatus { get; set; }
        public string ModifiedBy { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }


    }

    public class ParameterFormMember
    {
        public string Id { get; set; }
        //  public string VaccinationCentreId { get; set; }
        public string Form_Name { get; set; }
        public string IsInstall { get; set; }
        public string EmailId { get; set; }
        public string Description { get; set; }
        public string Form_Id { get; set; }
        public string Member_No { get; set; }
        public string Member_Name { get; set; }
        public string Job_Role { get; set; }
        public string Status { get; set; }
        public string Update_Field { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    //16/05/2021
    public class ParameterFormKeyword
    {

        public string Id { get; set; }
        public string KeywordType { get; set; }
        public string Date_Col { get; set; }
        public string WhatsAppGrpName { get; set; }
        public string MobileCol { get; set; }
        public string GoogleFormName { get; set; }
        public string GoogleFormLink { get; set; }
        public string SpreadsheetId { get; set; }
        public string SingleInsertion { get; set; }
        public string AutoReplyStatus { get; set; }
        public string AutoReplyMsg { get; set; }
        public string Frequency { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string NoData { get; set; }
        public string Keyword { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Form_Id { get; set; }
        public string Form_Name { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string Form_type { get; set; }
        public string Purpose { get; set; }

        public string formname { get; set; }
        public string bot_token { get; set; }
    }

    public class ParameterFormKeywordDesription
    {

        public string Id { get; set; }
        public string Heading { get; set; }
        public string MultipleAns { get; set; }
        public string Note { get; set; }
        public string Type { get; set; }
        public string SubQuestion { get; set; }
        public string NoData { get; set; }
        public string Description { get; set; }
        public string KeywordId { get; set; }
        public string KeywordName { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
    }

    public class ParameterVaccinationDetailsCount
    {
        public string VaccinationCentreId { get; set; }
        public string Count_ByCenter { get; set; }
        public string VaccinationDate_Count { get; set; }
        public string NoData { get; set; }
        public string Total_Cnt { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string VaccinationDateWise_Count { get; set; }
        //public string VaccinationDate { get; set; }
    }

    public class ParameterReferalCode
    {

        public string Id { get; set; }
        public string LocalId { get; set; }
        public string ReferenceMobile { get; set; }
        public string IMEI { get; set; }
        public string Mobile { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Error { get; set; }
    }

    public class RegstrationRefParameter
    {
        public string ServerId { get; set; }
        public string Mobile { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IMEI { get; set; }
        public string ReferenceMobile { get; set; }
        public string Error { get; set; }
        public string FullName { get; set; }

        public string Active { get; set; }
        public string RefMobile { get; set; }
        public string DyanmicURL { get; set; }
    }

    public class ParameterRegistrationShareUrl
    {

        public string EzeeDrugAppId { get; set; }
        public string usertype { get; set; }
        public string keyword { get; set; }
        public string CreatedDate { get; set; }
        public string strDevId { get; set; }
        public string strSimSerialNo { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string firmName { get; set; }
        public string mobileNo { get; set; }
        public string address { get; set; }
        public string eMailId { get; set; }
        public string DynamicURL { get; set; }
        public string ReferenceMobile { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string RechargeAmount { get; set; }
        public string PurchaseType { get; set; }
    }

    public class ParameterVClink
    {


        public string Id { get; set; }
        public string ServerId { get; set; }
        public string AdminNumber { get; set; }
        public string Taluka { get; set; }
        public string District { get; set; }
        public string link { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string modifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ReportId { get; set; }
        public string FieldId { get; set; }
    }


    public class ParameterVaccinationCenter
    {


        public string Id { get; set; }
        public string ServerId { get; set; }
        public string Taluka { get; set; }
        public string District { get; set; }
        public string CenterId { get; set; }
        public string CenterName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class ParameterVaccinationItemName
    {


        public string ItemID { get; set; }
        public string FieldID { get; set; }
        public string ReportID { get; set; }
        public string ItemName { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class ParameterGroupOfGroup

    {


        public string Id { get; set; }
        public string NoData { get; set; }
        public string Group_Name { get; set; }
        public string Group_Desc { get; set; }
        public string Group_Status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Groupid { get; set; }
        public string IsUpdated { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

    }
    public class ParameterKeywordField

    {
        public string FieldName { get; set; }
        //  public string Id { get; set; }
        public string Error { get; set; }
        public string Nodata { get; set; }

    }

    public class ParameterChatDetails
    {

        public string Id { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string livePeriod { get; set; }
        public string Heading { get; set; }
        public string ProximityAlertRadius { get; set; }
        public string HorizontalAccuracy { get; set; }
        public string ChatId { get; set; }
        public string Date { get; set; }
        public string Bot_Token { get; set; }
        public string UPDATED { get; set; }
        public string ChatType { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }

    }

    public class ParameterPhoene
    {

        public string FirstName { get; set; }
        public string Error { get; set; }
        public string MobileNo { get; set; }
        public string NoData { get; set; }
    }
    //
    public class ParameterLoginDetails
    {

        public string Username { get; set; }
        public string NewPass { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }

        public string Error { get; set; }
        public string NoData { get; set; }
    }
    public class ParameterWhatsAppMessageDetails
    {

        public string Id { get; set; }
        public string fileName { get; set; }
        public string file { get; set; }
        public string LocalId { get; set; }
        // public string Image1 { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string SecurityStatus { get; set; }
        public string ToUsernumber { get; set; }
        public string Msg { get; set; }
        public string ImgDateTime { get; set; }
        public string MsgType { get; set; }
        public string Image { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }

    }

    public class CustomerContactDetailsParameterSrNoCount
    {

        public string SrNo { get; set; }
        public string FromSrNo { get; set; }
        public string ToSrNo { get; set; }
        public string Id { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }

    public class ParameterDiscountDeduction
    {
        public string ID { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string ModifyDate { get; set; }
        public string NoData { get; set; }
        public string UserName { get; set; }
        public string UserMobileNo { get; set; }
        public string DiscountAmount { get; set; }
        public string PaymentId { get; set; }
        public string IMEI { get; set; }
        public string UpdatedId { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class UsageStatisticsDateparameter
    {
        public string CreatedBy { get; set; }
        public string Date { get; set; }
        public string Id { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }

    }

    public class MarketinReporteparameter
    {
        public string RechargePeriod { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string RefereCount { get; set; }
        public string Marketin_Person_Name { get; set; }
        public string CoupenpurchaseType { get; set; }
        public string CoupenCode { get; set; }
        public string referralCode { get; set; }
        public string PlanAmount { get; set; }
        public string DeductionAmount { get; set; }
        public string WallelAmount { get; set; }
        public string TotalAmount { get; set; }
        public string GSTAmount { get; set; }
        public string GrandAmount { get; set; }
        public string FinalAmount { get; set; }
    }
    public class TemplateCategoryDetailsParameter
    {

        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string CategoryName { get; set; }

    }
    public class FormTemplateDetailsParameter
    {

        public string Id { get; set; }
        public string NoData { get; set; }
        public string CategoryName { get; set; }
        public string ServerId { get; set; }
        public int CategoryId { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string TemplateDescription { get; set; }
        public string TemplateName { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
    }
    public class GoogleFormQuestionDetailsParameter
    {

        public string Id { get; set; }
        public string QuestionName { get; set; }
        public string QuestionType { get; set; }
        public int TemplateId { get; set; }
        public string DeleteStatus { get; set; }
        public string UpdateStatus { get; set; }
        public string RequiredStatus { get; set; }
        public string AnswerOne { get; set; }
        public string AnswerTwo { get; set; }
        public string FormName { get; set; }
        public string FormDescription { get; set; }
        public string NoData { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
    }
    public class TemplateLoginDetailsParameter
    {

        public string Id { get; set; }
        public string NoData { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
    }

    public class Cashbookparameter
    {

        public string Date { get; set; }
        public string RegMob { get; set; }
        public string RegId { get; set; }
        public string ShopType { get; set; }
        public string IH_Mobile { get; set; }
        public string Inst_Code { get; set; }
        public string UdiseCode { get; set; }
        public string TotalReceipt { get; set; }
        public string TotalPayment { get; set; }
        public string Balance { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public string IMEI { get; set; }
        public string ModifyDate { get; set; }
        public string LegderId { get; set; }
        public string OpeningBalance { get; set; }

        public string Error { get; set; }
        public string NoData { get; set; }
        public string Id { get; set; }


    }

    public class UdhaarDetailsParameter
    {

        public string Id { get; set; }
        public string NoData { get; set; }
        public string Status { get; set; }
        public string PhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string Reg_Mobile { get; set; }
        public string Reg_Id { get; set; }
        public string Shop_Type { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string UploadStatus { get; set; }
        public string Is_Updated { get; set; }
        public string Credit { get; set; }
        public string Debit { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionDetails { get; set; }
        public string Total { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }


        public string VoucherId { get; set; }
        public string LocalVoucherId { get; set; }
        public string LedgerId { get; set; }
        public string GroupId { get; set; }
        public string VoucherType { get; set; }
        public string PaymentBy { get; set; }
        public string PaymentId { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string BankDetails { get; set; }
        public string LedgerIdDepositeId { get; set; }
        public string TransactioType { get; set; }
    }
    public class CallLogFormDetailsParameter
    {

        public string Id { get; set; }
        public string Question { get; set; }
        public string ReportType { get; set; }
        public string QueSet { get; set; }
        public string Type { get; set; }
        public string QuetionType { get; set; }
        public string Mandatory { get; set; }
        public string RegId { get; set; }
        public string ShopType { get; set; }
        public string DropdownFields { get; set; }
        public string DropdownFields_Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string settype { get; set; }

        public string groupid { get; set; }
    }

    public class FeedBackFormNaswerParameter
    {
       
     
        public string Id { get; set; }
        public string Desription { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string AnswerOne { get; set; }
        public string AnswerTwo { get; set; }
        public string AnswerThree { get; set; }
        public string AnswerFour { get; set; }
        public string AnswerFive { get; set; }
        public string AnswerSix { get; set; }
        public string AnswerSeven { get; set; }
        public string AnswerEight { get; set; }
        public string AnswerNine { get; set; }
        public string AnswerTen { get; set; }
        public string CustomerName { get; set; }
        public string CustomerNumber { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string QueSet { get; set; }
        public string settype { get; set; }
    }

    public class WatsappSendMessageParameter
    {

        public string Id { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }

    public class TemplateCategoryParameter
    {

        public string Id { get; set; }
        public string DisplaySequence { get; set; }
        public string ImageUrl { get; set; }
        public string ParentId { get; set; }
        public string NoData { get; set; }
        public string TemplateType { get; set; }
        public string Error { get; set; }
        public string DaySpecial { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Dependancy { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
    }
    public class NeedlyRegiUpdateMobileSecurity
    {

        public string Id { get; set; }
        public string mobileNo { get; set; }
        public string MobileSecurity { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }

    }

    public class ParameterPurchaseTemplate
    {
        public string Id { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Status { get; set; }
        public string TemplateId { get; set; }
        public string PaidAmount { get; set; }
        public string CreatedBy { get; set; }
        public string UserName { get; set; }
        public string MailId { get; set; }
        public string RechargeWalletId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }

    }
    public class CheckIsAppInstalledParameter
    {
        public string Id { get; set; }
        public string usermobileno { get; set; }
        public string FirstName { get; set; }
        public string IsInstalled { get; set; }
        public string LastName { get; set; }
        public string EntryDate { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }

    }
    
    public class NeedlySpreadsheetParameter
    {
        public string ServerId { get; set; }
        public string FolderId { get; set; }
        public string NoData { get; set; }
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string MobileNumber { get; set; }
        public string App { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }
    

     public class TimeEvaluationReportParameter
    {
       
        public string FromTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ToTime { get; set; }
        public string WorkDetails { get; set; }
        public string GoalRelated { get; set; }
        public string Delegated { get; set; }
        public string DelegatedDetails { get; set; }
        public string MoneyMaking { get; set; }
        public string AnswerOne { get; set; }
        public string AnswerTwo { get; set; }
        public string AnswerThree { get; set; }
        public string AnswerFour { get; set; }
        public string AnswerFive { get; set; }
        public string CreatedBy { get; set; }
        public string Imei { get; set; }
        public string Date { get; set; }
        public string ServerId { get; set; }
        public string ReportType { get; set; }
        public string NoData { get; set; }
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string MoneyType { get; set; }
    }

    public class DeleterContactDetailsParameter
    {

        
        public string Status { get; set; }
        public string Error { get; set; }
    }

    public class KeywordExcelParameter
    {
      
        public string Id { get; set; }
        public string HeadingId { get; set; }
        public string SrNo { get; set; }
        public string MobileNo { get; set; }
        public string GroupId { get; set; }
        public string Prefix { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Keyword { get; set; }
        public string Category { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public string Field11 { get; set; }
        public string Field12 { get; set; }
        public string Field13 { get; set; }
        public string Field14 { get; set; }
        public string Field15 { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Column1 { get; set; }
        public string Column2 { get; set; }
        public string Column3 { get; set; }
        public string Column4 { get; set; }
        public string Column5 { get; set; }
        public string Column6 { get; set; }
        public string Column7 { get; set; }
        public string Column8 { get; set; }
        public string Column9 { get; set; }
        public string Column10 { get; set; }
        public string Column11 { get; set; }
        public string Column12 { get; set; }
        public string Column13 { get; set; }
        public string Column14 { get; set; }
        public string Column15 { get; set; }
        public string Column16 { get; set; }
        public string Column17 { get; set; }
        public string Column18 { get; set; }
        public string Column19 { get; set; }
        public string Column20 { get; set; }
        public string Column21 { get; set; }
        public string Column22 { get; set; }
        public string Column24 { get; set; }
        public string Column25 { get; set; }
        public string Column26 { get; set; }
        public string Column23 { get; set; }

   
      
    }

    public class NewQuotationBasicDetailsParaMeters
    {
        public string Id { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Quotation_No { get; set; }
        public string Date { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Mobile { get; set; }
        public string Customer_Id { get; set; }
        public string Header_Note { get; set; }
        public string Footer_Note { get; set; }
        public string Grand_Total { get; set; }
        public string Parent_Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public List<NewQuotationItemDetailsPara> QuotationItemDetails { get; set; }
     
    }

    public class NewQuotationItemDetailsPara
    {
       
        public string Id { get; set; }
        public string NoData { get; set; }
        public string Quotation_Server_Id { get; set; }
        public string Quotation_Grp_Name { get; set; }
        public string Quotation_Grp_Id { get; set; }
        public string Item_Name { get; set; }
        public string Item_Id { get; set; }
        public string Price { get; set; }
        public string Offer_Price { get; set; }
        public string Unit { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public string Quantity { get; set; }
        public string Amount { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
     
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
       

    }

    public class FormSpreadsheetDataParameters
    {
        
        public string CreatedBy { get; set; }
        public string ResponseStatus { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string Id { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string MemberNo { get; set; }
        public string AdminNo { get; set; }
        public string Form { get; set; }
        public string FormId { get; set; }
        public string ANSWER_1 { get; set; }
        public string ANSWER_2 { get; set; }
        public string ANSWER_3 { get; set; }
        public string ANSWER_4 { get; set; }
        public string ANSWER_5 { get; set; }
        public string ANSWER_6 { get; set; }
        public string ANSWER_7 { get; set; }
        public string ANSWER_8 { get; set; }
        public string ANSWER_9 { get; set; }
        public string ANSWER_10 { get; set; }
        public string ANSWER_11 { get; set; }
        public string ANSWER_12 { get; set; }
        public string ANSWER_13 { get; set; }
        public string ANSWER_14 { get; set; }
        public string ANSWER_15 { get; set; }
        public string ANSWER_16 { get; set; }
        public string ANSWER_17 { get; set; }
        public string ANSWER_18 { get; set; }
        public string ANSWER_19 { get; set; }
        public string ANSWER_20 { get; set; }
        public string ANSWER_21 { get; set; }
        public string ANSWER_22 { get; set; }
        public string ANSWER_23 { get; set; }
        public string ANSWER_24 { get; set; }
        public string ANSWER_25 { get; set; }
        public string ANSWER_26 { get; set; }
        public string ANSWER_27 { get; set; }
        public string ANSWER_28 { get; set; }
        public string ANSWER_29 { get; set; }
        public string ANSWER_30 { get; set; }
        public string ANSWER_31 { get; set; }
        public string ANSWER_32 { get; set; }
        public string ANSWER_33 { get; set; }
        public string ANSWER_34 { get; set; }
        public string ANSWER_35 { get; set; }
        public string ANSWER_36 { get; set; }
        public string ANSWER_37 { get; set; }
        public string ANSWER_38 { get; set; }
        public string ANSWER_39 { get; set; }
        public string ANSWER_40 { get; set; }
        public string ANSWER_41 { get; set; }
        public string ANSWER_42 { get; set; }
        public string ANSWER_43 { get; set; }
        public string ANSWER_44 { get; set; }
        public string ANSWER_45 { get; set; }
        public string ANSWER_46 { get; set; }
        public string ANSWER_47 { get; set; }
        public string ANSWER_48 { get; set; }
        public string ANSWER_49 { get; set; }
        public string ANSWER_50 { get; set; }
        public string ANSWER_51 { get; set; }
        public string ANSWER_52 { get; set; }
        public string ANSWER_53 { get; set; }
        public string ANSWER_54 { get; set; }
        public string ANSWER_55 { get; set; }
        public string ANSWER_56 { get; set; }
        public string ANSWER_57 { get; set; }
        public string ANSWER_58 { get; set; }
        public string ANSWER_59 { get; set; }
        public string ANSWER_60 { get; set; }
        public string ANSWER_61 { get; set; }
        public string ANSWER_62 { get; set; }
        public string ANSWER_63 { get; set; }
        public string ANSWER_64 { get; set; }
        public string ANSWER_65 { get; set; }
        public string ANSWER_66 { get; set; }
        public string ANSWER_67 { get; set; }
        public string ANSWER_68 { get; set; }
        public string ANSWER_69 { get; set; }
        public string ANSWER_70 { get; set; }
        public string ANSWER_71 { get; set; }
        public string ANSWER_72 { get; set; }
        public string ANSWER_73 { get; set; }
        public string ANSWER_74 { get; set; }
        public string ANSWER_75 { get; set; }
        public string ANSWER_76 { get; set; }
        public string ANSWER_77 { get; set; }
        public string ANSWER_78 { get; set; }
        public string ANSWER_79 { get; set; }
        public string ANSWER_80 { get; set; }
        public string ANSWER_81 { get; set; }
        public string ANSWER_82 { get; set; }
        public string ANSWER_83 { get; set; }
        public string ANSWER_84 { get; set; }
        public string ANSWER_85 { get; set; }
        public string ANSWER_86 { get; set; }
        public string ANSWER_87 { get; set; }
        public string ANSWER_88 { get; set; }
        public string ANSWER_89 { get; set; }
        public string ANSWER_90 { get; set; }
        public string ANSWER_91 { get; set; }
        public string ANSWER_92 { get; set; }
        public string ANSWER_93 { get; set; }
        public string ANSWER_94 { get; set; }
        public string ANSWER_95 { get; set; }
        public string ANSWER_96 { get; set; }
        public string ANSWER_97 { get; set; }
        public string ANSWER_98 { get; set; }
        public string ANSWER_99 { get; set; }
        public string ANSWER_100 { get; set; }
        public string ANSWER_101 { get; set; }
        public string ANSWER_102 { get; set; }
        public string ANSWER_103 { get; set; }
        public string ANSWER_104 { get; set; }
        public string ANSWER_105 { get; set; }
        public string ANSWER_106 { get; set; }
        public string ANSWER_107 { get; set; }
        public string ANSWER_108 { get; set; }
        public string ANSWER_109 { get; set; }
        public string ANSWER_110 { get; set; }
        public string ANSWER_111 { get; set; }
        public string ANSWER_112 { get; set; }
        public string ANSWER_113 { get; set; }
        public string ANSWER_114 { get; set; }
        public string ANSWER_115 { get; set; }
        public string ANSWER_116 { get; set; }
        public string ANSWER_117 { get; set; }
        public string ANSWER_118 { get; set; }
        public string ANSWER_119 { get; set; }
        public string ANSWER_120 { get; set; }
        public string ANSWER_121 { get; set; }
        public string ANSWER_122 { get; set; }
        public string ANSWER_123 { get; set; }
        public string ANSWER_124 { get; set; }
        public string ANSWER_125 { get; set; }
        public string ANSWER_126 { get; set; }
        public string ANSWER_127 { get; set; }
        public string ANSWER_128 { get; set; }
        public string ANSWER_129 { get; set; }
        public string ANSWER_130 { get; set; }
        public string ANSWER_131 { get; set; }
        public string ANSWER_132 { get; set; }
        public string ANSWER_133 { get; set; }
        public string ANSWER_134 { get; set; }
        public string ANSWER_135 { get; set; }
        public string ANSWER_136 { get; set; }
        public string ANSWER_137 { get; set; }
        public string ANSWER_138 { get; set; }
        public string ANSWER_139 { get; set; }
        public string ANSWER_140 { get; set; }
        public string ANSWER_141 { get; set; }
        public string ANSWER_142 { get; set; }
        public string ANSWER_143 { get; set; }
        public string ANSWER_144 { get; set; }
        public string ANSWER_145 { get; set; }
        public string ANSWER_146 { get; set; }
        public string ANSWER_147 { get; set; }
        public string ANSWER_148 { get; set; }
        public string ANSWER_149 { get; set; }
        public string ANSWER_150 { get; set; }
        public string ANSWER_151 { get; set; }
        public string ANSWER_152 { get; set; }
        public string ANSWER_153 { get; set; }
        public string ANSWER_154 { get; set; }
        public string ANSWER_155 { get; set; }
        public string ANSWER_156 { get; set; }
        public string ANSWER_157 { get; set; }
        public string ANSWER_158 { get; set; }
        public string ANSWER_159 { get; set; }
        public string ANSWER_160 { get; set; }
        public string ANSWER_161 { get; set; }
        public string ANSWER_162 { get; set; }
        public string ANSWER_163 { get; set; }
        public string ANSWER_164 { get; set; }
        public string ANSWER_165 { get; set; }
        public string ANSWER_166 { get; set; }
        public string ANSWER_167 { get; set; }
        public string ANSWER_168 { get; set; }
        public string ANSWER_169 { get; set; }
        public string ANSWER_170 { get; set; }
        public string ANSWER_171 { get; set; }
        public string ANSWER_172 { get; set; }
        public string ANSWER_173 { get; set; }
        public string ANSWER_174 { get; set; }
        public string ANSWER_175 { get; set; }
        public string ANSWER_176 { get; set; }
        public string ANSWER_177 { get; set; }
        public string ANSWER_178 { get; set; }
        public string ANSWER_179 { get; set; }
        public string ANSWER_180 { get; set; }
        public string ANSWER_181 { get; set; }
        public string ANSWER_182 { get; set; }
        public string ANSWER_183 { get; set; }
        public string ANSWER_184 { get; set; }
        public string ANSWER_185 { get; set; }
        public string ANSWER_186 { get; set; }
        public string ANSWER_187 { get; set; }
        public string ANSWER_188 { get; set; }
        public string ANSWER_189 { get; set; }
        public string ANSWER_190 { get; set; }
        public string ANSWER_191 { get; set; }
        public string ANSWER_192 { get; set; }
        public string ANSWER_193 { get; set; }
        public string ANSWER_194 { get; set; }
        public string ANSWER_195 { get; set; }
        public string ANSWER_196 { get; set; }
        public string ANSWER_197 { get; set; }
        public string ANSWER_198 { get; set; }
        public string ANSWER_199 { get; set; }
        public string ANSWER_200 { get; set; }
        public string ANSWER_201 { get; set; }
        public string ANSWER_202 { get; set; }
        public string ANSWER_203 { get; set; }
        public string ANSWER_204 { get; set; }
        public string ANSWER_205 { get; set; }
        public string ANSWER_206 { get; set; }
        public string ANSWER_207 { get; set; }
        public string ANSWER_208 { get; set; }
        public string ANSWER_209 { get; set; }
        public string ANSWER_210 { get; set; }
        public string ANSWER_211 { get; set; }
        public string ANSWER_212 { get; set; }
        public string ANSWER_213 { get; set; }
        public string ANSWER_214 { get; set; }
        public string ANSWER_215 { get; set; }
        public string ANSWER_216 { get; set; }
        public string ANSWER_217 { get; set; }
        public string ANSWER_218 { get; set; }
        public string ANSWER_219 { get; set; }
        public string ANSWER_220 { get; set; }
        public string ANSWER_221 { get; set; }
        public string ANSWER_222 { get; set; }
        public string ANSWER_223 { get; set; }
        public string ANSWER_224 { get; set; }
        public string ANSWER_225 { get; set; }
        public string ANSWER_226 { get; set; }
        public string ANSWER_227 { get; set; }
        public string ANSWER_228 { get; set; }
        public string ANSWER_229 { get; set; }
        public string ANSWER_230 { get; set; }
        public string ANSWER_231 { get; set; }
        public string ANSWER_232 { get; set; }
        public string ANSWER_233 { get; set; }
        public string ANSWER_234 { get; set; }
        public string ANSWER_235 { get; set; }
        public string ANSWER_236 { get; set; }
        public string ANSWER_237 { get; set; }
        public string ANSWER_238 { get; set; }
        public string ANSWER_239 { get; set; }
        public string ANSWER_240 { get; set; }
        public string ANSWER_241 { get; set; }
        public string ANSWER_242 { get; set; }
        public string ANSWER_243 { get; set; }
        public string ANSWER_244 { get; set; }
        public string ANSWER_245 { get; set; }
        public string ANSWER_246 { get; set; }
        public string ANSWER_247 { get; set; }
        public string ANSWER_248 { get; set; }
        public string ANSWER_249 { get; set; }
        public string ANSWER_250 { get; set; }
        public string ANSWER_251 { get; set; }
        public string ANSWER_252 { get; set; }
        public string ANSWER_253 { get; set; }
        public string ANSWER_254 { get; set; }
        public string ANSWER_255 { get; set; }
        public string ANSWER_256 { get; set; }
    }

    public class CustomerWiseItemRateParameter
    {
       
        public string Id { get; set; }
        public string Quotation_Grp_Id { get; set; }
        public string Item_Name { get; set; }
        public string Item_Id { get; set; }
        public string Customer_Id { get; set; }
        public string Rate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }
    public class LadiesSafetyHelplineParaMeters
    {
        public string Id { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }
   
    public class RecordHeadingParameter
    {
       

        public string Id { get; set; }
        public string DataSource { get; set; }
        public string Pincode { get; set; }
        public string MarketingPersonNo { get; set; }
        public string MarketingPersonName { get; set; }
        public string Category { get; set; }
        public string Prefix { get; set; }
        public string FromId { get; set; }
        public string ToId { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string AdminNumber { get; set; }
        public string AdminName { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
    }

    public class PincodeDataParameter
    {


        public string Id { get; set; }
        public string Mobile_Number { get; set; }
        public string Name { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
    }
    
    public class EmergencyNumberParameter
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string CreatorName { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
    }
    public class LocationTrackingParameter
    {
        public string Id { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
        public string Lat { get; set; }
        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string Long { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }

    public class UploadAddStaff1
    {
       
       
        public string Status { get; set; }
        
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

    }

    public class CallLogCountDetailsParameter
    { 
        public string csv_Duration { get; set; }
        public string Less_Than_Ten_sec_outgoing { get; set; }
        public string Ten_To_Thirty_Sec_outgoing { get; set; }
        public string Thirty_To_Sixty_Sec_outgoing { get; set; }
        public string Sixty_To_Nighty_Sec_outgoing { get; set; }
        public string Ninety_To_Onetwinty_Sec_outgoing { get; set; }
        public string Onetwinty_To_Oneeighty_Sec_outgoing { get; set; }
        public string Oneeighty_Sec_And_More_outgoing { get; set; }
        public string Less_Than_Ten_sec { get; set; }
        public string Ten_To_Thirty_Sec { get; set; }
        public string Thirty_To_Sixty_Sec { get; set; }
        public string Sixty_To_Nighty_Sec { get; set; }
        public string Ninety_To_Onetwinty_Sec { get; set; }
        public string Onetwinty_To_Oneeighty_Sec { get; set; }
        public string Oneeighty_Sec_And_More { get; set; }
        public string Close_Duration { get; set; }
        public string Incoming_Duration { get; set; }
        public string Outgoing_Duration { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Date { get; set; }
        public string Imei { get; set; }
        public string Extra { get; set; }
        public string Repeated { get; set; }
        public string CSV { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string Close { get; set; }
        public string Rejected { get; set; }
        public string Missed { get; set; }
        public string Incoming { get; set; }
        public string Outgoing { get; set; }
        public string Id { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
    } 
    
    public class AudioFileParameter
    {
        public string FilePath { get; set; }
        public string Time { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string fileName { get; set; }
    }

    public class GenerateCouponCodeParameter
    {

        public string ID { get; set; }
        public string usrno { get; set; }
        public string Couponcode { get; set; }
        public string MktPerson { get; set; }
        public string TransferredTo { get; set; }
        public string TransferredDate { get; set; }
        public string projectname { get; set; }
        public string Sublevel1 { get; set; }
        public string Sublevel2 { get; set; }
        public string Trns_CouponStatus { get; set; }
        public string Description { get; set; }
        public string DiscountScheme { get; set; }
        public string Regular_Partial { get; set; }
        public string createdate { get; set; }
        public string createdBy { get; set; }
        public string Amount { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string Id { get; set; }
        public string LocalId { get; set; }
       
    }

    public class CouponDistributedParameter
    {
       
        public string ProjectName { get; set; }
        public string Id { get; set; }
        public string UseType { get; set; }
        public string LicenseType { get; set; }
        public string TransferredTo { get; set; }
        public string AmountPerLicense { get; set; }
        public string Remark { get; set; }
        public string ServerId { get; set; }
        public string Result { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
    }
    public class TrueVoterUserParameter
    {

        public string Id { get; set; }
        public string CreatedBy { get; set; }
        public string UserMobile { get; set; }
        public string UserName { get; set; }
        public string Imei { get; set; }
        public string Email { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string Uid { get; set; }
    }
    public class TemplateShareParameter
    {
       
        public string Id { get; set; }
        public string Template_Id { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

    }
    public class CouponDistributionPara
    {
        

             public string Id { get; set; }
             public string LicenseType { get; set; }
             public string CouponCodeName { get; set; }
             public string MktPerson { get; set; }
             public string ProjectName { get; set; }
             public string Level1 { get; set; }
             public string SponserNamme { get; set; }
             public string Amount { get; set; }
             public string AdminNo { get; set; }
             public string Result { get; set; }
             public string Error { get; set; }
             public string Description { get; set; }

    }

   
    public class DistributedCoupon
    {
        public string Error { get; set; }
        public string NoData { get; set; }
        public string usrno { get; set; }
        public string Couponcode { get; set; }
        public string MktPerson { get; set; }
        public string TransferredTo { get; set; }
        public string ProjectName { get; set; }
       
        public string Sublevel1 { get; set; }
        public string Sublevel2 { get; set; }
        public string Amount { get; set; }
        public string createdBy { get; set; }
        public string createdate { get; set; }
        public string Regular_Partial { get; set; }

        public string DiscountScheme { get; set; }
        public string Description { get; set; }
        public string Trns_CouponStatus { get; set; }
        public string Rechargeperiod { get; set; }
        public string Watermark { get; set; }
        public string AssignTo { get; set; }
    }

    public class WebsiteAvilableParameter
    {
      
        public string Error { get; set; }
        public string Status { get; set; }
        public string Id { get; set; }
        
    }

    public class EmployeedataParameter
    {

        public string Id { get; set; }

        public string empname { get; set; }
        public string role { get; set; }
        public string city { get; set; }
        public string mobileno { get; set; }
        public string CreatedBy { get; set; }
        public string Createddate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }

        public string modifiedby { get; set; }
        public string modifieddate { get; set; }

    }

    public class GenerateAllocatecouponCodeParameter
    {

        public string ID { get; set; }
        public string SrNo { get; set; }
        public string TransferredDate { get; set; }
        public string usrno { get; set; }
        public string Couponcode { get; set; }
        public string MktPerson { get; set; }
        public string IsUsed { get; set; }
        public string ReferenceMobileNo { get; set; }
        public string projectname { get; set; }
        public string Sublevel1 { get; set; }
        public string Sublevel2 { get; set; }
        public string AssignTo { get; set; }
        public string Imei { get; set; }
        public string UsedDate { get; set; }
        public string Regular_Partial { get; set; }
        public string Rechargeperiod { get; set; }
        public string CodeFor { get; set; }
        public string CodeForName { get; set; }
        public string ExpireDate { get; set; }
        public string createdate { get; set; }
        public string createdBy { get; set; }
        public string Amount { get; set; }
        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string CoupanPlan { get; set; }
        public string Scheme { get; set; }
        public string Description { get; set; }
        public string DiscountAmount { get; set; }
        public string InitialAmount { get; set; }
        public string DiscountScheme { get; set; }
        public string PaymentStatus { get; set; }
        public string TransferredTo { get; set; }
        public string DiscountEdit { get; set; }
        public string ComboStatus { get; set; }
        public string Trns_CouponStatus { get; set; }
        public string CouponStatus { get; set; }

    }

    public class CouponAllocatePara
    {


        public string Id { get; set; }
        public string LicenseType { get; set; }
        public string CouponCodeName { get; set; }
        public string MktPerson { get; set; }
        public string ProjectName { get; set; }
        public string Level1 { get; set; }
        public string SponserNamme { get; set; }
        public string Amount { get; set; }
        public string AdminNo { get; set; }
        public string Result { get; set; }
        public string Error { get; set; }
        public string Description { get; set; }
        public string SrNo { get; set; }
        public string Imei { get; set; }
        public string AssignTo { get; set; }
        public string RefAddedDate { get; set; }
        public string CodeFor { get; set; }
        public string Scheme { get; set; }
        public string Level2 { get; set; }
        public string CoupanPlan { get; set; }
        public string RegularPartial { get; set; }
        public string DiscountAmount { get; set; }

    }

    public class truevoterparameter
    {


        public string firstname { get; set; }
        public string lastname { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string keyword { get; set; }
        public string Role { get; set; }

        public string Mobilenumber { get; set; }

        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Taluka { get; set; }
        public string referralId { get; set; }

    }

    //12/01/2022
    public class UploadRechargeWallet
    {

        public string CoupenCodeImage { get; set; }
        public string Period { get; set; }
        public string WaterMarkStatus { get; set; }
        public string ccExpireDate { get; set; }
        public string OtherMobileNumber { get; set; }
        public string Device { get; set; }
        public string Parents { get; set; }
        public string Id { get; set; }
        public string OrderID { get; set; }
        public string CouponCode { get; set; }
        public string ReferralCode { get; set; }
        public string DeviceID { get; set; }
        public string UserMobile { get; set; }
        public string TransactionDate { get; set; }
        public string RechargeAmount { get; set; }
        public string TotalAmount { get; set; }
        public string UsedAmount { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string PurchaseType { get; set; }
        public string GSTAmount { get; set; }
        public string PaymentStatus { get; set; }
        public string Transaction_ID { get; set; }
        public string Billing_Amt { get; set; }
        public string Time { get; set; }
        public string Message { get; set; }
        public string Signature { get; set; }
        public string Payment_Mode { get; set; }
        public string Keyword { get; set; }
    }


    public class CompanyDetailsParameter2
    {

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string MobileNo { get; set; }
        public string AlternateMobileNo { get; set; }
        public string WhatsappMobileNo { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string MobileApp { get; set; }

        public string BusinessLocation { get; set; }
        public string BusinessName { get; set; }
        public string ProfileUrl { get; set; }
        public string Theme { get; set; }
        public string AboutCompany { get; set; }
        public string Pincode { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Imei { get; set; }
        public string WebSiteFor { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }

    }

    //27/01/22
    public class TemplateParameter
    {

        public string Id { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string NoData { get; set; }

        public string ServerId { get; set; }

        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
    }

    //28/01/22
    public class WebsiteDetails
    {

        public string Id { get; set; }
        public string htmlText1 { get; set; }
        public string htmlText2 { get; set; }
        public string htmlText3 { get; set; }
        public string htmlText4 { get; set; }
        public string htmlText5 { get; set; }

        public string PageNo { get; set; }
        public string Heading { get; set; }

        public string NoData { get; set; }

        public string ServerId { get; set; }

        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
    }

    public class DeleteGroupofGroupDetailsparameter
    {
        public string Id { get; set; }
        public string Status { get; set; }

        public string NoData { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string ServerId { get; set; }
    }

    //09/01/22
    public class VisitCountParameter
    {

        public string VisitCount { get; set; }

        public string NoData { get; set; }

        public string ServerId { get; set; }


        public string Error { get; set; }
        public string LocalId { get; set; }
    }

    //11/02/22
    public class DownloadEmail
    {

        public string eMailId { get; set; }

        public string NoData { get; set; }

        public string ServerId { get; set; }


        public string Error { get; set; }
        public string LocalId { get; set; }
    }


    public class DownloadEnquiry
    {
        public string Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNo { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerMessage { get; set; }
        public string CreatedBy { get; set; }

        public string NoData { get; set; }

        public string ServerId { get; set; }


        public string Error { get; set; }
        public string LocalId { get; set; }
    }

    public class DownlaodCustomerFeedbackparameter
    {

        public string ID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerMobileNo { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerMessage { get; set; }
        public string Rating { get; set; }
        public string CreatedBy { get; set; }

        public string ServerId { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }



    }

    public class Reportdataparameter
    {


        public string Id { get; set; }
        public string report_name { get; set; }
        public string report_title { get; set; }
        public string report_subtitle { get; set; }
        public string header_text { get; set; }
        public string header_text_orientation { get; set; }
        public string layout_type { get; set; }
        public string header_type { get; set; }
        public string report_filter_for { get; set; }
        public string report_filter_oprator { get; set; }
        public string data { get; set; }
        public string value { get; set; }
        public string report_filter_oprator1 { get; set; }
        public string data1 { get; set; }
        public string value1 { get; set; }
        public string cal_no { get; set; }
        public string createdby { get; set; }
        public string createddate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string report_type { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string formid { get; set; }
        public string project_type { get; set; }
        public string category { get; set; }
    }

    public class fieldaliasdataparameter
    {


        public string Id { get; set; }
        public string report_id { get; set; }
        public string field_name { get; set; }
        public string field_id { get; set; }
        public string alias_name { get; set; }
        public string disp_seq { get; set; }
        public string is_updated { get; set; }
        public string project_id { get; set; }
        public string form_id { get; set; }
        public string createdby { get; set; }
        public string createddate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }

    }

    public class formstructuredataparameter
    {


        public string Id { get; set; }
        public string reportid { get; set; }
        public string isupdated { get; set; }
        public string level { get; set; }
        public string fieldlevel { get; set; }
        public string fieldname { get; set; }
        public string level2 { get; set; }
        public string level3 { get; set; }
        public string type { get; set; }
        public string formula { get; set; }
        public string sequence_id { get; set; }
        public string level3_seq { get; set; }
        public string level2_seq { get; set; }
        public string FORM_ID { get; set; }
        public string createdby { get; set; }
        public string createddate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }

    }

    //18/02/22
    public class VoterUpdateCounts
    {
        public string Id { get; set; }
        [DataMember]
        public string acNo { get; set; }
        [DataMember]
        public string partNo { get; set; }

        [DataMember]
        public string mobCount { get; set; }
        [DataMember]
        public string dobCount { get; set; }
        [DataMember]
        public string domCount { get; set; }
        [DataMember]
        public string colorCount { get; set; }
        [DataMember]
        public string occupationCount { get; set; }
        [DataMember]
        public string addressCount { get; set; }
        [DataMember]
        public string locationCount { get; set; }
        [DataMember]
        public string votedCount { get; set; }
        [DataMember]
        public string notVotedCount { get; set; }
        [DataMember]
        public string repMobile { get; set; }
        [DataMember]
        public string candidateMobile { get; set; }
        [DataMember]
        public string createdBy { get; set; }
        [DataMember]
        public string modifyby { get; set; }

        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string Error { get; set; }
        public string greenColorCount { get; set; }
        public string orangeColorCount { get; set; }
        public string redColorCount { get; set; }
        public string whiteColorCount { get; set; }
        public string qualificationCount { get; set; }

        public string NoData { get; set; }




    }

    public class Grouppostparameter
    {


        public string Id { get; set; }
        public string MESSAGE { get; set; }
        public string DATE { get; set; }
        public string TIME { get; set; }
        public string USER_MOBILE_NO { get; set; }
        public string USER_NAME { get; set; }
        public string GROUP_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string MODIFY_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string MODIFIED_DATE { get; set; }
        public string IMAGE { get; set; }
        public string DESCRIPTION { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string approvestatus { get; set; }

    }

    public class Groupchatparameter
    {


        public string Id { get; set; }
        public string MESSAGE { get; set; }
        public string DATE { get; set; }
        public string TIME { get; set; }
        public string REPLY { get; set; }
        public string TO_MOBILE_NO { get; set; }
        public string USER_MOBILE_NO { get; set; }
        public string USER_NAME { get; set; }
        public string GROUP_ID { get; set; }
        public string QUESTION_ID { get; set; }
        public string CREATED_BY { get; set; }
        public string MODIFIED_BY { get; set; }
        public string CREATED_DATE { get; set; }
        public string MODIFIED_DATE { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }

    }

    public class Grouppostparameter1
    {
        public string Error { get; set; }
        public string Id { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string approvestatus { get; set; }
        public string UpdateResult { get; set; }

    }


    public class DynamicReportparameter
    {


        public string Id { get; set; }

        public string Created_by { get; set; }
        public string Modified_by { get; set; }
        public string Created_date { get; set; }
        public string Modified_date { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Category_Name { get; set; }
        public string CategoryDescription { get; set; }

    }

    public class NeedlySOSPromotionparameter
    {


        public string Id { get; set; }

        public string Createdby { get; set; }
        public string Modifyby { get; set; }
        public string Createddate { get; set; }
        public string Modifieddate { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Name { get; set; }
        public string Mobilenumber { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Count { get; set; }

    }


    public class EductionDetails
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public int SchoolType { get; set; }
        public float Percentage { get; set; }
        public String SchoolName { get; set; }
        public String SchoolPassingYear { get; set; }
        public String BordName { get; set; }
        public String Specialization { get; set; }
        public String Custome { get; set; }
        public String DegreeName { get; set; }
        public String MobileNo { get; set; }
        public String ServerId { get; set; }
        public String LocalId { get; set; }
        public String Error { get; set; }
    }


    public class DownloadEductionalDetails
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string SchoolType { get; set; }
        public string Percentage { get; set; }
        public string SchoolPassingYear { get; set; }
        public string BordName { get; set; }
        public string Specialization { get; set; }
        public string Custome { get; set; }
        public string DegreeName { get; set; }
        public string MobileNo { get; set; }
        public string SchoolName { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }



    }

    public class JobRegistrationParameter
    {


        public string Id { get; set; }

        public string Jobsector { get; set; }
        public string Governmentjobtraining { get; set; }
        public string Policebhartiinterest { get; set; }
        public string PoliceBhartiTraining { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Competitiveexams { get; set; }
        public string achievements { get; set; }
        public string preferabledistrict { get; set; }
        public string CreatedBy { get; set; }
        public string ModifyBy { get; set; }

    }

    public class SelfEmploymentBusinessdataparameter
    {


        public string Id { get; set; }

        public string NameofBusiness { get; set; }
        public string BusinessType { get; set; }
        public string BusinessDescription { get; set; }
        public string EstablishmentYear { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string OwnFundInvestmentDonetillnow { get; set; }
        public string Ownedland { get; set; }
        public string LandArea { get; set; }
        public string ExpectedamountofFundRequiredforExpansion { get; set; }
        public string Whataboutmarketingfacilitiesavailable { get; set; }
        public string ExpectedExistingannualIncomefromthisBusiness { get; set; }
        public string Doyouhaveanyspecialskills { get; set; }
        public string CreatedBy { get; set; }
        public string ModifyBy { get; set; }

    }

    public class AgricultureRegistrationdataparameter
    {


        public string Id { get; set; }

        public string NameofProject { get; set; }
        public string ownland { get; set; }
        public string Areaofland { get; set; }
        public string TypeofLand { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Ownedland { get; set; }
        public string AnnualAgricultureIncome { get; set; }
        public string BusinessType { get; set; }
        public string GovernmentSchemes { get; set; }
        public string Schemedetails { get; set; }
        public string ownfund { get; set; }
        public string fundamount { get; set; }
        public string marketingfacilties { get; set; }
        public string other { get; set; }
        public string Createdby { get; set; }
        public string Modifyby { get; set; }

    }

    public class workdetailparameter
    {


        public string Id { get; set; }

        public string FROMDATE { get; set; }
        public string TODATE { get; set; }
        public string TOTALEXPERIENCEYEAR { get; set; }
        public string TOTALEXPERIENCEMONTH { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string JOBTITLE { get; set; }
        public string COMPANYNAME { get; set; }
        public string SALARY { get; set; }
        public string FUNCTIONALAREA { get; set; }
        public string ENTRYDATE { get; set; }
        public string DEVICEIDSIMNO { get; set; }
        public string USERMOBILE { get; set; }
        public string CreateBy { get; set; }
        public string ModifyBy { get; set; }

    }

    public class SectorWiseSchme
    {
        public int Id { get; set; }
        public int status { get; set; }
        public string MobileNo { get; set; }
        public string Jobe_Role { get; set; }
        public String VTP_center { get; set; }
        public String Training_Duration { get; set; }
        public String From_Date_Training_Duration { get; set; }
        public String To_Date_Training_Duration { get; set; }
        public String Skill_Level { get; set; }
        public String Remark { get; set; }
        public String Sector { get; set; }
        public String ServerId { get; set; }
        public String LocalId { get; set; }
        public String Error { get; set; }
        public String NoData { get; set; }


    }

    public class DownloadSector
    {
        public string Id { get; set; }
        public string status { get; set; }
        public string MobileNo { get; set; }
        public string Jobe_Role { get; set; }
        public string VTP_center { get; set; }
        public string Training_Duration { get; set; }
        public string From_Date_Training_Duration { get; set; }
        public string To_Date_Training_Duration { get; set; }
        public string Skill_Level { get; set; }
        public string Remark { get; set; }
        public string Sector { get; set; }

        public string NoData { get; set; }

        public string ServerId { get; set; }


        public string Error { get; set; }
        public string LocalId { get; set; }
    }

    public class DemandJobParameter
    {
        public string Id { get; set; }
        public string sector { get; set; }
        public string jobrole { get; set; }
        public string salary { get; set; }
        public String experience { get; set; }
        public String state { get; set; }
        public String district { get; set; }
        public String taluka { get; set; }
        public String expectedupto { get; set; }
        public String entrydate { get; set; }
        public String status { get; set; }
        public String updatestatus { get; set; }
        public String mobile { get; set; }
       // public String regular_job { get; set; }
        public String category { get; set; }
        public String jobtype { get; set; }
        public String CreatedBy { get; set; }
        public String ModifiedBy { get; set; }
        public String type { get; set; }

        public String ServerId { get; set; }
        public String LocalId { get; set; }
        public String Error { get; set; }
        public String NoData { get; set; }


    }

    public class JobCategoryparameter
    {


        public string Id { get; set; }

        public string job_category_name { get; set; }
        public string status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }

        public string ModifyBy { get; set; }
        public string ModifiedDate { get; set; }



    }

    public class jobtypeparameter
    {


        public string Id { get; set; }

        public string category_id { get; set; }
        public string jobtypename { get; set; }
        public string status { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }

        public string ModifyBy { get; set; }
        public string Modifieddate { get; set; }



    }

    public class assigntaskdataparameter
    {


        public string Id { get; set; }

        public string Task_Category { get; set; }
        public string Office_Name { get; set; }
        public string Departname_Name { get; set; }
        public string Post_Name { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Staff_Name { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string TaskDescription { get; set; }
        public string TaskType { get; set; }
        public string TaskStatus { get; set; }
        public string Remark { get; set; }
        public string CreatedBy { get; set; }
        public string ModifyBy { get; set; }
        public string MobileNo { get; set; }
        public string taskstatuschangedate { get; set; }

    }

    public class UploadEzeeClassProfile
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string MobileNo { get; set; }
        public string DOB { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string Qualification { get; set; }
        public string JobDetails { get; set; }
        public string Hobbies { get; set; }
        public string Photo { get; set; }
        public string Specialities { get; set; }
        public string Contribution_To { get; set; }
        public string SocialContribution { get; set; }
        public string Organization { get; set; }
        public string ContributionToOrg { get; set; }
        public string InvolvementInOtherGroup { get; set; }
        public string GroupId { get; set; }
        public string IMEI { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string UpdatedId { get; set; }
        public string NoData { get; set; }
    }

    public class UploadCreateGroup
    {
        public string Id { get; set; }
        public string GroupId { get; set; }
        public string Status { get; set; }
        public string ActiveStatus { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string TypeOfOrg { get; set; }
        public string NameOfOrg_SL { get; set; }
        public string NameOfOrg_DL { get; set; }
        public string NameOfOrg_BL { get; set; }
        public string HoAddress { get; set; }
        public string AdminMobNo { get; set; }
        public string NameOfAdmin { get; set; }
        public string GroupOfAdmin { get; set; }
        public string GroupOfImage { get; set; }
        public string GroupType { get; set; }
        public string Instituteid { get; set; }
        public string UDISECode { get; set; }
        public string IMEI { get; set; }
        public string CreatedBy { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string UpdatedId { get; set; }
        public string DynamicUrl { get; set; }
        public string blocklevel { get; set; }
        public string Hindi_msg { get; set; }
        public string marathi_msg { get; set; }
        public string English_msg { get; set; }
        public string CreatedDate { get; set; }
        public string NoData { get; set; }
        public string MemberCount { get; set; }
        public string sharelanguage { get; set; }

    }

    public class notificationreportparameter
    {


        public string Id { get; set; }

        public string app_keyword { get; set; }
        public string app_version_name { get; set; }
        public string app_version_code { get; set; }
        public string mobile { get; set; }

        public string Error { get; set; }
        public string LocalId { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public string date { get; set; }
        public string reportid { get; set; }
        public string template_id { get; set; }
        public string duration { get; set; }
        public string time { get; set; }
        public string csv_record { get; set; }
        public string call_date_time { get; set; }
        public string application { get; set; }
        public string createdby { get; set; }
        public string ModifyBy { get; set; }


    }

    public class MemberExtraQuestionAnswerParameter
    {

        public string Id { get; set; }
        public string description { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string answer_one { get; set; }
        public string answer_two { get; set; }
        public string answer_three { get; set; }
        public string answer_four { get; set; }
        public string answer_five { get; set; }
        public string customer_name { get; set; }
        public string customer_number { get; set; }
        public string imei { get; set; }
        public string fileid { get; set; }
        public string filepath { get; set; }
        public string answer_six { get; set; }
        public string answer_seven { get; set; }
        public string NoData { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string ServerId { get; set; }
        public string answer_eight { get; set; }
        public string answer_nine { get; set; }
        public string answer_ten { get; set; }
        public string que_set { get; set; }
        public string groupid { get; set; }
        public string settype { get; set; }
        public string created_by { get; set; }
        public string QuestionType { get; set; }


    }

    public class UploadMemberGroup
    {
        public string Id { get; set; }
        public string Group_Id { get; set; }
        public string OfficeLevel { get; set; }
        public string SequenceNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MobileNo { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string ActiveStatus { get; set; }
        public string Designation { get; set; }
        public string UDISECode { get; set; }
        public string IMEI { get; set; }
        public string MemberLevel { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string Taluka { get; set; }
        public string CreatedBy { get; set; }
        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string UpdatedId { get; set; }
        public string AdminStatus { get; set; }
        public string JoinStatus { get; set; }
        public string JoinUnder { get; set; }
        public string AdminCreated { get; set; }
        public string approvedstatus { get; set; }
        public string DynamicUrl { get; set; }
        public string blocklevel { get; set; }
        public string GroupCode { get; set; }
    }

    public class UploadCalllogHistoryparameter
    {
        public string Id { get; set; }
        public string number { get; set; }
        public string call_type { get; set; }
        public string name { get; set; }
        public string duration { get; set; }
        public string date { get; set; }
        public string call_diff { get; set; }
        public string miss_call_status { get; set; }
        public string mobileno { get; set; }
        public string CreatedBy { get; set; }

        public string ServerId { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }


    }

    public class Uploadkeyworddetails
    {


        public string Id { get; set; }
        public string MobileNo { get; set; }
        public string Prefix { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string Keyword { get; set; }
        public string Category { get; set; }
        public string Status { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public string Field7 { get; set; }
        public string Field8 { get; set; }
        public string Field9 { get; set; }
        public string Field10 { get; set; }
        public string Field11 { get; set; }
        public string Field12 { get; set; }
        public string Field13 { get; set; }
        public string Field14 { get; set; }
        public string Field15 { get; set; }
        public string Error { get; set; }
        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string LocalId { get; set; }
        public string IMEI { get; set; }

    }

    public class UploadExpenseGroupParameter
    {

        public string Category { get; set; }
        public string GroupName { get; set; }
        public string HeadName { get; set; }
        public string SubHeadName { get; set; }
        public string BudgetAmount { get; set; }
        public string PlannerDuration { get; set; }

        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }

        public string NoData { get; set; }
        public string ServerId { get; set; }
        public string Id { get; set; }
        public string Error { get; set; }
        public string LocalId { get; set; }
        public string HeadCount { get; set; }

        public string BalanceAmount { get; set; }
        public string PaidAmount { get; set; }

        public string PaidCount { get; set; }
        public string BalanceCount { get; set; }
        public string BudgetCount { get; set; }
        public string ParentId { get; set; }

        public string BudgetAmountSubHeadName { get; set; }
        public string PlannerDurationSubHead { get; set; }
        public string StandardRateId { get; set; }
        public string SubHeadId { get; set; }
        public string HeadId { get; set; }

        public string LocalBodyType { get; set; }
        public string Grade { get; set; }
        public string BudgetAmountGroup { get; set; }
        public string Role { get; set; }
        public string GroupId { get; set; }





    }
}
