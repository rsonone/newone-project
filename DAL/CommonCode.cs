﻿using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Linq;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;

namespace DAL
{
    public class CommonCode
    {
        string DateFormat = "";
        string dt = "";
        //public const int FAIL=103,Ok=101,ERROR_WHILE_EXECUTION=102,NO_RECORD_FOUND=105;
        public static int OK1 = 1, INVALID_REQUEST = 9, WRONG_INPUT1 = 2, DATA_NOT_FOUND = 3, FAIL1 = 4, SQL_ERROR = 5, ERROR = 6, PRIMARY_KEY_VIOLATION = 7, USER_NAME_ALREADY_USED = 8;
        public const int WRONG_INPUT = 101, NO_RECORD_FOUND = 102, INVALID_USER = 103, ERROR_OCCUR_WHILE_EXECUTION = 104, OK = 105, RECORDS_NOT_INSERTED = 106, DUBLICATE_RECORD = 107, RECORD_ALREADY_AVAILABLE = 2627, FAIL = 108;
        public CommonCode()
        {
            if (dt != "" || dt != null)
            {
                dt = DateFormatStatus();
            }
        }
        public void storeimageinds(string result)
        {
            DataSet ds = new DataSet();
        }
        DataTable dtCategory = new DataTable();

        private static byte[] keys = { };
        private static byte[] IVs = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
        private static string EncryptionKeys = "!5623a#de";


        const string DESKey = "AQWSEDRF";
        const string DESIV = "HGFEDCBA";


        /// <summary>
        ///  Check the extension of image
        /// </summary>
        /// <param name="imageName">file name of Image</param>
        /// <returns>true if file  is  image type</returns>
        public bool CheckImageExtension(string imageName)
        {
            bool imageOk = false;

            string fileExtension = Path.GetExtension(imageName.ToString()).ToLower();
            string[] allowExtensions = { ".png", ".jpeg", ".jpg", ".gif", ".PNG", ".JPEG", ".JPG", ".GIF" };

            for (int i = 0; i < allowExtensions.Length; i++)
            {
                if (fileExtension == allowExtensions[i])
                {
                    imageOk = true;
                }
            }

            return imageOk;
        }

        public string ExecuteScalarDrug(string Sql)
        {
            string Data = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringDrug"].ConnectionString))
            {

                try
                {
                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }

        public string ExecuteScalarEzeetest(string Sql)
        {
            string Data = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
            {

                try
                {

                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }


        public string DateFormatStatus()
        {
            DateTime dt = DateTime.Now; // get current date
            double d = 12; //add hours in time
            double m = 35; //add min in time
            DateTime SystemDate = Convert.ToDateTime(dt).AddHours(d);
            SystemDate = SystemDate.AddMinutes(m);
            DateFormat = SystemDate.ToString("yyyy'-'MM'-'dd HH':'mm':'ss''");
            string ds1 = Convert.ToString(DateFormat);
            return ds1;
        }

        //------------------------------------------------School Connection String---------------------------------------------------------------

        #region SchoolDatasetString
        public DataSet SchoolDataset(string Sql)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["schoolconnectionstring"]))
            {

                try
                {
                    con.Open();
                   ds = SqlHelper.ExecuteDataset(con, CommandType.Text, Sql);

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return ds;
        }
        #endregion SchoolDatasetString

        //------------------------------------------------Myct Quary Execution Code--------------------------------------------------------------

        #region MyctQuaryExecutionCode

        public DataSet ExecuteDatasetmyct(string Sql)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringmyct"].ConnectionString))
            {
                try
                {

                 ds = SqlHelper.ExecuteDataset(con, CommandType.Text, Sql);

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return ds;
        }

        public DataSet ExecuteDataset(string Sql)
        {
            DataSet ds = new DataSet();
            
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {

                try
                {

                    ds = SqlHelper.ExecuteDataset(con, CommandType.Text, Sql);

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return ds;
        }

        public int ExecuteScalar1(string Sql)
        {
            int Data = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {

                try
                {

                    Data = Convert.ToInt32(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));


                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }

        public string ExecuteScalarmyct(string Sql)
        {
            string Data = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringmyct"].ConnectionString))
            {

                try
                {

                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }

        internal string AddSMS(object mobileNo)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuerymyct(string Sql)
        {
            int flag = 0;
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringmyct"].ConnectionString))
            {
                try
                {
                   flag = SqlHelper.ExecuteNonQuery(con, CommandType.Text, Sql);
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return flag;
        }
       
        public string ExecuteScalar(string Sql)
        {
            string Data = "";
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))

            {

                try
                {

                 Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }

        public string ExecuteScalarContact(string Sql)
        {
            string Data = "";
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DBEzeeFormForNeedlyConnectionString"].ConnectionString))

            {

                try
                {
                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }
        public string ExecuteScalarNew(string Sql)
        {
            string Data = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionEzeeeClass"].ConnectionString))
            {

                try
                {

                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }
        public string SendSMSezeetestpinnacleNew(string Mobile_Number, string Message)
        {

            Mobile_Number = "91" + Mobile_Number;
            string smsUserName = "abhinavitsol";
            string smsPassword = "@3yPR$1m";
            //System.Object stringpost = "username=" + smsUserName + "&pass=" + smsPassword + "&senderid=eZiTst&dest_mobileno=" + Mobile_Number + "&message=" + Message + "&msgType=TXT&response=Y";

            //string smsUserName = "Abhi56";
            //string smsPassword = "0a62d73a7dXX";

            System.Object stringpost = "username=" + smsUserName + "&pass=" + smsPassword + "&senderid=eZiTst&dest_mobileno=" + Mobile_Number + "&message=" + Message + "&msgType=TXT&response=Y";

            //System.Object stringpost = "user=" + smsUserName + "&key=" + smsPassword + "&mobile=" + Mobile_Number + "&message=" + Message + "&senderid=EZITST&accusage=1";




            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = null;
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;

            try
            {
                string stringResult = null;

                objWebRequest = (HttpWebRequest)WebRequest.Create("http://www.smsjust.com/sms/user/urlsms.php?");

                //objWebRequest = (HttpWebRequest)WebRequest.Create("http://mobicomm.dove-sms.com//submitsms.jsp?");


                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return (stringResult);
            }
            catch (Exception ex)
            {

                return (ex.Message);
            }
            finally
            {

                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }
        }
        public int ExecuteNonQueryNeedly(string Sql)
        {
            int flag = 0;
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {
                try
                {
                    flag = SqlHelper.ExecuteNonQuery(con, CommandType.Text, Sql);
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return flag;
        }
        public string ExecuteScalarEzeetesConnection(string Sql)
        {
            string Data = "";
            //using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))

            {

                try
                {

                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }
        public int ExecuteNonQueryEzee(string Sql)
        {
            int flag = 0;
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionStringOnlineExam"].ConnectionString))
            {
                try
                {
                    flag = SqlHelper.ExecuteNonQuery(con, CommandType.Text, Sql);
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return flag;
        }
        public int ExecuteNonQuery(string Sql)
        {
            int flag = 0;
            using (SqlConnection con = new SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {
                  flag = SqlHelper.ExecuteNonQuery(con, CommandType.Text, Sql);
                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return flag;
        }
        #endregion MyctQuaryExecutionCode

        public string ExecuteScalarNeedly(string Sql)
        {
            string Data = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {

                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    // throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }


        public string AddSMS(string usrMoNo)
        {
            string str = "";
            
                
                string SqlAdd = "select SP.Id as ID,SP.Msg as sms, SP.Sent as sentsms from SMSPushingCity SPC " +
                "Inner Join UserMaster UM on SPC.CityId=UM.usrCityId Inner Join SMSPushing SP on SP.Id=SPC.SmsPushingId where usrMobileNo='" + usrMoNo.ToString() + "' and SP.TotalMsg > SP.Sent and CONVERT(DATETIME ,SP.StartDate)<= SYSDATETIME() and not CONVERT(DATETIME ,SP.StartDate)>= GETDATE()+SP.Days";
                DataSet ds = ExecuteDatasetmyct(SqlAdd);
                string sms = "";
                int id = 0, sendSms = 0;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    id = Convert.ToInt32(dr["ID"]);
                    sms = Convert.ToString(dr["sms"]);
                    sendSms = Convert.ToInt32(dr["sentsms"]);
                }
                if (sms == "")
                {
                    str = " http://ezeeform.ezeetest.in";
                }
                else
                {
                    str = "Ad: " + sms.ToString();
                    string SqlUpdate = "Update SMSPushing set Sent=" + (sendSms + 1).ToString() + " where Id=" + id.ToString() + "";
                    int ff = ExecuteNonQuery(SqlUpdate);
                }

            
            return str;
        }
        public string Decryption(string sInput)
        {
            Byte[] inputArray = new Byte[sInput.Length];
            try
            {
                keys = System.Text.Encoding.UTF8.GetBytes(EncryptionKeys.Substring(0, 8));
                DESCryptoServiceProvider description = new DESCryptoServiceProvider();
                inputArray = Convert.FromBase64String(sInput);
                MemoryStream mstream = new MemoryStream();
                CryptoStream cstream = new CryptoStream
                (mstream, description.CreateDecryptor(keys, IVs), CryptoStreamMode.Write);
                cstream.Write(inputArray, 0, inputArray.Length);
                cstream.FlushFinalBlock();

                Encoding encoding = Encoding.UTF8;
                return encoding.GetString(mstream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Encrypt(string sInput)
        {
            try
            {
                keys = System.Text.Encoding.UTF8.GetBytes
                (EncryptionKeys.Substring(0, 8));
                DESCryptoServiceProvider description = new DESCryptoServiceProvider();
                Byte[] inputArray = Encoding.UTF8.GetBytes(sInput);
                MemoryStream mstream = new MemoryStream();
                CryptoStream cstream = new CryptoStream
                (mstream, description.CreateEncryptor(keys, IVs), CryptoStreamMode.Write);
                cstream.Write(inputArray, 0, inputArray.Length);
                cstream.FlushFinalBlock();
                return Convert.ToBase64String(mstream.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Decrypt the Encrypted String with DES to normal string
        /// </summary>
        /// <param name="stringToDecrypt">Encrypted String to Decrypt</param>
        /// <returns>Decrypted string </returns>
        public string DESDecrypt(string stringToDecrypt)//Decrypt the content
        {

            byte[] key;
            byte[] IV;

            byte[] inputByteArray;
            try
            {

                key = Convert2ByteArray(DESKey);
                IV = Convert2ByteArray(DESIV);

                stringToDecrypt = stringToDecrypt.Replace(" ", "+");

                int len = stringToDecrypt.Length;
                inputByteArray = Convert.FromBase64String(stringToDecrypt);

                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                Encoding encoding = Encoding.UTF8; return encoding.GetString(ms.ToArray());
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }



        /// <summary>
        /// Encrypt the normal string with the DES to Encrypted string 
        /// </summary>
        /// <param name="stringToEncrypt">Normal String  to Encrypt</param>
        /// <returns></returns>
        public string DESEncrypt(string stringToEncrypt)// Encrypt the content
        {

            byte[] key;
            byte[] IV;

            byte[] inputByteArray;
            try
            {

                key = Convert2ByteArray(DESKey);
                IV = Convert2ByteArray(DESIV);
                inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }

            catch (System.Exception ex)
            {
                throw ex;
            }

        }

        static byte[] Convert2ByteArray(string strInput)
        {

            int intCounter; char[] arrChar;
            arrChar = strInput.ToCharArray();

            byte[] arrByte = new byte[arrChar.Length];

            for (intCounter = 0; intCounter <= arrByte.Length - 1; intCounter++)
                arrByte[intCounter] = Convert.ToByte(arrChar[intCounter]);

            return arrByte;
        }

        public string DTGet_LocalEvent(string Date1)//2013-16-01
        {

            string Date = "";
            try
            {
                string[] tmp = Date1.Split(' ');

                tmp = tmp[0].Split('-');

                Date = tmp[2].ToString() + "/" + tmp[1].ToString() + "/" + tmp[0].ToString() + "";
            }
            catch (Exception ex)
            { }
            return Date;

        }
        public string ChangeDate(string dt)
        {
            string[] tmpdt;

            string dt1 = "0";
            try
            {
                tmpdt = dt.Split('/');
                if (tmpdt[0].Length == 1)
                    tmpdt[0] = "0" + tmpdt[0];
                if (tmpdt[1].Length == 1)
                    tmpdt[1] = "0" + tmpdt[1];

                dt1 = tmpdt[1] + "/" + tmpdt[0] + "/" + tmpdt[2];

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return dt1;
        }

        public string ChangeDatenewformat(string dt)//02/14/2013  
        {
            string[] tmpdt = dt.Split('/');
            if (tmpdt[0] == "01")
            {
                tmpdt[0] = "Jan";
            }
            else if (tmpdt[0] == "02")
            {
                tmpdt[0] = "Feb";
            }
            else if (tmpdt[0] == "03")
            {
                tmpdt[0] = "Mar";
            }
            else if (tmpdt[0] == "04")
            {
                tmpdt[0] = "Apr";
            }
            else if (tmpdt[0] == "05")
            {
                tmpdt[0] = "May";
            }
            else if (tmpdt[0] == "06")
            {
                tmpdt[0] = "Jun";
            }
            else if (tmpdt[0] == "07")
            {
                tmpdt[0] = "Jul";
            }
            else if (tmpdt[0] == "08")
            {
                tmpdt[0] = "Aug";
            }
            else if (tmpdt[0] == "09")
            {
                tmpdt[0] = "Sep";
            }
            else if (tmpdt[0] == "10")
            {
                tmpdt[0] = "Oct";
            }
            else if (tmpdt[0] == "11")
            {
                tmpdt[0] = "Nov";
            }
            else if (tmpdt[0] == "12")
            {
                tmpdt[0] = "Dec";
            }
            dt = tmpdt[1] + " " + tmpdt[0] + " " + tmpdt[2];
            return dt;
        }
        public string ChangeDatenewformat1(string dt)//14/02/2013
        {
            string[] tmpdt = dt.Split('/');
            if (tmpdt[1] == "01")
            {
                tmpdt[1] = "Jan";
            }
            else if (tmpdt[1] == "02")
            {
                tmpdt[1] = "Feb";
            }
            else if (tmpdt[1] == "03")
            {
                tmpdt[1] = "Mar";
            }
            else if (tmpdt[1] == "04")
            {
                tmpdt[1] = "Apr";
            }
            else if (tmpdt[1] == "05")
            {
                tmpdt[1] = "May";
            }
            else if (tmpdt[1] == "06")
            {
                tmpdt[1] = "Jun";
            }
            else if (tmpdt[1] == "07")
            {
                tmpdt[1] = "Jul";
            }
            else if (tmpdt[1] == "08")
            {
                tmpdt[1] = "Aug";
            }
            else if (tmpdt[1] == "09")
            {
                tmpdt[1] = "Sep";
            }
            else if (tmpdt[1] == "10")
            {
                tmpdt[1] = "Oct";
            }
            else if (tmpdt[1] == "11")
            {
                tmpdt[1] = "Nov";
            }
            else if (tmpdt[1] == "12")
            {
                tmpdt[1] = "Dec";
            }
            dt = tmpdt[0] + " " + tmpdt[1] + " " + tmpdt[2];
            return dt;
        }

        public string ChangeDt(string Date1)//07-11-2012//07/11/2012
        {
            string Date = "";
            string time = System.DateTime.Now.ToString();
            string[] tm = time.Split(' ');
            try
            {
                string[] tmp = Date1.Split(' ');

                tmp = tmp[0].Split('/');
                Date = tmp[2].ToString() + "-" + tmp[0].ToString() + "-" + tmp[1].ToString() + " " + tm[1] + " " + tm[2];
            }
            catch (Exception ex)
            { }
            return Date;
        }
        public string ChangeDtnew(string Date1)//07-11-2012//07/11/2012
        {
            string Date = "";
            try
            {
                string[] tmp = Date1.Split(' ');

                Date = tmp[0].ToString();
            }
            catch (Exception ex)
            { }
            return Date;
        }
        public string ChangeDt1(string Date1)//2013-01-18
        //2013-01-19 4:31:53 PM
        {
            string Date = "";
            string time = System.DateTime.Now.ToString();
            string[] tm = time.Split(' ');
            try
            {
                string[] tmp = Date1.Split(' ');

                tmp = tmp[0].Split('/');
                Date = tmp[2].ToString() + "-" + tmp[0].ToString() + "-" + tmp[1].ToString();
            }
            catch (Exception ex)
            { }
            return Date;
        }

        public string ChangeDt2(string Date1)//2013-1-18
        {
            string Date = "";
            string new1 = "";
            string time = System.DateTime.Now.ToString();
            string[] tm = time.Split(' ');
            try
            {
                string[] tmp = Date1.Split(' ');

                tmp = tmp[0].Split('/');
                int count = tmp[0].Length;
                string dd = tmp[0];
                if (dd.StartsWith("0"))
                {
                    int length = dd.Length;
                    new1 = dd.Substring(1, length - 1);
                    // dd = dd.Substring(1,0);
                    //  dd.Remove(0, 1);




                }
                string a = dd;

                Date = tmp[2].ToString() + "-" + new1 + "-" + tmp[1].ToString();
            }
            catch (Exception ex)
            { }
            return Date;
        }
        public string ChangeDate1(string dt)
        {
            string[] tmpdt;

            string dt1 = "0";
            try
            {
                tmpdt = dt.Split('/');
                if (tmpdt[0].Length == 1)
                    tmpdt[0] = "0" + tmpdt[0];
                if (tmpdt[1].Length == 1)
                    tmpdt[1] = "0" + tmpdt[1];

                dt1 = tmpdt[2] + "-" + tmpdt[1] + "-" + tmpdt[0];

            }
            catch (Exception ex)
            {
                string msg = ex.Message;


            }
            return dt1;
        }

        public string ChangeDate2(string dt)
        {
            string[] tmpdt;

            string dt1 = "0";
            try
            {
                tmpdt = dt.Split('/');
                if (tmpdt[0].Length == 1)
                    tmpdt[0] = "0" + tmpdt[0];
                if (tmpdt[1].Length == 1)
                    tmpdt[1] = "0" + tmpdt[1];

                dt1 = tmpdt[2] + "-" + tmpdt[0] + "-" + tmpdt[1];

            }
            catch (Exception ex)
            {
                string msg = ex.Message;


            }
            return dt1;
        }

        public string DTGet_Local(string Date1)//2013-16-01
        {

            string Date = "";
            try
            {
                string[] tmp = Date1.Split(' ');

                tmp = tmp[0].Split('-');

                Date = tmp[1].ToString() + "/" + tmp[2].ToString() + "/" + tmp[0].ToString() + "";
            }
            catch (Exception ex)
            { }
            return Date;

        }


        public string DTInsert_Local(string Date1)//06/21/2012
        {


            string Date = "";
            string time = System.DateTime.Now.ToString();
            string[] tm = time.Split(' ');
            try
            {
                string[] tmp = Date1.Split(' ');

                tmp = tmp[0].Split('/');

                Date = tmp[2].ToString() + "-" + tmp[1].ToString() + "-" + tmp[0].ToString();
            }
            catch (Exception ex)
            { }
            return Date;
            //string time = System.DateTime.Now.ToString();
            //string[] tm = time.Split(' ');
            //string Date = "";
            //try
            //{
            //    string[] tmp = Date1.Split(' ');
            //    string tm = tmp[1];
            //    tmp = tmp[0].Split('/');
            //    Date = tmp[2].ToString() + "-" + tmp[0].ToString() + "-" + tmp[1].ToString() + " " + tmp[1];
            //}
            //catch (Exception ex)
            //{ }
            //return Date;
        }

        /// <summary>
        ///  Show the city Name according to the  cityId
        /// </summary>
        /// <param name="ct">City Id through the property</param>
        /// <returns>State---->District---->City</returns>


        public string AdvMessage()
        {

            string advMsg = "Ad: Join All India Mobile Directory Users Association on come2mycity.com";
            return advMsg;
        }

        //public string AddSMS(string usrMoNo)
        //{
        //    string str = "";
        //    string SqlAdd = "select SP.Id as ID,SP.Msg as sms, SP.Sent as sentsms from SMSPushingCity SPC Inner Join UserMaster UM on SPC.CityId=UM.usrCityId Inner Join SMSPushing SP on SP.Id=SPC.SmsPushingId where usrMobileNo='" + usrMoNo.ToString() + "' and SP.TotalMsg > SP.Sent and CONVERT(DATETIME ,SP.StartDate)<= SYSDATETIME() and not CONVERT(DATETIME ,SP.StartDate)>= GETDATE()+SP.Days";
        //    DataSet ds = ExecuteDataset(SqlAdd);
        //    string sms = "";
        //    int id = 0, sendSms = 0;
        //    foreach (DataRow dr in ds.Tables[0].Rows)
        //    {
        //        id = Convert.ToInt32(dr["ID"]);
        //        sms = Convert.ToString(dr["sms"]);
        //        sendSms = Convert.ToInt32(dr["sentsms"]);
        //    }
        //    if (sms == "")
        //    {
        //        str = " www.myct.in";
        //    }
        //    else
        //    {
        //        str = "Ad: " + sms.ToString();
        //        string SqlUpdate = "Update SMSPushing set Sent=" + (sendSms + 1).ToString() + " where Id=" + id.ToString() + "";
        //        int ff = ExecuteNonQuery(SqlUpdate);
        //    }


        //    return str;
        //}

        public DataSet getLoginDetails(string UserId, string Password)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                try
                {
                    SqlParameter[] par = new SqlParameter[2];

                    par[0] = new SqlParameter("@UserId", UserId);
                    par[1] = new SqlParameter("@Password", Password);
                   ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "Authenticate", par);

                }
                catch (SqlException ex)
                {
                    ds = null;
                }
                finally
                {
                    con.Close();
                }
            }
            return ds;
        }


        public DataSet LoadMonthYear()
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("intMonth");
            dt.Columns.Add(dc);
            dc = new DataColumn("strMonth");
            dt.Columns.Add(dc);

            DataRow dr = dt.NewRow();

            dr["intMonth"] = "1";
            dr["strMonth"] = "January";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "2";
            dr["strMonth"] = "February";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "3";
            dr["strMonth"] = "March";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "4";
            dr["strMonth"] = "April";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "5";
            dr["strMonth"] = "May";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "6";
            dr["strMonth"] = "June";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "7";
            dr["strMonth"] = "July";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "8";
            dr["strMonth"] = "August";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "9";
            dr["strMonth"] = "September";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "10";
            dr["strMonth"] = "October";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "11";
            dr["strMonth"] = "November";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "12";
            dr["strMonth"] = "December";
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["intMonth"] = "";
            dr["strMonth"] = "--Select--";
            dt.Rows.Add(dr);


            DataSet ds = new DataSet();
            ds.Tables.Add(dt);


            dt = new DataTable();
            dc = new DataColumn("intYear");
            dt.Columns.Add(dc);
            dc = new DataColumn("strYear");
            dt.Columns.Add(dc);

            int Year = System.DateTime.Now.Year;

            for (int i = Year - 5; i <= Year + 5; i++)
            {
                dr = dt.NewRow();
                dr["intYear"] = i.ToString();
                dr["strYear"] = i.ToString();
                dt.Rows.Add(dr);
            }
            dr = dt.NewRow();
            dr["intYear"] = "";
            dr["strYear"] = "--Select--";
            dt.Rows.Add(dr);
            ds.Tables.Add(dt);
            return ds;
        }
        public bool OtpSubAccountSMSCountry(string sendFrom, string sendTo, string fwdMessage, int smslength, int SenderCode, string senderId)
        {
            bool flagMsgSuccess = false;
            string senderid = string.Empty;
            try
            {
                fwdMessage = fwdMessage.Replace("&", "and");
                string sendto = Convert.ToString(sendTo);
                string NewSento = sendto.Substring(sendto.Length - 10);
                fwdMessage = fwdMessage.Replace("sssss", "'");
                fwdMessage = fwdMessage.Replace("aaaaa", "&");
                //mailSendingSMS(sendFrom, sendto, fwdMessage);
                // senderid = "myctin";
                string userid = "AISPLOTP";
                string password = "otp@myct";
                fwdMessage = System.Web.HttpUtility.UrlEncode(fwdMessage);
                //fwdMessage += "www.myct.in";
                string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + NewSento + "&message=" + fwdMessage + "&sid=" + senderId + "&mtype=N&DR=Y";
                // string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + sendto + "&message=" + fwdMessage + "&mtype=N&DR=Y";
                //string url = "http://www.smscountry.com/SMSCwebservice.aspx?";
                string url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?";

                string result = "";
                StreamWriter myWriter = null;
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url + strRequest);
                objRequest.Method = "POST";
                objRequest.ContentLength = strRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strRequest);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                if (result.ToString() == "As Per Trai Regulations we are unable to Process SMS's between 9 PM to 9 AM.")
                {
                    result = "unable to Process SMS between 9 PM to 9 AM.";
                }

                //fwdMessage = fwdMessage.Replace("'", "sssss");
                //fwdMessage = fwdMessage.Replace("&", "aaaaa");
                fwdMessage = System.Web.HttpUtility.UrlDecode(fwdMessage);
                string sql = "insert into [come2mycity].[sendSMSstatus](SendFrom,SendTo,sentMessage,Flag,sendercode,smslength,EntryDate) values('" + sendFrom.ToString() + "','" + sendTo.ToString() + "','" + fwdMessage.ToString() + "','" + result.ToString() + "'," + SenderCode + ",'" + smslength + "','" + dt + "')";
                int d = ExecuteNonQuerymyct(sql);
                string resultMsg = result;
                //if (resultMsg.Contains("EZEESOFT"))
                if (resultMsg.Contains("203179963"))
                {
                    flagMsgSuccess = true;
                }
                else
                {
                    flagMsgSuccess = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            flagMsgSuccess = true;
            return flagMsgSuccess;
        }
        //ezeetest piraji kundgir
        public string SendSMSezeetestpinnacle(string Mobile_Number, string Message)
        {

            Mobile_Number = "91" + Mobile_Number;
            string smsUserName = "abhinavitsol";
            string smsPassword = "@3yPR$1m";
            System.Object stringpost = "username=" + smsUserName + "&pass=" + smsPassword + "&senderid=NEEDLY&dest_mobileno=" + Mobile_Number + "&message=" + Message + "&msgType=TXT&response=Y";

            HttpWebRequest objWebRequest = null;
            HttpWebResponse objWebResponse = null;
            StreamWriter objStreamWriter = null;
            StreamReader objStreamReader = null;

            try
            {
                string stringResult = null;

                objWebRequest = (HttpWebRequest)WebRequest.Create("http://www.smsjust.com/sms/user/urlsms.php?");

                objWebRequest.Method = "POST";

                if ((objProxy1 != null))
                {
                    objWebRequest.Proxy = objProxy1;
                }

                objWebRequest.ContentType = "application/x-www-form-urlencoded";

                objStreamWriter = new StreamWriter(objWebRequest.GetRequestStream());
                objStreamWriter.Write(stringpost);
                objStreamWriter.Flush();
                //objStreamWriter.Close();

                objWebResponse = (HttpWebResponse)objWebRequest.GetResponse();
                objStreamReader = new StreamReader(objWebResponse.GetResponseStream());
                stringResult = objStreamReader.ReadToEnd();

                objStreamReader.Close();
                return (stringResult);
            }
            catch (Exception ex)
            {
                WriteError(ex);
                return (ex.Message);
            }
            finally
            {

                if ((objStreamWriter != null))
                {
                    objStreamWriter.Close();
                }
                if ((objStreamReader != null))
                {
                    objStreamReader.Close();
                }
                objWebRequest = null;
                objWebResponse = null;
                objProxy1 = null;
            }
        }
        public void WriteError(Exception exception)//,string Page,string function
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sw.WriteLine("Exception : " + DateTime.Now.ToString() + " : " + exception.Source.Trim() + " : " + exception.Message.Trim());
                sw.WriteLine("---------------------------------------------------------------------------------------------------------");
                sw.Flush();
                sw.Close();

            }
            catch { }

        }

        public void WriteMessage(string exception)//,string Page,string function
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
                sw.WriteLine("Exception : " + DateTime.Now.ToString() + " : " + exception + " : " + exception.Trim());
                sw.WriteLine("---------------------------------------------------------------------------------------------------------");
                sw.Flush();
                sw.Close();

            }
            catch { }

        }

        public  void ErrorLogging(Exception ex)
        {
            string strPath = @"\\ErrorLog.txt";
            if (!File.Exists(strPath))
            {
                File.Create(strPath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(strPath))
            {
                sw.WriteLine("=============Error Logging ===========");
                sw.WriteLine("===========Start============= " + DateTime.Now);
                sw.WriteLine("Error Message: " + ex.Message);
                sw.WriteLine("Stack Trace: " + ex.StackTrace);
                sw.WriteLine("===========End============= " + DateTime.Now);

            }
        }

        public bool TransactionalSMSCountryezeetest(string sendFrom, string sendTo, string fwdMessage, int smslength, int SenderCode)
        {
            bool flagMsgSuccess = false;
            string senderid = string.Empty;

            try
            {
                fwdMessage = fwdMessage.Replace("&", "and");
                string sendto = Convert.ToString(sendTo);
                string NewSento = sendto.Substring(sendto.Length - 10);
                fwdMessage = fwdMessage.Replace("sssss", "'");
                fwdMessage = fwdMessage.Replace("aaaaa", "&");
                //mailSendingSMS(sendFrom, sendto, fwdMessage);
                //  senderid = "myctin";
                //  senderid = "NEEDLY";
                string userid = "ezeesoft";
                string password = "67893";
                fwdMessage = System.Web.HttpUtility.UrlEncode(fwdMessage);
                //fwdMessage += "www.myct.in";
                string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + NewSento + "&message=" + fwdMessage + "&sid=" + senderid + "&mtype=N&DR=Y";
                // string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + sendto + "&message=" + fwdMessage + "&mtype=N&DR=Y";
                //string url = "http://www.smscountry.com/SMSCwebservice.aspx?";
                string url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?";

                string result = "";
                StreamWriter myWriter = null;
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url + strRequest);
                objRequest.Method = "POST";
                objRequest.ContentLength = strRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strRequest);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                if (result.ToString() == "As Per Trai Regulations we are unable to Process SMS's between 9 PM to 9 AM.")
                {
                    result = "unable to Process SMS between 9 PM to 9 AM.";
                }

                //fwdMessage = fwdMessage.Replace("'", "sssss");
                //fwdMessage = fwdMessage.Replace("&", "aaaaa");
                fwdMessage = System.Web.HttpUtility.UrlDecode(fwdMessage);
                string sql = "insert into [come2mycity].[sendSMSstatus](SendFrom,SendTo,sentMessage,Flag,sendercode,smslength,EntryDate) values('" + sendFrom.ToString() + "','" + sendTo.ToString() + "','" + fwdMessage.ToString() + "','" + result.ToString() + "'," + SenderCode + ",'" + smslength + "','" + dt + "')";
                int d = ExecuteNonQuerymyct(sql);
                string resultMsg = result;
                //if (resultMsg.Contains("EZEESOFT"))
                if (resultMsg.Contains("203179963"))
                {
                    flagMsgSuccess = true;
                }
                else
                {
                    flagMsgSuccess = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            flagMsgSuccess = true;
            return flagMsgSuccess;
        }
        public bool TransactionalSMSCountry(string sendFrom, string sendTo, string fwdMessage, int smslength, int SenderCode, string keyword)
        {
            bool flagMsgSuccess = false;
            string senderid = string.Empty;
            try
            {
                fwdMessage = fwdMessage.Replace("&", "and");
                string sendto = Convert.ToString(sendTo);
                string NewSento = sendto.Substring(sendto.Length - 10);
                fwdMessage = fwdMessage.Replace("sssss", "'");
                fwdMessage = fwdMessage.Replace("aaaaa", "&");
                //mailSendingSMS(sendFrom, sendto, fwdMessage);
                //    senderid = "myctin";
                senderid = "NEEDLY";
                string userid = "AISPLOTP";
                string password = "otp@myct";
                fwdMessage = System.Web.HttpUtility.UrlEncode(fwdMessage);
                //fwdMessage += "www.myct.in";
                string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + NewSento + "&message=" + fwdMessage + "&sid=" + senderid + "&mtype=N&DR=Y";
                // string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + sendto + "&message=" + fwdMessage + "&mtype=N&DR=Y";
                //string url = "http://www.smscountry.com/SMSCwebservice.aspx?";
                string url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?";

                string result = "";
                StreamWriter myWriter = null;
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url + strRequest);
                objRequest.Method = "POST";
                objRequest.ContentLength = strRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strRequest);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                if (result.ToString() == "As Per Trai Regulations we are unable to Process SMS's between 9 PM to 9 AM.")
                {
                    result = "unable to Process SMS between 9 PM to 9 AM.";
                }

                //fwdMessage = fwdMessage.Replace("'", "sssss");
                //fwdMessage = fwdMessage.Replace("&", "aaaaa");
                fwdMessage = System.Web.HttpUtility.UrlDecode(fwdMessage);
                string sql = "insert into [come2mycity].[sendSMSstatus](SendFrom,SendTo,sentMessage,Flag,sendercode,smslength,EntryDate) values('" + sendFrom.ToString() + "','" + sendTo.ToString() + "','" + fwdMessage.ToString() + "','" + result.ToString() + "'," + SenderCode + ",'" + smslength + "','" + dt + "')";
                int d = ExecuteNonQuery(sql);
                string resultMsg = result;
                //if (resultMsg.Contains("EZEESOFT"))
                if (resultMsg.Contains("203179963"))
                {
                    flagMsgSuccess = true;
                }
                else
                {
                    flagMsgSuccess = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            flagMsgSuccess = true;
            return flagMsgSuccess;
        }

        public int ExecuteNonQueryEzeeTest(string Sql)
        {
            int flag = 0;
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ezeeTestConnectionString"].ConnectionString)) //SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["ConnectionStringEzeeTest"]
            {

                try
                {

                 flag = SqlHelper.ExecuteNonQuery(con, CommandType.Text, Sql);

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return flag;
        }
        public string ExecuteScalarEzeeTest(string Sql)
        {
            string Data = "";
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ezeeTestConnectionString"].ConnectionString))
            {

                try
                {

                    Data = Convert.ToString(SqlHelper.ExecuteScalar(con, CommandType.Text, Sql));

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return Data;
        }
        public DataSet ExecuteDatasetEzeeTest(string Sql)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ezeeTestConnectionString"].ConnectionString))
            {

                try
                {

                    ds = SqlHelper.ExecuteDataset(con, CommandType.Text, Sql);

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return ds;
        }

        public DataSet ExecuteDatasetEzeeTest1(string Sql)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionStringsNeedly"].ConnectionString))
            {

                try
                {

                    ds = SqlHelper.ExecuteDataset(con, CommandType.Text, Sql);

                }
                catch (SqlException ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                }
            }
            return ds;
        }
        //SqlParameter[] par = new SqlParameter[12];

        //           par[0] = new SqlParameter("@usrUserId", ur.usrUserId);
        //           par[1] = new SqlParameter("@usrMobileNo", ur.usrMobileNo);
        //           par[2] = new SqlParameter("@usrAddress", ur.usrAddress);
        //           par[3] = new SqlParameter("@usrPassword", ur.usrPassword);
        //           par[4] = new SqlParameter("@usrFirstName", ur.usrFirstName);
        //           par[5] = new SqlParameter("@usrLastName", ur.usrLastName);
        //           par[6] = new SqlParameter("@usrGender", ur.usrGender);
        //           par[7] = new SqlParameter("@usrCityId", ur.usrCityId);
        //           par[8] = new SqlParameter("@usrFriendGroup", ur.frnrelGroup);
        //           par[9] = new SqlParameter("@Status", 11);
        //           par[9].Direction = ParameterDirection.Output;

        //           SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "spUserRegistrationIntialInsert", par);
        //           status = (int)par[9].Value;


        #region MAHARASHTRA SMS

        //private string aid = "639250";
        //private string pin = "M@h123";

        private WebProxy objProxy1 = null;

        public bool TransactionalSMSCountry(string sendFrom, string sendTo, string fwdMessage)
        {
            bool flagMsgSuccess = false;
            string senderid = string.Empty;
            try
            {
                fwdMessage = fwdMessage.Replace("&", "and");
                string sendto = Convert.ToString(sendTo);
                string NewSento = sendto.Substring(sendto.Length - 10);
                fwdMessage = fwdMessage.Replace("sssss", "'");
                fwdMessage = fwdMessage.Replace("aaaaa", "&");
                //mailSendingSMS(sendFrom, sendto, fwdMessage);
                // senderid = "myctin";
                senderid = "NEEDLY";
                string userid = "ezeesoft";
                string password = "67893";
                fwdMessage = System.Web.HttpUtility.UrlEncode(fwdMessage);
                fwdMessage += " www.myct.in";
                string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + NewSento + "&message=EzeeMarketing Login Password " + fwdMessage + "&sid=" + senderid + "&mtype=N&DR=Y";
                // string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + sendto + "&message=" + fwdMessage + "&mtype=N&DR=Y";
                //string url = "http://www.smscountry.com/SMSCwebservice.aspx?";
                string url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?";

                string result = "";
                StreamWriter myWriter = null;
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url + strRequest);
                objRequest.Method = "POST";
                objRequest.ContentLength = strRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strRequest);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                if (result.ToString() == "As Per Trai Regulations we are unable to Process SMS's between 9 PM to 9 AM.")
                {
                    result = "unable to Process SMS between 9 PM to 9 AM.";
                }
                
                //fwdMessage = fwdMessage.Replace("'", "sssss");
                //fwdMessage = fwdMessage.Replace("&", "aaaaa");
                fwdMessage = System.Web.HttpUtility.UrlDecode(fwdMessage);
                string sql = "insert into [come2mycity].[sendSMSstatus](SendFrom,SendTo,sentMessage,Flag,sendercode,smslength,EntryDate) values('" + sendFrom.ToString() + "','" + sendTo.ToString() + "','" + fwdMessage.ToString() + "','" + result.ToString() + "'," + 107 + ",'" + 10 + "','" + dt + "')";
                int d = ExecuteNonQuery(sql);
                string resultMsg = result;
                //if (resultMsg.Contains("EZEESOFT"))
                if (resultMsg.Contains("203179963"))
                {

                    flagMsgSuccess = true;
                }
                else
                {
                    flagMsgSuccess = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            flagMsgSuccess = true;
            return flagMsgSuccess;
        }


        #endregion
        public bool SendMessage1(string sendFrom, string sendTo, string fwdMessage) ////Not in use
        {
            bool flagMsgSuccess = false;

            try
            {
                fwdMessage = fwdMessage.Replace("&", "and");
                string sendto = Convert.ToString(sendTo);
                fwdMessage = fwdMessage.Replace("sssss", "'");
                fwdMessage = fwdMessage.Replace("aaaaa", "&");
                // mailSendingSMS(sendFrom, sendto, fwdMessage);
                //////string userid = "ezeesoft";
                //////string password = "abhinav313";
                string userid = "ezeesoft";
                string password = "67893";
                //string userid = "come2mycity";
                ////string password = "ezeesoft";
                ////string userid = "come2mycity-priority";
                //string password = "1j837BhZ2";
                ////// string strRequest = "username=" + userid + "&password=" + password + "&sender=" + gsmSenderId + "&to=" + cntNo + "&message=" + msg + "&priority=" + 1 + "&dnd=" + 1 + "&unicode=" + 0;

                //////string strRequest = "userid=" + userid + "&pwd=" + password + "&msgtype=s&ctype=1" + "&sender=" + sendFrom + "&pno=" + sendTo + "&msgtxt=" + fwdMessage + "&alert=1 ";

                ////string strRequest = "username=" + userid + "&password=" + password + "&message=" + fwdMessage + "&sender=" + sendFrom + "&numbers=" + sendTo + "&type=" + 1 + "";
                //"User=xxxx&passwd=xxxx&mobilenumber=919xxxxxxxxx&message=xxxx&sid=xxxx&mtype=N&DR=Y"
                string strRequest = "User=" + userid + "&passwd=" + password + "&mobilenumber=" + sendto + "&message=" + fwdMessage + "&sid=www.myct.in&mtype=N&DR=Y";
                //////string url = "http://203.122.58.168/prepaidgetbroadcast/PrepaidGetBroadcast?";
                ////string url = "http://smsblaster.info/pushsms.php?";
                string url = "http://www.smscountry.com/SMSCwebservice.asp?";
                //string url = "http://182.18.189.124:8800/sendsms.php?";
                string result = "";
                StreamWriter myWriter = null;
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url + strRequest);
                objRequest.Method = "POST";
                objRequest.ContentLength = strRequest.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strRequest);
                myWriter.Close();
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    result = sr.ReadToEnd();
                    sr.Close();
                }
                if (result.ToString() == "As Per Trai Regulations we are unable to Process SMS's between 9 PM to 9 AM.")
                {
                    result = "unable to Process SMS between 9 PM to 9 AM.";
                }

                fwdMessage = fwdMessage.Replace("'", "sssss");
                fwdMessage = fwdMessage.Replace("&", "aaaaa");
                string sql = "insert into come2mycity.sendSMSstatus(SendFrom,SendTo,sentMessage,Flag,EntryDate) values('" + sendFrom.ToString() + "','" + sendTo.ToString() + "','" + fwdMessage.ToString() + "','" + result.ToString() + "','" + dt + "')";
                int d = ExecuteNonQuery(sql);
                string resultMsg = result;
                //if (resultMsg.Contains("EZEESOFT"))
                if (resultMsg.Contains("203179963"))
                {

                    flagMsgSuccess = true;
                }
                else
                {
                    flagMsgSuccess = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            flagMsgSuccess = true;
            return flagMsgSuccess;
        }



        public void errorhandling(string errorhandling, string Page, string functiondata, string CreatedBy)
        {
            using (SqlConnection con = new System.Data.SqlClient.SqlConnection(WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd3 = new SqlCommand("uspInsertErrorHandling", con);
                    cmd3.CommandType = CommandType.StoredProcedure;
                    cmd3.Parameters.AddWithValue("@errorhandling", errorhandling);
                    cmd3.Parameters.AddWithValue("@Page", Page);
                    cmd3.Parameters.AddWithValue("@functiondata", functiondata);
                    cmd3.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                    con.Open();
                    cmd3.ExecuteNonQuery();
                    con.Close();

                }
                catch (Exception ex)
                {

                }
            }
        }
    }

}
